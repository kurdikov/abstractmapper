namespace MethodResolverBenchmarks
{
    using System.Collections.Generic;
    using System.Reflection;
    using Antlr4.Runtime.Misc;
    using BenchmarkDotNet.Attributes;
    using BenchmarkDotNet.Attributes.Jobs;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.PseudoExpressionParser.Bindings;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator;
    using TsSoft.Expressions.PseudoExpressionParser.Resolvers;

    [LegacyJitX86Job, LegacyJitX64Job, RyuJitX64Job]
    public class MethodResolvers
    {
        private readonly IExpressionCreator _simple;
        private readonly IExpressionCreator _new;

        public MethodResolvers()
        {
            AbstractMapperDependenciesFactory.AddBindings(new TsSoftExpressionsPseudoExpressionParserBindings());
            var simple = new SimpleMethodResolver(AbstractMapperDependenciesFactory.GetDependency<IParameterTypeMatcher>(null, null));
            var full = new MethodResolver(
                new DelegateTypeHelper(), AbstractMapperDependenciesFactory.GetDependency<ILambdaBodyMaker>(null, null),
                new ConversionChecker());

            _simple = AbstractMapperDependenciesFactory.GetDependency<IExpressionCreator>(
                MockObjects.Are(simple), null, null);
            _new = AbstractMapperDependenciesFactory.GetDependency<IExpressionCreator>(
                MockObjects.Are(full), null, null);
        }

        [Benchmark]
        public object Simple()
        {
            return _simple.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.GroupBy(c => c.Prop1, c => c.Parent, (p1, coll) => coll.First())");
        }

        [Benchmark]
        public object New()
        {
            return _new.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.GroupBy(c => c.Prop1, c => c.Parent, (p1, coll) => coll.First())");
        }

        public class TestEntity
        {
            public int Prop1 { get; set; }

            public string Prop2 { get; set; }

            public int? Prop3 { get; set; }

            public TestEntity Parent { get; set; }

            public ICollection<TestEntity> Children { get; set; }

            public int ExternalId { get; set; }
        }
    }
}
