﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using BenchmarkDotNet.Running;
using JetBrains.Annotations;
using TsSoft.AbstractMapper.Factory;
using TsSoft.Expressions.Helpers;
using TsSoft.Expressions.Helpers.Reflection;
using TsSoft.Expressions.Models.Reflection;
using TsSoft.Expressions.PseudoExpressionParser.Bindings;
using TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator;
using TsSoft.Expressions.PseudoExpressionParser.Resolvers;

namespace MethodResolverBenchmarks
{
    class Program
    {
        private static void RunExpressionCreators()
        {
            AbstractMapperDependenciesFactory.AddBindings(new TsSoftExpressionsPseudoExpressionParserBindings());
            AbstractMapperDependenciesFactory.RemoveBinding<IMethodResolver>();

            var simple = new SimpleMethodResolver(AbstractMapperDependenciesFactory.GetDependency<IParameterTypeMatcher>(null, null));
            var full = new MethodResolver(
                new DelegateTypeHelper(), AbstractMapperDependenciesFactory.GetDependency<ILambdaBodyMaker>(null, null),
                new ConversionChecker());

            AbstractMapperDependenciesFactory.AddStaticHelper<IMethodResolver>(simple);
            var sex = (IExpressionCreator)AbstractMapperDependenciesFactory.GetDependency(
                typeof(IExpressionCreator), null, null, MockObjects.Are(simple));
            AbstractMapperDependenciesFactory.AddStaticHelper<IMethodResolver>(full);
            var nex = (IExpressionCreator)AbstractMapperDependenciesFactory.GetDependency(
                typeof(IExpressionCreator), null, null, MockObjects.Are(full));

            const int count = 100000;

            var sw = Stopwatch.StartNew();
            object x;
            x = sex.CreateExpression<MethodResolvers.TestEntity, double>("x", "x.Children.Average(c => c.Prop1)");
            x = nex.CreateExpression<MethodResolvers.TestEntity, double>("x", "x.Children.Average(c => c.Prop1)");
            x = sex.CreateExpression<MethodResolvers.TestEntity, IEnumerable<MethodResolvers.TestEntity>>(
                    "x", "x.Children.GroupBy(c => c.Prop1, c => c.Parent, (p1, coll) => coll.First())");
            x = nex.CreateExpression<MethodResolvers.TestEntity, IEnumerable<MethodResolvers.TestEntity>>(
                    "x", "x.Children.GroupBy(c => c.Prop1, c => c.Parent, (p1, coll) => coll.First())");
            Console.WriteLine("Ran first time: {0}", sw.Elapsed);

            Console.WriteLine("Running simple");
            sw.Restart();
            for (int i = 0; i < count; ++i)
            {
                x = sex.CreateExpression<MethodResolvers.TestEntity, double>("x", "x.Children.Average(c => c.Prop1)");
                x = sex.CreateExpression<MethodResolvers.TestEntity, IEnumerable<MethodResolvers.TestEntity>>("x", "x.Children.GroupBy(c => c.Prop1, c => c.Parent, (p1, coll) => coll.First())");
            }
            Console.WriteLine("Ran simple: {0}", sw.Elapsed);

            Console.WriteLine("Running new");
            sw.Restart();
            for (int i = 0; i < count; ++i)
            {
                x = nex.CreateExpression<MethodResolvers.TestEntity, double>("x", "x.Children.Average(c => c.Prop1)");
                x = nex.CreateExpression<MethodResolvers.TestEntity, IEnumerable<MethodResolvers.TestEntity>>("x", "x.Children.GroupBy(c => c.Prop1, c => c.Parent, (p1, coll) => coll.First())");
            }
            Console.WriteLine("Ran new: {0}", sw.Elapsed);
        }


        static void Main(string[] args)
        {
            if (args != null && args.Length == 1 && args[0] == "manual")
            {
                RunExpressionCreators();
            }
            else
            {
                var runner = BenchmarkSwitcher.FromTypes(new Type[]
                {
                    typeof(MethodResolvers),
                });
                runner.Run(args);
            }

            Console.ReadLine();
            return;
        }
    }
}
