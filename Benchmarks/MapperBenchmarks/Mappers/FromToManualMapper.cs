﻿namespace MapperBenchmarks
{
    using System.Linq;
    using TsSoft.AbstractMapper;

    class FromToManualMapper : IMapper<From, To>
    {
        public To Map(From from)
        {
            if (from == null)
            {
                return null;
            }
            return new To
            {
                Byte = from.Byte.ToString(),
                Short = from.Short.ToString(),
                Int = from.Int.ToString(),
                Double = from.Double.ToString(),
                Decimal = from.Decimal.ToString(),
                Children = from.Children != null ? from.Children.Where(c => c.Byte % 2 == 0).Select(Map).ToList() : null,
                Kv = from.Kv.ToString(),
                Long = from.Long.ToString(),
                String = int.Parse(from.String),
            };
        }
    }
}
