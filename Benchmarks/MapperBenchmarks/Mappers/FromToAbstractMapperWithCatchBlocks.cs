﻿namespace MapperBenchmarks
{
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;

    class FromToAbstractMapperWithCatchBlocks : FromToAbstractMapper
    {
        public FromToAbstractMapperWithCatchBlocks([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override bool GenerateCatchBlocks
        {
            get { return true; }
        }
    }
}
