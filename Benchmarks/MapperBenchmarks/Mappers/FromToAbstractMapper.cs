﻿namespace MapperBenchmarks
{
    using System.Linq;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Rules;

    class FromToAbstractMapper : Mapper<From, To>
    {
        public FromToAbstractMapper([NotNull]IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override IMapRules<To, From> MapRules
        {
            get 
            { 
                return new MapRules<To, From>
                {
                    {t => t.String, f => f.String, s => int.Parse(s)},
                    {t => t.Children, f => f.Children.Where(c => c.Byte % 2 == 0)}
                }; 
            }
        }


        protected override InnerMapperStrategy InnerMapperStrategy
        {
            get { return InnerMapperStrategy.ResolveOnMapperBuilding; }
        }

        protected override bool GenerateCatchBlocks
        {
            get { return false; }
        }
    }
}
