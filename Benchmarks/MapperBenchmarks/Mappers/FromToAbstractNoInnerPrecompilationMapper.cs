﻿namespace MapperBenchmarks
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;

    class FromToAbstractNoInnerPrecompilationMapper : FromToAbstractMapper
    {
        public FromToAbstractNoInnerPrecompilationMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override Func<From, To> CompileMapper()
        {
            return MapperExpression.Expression.Compile();
        }
    }
}
