﻿namespace MapperBenchmarks
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.Expressions.Helpers.Closures;
    using TsSoft.Expressions.Helpers.Reflection;

    class FromToAbstractCompileToMethodMapper : FromToAbstractMapper
    {
        public FromToAbstractCompileToMethodMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {

        }

        protected override Func<From, To> CompileMapper()
        {
            return new MethodBuilderLambdaCompiler(new MemberInfoHelper()).Compile(MapperExpression.Expression);
        }
    }
}
