``` ini

BenchmarkDotNet=v0.10.8, OS=Windows 10 Threshold 2 (10.0.10586)
Processor=Intel Core i7-3770K CPU 3.50GHz (Ivy Bridge), ProcessorCount=8
Frequency=3421328 Hz, Resolution=292.2842 ns, Timer=TSC
  [Host]       : Clr 4.0.30319.42000, 64bit RyuJIT-v4.6.1590.0
  LegacyJitX64 : Clr 4.0.30319.42000, 64bit LegacyJIT/clrjit-v4.6.1590.0;compatjit-v4.6.1590.0
  LegacyJitX86 : Clr 4.0.30319.42000, 32bit LegacyJIT-v4.6.1590.0
  RyuJitX64    : Clr 4.0.30319.42000, 64bit RyuJIT-v4.6.1590.0

Runtime=Clr  

```
 |                                         Method |          Job |       Jit | Platform |      Mean |     Error |    StdDev |    Median |
 |----------------------------------------------- |------------- |---------- |--------- |----------:|----------:|----------:|----------:|
 |                                 AbstractMapper | LegacyJitX64 | LegacyJit |      X64 |  3.749 ms | 0.0728 ms | 0.1155 ms |  3.701 ms |
 |                                   ManualMapper | LegacyJitX64 | LegacyJit |      X64 |  2.701 ms | 0.0540 ms | 0.1090 ms |  2.683 ms |
 | AbstractMapperWithoutInnerLambdaPrecompilation | LegacyJitX64 | LegacyJit |      X64 |  3.828 ms | 0.0735 ms | 0.0817 ms |  3.814 ms |
 |                  AbstractMapperCompileToMethod | LegacyJitX64 | LegacyJit |      X64 |  2.835 ms | 0.0562 ms | 0.1198 ms |  2.772 ms |
 |                                     AutoMapper | LegacyJitX64 | LegacyJit |      X64 | 20.812 ms | 0.4268 ms | 0.6256 ms | 20.601 ms |
 |                                 AbstractMapper | LegacyJitX86 | LegacyJit |      X86 |  3.009 ms | 0.0581 ms | 0.0543 ms |  2.990 ms |
 |                                   ManualMapper | LegacyJitX86 | LegacyJit |      X86 |  2.292 ms | 0.0439 ms | 0.0389 ms |  2.286 ms |
 | AbstractMapperWithoutInnerLambdaPrecompilation | LegacyJitX86 | LegacyJit |      X86 |  3.283 ms | 0.0646 ms | 0.1245 ms |  3.229 ms |
 |                  AbstractMapperCompileToMethod | LegacyJitX86 | LegacyJit |      X86 |  2.467 ms | 0.0487 ms | 0.1157 ms |  2.432 ms |
 |                                     AutoMapper | LegacyJitX86 | LegacyJit |      X86 | 14.841 ms | 0.3071 ms | 0.2722 ms | 14.764 ms |
 |                                 AbstractMapper |    RyuJitX64 |    RyuJit |      X64 |  3.651 ms | 0.0725 ms | 0.1680 ms |  3.603 ms |
 |                                   ManualMapper |    RyuJitX64 |    RyuJit |      X64 |  2.640 ms | 0.0435 ms | 0.0386 ms |  2.629 ms |
 | AbstractMapperWithoutInnerLambdaPrecompilation |    RyuJitX64 |    RyuJit |      X64 |  3.974 ms | 0.0794 ms | 0.1260 ms |  3.916 ms |
 |                  AbstractMapperCompileToMethod |    RyuJitX64 |    RyuJit |      X64 |  2.999 ms | 0.0633 ms | 0.1480 ms |  2.938 ms |
 |                                     AutoMapper |    RyuJitX64 |    RyuJit |      X64 | 20.737 ms | 0.4106 ms | 0.7507 ms | 20.434 ms |
