﻿namespace MapperBenchmarks
{
    using System.Collections.Generic;

    public class To
    {
        public string Byte { get; set; }
        public string Short { get; set; }
        public string Int { get; set; }
        public string Long { get; set; }
        public string Decimal { get; set; }
        public string Double { get; set; }
        public string Kv { get; set; }
        public int String { get; set; }
        public ICollection<To> Children { get; set; }
    }
}
