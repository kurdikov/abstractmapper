﻿namespace MapperBenchmarks
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using AutoMapper;
    using BenchmarkDotNet.Running;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Factory;

    class Program
    {
        private static From[] GetFroms([NotNull] Stopwatch sw)
        {
            sw.Restart();
            var froms = Enumerable.Range(0, 1000).Select(
                i => new From
                {
                    Byte = (byte)(i % byte.MaxValue + 1),
                    Short = (short)(i % byte.MaxValue + 1),
                    Int = i,
                    Long = i,
                    Decimal = i,
                    Double = i,
                    Kv = new KeyValuePair<int, int>(i, i + 1),
                    String = i.ToString(CultureInfo.InvariantCulture),
                    Children = Enumerable.Range(0, 1000).Select(
                        j => new From
                        {
                            Byte = (byte)(j % byte.MaxValue + 1),
                            Short = (short)(j % byte.MaxValue + 1),
                            Int = j,
                            Long = j,
                            Decimal = j,
                            Double = j,
                            Kv = new KeyValuePair<int, int>(j, j - 1),
                            String = j.ToString(CultureInfo.InvariantCulture),
                        }).ToList(),
                }).ToArray();
            Console.WriteLine("Created froms: {0}", sw.Elapsed);
            return froms;
        }


        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        private static void RunMapper([NotNull]From[] froms, [NotNull]IMapper<From, To> mapper)
        {
            foreach (var from in froms)
            {
                mapper.Map(from);
            }
        }

        private static void RunMappers([NotNull]From[] froms, [NotNull]Stopwatch sw)
        {
            sw.Restart();
            var mapper = AbstractMapperFactory.Create<FromToAbstractMapper>(MapperTypes.Are(typeof(FromToAbstractMapper)));
            var mapperWithCatchBlocks = AbstractMapperFactory.Create<FromToAbstractMapperWithCatchBlocks>(MapperTypes.Are(typeof(FromToAbstractMapperWithCatchBlocks)));
            var mapperWithCompilationToMethod = AbstractMapperFactory.Create<FromToAbstractCompileToMethodMapper>(MapperTypes.Are(typeof(FromToAbstractCompileToMethodMapper)));
            var manualMapper = AbstractMapperDependenciesFactory.GetDependency<FromToManualMapper>(MapperTypes.Are(typeof(FromToManualMapper)), Entities.None, External.None);
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<From, To>()
                    .ForMember(f => f.Children, mcfg =>
                    {
                        mcfg.MapFrom(f => f.Children.Where(c => c.Byte % 2 == 0));
                    });
            });
            var automapper = config.CreateMapper();
            Console.WriteLine("Created mappers: {0}", sw.Elapsed);

            sw.Restart();
            mapper.Map(froms[0]);
            mapperWithCatchBlocks.Map(froms[0]);
            mapperWithCompilationToMethod.Map(froms[0]);
            manualMapper.Map(froms[0]);
            automapper.Map<From, To>(froms[0]);
            Console.WriteLine("Mapped first elements: {0}", sw.Elapsed);
            sw.Stop();

            sw.Restart();
            RunMapper(froms, mapper);
            Console.WriteLine("TsSoft.AbstractMapper: {0}", sw.Elapsed);
            Debug.Print("TsSoft.AbstractMapper: {0}", sw.Elapsed);

            sw.Restart();
            RunMapper(froms, mapperWithCatchBlocks);
            Console.WriteLine("TsSoft.AbstractMapper catching exceptions: {0}", sw.Elapsed);
            Debug.Print("TsSoft.AbstractMapper catching exceptions: {0}", sw.Elapsed);

            sw.Restart();
            RunMapper(froms, mapperWithCompilationToMethod);
            Console.WriteLine("TsSoft.AbstractMapper compiled to method: {0}", sw.Elapsed);
            Debug.Print("TsSoft.AbstractMapper catching exceptions: {0}", sw.Elapsed);

            sw.Restart();
            RunMapper(froms, manualMapper);
            Console.WriteLine("Manual               : {0}", sw.Elapsed);
            Debug.Print("Manual               : {0}", sw.Elapsed);

            sw.Restart();
            foreach (var from in froms)
            {
                var res = automapper.Map<From, To>(from);
            }
            Console.WriteLine("AutoMapper: {0}", sw.Elapsed);
            Debug.Print("AutoMapper: {0}", sw.Elapsed);
        }

        static void Main(string[] args)
        {
            if (args != null && args.Length == 1 && args[0] == "manual")
            {
                var sw = new Stopwatch();
                var froms = GetFroms(sw);
                RunMappers(froms, sw);
            }
            else
            {
                var runner = BenchmarkSwitcher.FromTypes(new Type[]
                {
                    typeof(Mappers),
                });
                runner.Run(args);
            }

            Console.ReadLine();
            return;
        }
    }
}
