namespace MapperBenchmarks
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using AutoMapper;
    using BenchmarkDotNet.Attributes;
    using BenchmarkDotNet.Attributes.Jobs;
    using TsSoft.AbstractMapper.Factory;

    [LegacyJitX86Job, LegacyJitX64Job, RyuJitX64Job]
    public class Mappers
    {
        private readonly From _from;
        private FromToAbstractMapper _abstractMapper;
        private FromToManualMapper _manualMapper;
        private FromToAbstractNoInnerPrecompilationMapper _noPrecompilationMapper;
        private FromToAbstractCompileToMethodMapper _compileToMethodMapper;
        private readonly IMapper _automapper;

        public Mappers()
        {
            int i = 0;
            _from = new From
            {
                Byte = (byte)(i % byte.MaxValue + 1),
                Short = (short)(i % byte.MaxValue + 1),
                Int = i,
                Long = i,
                Decimal = i,
                Double = i,
                Kv = new KeyValuePair<int, int>(i, i + 1),
                String = i.ToString(CultureInfo.InvariantCulture),
                Children = Enumerable.Range(0, 100).Select(
                    j => new From
                    {
                        Byte = (byte)(j % byte.MaxValue + 1),
                        Short = (short)(j % byte.MaxValue + 1),
                        Int = j,
                        Long = j,
                        Decimal = j,
                        Double = j,
                        Kv = new KeyValuePair<int, int>(j, j - 1),
                        String = j.ToString(CultureInfo.InvariantCulture),
                        Children = Enumerable.Range(0, 100).Select(
                            k => new From {Byte = (byte)(k % 2), String = "1"}).ToList()
                    }).ToList(),
            };

            _abstractMapper = AbstractMapperFactory.Create<FromToAbstractMapper>(MapperTypes.Are(typeof(FromToAbstractMapper)));
            _noPrecompilationMapper = AbstractMapperFactory.Create<FromToAbstractNoInnerPrecompilationMapper>(MapperTypes.Are(typeof(FromToAbstractNoInnerPrecompilationMapper)));
            _manualMapper = AbstractMapperDependenciesFactory.GetDependency<FromToManualMapper>(MapperTypes.Are(typeof(FromToManualMapper)), Entities.None, External.None);
            _compileToMethodMapper = AbstractMapperFactory.Create<FromToAbstractCompileToMethodMapper>(MapperTypes.Are(typeof(FromToAbstractCompileToMethodMapper)));
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<From, To>()
                    .ForMember(f => f.Children, mcfg =>
                    {
                        mcfg.MapFrom(f => f.Children.Where(c => c.Byte % 2 == 0));
                    });
            });
            _automapper = config.CreateMapper();
        }

        [Benchmark]
        public To AbstractMapper()
        {
            return _abstractMapper.Map(_from);
        }

        [Benchmark]
        public To ManualMapper()
        {
            return _manualMapper.Map(_from);
        }

        [Benchmark]
        public To AbstractMapperWithoutInnerLambdaPrecompilation()
        {
            return _noPrecompilationMapper.Map(_from);
        }

        [Benchmark]
        public To AbstractMapperCompileToMethod()
        {
            return _compileToMethodMapper.Map(_from);
        }

        [Benchmark]
        public To AutoMapper()
        {
            return _automapper.Map<From, To>(_from);
        }
    }
}
