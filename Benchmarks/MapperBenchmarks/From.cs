﻿namespace MapperBenchmarks
{
    using System.Collections.Generic;

    public class From
    {
        public byte Byte { get; set; }
        public short Short { get; set; }
        public int Int { get; set; }
        public long Long { get; set; }
        public decimal Decimal { get; set; }
        public double Double { get; set; }
        public KeyValuePair<int, int> Kv { get; set; }
        public string String { get; set; }
        public ICollection<From> Children { get; set; }
    }
}
