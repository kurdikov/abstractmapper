﻿namespace ObjectClonemakerBenchmarks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Reflection;

    /// <summary>
    /// Создаёт глубокие копии объектов
    /// </summary>
    public class ReflectionObjectClonemaker : IObjectClonemaker
    {
        [NotNull]private readonly IFlatPathParser _pathMapper;

        [NotNull]private readonly MethodInfo _cloneOneMethod;
        [NotNull]private readonly MethodInfo _cloneManyMethod;

        /// <summary>
        /// Создаёт глубокие копии объектов
        /// </summary>
        public ReflectionObjectClonemaker(
            [NotNull]IFlatPathParser pathMapper,
            [NotNull]IMemberInfoHelper memberInfoHelper)
        {
            _pathMapper = pathMapper;

            // ReSharper disable RedundantTypeArgumentsOfMethod
            _cloneOneMethod = memberInfoHelper.GetGenericDefinitionMethodInfo(() => Clone<object>(new object(), null, null, 0));
            _cloneManyMethod = memberInfoHelper.GetGenericDefinitionMethodInfo(() => Clone<object>(new object[0], null, null, 0));
            // ReSharper restore RedundantTypeArgumentsOfMethod
        }

        private T Clone<T>(
            T obj,
            [NotNull]ClonedPropertyManager manager,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths,
            int index)
            where T : class, new()
        {
            if (obj == null)
            {
                return null;
            }
            var result = new T();
            var properties = ValueHoldingMember.GetValueHoldingMembers(typeof(T));
            var pathStarts = paths.Where(p => p != null && p.Count > index).GroupBy(p => p[index]);
            foreach (var property in properties.Where(p => manager.Copy(p)))
            {
                if (property == null)
                {
                    continue;
                }
                property.SetValue(result, property.GetValue(obj));
            }
            foreach (var pathStart in pathStarts)
            {
                if (pathStart == null)
                {
                    continue;
                }
                var current = pathStart.Key;
                if (current == null)
                {
                    continue;
                }
                if (manager.SingleClone(current))
                {
                    var childClone = _cloneOneMethod.MakeGenericMethod(current.ValueType)
                        .Invoke(this, new[] { current.GetValue(obj), manager, pathStart, index + 1 });
                    current.SetValue(result, childClone);
                }
                else if (manager.CollectionClone(current))
                {
                    var childrenClones = _cloneManyMethod.MakeGenericMethod(current.ValueType.GetGenericEnumerableArgument())
                        .Invoke(this, new[] { current.GetValue(obj), manager, pathStart, index + 1 });
                    if (childrenClones != null)
                    {
                        current.SetValue(result, childrenClones);
                    }
                }
            }
            return result;
        }

        [ContractAnnotation("objects:notnull => notnull")]
        private IReadOnlyCollection<T> Clone<T>(
            IEnumerable<T> objects,
            [NotNull]ClonedPropertyManager manager,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths,
            int index)
            where T : class, new()
        {
            return objects != null ? objects.Select(obj => Clone(obj, manager, paths, index)).ToList() : null;
        }

        /// <summary>
        /// Создать глубокую копию объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="obj">Объект</param>
        /// <param name="paths">Пути, по которым осуществляется глубокое копирование</param>
        /// <param name="manager">Настройки копирования</param>
        /// <returns>Копия</returns>
        public T Clone<T>(T obj, IEnumerable<Expression<Func<T, object>>> paths, ClonedPropertyManager manager)
            where T : class, new()
        {
            return Clone(obj, manager, paths.Select(_pathMapper.Parse), 0);
        }

        /// <summary>
        /// Создать глубокую копию объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="obj">Объект</param>
        /// <param name="paths">Пути, по которым осуществляется глубокое копирование</param>
        /// <param name="manager">Настройки копирования</param>
        /// <returns>Копия</returns>
        public T Clone<T>(T obj, IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, ClonedPropertyManager manager)
            where T : class, new()
        {
            return Clone(obj, manager, paths, 0);
        }

        /// <summary>
        /// Создать глубокую копию объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="objects">Объект</param>
        /// <param name="paths">Пути, по которым осуществляется глубокое копирование</param>
        /// <param name="manager">Настройки копирования</param>
        /// <returns>Копия</returns>
        public IReadOnlyCollection<T> Clone<T>(IEnumerable<T> objects, IEnumerable<Expression<Func<T, object>>> paths, ClonedPropertyManager manager)
            where T : class, new()
        {
            return Clone(objects, manager, paths.Select(_pathMapper.Parse), 0);
        }

        /// <summary>
        /// Создать глубокую копию объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="objects">Объект</param>
        /// <param name="paths">Пути, по которым осуществляется глубокое копирование</param>
        /// <param name="manager">Настройки копирования</param>
        /// <returns>Копия</returns>
        public IReadOnlyCollection<T> Clone<T>(IEnumerable<T> objects, IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, ClonedPropertyManager manager)
            where T : class, new()
        {
            return Clone(objects, manager, paths, 0);
        }
    }
}