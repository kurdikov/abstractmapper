﻿namespace ObjectClonemakerBenchmarks
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Reflection;

    public class BadCodegenObjectClonemaker
    {
        [NotNull]
        private readonly IFlatPathParser _pathMapper;
        [NotNull]
        private readonly IMemberInfoLibrary _memberInfoLibrary;

        [NotNull]
        private readonly MethodInfo _cloneOneMethod;
        [NotNull]
        private readonly MethodInfo _cloneManyMethod;
        [NotNull]
        private readonly PropertyInfo _readonlylistValueHoldingMemberCount;
        [NotNull]
        private readonly MethodInfo _propertyReadonlyListIndexer;
        [NotNull]
        private readonly MethodInfo _propertyLookupIndexer;

        [NotNull]
        private readonly ConstantExpression _this;

        [NotNull]
        private readonly ConcurrentDictionary<KeyValuePair<Type, ClonedPropertyManager>, MulticastDelegate> _copyDelegateCache =
            new ConcurrentDictionary<KeyValuePair<Type, ClonedPropertyManager>, MulticastDelegate>();

        public BadCodegenObjectClonemaker(
            [NotNull]IFlatPathParser pathMapper,
            [NotNull]IMemberInfoHelper memberInfoHelper,
            [NotNull]IMemberInfoLibrary memberInfoLibrary)
        {
            _pathMapper = pathMapper;
            _memberInfoLibrary = memberInfoLibrary;

            // ReSharper disable RedundantTypeArgumentsOfMethod
            _cloneOneMethod = memberInfoHelper.GetGenericDefinitionMethodInfo(() => Clone<object>(new object(), null, null, 0));
            _cloneManyMethod = memberInfoHelper.GetGenericDefinitionMethodInfo(() => Clone<object>(new object[0], null, null, 0));
            _readonlylistValueHoldingMemberCount = memberInfoHelper.GetPropertyInfo((IReadOnlyList<ValueHoldingMember> l) => l.Count);
            _propertyReadonlyListIndexer = memberInfoHelper.GetMethodInfo((IReadOnlyList<ValueHoldingMember> l) => l[0]);
            _propertyLookupIndexer = memberInfoHelper.GetMethodInfo((ILookup<string, IReadOnlyList<ValueHoldingMember>> l) => l[null]);
            // ReSharper restore RedundantTypeArgumentsOfMethod

            _this = Expression.Constant(this);
        }

        [NotNull]
        private Expression<Func<T, IEnumerable<IReadOnlyList<ValueHoldingMember>>, int, T>> MakeCloneExpression<T>([NotNull]ClonedPropertyManager manager)
            where T : class, new()
        {
            var expressions = new List<Expression>();
            var properties = ValueHoldingMember.GetValueHoldingMembers(typeof(T));
            var source = Expression.Parameter(typeof(T), "source");
            var paths = Expression.Parameter(typeof(IEnumerable<IReadOnlyList<ValueHoldingMember>>), "paths");
            var index = Expression.Parameter(typeof(int), "index");
            var resultObj = Expression.Variable(typeof(T), "result");
            expressions.Add(Expression.Assign(resultObj, Expression.New(typeof(T))));
            var listParam = Expression.Parameter(typeof(IReadOnlyList<ValueHoldingMember>));
            var where = Expression.Lambda<Func<IReadOnlyList<ValueHoldingMember>, bool>>(
                Expression.GreaterThan(
                    Expression.Property(listParam, _readonlylistValueHoldingMemberCount),
                    index),
                listParam);
            var select = Expression.Lambda<Func<IReadOnlyList<ValueHoldingMember>, string>>(
                Expression.Call(
                    Expression.Call(
                        listParam,
                        _propertyReadonlyListIndexer,
                        new Expression[] { index }),
                    _memberInfoLibrary.ObjectToString()),
                listParam);
            var lookup = Expression.Call(
                _memberInfoLibrary.EnumerableToLookup(typeof(IReadOnlyList<ValueHoldingMember>), typeof(string)),
                Expression.Call(
                    _memberInfoLibrary.EnumerableWhere(typeof(IReadOnlyList<ValueHoldingMember>)),
                    paths,
                    where),
                select);
            var lookupVariable = Expression.Variable(typeof(ILookup<string, IReadOnlyList<ValueHoldingMember>>), "pathLookup");
            var pathsThroughVariable = Expression.Variable(typeof(IEnumerable<IReadOnlyList<ValueHoldingMember>>), "pathsThrough");
            expressions.Add(Expression.Assign(lookupVariable, lookup));
            foreach (var property in properties)
            {
                if (property == null)
                {
                    continue;
                }
                MethodInfo deepMethod = null;
                if (manager.Copy(property))
                {
                    expressions.Add(Expression.Assign(
                        Expression.MakeMemberAccess(resultObj, property.Member),
                        Expression.MakeMemberAccess(source, property.Member)));
                }
                else if (manager.SingleClone(property))
                {
                    deepMethod = _cloneOneMethod.MakeGenericMethod(property.ValueType);

                }
                else if (manager.CollectionClone(property))
                {
                    deepMethod = _cloneManyMethod.MakeGenericMethod(
                        property.ValueType.GetGenericEnumerableArgument());
                }
                if (deepMethod != null)
                {
                    expressions.Add(Expression.Assign(
                        pathsThroughVariable,
                        Expression.Call(
                            lookupVariable,
                            _propertyLookupIndexer,
                            new Expression[] { Expression.Constant(property.Name, typeof(string)) })));
                    expressions.Add(Expression.IfThen(
                        Expression.AndAlso(
                            Expression.NotEqual(
                                pathsThroughVariable,
                                Expression.Constant(null)),
                            Expression.Call(
                                _memberInfoLibrary.EnumerableAny(typeof(IReadOnlyList<ValueHoldingMember>)),
                                pathsThroughVariable)),
                        Expression.Assign(
                            Expression.MakeMemberAccess(resultObj, property.Member),
                            Expression.Call(
                                _this,
                                deepMethod,
                                Expression.MakeMemberAccess(source, property.Member),
                                Expression.Constant(manager),
                                pathsThroughVariable,
                                Expression.Add(index, Expression.Constant(1))))));
                }
            }
            expressions.Add(resultObj);
            var block = Expression.Block(new[] { resultObj, lookupVariable, pathsThroughVariable }, expressions);
            var checkNullThenClone = Expression.Condition(
                Expression.NotEqual(source, Expression.Constant(null)),
                block,
                Expression.Constant(null, typeof(T)));
            return Expression.Lambda<Func<T, IEnumerable<IReadOnlyList<ValueHoldingMember>>, int, T>>(
                checkNullThenClone,
                source, paths, index);
        }

        private Func<T, IEnumerable<IReadOnlyList<ValueHoldingMember>>, int, T> MakeCloneFunc<T>(
            [NotNull] ClonedPropertyManager manager)
            where T : class, new()
        {
            return MakeCloneExpression<T>(manager).Compile();
        }

        [NotNull]
        private Func<T, IEnumerable<IReadOnlyList<ValueHoldingMember>>, int, T> GetCloneFunc<T>([NotNull]ClonedPropertyManager manager)
            where T : class, new()
        {
            var result = _copyDelegateCache.GetOrAdd(new KeyValuePair<Type, ClonedPropertyManager>(typeof(T), manager), a => MakeCloneFunc<T>(manager))
                as Func<T, IEnumerable<IReadOnlyList<ValueHoldingMember>>, int, T>;
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Copy delegate cache contains null for {0}", typeof(T)));
            }
            return result;
        }

        private T Clone<T>(
            T source,
            [NotNull]ClonedPropertyManager manager,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths,
            int index)
            where T : class, new()
        {
            var func = GetCloneFunc<T>(manager);
            return func.Invoke(source, paths, index);
        }

        [ContractAnnotation("objects:notnull => notnull")]
        private List<T> Clone<T>(
            IEnumerable<T> objects,
            [NotNull]ClonedPropertyManager manager,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths,
            int index)
            where T : class, new()
        {
            return objects != null ? objects.Select(obj => Clone(obj, manager, paths, index)).ToList() : null;
        }

        public T Clone<T>(T obj, IEnumerable<Expression<Func<T, object>>> paths, ClonedPropertyManager manager)
            where T : class, new()
        {
            return Clone(obj, manager, paths.Select(_pathMapper.Parse), 0);
        }

        public T Clone<T>(T obj, IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, ClonedPropertyManager manager)
            where T : class, new()
        {
            return Clone(obj, manager, paths, 0);
        }

        public IReadOnlyCollection<T> Clone<T>(IEnumerable<T> objects, IEnumerable<Expression<Func<T, object>>> paths, ClonedPropertyManager manager)
            where T : class, new()
        {
            return Clone(objects, manager, paths.Select(_pathMapper.Parse), 0);
        }

        public IReadOnlyCollection<T> Clone<T>(IEnumerable<T> objects, IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, ClonedPropertyManager manager)
            where T : class, new()
        {
            return Clone(objects, manager, paths, 0);
        }
    }
}