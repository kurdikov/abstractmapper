﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using BenchmarkDotNet.Running;
using JetBrains.Annotations;
using TsSoft.Expressions.Helpers;
using TsSoft.Expressions.Helpers.Reflection;
using TsSoft.Expressions.Models.Reflection;

namespace ObjectClonemakerBenchmarks
{
    class Program
    {
        private static From[] GetFroms([NotNull] Stopwatch sw)
        {
            sw.Restart();
            var froms = Enumerable.Range(0, 1000).Select(
                i => new From
                {
                    Byte = (byte)(i % byte.MaxValue + 1),
                    Short = (short)(i % byte.MaxValue + 1),
                    Int = i,
                    Long = i,
                    Decimal = i,
                    Double = i,
                    Kv = new KeyValuePair<int, int>(i, i + 1),
                    String = i.ToString(CultureInfo.InvariantCulture),
                    Children = Enumerable.Range(0, 1000).Select(
                        j => new From
                        {
                            Byte = (byte)(j % byte.MaxValue + 1),
                            Short = (short)(j % byte.MaxValue + 1),
                            Int = j,
                            Long = j,
                            Decimal = j,
                            Double = j,
                            Kv = new KeyValuePair<int, int>(j, j - 1),
                            String = j.ToString(CultureInfo.InvariantCulture),
                        }).ToList(),
                }).ToArray();
            Console.WriteLine("Created froms: {0}", sw.Elapsed);
            return froms;
        }

        private static void RunClonemakers(From[] froms, [NotNull] Stopwatch sw)
        {
            var cloneableTypes = new[] { typeof(From) };
            var cloneableCollectionTypes = new[]
            {
                typeof(ICollection<From>),
            };

            sw.Restart();
            ClonedPropertyManager manager = new ClonedPropertyManager
            (
                copy: pi => pi.ValueType.IsValueType || pi.ValueType == typeof(string),
                singleClone: pi => cloneableTypes.Contains(pi.ValueType),
                collectionClone: pi => cloneableCollectionTypes.Contains(pi.ValueType)
            );
            var fmParser = new FlatPathParser();
            var includes = new Expression<Func<T1, object>>[]
            {
                p => p.ChildrenWithChildren.Select(pe => pe.Children),
                p => p.ChildrenWithSingleChild.Select(idp => idp.Child)
            }
            .Select(fmParser.Parse)
            .ToArray();
            var fromIncludes = new Expression<Func<From, object>>[]
            {
                f => f.Children,
            }
            .Select(fmParser.Parse)
            .ToArray();
            var mi = new MemberInfoHelper();
            var ml = new MemberInfoLibrary(mi);
            var legacyClonemaker = new ReflectionObjectClonemaker(fmParser, mi);
            var clonemaker = new ObjectClonemaker(fmParser, mi, ml);
            var badClonemaker = new BadCodegenObjectClonemaker(fmParser, mi, ml);
            Console.WriteLine("Created clonemakers: {0}", sw.Elapsed);

            sw.Restart();
            legacyClonemaker.Clone(froms[0], includes, manager);
            clonemaker.Clone(froms[0], includes, manager);
            badClonemaker.Clone(froms[0], includes, manager);
            Console.WriteLine("Cloned first elements: {0}", sw.Elapsed);

            const int count = 10000;

            sw.Restart();
            for (int i = 0; i < count; ++i)
            {
                legacyClonemaker.Clone(froms[0], fromIncludes, manager);
            }
            Console.WriteLine("Reflection: {0}", sw.Elapsed);

            sw.Restart();
            for (int i = 0; i < count; ++i)
            {
                badClonemaker.Clone(froms[0], fromIncludes, manager);
            }
            Console.WriteLine("Autocompiled: {0}", sw.Elapsed);

            sw.Restart();
            for (int i = 0; i < count; ++i)
            {
                clonemaker.Clone(froms[0], fromIncludes, manager);
            }
            Console.WriteLine("Specially compiled: {0}", sw.Elapsed);
        }


        static void Main(string[] args)
        {
            if (args != null && args.Length == 1 && args[0] == "manual")
            {
                var sw = new Stopwatch();
                var froms = GetFroms(sw);
                RunClonemakers(froms, sw);
            }
            else
            {
                var runner = BenchmarkSwitcher.FromTypes(new Type[]
                {
                    typeof(ObjectClonemakers),
                });
                runner.Run(args);
            }

            Console.ReadLine();
            return;
        }
    }
}
