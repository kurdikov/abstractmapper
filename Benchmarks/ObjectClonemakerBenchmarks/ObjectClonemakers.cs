namespace ObjectClonemakerBenchmarks
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using BenchmarkDotNet.Attributes;
    using BenchmarkDotNet.Attributes.Jobs;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Reflection;

    [LegacyJitX86Job, LegacyJitX64Job, RyuJitX64Job]
    public class ObjectClonemakers
    {
        private ReflectionObjectClonemaker _reflection;
        private BadCodegenObjectClonemaker _bad;
        private ObjectClonemaker _better;

        private T1 _entity;
        private From _from;
        private ClonedPropertyManager _manager;
        private IReadOnlyList<ValueHoldingMember>[] _includes;

        public ObjectClonemakers()
        {
            _entity = new T1
            {
                Id = 1,
                Bool = true,
                Date = new DateTime(1993, 10, 4),
                Guid = Guid.Parse("33333333-3333-3333-3333-333333333333"),
                String = "����",
                ChildrenWithChildren = new[]
                {
                    new T2
                    {
                        Id = 2,
                        Children = new[]
                        {
                            new T3
                            {
                                Id = Guid.Parse("22222222-2222-2222-2222-222222222222"),
                                Enum = E1.One,
                                Child = new T4
                                {
                                    Id = 3,
                                }
                            }
                        }
                    },
                    new T2
                    {
                        Id = 3,
                    }
                },
                ChildrenWithSingleChild = new[]
                {
                    new T5
                    {
                        Id = Guid.Parse("11111111-1111-1111-1111-111111111111"),
                        Enum = E1.Three,
                        Child = new T4
                        {
                            Id = 2,
                        }
                    },
                    new T5
                    {
                        Id = Guid.Parse("44444444-4444-4444-4444-444444444444"),
                        Enum = E1.Three,
                    },
                },
                NotInterestingChildren = new[]
                {
                    new T6(),
                }
            };
            int i = 0;
            _from = new From
            {
                Byte = (byte)(i % byte.MaxValue + 1),
                Short = (short)(i % byte.MaxValue + 1),
                Int = i,
                Long = i,
                Decimal = i,
                Double = i,
                Kv = new KeyValuePair<int, int>(i, i + 1),
                String = i.ToString(CultureInfo.InvariantCulture),
                Children = Enumerable.Range(0, 1000).Select(
                    j => new From
                    {
                        Byte = (byte)(j % byte.MaxValue + 1),
                        Short = (short)(j % byte.MaxValue + 1),
                        Int = j,
                        Long = j,
                        Decimal = j,
                        Double = j,
                        Kv = new KeyValuePair<int, int>(j, j - 1),
                        String = j.ToString(CultureInfo.InvariantCulture),
                    }).ToList(),
            };
            var cloneableTypes = new[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(From), };
            var cloneableCollectionTypes = new[]
            {
                typeof(ICollection<T1>),
                typeof(ICollection<T2>),
                typeof(ICollection<T3>),
                typeof(ICollection<T4>),
                typeof(ICollection<T5>),
                typeof(ICollection<T6>),
                typeof(ICollection<From>),
            };
            _manager = new ClonedPropertyManager
            (
                copy: pi => pi.PropertyType.IsValueType || pi.PropertyType == typeof(string),
                singleClone: pi => cloneableTypes.Contains(pi.PropertyType),
                collectionClone: pi => cloneableCollectionTypes.Contains(pi.PropertyType)
            );

            var fpp = new FlatPathParser();
            _includes = new Expression<Func<T1, object>>[]
            {
                p => p.ChildrenWithChildren.Select(pe => pe.Children),
                p => p.ChildrenWithSingleChild.Select(idp => idp.Child)
            }
            .Select(fpp.Parse)
            .ToArray();
            var mi = new MemberInfoHelper();
            var ml = new MemberInfoLibrary(mi);
            _bad = new BadCodegenObjectClonemaker(fpp, mi, ml);
            _better = new ObjectClonemaker(fpp, mi, ml);
            _reflection = new ReflectionObjectClonemaker(fpp, mi);
        }

        [Benchmark]
        public From Bad()
        {
            return _bad.Clone(_from, _includes, _manager);
        }

        [Benchmark]
        public From Better()
        {
            return _better.Clone(_from, _includes, _manager);
        }

        [Benchmark]
        public From Reflection()
        {
            return _reflection.Clone(_from, _includes, _manager);
        }
    }
}
