﻿namespace ObjectClonemakerBenchmarks
{
    using System;
    using System.Collections.Generic;


    public class T1
    {
        public int Id { get; set; }
        public bool Bool { get; set; }
        public DateTime Date { get; set; }
        public Guid Guid { get; set; }
        public string String { get; set; }
        public ICollection<T2> ChildrenWithChildren { get; set; }
        public ICollection<T5> ChildrenWithSingleChild { get; set; }
        public ICollection<T6> NotInterestingChildren { get; set; }
    }
    public class T2
    {
        public int Id { get; set; }
        public ICollection<T3> Children { get; set; }
    }
    public class T3
    {
        public Guid Id { get; set; }
        public E1 Enum { get; set; }
        public T4 Child { get; set; }
    }
    public enum E1
    {
        One,
        Two,
        Three
    }
    public class T4
    {
        public int Id { get; set; }
    }
    public class T5
    {
        public Guid Id { get; set; }
        public E1 Enum { get; set; }
        public T4 Child { get; set; }
    }
    public class T6
    {
    }
}
