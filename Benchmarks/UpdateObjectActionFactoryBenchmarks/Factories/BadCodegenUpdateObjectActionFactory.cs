﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using TsSoft.Expressions.Helpers;
using TsSoft.Expressions.Helpers.Reflection;
using TsSoft.Expressions.Models;
using TsSoft.Expressions.Models.Reflection;

namespace UpdateObjectActionFactoryBenchmarks
{
    class BadCodegenUpdateObjectActionFactory
    {
        [NotNull]private readonly IFlatPathParser _pathMapper;
        [NotNull]private readonly IExceptionWrapperHelper _exceptionWrapperHelper;
        [NotNull]private readonly IMemberInfoHelper _memberInfoHelper;
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;

        [NotNull]private readonly MethodInfo _makeUpdateCollectionExpressionBody;
        [NotNull]private readonly MethodInfo _makeRecreateCollectionExpressionBody;
        [NotNull]private readonly MethodInfo _makeUpdateObjectExpressionBody;
        [NotNull]private readonly MethodInfo _listFuncTaskAdd;
        [NotNull]private readonly MethodInfo _getSingleOrDefault;
        [NotNull]private readonly MethodInfo _makeFuncWithoutParam;

        private readonly ConstantExpression _this;

        [NotNull]
        public Func<Expression, Expression> Wrapper { get; set; }

        public BadCodegenUpdateObjectActionFactory(
            [NotNull] IFlatPathParser pathMapper,
            [NotNull] IExceptionWrapperHelper exceptionWrapperHelper,
            [NotNull] IMemberInfoHelper memberInfoHelper,
            [NotNull] IExpressionBuilder expressionBuilder)
        {
            _pathMapper = pathMapper;
            _exceptionWrapperHelper = exceptionWrapperHelper;
            _memberInfoHelper = memberInfoHelper;
            _expressionBuilder = expressionBuilder;

            _makeUpdateCollectionExpressionBody = memberInfoHelper.GetGenericDefinitionMethodInfo(
                () => MakeUpdateCollectionExpressionBody<object>(null, null, 0, null, null, null, null));
            _makeRecreateCollectionExpressionBody = memberInfoHelper.GetGenericDefinitionMethodInfo(
                () => MakeRecreateCollectionExpressionBody<object>(null, null, 0, null, null, null));
            _makeUpdateObjectExpressionBody = memberInfoHelper.GetGenericDefinitionMethodInfo(
                () => MakeUpdateExpressionBody<object>(null, null, 0, null, null, null, null));
            _listFuncTaskAdd = memberInfoHelper.GetMethodInfo(
                (List<Func<Task>> l) => l.Add(null));
            _getSingleOrDefault = memberInfoHelper.GetGenericDefinitionMethodInfo(
                () => GetSingleOrDefault<object, string>(null, null, null));
            _makeFuncWithoutParam = memberInfoHelper.GetGenericDefinitionMethodInfo(
                () => MakeFuncWithoutParams<object, object>(null, null, null));

            Wrapper = e => _exceptionWrapperHelper.Wrap(e, message: "An error has occurred in an action prepared by UpdateObjectActionFactory.");

            _this = Expression.Constant(this);
        }

        public Action<T, T> MakeUpdateAction<T>(
            IEnumerable<Expression<Func<T, object>>> paths,
            IObjectUpdateManager manager,
            IEnumerable<Expression<Func<T, object>>> dropCreateOnPaths = null)
        {
            var expression = MakeUpdateExpression<T>(
                paths.Select(_pathMapper.Parse).ToList(),
                (dropCreateOnPaths ?? Enumerable.Empty<Expression<Func<T, object>>>()).Select(_pathMapper.Parse).ToList(),
                manager);
            return expression.Compile();
        }

        public Func<T, T, IEnumerable<Func<Task>>> MakeAsyncUpdateAction<T>(
            IEnumerable<Expression<Func<T, object>>> paths,
            IObjectUpdateManager manager,
            IEnumerable<Expression<Func<T, object>>> dropCreateOnPaths = null)
        {
            var expression = MakeAsyncUpdateExpression<T>(
                paths.Select(_pathMapper.Parse).ToList(),
                (dropCreateOnPaths ?? Enumerable.Empty<Expression<Func<T, object>>>()).Select(_pathMapper.Parse).ToList(),
                manager);
            return expression.Compile();
        }

        [NotNull]
        private Expression<Action<T, T>> MakeUpdateExpression<T>(
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> dropCreateOnPaths,
            [NotNull]IObjectUpdateManager manager)
        {
            var source = Expression.Parameter(typeof(T), "source");
            var target = Expression.Parameter(typeof(T), "target");
            var context = new UpdateContext(source, target, false);
            var body = MakeUpdateExpressionBody<T>(paths, dropCreateOnPaths, 0, source, target, manager, context);
            var lambda = Expression.Lambda<Action<T, T>>(body, new[] { source, target });
            return lambda;
        }

        [NotNull]
        private Expression<Func<T, T, IEnumerable<Func<Task>>>> MakeAsyncUpdateExpression<T>(
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> dropCreateOnPaths,
            [NotNull]IObjectUpdateManager manager)
        {
            var source = Expression.Parameter(typeof(T), "source");
            var target = Expression.Parameter(typeof(T), "target");
            var context = new UpdateContext(source, target, true);
            var body = MakeUpdateExpressionBody<T>(paths, dropCreateOnPaths, 0, source, target, manager, context);
            var wrapper = Expression.Block(
                new[] { context.TaskList },
                Expression.Assign(context.TaskList, Expression.New(typeof(List<Func<Task>>))),
                body,
                context.TaskList);
            var lambda = Expression.Lambda<Func<T, T, IEnumerable<Func<Task>>>>(wrapper, new[] { source, target });
            return lambda;
        }

        [NotNull]
        private Expression MakeUpdateExpressionBody<T>(
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> dropCreate,
            int start,
            [NotNull]Expression source,
            [NotNull]Expression target,
            [NotNull]IObjectUpdateManager manager,
            [NotNull]UpdateContext context)
        {
            var statements = new List<Expression>();
            var pathStarts = paths.GroupBy(p => p == null || p.Count <= start ? null : p[start]);
            var dropCreateStarts = dropCreate.ToLookup(p => p == null || p.Count <= start ? null : p[start]);
            foreach (var pathStart in pathStarts)
            {
                if (pathStart == null)
                {
                    continue;
                }
                if (pathStart.Key == null)
                {
                    statements.Add(Wrapper(FullPrimitiveUpdate<T>(source, target, manager)));
                }
                else if (manager.IsPrimitive(pathStart.Key.ValueType))
                {
                    statements.Add(Wrapper(UpdatePrimitiveProperty(pathStart.Key, source, target)));
                }
                else if (manager.IsCollection(pathStart.Key.ValueType))
                {
                    var entityType = pathStart.Key.ValueType.GetGenericEnumerableArgument();
                    if (!dropCreateStarts[pathStart.Key].Any(s => s != null && s.Count == start + 1))
                    {
                        statements.Add(Wrapper(
                            Expression.Assign(
                                Expression.MakeMemberAccess(target, pathStart.Key.Member),
                                MakeUpdateCollectionExpressionBody(
                                    entityType,
                                    pathStart,
                                    dropCreateStarts[pathStart.Key],
                                    start + 1,
                                    Expression.MakeMemberAccess(source, pathStart.Key.Member),
                                    Expression.MakeMemberAccess(target, pathStart.Key.Member),
                                    manager,
                                    context))));
                    }
                    else
                    {
                        if (!context.Async)
                        {
                            statements.Add(Wrapper(manager.MakeRemoveCollectionExpression(
                                pathStart.Key,
                                entityType,
                                target)));
                        }
                        else
                        {
                            statements.Add(AddTask(context, manager.MakeAsyncRemoveCollectionExpression(
                                pathStart.Key,
                                entityType,
                                target)));
                        }
                        statements.Add(Wrapper(
                            Expression.Assign(
                                Expression.MakeMemberAccess(target, pathStart.Key.ValueType),
                                MakeRecreateCollectionExpressionBody(
                                    entityType,
                                    pathStart,
                                    dropCreateStarts[pathStart.Key],
                                    start + 1,
                                    Expression.MakeMemberAccess(source, pathStart.Key.ValueType),
                                    manager,
                                    context))));
                    }
                }
                else
                {
                    statements.Add(Wrapper(
                        Expression.Assign(
                            Expression.MakeMemberAccess(target, pathStart.Key.Member),
                            Expression.Condition(
                                Expression.NotEqual(
                                    Expression.MakeMemberAccess(source, pathStart.Key.Member),
                                    Expression.Constant(null, pathStart.Key.ValueType)),
                                Expression.Coalesce(
                                    Expression.MakeMemberAccess(target, pathStart.Key.Member),
                                    Expression.New(pathStart.Key.ValueType)),
                                Expression.Constant(null, pathStart.Key.ValueType)))));
                    statements.Add(Wrapper(
                        MakeUpdateObjectExpressionBody(
                            pathStart.Key.ValueType,
                            pathStart,
                            dropCreateStarts[pathStart.Key],
                            start + 1,
                            Expression.MakeMemberAccess(source, pathStart.Key.Member),
                            Expression.MakeMemberAccess(target, pathStart.Key.Member),
                            manager,
                            context)));
                }
            }
            if (!statements.Any())
            {
                statements.Add(Expression.Empty());
            }
            var block = Expression.Block(statements);
            var body = Expression.IfThen(Expression.NotEqual(source, Expression.Constant(null)), block);
            return body;
        }

        [NotNull]
        private Expression FullPrimitiveUpdate<T>([NotNull]Expression source, [NotNull]Expression target, [NotNull]IObjectUpdateManager manager)
        {
            var setProperties = new List<Expression>();
            var propertyInfos = ValueHoldingMember.GetValueHoldingMembers(typeof(T)).Where(x => x != null && manager.IsPrimitive(x.ValueType));
            foreach (var pi in propertyInfos)
            {
                if (pi == null)
                {
                    continue;
                }
                setProperties.Add(UpdatePrimitiveProperty(pi, source, target));
            }
            var block = Expression.Block(setProperties);
            return block;
        }

        [NotNull]
        private Expression UpdatePrimitiveProperty([NotNull]ValueHoldingMember pi, [NotNull]Expression source, [NotNull]Expression target)
        {
            if (!pi.IsReadable || !pi.IsWriteable)
            {
                return Expression.Empty();
            }
            var getExpression = Expression.MakeMemberAccess(source, pi.Member);
            var setExpression = Expression.Assign(Expression.MakeMemberAccess(target, pi.Member), getExpression);
            return setExpression;
        }

        private void ValidateCopiedElementType([NotNull]Type objectType, [NotNull]IObjectUpdateManager manager)
        {
            if (!manager.IsObject(objectType))
            {
                throw new InvalidOperationException(string.Format("Can not build copy action: {0} is not an object type", objectType.FullName));
            }
        }

        [NotNull]
        private Expression MakeUpdateCollectionExpressionBody(
            [NotNull]Type elementType,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> dropCreateOnPaths,
            int start,
            [NotNull]Expression source,
            [NotNull]Expression target,
            [NotNull]IObjectUpdateManager manager,
            UpdateContext context)
        {
            ValidateCopiedElementType(elementType, manager);
            var method = _makeUpdateCollectionExpressionBody.MakeGenericMethod(elementType);
            var action = method.Invoke(this, new object[] { paths, dropCreateOnPaths, start, source, target, manager, context }) as Expression;
            if (action == null)
            {
                throw new InvalidOperationException("ICopyActionFactory::MakeCopyCollectionActionBody returned null");
            }
            return action;
        }

        [NotNull]
        private Expression MakeUpdateCollectionExpressionBody<T>(
            [NotNull] IEnumerable<IReadOnlyList<ValueHoldingMember>> paths,
            [NotNull] IEnumerable<IReadOnlyList<ValueHoldingMember>> dropCreateOnPaths,
            int start,
            [NotNull] Expression source,
            [NotNull] Expression target,
            [NotNull] IObjectUpdateManager manager,
            [NotNull]UpdateContext context)
        {
            var listAdd = _memberInfoHelper.GetMethodInfo((List<T> list) => list.Add(default(T)));

            var statements = new List<Expression>();
            var result = Expression.Variable(typeof(List<T>), "result");
            var variables = new List<ParameterExpression>(2) { result };
            statements.Add(Expression.Assign(result, Expression.New(typeof(List<T>))));
            var sourceElement = Expression.Variable(typeof(T), "sourceElement");
            var sourceElementExtractedKey = manager.MakeExtractKeyExpression(sourceElement);
            var sourceKey = Expression.Variable(sourceElementExtractedKey.Type, "sourceKey");
            var targetElement = Expression.Variable(typeof(T), "targetElement");
            var selectLambdaParam = Expression.Parameter(typeof(T));
            var foreachBody = Expression.Block(new[] { sourceKey, targetElement, },
                Expression.Assign(
                    sourceKey,
                    sourceElementExtractedKey),
                Expression.Assign(
                    targetElement,
                    Expression.Coalesce(
                        Expression.Call(
                            _getSingleOrDefault.MakeGenericMethod(typeof(T), sourceElementExtractedKey.Type),
                            target,
                            sourceKey,
                            Expression.Constant(
                                Expression.Lambda(
                                    typeof(Func<,>).MakeGenericType(typeof(T), sourceElementExtractedKey.Type),
                                    manager.MakeExtractKeyExpression(selectLambdaParam),
                                    selectLambdaParam).Compile())),
                        Expression.New(typeof(T)))),
                MakeUpdateExpressionBody<T>(paths, dropCreateOnPaths, start, sourceElement, targetElement, manager, context),
                Expression.Call(
                    result,
                    listAdd,
                    new Expression[] { targetElement }));
            statements.Add(_expressionBuilder.Foreach<T>(source, sourceElement, foreachBody));
            if (!context.Async)
            {
                statements.Add(manager.MakeRemoveCollectionPartExpression(source, target));
            }
            else
            {
                // it is essential that this assignment is outside of the async task
                var targetVar = Expression.Variable(target.Type, "target");
                variables.Add(targetVar);
                statements.Add(Expression.Assign(targetVar, target));
                statements.Add(AddTask(context, manager.MakeAsyncRemoveCollectionPartExpression(source, targetVar)));
            }
            statements.Add(result);
            var block = Expression.Block(variables, statements);
            return block;
        }

        private static T GetSingleOrDefault<T, TKey>([NotNull]IEnumerable<T> collection, TKey sourceKey, [NotNull]Func<T, TKey> keyExtractor)
            where TKey : IEquatable<TKey>
        {
            if (sourceKey == null)
            {
                return default(T);
            }
            return collection.SingleOrDefault(elem => sourceKey.Equals(keyExtractor(elem)));
        }

        [NotNull]
        private Expression MakeRecreateCollectionExpressionBody(
            [NotNull] Type elementType,
            [NotNull] IEnumerable<IReadOnlyList<ValueHoldingMember>> paths,
            [NotNull] IEnumerable<IReadOnlyList<ValueHoldingMember>> dropCreateOnPaths,
            int start,
            [NotNull] Expression source,
            [NotNull]IObjectUpdateManager manager,
            [NotNull]UpdateContext context)
        {
            ValidateCopiedElementType(elementType, manager);
            var method = _makeRecreateCollectionExpressionBody.MakeGenericMethod(elementType);
            var action = method.Invoke(this, new object[] { paths, dropCreateOnPaths, start, source, manager, context }) as Expression;
            if (action == null)
            {
                throw new InvalidOperationException("ICopyActionFactory::MakeSimpleCollectionCopyActionBody returned null");
            }
            return action;
        }

        [NotNull]
        private Expression MakeRecreateCollectionExpressionBody<T>(
            [NotNull] IEnumerable<IReadOnlyList<ValueHoldingMember>> paths,
            [NotNull] IEnumerable<IReadOnlyList<ValueHoldingMember>> dropCreateOnPaths,
            int start,
            Expression source,
            [NotNull]IObjectUpdateManager manager,
            [NotNull]UpdateContext context)
        {
            var listAdd = _memberInfoHelper.GetMethodInfo((List<T> list) => list.Add(default(T)));
            var statements = new List<Expression>();
            var result = Expression.Variable(typeof(List<T>), "result");
            statements.Add(Expression.Assign(result, Expression.New(typeof(List<T>))));
            var sourceElement = Expression.Variable(typeof(T), "sourceElement");
            var targetElement = Expression.Variable(typeof(T), "targetElement");
            var foreachBody = Expression.Block(new[] { targetElement },
                Expression.Assign(
                    targetElement,
                    Expression.New(typeof(T))),
                MakeUpdateExpressionBody<T>(paths, dropCreateOnPaths, start, sourceElement, targetElement, manager, context),
                Expression.Call(
                    result,
                    listAdd,
                    new Expression[] { targetElement }));
            statements.Add(_expressionBuilder.Foreach<T>(source, sourceElement, foreachBody));
            statements.Add(result);
            var block = Expression.Block(new[] { result }, statements);
            return block;
        }

        [NotNull]
        private Expression MakeUpdateObjectExpressionBody(
            [NotNull]Type objectType,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> dropCreateOnPaths,
            int start,
            [NotNull]Expression source,
            [NotNull]Expression target,
            [NotNull]IObjectUpdateManager manager,
            UpdateContext context)
        {
            ValidateCopiedElementType(objectType, manager);
            var method = _makeUpdateObjectExpressionBody.MakeGenericMethod(objectType);
            var expressionBody = method.Invoke(this, new object[] { paths, dropCreateOnPaths, start, source, target, manager, context }) as Expression;
            if (expressionBody == null)
            {
                throw new InvalidOperationException("ICopyActionFactory::MakeCopyActionBody returned null");
            }
            return expressionBody;
        }

        private Expression AddTask([NotNull] UpdateContext context, [NotNull] Expression taskExpression)
        {
            return Expression.Call(
                context.TaskList,
                _listFuncTaskAdd,
                new[] { WrapWithFunc(taskExpression) });
        }

        private Expression WrapWithFunc([NotNull]Expression taskExpression)
        {
            return Expression.Lambda<Func<Task>>(taskExpression);
        }

        private Func<Task> MakeFuncWithoutParams<T1, T2>(
            [NotNull]Func<T1, T2, Task> funcWithParams,
            T1 one,
            T2 two)
        {
            return () => funcWithParams(one, two);
        }

        private class UpdateContext
        {
            [NotNull]
            public ParameterExpression Source { get; private set; }
            [NotNull]
            public ParameterExpression Target { get; private set; }

            public bool Async { get; private set; }

            private readonly ParameterExpression _taskList;

            [NotNull]
            public ParameterExpression TaskList
            {
                get
                {
                    if (!Async || _taskList == null)
                    {
                        throw new InvalidOperationException("There is no task list on synchronous operations");
                    }
                    return _taskList;
                }
            }

            public UpdateContext([NotNull]ParameterExpression source, [NotNull]ParameterExpression target, bool async)
            {
                Source = source;
                Target = target;
                Async = async;
                if (async)
                {
                    _taskList = Expression.Variable(typeof(List<Func<Task>>));
                }
            }
        }
    }
}
