﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Jobs;
using TsSoft.AbstractMapper.Factory;
using TsSoft.Expressions.Helpers;

namespace UpdateObjectActionFactoryBenchmarks
{
    [LegacyJitX86Job, LegacyJitX64Job, RyuJitX64Job]
    public class Factories
    {
        private readonly Updated _updatedSource;
        private readonly Updated _updatedTarget;
        private IUpdateObjectActionFactory _factory;
        private BadCodegenUpdateObjectActionFactory _badCodegenFactory;

        private Func<Updated, Updated, IEnumerable<Func<Task>>> _action;
        private Func<Updated, Updated, IEnumerable<Func<Task>>> _badCodegenAction;

        public Factories()
        {
            _updatedSource = new Updated
            {
                ValueType = 1,
                ReferenceType = "1",
                ByDrop = new[]
                {
                    new Updated.UpdatedByDrop {ValueType = 2, ReferenceType = "2"},
                    new Updated.UpdatedByDrop {ValueType = 3, ReferenceType = "3"},
                },
                NotByDrop = new[]
                {
                    new Updated.UpdatedNotByDrop {ValueType = 4, ReferenceType = "4"},
                    new Updated.UpdatedNotByDrop {ValueType = 5, ReferenceType = "5"},
                }
            };
            _updatedTarget = new Updated
            {
                ValueType = 6,
                ReferenceType = "6",
                ByDrop = new[]
                {
                    new Updated.UpdatedByDrop {ValueType = 7, ReferenceType = "7"},
                    new Updated.UpdatedByDrop {ValueType = 8, ReferenceType = "8"},
                },
                NotByDrop = new[]
                {
                    new Updated.UpdatedNotByDrop {ValueType = 9, ReferenceType = "9"},
                    new Updated.UpdatedNotByDrop {ValueType = 10, ReferenceType = "10"},
                }
            };
            var paths = new Expression<Func<Updated, object>>[]
            {
                p => p.ValueType,
                p => p.ReferenceType,
                p => p.ByDrop,
                p => p.NotByDrop,
            };
            var dropCreateOnPaths = new Expression<Func<Updated, object>>[]
            {
                p => p.ByDrop,
            };
            var manager = AbstractMapperDependenciesFactory.GetDependency<Manager>(
                MockObjects.None, Entities.None, External.None);
            _factory = AbstractMapperDependenciesFactory.GetDependency<IUpdateObjectActionFactory>(MockObjects.None, Entities.None, External.None);
            _badCodegenFactory = AbstractMapperDependenciesFactory.GetDependency<BadCodegenUpdateObjectActionFactory>(MockObjects.None, Entities.None, External.None);

            _action = _factory.MakeAsyncUpdateAction(paths, manager, dropCreateOnPaths);
            _badCodegenAction = _badCodegenFactory.MakeAsyncUpdateAction(paths, manager, dropCreateOnPaths);
        }

        [Benchmark]
        public void Update()
        {
            _action(_updatedSource, _updatedTarget);
        }

        [Benchmark]
        public void BadCodegenUpdate()
        {
            _badCodegenAction(_updatedSource, _updatedTarget);
        }
    }
}
