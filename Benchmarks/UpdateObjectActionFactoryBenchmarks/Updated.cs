﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using TsSoft.Expressions.Helpers;
using TsSoft.Expressions.Helpers.Reflection;
using TsSoft.Expressions.Models;
using TsSoft.Expressions.Models.Reflection;

namespace UpdateObjectActionFactoryBenchmarks
{
    public class Updated
    {
        public int ValueType { get; set; }

        public string ReferenceType { get; set; }

        public ICollection<UpdatedByDrop> ByDrop { get; set; }

        public ICollection<UpdatedNotByDrop> NotByDrop { get; set; }

        public class UpdatedByDrop
        {
            public int ValueType { get; set; }
            public string ReferenceType { get; set; }
        }

        public class UpdatedNotByDrop
        {
            public int ValueType { get; set; }

            public string ReferenceType { get; set; }
        }
    }

    public class Manager : IObjectUpdateManager
    {
        private readonly IExpressionBuilder _expressionBuilder;

        public Manager(IExpressionBuilder expressionBuilder)
        {
            _expressionBuilder = expressionBuilder;
        }

        public bool IsPrimitive(Type type)
        {
            return type == typeof(int) || type == typeof(string);
        }

        public bool IsObject(Type type)
        {
            return type == typeof(Updated.UpdatedByDrop) || type == typeof(Updated.UpdatedNotByDrop);
        }

        public bool IsCollection(Type type)
        {
            return type == typeof(ICollection<Updated.UpdatedByDrop>)
                   || type == typeof(ICollection<Updated.UpdatedNotByDrop>);
        }

        public Expression MakeRemoveCollectionExpression(ValueHoldingMember collectionProperty, Type objectType, Expression target)
        {
            var elementType = collectionProperty.ValueType.GetGenericEnumerableArgument();
            var current = Expression.Parameter(elementType);
            var body = Expression.Call(
                typeof(Console).GetMethod("WriteLine", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(int) }, null),
                Expression.Property(current, "ValueType"));
            return _expressionBuilder.Foreach(
                Expression.MakeMemberAccess(target, collectionProperty.Member),
                current,
                body,
                elementType);
        }

        public Expression MakeAsyncRemoveCollectionExpression(ValueHoldingMember collectionProperty, Type objectType, Expression target)
        {
            var elementType = collectionProperty.ValueType.GetGenericEnumerableArgument();
            var current = Expression.Parameter(elementType);
            var body = Expression.Call(
                typeof(Console).GetMethod("WriteLine", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(int) }, null),
                Expression.Property(current, "ValueType"));
            var task = Expression.Constant(Task.CompletedTask);
            return Expression.Block(_expressionBuilder.Foreach(
                Expression.MakeMemberAccess(target, collectionProperty.Member),
                current,
                body,
                elementType), task);
        }

        public Expression MakeRemoveCollectionPartExpression(Expression source, Expression target)
        {
            var elementType = source.Type.GetGenericEnumerableArgument();
            var current = Expression.Parameter(elementType);
            var body = Expression.Call(
                typeof(Console).GetMethod("WriteLine", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(int) }, null),
                Expression.Property(current, "ValueType"));
            return _expressionBuilder.Foreach(
                target,
                current,
                body,
                elementType);
        }

        public Expression MakeAsyncRemoveCollectionPartExpression(Expression source, Expression target)
        {
            var elementType = source.Type.GetGenericEnumerableArgument();
            var current = Expression.Parameter(elementType);
            var body = Expression.Call(
                typeof(Console).GetMethod("WriteLine", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(int) }, null),
                Expression.Property(current, "ValueType"));
            var task = Expression.Constant(Task.CompletedTask);
            return Expression.Block(_expressionBuilder.Foreach(
                target,
                current,
                body,
                elementType), task);
        }

        public Expression MakeExtractKeyExpression(Expression obj)
        {
            return Expression.Property(obj, "ValueType");
        }
    }
}
