﻿using System;
using System.Diagnostics;
using System.Linq.Expressions;
using BenchmarkDotNet.Running;
using JetBrains.Annotations;
using TsSoft.AbstractMapper.Factory;
using TsSoft.Expressions.Helpers;
using TsSoft.Expressions.Helpers.Reflection;

namespace UpdateObjectActionFactoryBenchmarks
{
    class Program
    {
        private static void RunUpdaters([NotNull] Stopwatch sw)
        {
            var updateFactory = AbstractMapperDependenciesFactory.GetDependency<IUpdateObjectActionFactory>(MockObjects.None, Entities.None, External.None);
            var paths = new Expression<Func<Updated, object>>[]
            {
                p => p.ValueType,
                p => p.ReferenceType,
                p => p.ByDrop,
                p => p.NotByDrop,
            };
            var dropCreateOnPaths = new Expression<Func<Updated, object>>[]
            {
                p => p.ByDrop,
            };
            var action = updateFactory.MakeAsyncUpdateAction(
                paths,
                new Manager(
                    new ExpressionBuilder(new MemberInfoHelper(), new MemberInfoLibrary(new MemberInfoHelper()))),
                dropCreateOnPaths);
            Console.WriteLine("Created action: {0}", sw.Elapsed);

            sw.Restart();
            var tasks = action(new Updated
            {
                ValueType = 1,
                ReferenceType = "1",
                ByDrop = new[]
                {
                    new Updated.UpdatedByDrop {ValueType = 2, ReferenceType = "2"},
                    new Updated.UpdatedByDrop {ValueType = 3, ReferenceType = "3"},
                },
                NotByDrop = new[]
                {
                    new Updated.UpdatedNotByDrop {ValueType = 4, ReferenceType = "4"},
                    new Updated.UpdatedNotByDrop {ValueType = 5, ReferenceType = "5"},
                }
            },
            new Updated
            {
                ValueType = 6,
                ReferenceType = "6",
                ByDrop = new[]
                {
                    new Updated.UpdatedByDrop {ValueType = 7, ReferenceType = "7"},
                    new Updated.UpdatedByDrop {ValueType = 8, ReferenceType = "8"},
                },
                NotByDrop = new[]
                {
                    new Updated.UpdatedNotByDrop {ValueType = 9, ReferenceType = "9"},
                    new Updated.UpdatedNotByDrop {ValueType = 10, ReferenceType = "10"},
                }
            });
            Console.WriteLine("Updated once: {0}", sw.Elapsed);

            sw.Restart();
            int count = 1000000;
            for (int i = 0; i < count; ++i)
            {
                tasks = action(new Updated
                {
                    ValueType = 1,
                    ReferenceType = "1",
                    ByDrop = new[]
                {
                    new Updated.UpdatedByDrop {ValueType = 2, ReferenceType = "2"},
                    new Updated.UpdatedByDrop {ValueType = 3, ReferenceType = "3"},
                },
                    NotByDrop = new[]
                {
                    new Updated.UpdatedNotByDrop {ValueType = 4, ReferenceType = "4"},
                    new Updated.UpdatedNotByDrop {ValueType = 5, ReferenceType = "5"},
                }
                },
                new Updated
                {
                    ValueType = 6,
                    ReferenceType = "6",
                    ByDrop = new[]
                    {
                        new Updated.UpdatedByDrop {ValueType = 7, ReferenceType = "7"},
                        new Updated.UpdatedByDrop {ValueType = 8, ReferenceType = "8"},
                    },
                    NotByDrop = new[]
                    {
                        new Updated.UpdatedNotByDrop {ValueType = 9, ReferenceType = "9"},
                        new Updated.UpdatedNotByDrop {ValueType = 10, ReferenceType = "10"},
                    }
                });
            }
            Console.WriteLine("Updated: {0}", sw.Elapsed);
        }

        static void Main(string[] args)
        {
            if (args != null && args.Length == 1 && args[0] == "manual")
            {
                var sw = new Stopwatch();
                RunUpdaters(sw);
            }
            else
            {
                var runner = BenchmarkSwitcher.FromTypes(new Type[]
                {
                    typeof(Factories),
                });
                runner.Run(args);
            }

            Console.ReadLine();
            return;
        }

    }
}
