Param([Parameter(Mandatory=$true)]$ProjectNames, [Parameter(Mandatory=$true)]$VersionConfigPath)

$ErrorActionPreference = 'Continue'

try
{
    $ProjectNames = @($ProjectNames.Split(","))

    if (!(test-path $VersionConfigPath))
    {
        throw "Version config file '$VersionConfigPath' not found"
    }

    $versionsContent = ([string](Get-Content $VersionConfigPath))
    if (!$versionsContent)
    {
        $versionsContent = ''
    }
    $versions = ConvertFrom-Json $versionsContent
    if ($versions -eq $null)
    {
        $versions = [pscustomobject]@{}
    }

    $buildNupkg = '<PropertyGroup><GeneratePackageOnBuild>True</GeneratePackageOnBuild></PropertyGroup>'

    $projects = @(Get-ChildItem -Path Solution -Directory TsSoft.*)

    foreach ($project in $projects)
    {
        $csprojPath = $project.FullName + '\' + $project + '.csproj'
        $projectName = $project.Name
        if ($projectName.EndsWith('Tests'))
        {
            echo "Project $projectName is a test project, skipping"
            echo ''
            continue
        }

        if ($ProjectNames.Contains($projectName))
        {
            echo "Project $projectName is being rebuilt, bumping its version in $csprojPath"
            $rebuild = $true
        }
        else
        {
            $rebuild = $false
        }

        if ($versions.PSObject.Properties[$projectName] -eq $null)
        {
            Add-Member -InputObject $versions -MemberType NoteProperty -Name $projectName -Value ([pscustomobject]@{Version = $null})
        }
        $version = $versions.$projectName.Version
        echo "Old version for $projectName : $version"
        if ($version -eq $null)
        {
            echo "Config file does not contain the last version for project $projectName, setting build '0' in csproj"
            $version = 0
        }
        elseif ($rebuild)
        {
            ++$version
        }
        $versions.$projectName.Version = $version
        echo "New version for $projectName : $version"

        $csproj = Get-Content $csprojPath -Encoding UTF8
        $replacedVersion = $false
        $setRebuild = $false

        for ($i = 0; $i -lt $csproj.Length; ++$i)
        {
            $match = [regex]::Match($csproj[$i], '<Version>(\d+)\.(\d+)\.(\d+)</Version>')
            if ($match.Success)
            {
                $csproj[$i] = $csproj[$i] -replace '<Version>(\d+)\.(\d+)\.(\d+)</Version>',"<Version>`$1.`$2.$version</Version>"
                $replacedVersion = $true
            }

            if ($rebuild -and $csproj[$i].Contains('</Project>'))
            {
                $csproj[$i] = $csproj[$i].Replace('</Project>', $buildNupkg + '</Project>')
                $setRebuild = $true
            }
            elseif (-not $rebuild -and $csproj[$i].Contains($buildNupkg))
            {
                $csproj[$i] = $csproj[$i].Replace($buildNupkg, '')
            }
        }
        if (!$replacedVersion)
        {
            throw "Version not found in $projectName"
        }
        if ($rebuild -and -not $setRebuild)
        {
            throw "Could not configure nuget package build for $(projectName): </Project> tag not found"
        }
        Set-Content -Path $csprojPath -Encoding UTF8 -Value $csproj

        echo ''
    }
    Copy-Item $VersionConfigPath ($VersionConfigPath + '.bak')
    ConvertTo-Json $versions | Set-Content -Path $VersionConfigPath
}
catch
{
    Write-Error $_
    Write-Error $_.ScriptStackTrace
    Write-Error $_.Exception
    ##teamcity[buildStatus status='FAILURE']
    #[System.Environment]::Exit(1)
}
