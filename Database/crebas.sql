/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     25.12.2015 17:10:27                          */
/*==============================================================*/


/*==============================================================*/
/* Table: ComplexEntities                                       */
/*==============================================================*/
create table ComplexEntities (
   Id                   int                  not null,
   IsSaved              bit              not null,
   constraint PK_COMPLEXENTITIES primary key (Id)
)
go

/*==============================================================*/
/* Table: CompositeFks                                          */
/*==============================================================*/
create table CompositeFks (
   Id                   int                  not null,
   Part1Id              uniqueidentifier     not null,
   Part2Id              uniqueidentifier     not null,
   Data                 nvarchar(1000)       not null,
   constraint PK_COMPOSITEFKS primary key (Id)
)
go

/*==============================================================*/
/* Table: CompositePks                                          */
/*==============================================================*/
create table CompositePks (
   Part1                uniqueidentifier     not null,
   Part2                uniqueidentifier     not null,
   Data                 nvarchar(1000)       null,
   CompositeRootId      int                  not null,
   constraint PK_COMPOSITEPKS primary key (Part1, Part2)
)
go

/*==============================================================*/
/* Table: CompositeRoots                                        */
/*==============================================================*/
create table CompositeRoots (
   Id                   int                  not null,
   Data                 nvarchar(1000)       null,
   constraint PK_COMPOSITEROOTS primary key (Id)
)
go

/*==============================================================*/
/* Table: DiamondBottoms                                        */
/*==============================================================*/
create table DiamondBottoms (
   Id                   int                  not null,
   FirstId              int                  not null,
   Data                 int                  not null,
   constraint PK_DIAMONDBOTTOMS primary key (Id)
)
go

/*==============================================================*/
/* Table: DiamondOnes                                           */
/*==============================================================*/
create table DiamondOnes (
   Id                   int                  not null,
   FirstId              int                  not null,
   DiamondBottomId      int                  null,
   Data                 int                  not null,
   constraint PK_DIAMONDONES primary key (Id)
)
go

/*==============================================================*/
/* Table: DiamondTwos                                           */
/*==============================================================*/
create table DiamondTwos (
   Id                   int                  not null,
   FirstId              int                  not null,
   DiamondBottomId      int                  null,
   Data                 int                  not null,
   constraint PK_DIAMONDTWOS primary key (Id)
)
go

/*==============================================================*/
/* Table: Fifths                                                */
/*==============================================================*/
create table Fifths (
   Id                   int                  not null,
   ClientId             int                  not null,
   Type                 nvarchar(1000)       null,
   FourthId             uniqueidentifier     not null,
   constraint PK_FIFTHS primary key (Id)
)
go

/*==============================================================*/
/* Table: Firsts                                                */
/*==============================================================*/
create table Firsts (
   Id                   int                  not null,
   ClientId             int                  not null,
   Name                 nvarchar(1000)       null,
   IsDeleted            bit              not null,
   ImportantMoment      datetime            not null,
   ZeroId               int                  null,
   NullableBool         bit              null,
   ExternalEntityNullableId uniqueidentifier     null,
   ExternalEntityId     uniqueidentifier     not null,
   IsSaved              bit              not null,
   constraint PK_FIRSTS primary key (Id)
)
go

/*==============================================================*/
/* Table: Fourths                                               */
/*==============================================================*/
create table Fourths (
   Id                   uniqueidentifier     not null,
   ClientId             int                  not null,
   constraint PK_FOURTHS primary key (Id)
)
go

/*==============================================================*/
/* Table: NotInterestingSeconds                                 */
/*==============================================================*/
create table NotInterestingSeconds (
   Id                   int                  not null,
   ClientId             int                  not null,
   FirstId              int                  not null,
   Guid                 uniqueidentifier     not null,
   constraint PK_NOTINTERESTINGSECONDS primary key (Id)
)
go

/*==============================================================*/
/* Table: SecondWithFkPks                                       */
/*==============================================================*/
create table SecondWithFkPks (
   Id                   uniqueidentifier     not null,
   Type                 nvarchar(1000)       null,
   FirstId              int                  not null,
   constraint PK_SECONDWITHFKPKS primary key (Id)
)
go

/*==============================================================*/
/* Table: Seconds                                               */
/*==============================================================*/
create table Seconds (
   Id                   int                  not null,
   ClientId             int                  not null,
   Type                 nvarchar(1000)       null,
   FirstId              int                  not null,
   NullableFirstId      int                  null,
   Bool                 bit              not null,
   constraint PK_SECONDS primary key (Id)
)
go

/*==============================================================*/
/* Table: ThirdWithPkPks                                        */
/*==============================================================*/
create table ThirdWithPkPks (
   Id                   uniqueidentifier     not null,
   Data                 int                  null,
   constraint PK_THIRDWITHPKPKS primary key (Id)
)
go

/*==============================================================*/
/* Table: Thirds                                                */
/*==============================================================*/
create table Thirds (
   Id                   int                  not null,
   ClientId             int                  not null,
   Type                 nvarchar(1000)       not null,
   SecondId             int                  not null,
   FourthId             uniqueidentifier     null,
   NullableSecondId     int                  null,
   constraint PK_THIRDS primary key (Id)
)
go

/*==============================================================*/
/* Table: Trees                                                 */
/*==============================================================*/
create table Trees (
   ParentId             int                  null,
   Id                   int                  not null,
   Number               int                  not null,
   Data                 nvarchar(1000)       null,
   AdditionalData       int                  null,
   NotEnum              nvarchar(1000)       null,
   FirstId              int                  null,
   constraint PK_TREES primary key (Id)
)
go

/*==============================================================*/
/* Table: Zeroes                                                */
/*==============================================================*/
create table Zeroes (
   Id                   int                  not null,
   Guid                 uniqueidentifier     not null,
   Date                 datetime            not null,
   String               nvarchar(1000)       null,
   constraint PK_ZEROES primary key (Id)
)
go

alter table CompositeFks
   add constraint FK_COMPOSITEFK_REFERENCE_COMPOSITEPK foreign key (Part1Id, Part2Id)
      references CompositePks (Part1, Part2)
go

alter table CompositePks
   add constraint FK_COMPOSITEPK_REFERENCE_COMPOSITEROOT foreign key (CompositeRootId)
      references CompositeRoots (Id)
go

alter table DiamondBottoms
   add constraint FK_DIAMONDB_FK_DIAMON_FIRSTS foreign key (FirstId)
      references Firsts (Id)
go

alter table DiamondOnes
   add constraint FK_DIAMONDO_FK_DIAMON_DIAMONDB foreign key (DiamondBottomId)
      references DiamondBottoms (Id)
go

alter table DiamondOnes
   add constraint FK_DIAMONDO_FK_DIAMON_FIRSTS foreign key (FirstId)
      references Firsts (Id)
go

alter table DiamondTwos
   add constraint FK_DIAMONDT_FK_DIAMON_DIAMONDB foreign key (DiamondBottomId)
      references DiamondBottoms (Id)
go

alter table DiamondTwos
   add constraint FK_DIAMONDT_FK_DIAMON_FIRSTS foreign key (FirstId)
      references Firsts (Id)
go

alter table Fifths
   add constraint FK_FIFTHS_FK_FIFTHS_FOURTHS foreign key (FourthId)
      references Fourths (Id)
go

alter table Firsts
   add constraint FK_FIRSTS_FK_FIRSTS_ZEROES foreign key (ZeroId)
      references Zeroes (Id)
go

alter table NotInterestingSeconds
   add constraint FK_NOTINTER_FK_NOTINT_FIRSTS foreign key (FirstId)
      references Firsts (Id)
go

alter table SecondWithFkPks
   add constraint FK_SECONDWI_FK_SECOND_FIRSTS foreign key (FirstId)
      references Firsts (Id)
go

alter table SecondWithFkPks
   add constraint FK_SECONDWI_FK_SECOND_THIRDWIT foreign key (Id)
      references ThirdWithPkPks (Id)
go

alter table Seconds
   add constraint FK_SECONDS_FK_SECOND_FIRSTS foreign key (FirstId)
      references Firsts (Id)
go

alter table Thirds
   add constraint FK_THIRDS_FK_THIRDS_FOURTHS foreign key (FourthId)
      references Fourths (Id)
go

alter table Thirds
   add constraint FK_THIRDS_FK_THIRDS_SECONDS foreign key (SecondId)
      references Seconds (Id)
go

alter table Thirds
   add constraint FK_THIRDS_REFERENCE_SECONDS_NULLABLE foreign key (NullableSecondId)
      references Seconds (Id)
         on delete set null
go

alter table Trees
   add constraint FK_TREES_FK_TREES__FIRSTS foreign key (FirstId)
      references Firsts (Id)
go

alter table Trees
   add constraint FK_TREES_FK_TREES__TREES foreign key (ParentId)
      references Trees (Id)
go

