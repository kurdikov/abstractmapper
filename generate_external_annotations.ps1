﻿& 'c:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe' 'Solution\AbstractMapper.sln' '/t:Clean;Build' /p:Configuration=Debug /p:DefineConstants="JETBRAINS_ANNOTATIONS"

if (!(test-path 'Annotations'))
{
	new-item -itemtype directory 'Annotations'
}

foreach ($nuspec in (get-childitem "*.nuspec.xml"))
{
    $projectName = $nuspec.Name.Replace(".nuspec.xml", "")
	if (!(test-path "Annotations\$projectName"))
	{
		new-item -itemtype directory "Annotations\$projectName"
	}
	echo "processing $projectName"
	cp '.\Tools\reag.exe' "Solution\$projectName\bin\Debug\reag.exe"
	& "Solution\$projectName\bin\Debug\reag.exe" "Solution\$projectName\bin\Debug\$projectName.dll" | select -skip 1 | out-file -encoding oem "Annotations\$projectName\$projectName.ExternalAnnotations.xml"
	rm "Solution\$projectName\bin\Debug\reag.exe"
}
