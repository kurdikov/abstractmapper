﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules.Factories;
    using TsSoft.Expressions.Helpers.Closures;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models.AbstractMapper;

    internal class AbstractIncludeProviderMapperHelper : AbstractMapperHelper, IAbstractIncludeProviderMapperHelper
    {
        [NotNull] private readonly IIncludeCreator _includeCreator;

        public AbstractIncludeProviderMapperHelper(
            [NotNull] IMapExpressionCreator expressionCreator,
            [NotNull] IIgnoreRulesFactory ignoreRulesFactory,
            [NotNull] ILambdaCompiler lambdaCompiler,
            [NotNull] IFullMapRulesCreator fullMapRulesCreator,
            [NotNull] IIncludeCreator includeCreator)
            : base(expressionCreator, ignoreRulesFactory, lambdaCompiler, fullMapRulesCreator)
        {
            if (includeCreator == null) throw new ArgumentNullException("includeCreator");
            _includeCreator = includeCreator;
        }

        public Includes<TFrom> GetIncludesFromPaths<TFrom, TTo>(IEnumerable<MappedPathDescription> paths, GeneratorContext context = null)
        {
            return _includeCreator.Create<TFrom>(paths, typeof (TTo), context ?? new GeneratorContext());
        }
    }
}
