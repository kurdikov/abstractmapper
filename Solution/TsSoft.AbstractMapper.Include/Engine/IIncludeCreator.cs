﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Создаёт инклюды по описанию маппинга
    /// </summary>
    public interface IIncludeCreator
    {
        /// <summary>
        /// Создать include-выражения по используемым преобразователем путям
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности БД</typeparam>
        /// <param name="paths">Используемые преобразователем пути</param>
        /// <param name="to">Тип, в который идёт преобразование</param>
        /// <param name="context">Контекст генерации</param>
        [NotNull]
        Includes<TEntity> Create<TEntity>(IEnumerable<MappedPathDescription> paths, Type to, GeneratorContext context);
    }
}
