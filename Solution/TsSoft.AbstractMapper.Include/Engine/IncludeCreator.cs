﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.NestedGenerators;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.AbstractMapper;

    internal class IncludeCreator : IIncludeCreator
    {
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;
        [NotNull]private readonly IEntityTypesRetriever _entityTypesRetriever;
        [NotNull]private readonly IMapperResolver _mapperResolver;
        [NotNull]private readonly IFlatPathParser _flatPathParser;
        [NotNull]private readonly IPathHelper _pathHelper;
        [NotNull] private readonly INestedGeneratorHelper _nestedGeneratorHelper;

        [NotNull]private readonly MethodInfo _tryGetMapperIncludes;
        [NotNull]private readonly MethodInfo _tryGetProcessorIncludes;

        [NotNull]private readonly ParsedIncludes _nestingExhausted = new ParsedIncludes { NestingExhausted = true, Path = new IEnumerable<ValueHoldingMember>[0]};

        public IncludeCreator(
            [NotNull] IExpressionBuilder expressionBuilder,
            [NotNull] IEntityTypesRetriever entityTypesRetriever,
            [NotNull]IMemberInfoHelper memberInfoHelper,
            [NotNull] IMapperResolver mapperResolver,
            [NotNull]IFlatPathParser flatPathParser, 
            [NotNull]IPathHelper pathHelper, 
            [NotNull]INestedGeneratorHelper nestedGeneratorHelper)
        {
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (entityTypesRetriever == null) throw new ArgumentNullException("entityTypesRetriever");
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (mapperResolver == null) throw new ArgumentNullException("mapperResolver");
            if (flatPathParser == null) throw new ArgumentNullException("flatPathParser");
            if (pathHelper == null) throw new ArgumentNullException("pathHelper");
            if (nestedGeneratorHelper == null) throw new ArgumentNullException("nestedGeneratorHelper");

            _expressionBuilder = expressionBuilder;
            _entityTypesRetriever = entityTypesRetriever;
            _mapperResolver = mapperResolver;
            _flatPathParser = flatPathParser;
            _pathHelper = pathHelper;
            _nestedGeneratorHelper = nestedGeneratorHelper;

            _tryGetMapperIncludes = memberInfoHelper.GetGenericDefinitionMethodInfo(() => TryGetMapperIncludes<object, object, object>(null));
            _tryGetProcessorIncludes = memberInfoHelper.GetGenericDefinitionMethodInfo(() => TryGetProcessorIncludes<object>(null, null));
        }

        public Includes<TEntity> Create<TEntity>(IEnumerable<MappedPathDescription> paths, Type to, GeneratorContext context)
        {
            var result = new Includes<TEntity>();
            foreach (var path in paths ?? Enumerable.Empty<MappedPathDescription>())
            {
                if (path == null || path.Path == null || path.Path.Elements == null || !path.Path.Elements.Any())
                {
                    continue;
                }
                var pathToInclude = path.Path.Elements
                    .Select(pe => pe.ThrowIfNull("pe").Step)
                    .TakeWhile(pi => _entityTypesRetriever.IsIncludableType(pi.ThrowIfNull("pi").ValueType))
                    .ToList();
                if (path.MapperDescription == null && path.ProcessorDescription == null)
                {
                    var include = _expressionBuilder.BuildIncludeByPath<TEntity>(pathToInclude);
                    if (include != null)
                    {
                        result.Add(include);
                    }
                }
                else if (path.ProcessorDescription != null)
                {
                    ExtractAndAdd(path.ProcessorDescription.Processor, result, pathToInclude, context);
                }
                else if (path.MapperDescription.MapperObject != null)
                {
                    ExtractAndAdd(path.MapperDescription.MapperObject, result, pathToInclude, context);
                }
                else
                {
                    var tryGetMapperIncludesMethod = _tryGetMapperIncludes.MakeGenericMethod(
                        path.MapperDescription.MapperFromType,
                        path.MapperDescription.MapperFromSingleType,
                        path.MapperDescription.MapperToType);
                    var tryGetMapperIncludesDelegate = DelegateCreator.Create<Func<GeneratorContext, ParsedIncludes>>(
                        this, tryGetMapperIncludesMethod);
                    var innerIncludedPaths = tryGetMapperIncludesDelegate(context);
                    AddIncludes(result, pathToInclude, innerIncludedPaths);
                }
            }
            return new Includes<TEntity>(result.Where(i => i != null).Distinct(IncludeEqualityComparer<TEntity>.Instance));
        }

        private void ExtractAndAdd<TEntity>([NotNull]object processor, [NotNull]Includes<TEntity> result, [NotNull]List<ValueHoldingMember> pathToInclude, GeneratorContext context)
        {
            var pathSingleType = _pathHelper.GetPathSingleType(typeof(TEntity), pathToInclude);
            var tryGetProcessorIncludesMethod = _tryGetProcessorIncludes.MakeGenericMethod(pathSingleType);
            var tryGetProcessorIncludesDelegate = DelegateCreator.Create<Func<object, GeneratorContext, ParsedIncludes>>(
                this, tryGetProcessorIncludesMethod);
            var innerIncludedPaths = tryGetProcessorIncludesDelegate(processor, context);
            AddIncludes(result, pathToInclude, innerIncludedPaths);
        }

        private void AddIncludes<TEntity>(
            [NotNull]List<Expression<Func<TEntity, object>>> includes,
            [NotNull]IReadOnlyCollection<ValueHoldingMember> prefix,
            ParsedIncludes innerIncludedPaths)
        {
            if (innerIncludedPaths == null || (!innerIncludedPaths.NestingExhausted && !innerIncludedPaths.Path.Any()))
            {
                includes.Add(_expressionBuilder.BuildIncludeByPath<TEntity>(prefix));
            }
            else if (!innerIncludedPaths.NestingExhausted)
            {
                includes.AddRange(innerIncludedPaths.Path.Where(p => p != null).Select(includedPath =>
                    _expressionBuilder.BuildIncludeByPath<TEntity>(prefix.Concat(includedPath))));
            }
        }

        private ParsedIncludes ParseIncludes<TInner>([NotNull]IEnumerable<Expression<Func<TInner, object>>> includes)
        {
            return new ParsedIncludes { Path = includes.Select(_flatPathParser.Parse).ToList() };
        }

        private ParsedIncludes GetProcessorIncludes<TInner>(
            IIncludeProvider<TInner> processor)
        {
            var includes = processor != null
                ? processor.Includes ?? Enumerable.Empty<Expression<Func<TInner, object>>>()
                : Enumerable.Empty<Expression<Func<TInner, object>>>();
            return ParseIncludes(includes);
        }

        private ParsedIncludes TryGetMapperIncludes<TInnerFrom, TInnerFromSingle, TInnerTo>(GeneratorContext context)
        {
            var mapper = _mapperResolver.TryGet<TInnerFrom, TInnerTo>();
            if (mapper == null)
            {
                throw new AbstractMapperResolverException(typeof(IMapper<TInnerFrom, TInnerTo>));
            }
            return TryGetProcessorIncludes<TInnerFromSingle>(mapper, context);
        }

        private ParsedIncludes TryGetProcessorIncludes<TInnerSingle>(object processorCandidate, GeneratorContext context)
        {
            var generator = processorCandidate as ICyclePreventingIncludeGenerator<TInnerSingle>;
            if (generator != null)
            {
                return _nestedGeneratorHelper.GenerateInContext(
                    generator,
                    g => g.Includes,
                    (g, c) => g.GenerateIncludes(c),
                    ParseIncludes,
                    context,
                    _nestingExhausted);
            }
            var processor = processorCandidate as IIncludeProvider<TInnerSingle>;
            return GetProcessorIncludes(processor);
        }

        private class ParsedIncludes
        {
            [NotNull]
            public IReadOnlyCollection<IEnumerable<ValueHoldingMember>> Path { get; set; }

            public bool NestingExhausted { get; set; }
        }
    }
}
