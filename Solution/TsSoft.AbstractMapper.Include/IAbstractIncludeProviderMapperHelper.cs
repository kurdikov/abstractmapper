﻿namespace TsSoft.AbstractMapper
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Хелпер для абстрактного маппера, отдающего инклюды
    /// </summary>
    public interface IAbstractIncludeProviderMapperHelper : IAbstractMapperHelper
    {
        /// <summary>
        /// Получить инклюды по описанию маппинга
        /// </summary>
        /// <typeparam name="TFrom">Тип, из которого осуществляется преобразование</typeparam>
        /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
        /// <param name="paths">Описание маппинга</param>
        /// <param name="context">Контекст генерации</param>
        [NotNull]
        Includes<TFrom> GetIncludesFromPaths<TFrom, TTo>(IEnumerable<MappedPathDescription> paths, GeneratorContext context = null);
    }
}
