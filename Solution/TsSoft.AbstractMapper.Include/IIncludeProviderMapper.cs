﻿namespace TsSoft.AbstractMapper
{
    using System.Collections.Generic;
    using TsSoft.Expressions.IncludeBuilder.Models;

    /// <summary>
    /// Маппер, отдающий необходимые ему инклюды
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    /// <typeparam name="TModel">Тип модели</typeparam>
    public interface IIncludeProviderMapper<TEntity, out TModel> : IMapper<TEntity, TModel>, IIncludeProvider<TEntity>
    {
    }

    /// <summary>
    /// Маппер коллекции, отдающий необходимые ему инклюды
    /// </summary>
    /// <typeparam name="TFrom">Тип сущности</typeparam>
    /// <typeparam name="TTo">Тип модели</typeparam>
    public interface IIncludeProviderCollectionMapper<TFrom, out TTo> : IMapper<IEnumerable<TFrom>, TTo>, IIncludeProvider<TFrom>
    {
    }
}
