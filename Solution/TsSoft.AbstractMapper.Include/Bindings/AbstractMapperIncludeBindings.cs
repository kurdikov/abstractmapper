﻿namespace TsSoft.Bindings
{
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Engine;
    using TsSoft.BindingsDescription;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.AbstractMapper.Include
    /// </summary>
    public class AbstractMapperIncludeBindings : BindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке TsSoft.AbstractMapper.Include
        /// </summary>
        public AbstractMapperIncludeBindings()
        {
            Bind<IAbstractIncludeProviderMapperHelper, AbstractIncludeProviderMapperHelper>();
            Bind<IIncludeCreator, IncludeCreator>();
        }
    }
}
