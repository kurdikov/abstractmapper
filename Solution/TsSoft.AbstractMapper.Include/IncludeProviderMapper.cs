﻿namespace TsSoft.AbstractMapper
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Маппер, отдающий необходимые ему инклюды
    /// </summary>
    /// <typeparam name="TFrom">Тип, из которого осуществляется преобразование</typeparam>
    /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
    public class IncludeProviderMapper<TFrom, TTo> : Mapper<TFrom, TTo>, IIncludeProviderMapper<TFrom, TTo>, ICyclePreventingIncludeGenerator<TFrom> 
        where TFrom : class
        where TTo : new()
    {
        [NotNull]
        private readonly Lazy<Includes<TFrom>> _includes;

        /// <summary>
        /// Маппер, отдающий необходимые ему инклюды
        /// </summary>
        public IncludeProviderMapper([NotNull]IAbstractIncludeProviderMapperHelper mapperHelper)
            : base(mapperHelper)
        {
            _includes = new Lazy<Includes<TFrom>>(() => GenerateIncludes(new GeneratorContext(GetType(), MaxNestingLevel)));
        }

        /// <summary>
        /// Вся ли необходимая информация получена из построенного выражения
        /// </summary>
        protected override bool IsExpressionProcessed
        {
            get { return base.IsExpressionProcessed && _includes.IsValueCreated; }
        }

        /// <summary>
        /// Инклюды, необходимые мапперу
        /// </summary>
        public virtual IReadOnlyIncludes<TFrom> Includes { get { return GetLazyValueAndClearCacheIfProcessed(_includes); } }

        /// <summary>
        /// Добавленные в обход правил преобразования инклюды
        /// </summary>
        protected virtual IReadOnlyIncludes<TFrom> AdditionalIncludes {get { return null; }}

        /// <summary>
        /// Создать инклюды в заданном контексте
        /// </summary>
        public Includes<TFrom> GenerateIncludes(GeneratorContext context)
        {
            try
            {
                var result = _includes.IsValueCreated// || (context == null || !context.RemainingNestingLevel.ContainsKey(GetType()))
                    ? _includes.Value
                    : ((IAbstractIncludeProviderMapperHelper)MapperHelper).GetIncludesFromPaths<TFrom, TTo>(MapperExpression.MappedPaths, context);
                var additionalIncludes = AdditionalIncludes;
                if (additionalIncludes != null)
                {
                    result.AddRange(additionalIncludes);
                    result.IncludeConditions.AddRange(additionalIncludes.IncludeConditions);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    string.Format("Unable to build includes for {0}", GetType()),
                    ex);
            }
        }

        /// <summary>
        /// Максимальное количество вложений маппера в себя
        /// </summary>
        public virtual int MaxNestingLevel
        {
            get { return 0; }
        }
    }
}
