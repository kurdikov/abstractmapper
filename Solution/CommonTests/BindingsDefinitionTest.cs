﻿namespace CommonTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using ParserGenerator;
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.BindingsDescription;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository;
    using TsSoft.EntityRepository.External;
    using TsSoft.EntityRepository.Factory;
    using TsSoft.EntityRepository.Helpers.Clone;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.EntityRepository.Models;
    using TsSoft.EntityService.Mapper;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Differences;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.Models.Differences;
    using TsSoft.Expressions.Models.Reflection;
    using TsSoft.Expressions.Models.Tuples;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator;
    using TsSoft.Expressions.SelectBuilder.DynamicTypes;
    using TsSoft.Expressions.SelectBuilder.Models;

    [TestClass]
    public class BindingsDefinitionTest
    {
        [TestMethod]
        public void TestDefinitions()
        {
            var nonbindableInterfaces = new HashSet<Type>
            {
#if !NET45
                typeof(IMapperResolver),
#endif
                typeof(IEntityTypesRetriever),
                typeof(IMapper<,>),
                typeof(IIncludeProviderMapper<,>),
                typeof(IIncludeProviderCollectionMapper<,>),
                typeof(ISelectProviderMapper<,>),
                typeof(ISelectProviderCollectionMapper<,>),
                typeof(IIncludeProvider<>),
                typeof(ISelectProvider<>),
                typeof(IUpdatePathProvider<>),
                typeof(IUpdatePathProviderMapper<,>),
                typeof(IUpdateEntityMapper<,>),
                typeof(IDeletable),
                typeof(IEntityWithId<>),
                typeof(IComplexEntity),
                typeof(IExternalEntity),
                typeof(IExternalEntity<>),
                typeof(IExternalRepository<>),
                typeof(IExternalRepository<,>),
                typeof(IAsyncExternalRepository<,>),
                typeof(IExternalIncludeFunctionalDescription<,>),
                typeof(IConditionalIncludeFunctionalDescription<,>),
                typeof(IRepositoryFactory),
                typeof(IIncludeCondition<>),
                typeof(IOrderByClause<>),
                typeof(IAssemblyRetriever),
                typeof(IAssemblyTypesRetriever),
                typeof(IBindingsRetriever),
                typeof(ICyclicForeignKey<>),
                typeof(IDynamicTypePool),
                typeof(IBindingsDescription),
                typeof(IReadOnlyIncludes<>),
                typeof(IReadOnlySelectExpression<>),
                typeof(IHasParent<>),
                typeof(IWithChildren<>),
                typeof(ITreeElement<,>),
                typeof(IIgnoreRules<>),
                typeof(IMapRules<,>),
                typeof(IUpdateRules<>),
                typeof(IItemStore),
                typeof(IItemFactory<>),
                typeof(ILazy<>),
                typeof(IItemWrapper),
                typeof(IPage<>),
                typeof(IExpressionContext),
                typeof(IPseudoExpressionListener),
                typeof(IPseudoExpressionVisitor<>),
                typeof(IEqualityComparerProvider),
                typeof(IDiff),
                typeof(IPrimitiveCollectionDiff),
                typeof(ICollectionDiff),
                typeof(IPrimitiveCollectionDiff<>),
                typeof(ICollectionDiff<>),
                typeof(IObjectDiff),
                typeof(IObjectDiff<>),
                typeof(IObjectWithDiff<>),
                typeof(IPrimitiveDiff),
                typeof(IPrimitiveDiff<>),
                typeof(IDisposableLazy<>),
                typeof(IRawSqlRepository),
                typeof(IRawSqlReadRepository),
                typeof(IRawSqlWriteRepository),
                typeof(IHasParentId<>),
                typeof(ITsSoftDatabaseTransaction),
                typeof(IObjectUpdateManager),
                typeof(IUpdateFuncProvider<>),
                typeof(IAsyncUpdateFuncProvider<>),
                typeof(IUpdateFuncProviderMapper<,>),
                typeof(ICyclePreventingSelectGenerator<>),
                typeof(ICyclePreventingIncludeGenerator<>),
                typeof(ICyclePreventingUpdatePathGenerator<>),
                typeof(ICyclePreventingGenerator),
                typeof(INestedGenerationContext),
                typeof(IEquatableTuple),
                typeof(IPredicateBuilder<>),
                typeof(IUnresolvedDependency),
                typeof(IFullMapRules),
            };
            var assemblies = Assemblies.GetAssemblies();
            var bindingRetriever = new BindingsRetriever();
            var bindings = bindingRetriever.GetBindings(assemblies).ToDictionary(b => b.Key, b => b.Value);
            var badBindings = bindings.Where(b => b.Key == null || b.Value == null || (b.Key.GetTypeInfo().IsGenericType && b.Key.GetTypeInfo().IsGenericTypeDefinition && b.Value.GetInterfaces()
                .All(i => i == null || !i.GetTypeInfo().IsGenericType || i.GetGenericTypeDefinition() != b.Key))).ToList();
            if (badBindings.Any())
            {
                Assert.Fail("Bad bindings : {0}", string.Join(", ", badBindings.Select(bb => string.Format("{0} to {1}", bb.Key, bb.Value))));
            }
            var interfaces = assemblies.SelectMany(a => a.GetTypes()).Where(t => t != null && t.GetTypeInfo().IsInterface).Except(nonbindableInterfaces);
            var notBound = interfaces.Where(inter => !bindings.ContainsKey(inter)).ToList();
            if (notBound.Any())
            {
                Assert.Fail("Not bound interfaces: {0}", string.Join(",", notBound.Select(t => t.Name)));
            }
        }
    }
}
