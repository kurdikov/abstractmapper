﻿namespace CommonTests
{
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Bindings;
    using TsSoft.BindingsDescription;

    static class Assemblies
    {
        [NotNull]
        public static IBindingsDescription[] GetBindingDescriptions()
        {
            return new IBindingsDescription[]
            {
                new AbstractMapperBindings(),
                new AbstractMapperIncludeBindings(),
#if NET45
                new AbstractMapperNinjectResolverBindings(),
#endif
                new AbstractMapperSelectBindings(),
                new AbstractMapperUpdateBindings(),
                new EntityRepositoryBindings(),
                new EntityServiceBindings(),
                new ExpressionsHelpersBindings(),
                new ExpressionsHelpersEntityBindings(),
                new ExpressionsIncludeBuilderBindings(),
                new ExpressionsSelectBuilderBindings(),
                new ContextWrapperBindings(),
                new ExpressionsPseudoExpressionParserBindings(),
            };
        }

        [NotNull]
        public static Assembly[] GetAssemblies()
        {
            var assemblyRetriever = new AssemblyRetriever();
            var assemblies = assemblyRetriever.GetAssembliesWithReferences(GetBindingDescriptions().Select(d => d != null ? d.GetType() : null));
            return assemblies.ToArray();
        }
    }
}
