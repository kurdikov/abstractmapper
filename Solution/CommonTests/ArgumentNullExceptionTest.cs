﻿namespace CommonTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.EntityRepository;
    using TsSoft.Expressions.Helpers.DynamicTypes;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;

    [TestClass]
    public class ArgumentNullExceptionTest
    {
        [NotNull]
        private readonly InterfaceImplementationCreator _interfaceImplementationCreator =
            new InterfaceImplementationCreator(new PropertyCreator());
        [NotNull]
        private readonly IDelegateTypeHelper _delegateTypeHelper = new DelegateTypeHelper();

        private abstract class UnexpectedParamName
        {
            public abstract MemberInfo Member { get; }

            public string ExpectedParam { get; protected set; }

            public string ActualParam { get; protected set; }

            protected UnexpectedParamName(string expectedParam, string actualParam)
            {
                ExpectedParam = expectedParam;
                ActualParam = actualParam;
            }
        }

        private class ConstructorUnexpectedParamName : UnexpectedParamName
        {
            public ConstructorInfo Constructor { get; private set; }

            public override MemberInfo Member
            {
                get { return Constructor; }
            }

            public ConstructorUnexpectedParamName(string expectedParam, string actualParam, ConstructorInfo constructor) : base(expectedParam, actualParam)
            {
                Constructor = constructor;
            }
        }

        [UsedImplicitly]
        public readonly object Field;
        public void Method(object param)
        {
        }

        [NotNull]
        private Type FillGenericArguments([NotNull] Type genericType)
        {
            var genericParams = genericType.GetGenericArguments();
            var genericArgs = new Type[genericParams.Length];
            for (int i = 0; i < genericParams.Length; ++i)
            {
                var genericParam = genericParams[i].ThrowIfNull("genericParam");
                var attrs = genericParam.GetTypeInfo().GenericParameterAttributes;
                if (attrs.HasFlag(GenericParameterAttributes.NotNullableValueTypeConstraint))
                {
                    genericArgs[i] = typeof(int);
                }
                else
                {
                    genericArgs[i] = typeof(object);
                }
                var constraints = genericParam.GetTypeInfo().GetGenericParameterConstraints();
                if (constraints.Any(c => c != null && !c.IsAssignableFrom(genericArgs[i])))
                {
                    if (constraints.Length == 1)
                    {
                        if (!constraints[0].GetTypeInfo().IsGenericType)
                        {
                            genericArgs[i] = constraints[0];
                        }
                    }
                    else if (constraints.All(c => c != null && c.GetTypeInfo().IsInterface && !c.GetTypeInfo().IsGenericType))
                    {
                        genericArgs[i] = _interfaceImplementationCreator.CreateInterfaceImplementation(constraints);
                    }
                    else
                    {
                        throw new Exception(string.Format("Could not fill generic arguments for {0} - edit test", genericType));
                    }
                }
            }
            return genericType.MakeGenericType(genericArgs);
        }

        private LambdaExpression GetLambdaExpression(Type delegateType)
        {
            var lambdaDesc = _delegateTypeHelper.GetDelegateDescription(delegateType);
            return Expression.Lambda(
                delegateType,
                Expression.Default(lambdaDesc.ReturnType),
                lambdaDesc.ParameterTypes.Select(p => Expression.Parameter(p)));
        }

        private Delegate GetDelegate(Type delegateType)
        {
            return GetLambdaExpression(delegateType).Compile();
        }

        private object GetObject(Type type, Type parentType, ICollection<Exception> exceptions, object[] mockCtorArgs)
        {
            if (type == typeof(string))
            {
                return string.Empty;
            }
            if (type == typeof(Type))
            {
                return typeof(string);
            }
            if (type == typeof(MemberInfo))
            {
                return GetType().GetField("Field");
            }
            if (type == typeof(MethodInfo))
            {
                return GetType().GetMethod("Method");
            }
            if (type == typeof(ParameterInfo))
            {
                return GetType().GetMethod("Method").GetParameters()[0];
            }
            if (type == typeof(LambdaExpression) || type == typeof(Expression))
            {
                return GetLambdaExpression(typeof(Action));
            }
            if (type == typeof(ParameterExpression))
            {
                return Expression.Parameter(typeof(object));
            }
            if (type.GetTypeInfo().IsGenericType && type.GetGenericTypeDefinition() == typeof(Expression<>))
            {
                var delegateType = type.GetGenericArguments()[0];
                return GetLambdaExpression(delegateType);
            }
            if (_delegateTypeHelper.IsDelegateType(type))
            {
                return GetDelegate(type);
            }
            if (type.IsArray)
            {
                return Array.CreateInstance(type.GetElementType(), 0);
            }
            if (type.IsGenericEnumerable())
            {
                return Array.CreateInstance(type.GetGenericEnumerableArgument(), 0);
            }
            if (!(type.GetTypeInfo().IsInterface || type.GetTypeInfo().IsClass && !type.GetTypeInfo().IsSealed))
            {
                throw new Exception(string.Format("Can not mock argument {0} of constructor of {1} - edit test", type, parentType));
            }
            if (!type.GetTypeInfo().IsInterface && type.GetConstructors().All(c => c.GetParameters().Length > 0))
            {
                var argCtor = type.GetConstructors().FirstOrDefault(c => c.GetParameters().All(p => p.ParameterType != type));
                if (argCtor != null)
                {
                    object[] argObjs = new object[0];
                    try
                    {
                        argObjs = argCtor.GetParameters().Select(o => GetObject(o.ParameterType, type, exceptions, mockCtorArgs)).ToArray();
                        return argCtor.Invoke(argObjs);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Unable to create an instance of {0} using {1}", type, string.Join(",", argObjs)), ex);
                    }
                }
                throw new Exception(string.Format("Unable to find a suitable constructor of {0}", type));
            }

            try
            {
                var mockType = typeof(Mock<>).MakeGenericType(type);
                var mock = Activator.CreateInstance(mockType, mockCtorArgs);
                return mockType.GetProperty("Object", type).ThrowIfNull("Object property of mock").GetValue(mock);
            }
            catch (Exception ex)
            {
                exceptions.Add(new Exception(string.Format("Failed to mock {0} for {1}", type, parentType), ex));
            }
            return null;
        }

        [NotNull]
        private IEnumerable<Type> GetInstantiableTypes()
        {
            var assemblies = Assemblies.GetAssemblies();
            var instantiableClasses = assemblies
                .SelectMany(a => a.ThrowIfNull("a").GetTypes())
                .Where(IsInstantiable);
            return instantiableClasses;
        }

        private bool IsInstantiable(Type t)
        {
            if (t == null)
            {
                return false;
            }
            var typeInfo = t.GetTypeInfo();
            return typeInfo.IsClass
                && !typeInfo.IsAbstract
                && !typeof(Delegate).IsAssignableFrom(t)
                && !typeInfo.IsDefined(typeof(CompilerGeneratedAttribute));
        }

        [TestMethod]
        public void CheckConstructorsThrowNullArgumentExceptions()
        {
            var exceptions = new List<Exception>();
            var mockCtorArgs = new object[] { MockBehavior.Loose };

            var instantiableClasses = GetInstantiableTypes();

            var doNotCheck = new[]
            {
                typeof(TsSoftDbContextTransaction),
            };

            foreach (var instantiableClass in instantiableClasses.Except(doNotCheck))
            {
                var type = instantiableClass.ThrowIfNull("type");
                var constructors = type.GetConstructors();
                if (!constructors.Any(c => c.GetParameters().Length > 0))
                {
                    continue;
                }
                if (type.GetTypeInfo().IsGenericTypeDefinition)
                {
                    type = FillGenericArguments(type);
                }
                foreach (var constructor in type.GetConstructors())
                {
                    var ctor = constructor.ThrowIfNull("ctor");
                    var ctorParams = ctor.GetParameters();
                    var ctorArgs = new ConstructorArgument[ctorParams.Length];
                    for (int i = 0; i < ctorParams.Length; ++i)
                    {
                        var param = ctorParams[i].ThrowIfNull("param");
                        ctorArgs[i] = new ConstructorArgument(param.ParameterType);
                        if (!param.ParameterType.IsReferenceType())
                        {
                            ctorArgs[i].Value = Activator.CreateInstance(param.ParameterType);
                        }
                    }
                    for (int i = 0; i < ctorParams.Length; ++i)
                    {
                        var param = ctorParams[i].ThrowIfNull("param");
                        var arg = ctorArgs[i].ThrowIfNull("arg");
                        if (!param.ParameterType.IsReferenceType())
                        {
                            continue;
                        }
                        if (param.IsDefined(typeof(NotNullAttribute)))
                        {
                            try
                            {
                                ExceptAssert.Throws<ArgumentNullException>(
                                    () => Create(ctor, ctorArgs),
                                    ex => ex != null && (ex.ParamName == param.Name),
                                    ex => ex == null
                                        ? string.Format("ArgumentNullException not thrown for {0} parameter of constructor of {1}", param.Name, type)
                                        : string.Format("Parameter name of constructor of {0} : {1}. ParamName of thrown exception: {2}.", type, param.Name, ex.ParamName));
                            }
                            catch (Exception ex)
                            {
                                exceptions.Add(ex);
                            }

                            arg.Value = GetObject(arg.Type, type, exceptions, mockCtorArgs);
                        }
                        //else if (!param.IsDefined(typeof(CanBeNullAttribute)))
                        //{
                        //    exceptions.Add(new AssertFailedException(string.Format(
                        //        "Parameter {0} of constructor of {1} does not have NotNullAttribute and CanBeNullAttribute",
                        //        param.Name,
                        //        type)));
                        //}
                    }
                }
            }

            PrintExceptionsAndFailIfAny(exceptions);
        }

        private void PrintExceptionsAndFailIfAny([NotNull]IReadOnlyCollection<Exception> exceptions)
        {
            foreach (var ex in exceptions)
            {
                Debug.WriteLine(ex.ThrowIfNull("ex").ToString());
                Debug.WriteLine("\n------\n");
            }
            if (exceptions.Any())
            {
                Assert.Fail();
            }
        }

        object Create([NotNull]ConstructorInfo ctor, [NotNull]IEnumerable<ConstructorArgument> args)
        {
            var expr = Expression.New(ctor, args.Select(a => Expression.Constant(a.ThrowIfNull("a").Value, a.ThrowIfNull("a").Type)));
            var lambda = Expression.Lambda<Func<object>>(expr);
            return lambda.Compile()();
        }

        private class ConstructorArgument
        {
            [NotNull]
            public Type Type { get; private set; }
            public object Value { get; set; }

            public ConstructorArgument([NotNull] Type type)
            {
                if (type == null) throw new ArgumentNullException("type");
                Type = type;
            }
        }
    }
}
