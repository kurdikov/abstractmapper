﻿namespace TsSoft.Expressions.Helpers.NestedGenerators
{
    using System;
    using TsSoft.Expressions.Models.AbstractMapper;

    class NestedGeneratorHelper : INestedGeneratorHelper
    {
        public T GenerateInContext<T, TRaw, TGenerator, TContext>(
            TGenerator generator, 
            Func<TGenerator, TRaw> generationWithoutContextFunc,
            Func<TGenerator, TContext, TRaw> generationFunc,
            Func<TRaw, T> processor,
            TContext context,
            T onExhaustedNesting)
            where T: class 
            where TContext: class, INestedGenerationContext
            where TGenerator: class, ICyclePreventingGenerator
        {
            T result;
            if (generator == null)
            {
                result = null;
            }
            else if (context == null)
            {
                result = generationWithoutContextFunc != null ? processor(generationWithoutContextFunc(generator)) : null;
            }
            else
            {
                int remainingNesting = context.RemainingNestingLevel.AddOrUpdate(
                    generator.GetType(),
                    generator.MaxNestingLevel,
                    (type, value) => value - 1);
                result = remainingNesting < 0
                    ? onExhaustedNesting
                    : processor(generationFunc(generator, context));
                context.RemainingNestingLevel.AddOrUpdate(
                    generator.GetType(), generator.MaxNestingLevel, (type, value) => value + 1);
            }
            return result;
        }
    }
}
