﻿namespace TsSoft.Expressions.Helpers.NestedGenerators
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Хелпер для построения объектов в контексте
    /// </summary>
    public interface INestedGeneratorHelper
    {
        /// <summary>
        /// Построить объект в контексте
        /// </summary>
        /// <typeparam name="T">Тип объекта для построения</typeparam>
        /// <typeparam name="TGenerator">Тип генератора</typeparam>
        /// <typeparam name="TContext">Тип контекста</typeparam>
        /// <typeparam name="TRaw">Тип сырого объекта, отдаваемого генератором</typeparam>
        /// <param name="generator">Генератор</param>
        /// <param name="generationWithoutContextFunc">Делегат для получения уже сгенерированного объекта</param>
        /// <param name="generationFunc">Делегат для генерации объекта в контексте</param>
        /// <param name="processor">Обработчик сырого объекта</param>
        /// <param name="context">Контекст</param>
        /// <param name="onExhaustedNesting">Объект, возвращаемый при исчерпанной вложенности</param>
        /// <returns>Построенный объект</returns>
        T GenerateInContext<T, TRaw, TGenerator, TContext>(
            [CanBeNull]TGenerator generator,
            [CanBeNull]Func<TGenerator, TRaw> generationWithoutContextFunc,
            [NotNull]Func<TGenerator, TContext, TRaw> generationFunc,
            [NotNull]Func<TRaw, T> processor,
            [CanBeNull]TContext context,
            T onExhaustedNesting)
            where T : class
            where TContext : class, INestedGenerationContext
            where TGenerator : class, ICyclePreventingGenerator;
    }
}
