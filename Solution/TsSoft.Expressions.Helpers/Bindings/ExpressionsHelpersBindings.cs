﻿namespace TsSoft.Bindings
{
    using TsSoft.BindingsDescription;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Closures;
    using TsSoft.Expressions.Helpers.Collections;
    using TsSoft.Expressions.Helpers.Differences;
    using TsSoft.Expressions.Helpers.DynamicTypes;
    using TsSoft.Expressions.Helpers.NestedGenerators;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.Expressions.Helpers
    /// </summary>
    public class ExpressionsHelpersBindings : BindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке TsSoft.Expressions.Helpers
        /// </summary>
        public ExpressionsHelpersBindings()
        {
            Bind<IExceptionWrapperHelper, ExceptionWrapperHelper>();
            Bind<IExpressionBuilder, ExpressionBuilder>();
            Bind<IExpressionCompositionHelper, ExpressionCompositionHelper>();
            Bind<IFlatPathParser, FlatPathParser>();
            Bind<INewExpressionHelper, NewExpressionHelper>();
            Bind<IPathParser, PathParser>();
            Bind<ICollectionHelper, CollectionHelper>();
            Bind<ICollectionReflectionHelper, CollectionReflectionHelper>();
            Bind<ICorrespondingMemberHelper, CorrespondingMemberHelper>();
            Bind<IMemberInfoHelper, MemberInfoHelper>();
            Bind<IFlatPathApplicator, FlatPathApplicator>();
            Bind<ITypeHelper, TypeHelper>();
            Bind<IObjectClonemaker, ObjectClonemaker>();
            Bind<IPathHelper, PathHelper>();
            Bind<IConvertRemover, ConvertRemover>();
            Bind<IMemberInfoLibrary, MemberInfoLibrary>();
            Bind<ITreeHelper, TreeHelper>();
            Bind<IDiffComputer, DiffComputer>();
            Bind<IUpdateObjectActionFactory, UpdateObjectActionFactory>();
            Bind<INestedGeneratorHelper, NestedGeneratorHelper>();
            Bind<IKeyExpressionBuilder, KeyExpressionBuilder>();
            Bind<IPathBuilder, PathBuilder>();
            Bind<ISpecialInnerLambdaCompiler, SpecialInnerLambdaCompiler>();
            Bind<IInnerLambdaCompiler, InnerLambdaCompiler>();
            Bind<ILambdaCompiler, LambdaCompiler>();
            Bind<IDelegateTypeHelper, DelegateTypeHelper>();
            Bind<IMethodBuilderLambdaCompiler, MethodBuilderLambdaCompiler>();
            Bind<IPropertyCreator, PropertyCreator>();
            Bind<IInterfaceImplementationCreator, InterfaceImplementationCreator>();
            Bind<IInterfaceImplementationProvider, InterfaceImplementationProvider>();
            Bind<IConversionChecker, ConversionChecker>();
            Bind<IMemberNamingHelper, MemberNamingHelper>();
        }
    }
}
