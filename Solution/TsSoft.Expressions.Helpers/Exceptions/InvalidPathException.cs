﻿namespace TsSoft.Expressions.Helpers.Exceptions
{
    using System;

    /// <summary>
    /// Неверное выражение пути
    /// </summary>
    public class InvalidPathException : Exception
    {
        /// <summary>
        /// Неверное выражение пути
        /// </summary>
        public InvalidPathException(string message)
            : base(message)
        {
        }
    }
}
