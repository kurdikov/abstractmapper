﻿namespace TsSoft.Expressions.Helpers.Exceptions
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Исключение внутри выражения маппинга
    /// </summary>
    public class InnerExpressionException : Exception
    {
        /// <summary>
        /// Выражение, в котором возникло исключение
        /// </summary>
        public Expression ErrorExpression { get; private set; }

        [NotNull]
        private static readonly Func<Expression, string> Stringifier;

        [NotNull]
        static Func<Expression, string> GetStringifier()
        {
            var debugViewWriterType = typeof(Expression).GetTypeInfo().Assembly.GetType("System.Linq.Expressions.DebugViewWriter");
            if (debugViewWriterType == null)
            {
                return expression => expression != null ? expression.ToString() : null;
            }
            var method = debugViewWriterType.GetTypeInfo().GetMethods(BindingFlags.Static | BindingFlags.NonPublic)
                .FirstOrDefault(x => x != null && x.Name == "WriteTo");
            if (method == null)
            {
                return expression => expression != null ? expression.ToString() : null;
            }
            var methodDelegate = (Action<Expression, StringWriter>)method.CreateDelegate(typeof(Action<Expression, StringWriter>));
            return expression =>
            {
                using (var stringWriter = new StringWriter(CultureInfo.CurrentCulture))
                {
                    methodDelegate(expression, stringWriter);
                    return stringWriter.ToString();
                }
            };
        }

        static InnerExpressionException()
        {
            Stringifier = GetStringifier();
        }

        private static string DebugView([NotNull]Expression expression)
        {
            return Stringifier(expression);
        }

        /// <summary>
        /// Сообщение
        /// </summary>
        public override string Message
        {
            get
            {
                return string.Format("{0} \r\n Expression: \r\n {1} \r\n", base.Message, ErrorExpression != null ? DebugView(ErrorExpression) : "NULL");
            }
        }

        /// <summary>
        /// Исключение внутри выражения маппинга
        /// </summary>
        /// <param name="errorExpression">Выражение, внутри которого возникло исключение</param>
        /// <param name="message">Сообщение</param>
        /// <param name="innerException">Исключение</param>
        public InnerExpressionException(Expression errorExpression, string message, Exception innerException)
            : base(message, innerException)
        {
            ErrorExpression = errorExpression;
        }
    }
}
