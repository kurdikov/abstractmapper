﻿namespace TsSoft.Expressions.Helpers.Exceptions
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Исключение, возникающее при невозможности построить обновляющий делегат
    /// </summary>
    public class UnableToBuildActionException : Exception
    {
        /// <summary>
        /// Тип внутреннего объекта, который не удалось создать
        /// </summary>
        public Type InvalidType { get; private set; }
        /// <summary>
        /// Создавался ли объект внутри вложенной коллекции или как отдельный вложенный объект
        /// </summary>
        public bool IsCollectionElement { get; private set; }
        /// <summary>
        /// Удалось ли установить тип объекта через <see cref="TsSoft.Expressions.Models.Reflection.IObjectUpdateManager"/>
        /// </summary>
        public bool IsKnown { get; private set; }

        /// <summary>
        /// Исключение, возникающее при невозможности создания внутреннего объекта для обновления контейнера
        /// </summary>
        public UnableToBuildActionException([NotNull]Type invalidType, bool isCollectionElement, bool isKnown)
            : base(GetMessage(invalidType, isCollectionElement, isKnown))
        {
            if (invalidType == null) throw new ArgumentNullException("invalidType");
            InvalidType = invalidType;
            IsCollectionElement = isCollectionElement;
            IsKnown = isKnown;
        }

        private static string GetMessage(Type invalidType, bool isCollectionElement, bool isKnown)
        {
            if (isKnown)
            {
                return string.Format("Type {0} is recognized as a primitive but has no default constructor", invalidType);
            }
            else
            {
                return string.Format("Type {0} is not a primitive, but is not recognized as a known complex type or a collection with known complex element", invalidType);
            }
        }
    }
}
