﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Обрабатывает выражения присваивания значений свойствам/полям объекта
    /// </summary>
    public interface INewExpressionHelper
    {
        /// <summary>
        /// Получить присваиваемые значения из выражения e => new {...} или e => new X {...}
        /// </summary>
        [NotNull]
        IEnumerable<KeyValuePair<MemberInfo, Expression>> GetAssignments(LambdaExpression selectExpression);

        /// <summary>
        /// Получить присваиваемые значения из выражения new X {...}
        /// </summary>
        [NotNull]
        IEnumerable<KeyValuePair<MemberInfo, Expression>> GetAssignments([NotNull]MemberInitExpression selectExpression);

        /// <summary>
        /// Получить присваиваемые значения из выражения new X {...}
        /// </summary>
        [NotNull]
        IEnumerable<KeyValuePair<MemberInfo, Expression>> GetAssignments([NotNull]NewExpression selectExpression);

        /// <summary>
        /// Разобрать выражение присваивания
        /// </summary>
        [NotNull]
        SelectMemberAssignment ParseAssignment([NotNull]MemberAssignment binding, [NotNull]Type entityType);

        /// <summary>
        /// Разобрать выражение присваивания
        /// </summary>
        [NotNull]
        SelectMemberAssignment ParseAssignment(KeyValuePair<MemberInfo, Expression> binding, [NotNull]Type entityType);

        /// <summary>
        /// Создать выражение присваивания
        /// </summary>
        /// <param name="assignment">Присваивание как пара (свойство, значение)</param>
        /// <param name="destinationType">Тип, свойству которого присваивается значение</param>
        [NotNull]
        MemberAssignment ComposeAssignment(KeyValuePair<MemberInfo, Expression> assignment, [NotNull]Type destinationType);

        /// <summary>
        /// Разобрать выражение присваивания на пару (свойство, значение)
        /// </summary>
        KeyValuePair<MemberInfo, Expression> DecomposeAssignment([NotNull]MemberAssignment assignment);
    }
}
