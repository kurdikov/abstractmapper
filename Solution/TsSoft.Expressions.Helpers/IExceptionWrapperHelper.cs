﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Построитель выражения для ловли и оборачивания исключений
    /// </summary>
    public interface IExceptionWrapperHelper
    {
        /// <summary>
        /// Построить выражение, ловящее и оборачивающее исключение
        /// </summary>
        /// <param name="inner">Исходное выражение</param>
        /// <param name="caughtExceptionType">Тип отлавливаемых исключений</param>
        /// <param name="message">Сообщение исключения-обёртки</param>
        /// <returns>Обёрнутое выражение</returns>
        [NotNull]
        TryExpression Wrap(Expression inner, Type caughtExceptionType = null, string message = null);
    }

}
