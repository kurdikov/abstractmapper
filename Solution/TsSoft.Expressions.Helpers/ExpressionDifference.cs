﻿using System.Linq.Expressions;

namespace TsSoft.Expressions.Helpers
{
    /// <summary>
    /// Различие в выражениях
    /// </summary>
    public class ExpressionDifference
    {
        /// <summary>
        /// Первое подвыражение, в котором найдено различие
        /// </summary>
        public Expression First { get; private set; }
        /// <summary>
        /// Второе подвыражение, в котором найдено различие
        /// </summary>
        public Expression Second { get; private set; }
        /// <summary>
        /// Описание различия
        /// </summary>
        public string Reason { get; private set; }

        /// <summary>
        /// Различие в выражениях
        /// </summary>
        public ExpressionDifference(
            Expression first,
            Expression second,
            string reason)
        {
            First = first;
            Second = second;
            Reason = reason;
        }

        /// <summary>
        /// Строковое представление
        /// </summary>
        public override string ToString()
        {
            return string.Format("Expressions '{0}' and '{1}' are different. Reason: {2}", First, Second, Reason);
        }
    }

    /// <summary>
    /// Различие в выражениях
    /// </summary>
    public class SpecificExpressionDifference<T> : ExpressionDifference
    {
        /// <summary>
        /// Отличающийся элемент первого выражения
        /// </summary>
        public T FirstElement { get; private set; }
        /// <summary>
        /// Отличающийся элемент второго выражения
        /// </summary>
        public T SecondElement { get; private set; }

        /// <summary>
        /// Различие в выражениях
        /// </summary>
        public SpecificExpressionDifference(
            Expression first,
            Expression second,
            T firstElement,
            T secondElement,
            string reason)
            : base(first, second, reason)
        {
            FirstElement = firstElement;
            SecondElement = secondElement;
        }

        /// <summary>
        /// Строковое представление
        /// </summary>
        public override string ToString()
        {
            return string.Format("Elements '{0}' and '{1}' of expressions '{2}' and '{3}' are different. Reason: {4}", FirstElement, SecondElement, First, Second, Reason);
        }
    }

    /// <summary>
    /// Различие в выражениях
    /// </summary>
    public class SpecificExpressionDifferenceWithMatch<T> : SpecificExpressionDifference<T>
    {
        /// <summary>
        /// Элемент, соответствующий элементу первого выражения во втором выражении
        /// </summary>
        public T Match { get; private set; }

        /// <summary>
        /// Различие в выражениях
        /// </summary>
        public SpecificExpressionDifferenceWithMatch(
            Expression first,
            Expression second,
            T firstElement,
            T secondElement,
            T match,
            string reason)
            : base(first, second, firstElement, secondElement, reason)
        {
            Match = match;
        }

        /// <summary>
        /// Строковое представление
        /// </summary>
        public override string ToString()
        {
            return base.ToString() + string.Format(". '{0}' expected instead of '{1}'", Match, SecondElement);
        }
    }
}
