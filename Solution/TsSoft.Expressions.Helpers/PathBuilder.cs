namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// ����������� ���� � ������
    /// </summary>
    public class PathBuilder : IPathBuilder
    {
        [NotNull] private readonly IMemberInfoLibrary _library;

        /// <summary>
        /// ����������� ���� � ������
        /// </summary>
        public PathBuilder([NotNull] IMemberInfoLibrary library)
        {
            if (library == null) throw new ArgumentNullException("library");
            _library = library;
        }

        /// <summary>
        /// ������������� ����������� ������� ���� � ������
        /// </summary>
        public LambdaExpression Build(IEnumerable<ValueHoldingMember> flatPath)
        {
            using (var enumerator = flatPath.GetEnumerator())
            {
                if (!enumerator.MoveNext())
                {
                    throw new InvalidPathException("Path has no elements");
                }
                if (enumerator.Current == null)
                {
                    throw new InvalidPathException("Path contains null first element");
                }
                var parameter = Expression.Parameter(enumerator.Current.DeclaringType);
                var path = new List<PathElement> {new PathElement(enumerator.Current)};
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current == null)
                    {
                        throw new InvalidPathException("Path contains null element");
                    }
                    path.Add(new PathElement(enumerator.Current));
                }
                return Build(new ParsedPath(path, parameter));
            }
        }

        /// <summary>
        /// ������������� ����������� ������� ���� � ������
        /// </summary>
        public LambdaExpression Build(IEnumerable<PropertyInfo> flatPath)
        {
            return Build(flatPath.Select(pi => new ValueHoldingMember(pi)));
        }

        /// <summary>
        /// ������������� ����������� ���� � ������
        /// </summary>
        public LambdaExpression Build(ParsedPath path)
        {
            Expression prefix = path.Start;
            Type currentSingleType = path.Start.Type;
            bool isCollection = false;
            foreach (var element in path.Elements)
            {
                if (element == null)
                {
                    throw new InvalidPathException("Path contains null element");
                }
                if (!isCollection)
                {
                    prefix = Expression.MakeMemberAccess(prefix, element.Step.Member);
                    currentSingleType = prefix.Type.GetGenericEnumerableArgumentOrSelf();
                    isCollection = currentSingleType != prefix.Type;
                }
                else
                {
                    var nextSingleType = element.Step.ValueType.GetGenericEnumerableArgumentOrSelf();
                    var method = (nextSingleType != element.Step.ValueType)
                        ? _library.EnumerableSelectMany(currentSingleType, nextSingleType)
                        : _library.EnumerableSelect(currentSingleType, nextSingleType);
                    var innerParam = Expression.Parameter(currentSingleType);
                    prefix = Expression.Call(
                        method,
                        prefix,
                        Expression.Lambda(
                            typeof(Func<,>).MakeGenericType(currentSingleType, element.Step.ValueType),
                            Expression.MakeMemberAccess(innerParam, element.Step.Member), innerParam));
                    currentSingleType = nextSingleType;
                }

                if (element.Conditions != null)
                {
                    if (!isCollection)
                    {
                        throw new InvalidPathException("Condition on non-collection");
                    }
                    var method = _library.EnumerableWhere(currentSingleType);
                    foreach (var condition in element.Conditions)
                    {
                        if (condition == null)
                        {
                            continue;
                        }
                        if (condition.DbCondition == null)
                        {
                            throw new InvalidPathException("Condition with null DbCondition");
                        }
                        prefix = Expression.Call(method, prefix, condition.DbCondition);
                    }
                }
            }
            return Expression.Lambda(
                typeof(Func<,>).MakeGenericType(path.Start.Type, prefix.Type),
                prefix,
                path.Start);
        }
    }
}
