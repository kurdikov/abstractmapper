﻿namespace TsSoft.Expressions.Helpers.Extensions
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Расширения для IEnumerator
    /// </summary>
    public static class EnumeratorExtensions
    {
        /// <summary>
        /// Создать List из IEnumerator-а (без текущего элемента, если сдвиг уже произошёл)
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        [NotNull]
        public static List<T> ToList<T>([NotNull]this IEnumerator<T> enumerator)
        {
            var result = new List<T>();
            while (enumerator.MoveNext())
            {
                result.Add(enumerator.Current);
            }
            return result;
        }

        /// <summary>
        /// Создать IReadOnlyCollection из IEnumerator-а (без текущего элемента, если сдвиг уже произошёл)
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        [NotNull]
        public static IReadOnlyCollection<T> ToReadOnlyCollection<T>([NotNull]this IEnumerator<T> enumerator)
        {
            return enumerator.ToList();
        }

        /// <summary>
        /// Создать List из IEnumerator-а (вместе с текущим элементом)
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        [NotNull]
        public static List<T> ToListWithCurrent<T>([NotNull]this IEnumerator<T> enumerator)
        {
            var result = new List<T>();
            do
            {
                result.Add(enumerator.Current);
            } while (enumerator.MoveNext());
            return result;
        }

        /// <summary>
        /// Создать IReadOnlyCollection из IEnumerator-а (вместе с текущим элементом)
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        [NotNull]
        public static IReadOnlyCollection<T> ToReadOnlyCollectionWithCurrent<T>([NotNull]this IEnumerator<T> enumerator)
        {
            return enumerator.ToListWithCurrent();
        }
    }
}
