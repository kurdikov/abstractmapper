﻿namespace TsSoft.Expressions.Helpers.Extensions
{
    using System;
    using System.Reflection.Emit;
    using JetBrains.Annotations;

    /// <summary>
    /// Расширения генератора IL
    /// </summary>
    public static class IlGeneratorExtensions
    {
        /// <summary>
        /// Загрузить на стек аргумент
        /// </summary>
        /// <param name="generator">Генератор</param>
        /// <param name="argIndex">Номер аргумента</param>
        public static void EmitLoadArg([NotNull]this ILGenerator generator, int argIndex)
        {
            if (argIndex < 0 || argIndex > short.MaxValue)
            {
                throw new ArgumentOutOfRangeException("argIndex");
            }
            if (argIndex == 0)
            {
                generator.Emit(OpCodes.Ldarg_0);
            }
            else if (argIndex == 1)
            {
                generator.Emit(OpCodes.Ldarg_1);
            }
            else if (argIndex == 2)
            {
                generator.Emit(OpCodes.Ldarg_2);
            }
            else if (argIndex == 3)
            {
                generator.Emit(OpCodes.Ldarg_3);
            }
            else if (argIndex < 255)
            {
                generator.Emit(OpCodes.Ldarg_S, (byte)argIndex);
            }
            else
            {
                generator.Emit(OpCodes.Ldarg, (short)argIndex);
            }
        }
    }
}
