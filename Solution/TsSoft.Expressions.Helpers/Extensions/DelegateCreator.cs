﻿namespace TsSoft.Expressions.Helpers.Extensions
{
    using System;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Создаёт делегаты
    /// </summary>
    public static class DelegateCreator
    {
        /// <summary>
        /// Создать делегат по объекту и его методу
        /// </summary>
        /// <typeparam name="T">Тип делегата</typeparam>
        /// <param name="firstArgument">this</param>
        /// <param name="method">Метод</param>
        /// <returns>Делегат</returns>
        [NotNull]
        public static T Create<T>(object firstArgument, [NotNull]MethodInfo method)
        {
            return method.CreateDelegate(typeof(T), firstArgument).As<T>();
        }
    }
}
