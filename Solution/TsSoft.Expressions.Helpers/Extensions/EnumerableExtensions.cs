﻿namespace TsSoft.Expressions.Helpers.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;

    /// <summary>
    /// Расширения перечислений
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Привести перечисление к IReadOnlyCollection, материализовав его при необходимости
        /// </summary>
        [NotNull]
        public static IReadOnlyCollection<T> ToReadOnlyCollectionIfNeeded<T>(this IEnumerable<T> elem)
        {
            return (elem as IReadOnlyCollection<T>) ?? (elem != null ? elem.ToList() : (IReadOnlyCollection<T>)new T[0]);
        }

        /// <summary>
        /// Привести перечисление к IReadOnlyList, материализовав его при необходимости
        /// </summary>
        [NotNull]
        public static IReadOnlyList<T> ToReadOnlyListIfNeeded<T>(this IEnumerable<T> elem)
        {
            return (elem as IReadOnlyList<T>) ?? (elem != null ? elem.ToList() : (IReadOnlyList<T>)new T[0]);
        }

        /// <summary>
        /// Привести перечисление к массиву, преобразовав его при необходимости
        /// </summary>
        [NotNull]
        public static T[] ToArrayIfNeeded<T>(this IEnumerable<T> elem)
        {
            return (elem as T[]) ?? (elem != null ? elem.ToArray() : new T[0]);
        }

        /// <summary>
        /// Заменить null на пустое перечисление
        /// </summary>
        [NotNull]
        public static IEnumerable<T> NotNull<T>(this IEnumerable<T> elem)
        {
            return elem ?? Enumerable.Empty<T>();
        }

        /// <summary>
        /// Разбить перечисление на перечисление массивов фиксированного размера
        /// </summary>
        /// <typeparam name="T">Тип перечисляемого объекта</typeparam>
        /// <param name="source">Перечисление</param>
        /// <param name="size">Размер массивов</param>
        [NotNull]
        public static IEnumerable<T[]> ArrayBatch<T>(
            [NotNull]this IEnumerable<T> source, int size)
        {
            T[] bucket = null;
            var count = 0;

            foreach (var item in source)
            {
                if (bucket == null)
                {
                    bucket = new T[size];
                }

                bucket[count++] = item;

                if (count != size)
                {
                    continue;
                }

                yield return bucket;

                bucket = null;
                count = 0;
            }

            if (bucket != null && count > 0)
            {
                yield return bucket.Take(count).ToArray();
            }
        }

        /// <summary>
        /// Выполнить действие после перебора всех элементов перечисления
        /// </summary>
        /// <typeparam name="T">Тип элементов перечисления</typeparam>
        /// <param name="enumerable">Перечисление</param>
        /// <param name="action">Действие</param>
        [NotNull]
        public static IEnumerable<T> AfterEnumeration<T>([NotNull]this IEnumerable<T> enumerable, [NotNull]Action action)
        {
            foreach (var elem in enumerable)
            {
                yield return elem;
            }
            action();
        }

        /// <summary>
        /// Добавить элемент в перечисление
        /// </summary>
        /// <typeparam name="T">Тип элементов перечисления</typeparam>
        /// <param name="enumerable">Перечисление</param>
        /// <param name="value">Добавляемый элемент</param>
        [NotNull]
        public static IEnumerable<T> AppendOne<T>([NotNull] this IEnumerable<T> enumerable, T value)
        {
            foreach (var elem in enumerable)
            {
                yield return elem;
            }
            yield return value;
        }

        /// <summary>
        /// Возвращает первое значение перечисления, если оно единственное, default(T) в противном случае
        /// </summary>
        /// <typeparam name="T">Тип элементов перечисления</typeparam>
        /// <param name="enumerable">Перечисление</param>
        [CanBeNull]
        public static T FirstIfSingleOtherwiseDefault<T>([NotNull] this IEnumerable<T> enumerable)
        {
            using (var enumerator = enumerable.GetEnumerator())
            {
                if (!enumerator.MoveNext())
                {
                    return default(T);
                }
                var result = enumerator.Current;
                return !enumerator.MoveNext() ? result : default(T);
            }
        }
    }
}
