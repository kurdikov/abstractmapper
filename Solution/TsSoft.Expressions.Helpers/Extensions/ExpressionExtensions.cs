﻿namespace TsSoft.Expressions.Helpers.Extensions
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Расширения для лямбда-выражений
    /// </summary>
    public static class ExpressionExtensions
    {
        /// <summary>
        /// Получить параметр по индексу
        /// </summary>
        [NotNull]
        public static ParameterExpression ParameterAt([NotNull]this LambdaExpression expression, int index)
        {
            if(expression == null){throw new ArgumentNullException("expression");}
            if (expression.Parameters.Count <= index)
            {
                throw new ArgumentOutOfRangeException("index", index, string.Format("Lambda {0} has {1} parameters", expression, expression.Parameters.Count));
            }
            var result = expression.Parameters[index];
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Null parameter in lambda {0}", expression));
            }
            return result;
        }

        /// <summary>
        /// Получить аргумент метода, являющийся лямбда-выражением
        /// </summary>
        [NotNull]
        public static LambdaExpression LambdaArgument([NotNull]this MethodCallExpression expression, int index)
        {
            var argument = expression.Arguments[index];
            var lambdaArgument = argument as LambdaExpression;
            if (lambdaArgument == null)
            {
                throw new InvalidOperationException(string.Format("Unexpected null lambda argument in {0}", expression));
            }
            return lambdaArgument;
        }
    }
}
