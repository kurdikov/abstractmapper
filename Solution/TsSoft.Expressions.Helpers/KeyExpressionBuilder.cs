﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Tuples;

    /// <summary>
    /// Построитель выражений с ключами
    /// </summary>
    public class KeyExpressionBuilder : ExpressionBuilder, IKeyExpressionBuilder
    {

        [NotNull]
        [ItemNotNull]
        private readonly static Type[] TupleTypes =
        {
            typeof(void),
            typeof(EquatableTuple<>),
            typeof(EquatableTuple<,>),
            typeof(EquatableTuple<,,>),
            typeof(EquatableTuple<,,,>),
            typeof(EquatableTuple<,,,,>),
            typeof(EquatableTuple<,,,,,>),
            typeof(EquatableTuple<,,,,,,>),
            typeof(EquatableTuple<,,,,,,,>),
        };

        /// <summary>
        /// Построитель выражений с ключами
        /// </summary>
        public KeyExpressionBuilder([NotNull] IMemberInfoHelper memberInfoHelper, [NotNull] IMemberInfoLibrary memberInfoLibrary)
            : base(memberInfoHelper, memberInfoLibrary)
        {
        }

        /// <summary>
        /// Получить тип ключа, который можно выделить
        /// </summary>
        public Type GetKeyType(IReadOnlyList<ValueHoldingMember> key)
        {
            return key.Count == 1
                ? key[0].ValueType
                : TupleTypes[key.Count].MakeGenericType(key.Select(k => k.ValueType).ToArray());
        }

        /// <summary>
        /// Получить тип ключа, который можно выделить
        /// </summary>
        public Type GetKeyType(IReadOnlyList<PropertyInfo> key)
        {
            return key.Count == 1
                ? key[0].PropertyType
                : TupleTypes[key.Count].MakeGenericType(key.Select(k => k.PropertyType).ToArray());
        }

        /// <summary>
        /// Создать выделитель ключа из объекта
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <param name="key">Ключ</param>
        /// <param name="replaceNullsWithDefaults">Заменить nullable-типы на не-nullable, подставляя значение по умолчанию вместо null</param>
        public Expression CreateExtractKeyExpression(Expression obj, IReadOnlyList<ValueHoldingMember> key, bool replaceNullsWithDefaults = false)
        {
            if (key.Count > 7)
            {
                throw new NotSupportedException("Keys with more than 7 columns are not supported");
            }
            if (key.Count < 1)
            {
                throw new ArgumentException("No key");
            }
            if (key.Count == 1)
            {
                if (key[0] == null)
                {
                    throw new ArgumentException("key[0] is null");
                }
                return Expression.MakeMemberAccess(obj, key[0].Member);
            }
            var typeDefinition = TupleTypes[key.Count];
            var keyTypes = new Type[key.Count];
            var keyExprs = new Expression[key.Count];
            for (int i = 0; i < key.Count; ++i)
            {
                if (key[i] == null)
                {
                    throw new ArgumentException(string.Format("key[{0}] is null", i));
                }
                var replaceThis = replaceNullsWithDefaults && key[i].ValueType.IsNullableStruct();
                keyTypes[i] = replaceNullsWithDefaults && replaceThis
                    ? key[i].ValueType.NullableStructUnderlyingType()
                    : key[i].ValueType;
                keyExprs[i] = replaceThis
                    ? Expression.Coalesce(
                        Expression.MakeMemberAccess(obj, key[i].Member),
                        Expression.Default(keyTypes[i]))
                    : (Expression)Expression.MakeMemberAccess(obj, key[i].Member);
            }
            var type = typeDefinition.MakeGenericType(keyTypes);
            var constructor = type.GetTypeInfo().GetConstructor(keyTypes);
            if (constructor == null)
            {
                throw new InvalidOperationException(
                    string.Format(
                        "Constructor of {0} with {1} not found", type,
                        string.Join(",", keyTypes.Select(t => t != null ? t.ToString() : "NULL"))));
            }
            return Expression.New(constructor, keyExprs);
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <typeparam name="TValue">Тип композитного ключа</typeparam>
        /// <param name="key">Ключ</param>
        /// <param name="values">Значения</param>
        /// <returns>e => (e.Property1 == values1.Item1 AND e.Property2 = values1.Item2) || (e.Property1 == values2.Item1 AND e.Property2 = values2.Item2) || ...</returns>
        public Expression<Func<TEntity, bool>> BuildWhereIn<TEntity, TValue>(IReadOnlyList<ValueHoldingMember> key, IEnumerable<TValue> values)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            if (key.Count == 1 && !typeof(IEquatableTuple).GetTypeInfo().IsAssignableFrom(typeof(TValue)))
            {
                return BuildWhereIn<TEntity, TValue>(key[0], values);
            }
            if (!typeof(IEquatableTuple).GetTypeInfo().IsAssignableFrom(typeof(TValue)))
            {
                throw new ArgumentException(string.Format("Type {0} is not supported for composite keys", typeof(TValue)));
            }
            var param = Expression.Parameter(typeof(TEntity), "p");
            var valueProps = new PropertyInfo[key.Count];
            for (int i = 0; i < key.Count; ++i)
            {
                valueProps[i] = EquatableTuple.GetItemProperty(typeof(TValue), i);
            }
            var result = (values ?? Enumerable.Empty<TValue>()).Aggregate(
                (Expression)null,
                (current, value) => current != null
                    ? Expression.OrElse(
                        current,
                        CompositeKeyEquals(param, key, value, valueProps))
                    : CompositeKeyEquals(param, key, value, valueProps));
            result = result ?? Expression.Constant(false, typeof(bool));
            return Expression.Lambda<Func<TEntity, bool>>(result, param);
        }

        private Expression CompositeKeyEquals<TValue>(
            [NotNull]ParameterExpression param,
            [NotNull]IReadOnlyList<ValueHoldingMember> keyProperties, 
            [NotNull]TValue value,
            [NotNull]IReadOnlyList<PropertyInfo> valueProperties)
        {
            var valueExpr = Expression.Constant(value, typeof(TValue));
            var result = Expression.Equal(
                Expression.MakeMemberAccess(param, keyProperties[0].Property),
                Expression.Property(valueExpr, valueProperties[0]));
            for (int i = 1; i < keyProperties.Count; ++i)
            {
                result = Expression.AndAlso(
                    result,
                    Expression.Equal(
                        Expression.MakeMemberAccess(param, keyProperties[i].Property),
                        Expression.Property(valueExpr, valueProperties[i])));
            }
            return result;
        }
    }
}
