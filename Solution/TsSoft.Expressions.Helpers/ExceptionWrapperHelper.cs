﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Reflection;

    internal class ExceptionWrapperHelper : IExceptionWrapperHelper
    {
        [NotNull] private readonly ConstructorInfo _exceptionCtor;

        public ExceptionWrapperHelper(
            [NotNull]IMemberInfoHelper memberInfoHelper)
        {
            _exceptionCtor = memberInfoHelper.GetConstructorInfo(() => new InnerExpressionException((Expression)null, (string)null, (Exception)null));
        }

        public TryExpression Wrap(Expression inner, Type caughtExceptionType = null, string message = null)
        {
            if (inner == null)
            {
                throw new NullReferenceException("Expression can not be null");
            }
            caughtExceptionType = caughtExceptionType ?? typeof(Exception);
            var exceptionParameter = Expression.Parameter(caughtExceptionType, "innerException");
            var outerException = Expression.New(
                _exceptionCtor,
                Expression.Constant(inner, typeof(Expression)),
                Expression.Constant(message, typeof(string)),
                exceptionParameter);
            var throwExpression = Expression.Throw(outerException);
            return Expression.TryCatch(
                Expression.Block(typeof(void), inner),
                Expression.Catch(exceptionParameter, Expression.Block(typeof(void), throwExpression)));
        }
    }
}
