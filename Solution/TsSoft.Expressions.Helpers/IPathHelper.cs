﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Хелпер для работы с путями
    /// </summary>
    public interface IPathHelper
    {
        /// <summary>
        /// Получить тип выхода пути (или тип элемента коллекции выхода пути, если выход - коллекция)
        /// </summary>
        /// <param name="startType">Тип входа пути</param>
        /// <param name="path">Путь</param>
        /// <returns>Тип выхода</returns>
        [NotNull]
        Type GetPathSingleType([NotNull]Type startType, [NotNull]IReadOnlyList<ValueHoldingMember> path);

        /// <summary>
        /// Получить тип выхода пути (или тип элемента коллекции выхода пути, если выход - коллекция)
        /// </summary>
        /// <param name="startType">Тип входа пути</param>
        /// <param name="path">Путь</param>
        /// <returns>Тип выхода</returns>
        [NotNull]
        Type GetPathSingleType([NotNull]Type startType, [NotNull]IReadOnlyList<PropertyInfo> path);

        /// <summary>
        /// Получить тип выхода пути (или тип элемента коллекции выхода пути, если выход - коллекция)
        /// </summary>
        /// <param name="path">Путь</param>
        /// <returns>Тип выхода</returns>
        [NotNull]
        Type GetPathSingleType([NotNull]ParsedPath path);
    }
}
