﻿namespace TsSoft.Expressions.Helpers.Closures
{
    /// <summary>
    /// Замыкание
    /// </summary>
    public class Closure
    {
        /// <summary>
        /// Объекты в замыкании
        /// </summary>
        public object[] Objects;
    }
}
