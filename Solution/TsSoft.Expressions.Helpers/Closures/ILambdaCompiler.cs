﻿namespace TsSoft.Expressions.Helpers.Closures
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Компилирует лямбда-выражения, предварительно компилируя внутренние лямбды
    /// </summary>
    public interface ILambdaCompiler
    {
        /// <summary>
        /// Скомпилировать лямбда-выражение
        /// </summary>
        /// <typeparam name="T">Тип делегата</typeparam>
        /// <param name="lambda">Лямбда-выражение</param>
        /// <param name="compilationType">Настройки компиляции</param>
        /// <returns>Делегат</returns>
        [NotNull]
        T Compile<T>([NotNull]Expression<T> lambda, LambdaCompilationType compilationType = LambdaCompilationType.PrecompileInner);

        /// <summary>
        /// Скомпилировать лямбда-выражение
        /// </summary>
        /// <param name="lambda">Лямбда-выражение</param>
        /// <param name="compilationType">Настройки компиляции</param>
        /// <returns>Делегат</returns>
        [NotNull]
        Delegate Compile([NotNull]LambdaExpression lambda, LambdaCompilationType compilationType = LambdaCompilationType.PrecompileInner);
    }
}
