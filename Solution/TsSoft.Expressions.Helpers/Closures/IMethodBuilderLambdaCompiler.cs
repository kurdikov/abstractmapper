﻿namespace TsSoft.Expressions.Helpers.Closures
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Компилирует лямбду в метод в динамической сборке
    /// </summary>
    public interface IMethodBuilderLambdaCompiler
    {
        /// <summary>
        /// Компилировать лямбду в метод в динамической сборке
        /// </summary>
        /// <typeparam name="T">Тип делегата лямбды</typeparam>
        /// <param name="lambda">Лямбда-выражение</param>
        [NotNull]
        T Compile<T>([NotNull]Expression<T> lambda);

        /// <summary>
        /// Компилировать лямбду в метод в динамической сборке
        /// </summary>
        /// <param name="lambda">Лямбда-выражение</param>
        [NotNull]
        Delegate Compile([NotNull]LambdaExpression lambda);
    }
}
