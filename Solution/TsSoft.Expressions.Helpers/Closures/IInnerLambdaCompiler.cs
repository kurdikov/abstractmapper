﻿namespace TsSoft.Expressions.Helpers.Closures
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Превращает выражение с внутренней лямбдой в выражение со скомпилированным делегатом
    /// во избежание медленных вызовов Delegate.CreateDelegate, появляющихся при компиляции лямбды с внутренними лямбдами
    /// </summary>
    public interface IInnerLambdaCompiler
    {
        /// <summary>
        /// Сгенерировать выражение со скомпилированным делегатом из лямбды
        /// </summary>
        /// <param name="lambda">Дерево лямбда-выражения</param>
        /// <param name="openParams">Несобственные параметры лямбды</param>
        /// <returns>Выражение того же типа, что исходная лямбда</returns>
        [NotNull]
        Expression GenerateExpressionWithCompiledLambda(
            [NotNull] LambdaExpression lambda,
            [NotNull] ISet<ParameterExpression> openParams);
    }
}
