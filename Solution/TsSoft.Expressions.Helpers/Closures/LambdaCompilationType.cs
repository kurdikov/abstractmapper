﻿namespace TsSoft.Expressions.Helpers.Closures
{
    using System.Reflection.Emit;

    /// <summary>
    /// Настройки компиляции лямбды
    /// </summary>
    public enum LambdaCompilationType
    {
        /// <summary>
        /// Компилировать внутренние лямбды перед компиляцией внешней
        /// </summary>
        PrecompileInner,
        /// <summary>
        /// Компилировать внутренние лямбды перед компиляцией внешней для фиксированного набора типов делегатов
        /// </summary>
        PrecompileInnerSpecial,
        /// <summary>
        /// Компилировать внутренние лямбды, в теле которых не встречается чужих параметров
        /// </summary>
        PrecompileInnerWithoutOpenParams,
        /// <summary>
        /// Не компилировать внутренние лямбды перед внешней
        /// </summary>
        DoNotPrecompile,
        /// <summary>
        /// Компилировать в <see cref="MethodBuilder"/> вместо <see cref="DynamicMethod"/>
        /// </summary>
        CompileToMethod,
    }
}
