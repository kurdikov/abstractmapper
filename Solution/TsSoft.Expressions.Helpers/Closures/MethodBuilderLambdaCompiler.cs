﻿namespace TsSoft.Expressions.Helpers.Closures
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Reflection.Emit;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.DynamicTypes;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Helpers.Visitors;

    /// <summary>
    /// Компилирует лямбду в метод в динамической сборке
    /// </summary>
    public class MethodBuilderLambdaCompiler : AssemblyBuilderContainer, IMethodBuilderLambdaCompiler
    {
        [NotNull]private readonly ConstructorInfo _objectConstructor;

        private const string AssemblyName = "TsSoft.MethodBuilderLambdaCompiler";

        /// <summary>
        /// Компилирует лямбду в метод в динамической сборке
        /// </summary>
        public MethodBuilderLambdaCompiler([NotNull]IMemberInfoHelper memberInfoHelper)
            : base(AssemblyName)
        {
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            _objectConstructor = memberInfoHelper.GetConstructorInfo(() => new object());
        }

        [NotNull]
        private string GetContextFieldName(int index)
        {
            return "Element" + index;
        }

        /// <summary>
        /// Компилировать лямбду в метод в динамической сборке
        /// </summary>
        /// <typeparam name="T">Тип делегата лямбды</typeparam>
        /// <param name="lambda">Лямбда-выражение</param>
        public T Compile<T>(Expression<T> lambda)
        {
            return Compile((LambdaExpression)lambda).As<T>();
        }

        /// <summary>
        /// Компилировать лямбду в метод в динамической сборке
        /// </summary>
        /// <param name="lambda">Лямбда-выражение</param>
        public Delegate Compile(LambdaExpression lambda)
        {
#if NET45
            var id = Guid.NewGuid().ToString("N");
            var consts = ConstantExpressionFinderVisitor.GetConstants(lambda).ToArray();
            var constTypes = new Type[consts.Length];
            var lambdaContextTypeBuilder = ModuleBuilder.DefineType("Context_" + id, PublicClass);
            var lambdaContextTypeFields = new FieldInfo[consts.Length];
            for (int i = 0; i < consts.Length; ++i)
            {
                constTypes[i] = consts[i].ThrowIfNull("consts[i]").GetType();
                lambdaContextTypeFields[i] = lambdaContextTypeBuilder.DefineField(GetContextFieldName(i), constTypes[i], FieldAttributes.Public);
            }
            var lambdaContextTypeCtor = lambdaContextTypeBuilder.DefineConstructor(
                MethodAttributes.Public, CallingConventions.Standard, constTypes);
            var lambdaContextTypeCtorGenerator = lambdaContextTypeCtor.GetILGenerator();
            lambdaContextTypeCtorGenerator.Emit(OpCodes.Ldarg_0);
            lambdaContextTypeCtorGenerator.Emit(OpCodes.Call, _objectConstructor);
            for (int i = 0; i < consts.Length; ++i)
            {
                lambdaContextTypeCtorGenerator.Emit(OpCodes.Ldarg_0);
                lambdaContextTypeCtorGenerator.EmitLoadArg(i + 1);
                lambdaContextTypeCtorGenerator.Emit(OpCodes.Stfld, lambdaContextTypeFields[i].ThrowIfNull("lambdaContextTypeFields[i]"));
            }
            lambdaContextTypeCtorGenerator.Emit(OpCodes.Ret);
            var lambdaContextType = lambdaContextTypeBuilder.CreateTypeInfo().ThrowIfNull("lambdaContextTypeBuilder.CreateType()");

            var lambdaContextParam = Expression.Parameter(lambdaContextType.AsType());
            var constReplacements = consts
                .Select((c, i) => new KeyValuePair<object, Expression>(c, Expression.Field(lambdaContextParam, GetContextFieldName(i))))
                .ToDictionary(kv => kv.Key, kv => kv.Value);
            var processedLambdaBody = ConstantExpressionReplacerVisitor.ReplaceConstants(lambda.Body, constReplacements);
            var processedLambda = Expression.Lambda(
                processedLambdaBody, lambdaContextParam.ConcatWith(lambda.Parameters)).ThrowIfNull("processedLambda");

            var compiledMethodContainerBuilder = ModuleBuilder.DefineType("Lambda_" + id, PublicClass);
            var compiledMethodBuilder = compiledMethodContainerBuilder.DefineMethod(
                "Run",
                MethodAttributes.Public | MethodAttributes.Static,
                processedLambda.ReturnType,
                processedLambda.Parameters.Select(p => p.ThrowIfNull("p").Type).ToArray());
            processedLambda.CompileToMethod(compiledMethodBuilder);
            var compiledMethodContainer = compiledMethodContainerBuilder.CreateTypeInfo().ThrowIfNull("compiledMethodContainerBuilder.CreateType()");
            var compiledMethod = compiledMethodContainer.GetMethod("Run").ThrowIfNull("compiledMethod");

            var lambdaContext = Activator.CreateInstance(lambdaContextType.AsType(), consts);
            var result = compiledMethod.CreateDelegate(lambda.Type, lambdaContext);
            return result;
#else
            throw new NotSupportedException("Expression<TDelegate>.CompileToMethod() is missing on .net standard");
#endif
        }
    }
}
