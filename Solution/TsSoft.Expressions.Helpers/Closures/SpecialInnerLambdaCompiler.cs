﻿namespace TsSoft.Expressions.Helpers.Closures
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Helpers.Visitors;

    class SpecialInnerLambdaCompiler : ISpecialInnerLambdaCompiler
    {
        private class DelegateConversionDescription
        {
            [NotNull]
            public MethodInfo ConversionMethod { get; private set; }

            [NotNull]
            public Type DelegateWithClosureInputType { get; private set; }

            public DelegateConversionDescription([NotNull] MethodInfo conversionMethod, [NotNull] Type delegateWithClosureInputType)
            {
                if (conversionMethod == null) throw new ArgumentNullException("conversionMethod");
                if (delegateWithClosureInputType == null) throw new ArgumentNullException("delegateWithClosureInputType");
                ConversionMethod = conversionMethod;
                DelegateWithClosureInputType = delegateWithClosureInputType;
            }
        }

        [NotNull]
        private readonly IReadOnlyDictionary<Type, DelegateConversionDescription> _supportedDelegateTypes;

        [NotNull]
        private readonly FieldInfo _closureObjects;

        public SpecialInnerLambdaCompiler([NotNull]IMemberInfoHelper memberInfoHelper)
        {
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            var supportedDelegateTypes = new Dictionary<Type, DelegateConversionDescription>();
            var convertMethods = typeof(Conversions).GetTypeInfo().GetMember(
                "Convert",
                MemberTypes.Method,
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static)
                .OfType<MethodInfo>();
            foreach (var convertMethod in convertMethods)
            {
                if (convertMethod == null)
                {
                    continue;
                }
                var parameters = convertMethod.GetParameters();
                ParameterInfo delegateParam;
                if (parameters.Length != 2 
                    || parameters[0] == null 
                    || (delegateParam = parameters[1]) == null
                    || !delegateParam.ParameterType.GetTypeInfo().IsGenericType)
                {
                    continue;
                }
                var inType = delegateParam.ParameterType;
                var outType = convertMethod.ReturnType;
                var inArgs = GenericArguments(inType);
                var outArgs = GenericArguments(outType);
                var methodArgs = convertMethod.IsGenericMethod ? convertMethod.GetGenericArguments() : Type.EmptyTypes;
                if (inArgs.Length != outArgs.Length + 1
                    || inArgs.Length != methodArgs.Length
                    || !inArgs.Skip(1).SequenceEqual(outArgs)
                    || !inArgs.SequenceEqual(methodArgs))
                {
                    continue;
                }
                supportedDelegateTypes.Add(
                    GenericDefinitionOrSelf(outType),
                    new DelegateConversionDescription(convertMethod, GenericDefinitionOrSelf(inType)));
            }
            _supportedDelegateTypes = supportedDelegateTypes;

            _closureObjects = memberInfoHelper.GetFieldInfo((Closure c) => c.Objects);
        }

        [NotNull]
        private static Type GenericDefinitionOrSelf([NotNull]Type type)
        {
            var typeInfo = type.GetTypeInfo();
            return typeInfo.IsGenericType ? typeInfo.GetGenericTypeDefinition() : type;
        }

        [NotNull]
        private static Type[] GenericArguments([NotNull] Type type)
        {
            var typeInfo = type.GetTypeInfo();
            return typeInfo.IsGenericType ? typeInfo.GetGenericArguments() : Type.EmptyTypes;
        }

        private class ClosureExpression
        {
            [NotNull]
            public Expression Closure { get; private set; }

            [NotNull]
            public ParameterExpression ClosureParameter { get; private set; }

            public IEnumerable<KeyValuePair<ParameterExpression, Expression>> Replacements { get; private set; }

            public ClosureExpression([NotNull] Expression closure, [NotNull] ParameterExpression closureParameter, IEnumerable<KeyValuePair<ParameterExpression, Expression>> replacements)
            {
                if (closure == null) throw new ArgumentNullException("closure");
                if (closureParameter == null) throw new ArgumentNullException("closureParameter");
                Closure = closure;
                ClosureParameter = closureParameter;
                Replacements = replacements;
            }
        }

        [NotNull]
        private ClosureExpression MakeClosureExpression([NotNull]IReadOnlyList<ParameterExpression> paramList)
        {
            if (paramList.Count == 1 && paramList[0] != null)
            {
                return new ClosureExpression(
                    paramList[0],
                    paramList[0],
                    null);
            }
            var closureParam = Expression.Parameter(typeof(Closure), "closure");
            return new ClosureExpression(
                Expression.MemberInit(
                    Expression.New(typeof(Closure)),
                    Expression.Bind(
                        _closureObjects,
                        Expression.NewArrayInit(
                            typeof(object),
                            paramList.Select(
                                p => p.Type.GetTypeInfo().IsValueType ? Expression.Convert(p, typeof(object)) : (Expression)p)))),
                closureParam,
                paramList.Select((p, i) =>
                    new KeyValuePair<ParameterExpression, Expression>(
                        p,
                        Expression.Convert(
                            Expression.ArrayAccess(
                                Expression.Field(closureParam, _closureObjects),
                                Expression.Constant(i)),
                            p.Type))));
        }

        public Expression TryGenerateExpressionWithCompiledLambda(LambdaExpression lambda, ISet<ParameterExpression> openParams)
        {
            if (openParams.Count == 0)
            {
                return Expression.Constant(lambda.Compile(), lambda.Type);
            }
            DelegateConversionDescription conversionDescription;
            if (!_supportedDelegateTypes.TryGetValue(GenericDefinitionOrSelf(lambda.Type), out conversionDescription)
                || conversionDescription == null)
            {
                return lambda;
            }
            var paramList = openParams.ToList();
            var closureExpression = MakeClosureExpression(paramList);
            var body = closureExpression.Replacements != null
                ? ParameterReplacerVisitor.Replace(lambda.Body, closureExpression.Replacements)
                : lambda.Body;
            var lambdaTypeInfo = lambda.Type.GetTypeInfo();
            var originalGenericArgs = lambdaTypeInfo.IsGenericType
                ? lambdaTypeInfo.GetGenericArguments()
                : new Type[0];
            var lambdaWithClosureGenericArgs = new Type[originalGenericArgs.Length + 1];
            lambdaWithClosureGenericArgs[0] = closureExpression.ClosureParameter.Type;
            Array.Copy(originalGenericArgs, 0, lambdaWithClosureGenericArgs, 1, originalGenericArgs.Length);
            var lambdaWithClosure = Expression.Lambda(
                conversionDescription.DelegateWithClosureInputType.MakeGenericType(lambdaWithClosureGenericArgs),
                body,
                closureExpression.ClosureParameter.ConcatWith(lambda.Parameters));
            var compiledLambdaWithClosure = lambdaWithClosure.Compile();
            var convertMethod = conversionDescription.ConversionMethod.IsGenericMethod
                ? conversionDescription.ConversionMethod.MakeGenericMethod(lambdaWithClosureGenericArgs)
                : conversionDescription.ConversionMethod;
            return Expression.Call(
                convertMethod,
                closureExpression.Closure,
                Expression.Constant(compiledLambdaWithClosure));
        }

        private static class Conversions
        {
            // ReSharper disable UnusedMember.Local
            // ReSharper disable RedundantLambdaSignatureParentheses
            private static Func<TOut> Convert<TClosure, TOut>([NotNull]TClosure closure, [NotNull]Func<TClosure, TOut> func)
            {
                return () => func(closure);
            }

            private static Func<T1, TOut> Convert<TClosure, T1, TOut>([NotNull]TClosure closure, [NotNull]Func<TClosure, T1, TOut> func)
            {
                return (t1) => func(closure, t1);
            }

            private static Func<T1, T2, TOut> Convert<TClosure, T1, T2, TOut>([NotNull]TClosure closure, [NotNull]Func<TClosure, T1, T2, TOut> func)
            {
                return (t1, t2) => func(closure, t1, t2);
            }

            private static Action Convert<TClosure>([NotNull]TClosure closure, [NotNull]Action<TClosure> func)
            {
                return () => func(closure);
            }

            private static Action<T1> Convert<TClosure, T1>([NotNull] TClosure closure, [NotNull] Action<TClosure, T1> func)
            {
                return (t1) => func(closure, t1);
            }

            private static Action<T1, T2> Convert<TClosure, T1, T2>([NotNull] TClosure closure, [NotNull] Action<TClosure, T1, T2> func)
            {
                return (t1, t2) => func(closure, t1, t2);
            }
            // ReSharper restore UnusedMember.Local
            // ReSharper restore RedundantLambdaSignatureParentheses
        }
    }
}
