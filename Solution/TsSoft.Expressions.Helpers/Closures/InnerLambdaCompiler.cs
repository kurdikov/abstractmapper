namespace TsSoft.Expressions.Helpers.Closures
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Reflection.Emit;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.DynamicTypes;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models.Tuples;

    /// <summary>
    /// ���������� ��������� � ���������� ������� � ��������� �� ���������������� ���������
    /// �� ��������� ��������� ������� Delegate.CreateDelegate, ������������ ��� ���������� ������ � ����������� ��������
    /// </summary>
    public sealed class InnerLambdaCompiler : AssemblyBuilderContainer, IInnerLambdaCompiler
    {
        [NotNull] private readonly IDelegateTypeHelper _delegateTypeHelper;
        
        [NotNull]
        private readonly LazyConcurrentDictionary<EquatableTuple<Type, Type>,ClosureTypeDescription>
            _closureTypeCache = new LazyConcurrentDictionary<EquatableTuple<Type, Type>, ClosureTypeDescription>();

        [NotNull]
        private readonly LazyConcurrentDictionary<DelegateTypeDescription, Type>
            _delegateTypeCache = new LazyConcurrentDictionary<DelegateTypeDescription, Type>();

        private const string AssemblyName = "TsSoft.Expressions.Helpers.DynamicClosures";

        /// <summary>
        /// ���������� ��������� � ���������� ������� � ��������� �� ���������������� ���������
        /// �� ��������� ��������� ������� Delegate.CreateDelegate, ������������ ��� ���������� ������ � ����������� ��������
        /// </summary>
        public InnerLambdaCompiler([NotNull] IDelegateTypeHelper delegateTypeHelper)
            : base(AssemblyName)
        {
            if (delegateTypeHelper == null) throw new ArgumentNullException("delegateTypeHelper");
            _delegateTypeHelper = delegateTypeHelper;
        }

        private class ClosureTypeDescription
        {
            [NotNull]public TypeInfo TypeInfo { get; private set; }
            [NotNull]public FieldInfo[] ClosureFields { get; private set; }
            [NotNull]public FieldInfo DelegateField { get; private set; }
            [NotNull]public MethodInfo ApplyMethod { get; private set; }
            [NotNull]public Delegate GetDelegateDelegate { get; private set; }

            [NotNull]
            private static string GetFieldName(int index)
            {
                return "Field" + index;
            }

            private const string DelegateFieldName = "Delegate";
            private const string ApplyMethodName = "Apply";
            private const string GetDelegateMethodName = "GetDelegate";

            [NotNull]
            private static string GetNewTypeName()
            {
                return "Closure_" + Guid.NewGuid().ToString("N");
            }

            [NotNull]
            public static ClosureTypeDescription CreateClosureType(
                [NotNull]ModuleBuilder moduleBuilder,
                [NotNull]Type[] openParamTypes,
                [NotNull]LambdaExpression lambda,
                [NotNull]Type lambdaWithClosureType,
                [NotNull]IDelegateTypeHelper delegateTypeHelper)
            {
                var result = new ClosureTypeDescription
                {
                    ClosureFields = new FieldInfo[openParamTypes.Length],
                };

                var typeBuilder = moduleBuilder.DefineType(GetNewTypeName(), TypeAttributes.Public |
                    TypeAttributes.Class |
                    TypeAttributes.AutoClass |
                    TypeAttributes.AnsiClass |
                    TypeAttributes.BeforeFieldInit |
                    TypeAttributes.AutoLayout);
                typeBuilder.DefineDefaultConstructor(MethodAttributes.Public);

                for (int i = 0; i < openParamTypes.Length; ++i)
                {
                    var paramType = openParamTypes[i].ThrowIfNull("paramType");
                    result.ClosureFields[i] = typeBuilder.DefineField(
                        GetFieldName(i),
                        paramType,
                        FieldAttributes.Public);
                }
                result.DelegateField = typeBuilder.DefineField(DelegateFieldName, lambdaWithClosureType, FieldAttributes.Public);

                var applyDelegateMethod = typeBuilder.DefineMethod(
                    ApplyMethodName,
                    MethodAttributes.Public,
                    lambda.ReturnType,
                    lambda.Parameters.Select(p => p.Type).ToArray());

                var applyGenerator = applyDelegateMethod.GetILGenerator();
                applyGenerator.Emit(OpCodes.Ldarg_0);
                applyGenerator.Emit(OpCodes.Ldfld, result.DelegateField);
                for (int i = 0; i < openParamTypes.Length; ++i)
                {
                    applyGenerator.Emit(OpCodes.Ldarg_0);
                    applyGenerator.Emit(OpCodes.Ldfld, result.ClosureFields[i].ThrowIfNull("result.ClosureFields[i]"));
                }
                for (int i = 0; i < lambda.Parameters.Count; ++i)
                {
                    applyGenerator.EmitLoadArg(i + 1);
                }
                applyGenerator.Emit(OpCodes.Callvirt, delegateTypeHelper.GetDelegateInvokeMethod(lambdaWithClosureType));
                applyGenerator.Emit(OpCodes.Ret);


                result.TypeInfo = typeBuilder.CreateTypeInfo().ThrowIfNull("typeBuilder.CreateType()");
                var resultType = result.TypeInfo.AsType();
                for (int i = 0; i < openParamTypes.Length; ++i)
                {
                    result.ClosureFields[i] = result.TypeInfo.GetField(GetFieldName(i)).ThrowIfNull("ClosureFields[i]");
                }
                result.DelegateField = result.TypeInfo.GetField(DelegateFieldName).ThrowIfNull("DelegateField");
                result.ApplyMethod = result.TypeInfo.GetMethod(ApplyMethodName).ThrowIfNull("ApplyMethod");

                var getDelegateMethod = new DynamicMethod(GetDelegateMethodName, lambda.Type, new[] { resultType }, true);
                var getDelegateGenerator = getDelegateMethod.GetILGenerator();
                getDelegateGenerator.Emit(OpCodes.Ldarg_0);
                getDelegateGenerator.Emit(OpCodes.Ldftn, result.ApplyMethod);
                getDelegateGenerator.Emit(OpCodes.Newobj, delegateTypeHelper.GetDelegateConstructor(lambda.Type));
                getDelegateGenerator.Emit(OpCodes.Ret);

                result.GetDelegateDelegate = getDelegateMethod.CreateDelegate(typeof(Func<,>).MakeGenericType(resultType, lambda.Type));
                return result;
            }
        }

        private static bool IsAccessible([NotNull] TypeInfo type)
        {
            return type.IsPublic || type.IsNestedPublic && type.DeclaringType != null && type.DeclaringType.GetTypeInfo().IsPublic;
        }


        [NotNull]
        private Type GetDelegateType([NotNull]DelegateTypeDescription description)
        {
            return _delegateTypeCache.LazyGetOrAdd(
                description,
                d => _delegateTypeHelper.CreateDelegateType(ModuleBuilder, "Delegate_" + Guid.NewGuid().ToString("N"), description)).ThrowIfNull("DelegateTypeCache.GetOrAdd");
        }

        /// <summary>
        /// ������������� ��������� �� ���������������� ��������� �� ������
        /// </summary>
        /// <param name="lambda">������ ������-���������</param>
        /// <param name="openParams">������������� ��������� ������</param>
        /// <returns>��������� ���� �� ����, ��� �������� ������</returns>
        public Expression GenerateExpressionWithCompiledLambda(
            LambdaExpression lambda,
            ISet<ParameterExpression> openParams)
        {
            if (openParams.Count == 0)
            {
                return Expression.Constant(lambda.Compile(), lambda.Type);
            }
            if (lambda.Parameters.Count > short.MaxValue)  // impossible to generate IL for such stuff
            {
                return lambda;
            }
            //if (!IsAccessible(lambda.Type) || lambda.Parameters.Any(p => !IsAccessible(p.Type)) || lambda.ReturnType != null && !IsAccessible(lambda.ReturnType))
            //    // can't emit code with private delegate type
            //{
            //    return lambda;
            //}

            var paramList = openParams.ToList();

            var paramTypes = paramList.Select(p => p.Type).ToArray();

            var lambdaWithClosureType = GetDelegateType(new DelegateTypeDescription(
                paramTypes.Concat(lambda.Parameters.Select(lp => lp.Type)),
                lambda.ReturnType.ThrowIfNull("lambda.ReturnType")));

            var closureDesc = _closureTypeCache.LazyGetOrAdd(
                EquatableTuple.Create(lambda.Type, lambdaWithClosureType),
                tt => ClosureTypeDescription.CreateClosureType(ModuleBuilder, paramTypes, lambda, lambdaWithClosureType, _delegateTypeHelper))
                .ThrowIfNull("closureDesc");

            var lambdaWithClosure = Expression.Lambda(
                closureDesc.DelegateField.FieldType,
                lambda.Body,
                paramList.Concat(lambda.Parameters));
            var compiledLambdaWithClosure = lambdaWithClosure.Compile();


            var bindings = new MemberBinding[paramList.Count + 1];
            for (int i = 0; i < paramList.Count; ++i)
            {
                var field = closureDesc.ClosureFields[i].ThrowIfNull("field");
                Expression param = paramList[i].ThrowIfNull("param");
                bindings[i] = Expression.Bind(field, param);
            }
            bindings[paramList.Count] = Expression.Bind(
                closureDesc.DelegateField,
                Expression.Constant(compiledLambdaWithClosure, closureDesc.DelegateField.FieldType));

            var closureExpression = Expression.MemberInit(
                Expression.New(closureDesc.TypeInfo.AsType()),
                bindings);
            return Expression.Invoke(Expression.Constant(closureDesc.GetDelegateDelegate), closureExpression);
        }
    }
}
