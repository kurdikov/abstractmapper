namespace TsSoft.Expressions.Helpers.Closures
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// ���������� ��������� � ���������� ������� � ��������� �� ���������������� ��������� ��� ��������� ����� ������������� ����� ���������
    /// �� ��������� ��������� ������� Delegate.CreateDelegate, ������������ ��� ���������� ������ � ����������� ��������
    /// </summary>
    public interface ISpecialInnerLambdaCompiler
    {
        /// <summary>
        /// ���������� ������������� ��������� �� ���������������� ��������� �� ������
        /// </summary>
        /// <param name="lambda">������ ������-���������</param>
        /// <param name="openParams">������������� ��������� ������</param>
        /// <returns>��������� ���� �� ����, ��� �������� ������ (�������� ������ � ������, ���� ��� �������� � ��� �� ��������������)</returns>
        [NotNull]
        Expression TryGenerateExpressionWithCompiledLambda([NotNull]LambdaExpression lambda, [NotNull]ISet<ParameterExpression> openParams);
    }
}
