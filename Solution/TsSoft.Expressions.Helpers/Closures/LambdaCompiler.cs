﻿namespace TsSoft.Expressions.Helpers.Closures
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Visitors;

    class LambdaCompiler : ILambdaCompiler
    {
        [NotNull] private readonly IInnerLambdaCompiler _innerLambdaCompiler;
        [NotNull] private readonly ISpecialInnerLambdaCompiler _specialInnerLambdaCompiler;
        [NotNull] private readonly IMethodBuilderLambdaCompiler _methodBuilderLambdaCompiler;

        public LambdaCompiler(
            [NotNull] IInnerLambdaCompiler innerLambdaCompiler,
            [NotNull] ISpecialInnerLambdaCompiler specialInnerLambdaCompiler,
            [NotNull] IMethodBuilderLambdaCompiler methodBuilderLambdaCompiler)
        {
            if (innerLambdaCompiler == null) throw new ArgumentNullException("innerLambdaCompiler");
            if (specialInnerLambdaCompiler == null) throw new ArgumentNullException("specialInnerLambdaCompiler");
            if (methodBuilderLambdaCompiler == null) throw new ArgumentNullException("methodBuilderLambdaCompiler");
            _innerLambdaCompiler = innerLambdaCompiler;
            _specialInnerLambdaCompiler = specialInnerLambdaCompiler;
            _methodBuilderLambdaCompiler = methodBuilderLambdaCompiler;
        }

        public T Compile<T>(Expression<T> lambda, LambdaCompilationType compilationType = LambdaCompilationType.PrecompileInner)
        {
            return InnerLambdaCompilerVisitor.Compile(lambda, compilationType, _innerLambdaCompiler, _specialInnerLambdaCompiler, _methodBuilderLambdaCompiler);
        }

        public Delegate Compile(LambdaExpression lambda, LambdaCompilationType compilationType = LambdaCompilationType.PrecompileInner)
        {
            return InnerLambdaCompilerVisitor.Compile(lambda, compilationType, _innerLambdaCompiler, _specialInnerLambdaCompiler, _methodBuilderLambdaCompiler);
        }
    }
}
