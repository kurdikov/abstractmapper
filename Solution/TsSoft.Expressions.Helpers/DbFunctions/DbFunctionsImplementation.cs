﻿namespace TsSoft.Expressions.Helpers.DbFunctions
{
    using System;
    using System.Linq;

    /// <summary>
    /// Реализация методов из System.Data.Entity.DbFunctions
    /// </summary>
    public class DbFunctionsImplementation
    {
        /// <summary>
        /// Отрезать часть строки слева
        /// </summary>
        /// <param name="stringArgument">Строка</param>
        /// <param name="length">Длина отрезаемого куска</param>
        /// <returns>Отрезанный кусок</returns>
        public static string Left(string stringArgument, long? length)
        {
            return length.HasValue && stringArgument != null ? stringArgument.Substring(0, (int)length.Value) : null;
        }

        /// <summary>
        /// Отрезать часть строки справа
        /// </summary>
        /// <param name="stringArgument">Строка</param>
        /// <param name="length">Длина отрезаемого куска</param>
        /// <returns>Отрезанный кусок</returns>
        public static string Right(string stringArgument, long? length)
        {
            return length.HasValue && stringArgument != null
                       ? stringArgument.Substring(stringArgument.Length - (int) length.Value, (int)length.Value)
                       : null;
        }

        /// <summary>
        /// Отразить строку зеркально
        /// </summary>
        /// <param name="stringArgument">Строка</param>
        /// <returns>Отражённая строка</returns>
        /// <remarks>Так же, как и SQL Server, криво работает со строками, в которых есть символы из двух UTF-16 code unit-ов (как комбинаторы, так и суррогатные пары)</remarks>
        public static string Reverse(string stringArgument)
        {
            return stringArgument != null ? new string(stringArgument.Reverse().ToArray()) : null;
        }

        /// <summary>
        /// Получить разницу временных зон данного времени и UTC в минутах
        /// </summary>
        /// <param name="dateTimeOffsetArgument">Время с временной зоной</param>
        /// <returns>Разница заданной временной зоны и UTC в минутах</returns>
        public static int? GetTotalOffsetMinutes(DateTimeOffset? dateTimeOffsetArgument)
        {
            return dateTimeOffsetArgument.HasValue ? dateTimeOffsetArgument.Value.Offset.Minutes : (int?)null;
        }

        /// <summary>
        /// Удалить время из даты
        /// </summary>
        /// <param name="dateValue">Дата</param>
        /// <returns>Дата с обнулённым временем</returns>
        public static DateTimeOffset? TruncateTime(DateTimeOffset? dateValue)
        {
            return dateValue.HasValue ? dateValue.Value.Date : (DateTimeOffset?)null;
        }

        /// <summary>
        /// Удалить время из даты
        /// </summary>
        /// <param name="dateValue">Дата</param>
        /// <returns>Дата с обнулённым временем</returns>
        public static DateTime? TruncateTime(DateTime? dateValue)
        {
            return dateValue.HasValue ? dateValue.Value.Date : (DateTime?) null;
        }

        /// <summary>
        /// Создать дату
        /// </summary>
        /// <param name="year">Год</param>
        /// <param name="month">Месяц</param>
        /// <param name="day">День</param>
        /// <param name="hour">Час</param>
        /// <param name="minute">Минута</param>
        /// <param name="second">Секунда</param>
        /// <returns>Созданная дата</returns>
        public static DateTime? CreateDateTime(int? year, int? month, int? day, int? hour, int? minute, double? second)
        {
            return year.HasValue && month.HasValue && day.HasValue && hour.HasValue && minute.HasValue && second.HasValue
                ? new DateTime(year.Value, month.Value, day.Value, hour.Value, minute.Value, 
                    (int)second.Value, (int)(second.Value * 1000.0) % 1000)
                : (DateTime?)null;
        }

        /// <summary>
        /// Создать дату
        /// </summary>
        /// <param name="year">Год</param>
        /// <param name="month">Месяц</param>
        /// <param name="day">День</param>
        /// <param name="hour">Час</param>
        /// <param name="minute">Минута</param>
        /// <param name="second">Секунда</param>
        /// <param name="timeZoneOffset">Разница между временной зоной даты и UTC</param>
        /// <returns>Созданная дата</returns>
        public static DateTimeOffset? CreateDateTimeOffset(int? year, int? month, int? day, int? hour, int? minute, double? second, int? timeZoneOffset)
        {
            return year.HasValue && month.HasValue && day.HasValue && hour.HasValue && minute.HasValue && second.HasValue && timeZoneOffset.HasValue
                ? new DateTimeOffset(year.Value, month.Value, day.Value, hour.Value, minute.Value,
                    (int)second.Value, (int)(second.Value * 1000.0) % 1000, new TimeSpan(0, timeZoneOffset.Value, 0))
                : (DateTimeOffset?)null; 
        }

        /// <summary>
        /// Создать временной промежуток
        /// </summary>
        /// <param name="hour">Час</param>
        /// <param name="minute">Минута</param>
        /// <param name="second">Секунда</param>
        /// <returns>Созданный временной промежуток</returns>
        public static TimeSpan? CreateTime(int? hour, int? minute, double? second)
        {
            return hour.HasValue && minute.HasValue && second.HasValue
                       ? new TimeSpan(hour.Value, minute.Value, (int) second.Value, (int) (second.Value*1000.0)%1000)
                       : (TimeSpan?) null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="dateValue">Дата</param>
        /// <param name="addValue">На сколько лет смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTimeOffset? AddYears(DateTimeOffset? dateValue, int? addValue)
        {
            return dateValue.HasValue && addValue.HasValue
                       ? dateValue.Value.AddYears(addValue.Value)
                       : (DateTimeOffset?) null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="dateValue">Дата</param>
        /// <param name="addValue">На сколько лет смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTime? AddYears(DateTime? dateValue, int? addValue)
        {
            return dateValue.HasValue && addValue.HasValue
                       ? dateValue.Value.AddYears(addValue.Value)
                       : (DateTime?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="dateValue">Дата</param>
        /// <param name="addValue">На сколько месяцев смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTimeOffset? AddMonths(DateTimeOffset? dateValue, int? addValue)
        {
            return dateValue.HasValue && addValue.HasValue
                       ? dateValue.Value.AddMonths(addValue.Value)
                       : (DateTimeOffset?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="dateValue">Дата</param>
        /// <param name="addValue">На сколько лет смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTime? AddMonths(DateTime? dateValue, int? addValue)
        {
            return dateValue.HasValue && addValue.HasValue
                       ? dateValue.Value.AddMonths(addValue.Value)
                       : (DateTime?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="dateValue">Дата</param>
        /// <param name="addValue">На сколько дней смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTimeOffset? AddDays(DateTimeOffset? dateValue, int? addValue)
        {
            return dateValue.HasValue && addValue.HasValue
                       ? dateValue.Value.AddDays(addValue.Value)
                       : (DateTimeOffset?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="dateValue">Дата</param>
        /// <param name="addValue">На сколько дней смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTime? AddDays(DateTime? dateValue, int? addValue)
        {
            return dateValue.HasValue && addValue.HasValue
                       ? dateValue.Value.AddDays(addValue.Value)
                       : (DateTime?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько часов смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTimeOffset? AddHours(DateTimeOffset? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.AddHours(addValue.Value)
                       : (DateTimeOffset?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько часов смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTime? AddHours(DateTime? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.AddHours(addValue.Value)
                       : (DateTime?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько часов смещать</param>
        /// <returns>Смещённая дата</returns>
        public static TimeSpan? AddHours(TimeSpan? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.Add(new TimeSpan(addValue.Value, 0, 0))
                       : (TimeSpan?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько минут смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTimeOffset? AddMinutes(DateTimeOffset? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.AddMinutes(addValue.Value)
                       : (DateTimeOffset?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько минут смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTime? AddMinutes(DateTime? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.AddMinutes(addValue.Value)
                       : (DateTime?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько минут смещать</param>
        /// <returns>Смещённая дата</returns>
        public static TimeSpan? AddMinutes(TimeSpan? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.Add(new TimeSpan(0, addValue.Value, 0))
                       : (TimeSpan?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько секунд смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTimeOffset? AddSeconds(DateTimeOffset? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.AddSeconds(addValue.Value)
                       : (DateTimeOffset?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько секунд смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTime? AddSeconds(DateTime? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.AddSeconds(addValue.Value)
                       : (DateTime?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько секунд смещать</param>
        /// <returns>Смещённая дата</returns>
        public static TimeSpan? AddSeconds(TimeSpan? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.Add(new TimeSpan(0, 0, addValue.Value))
                       : (TimeSpan?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько миллисекунд смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTimeOffset? AddMilliseconds(DateTimeOffset? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.AddMilliseconds(addValue.Value)
                       : (DateTimeOffset?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько миллисекунд смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTime? AddMilliseconds(DateTime? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.AddMilliseconds(addValue.Value)
                       : (DateTime?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько миллисекунд смещать</param>
        /// <returns>Смещённая дата</returns>
        public static TimeSpan? AddMilliseconds(TimeSpan? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.Add(new TimeSpan(0, 0, 0, 0, addValue.Value))
                       : (TimeSpan?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько микросекунд смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTimeOffset? AddMicroseconds(DateTimeOffset? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.AddTicks(10 * addValue.Value)
                       : (DateTimeOffset?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько микросекунд смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTime? AddMicroseconds(DateTime? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.AddTicks(10 * addValue.Value)
                       : (DateTime?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько микросекунд смещать</param>
        /// <returns>Смещённая дата</returns>
        public static TimeSpan? AddMicroseconds(TimeSpan? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.Add(new TimeSpan(10 * addValue.Value))
                       : (TimeSpan?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько наносекунд смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTimeOffset? AddNanoseconds(DateTimeOffset? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.AddTicks(addValue.Value / 100)
                       : (DateTimeOffset?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько наносекунд смещать</param>
        /// <returns>Смещённая дата</returns>
        public static DateTime? AddNanoseconds(DateTime? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.AddTicks(addValue.Value / 100)
                       : (DateTime?)null;
        }

        /// <summary>
        /// Сместить дату
        /// </summary>
        /// <param name="timeValue">Дата</param>
        /// <param name="addValue">На сколько наносекунд смещать</param>
        /// <returns>Смещённая дата</returns>
        public static TimeSpan? AddNanoseconds(TimeSpan? timeValue, int? addValue)
        {
            return timeValue.HasValue && addValue.HasValue
                       ? timeValue.Value.Add(new TimeSpan(addValue.Value % 100))
                       : (TimeSpan?)null;
        }

        /// <summary>
        /// Получить разницу между датами в годах
        /// </summary>
        /// <param name="dateValue1">Первая дата</param>
        /// <param name="dateValue2">Вторая дата</param>
        /// <returns>Разница</returns>
        public static int? DiffYears(DateTimeOffset? dateValue1, DateTimeOffset? dateValue2)
        {
            return dateValue1.HasValue && dateValue2.HasValue
                       ? dateValue2.Value.Year - dateValue1.Value.Year
                       : (int?)null;
        }

        /// <summary>
        /// Получить разницу между датами в годах
        /// </summary>
        /// <param name="dateValue1">Первая дата</param>
        /// <param name="dateValue2">Вторая дата</param>
        /// <returns>Разница</returns>
        public static int? DiffYears(DateTime? dateValue1, DateTime? dateValue2)
        {
            return dateValue1.HasValue && dateValue2.HasValue
                       ? dateValue2.Value.Year - dateValue1.Value.Year
                       : (int?)null;
        }
    }
}
