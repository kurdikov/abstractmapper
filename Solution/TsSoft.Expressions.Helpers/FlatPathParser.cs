﻿namespace TsSoft.Expressions.Helpers
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Выделяет простой путь из выражения
    /// </summary>
    public class FlatPathParser : IFlatPathParser
    {
        /// <summary>
        /// Выделить путь (последовательность свойств) из выражения
        /// </summary>
        /// <param name="pathExpression">Выражение</param>
        /// <returns>Путь</returns>
        public IReadOnlyList<ValueHoldingMember> Parse(Expression pathExpression)
        {
            if (pathExpression == null)
            {
                return new ValueHoldingMember[0];
            }
            var visitor = new FlatPathParserVisitor();
            var result = visitor.GetMemberCollection(pathExpression);
            return result;
        }
    }
}
