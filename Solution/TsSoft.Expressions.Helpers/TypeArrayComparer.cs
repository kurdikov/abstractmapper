namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// ���������� ������� ����� �� ������������ ���������
    /// </summary>
    public class TypeArrayComparer : IEqualityComparer<Type[]>
    {
        /// <summary>
        /// ���������� ������� ����� �� ������������ ���������
        /// </summary>
        [NotNull]
        public static readonly TypeArrayComparer Instance = new TypeArrayComparer();

        /// <summary>
        /// ��������� �� ������� ����� �����������
        /// </summary>
        public bool Equals(Type[] x, Type[] y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                return false;
            }
            if (x.Length != y.Length)
            {
                return false;
            }
            for (int i = 0; i < x.Length; ++i)
            {
                if (x[i] != y[i])
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// ���-���
        /// </summary>
        public int GetHashCode(Type[] obj)
        {
            if (obj == null)
            {
                return 0;
            }
            unchecked
            {
                int result = 0;
                for (int i = 0; i < obj.Length; ++i)
                {
                    result = result * 397;
                    if (obj[i] != null)
                    {
                        result ^= obj[i].GetHashCode();
                    }
                }
                return result;
            }
        }
    }
}
