﻿namespace TsSoft.Expressions.Helpers
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Выделяет простой путь из выражения
    /// </summary>
    public interface IFlatPathParser
    {
        /// <summary>
        /// Выделить путь (последовательность свойств) из выражения
        /// </summary>
        /// <param name="pathExpression">Выражение</param>
        /// <returns>Путь</returns>
        [NotNull]
        IReadOnlyList<ValueHoldingMember> Parse(Expression pathExpression);
    }
}
