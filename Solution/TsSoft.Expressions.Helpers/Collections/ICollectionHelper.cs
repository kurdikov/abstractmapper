﻿namespace TsSoft.Expressions.Helpers.Collections
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Хелпер для работы с коллекциями
    /// </summary>
    public interface ICollectionHelper
    {
        /// <summary>
        /// Вернуть исходную коллекцию или пустую, если пришёл null
        /// </summary>
        /// <typeparam name="TElement">Тип элемента коллекции</typeparam>
        /// <param name="input">Исходная коллекция</param>
        /// <returns>Коллекция, не являющаяся null</returns>
        [NotNull]
        IEnumerable<TElement> NotNull<TElement>(IEnumerable<TElement> input);

        /// <summary>
        /// Определить, является ли коллекция префиксом другой
        /// </summary>
        /// <typeparam name="TElement">Тип элемента коллекции</typeparam>
        /// <param name="sequence">Коллекция</param>
        /// <param name="prefixCandidate">Коллекция-кандидат на префикс</param>
        /// <param name="equalityComparer">Сравнитель элементов</param>
        /// <returns>Является ли кандидат префиксом</returns>
        bool IsPrefix<TElement>(
            IEnumerable<TElement> sequence,
            IEnumerable<TElement> prefixCandidate,
            IEqualityComparer<TElement> equalityComparer = null);

        /// <summary>
        /// Определить, является ли коллекция собственным префиксом другой
        /// </summary>
        /// <typeparam name="TElement">Тип элемента коллекции</typeparam>
        /// <param name="sequence">Коллекция</param>
        /// <param name="prefixCandidate">Коллекция-кандидат на префикс</param>
        /// <param name="equalityComparer">Сравнитель элементов</param>
        /// <returns>Является ли кандидат собственным префиксом</returns>
        bool IsPrefixAndNotEqual<TElement>(
            IEnumerable<TElement> sequence,
            IEnumerable<TElement> prefixCandidate,
            IEqualityComparer<TElement> equalityComparer = null);
    }
}