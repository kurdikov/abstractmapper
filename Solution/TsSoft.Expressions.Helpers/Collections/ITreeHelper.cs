﻿namespace TsSoft.Expressions.Helpers.Collections
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    ///     Хелпер для работы с древесными структурами
    /// </summary>
    public interface ITreeHelper
    {
        /// <summary>
        ///     Заполняет навигационные поля сущности с древесной структурой, сохраняя исходный порядок элементов
        /// </summary>
        /// <typeparam name="T">Сущность</typeparam>
        /// <typeparam name="TId">Тип идентификатора сущности (уникального для сущности)</typeparam>
        /// <param name="treeElements">Выбранные сущности</param>
        /// <param name="idSelector">Функция, возвращающая идентификатор сущности</param>
        /// <param name="parentIdSelector">Функция, возвращающая идентификатор родителя сущности</param>
        /// <param name="parentSetter">Функция, заполняющая поле родителя сущности ( (child, parent) => child.Parent = parent )</param>
        /// <param name="childrenCollectionSelector">Функция, возвращающая коллекцию детей сущности</param>
        /// <returns>Сущности с заполненными навигационными полями</returns>
        [NotNull]
        IEnumerable<T> ConstructTreeStructureStable<T, TId>(
            [NotNull] IEnumerable<T> treeElements,
            [NotNull] Func<T, TId> idSelector,
            [NotNull] Func<T, TId?> parentIdSelector,
            [NotNull] Action<T, T> parentSetter,
            [NotNull] Func<T, ICollection<T>> childrenCollectionSelector)
            where T : class
            where TId : struct;

        /// <summary>
        ///     Заполняет навигационные поля сущности с древесной структурой
        /// </summary>
        /// <typeparam name="T">Сущность</typeparam>
        /// <typeparam name="TId">Тип идентификатора сущности (уникального для сущности)</typeparam>
        /// <param name="treeElements">Выбранные сущности</param>
        /// <param name="idSelector">Функция, возвращающая идентификатор сущности</param>
        /// <param name="parentIdSelector">Функция, возвращающая идентификатор родителя сущности</param>
        /// <param name="parentSetter">Функция, заполняющая поле родителя сущности ( (child, parent) => child.Parent = parent )</param>
        /// <param name="childrenCollectionSelector">Функция, возвращающая коллекцию детей сущности</param>
        /// <returns>Сущности с заполненными навигационными полями</returns>
        [NotNull]
        IEnumerable<T> ConstructTreeStructure<T, TId>(
            [NotNull] IEnumerable<T> treeElements,
            [NotNull] Func<T, TId> idSelector,
            [NotNull] Func<T, TId?> parentIdSelector,
            [NotNull] Action<T, T> parentSetter,
            [NotNull] Func<T, ICollection<T>> childrenCollectionSelector)
            where T : class
            where TId : struct;

        /// <summary>
        ///     Поиск листа в дереве. При нахождении листа, в его детях не ищет
        /// </summary>
        /// <typeparam name="T">Сущность</typeparam>
        /// <param name="treeElements">Выбранные сущности</param>
        /// <param name="childrenCollectionSelector">Функция, возвращающая коллекцию детей сущности</param>
        /// <param name="functor">Функция проверки соответствия условию</param>
        /// <returns></returns>
        [NotNull]
        IEnumerable<T> Find<T>(
            [NotNull] IEnumerable<T> treeElements,
            [NotNull] Func<T, ICollection<T>> childrenCollectionSelector,
            [NotNull] Func<T, bool> functor);

        /// <summary>
        ///     Сортирует элементы древесной структуры в виде плоского дерева (1, 2, 3, 1.1, 1.1.2,2.1) -> (1, 1.1, 1.1.2, 2, 2.1, 3)
        /// </summary>
        /// <typeparam name="T">Сущность</typeparam>
        /// <typeparam name="TF">Поле для сортировки</typeparam>
        /// <typeparam name="TId">Идентификатор сущности</typeparam>
        /// <param name="treeElements"></param>
        /// <param name="idSelector">Функция, возвращающая идентификатор сущности</param>
        /// <param name="parentIdSelector">Функция, возвращающая идентификатор родителя сущности</param>
        /// <param name="parentSetter">Функция, заполняющая поле родителя сущности ( (child, parent) => child.Parent = parent )</param>
        /// <param name="childrenCollectionSelector">Функция, возвращающая коллекцию детей сущности</param>
        /// <param name="compareFieldSelector">Селектор поля, по которому идет сортировка</param>
        /// <param name="postprocessor">Постпроцессор отсортированных сущностей</param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        [NotNull]
        IEnumerable<T> OrderByHierarchy<T, TF, TId>(
            [NotNull] IEnumerable<T> treeElements,
            [NotNull] Func<T, TId> idSelector,
            [NotNull] Func<T, TId?> parentIdSelector,
            [NotNull] Func<T, ICollection<T>> childrenCollectionSelector,
            [NotNull] Action<T, T> parentSetter,
            [NotNull] Func<T, TF> compareFieldSelector,
            Action<T> postprocessor,
            IComparer<TF> comparer = null)
            where T : class
            where TId : struct;

        /// <summary>
        ///     Уплощает древовидную структуру
        /// </summary>
        /// <typeparam name="T">Сущность</typeparam>
        /// <typeparam name="TId">Идентификатор сущности</typeparam>
        /// <param name="treeElements"></param>
        /// <param name="idSelector">Функция, возвращающая идентификатор сущности</param>
        /// <param name="parentIdSelector">Функция, возвращающая идентификатор родителя сущности</param>
        /// <param name="parentSetter">Функция, заполняющая поле родителя сущности ( (child, parent) => child.Parent = parent )</param>
        /// <param name="childrenCollectionSelector">Функция, возвращающая коллекцию детей сущности</param>
        /// <returns>Уплощенное дерево</returns>
        [NotNull]
        IEnumerable<T> Flat<T, TId>(
            [NotNull] IEnumerable<T> treeElements,
            [NotNull] Func<T, TId> idSelector,
            [NotNull] Func<T, TId?> parentIdSelector,
            [NotNull] Func<T, ICollection<T>> childrenCollectionSelector,
            [NotNull] Action<T, T> parentSetter)
            where T : class
            where TId : struct;
    }
}