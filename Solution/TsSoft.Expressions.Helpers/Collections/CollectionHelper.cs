﻿namespace TsSoft.Expressions.Helpers.Collections
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Хелпер для работы с коллекциями
    /// </summary>
    public class CollectionHelper : ICollectionHelper
    {
        /// <summary>
        /// Вернуть исходную коллекцию или пустую, если пришёл null
        /// </summary>
        /// <typeparam name="TElement">Тип элемента коллекции</typeparam>
        /// <param name="input">Исходная коллекция</param>
        /// <returns>Коллекция, не являющаяся null</returns>
        public IEnumerable<TElement> NotNull<TElement>(IEnumerable<TElement> input)
        {
            return input ?? Enumerable.Empty<TElement>();
        }

        private bool IsPrefix<TElement>(
            IEnumerable<TElement> sequence,
            IEnumerable<TElement> prefixCandidate,
            IEqualityComparer<TElement> equalityComparer,
            bool countEqualAsPrefix)
        {
            if (prefixCandidate == null)
            {
                return countEqualAsPrefix || sequence != null && sequence.Any();
            }
            if (sequence == null)
            {
                return false;
            }
            using (var sequenceEnumerator = sequence.GetEnumerator())
            using (var prefixEnumerator = prefixCandidate.GetEnumerator())
            {
                while (prefixEnumerator.MoveNext())
                {
                    if (!sequenceEnumerator.MoveNext())
                    {
                        return false;
                    }
                    if (!equalityComparer.Equals(sequenceEnumerator.Current, prefixEnumerator.Current))
                    {
                        return false;
                    }
                }
                return countEqualAsPrefix || sequenceEnumerator.MoveNext();
            }
        }

        /// <summary>
        /// Определить, является ли коллекция префиксом другой
        /// </summary>
        /// <typeparam name="TElement">Тип элемента коллекции</typeparam>
        /// <param name="sequence">Коллекция</param>
        /// <param name="prefixCandidate">Коллекция-кандидат на префикс</param>
        /// <param name="equalityComparer">Сравнитель элементов</param>
        /// <returns>Является ли кандидат префиксом</returns>
        public bool IsPrefix<TElement>(
            IEnumerable<TElement> sequence,
            IEnumerable<TElement> prefixCandidate,
            IEqualityComparer<TElement> equalityComparer = null)
        {
            return IsPrefix(sequence, prefixCandidate, equalityComparer ?? EqualityComparer<TElement>.Default, true);
        }

        /// <summary>
        /// Определить, является ли коллекция собственным префиксом другой
        /// </summary>
        /// <typeparam name="TElement">Тип элемента коллекции</typeparam>
        /// <param name="sequence">Коллекция</param>
        /// <param name="prefixCandidate">Коллекция-кандидат на префикс</param>
        /// <param name="equalityComparer">Сравнитель элементов</param>
        /// <returns>Является ли кандидат собственным префиксом</returns>
        public bool IsPrefixAndNotEqual<TElement>(
            IEnumerable<TElement> sequence, 
            IEnumerable<TElement> prefixCandidate, 
            IEqualityComparer<TElement> equalityComparer = null)
        {
            return IsPrefix(sequence, prefixCandidate, equalityComparer ?? EqualityComparer<TElement>.Default, false);
        }
    }
}
