﻿namespace TsSoft.Expressions.Helpers.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

    internal class TreeHelper : ITreeHelper
    {
        public IEnumerable<T> ConstructTreeStructure<T, TId>(
            IEnumerable<T> treeElements,
            Func<T, TId> idSelector,
            Func<T, TId?> parentIdSelector,
            Action<T, T> parentSetter,
            Func<T, ICollection<T>> childrenCollectionSelector)
            where T : class
            where TId : struct
        {
            if (treeElements == null)
            {
                throw new ArgumentNullException("treeElements");
            }
            if (idSelector == null)
            {
                throw new ArgumentNullException("idSelector");
            }
            if (parentIdSelector == null)
            {
                throw new ArgumentNullException("parentIdSelector");
            }
            if (childrenCollectionSelector == null)
            {
                throw new ArgumentNullException("childrenCollectionSelector");
            }
            if (parentSetter == null)
            {
                throw new ArgumentNullException("parentSetter");
            }
            var lookup = GetLookup(treeElements, idSelector, parentIdSelector);
            ConstructTreeStructure(lookup.Values, lookup, parentSetter, childrenCollectionSelector, idSelector);
            return lookup.Select(l => l.Value.Key);
        }

        public IEnumerable<T> Find<T>(
            IEnumerable<T> treeElements,
            Func<T, ICollection<T>> childrenCollectionSelector,
            Func<T, bool> functor)
        {
            if (treeElements == null)
            {
                throw new ArgumentNullException("treeElements");
            }
            if (childrenCollectionSelector == null)
            {
                throw new ArgumentNullException("childrenCollectionSelector");
            }
            if (functor == null)
            {
                throw new ArgumentNullException("functor");
            }
            var result = new List<T>();
            foreach (var elem in treeElements)
            {
                if (functor(elem))
                {
                    result.Add(elem);
                }
                else
                {
                    var collection = childrenCollectionSelector(elem);
                    if (collection != null)
                    {
                        result.AddRange(Find(collection, childrenCollectionSelector, functor));
                    }
                }
            }
            return result;
        }

        public IEnumerable<T> OrderByHierarchy<T, TF, TId>(
            IEnumerable<T> treeElements,
            Func<T, TId> idSelector,
            Func<T, TId?> parentIdSelector,
            Func<T, ICollection<T>> childrenCollectionSelector,
            Action<T, T> parentSetter,
            Func<T, TF> compareFieldSelector,
            Action<T> postprocessor,
            IComparer<TF> comparer = null)
            where T : class
            where TId : struct
        {
            if (treeElements == null)
            {
                throw new ArgumentNullException("treeElements");
            }
            if (idSelector == null)
            {
                throw new ArgumentNullException("idSelector");
            }
            if (parentIdSelector == null)
            {
                throw new ArgumentNullException("parentIdSelector");
            }
            if (childrenCollectionSelector == null)
            {
                throw new ArgumentNullException("childrenCollectionSelector");
            }
            if (parentSetter == null)
            {
                throw new ArgumentNullException("parentSetter");
            }
            if (compareFieldSelector == null)
            {
                throw new ArgumentNullException("compareFieldSelector");
            }
            comparer = comparer ?? Comparer<TF>.Default;
            var topElements = ConstructTreeStructureAndGetTopElements(
                treeElements, idSelector, parentIdSelector, parentSetter, childrenCollectionSelector);
            var resultList = GetOrderedElements(
                topElements,
                childrenCollectionSelector,
                enumerable => enumerable.ThrowIfNull("enumerable").OrderBy(compareFieldSelector, comparer),
                postprocessor ?? (p => { }));
            return resultList;
        }

        public IEnumerable<T> Flat<T, TId>(
            IEnumerable<T> treeElements, 
            Func<T, TId> idSelector, 
            Func<T, TId?> parentIdSelector,
            Func<T, ICollection<T>> childrenCollectionSelector, 
            Action<T, T> parentSetter)
            where T : class
            where TId : struct
        {
            if (treeElements == null)
            {
                throw new ArgumentNullException("treeElements");
            }
            if (idSelector == null)
            {
                throw new ArgumentNullException("idSelector");
            }
            if (parentIdSelector == null)
            {
                throw new ArgumentNullException("parentIdSelector");
            }
            if (childrenCollectionSelector == null)
            {
                throw new ArgumentNullException("childrenCollectionSelector");
            }
            if (parentSetter == null)
            {
                throw new ArgumentNullException("parentSetter");
            }
            var topElements = ConstructTreeStructureAndGetTopElements(
                treeElements,
                idSelector,
                parentIdSelector,
                parentSetter,
                childrenCollectionSelector);

            return GetOrderedElements(
                topElements,
                childrenCollectionSelector,
                enumerable => enumerable,
                obj => { });
        }

        [NotNull]
        private static IDictionary<TId, KeyValuePair<T, TId?>> GetLookup<T, TId>(
            [NotNull] IEnumerable<T> treeElements,
            [NotNull] Func<T, TId> idSelector,
            [NotNull] Func<T, TId?> parentIdSelector)
            where T : class
            where TId : struct
        {
            return treeElements.ToDictionary(idSelector, elem => new KeyValuePair<T, TId?>(elem, parentIdSelector(elem)));
        }

        private static void ConstructTreeStructure<T, TId>(
            [NotNull] IEnumerable<KeyValuePair<T, TId?>> elements,
            [NotNull] IDictionary<TId, KeyValuePair<T, TId?>> lookup,
            [NotNull] Action<T, T> parentSetter,
            [NotNull] Func<T, ICollection<T>> childrenCollectionSelector,
            [NotNull] Func<T, TId> idSelector)
            where T : class
            where TId : struct
        {
            foreach (var element in elements)
            {
                TId? parentId;
                if (element.Key != null && (parentId = element.Value).HasValue)
                {
                    KeyValuePair<T, TId?> parent;
                    lookup.TryGetValue(parentId.Value, out parent);
                    if (parent.Key != null)
                    {
                        parentSetter(element.Key, parent.Key);
                        var childrenCollection = childrenCollectionSelector(parent.Key);
                        if (childrenCollection == null)
                        {
                            throw new InvalidOperationException(
                                string.Format("childrenCollectionSelector returned null. Element id = {0}", idSelector(element.Key)));
                        }
                        childrenCollection.Add(element.Key);
                    }
                }
            }
        }

        [NotNull]
        public IEnumerable<T> ConstructTreeStructureAndGetTopElements<T, TId>(
            [NotNull] IEnumerable<T> treeElements,
            [NotNull] Func<T, TId> idSelector,
            [NotNull] Func<T, TId?> parentIdSelector,
            [NotNull] Action<T, T> parentSetter,
            [NotNull] Func<T, ICollection<T>> childrenCollectionSelector)
            where T : class
            where TId : struct
        {
            var lookup = GetLookup(treeElements, idSelector, parentIdSelector);
            ConstructTreeStructure(lookup.Values, lookup, parentSetter, childrenCollectionSelector, idSelector);
            return lookup.Where(l => !l.Value.Value.HasValue || !lookup.ContainsKey(l.Value.Value.Value)).Select(l => l.Value.Key);
        }

        private static IEnumerable<T> GetOrderedElements<T>(
            [NotNull] IEnumerable<T> elements,
            [NotNull] Func<T, ICollection<T>> childrenCollectionSelector,
            [NotNull] Func<IEnumerable<T>, IEnumerable<T>> orderer,
            [NotNull] Action<T> postprocessor)
            where T : class
        {
            foreach (var element in orderer(elements))
            {
                postprocessor(element);
                yield return element;
                var collection = childrenCollectionSelector(element);
                if (collection == null)
                {
                    continue;
                }
                foreach (var elem in GetOrderedElements(collection,
                                                        childrenCollectionSelector,
                                                        orderer,
                                                        postprocessor))
                {
                    yield return elem;
                }
            }
        }

        public IEnumerable<T> ConstructTreeStructureStable<T, TId>(
            IEnumerable<T> treeElements, 
            Func<T, TId> idSelector, 
            Func<T, TId?> parentIdSelector, 
            Action<T, T> parentSetter, 
            Func<T, ICollection<T>> childrenCollectionSelector)
            where T : class
            where TId : struct
        {
            if (treeElements == null)
            {
                throw new ArgumentNullException("treeElements");
            }
            if (idSelector == null)
            {
                throw new ArgumentNullException("idSelector");
            }
            if (parentIdSelector == null)
            {
                throw new ArgumentNullException("parentIdSelector");
            }
            if (childrenCollectionSelector == null)
            {
                throw new ArgumentNullException("childrenCollectionSelector");
            }
            if (parentSetter == null)
            {
                throw new ArgumentNullException("parentSetter");
            }
            var collection = treeElements.ToReadOnlyCollectionIfNeeded();
            var lookup = GetLookup(collection, idSelector, parentIdSelector);
            ConstructTreeStructure(collection.Select(e => new KeyValuePair<T, TId?>(e, parentIdSelector(e))), lookup, parentSetter, childrenCollectionSelector, idSelector);
            return lookup.Select(l => l.Value.Key);
        }
    }
}