﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Reflection;

    /// <summary>
    /// Фабрика обновляющих объекты функций
    /// </summary>
    public interface IUpdateObjectActionFactory
    {
        /// <summary>
        /// Построить обновляющую функцию
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="paths">Обновляемые пути</param>
        /// <param name="manager">Определитель типа свойства (примитивное, объект или коллекция)</param>
        /// <param name="dropCreateOnPaths">На каких путях производить перезапись коллекции вместо поэлементного обновления</param>
        /// <returns>Обновляющая функция</returns>
        [NotNull]
        Action<T, T> MakeUpdateAction<T>(
            [NotNull]IEnumerable<Expression<Func<T, object>>> paths,
            [NotNull]IObjectUpdateManager manager,
            IEnumerable<Expression<Func<T, object>>> dropCreateOnPaths = null);

        /// <summary>
        /// Построить асинхронную обновляющую функцию
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="paths">Обновляемые пути</param>
        /// <param name="manager">Определитель типа свойства (примитивное, объект или коллекция)</param>
        /// <param name="dropCreateOnPaths">На каких путях производить перезапись коллекции вместо поэлементного обновления</param>
        /// <returns>Функция, обновляющая объект и возвращающая набор производителей дополнительных задач</returns>
        [NotNull]
        Func<T, T, IEnumerable<Func<Task>>> MakeAsyncUpdateAction<T>(
            [NotNull]IEnumerable<Expression<Func<T, object>>> paths,
            [NotNull]IObjectUpdateManager manager,
            IEnumerable<Expression<Func<T, object>>> dropCreateOnPaths = null);
    }
}
