﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.AbstractMapper;

    internal class NewExpressionHelper : INewExpressionHelper
    {
        [NotNull]private readonly ICorrespondingMemberHelper _correspondingMemberHelper;
        [NotNull]private readonly IMemberInfoLibrary _memberInfoLibrary;
        [NotNull]private readonly IMemberNamingHelper _namingHelper;

        public NewExpressionHelper(
            [NotNull]ICorrespondingMemberHelper correspondingMemberHelper, 
            [NotNull]IMemberInfoLibrary library,
            [NotNull]IMemberNamingHelper namingHelper)
        {
            if (correspondingMemberHelper == null) throw new ArgumentNullException("correspondingMemberHelper");
            if (library == null) throw new ArgumentNullException("library");
            if (namingHelper == null) throw new ArgumentNullException("namingHelper");

            _correspondingMemberHelper = correspondingMemberHelper;
            _memberInfoLibrary = library;
            _namingHelper = namingHelper;
        }

        public SelectMemberAssignment ParseAssignment(MemberAssignment binding, Type entityType)
        {
            var result = new MemberAssignmentParserVisitor(_memberInfoLibrary, this).Parse(binding.Expression);
            result.UsedMember = GetCorrespondingMember(entityType, binding.Member);
            return result;
        }

        public SelectMemberAssignment ParseAssignment(KeyValuePair<MemberInfo, Expression> binding, Type entityType)
        {
            if (binding.Key == null) throw new ArgumentException("binding.Key is null");
            var result = new MemberAssignmentParserVisitor(_memberInfoLibrary, this).Parse(binding.Value);
            result.UsedMember = GetCorrespondingMember(entityType, binding.Key);
            return result;
        }

        private ValueHoldingMember GetCorrespondingMember(Type entityType, MemberInfo member)
        {
            var name = _namingHelper.GetShadowedName(member.Name);
            if (name == member.Name)
            {
                return _correspondingMemberHelper.GetCorrespondingMember(entityType, member);
            }
            var shadowedProperty = entityType.GetTypeInfo().GetProperty(name, Type.EmptyTypes);
            if (shadowedProperty == null)
            {
                return _correspondingMemberHelper.GetCorrespondingMember(entityType, member);
            }
            return _correspondingMemberHelper.GetCorrespondingMember(entityType, shadowedProperty);
        }

        public IEnumerable<KeyValuePair<MemberInfo, Expression>> GetAssignments(LambdaExpression selectExpression)
        {
            if (selectExpression == null)
            {
                return Enumerable.Empty<KeyValuePair<MemberInfo, Expression>>();
            }
            var body = selectExpression.Body as MemberInitExpression;
            IEnumerable<KeyValuePair<MemberInfo, Expression>> result;
            if (body == null)
            {
                var bodyNew = selectExpression.Body as NewExpression;
                if (bodyNew == null)
                {
                    throw new ArgumentException("Select expression body must be a MemberInitExpression or a NewExpression");
                }
                result = GetAssignments(bodyNew);
            }
            else
            {
                result = GetAssignments(body);
            }
            return result;
        }

        public IEnumerable<KeyValuePair<MemberInfo, Expression>> GetAssignments(MemberInitExpression expression)
        {
            return expression.Bindings.OfType<MemberAssignment>().Where(a => a != null).Select(
                a => new KeyValuePair<MemberInfo, Expression>(a.Member, a.Expression));
        }

        public IEnumerable<KeyValuePair<MemberInfo, Expression>> GetAssignments(NewExpression expression)
        {
            // ReSharper disable ConstantNullCoalescingCondition
            // R# is wrong, Members is null for constructors without parameters
            return (expression.Members ?? Enumerable.Empty<MemberInfo>())
                // ReSharper restore ConstantNullCoalescingCondition
                .Select((m, i) => new KeyValuePair<MemberInfo, Expression>(m, expression.Arguments[i]));
        }

        public MemberAssignment ComposeAssignment(KeyValuePair<MemberInfo, Expression> assignment, Type destinationType)
        {
            if (assignment.Key == null) throw new ArgumentException("assignment.Key is null");
            if (assignment.Value == null) throw new ArgumentException("assignment.Value is null");
            return Expression.Bind(
                _correspondingMemberHelper.GetCorrespondingMember(destinationType, assignment.Key).Member,
                assignment.Value);
        }

        public KeyValuePair<MemberInfo, Expression> DecomposeAssignment(MemberAssignment assignment)
        {
            return new KeyValuePair<MemberInfo, Expression>(assignment.Member, assignment.Expression);
        }
    }
}
