﻿namespace TsSoft.Expressions.Helpers
{
    using System.Linq.Expressions;
    using TsSoft.Expressions.Helpers.Visitors;

    internal class ConvertRemover : IConvertRemover
    {
        public TExpression RemoveInterfaceCast<TExpression>(TExpression expression) where TExpression : LambdaExpression
        {
            return EntityCastInterfaceRemoverVisitor.Convert(expression);
        }
    }
}