namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// ��������� �� ��������� ���������-����
    /// </summary>
    /// <typeparam name="TEntity">��� ������ ����</typeparam>
    public class IncludeEqualityComparer<TEntity> : IEqualityComparer<Expression<Func<TEntity, object>>>
    {
        /// <summary>
        /// ��������� �� ��������� ���������-����
        /// </summary>
        [NotNull]
        public static IncludeEqualityComparer<TEntity> Instance = new IncludeEqualityComparer<TEntity>();

        /// <summary>
        /// ����� �� ����
        /// </summary>
        public bool Equals(Expression<Func<TEntity, object>> x, Expression<Func<TEntity, object>> y)
        {
            return ExpressionEqualityComparer.Instance.Equals(x, y);
        }

        /// <summary>
        /// �������� ��� ����
        /// </summary>
        public int GetHashCode(Expression<Func<TEntity, object>> obj)
        {
            return ExpressionEqualityComparer.Instance.GetHashCode(obj);
        }
    }
}
