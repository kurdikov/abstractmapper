﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Проверяет на равенство выражения
    /// </summary>
    public class ExpressionEqualityComparer : IEqualityComparer<Expression>, IEqualityComparer<LambdaExpression>
    {
        /// <summary>
        /// Сравнивает выражения с точностью до имён параметров и меток
        /// </summary>
        [NotNull]
        public static readonly ExpressionEqualityComparer Instance = new ExpressionEqualityComparer(false);
        /// <summary>
        /// Сравнивает выражения с точностью до имён параметров и меток, вычисляя значения констант в замыканиях
        /// </summary>
        [NotNull]
        public static readonly ExpressionEqualityComparer ClosureAwareInstance = new ExpressionEqualityComparer(true);
        /// <summary>
        /// Сравнивает выражения с точностью до имён параметров и меток, вычисляя значения констант в выражениях MemberAccess
        /// </summary>
        [NotNull]
        public static readonly ExpressionEqualityComparer ConstantComputingInstance = new ExpressionEqualityComparer(false, false, true);
        /// <summary>
        /// Сравнивает выражения с точностью до имён параметров и меток, одноимённые свойства разных типов считаются эквивалентными
        /// </summary>
        [NotNull]
        public static readonly ExpressionEqualityComparer TypeUnawareInstance = new ExpressionEqualityComparer(false, false);
        /// <summary>
        /// Сравнивает выражения с точностью до имён параметров и меток, одноимённые свойства разных типов считаются эквивалентными, порядок следования биндингов не учитывается
        /// </summary>
        [NotNull]
        public static readonly ExpressionEqualityComparer TypeUnawareIgnoreOrderInstance = new ExpressionEqualityComparer(false, false, false, true);

        private readonly bool _computeClosures;
        private readonly bool _compareTypes;
        private readonly bool _computeConstantMembers;
        private readonly bool _orderMemberNames;

        /// <summary>
        /// Проверяет на равенство выражения
        /// </summary>
        public ExpressionEqualityComparer(bool computeClosures, bool compareTypes = true, bool computeConstantMembers = false, bool orderMemberNames = false)
        {
            _computeClosures = computeClosures;
            _compareTypes = compareTypes;
            _computeConstantMembers = computeConstantMembers;
            _orderMemberNames = orderMemberNames;
        }

        /// <summary>
        /// Равны ли выражения
        /// </summary>
        /// <param name="x">Первое выражение</param>
        /// <param name="y">Второе выражение</param>
        public bool Equals(Expression x, Expression y)
        {
            var parameterMap = new Dictionary<ParameterExpression, ParameterExpression>();
            var labelMap = new Dictionary<LabelTarget, LabelTarget>();
            return AreEqual(x, y, parameterMap, labelMap);
        }

        /// <summary>
        /// Равны ли выражения
        /// </summary>
        /// <param name="x">Первое выражение</param>
        /// <param name="y">Второе выражение</param>
        public bool Equals(LambdaExpression x, LambdaExpression y)
        {
            return Equals((Expression)x, y);
        }

        /// <summary>
        /// Хэш выражения
        /// </summary>
        public int GetHashCode(LambdaExpression obj)
        {
            return GetHashCode((Expression)obj);
        }

        /// <summary>
        /// Равны ли выражения
        /// </summary>
        /// <param name="x">Первое выражение</param>
        /// <param name="y">Второе выражение</param>
        public bool Equals<T>(Expression<T> x, Expression<T> y)
        {
            return Equals((Expression)x, y);
        }

        private static int ComposeHash([NotNull] IEnumerable<int> hashes)
        {
            return hashes.Aggregate(0, (current, hash) => current * 17 + hash);
        }

        private static int ComposeHash([NotNull] params int[] hashes)
        {
            return ComposeHash((IEnumerable<int>)hashes);
        }

        /// <summary>
        /// Хэш набора инициализаторов коллекции
        /// </summary>
        protected virtual int GetHashCode([NotNull] IReadOnlyList<ElementInit> elements)
        {
            return ComposeHash(elements.Select(li => li != null ? li.AddMethod.GetHashCode() : 0).Concat(
                elements.SelectMany(li => li != null ? li.Arguments.Select(GetHashCode) : Enumerable.Repeat(0, 1))));
        }

        /// <summary>
        /// Хэш набора инициализаторов объекта
        /// </summary>
        protected virtual int GetHashCode([NotNull] IEnumerable<MemberBinding> bindings)
        {
            int result = 0;
            var bindingsList = bindings.ToReadOnlyCollectionIfNeeded();
            foreach (var binding in bindingsList)
            {
                if (binding == null)
                {
                    continue;
                }
                if (_orderMemberNames)
                {
                    bindingsList = bindingsList.OrderBy(b => b != null ? b.Member.Name : string.Empty).ToList();
                }
                switch (binding.BindingType)
                {
                    case MemberBindingType.Assignment:
                        var assignment = binding as MemberAssignment;
                        if (assignment != null)
                        {
                            result = ComposeHash(result, GetHashCode(assignment.Expression));
                        }
                        break;
                    case MemberBindingType.ListBinding:
                        var listBinding = binding as MemberListBinding;
                        if (listBinding != null)
                        {
                            result = ComposeHash(result, GetHashCode(listBinding.Initializers));
                        }
                        break;
                    case MemberBindingType.MemberBinding:
                        var memberBinding = binding as MemberMemberBinding;
                        if (memberBinding != null)
                        {
                            result = ComposeHash(result, GetHashCode(memberBinding.Bindings));
                        }
                        break;
                }
            }
            return result;
        }

        /// <summary>
        /// Хэш набора блоков оператора switch
        /// </summary>
        protected virtual int GetHashCode([NotNull] IEnumerable<SwitchCase> cases)
        {
            return
                ComposeHash(
                    cases.SelectMany(
                    c => c != null && c.TestValues != null ? c.TestValues.Select(GetHashCode).Concat(GetHashCode(c.Body).ToEnumerable()) : Enumerable.Empty<int>()));
        }

        /// <summary>
        /// Хэш члена класса
        /// </summary>
        protected virtual int GetHashCode([NotNull]MemberInfo member)
        {
            return _compareTypes ? MemberInfoComparer.Instance.GetHashCode(member) : member.Name.GetHashCode();
        }

        /// <summary>
        /// Хэш набора инициализаторов коллекции
        /// </summary>
        protected virtual int GetHashCode(CatchBlock block)
        {
            return block != null ? ComposeHash(
                GetHashCode(block.Filter),
                GetHashCode(block.Body),
                _compareTypes && block.Test != null ? block.Test.GetHashCode() : 0) : 0;
        }

        /// <summary>
        /// Хэш набора блоков catch
        /// </summary>
        protected virtual int GetHashCode([NotNull] IEnumerable<CatchBlock> blocks)
        {
            return ComposeHash(blocks.Select(GetHashCode));
        }

        /// <summary>
        /// Получить хэш выражения
        /// </summary>
        /// <param name="obj">Выражение</param>
        public virtual int GetHashCode(Expression obj)
        {
            int result = 0;
            if (_computeClosures)
            {
                obj = ComputeIfClosure(obj);
            }
            if (_computeConstantMembers)
            {
                obj = ComputeIfConstantMember(obj);
            }
            if (obj is BinaryExpression)
            {
                var objBinary = (BinaryExpression)obj;
                result = ComposeHash(GetHashCode(objBinary.Left), GetHashCode(objBinary.Right),
                    objBinary.Method != null ? GetHashCode(objBinary.Method) : 0,
                    GetHashCode(objBinary.Conversion),
                    objBinary.IsLifted.GetHashCode(), objBinary.IsLiftedToNull.GetHashCode());
            }
            else if (obj is UnaryExpression)
            {
                var objUnary = (UnaryExpression)obj;
                result = ComposeHash(GetHashCode(objUnary.Operand),
                                   objUnary.IsLifted.GetHashCode(), objUnary.IsLiftedToNull.GetHashCode());
            }
            else if (obj is MethodCallExpression)
            {
                var objMethodCall = (MethodCallExpression)obj;
                result = ComposeHash(objMethodCall.Arguments.Select(GetHashCode));
                result = ComposeHash(result, GetHashCode(objMethodCall.Object), GetHashCode(objMethodCall.Method));
            }
            else if (obj is ConditionalExpression)
            {
                var objConditional = (ConditionalExpression)obj;
                result = ComposeHash(
                    GetHashCode(objConditional.Test),
                    GetHashCode(objConditional.IfFalse),
                    GetHashCode(objConditional.IfTrue));
            }
            else if (obj is ConstantExpression)
            {
                var objConstant = (ConstantExpression)obj;
                if (objConstant.Value != null)
                {
                    result = objConstant.Value.GetHashCode();
                }
            }
            else if (obj is InvocationExpression)
            {
                var objInvocation = (InvocationExpression)obj;
                result = ComposeHash(objInvocation.Arguments.Select(GetHashCode));
                result = ComposeHash(result, GetHashCode(objInvocation.Expression));
            }
            else if (obj is LambdaExpression)
            {
                var objLambda = (LambdaExpression)obj;
                result = ComposeHash(objLambda.Parameters.Select(GetHashCode));
                result = ComposeHash(result, GetHashCode(objLambda.Body), objLambda.TailCall.GetHashCode());
                if (_compareTypes && objLambda.ReturnType != null)
                {
                    result = ComposeHash(result, objLambda.ReturnType.GetHashCode());
                }
            }
            else if (obj is ListInitExpression)
            {
                var objListInit = (ListInitExpression)obj;
                result = GetHashCode(objListInit.Initializers);
            }
            else if (obj is MemberExpression)
            {
                var objMember = (MemberExpression)obj;
                result = ComposeHash(GetHashCode(objMember.Expression), GetHashCode(objMember.Member));
            }
            else if (obj is MemberInitExpression)
            {
                var objMemberInit = (MemberInitExpression)obj;
                result = GetHashCode(objMemberInit.Bindings);
                result = ComposeHash(result, GetHashCode(objMemberInit.NewExpression));
            }
            else if (obj is NewExpression)
            {
                var objNew = (NewExpression)obj;
                result = ComposeHash(objNew.Arguments.Select(GetHashCode));
                result = ComposeHash(result, GetHashCode(objNew.Constructor));
            }
            else if (obj is NewArrayExpression)
            {
                var objNewArray = (NewArrayExpression)obj;
                result = ComposeHash(objNewArray.Expressions.Select(GetHashCode));
            }
            else if (obj is ParameterExpression)
            {
                var objParam = (ParameterExpression)obj;
                result = objParam.IsByRef.GetHashCode();
            }
            else if (obj is TypeBinaryExpression)
            {
                var objTypeBinary = (TypeBinaryExpression)obj;
                result = ComposeHash(GetHashCode(objTypeBinary.Expression), objTypeBinary.TypeOperand.GetHashCode());
            }
            else if (obj is BlockExpression)
            {
                var objBlock = (BlockExpression)obj;
                result = ComposeHash(objBlock.Variables != null ? objBlock.Variables.Select(GetHashCode) : Enumerable.Empty<int>()
                    .Concat(objBlock.Expressions != null ? objBlock.Expressions.Select(GetHashCode) : Enumerable.Empty<int>())
                    .AppendOne(GetHashCode(objBlock.Result)));
            }
            else if (obj is DefaultExpression)
            {
            }
            else if (obj is GotoExpression)
            {
                var objGoto = (GotoExpression)obj;
                result = ComposeHash(objGoto.Kind.GetHashCode(), GetHashCode(objGoto.Value));
            }
            else if (obj is LabelExpression)
            {
                var objLabel = (LabelExpression)obj;
                result = GetHashCode(objLabel.DefaultValue);
            }
            else if (obj is IndexExpression)
            {
                var objIndex = (IndexExpression)obj;
                result = ComposeHash(objIndex.Indexer != null ? objIndex.Indexer.GetHashCode() : 0, GetHashCode(objIndex.Object));
                result = ComposeHash(result, ComposeHash(objIndex.Arguments != null ? objIndex.Arguments.Select(GetHashCode) : Enumerable.Empty<int>()));
            }
            else if (obj is RuntimeVariablesExpression)
            {
                var objRuntime = (RuntimeVariablesExpression)obj;
                result = ComposeHash(objRuntime.Variables != null ? objRuntime.Variables.Select(GetHashCode) : Enumerable.Empty<int>());
            }
            else if (obj is LoopExpression)
            {
                var objLoop = (LoopExpression)obj;
                result = GetHashCode(objLoop.Body);
            }
            else if (obj is SwitchExpression)
            {
                var objSwitch = (SwitchExpression)obj;
                result = ComposeHash(
                    objSwitch.Cases != null ? GetHashCode(objSwitch.Cases) : 0,
                    objSwitch.Comparison != null ? GetHashCode(objSwitch.Comparison) : 0,
                    GetHashCode(objSwitch.SwitchValue),
                    GetHashCode(objSwitch.DefaultBody));
            }
            else if (obj is TryExpression)
            {
                var objTry = (TryExpression)obj;
                result = ComposeHash(
                    GetHashCode(objTry.Body),
                    objTry.Handlers != null ? GetHashCode(objTry.Handlers) : 0,
                    GetHashCode(objTry.Finally),
                    GetHashCode(objTry.Fault));
            }
            else if (obj == null)
            {
            }
            else if (obj is DebugInfoExpression)
            {
                throw new NotSupportedException();
            }
#if NET45
            else if (obj is DynamicExpression)
            {
                throw new NotSupportedException();
            }
#endif
            else
            {
                throw new NotSupportedException();
            }
            if (obj != null)
            {
                result = ComposeHash(result, obj.NodeType.GetHashCode());
                if (_compareTypes)
                {
                    result = ComposeHash(result, obj.Type.GetHashCode());
                }
            }
            return result;
        }

        /// <summary>
        /// Равны ли члены класса
        /// </summary>
        protected virtual bool AreEqual(MemberInfo x, MemberInfo y, Expression xParent, Expression yParent, out ExpressionDifference diff)
        {
            var result = _compareTypes
                ? MemberInfoComparer.Instance.Equals(x, y)
                : MemberInfoNameComparer.Instance.Equals(x, y);
            diff = result ? null : Diff(xParent, yParent, x, y, "Members are different");
            return result;
        }

        /// <summary>
        /// Равны ли списки выражений
        /// </summary>
        protected virtual bool AreEqual(
            IReadOnlyList<Expression> x,
            IReadOnlyList<Expression> y,
            Expression xParent,
            Expression yParent,
            [NotNull] Context context,
            out ExpressionDifference diff)
        {
            diff = null;
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                diff = Diff(xParent, yParent, x, y, "Parameter collection is null");
                return false;
            }
            if (x.Count != y.Count)
            {
                diff = Diff(xParent, yParent, x, y, "Parameter count does not match");
                return false;
            }
            for (var index = 0; index < x.Count; ++index)
            {
                if (!AreEqual(x[index], y[index], context, out diff))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Равны ли списки инициализаторов коллекций
        /// </summary>
        protected virtual bool AreEqual(
            IReadOnlyList<ElementInit> x,
            IReadOnlyList<ElementInit> y,
            Expression xParent,
            Expression yParent,
            [NotNull] Context context,
            out ExpressionDifference diff)
        {
            diff = null;
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                diff = Diff(xParent, yParent, x, y, "Initializer collection is null");
                return false;
            }
            if (x.Count != y.Count)
            {
                diff = Diff(xParent, yParent, x, y, "Initializer count does not match");
                return false;
            }
            for (var index = 0; index < x.Count; ++index)
            {
                if (x[index] == null && y[index] == null)
                {
                    continue;
                }
                if (x[index] == null || y[index] == null)
                {
                    diff = Diff(xParent, yParent, x[index], y[index], string.Format("ElementInit[{0}] is null", index));
                    return false;
                }
                if (!AreEqual(x[index].AddMethod, y[index].AddMethod, xParent, yParent, out diff)
                    || !AreEqual(x[index].Arguments, y[index].Arguments, xParent, yParent, context, out diff))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Равны ли списки инициализаторов объекта
        /// </summary>
        protected virtual bool AreEqual(
            IReadOnlyList<MemberBinding> x,
            IReadOnlyList<MemberBinding> y,
            Expression xParent,
            Expression yParent,
            [NotNull] Context context,
            out ExpressionDifference diff)
        {
            diff = null;
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                diff = Diff(xParent, yParent, x, y, "Binding collection is null");
                return false;
            }
            if (x.Count != y.Count)
            {
                diff = Diff(xParent, yParent, x, y, "Binding count does not match");
                return false;
            }
            if (_orderMemberNames)
            {
                x = x.OrderBy(mb => mb != null ? mb.Member.Name : string.Empty).ToList();
                y = y.OrderBy(mb => mb != null ? mb.Member.Name : string.Empty).ToList();
            }
            for (var index = 0; index < x.Count; ++index)
            {
                if (x[index] == null && y[index] == null)
                {
                    continue;
                }
                if (x[index] == null || y[index] == null)
                {
                    diff = Diff(xParent, yParent, x[index], y[index], string.Format("Member binding [{0}] is null", index));
                    return false;
                }
                if (!AreEqual(x[index].Member, y[index].Member, xParent, yParent, out diff))
                {
                    return false;
                }
                if (x[index].BindingType != y[index].BindingType)
                {
                    diff = Diff(xParent, yParent, x[index], y[index], string.Format("Binding types are different"));
                    return false;
                }
                switch (x[index].BindingType)
                {
                    case MemberBindingType.Assignment:
                        var xAssignment = (MemberAssignment)x[index];
                        var yAssignment = (MemberAssignment)y[index];
                        if (!AreEqual(xAssignment.Expression, yAssignment.Expression, context, out diff))
                        {
                            return false;
                        }
                        break;
                    case MemberBindingType.ListBinding:
                        var xList = (MemberListBinding)x[index];
                        var yList = (MemberListBinding)y[index];
                        if (!AreEqual(xList.Initializers, yList.Initializers, xParent, yParent, context, out diff))
                        {
                            return false;
                        }
                        break;
                    case MemberBindingType.MemberBinding:
                        var xMember = (MemberMemberBinding)x[index];
                        var yMember = (MemberMemberBinding)y[index];
                        if (!AreEqual(xMember.Bindings, yMember.Bindings, xParent, yParent, context, out diff))
                        {
                            return false;
                        }
                        break;
                    default:
                        diff = Diff(xParent, yParent, x[index], y[index], string.Format("Unknown binding type {0}", x[index].BindingType));
                        return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Равны ли списки параметров, обновить маппинг параметров
        /// </summary>
        protected virtual bool AreEqualAndMap(
            IReadOnlyList<ParameterExpression> x,
            IReadOnlyList<ParameterExpression> y,
            Expression xParent,
            Expression yParent,
            [NotNull]IDictionary<ParameterExpression, ParameterExpression> parameterMap,
            [NotNull]IDictionary<LabelTarget, LabelTarget> labelMap,
            out ExpressionDifference diff)
        {
            diff = null;
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                diff = Diff(xParent, yParent, x, y, "Parameter collection is null");
                return false;
            }
            if (x.Count != y.Count)
            {
                diff = Diff(xParent, yParent, x, y, "Parameter count does not match");
                return false;
            }
            for (int parameterIndex = 0; parameterIndex < x.Count; ++parameterIndex)
            {
                if (x[parameterIndex] == null && y[parameterIndex] == null)
                {
                    continue;
                }
                if (x[parameterIndex] == null || y[parameterIndex] == null)
                {
                    diff = Diff(xParent, yParent, x[parameterIndex], y[parameterIndex], string.Format("Parameter[{0}] is null", parameterIndex));
                    return false;
                }
                if (_compareTypes && x[parameterIndex].Type != y[parameterIndex].Type)
                {
                    diff = Diff(xParent, yParent, x[parameterIndex], y[parameterIndex], "Parameter types do not match");
                    return false;
                }
                parameterMap[x[parameterIndex]] = y[parameterIndex];
            }
            return true;
        }

        /// <summary>
        /// Равны ли метки, обновить маппинг меток
        /// </summary>
        protected virtual bool AreEqualAndMap(LabelTarget x, LabelTarget y, [NotNull]IDictionary<LabelTarget, LabelTarget> labelMap,
            out ExpressionDifference diff)
        {
            diff = null;
            labelMap[x] = y;
            return true;
        }

        /// <summary>
        /// Равны ли параметры
        /// </summary>
        protected virtual bool AreEqual(
            ParameterExpression x, ParameterExpression y,
            Expression xParent,
            Expression yParent,
            [NotNull]IDictionary<ParameterExpression, ParameterExpression> parameterMap,
            out ExpressionDifference diff)
        {
            ParameterExpression match;
            if (!parameterMap.TryGetValue(x, out match))
            {
                diff = Diff(xParent, yParent, x, y, string.Format("Matching parameter for {0} not found", x));
                return false;
            }
            if (match != y)
            {
                diff = Diff(xParent, yParent, x, y, match, string.Format("Parameters do not match: {0} is a match for {1}, not {2}", match, x, y));
                return false;
            }
            diff = null;
            return true;
        }

        /// <summary>
        /// Равны ли метки
        /// </summary>
        protected virtual bool AreEqual(LabelTarget x, LabelTarget y,
            Expression xParent,
            Expression yParent,
            [NotNull]IDictionary<LabelTarget, LabelTarget> labelMap,
            out ExpressionDifference diff)
        {
            LabelTarget match;
            if (!labelMap.TryGetValue(x, out match))
            {
                diff = Diff(xParent, yParent, x, y, string.Format("Matching label for {0} not found", x));
                return false;
            }
            if (match != y)
            {
                diff = Diff(xParent, yParent, x, y, match, string.Format("Labels do not match: {0} is a match for {1}, not {2}", match, x, y));
                return false;
            }
            diff = null;
            return true;
        }

        /// <summary>
        /// Равны ли списки блоков оператора switch
        /// </summary>
        protected virtual bool AreEqual(
            IReadOnlyList<SwitchCase> x,
            IReadOnlyList<SwitchCase> y,
            Expression xParent,
            Expression yParent,
            [NotNull]Context context,
            out ExpressionDifference diff)
        {
            diff = null;
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                diff = Diff(xParent, yParent, x, y, "Case collection is null");
                return false;
            }
            if (x.Count != y.Count)
            {
                diff = Diff(xParent, yParent, x, y, "Case count does not match");
                return false;
            }
            for (int index = 0; index < x.Count; ++index)
            {
                if (x[index] == null && y[index] == null)
                {
                    continue;
                }
                if (x[index] == null || y[index] == null)
                {
                    diff = Diff(xParent, yParent, x[index], y[index], string.Format("Case[{0}] is null", index));
                    return false;
                }
                if (!AreEqual(x[index].TestValues, y[index].TestValues, xParent, yParent, context, out diff)
                    || !AreEqual(x[index].Body, y[index].Body, context, out diff))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Равны ли списки блоков catch
        /// </summary>
        protected virtual bool AreEqual(
            IReadOnlyList<CatchBlock> x, IReadOnlyList<CatchBlock> y,
            Expression xParent,
            Expression yParent,
            [NotNull]Context context,
            out ExpressionDifference diff)
        {
            diff = null;
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                diff = Diff(xParent, yParent, x, y, "Catch block collection is null");
                return false;
            }
            if (x.Count != y.Count)
            {
                diff = Diff(xParent, yParent, x, y, "Catch block count does not match");
                return false;
            }
            for (int index = 0; index < x.Count; ++index)
            {
                if (x[index] == null && y[index] == null)
                {
                    continue;
                }
                if (x[index] == null || y[index] == null)
                {
                    diff = Diff(xParent, yParent, x[index], y[index], string.Format("Catch block[{0}] is null", index));
                    return false;
                }
                if (_compareTypes && x[index].Test != y[index].Test)
                {
                    diff = Diff(xParent, yParent, x[index].Test, y[index].Test, "Tested types are different");
                    return false;
                }
                if (!AreEqual(x[index].Filter, y[index].Filter, context, out diff)
                    || !AreEqual(x[index].Body, y[index].Body, context, out diff))
                {
                    return false;
                }
            }
            return true;
        }

        private bool IsClosure(Type type)
        {
            return type != null && type.IsNested && type.GetTypeInfo().IsNestedPrivate
                && type.GetTypeInfo().IsDefined(typeof(CompilerGeneratedAttribute))
                && type.Name.StartsWith("<>c__DisplayClass");
        }

        private bool IsSimpleClosureExpression(MemberExpression expr)
        {
            return
                expr != null
                && IsClosure(expr.Member.DeclaringType)
                && expr.Member.MemberType == MemberTypes.Field
                && expr.Expression is ConstantExpression;
        }

        [ContractAnnotation("null => false")]
        private bool IsSimpleClosureExpression(Expression expr)
        {
            return IsSimpleClosureExpression(expr as MemberExpression);
        }

        private object ComputeSimpleClosure([NotNull]MemberExpression expr)
        {
            return ((FieldInfo)expr.Member).GetValue(((ConstantExpression)expr.Expression).Value);
        }

        [NotNull]
        private Expression ComputeIfClosure([NotNull]Expression expr)
        {
            return IsSimpleClosureExpression(expr)
                ? Expression.Constant(ComputeSimpleClosure((MemberExpression)expr), expr.Type)
                : expr;
        }

        [ContractAnnotation("null => false")]
        private bool IsConstantMember(Expression expr)
        {
            var eMember = expr as MemberExpression;
            return eMember != null
                && (eMember.Expression is ConstantExpression || IsConstantMember(eMember.Expression));
        }

        private object ComputeConstantMember([NotNull]Expression expr)
        {
            var current = (MemberExpression)expr;
            var members = new Stack<ValueHoldingMember>();
            ConstantExpression constant = null;
            while (current != null)
            {
                members.Push(ValueHoldingMember.GetFromExpression(current));
                constant = current.Expression as ConstantExpression;
                current = current.Expression as MemberExpression;
            }
            var currentValue = constant != null ? constant.Value : null;
            foreach (var member in members)
            {
                currentValue = member.GetValue(currentValue);
            }
            return currentValue;
        }

        [ContractAnnotation("null => null; notnull => notnull")]
        private Expression ComputeIfConstantMember(Expression expr)
        {
            return IsConstantMember(expr)
                       ? Expression.Constant(ComputeConstantMember(expr), expr.Type)
                       : expr;
        }

        /// <summary>
        /// Равны ли выражения
        /// </summary>
        /// <param name="x">Первое выражение</param>
        /// <param name="y">Второе выражение</param>
        /// <param name="parameterMap">Сопоставление параметров</param>
        /// <param name="labelMap">Сопоставление меток</param>
        public bool AreEqual(
            Expression x,
            Expression y,
            [NotNull] IDictionary<ParameterExpression, ParameterExpression> parameterMap,
            [NotNull] IDictionary<LabelTarget, LabelTarget> labelMap)
        {
            ExpressionDifference difference;
            return AreEqual(x, y, parameterMap, labelMap, out difference);
        }

        /// <summary>
        /// Равны ли выражения
        /// </summary>
        /// <param name="x">Первое выражение</param>
        /// <param name="y">Второе выражение</param>
        /// <param name="diff">Найденное различие</param>
        public bool AreEqual(
            Expression x,
            Expression y,
            out ExpressionDifference diff)
        {
            return AreEqual(x, y, new Dictionary<ParameterExpression, ParameterExpression>(), new Dictionary<LabelTarget, LabelTarget>(), out diff);
        }

        /// <summary>
        /// Равны ли выражения
        /// </summary>
        /// <param name="x">Первое выражение</param>
        /// <param name="y">Второе выражение</param>
        /// <param name="parameterMap">Сопоставление параметров</param>
        /// <param name="labelMap">Сопоставление меток</param>
        /// <param name="diff">Найденное различие</param>
        public bool AreEqual(
            Expression x,
            Expression y,
            [NotNull] IDictionary<ParameterExpression, ParameterExpression> parameterMap,
            [NotNull] IDictionary<LabelTarget, LabelTarget> labelMap,
            out ExpressionDifference diff)
        {
            return AreEqual(x, y, new Context(parameterMap, labelMap), out diff);
        }

        /// <summary>
        /// Равны ли выражения
        /// </summary>
        /// <param name="x">Первое выражение</param>
        /// <param name="y">Второе выражение</param>
        /// <param name="parameterMap">Сопоставление параметров</param>
        /// <param name="labelMap">Сопоставление меток</param>
        public bool AreEqual(
            Expression x,
            Expression y,
            [NotNull] Stack<IDictionary<ParameterExpression, ParameterExpression>> parameterMap,
            [NotNull] IDictionary<LabelTarget, LabelTarget> labelMap)
        {
            ExpressionDifference difference;
            return AreEqual(x, y, new Context(parameterMap, labelMap), out difference);
        }

        /// <summary>
        /// Равны ли выражения
        /// </summary>
        /// <param name="x">Первое выражение</param>
        /// <param name="y">Второе выражение</param>
        /// <param name="context">Контекст сравнения</param>
        public bool AreEqual(
            Expression x,
            Expression y,
            [NotNull]Context context)
        {
            ExpressionDifference difference;
            return AreEqual(x, y, context, out difference);
        }

        /// <summary>
        /// Равны ли выражения
        /// </summary>
        /// <param name="x">Первое выражение</param>
        /// <param name="y">Второе выражение</param>
        /// <param name="context">Контекст сравнения</param>
        /// <param name="diff">Найденное различие</param>
        public virtual bool AreEqual(
            Expression x,
            Expression y,
            [NotNull]Context context,
            out ExpressionDifference diff)
        {
            context.IncreaseDepth();
            bool result;
            diff = null;
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                diff = Diff(x, y, "Expression is null");
                return false;
            }
            if (_compareTypes && x.Type != y.Type)
            {
                diff = Diff(x, y, x.Type, y.Type, "Expression types do not match");
                return false;
            }
            if (_computeClosures)
            {
                x = ComputeIfClosure(x);
                y = ComputeIfClosure(y);
            }
            if (_computeConstantMembers)
            {
                x = ComputeIfConstantMember(x);
                y = ComputeIfConstantMember(y);
            }
            if (x.NodeType != y.NodeType)
            {
                diff = Diff(x, y, x.NodeType, y.NodeType, "Node types do not match");
                return false;
            }
            switch (x.NodeType)
            {
                case ExpressionType.Add:
                case ExpressionType.AddChecked:
                case ExpressionType.And:
                case ExpressionType.AndAlso:
                case ExpressionType.ArrayIndex:
                case ExpressionType.Coalesce:
                case ExpressionType.Divide:
                case ExpressionType.Equal:
                case ExpressionType.ExclusiveOr:
                case ExpressionType.GreaterThan:
                case ExpressionType.GreaterThanOrEqual:
                case ExpressionType.LeftShift:
                case ExpressionType.LessThan:
                case ExpressionType.LessThanOrEqual:
                case ExpressionType.Modulo:
                case ExpressionType.Multiply:
                case ExpressionType.MultiplyChecked:
                case ExpressionType.NotEqual:
                case ExpressionType.Or:
                case ExpressionType.OrElse:
                case ExpressionType.Power:
                case ExpressionType.RightShift:
                case ExpressionType.Subtract:
                case ExpressionType.SubtractChecked:
                case ExpressionType.Assign:
                case ExpressionType.AddAssign:
                case ExpressionType.AndAssign:
                case ExpressionType.DivideAssign:
                case ExpressionType.ExclusiveOrAssign:
                case ExpressionType.LeftShiftAssign:
                case ExpressionType.ModuloAssign:
                case ExpressionType.MultiplyAssign:
                case ExpressionType.OrAssign:
                case ExpressionType.PowerAssign:
                case ExpressionType.RightShiftAssign:
                case ExpressionType.SubtractAssign:
                case ExpressionType.AddAssignChecked:
                case ExpressionType.MultiplyAssignChecked:
                case ExpressionType.SubtractAssignChecked:
                    var xBinary = (BinaryExpression)x;
                    var yBinary = (BinaryExpression)y;
                    if (xBinary.IsLifted != yBinary.IsLifted)
                    {
                        diff = Diff(xBinary, yBinary, xBinary.IsLifted, yBinary.IsLifted, "IsLifted values do not match");
                        return false;
                    }
                    if (xBinary.IsLiftedToNull != yBinary.IsLiftedToNull)
                    {
                        diff = Diff(xBinary, yBinary, xBinary.IsLiftedToNull, yBinary.IsLiftedToNull, "IsLiftedToNull values do not match");
                        return false;
                    }
                    return AreEqual(xBinary.Left, yBinary.Left, context, out diff)
                        && AreEqual(xBinary.Right, yBinary.Right, context, out diff)
                        && AreEqual(xBinary.Conversion, yBinary.Conversion, context, out diff)
                        && AreEqual(xBinary.Method, yBinary.Method, x, y, out diff);
                case ExpressionType.ArrayLength:
                case ExpressionType.Convert:
                case ExpressionType.ConvertChecked:
                case ExpressionType.Negate:
                case ExpressionType.UnaryPlus:
                case ExpressionType.NegateChecked:
                case ExpressionType.Not:
                case ExpressionType.Quote:
                case ExpressionType.TypeAs:
                case ExpressionType.Decrement:
                case ExpressionType.Increment:
                case ExpressionType.PreIncrementAssign:
                case ExpressionType.PreDecrementAssign:
                case ExpressionType.PostIncrementAssign:
                case ExpressionType.PostDecrementAssign:
                case ExpressionType.OnesComplement:
                case ExpressionType.IsTrue:
                case ExpressionType.IsFalse:
                case ExpressionType.Unbox:
                case ExpressionType.Throw:
                    var xUnary = (UnaryExpression)x;
                    var yUnary = (UnaryExpression)y;
                    if (xUnary.IsLifted != yUnary.IsLifted)
                    {
                        diff = Diff(xUnary, yUnary, xUnary.IsLifted, yUnary.IsLifted, "IsLifted values do not match");
                        return false;
                    }
                    if (xUnary.IsLiftedToNull != yUnary.IsLiftedToNull)
                    {
                        diff = Diff(xUnary, yUnary, xUnary.IsLiftedToNull, yUnary.IsLiftedToNull, "IsLiftedToNull values do not match");
                        return false;
                    }
                    return AreEqual(xUnary.Operand, yUnary.Operand, context, out diff);
                case ExpressionType.Call:
                    var xMethodCall = (MethodCallExpression)x;
                    var yMethodCall = (MethodCallExpression)y;
                    return AreEqual(xMethodCall.Method, yMethodCall.Method, x, y, out diff)
                        && AreEqual(xMethodCall.Object, yMethodCall.Object, context, out diff)
                        && AreEqual(xMethodCall.Arguments, yMethodCall.Arguments, x, y, context, out diff);
                case ExpressionType.Conditional:
                    var xConditional = (ConditionalExpression)x;
                    var yConditional = (ConditionalExpression)y;
                    return AreEqual(xConditional.Test, yConditional.Test, context, out diff)
                        && AreEqual(xConditional.IfFalse, yConditional.IfFalse, context, out diff)
                        && AreEqual(xConditional.IfTrue, yConditional.IfTrue, context, out diff);
                case ExpressionType.Constant:
                    var xConstant = (ConstantExpression)x;
                    var yConstant = (ConstantExpression)y;
                    if (!context.ConstantComparer.Equals(xConstant.Value, yConstant.Value))
                    {
                        diff = Diff(x, y, "Constants are different");
                        return false;
                    }
                    return true;
                case ExpressionType.Invoke:
                    var xInvocation = (InvocationExpression)x;
                    var yInvocation = (InvocationExpression)y;
                    return AreEqual(xInvocation.Expression, yInvocation.Expression, context, out diff)
                        && AreEqual(xInvocation.Arguments, yInvocation.Arguments, x, y, context, out diff);
                case ExpressionType.Lambda:
                    var xLambda = (LambdaExpression)x;
                    var yLambda = (LambdaExpression)y;
                    if (xLambda.TailCall != yLambda.TailCall)
                    {
                        diff = Diff(x, y, xLambda.TailCall, yLambda.TailCall, "TailCall values do not match");
                        return false;
                    }
                    if (_compareTypes && xLambda.ReturnType != yLambda.ReturnType)
                    {
                        diff = Diff(x, y, xLambda.ReturnType, yLambda.ReturnType, "Return types do not match");
                        return false;
                    }
                    var currentParameterMap = NewParameterMap(context.ParameterMap);
                    result =
                        AreEqualAndMap(xLambda.Parameters, yLambda.Parameters, x, y, currentParameterMap, context.LabelMap, out diff)
                        && AreEqual(xLambda.Body, yLambda.Body, context, out diff);
                    PopParameterMap(context.ParameterMap);
                    return result;
                case ExpressionType.ListInit:
                    var xListInit = (ListInitExpression)x;
                    var yListInit = (ListInitExpression)y;
                    return AreEqual(xListInit.NewExpression, yListInit.NewExpression, context, out diff)
                        && AreEqual(xListInit.Initializers, yListInit.Initializers, x, y, context, out diff);
                case ExpressionType.MemberAccess:
                    var xMember = (MemberExpression)x;
                    var yMember = (MemberExpression)y;
                    return AreEqual(xMember.Member, yMember.Member, x, y, out diff)
                        && AreEqual(xMember.Expression, yMember.Expression, context, out diff);
                case ExpressionType.MemberInit:
                    var xMemberInit = (MemberInitExpression)x;
                    var yMemberInit = (MemberInitExpression)y;
                    return AreEqual(xMemberInit.NewExpression, yMemberInit.NewExpression, context, out diff)
                        && AreEqual(xMemberInit.Bindings, yMemberInit.Bindings, x, y, context, out diff);
                case ExpressionType.New:
                    var xNew = (NewExpression)x;
                    var yNew = (NewExpression)y;
                    return AreEqual(xNew.Arguments, yNew.Arguments, x, y, context, out diff)
                        && (!_compareTypes || AreEqual(xNew.Constructor, yNew.Constructor, x, y, out diff));
                case ExpressionType.NewArrayInit:
                case ExpressionType.NewArrayBounds:
                    var xNewArray = (NewArrayExpression)x;
                    var yNewArray = (NewArrayExpression)y;
                    return AreEqual(xNewArray.Expressions, yNewArray.Expressions, x, y, context, out diff);
                case ExpressionType.Parameter:
                    var xParameter = (ParameterExpression)x;
                    var yParameter = (ParameterExpression)y;
                    if (xParameter.IsByRef != yParameter.IsByRef)
                    {
                        diff = Diff(xParameter, yParameter, xParameter.IsByRef, yParameter.IsByRef, "IsByRef values do not match");
                        return false;
                    }
                    return AreEqual(xParameter, yParameter, x, y, context.ParameterMap.Peek(), out diff);
                case ExpressionType.TypeEqual:
                case ExpressionType.TypeIs:
                    var xTypeBinary = (TypeBinaryExpression)x;
                    var yTypeBinary = (TypeBinaryExpression)y;
                    if (xTypeBinary.TypeOperand != yTypeBinary.TypeOperand)
                    {
                        diff = Diff(xTypeBinary, yTypeBinary, xTypeBinary.TypeOperand, yTypeBinary.TypeOperand, "Types do not match");
                        return false;
                    }
                    return AreEqual(xTypeBinary.Expression, yTypeBinary.Expression, context, out diff);
                case ExpressionType.Block:
                    var xBlock = (BlockExpression)x;
                    var yBlock = (BlockExpression)y;
                    currentParameterMap = NewParameterMap(context.ParameterMap);
                    result = AreEqualAndMap(xBlock.Variables, yBlock.Variables, x, y, currentParameterMap, context.LabelMap, out diff)
                        && AreEqual(xBlock.Expressions, yBlock.Expressions, x, y, context, out diff)
                        && AreEqual(xBlock.Result, yBlock.Result, context, out diff);
                    PopParameterMap(context.ParameterMap);
                    return result;
                case ExpressionType.Default:
                    var xDefault = (DefaultExpression)x;
                    var yDefault = (DefaultExpression)y;
                    return true;
                case ExpressionType.Goto:
                    var xGoto = (GotoExpression)x;
                    var yGoto = (GotoExpression)y;
                    if (xGoto.Kind != yGoto.Kind)
                    {
                        diff = Diff(x, y, xGoto.Kind, yGoto.Kind, "Goto kinds do not match");
                        return false;
                    }
                    return AreEqual(xGoto.Value, yGoto.Value, context, out diff)
                        && AreEqual(xGoto.Target, yGoto.Target, x, y, context.LabelMap, out diff);
                case ExpressionType.Label:
                    var xLabel = (LabelExpression)x;
                    var yLabel = (LabelExpression)y;
                    return AreEqualAndMap(xLabel.Target, yLabel.Target, context.LabelMap, out diff)
                        && AreEqual(xLabel.DefaultValue, yLabel.DefaultValue, context, out diff);
                case ExpressionType.Index:
                    var xIndex = (IndexExpression)x;
                    var yIndex = (IndexExpression)y;
                    return AreEqual(xIndex.Indexer, yIndex.Indexer, x, y, out diff)
                        && AreEqual(xIndex.Object, yIndex.Object, context, out diff)
                        && AreEqual(xIndex.Arguments, yIndex.Arguments, x, y, context, out diff);
                case ExpressionType.RuntimeVariables:
                    var xRuntime = (RuntimeVariablesExpression)x;
                    var yRuntime = (RuntimeVariablesExpression)y;
                    return AreEqual(xRuntime.Variables, yRuntime.Variables, x, y, context, out diff);
                case ExpressionType.Loop:
                    var xLoop = (LoopExpression)x;
                    var yLoop = (LoopExpression)y;
                    return
                        AreEqualAndMap(xLoop.BreakLabel, yLoop.BreakLabel, context.LabelMap, out diff)
                        && AreEqualAndMap(xLoop.ContinueLabel, yLoop.ContinueLabel, context.LabelMap, out diff)
                        && AreEqual(xLoop.Body, yLoop.Body, context, out diff);
                case ExpressionType.Switch:
                    var xSwitch = (SwitchExpression)x;
                    var ySwitch = (SwitchExpression)y;
                    return AreEqual(xSwitch.SwitchValue, ySwitch.SwitchValue, context, out diff)
                        && AreEqual(xSwitch.Comparison, ySwitch.Comparison, x, y, out diff)
                        && AreEqual(xSwitch.Cases, ySwitch.Cases, x, y, context, out diff)
                        && AreEqual(xSwitch.DefaultBody, ySwitch.DefaultBody, context, out diff);
                case ExpressionType.Try:
                    var xTry = (TryExpression)x;
                    var yTry = (TryExpression)y;
                    return AreEqual(xTry.Body, yTry.Body, context, out diff)
                        && AreEqual(xTry.Handlers, yTry.Handlers, x, y, context, out diff)
                        && AreEqual(xTry.Finally, yTry.Finally, context, out diff)
                        && AreEqual(xTry.Fault, yTry.Fault, context, out diff);
                case ExpressionType.DebugInfo:
                    var xDebugInfo = (DebugInfoExpression)x;
                    var yDebugInfo = (DebugInfoExpression)y;
                    throw new NotSupportedException();
#if NET45
                case ExpressionType.Dynamic:
                    var xDynamic = (DynamicExpression)x;
                    var yDynamic = (DynamicExpression)y;
                    throw new NotSupportedException();
#endif
                case ExpressionType.Extension:
                    throw new NotSupportedException();
                default:
                    return false;
            }
        }

        private void PopParameterMap([NotNull]Stack<IDictionary<ParameterExpression, ParameterExpression>> parameterMap)
        {
            parameterMap.Pop();
        }

        [NotNull]
        private IDictionary<ParameterExpression, ParameterExpression> NewParameterMap(
            [NotNull]Stack<IDictionary<ParameterExpression, ParameterExpression>> parameterMap)
        {
            var current = parameterMap.Peek();
            var newMap = current != null
                ? new Dictionary<ParameterExpression, ParameterExpression>(current)
                : new Dictionary<ParameterExpression, ParameterExpression>();
            parameterMap.Push(newMap);
            return newMap;
        }

        private ExpressionDifference Diff(Expression first, Expression second, string reason)
        {
            return new ExpressionDifference(first, second, reason);
        }

        private SpecificExpressionDifference<T> Diff<T>(Expression first, Expression second, T firstElement, T secondElement, string reason)
        {
            return new SpecificExpressionDifference<T>(first, second, firstElement, secondElement, reason);
        }

        private SpecificExpressionDifferenceWithMatch<T> Diff<T>(Expression first, Expression second, T firstElement, T secondElement, T match, string reason)
        {
            return new SpecificExpressionDifferenceWithMatch<T>(first, second, firstElement, secondElement, match, reason);
        }

        /// <summary>
        /// Контекст сравнения
        /// </summary>
        public class Context
        {
            /// <summary>
            /// Сопоставление параметров
            /// </summary>
            [NotNull]
            public Stack<IDictionary<ParameterExpression, ParameterExpression>> ParameterMap { get; set; }

            /// <summary>
            /// Сопоставление меток
            /// </summary>
            [NotNull]
            public IDictionary<LabelTarget, LabelTarget> LabelMap { get; set; }

            /// <summary>
            /// Сравнитель констант
            /// </summary>
            [NotNull]
            public IEqualityComparer<object> ConstantComparer { get; set; }

            /// <summary>
            /// Текущая глубина стека вызовов
            /// </summary>
            public int Depth { get; set; }

            /// <summary>
            /// Максимально допустимая глубина стека вызовов
            /// </summary>
            public int MaxDepth { get; set; } = 1000;

            /// <summary>
            /// Сравнитель констант по умолчанию - полагается на object.Equals
            /// </summary>
            public static IEqualityComparer<object> SingleComparer = EqualityComparer<object>.Default;
            /// <summary>
            /// Сравнитель констант, сравнивающий объекты, реализующие IEnumerable, поэлементно
            /// </summary>
            public static IEqualityComparer<object> CollectionElementComparer = new CollectionElementComparer();

            /// <summary>Создать контекст сравнения</summary>
            /// <param name="parameterMap">Сопоставление параметров</param>
            /// <param name="labelMap">Сопоставление меток</param>
            public Context(
                IDictionary<ParameterExpression, ParameterExpression> parameterMap = null,
                IDictionary<LabelTarget, LabelTarget> labelMap = null)
                : this(MakeStack(parameterMap), labelMap)
            {
            }

            private static Stack<IDictionary<ParameterExpression, ParameterExpression>> MakeStack(IDictionary<ParameterExpression, ParameterExpression> parameterMap)
            {
                var stack = new Stack<IDictionary<ParameterExpression, ParameterExpression>>();
                stack.Push(parameterMap ?? new Dictionary<ParameterExpression, ParameterExpression>());
                return stack;
            }

            /// <summary>Создать контекст сравнения</summary>
            /// <param name="parameterMap">Сопоставление параметров</param>
            /// <param name="labelMap">Сопоставление меток</param>
            public Context(
                Stack<IDictionary<ParameterExpression, ParameterExpression>> parameterMap,
                IDictionary<LabelTarget, LabelTarget> labelMap)
            {
                ParameterMap = parameterMap ?? MakeStack(null);
                LabelMap = labelMap ?? new Dictionary<LabelTarget, LabelTarget>();
                ConstantComparer = SingleComparer;
            }

            internal void IncreaseDepth()
            {
                if (++Depth > MaxDepth)
                {
                    throw new InvalidOperationException("Call stack depth limit exceeded");
                }
            }
        }

        private class CollectionElementComparer : IEqualityComparer<object>
        {
            public new bool Equals(object x, object y)
            {
                if (x == null && y == null)
                {
                    return true;
                }
                if (x == null || y == null)
                {
                    return false;
                }
                if (typeof(IEnumerable).GetTypeInfo().IsAssignableFrom(x.GetType()) && typeof(IEnumerable).GetTypeInfo().IsAssignableFrom(y.GetType()))
                {
                    return ((IEnumerable)x).Cast<object>().SequenceEqual(((IEnumerable)y).Cast<object>(), this);
                }
                return x.Equals(y);
            }

            public int GetHashCode(object obj)
            {
                throw new NotImplementedException();
            }
        }
    }
}
