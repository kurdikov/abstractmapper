﻿namespace TsSoft.Expressions.Helpers
{
    using System.Linq.Expressions;

    /// <summary>
    /// Удаляет элементы Convert из выражения
    /// </summary>
    public interface IConvertRemover
    {
        /// <summary>
        /// Удалить Convert к интерфейсам из выражения
        /// </summary>
        /// <typeparam name="TExpression">Тип выражения</typeparam>
        /// <param name="expression">Выражение</param>
        /// <returns>Преобразованное выражение</returns>
        TExpression RemoveInterfaceCast<TExpression>(TExpression expression) where TExpression : LambdaExpression;
    }
}
