namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// ��������� �� ��������� MemberInfo, ��������� ������ �����
    /// </summary>
    public class MemberInfoNameComparer : IEqualityComparer<MemberInfo>, IEqualityComparer<PropertyInfo>, IEqualityComparer<ValueHoldingMember>
    {
        /// <summary>
        /// ��������� �� ��������� MemberInfo, ��������� ������ �����
        /// </summary>
        [NotNull]
        public static MemberInfoNameComparer Instance = new MemberInfoNameComparer();

        /// <summary>
        /// ����� �� ����� �������
        /// </summary>
        /// <returns></returns>
        public bool Equals(PropertyInfo x, PropertyInfo y)
        {
            return Equals((MemberInfo) x, y);
        }

        /// <summary>
        /// �������� ��� ����� ��������
        /// </summary>
        public int GetHashCode(PropertyInfo obj)
        {
            return GetHashCode((MemberInfo) obj);
        }

        /// <summary>
        /// ����� �� ����� ������
        /// </summary>
        public bool Equals(MemberInfo x, MemberInfo y)
        {
            return x == null && y == null || x != null && y != null && x.Name.Equals(y.Name, StringComparison.Ordinal);
        }

        /// <summary>
        /// �������� ��� ����� �����
        /// </summary>
        public int GetHashCode(MemberInfo obj)
        {
            return obj != null ? obj.Name.GetHashCode() : 0;
        }

        /// <summary>
        /// ����� �� ����� ������
        /// </summary>
        public bool Equals(ValueHoldingMember x, ValueHoldingMember y)
        {
            return x == null && y == null || x != null && y != null && x.Name.Equals(y.Name, StringComparison.Ordinal);
        }

        /// <summary>
        /// �������� ��� ����� �����
        /// </summary>
        public int GetHashCode(ValueHoldingMember obj)
        {
            return obj != null ? obj.Name.GetHashCode() : 0;
        }
    }
}
