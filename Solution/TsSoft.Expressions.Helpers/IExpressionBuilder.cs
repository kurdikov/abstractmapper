﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Построитель выражений
    /// </summary>
    public interface IExpressionBuilder
    {
        /// <summary>
        /// Построить инклюд-выражение по линейному пути (используется Enumerable.Select, не используется Enumerable.SelectMany, результат может иметь тип IEnumerable{IEnumerable{...}})
        /// </summary>
        /// <typeparam name="TEntity">Тип входа</typeparam>
        /// <param name="properties">Линейный путь</param>
        /// <returns>Лямбда-выражение</returns>
        [CanBeNull]
        Expression<Func<TEntity, object>> BuildIncludeByPath<TEntity>(IEnumerable<ValueHoldingMember> properties);

        /// <summary>
        /// Построить инклюд-выражение по линейному пути (используется Enumerable.Select, не используется Enumerable.SelectMany, результат может иметь тип IEnumerable{IEnumerable{...}})
        /// </summary>
        /// <param name="paramType">Тип входа</param>
        /// <param name="properties">Линейный путь</param>
        /// <returns>Лямбда-выражение</returns>
        [CanBeNull]
        LambdaExpression BuildIncludeByPath([NotNull]Type paramType, IEnumerable<ValueHoldingMember> properties);

        /// <summary>
        /// Построить выражение обращения к свойству o => o.Property
        /// </summary>
        /// <param name="property">Свойство</param>
        /// <returns>Лямбда-выражение</returns>
        [NotNull]
        LambdaExpression BuildPropertyExpression(PropertyInfo property);

        /// <summary>
        /// Построить выражение обращения к члену o => o.Property
        /// </summary>
        /// <param name="member">Член</param>
        /// <returns>Лямбда-выражение</returns>
        [NotNull]
        LambdaExpression BuildMemberAccessExpression([NotNull]ValueHoldingMember member);

        /// <summary>
        /// Построить выражение обращения к члену o => o.Property
        /// </summary>
        /// <param name="parameterType">Тип параметра</param>
        /// <param name="member">Член</param>
        /// <returns>Лямбда-выражение</returns>
        [NotNull]
        LambdaExpression BuildMemberAccessExpression([NotNull]Type parameterType, [NotNull]ValueHoldingMember member);

        /// <summary>
        /// Построить выражение обращения к свойству o => o.Property
        /// </summary>
        /// <param name="declaringType">Тип параметра</param>
        /// <param name="property">Имя свойства</param>
        /// <returns>Лямбда-выражение</returns>
        [NotNull]
        LambdaExpression BuildPropertyExpression(Type declaringType, string property);

        /// <summary>
        /// Построить по списку свойств (A, B, C, D) выражение x?.A?.Select(a => a?.B)?.SelectMany(b => b?.C)?.Select(c => c?.D);
        /// </summary>
        [NotNull]
        Expression BuildPathWithNullChecks(Expression start, IEnumerable<ValueHoldingMember> path, bool checkFirst = true);

        /// <summary>
        /// Построить по списку свойств (A, B, C, D) выражение x?.A?.Select(a => a?.B)?.SelectMany(b => b?.C)?.Select(c => c?.D);
        /// </summary>
        [NotNull]
        Expression BuildPathWithNullChecks(Expression start, IEnumerable<PathElement> path, bool checkFirst = true);

        /// <summary>
        /// Построить по списку свойств (A, B, C, D) выражение x?.A?.Select(a => a?.B)?.SelectMany(b => b?.C)?.Select(c => c?.D);
        /// </summary>
        [NotNull]
        Expression BuildPathWithNullChecks(Expression start, IEnumerable<PathElement> path, bool checkFirst, Type resultType);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="property">Свойство</param>
        /// <param name="pattern">Искомая строка</param>
        /// <returns>e => e.Property.ToString().Contains(pattern)</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereContains<TEntity>(PropertyInfo property, string pattern);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <param name="pattern">Искомая строка</param>
        /// <returns>e => e.Property.ToString().Contains(pattern)</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereContains<TEntity>(string propertyName, string pattern);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="properties">Свойства</param>
        /// <param name="pattern">Искомая строка</param>
        /// <returns>e => e.Property[0].ToString().Contains(pattern) || e.Property[1].ToString().Contains(pattern) || ...</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereAnyContains<TEntity>(IEnumerable<PropertyInfo> properties, string pattern);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="properties">Имена свойств</param>
        /// <param name="pattern">Искомая строка</param>
        /// <returns>e => e.Property[0].ToString().Contains(pattern) || e.Property[1].ToString().Contains(pattern) || ...</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereAnyContains<TEntity>(IEnumerable<string> properties, string pattern);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <typeparam name="TProperty">Тип свойства</typeparam>
        /// <param name="property">Свойство</param>
        /// <param name="value">Значение</param>
        /// <returns>e => e.Property == value</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereEquals<TEntity, TProperty>(PropertyInfo property, TProperty value);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <typeparam name="TProperty">Тип свойства</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <param name="value">Значение</param>
        /// <returns>e => e.Property == value</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereEquals<TEntity, TProperty>(string propertyName, TProperty value);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <param name="value">Значение</param>
        /// <returns>e => e.Property == value</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereEquals<TEntity>(string propertyName, object value);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="property">Свойство</param>
        /// <param name="value">Значение</param>
        /// <returns>e => e.Property == value</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereEquals<TEntity>(PropertyInfo property, object value);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="values">Имена и значения свойств</param>
        /// <returns>e => e.Property1 == value1 and e.Property2 == value2 and ...</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereEquals<TEntity>(IEnumerable<KeyValuePair<string, object>> values);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="values">Свойства и их значения</param>
        /// <returns>e => e.Property1 == value1 and e.Property2 == value2 and ...</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereEquals<TEntity>(IEnumerable<KeyValuePair<PropertyInfo, object>> values);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="property">Свойство</param>
        /// <param name="values">Значения</param>
        /// <returns>e => e.Property == value1 || e.Property == value2 || ...</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereIn<TEntity>(PropertyInfo property, IEnumerable<object> values);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <typeparam name="TValue">Тип значения свойства</typeparam>
        /// <param name="property">Свойство</param>
        /// <param name="values">Значения</param>
        /// <returns>e => e.Property == value1 || e.Property == value2 || ...</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereIn<TEntity, TValue>(PropertyInfo property, IEnumerable<TValue> values);

        /// <summary>
        /// Построить выражение для for-цикла
        /// </summary>
        /// <param name="init">Инициализация перед циклом</param>
        /// <param name="check">Условие выполнения шага</param>
        /// <param name="step">Перход к следующему шагу</param>
        /// <param name="body">Тело</param>
        /// <param name="variables">Переменные цикла</param>
        [NotNull]
        Expression For(
            [CanBeNull]Expression init,
            [CanBeNull]Expression check,
            [CanBeNull]Expression step,
            [CanBeNull]Expression body,
            [NotNull]params ParameterExpression[] variables);

        /// <summary>
        /// Построить выражение для for-цикла с одной целочисленной переменной
        /// </summary>
        /// <param name="variable">Переменная цикла</param>
        /// <param name="first">Первое значение</param>
        /// <param name="afterLast">Значение, следующее за последним</param>
        /// <param name="body">Тело</param>
        [NotNull]
        Expression ForRange(
            [NotNull]ParameterExpression variable,
            [NotNull]Expression first,
            [NotNull]Expression afterLast,
            [NotNull]Expression body);

        /// <summary>
        /// Построить выражение для foreach-цикла
        /// </summary>
        /// <typeparam name="T">Тип элемента коллекции</typeparam>
        /// <param name="collection">Коллекция, по которой проходит цикл</param>
        /// <param name="current">Параметр цикла</param>
        /// <param name="body">Тело цикла</param>
        /// <returns>Выражение для цикла</returns>
        [NotNull]
        Expression Foreach<T>(Expression collection, ParameterExpression current, Expression body);

        /// <summary>
        /// Построить выражение для foreach-цикла
        /// </summary>
        /// <param name="collection">Коллекция, по которой проходит цикл</param>
        /// <param name="current">Параметр цикла</param>
        /// <param name="body">Тело цикла</param>
        /// <param name="elementType">Тип элемента коллекции</param>
        /// <returns>Выражение для цикла</returns>
        [NotNull]
        Expression Foreach(Expression collection, ParameterExpression current, Expression body, Type elementType);

        /// <summary>
        /// Отфильтровать null-элементы из выражения-коллекции
        /// </summary>
        /// <param name="collection">Выражение-коллекция</param>
        /// <returns>coll.Where(x => x != null)</returns>
        [NotNull]
        Expression WhereNotNull(Expression collection);

        /// <summary>
        /// Построить условие того, что проход по пути не выбросит <see cref="NullReferenceException"/>
        /// </summary>
        /// <param name="path">Путь</param>
        /// <param name="checkPathResult">Проверить также результат прохода по пути</param>
        [CanBeNull]
        Expression BuildPreconditionExpression([NotNull]ParsedPath path, bool checkPathResult);

        /// <summary>
        /// Построить условие того, что на пути встречается null
        /// </summary>
        /// <param name="path">Путь</param>
        /// <param name="checkPathResult">Проверить также результат прохода по пути</param>
        [CanBeNull]
        Expression BuildPathLeadsToNullExpression([NotNull]ParsedPath path, bool checkPathResult);
    }
}
