﻿namespace TsSoft.Expressions.Helpers
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Преобразует путь в лямбду
    /// </summary>
    public interface IPathBuilder
    {
        /// <summary>
        /// Преобразовать разобранный плоский путь в лямбду
        /// </summary>
        LambdaExpression Build([NotNull]IEnumerable<ValueHoldingMember> flatPath);

        /// <summary>
        /// Преобразовать разобранный плоский путь в лямбду
        /// </summary>
        LambdaExpression Build([NotNull]IEnumerable<PropertyInfo> flatPath);

        /// <summary>
        /// Преобразовать разобранный путь в лямбду
        /// </summary>
        LambdaExpression Build([NotNull]ParsedPath path);
    }
}
