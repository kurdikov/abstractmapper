﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Построитель выражений
    /// </summary>
    public class ExpressionBuilder : IExpressionBuilder
    {
        [NotNull]private readonly MethodInfo _foreachMethod;

        [NotNull]private readonly IMemberInfoHelper _memberInfoHelper;
        [NotNull]private readonly IMemberInfoLibrary _memberInfoLibrary;

        /// <summary>
        /// Построитель выражений
        /// </summary>
        /// <param name="memberInfoHelper">Хелпер для извлечения методов</param>
        /// <param name="memberInfoLibrary">Библиотека методов</param>
        public ExpressionBuilder([NotNull]IMemberInfoHelper memberInfoHelper, [NotNull]IMemberInfoLibrary memberInfoLibrary)
        {
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (memberInfoLibrary == null) throw new ArgumentNullException("memberInfoLibrary");
            _memberInfoHelper = memberInfoHelper;
            _memberInfoLibrary = memberInfoLibrary;

            _foreachMethod = memberInfoHelper.GetGenericDefinitionMethodInfo(() => Foreach<object>(null, null, null));
        }

        /// <summary>
        /// Построить выражение по линейному пути
        /// </summary>
        /// <typeparam name="TEntity">Тип входа</typeparam>
        /// <param name="properties">Линейный путь</param>
        /// <returns>Лямбда-выражение</returns>
        public Expression<Func<TEntity, object>> BuildIncludeByPath<TEntity>(IEnumerable<ValueHoldingMember> properties)
        {
            return BuildIncludeByPath(typeof(TEntity), properties) as Expression<Func<TEntity, object>>;
        }

        /// <summary>
        /// Построить выражение по линейному пути
        /// </summary>
        /// <param name="paramType">Тип входа</param>
        /// <param name="properties">Линейный путь</param>
        /// <returns>Лямбда-выражение</returns>
        public LambdaExpression BuildIncludeByPath(Type paramType, IEnumerable<ValueHoldingMember> properties)
        {
            if (properties == null)
            {
                return null;
            }
            using (var enumerator = properties.GetEnumerator())
            {
                if (!enumerator.MoveNext())
                {
                    return null;
                }
                var param = Expression.Parameter(paramType, "p");
                var resultBody = BuildIncludeByPath(param, enumerator);
                if (resultBody.Type.GetTypeInfo().IsValueType)
                {
                    resultBody = Expression.Convert(resultBody, typeof(object));
                }
                return Expression.Lambda(typeof(Func<,>).MakeGenericType(paramType, typeof(object)), resultBody, new[] { param });
            }
        }

        [NotNull]
        private Expression BuildIncludeByPath([NotNull]Expression prefix, [NotNull]IEnumerator<ValueHoldingMember> enumerator)
        {
            Expression result;
            var current = enumerator.Current;
            if (current == null)
            {
                throw new InvalidOperationException("Path contains null property");
            }
            if (current.DeclaringType == null)
            {
                throw new InvalidOperationException("Path contains property without declaring type");
            }
            if (current.DeclaringType.GetTypeInfo().IsAssignableFrom(prefix.Type))
            {
                var nextPrefix = Expression.MakeMemberAccess(prefix, current.Member);
                result = !enumerator.MoveNext() 
                    ? nextPrefix 
                    : BuildIncludeByPath(nextPrefix, enumerator);
            }
            else if (typeof(IEnumerable<>).MakeGenericType(current.DeclaringType).GetTypeInfo().IsAssignableFrom(prefix.Type))
            {
                var param = Expression.Parameter(current.DeclaringType);
                var selectInternalExpr = BuildIncludeByPath(param, enumerator);
                var selectType = selectInternalExpr.Type;
                var funcType = typeof (Func<,>).MakeGenericType(current.DeclaringType, selectType);
                var selectExpr = Expression.Lambda(funcType, selectInternalExpr, param);
                var select = _memberInfoLibrary.EnumerableSelect(current.DeclaringType, selectType);
                result = Expression.Call(select, new[] {prefix, selectExpr});
            }
            else
            {
                throw new ArgumentException(string.Format(
                    "Property type mismatch: property {0} is declared on type {1} but expression prefix has type {2}",
                        current.Name,
                        current.DeclaringType.Name,
                        prefix.Type.Name));
            }
            return result;
        }

        [NotNull]
        private Expression MethodWithLambda(
            [NotNull]Expression source,
            [NotNull]Type sourceSingleType,
            [NotNull]ValueHoldingMember property,
            [NotNull]MethodInfo method,
            [NotNull]Type resultType,
            [NotNull]Expression defaultValue)
        {
            var param = Expression.Parameter(sourceSingleType);
            return Expression.Call(
                    method,
                    source,
                    Expression.Lambda(
                        typeof (Func<,>).MakeGenericType(sourceSingleType, resultType),
                        CheckNull(resultType, Expression.MakeMemberAccess(param, property.Member), param, defaultValue),
                        param));
        }

        [NotNull]
        private Expression DefaultDefaultValue([NotNull]Type type)
        {
            return type.GetTypeInfo().IsValueType
                       ? (Expression) Expression.Default(type)
                       : Expression.Constant(null, type);
        }

        [NotNull]
        private Expression CheckNull([NotNull]Type type, [NotNull]Expression computed, [NotNull]Expression checkedObject, Expression defaultValue = null)
        {
            return Expression.Condition(
                Expression.NotEqual(
                    checkedObject,
                    Expression.Constant(null, typeof(object))),
                computed.Type != type ? Expression.Convert(computed, type) : computed,
                defaultValue ?? DefaultDefaultValue(type));
        }

        [NotNull]
        private Expression AddCondition(
            [NotNull]Expression prefix,
            [NotNull]Type prefixSingleType,
            ref bool isCollection,
            ref bool checkNull,
            [NotNull]PathElement conditionedElement,
            [NotNull]Stack<Expression> checkedPrefixes)
        {
            Expression nextPrefix = prefix;
            if (conditionedElement.Conditions != null && conditionedElement.Conditions.Any())
            {
                if (!isCollection)
                {
                    throw new InvalidOperationException("Where condition on non-collection");
                }

                foreach (var condition in conditionedElement.Conditions)
                {
                    if (condition == null)
                    {
                        continue;
                    }
                    var method = condition.ConditionMethod
                        ?? _memberInfoLibrary.EnumerableWhere(prefixSingleType);
                    var paramCount = method.GetParameters().Length;
                    if (paramCount == 2)
                    {
                        if (condition.ServerCondition == null)
                        {
                            throw new InvalidOperationException("Null server condition");
                        }
                        nextPrefix = Expression.Call(
                            method,
                            prefix,
                            condition.ServerCondition);
                    }
                    else if (paramCount == 1)
                    {
                        nextPrefix = Expression.Call(
                            method,
                            prefix);
                    }
                    else
                    {
                        throw new NotSupportedException(string.Format("Method {0} with {1} parameters is not supported on paths", method, paramCount));
                    }
                }
                isCollection = nextPrefix.Type.IsGenericEnumerable();
                if (checkNull)
                {
                    checkedPrefixes.Push(prefix);
                }
                checkNull = !isCollection && nextPrefix.Type.IsNullableType();
            }
            else
            {
                nextPrefix = prefix;
            }
            return nextPrefix;
        }

        [NotNull]
        private Expression BuildPathWithNullChecks(
            [NotNull]Expression prefix,
            [NotNull]Type prefixSingleType,
            bool isCollection,
            bool checkNull,
            [NotNull]IEnumerable<PathElement> path,
            Type resultType = null)
        {
            Expression nextPrefix = prefix;
            var checkedPrefixes = new Stack<Expression>();
            foreach (var element in path)
            {
                if (element == null)
                {
                    throw new InvalidOperationException("Path contains null");
                }
                if (checkNull)
                {
                    checkedPrefixes.Push(prefix);
                }
                var property = element.Step;
                if (!property.DeclaringType.GetTypeInfo().IsAssignableFrom(prefixSingleType))
                {
                    throw new ArgumentException(string.Format(
                        "Property {0} is declared on {1}, but currently built expression {2} has underlying type {3}",
                        property, property.DeclaringType, prefix, prefixSingleType));
                }
                Type nextSingleType;
                bool nextIsCollection;
                if (property.ValueType.IsGenericEnumerable())
                {
                    nextSingleType = property.ValueType.GetGenericEnumerableArgument();
                    nextPrefix = isCollection
                        ? MethodWithLambda(nextPrefix, prefixSingleType, property,
                            _memberInfoLibrary.EnumerableSelectMany(prefixSingleType, nextSingleType),
                            typeof(IEnumerable<>).MakeGenericType(nextSingleType),
                            Expression.Call(
                                _memberInfoLibrary.EnumerableEmpty(nextSingleType)))
                        : Expression.MakeMemberAccess(nextPrefix, property.Member);
                    nextIsCollection = true;
                }
                else
                {
                    nextPrefix = isCollection
                        ? MethodWithLambda(nextPrefix, prefixSingleType, property,
                            _memberInfoLibrary.EnumerableSelect(prefixSingleType, property.ValueType),
                            property.ValueType,
                            DefaultDefaultValue(property.ValueType))
                        : Expression.MakeMemberAccess(nextPrefix, property.Member);
                    nextSingleType = property.ValueType;
                    nextIsCollection = isCollection;
                }
                checkNull = !isCollection && nextPrefix.Type.IsNullableType();
                nextPrefix = AddCondition(nextPrefix, nextSingleType, ref nextIsCollection, ref checkNull, element, checkedPrefixes);

                prefix = nextPrefix;
                prefixSingleType = nextSingleType;
                isCollection = nextIsCollection;
            }
            resultType = resultType ?? prefix.Type;
            if (resultType != prefix.Type)
            {
                prefix = Expression.Convert(prefix, resultType);
            }
            foreach (var checkedPrefix in checkedPrefixes)
            {
                if (checkedPrefix == null)
                {
                    continue;
                }
                prefix = CheckNull(resultType, prefix, checkedPrefix);
            }
            return prefix;
        }

        /// <summary>
        /// Построить по списку свойств (A, B, C, D) выражение x?.A?.Select(a => a?.B)?.SelectMany(b => b?.C)?.Select(c => c?.D);
        /// </summary>
        public Expression BuildPathWithNullChecks(Expression start, IEnumerable<ValueHoldingMember> properties, bool checkFirst = true)
        {
            return BuildPathWithNullChecks(start, (properties ?? Enumerable.Empty<ValueHoldingMember>()).Select(
                p =>
                {
                    if (p == null)
                    {
                        throw new InvalidPathException("Path contains null element");
                    }
                    return new PathElement(p);
                }), checkFirst);
        }

        /// <summary>
        /// Построить по списку свойств (A, B, C, D) выражение x?.A?.Select(a => a?.B)?.SelectMany(b => b?.C)?.Select(c => c?.D);
        /// </summary>
        public Expression BuildPathWithNullChecks(Expression start, IEnumerable<PathElement> path, bool checkFirst = true)
        {
            return BuildPathWithNullChecks(start, path, checkFirst, null);
        }

        /// <summary>
        /// Построить по списку свойств (A, B, C, D) выражение x?.A?.Select(a => a?.B)?.SelectMany(b => b?.C)?.Select(c => c?.D);
        /// </summary>
        public Expression BuildPathWithNullChecks(Expression start, IEnumerable<PathElement> path, bool checkFirst, Type resultType)
        {
            if (start == null)
            {
                throw new ArgumentNullException("start");
            }
            if (path == null)
            {
                throw new ArgumentNullException("path");
            }
            var isCollection = start.Type.IsGenericEnumerable();
            var currentSingleType = isCollection ? start.Type.GetGenericEnumerableArgument() : start.Type;
            return BuildPathWithNullChecks(start, currentSingleType, isCollection, checkFirst, path, resultType);
        }

        /// <summary>
        /// Построить выражение обращения к свойству o => o.Property
        /// </summary>
        /// <param name="property">Свойство</param>
        /// <returns>Лямбда-выражение</returns>
        public LambdaExpression BuildPropertyExpression(PropertyInfo property)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            if (property.DeclaringType == null)
            {
                throw new InvalidOperationException("Property without declaring type");
            }
            var funcType = typeof (Func<,>).MakeGenericType(property.DeclaringType, property.PropertyType);
            var param = Expression.Parameter(property.DeclaringType, "p");
            var result = Expression.Lambda(funcType, Expression.Property(param, property), param);
            return result;
        }

        /// <summary>
        /// Построить выражение обращения к члену o => o.Property
        /// </summary>
        /// <param name="member">Член</param>
        /// <returns>Лямбда-выражение</returns>
        public LambdaExpression BuildMemberAccessExpression(ValueHoldingMember member)
        {
            return BuildMemberAccessExpression(member.DeclaringType, member);
        }

        /// <summary>
        /// Построить выражение обращения к члену o => o.Property
        /// </summary>
        /// <param name="parameterType">Тип параметра</param>
        /// <param name="member">Член</param>
        /// <returns>Лямбда-выражение</returns>
        public LambdaExpression BuildMemberAccessExpression(Type parameterType, ValueHoldingMember member)
        {
            var funcType = typeof(Func<,>).MakeGenericType(parameterType, member.ValueType);
            var param = Expression.Parameter(parameterType, "p");
            var result = Expression.Lambda(funcType, Expression.MakeMemberAccess(param, member.Member), param);
            return result;
        }

        private PropertyInfo GetProperty([NotNull]Type declaringType, string propertyName)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException("propertyName");
            }
            var property = declaringType.GetTypeInfo().GetProperty(propertyName);
            if (property == null)
            {
                throw new ArgumentException(string.Format("Property {0} not found in {1}", propertyName, declaringType));
            }
            return property;
        }

        /// <summary>
        /// Построить выражение обращения к свойству o => o.Property
        /// </summary>
        /// <param name="declaringType">Тип параметра</param>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Лямбда-выражение</returns>
        public LambdaExpression BuildPropertyExpression(Type declaringType, string propertyName)
        {
            if (declaringType == null)
            {
                throw new ArgumentNullException("declaringType");
            }
            return BuildPropertyExpression(GetProperty(declaringType, propertyName));
        }

        [NotNull]
        private Expression ContainsExpression(Expression param, [NotNull]PropertyInfo property, string pattern)
        {
            if (pattern == null)
            {
                throw new ArgumentNullException("pattern");
            }
            return Expression.Call(
                property.PropertyType == typeof(string)
                ? (Expression)Expression.Property(param, property)
                : Expression.Call(
                    Expression.Property(param, property),
                    _memberInfoLibrary.ObjectToString()),
                _memberInfoLibrary.StringContains(),
                new Expression[] {Expression.Constant(pattern, typeof(string))});
        }

        [NotNull]
        private Expression Compose(
            [NotNull] Expression start,
            [NotNull] Func<Expression, Expression, Expression> combinator,
            IEnumerable<Expression> expressions)
        {
            if (expressions == null)
            {
                return start;
            }
            using (var enumerator = expressions.GetEnumerator())
            {
                if (!enumerator.MoveNext())
                {
                    return start;
                }
                var result = enumerator.Current ?? start;
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current == null)
                    {
                        continue;
                    }
                    result = combinator(result, enumerator.Current) ?? start;
                }
                return result;
            }
        }

        [NotNull]
        private Expression ComposeWithOr(IEnumerable<Expression> expressions)
        {
            return Compose(Expression.Constant(false, typeof(bool)), Expression.OrElse, expressions);
        }

        [NotNull]
        private Expression ComposeWithAnd(IEnumerable<Expression> expressions)
        {
            return Compose(Expression.Constant(true, typeof(bool)), Expression.AndAlso, expressions);
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="property">Свойство</param>
        /// <param name="pattern">Искомая строка</param>
        /// <returns>e => e.Property.ToString().Contains(pattern)</returns>
        public Expression<Func<TEntity, bool>> BuildWhereContains<TEntity>(PropertyInfo property, string pattern)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            var param = Expression.Parameter(typeof(TEntity), "p");
            var result = Expression.Lambda<Func<TEntity, bool>>(ContainsExpression(param, property, pattern), param);
            return result;
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <param name="pattern">Искомая строка</param>
        /// <returns>e => e.Property.ToString().Contains(pattern)</returns>
        public Expression<Func<TEntity, bool>> BuildWhereContains<TEntity>(string propertyName, string pattern)
        {
            return BuildWhereContains<TEntity>(GetProperty(typeof(TEntity), propertyName), pattern);
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="properties">Свойства</param>
        /// <param name="pattern">Искомая строка</param>
        /// <returns>e => e.Property[0].ToString().Contains(pattern) || e.Property[1].ToString().Contains(pattern) || ...</returns>
        public Expression<Func<TEntity, bool>> BuildWhereAnyContains<TEntity>(IEnumerable<PropertyInfo> properties, string pattern)
        {
            if (properties == null)
            {
                return e => true;
            }
            var param = Expression.Parameter(typeof(TEntity), "p");
            var result = Expression.Lambda<Func<TEntity, bool>>(
                ComposeWithOr(properties.Where(p => p != null).Select(p => ContainsExpression(param, p, pattern))), param);
            return result;
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="properties">Имена свойств</param>
        /// <param name="pattern">Искомая строка</param>
        /// <returns>e => e.Property[0].ToString().Contains(pattern) || e.Property[1].ToString().Contains(pattern) || ...</returns>
        public Expression<Func<TEntity, bool>> BuildWhereAnyContains<TEntity>(IEnumerable<string> properties, string pattern)
        {
            if (properties == null)
            {
                return e => true;
            }
            return BuildWhereAnyContains<TEntity>(properties.Select(p => GetProperty(typeof(TEntity), p)), pattern);
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <typeparam name="TProperty">Тип свойства</typeparam>
        /// <param name="property">Свойство</param>
        /// <param name="value">Значение</param>
        /// <returns>e => e.Property == value</returns>
        public Expression<Func<TEntity, bool>> BuildWhereEquals<TEntity, TProperty>(PropertyInfo property, TProperty value)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            if (!property.PropertyType.GetTypeInfo().IsInstanceOfType(value))
            {
                throw new ArgumentException(string.Format("Type mismatch: property {0} has type {1}; value {2} has type {3}",
                    property, property.PropertyType, value, typeof(TProperty)));
            }
            var param = Expression.Parameter(typeof(TEntity), "p");
            var result = Expression.Lambda<Func<TEntity, bool>>(
                Expression.Equal(Expression.Property(param, property), Expression.Constant(value, typeof (TProperty))), 
                param);
            return result;
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <typeparam name="TProperty">Тип свойства</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <param name="value">Значение</param>
        /// <returns>e => e.Property == value</returns>
        public Expression<Func<TEntity, bool>> BuildWhereEquals<TEntity, TProperty>(string propertyName, TProperty value)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException("propertyName");
            }
            return BuildWhereEquals<TEntity, TProperty>(GetProperty(typeof(TEntity), propertyName), value);
        }

        [NotNull]
        private Expression EqualsExpression([NotNull]Expression param, [NotNull]PropertyInfo property, object value)
        {
            if (!property.PropertyType.GetTypeInfo().IsInstanceOfType(value))
            {
                throw new ArgumentException(string.Format("Invalid value type {0} for property {1} of type {2}",
                    value, property, property.PropertyType));
            }
            return Expression.Equal(Expression.Property(param, property), Expression.Constant(value, property.PropertyType));
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="property">Свойство</param>
        /// <param name="value">Значение</param>
        /// <returns>e => e.Property == value</returns>
        public Expression<Func<TEntity, bool>> BuildWhereEquals<TEntity>(PropertyInfo property, object value)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            var param = Expression.Parameter(typeof(TEntity), "p");
            var result = Expression.Lambda<Func<TEntity, bool>>(EqualsExpression(param, property, value), param);
            return result;
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <param name="value">Значение</param>
        /// <returns>e => e.Property == value</returns>
        public Expression<Func<TEntity, bool>> BuildWhereEquals<TEntity>(string propertyName, object value)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException("propertyName");
            }
            return BuildWhereEquals<TEntity>(GetProperty(typeof(TEntity), propertyName), value);
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="values">Свойства и их значения</param>
        /// <returns>e => e.Property1 == value1 and e.Property2 == value2 and ...</returns>
        public Expression<Func<TEntity, bool>> BuildWhereEquals<TEntity>(IEnumerable<KeyValuePair<PropertyInfo, object>> values)
        {
            if (values == null)
            {
                return e => true;
            }
            var param = Expression.Parameter(typeof(TEntity), "p");
            var result = Expression.Lambda<Func<TEntity, bool>>(
                ComposeWithAnd(values.Select(v => v.Key != null ? EqualsExpression(param, v.Key, v.Value) : null)), param);
            return result;
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="values">Имена и значения свойств</param>
        /// <returns>e => e.Property1 == value1 and e.Property2 == value2 and ...</returns>
        public Expression<Func<TEntity, bool>> BuildWhereEquals<TEntity>(IEnumerable<KeyValuePair<string, object>> values)
        {
            if (values == null)
            {
                return e => true;
            }
            return BuildWhereEquals<TEntity>(values.Select(v => 
                new KeyValuePair<PropertyInfo, object>(GetProperty(typeof(TEntity), v.Key), v.Value)));
        }
        
        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <param name="property">Свойство</param>
        /// <param name="values">Значения</param>
        /// <returns>e => e.Property == value1 || e.Property == value2 || ...</returns>
        public Expression<Func<TEntity, bool>> BuildWhereIn<TEntity>(PropertyInfo property, IEnumerable<object> values)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            var param = Expression.Parameter(typeof(TEntity), "p");
            Expression result = null;
            foreach (var value in values ?? Enumerable.Empty<object>())
            {
                if (!property.PropertyType.GetTypeInfo().IsInstanceOfType(value) && (value != null || !property.PropertyType.IsNullableType()))
                {
                    throw new ArgumentException(string.Format("Type mismatch: expected {0}, got {1}: '{2}'",
                        property.PropertyType, value != null ? value.GetType() : null, value));
                }
                var equalityCheck = Expression.Equal(
                    Expression.Property(param, property),
                    Expression.Constant(value, property.PropertyType));
                result = result != null 
                    ? Expression.OrElse(result, equalityCheck)
                    : equalityCheck;
            }
            return Expression.Lambda<Func<TEntity, bool>>(result ?? Expression.Constant(false, typeof(bool)), param);
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <typeparam name="TValue">Тип значения свойства</typeparam>
        /// <param name="property">Свойство</param>
        /// <param name="values">Значения</param>
        /// <returns>e => e.Property == value1 || e.Property == value2 || ...</returns>
        public Expression<Func<TEntity, bool>> BuildWhereIn<TEntity, TValue>(PropertyInfo property, IEnumerable<TValue> values)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            var param = Expression.Parameter(typeof(TEntity), "p");
            if (typeof(TValue) != property.PropertyType)
            {
                throw new ArgumentException(string.Format("Type mismatch: expected {0}, got {1}", property.PropertyType, typeof(TValue)));
            }
            var result = (values ?? Enumerable.Empty<TValue>()).Aggregate(
                (Expression)null, 
                (current, value) => current != null
                    ? Expression.OrElse(
                        current, 
                        Expression.Equal(
                            Expression.Property(param, property), 
                            Expression.Constant(value, property.PropertyType)))
                    : Expression.Equal(
                            Expression.Property(param, property),
                            Expression.Constant(value, property.PropertyType)));
            result = result ?? Expression.Constant(false, typeof(bool));
            return Expression.Lambda<Func<TEntity, bool>>(result, param);
        }

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <typeparam name="TValue">Тип значения свойства</typeparam>
        /// <param name="property">Свойство</param>
        /// <param name="values">Значения</param>
        /// <returns>e => e.Property == value1 || e.Property == value2 || ...</returns>
        public Expression<Func<TEntity, bool>> BuildWhereIn<TEntity, TValue>(ValueHoldingMember property, IEnumerable<TValue> values)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            var param = Expression.Parameter(typeof(TEntity), "p");
            if (typeof(TValue) != property.ValueType)
            {
                throw new ArgumentException(string.Format("Type mismatch: expected {0}, got {1}", property.ValueType, typeof(TValue)));
            }
            var result = (values ?? Enumerable.Empty<TValue>()).Aggregate(
                (Expression)null,
                (current, value) => current != null
                    ? Expression.OrElse(
                        current,
                        Expression.Equal(
                            Expression.MakeMemberAccess(param, property.Member),
                            Expression.Constant(value, property.ValueType)))
                    : Expression.Equal(
                            Expression.MakeMemberAccess(param, property.Member),
                            Expression.Constant(value, property.ValueType)));
            result = result ?? Expression.Constant(false, typeof(bool));
            return Expression.Lambda<Func<TEntity, bool>>(result, param);
        }

        /// <summary>
        /// Построить выражение для for-цикла
        /// </summary>
        /// <param name="init">Инициализация перед циклом</param>
        /// <param name="check">Условие выполнения шага</param>
        /// <param name="step">Перход к следующему шагу</param>
        /// <param name="body">Тело</param>
        /// <param name="variables">Переменные цикла</param>
        public Expression For(
            Expression init,
            Expression check,
            Expression step,
            Expression body,
            params ParameterExpression[] variables)
        {
            if (check != null && check.Type != typeof(bool))
            {
                throw new ArgumentException(string.Format("Boolean expression expected instead of {0}", check));
            }
            var endLoop = Expression.Label();
            return Expression.Block(
                variables,
                init ?? Expression.Empty(),
                Expression.Loop(
                    Expression.IfThenElse(
                        check ?? Expressions.True,
                        Expression.Block(body ?? Expression.Empty(), step ?? Expression.Empty()),
                        Expression.Break(endLoop)),
                    endLoop));
        }

        /// <summary>
        /// Построить выражение для for-цикла с одной целочисленной переменной
        /// </summary>
        /// <param name="variable">Переменная цикла</param>
        /// <param name="first">Первое значение</param>
        /// <param name="afterLast">Значение, следующее за последним</param>
        /// <param name="body">Тело</param>
        public Expression ForRange(ParameterExpression variable, Expression first, Expression afterLast, Expression body)
        {
            return For(
                Expression.Assign(variable, first),
                Expression.LessThan(variable, afterLast),
                Expression.PreIncrementAssign(variable),
                body,
                variable);
        }

        /// <summary>
        /// Построить выражение для foreach-цикла
        /// </summary>
        /// <typeparam name="T">Тип элемента коллекции</typeparam>
        /// <param name="collection">Коллекция, по которой проходит цикл</param>
        /// <param name="current">Параметр цикла</param>
        /// <param name="body">Тело цикла</param>
        /// <returns>Выражение для цикла</returns>
        public Expression Foreach<T>(Expression collection, ParameterExpression current, Expression body)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (current == null)
            {
                throw new ArgumentNullException("current");
            }
            if (body == null)
            {
                throw new ArgumentNullException("body");
            }
            if (!typeof(IEnumerable<T>).GetTypeInfo().IsAssignableFrom(collection.Type))
            {
                throw new ArgumentException(string.Format("{0} is of type {1} and does not implement IEnumerable[{2}]", collection, collection.Type, typeof(T)));
            }
            if (!typeof(T).GetTypeInfo().IsAssignableFrom(current.Type))
            {
                throw new ArgumentException(string.Format("{0} is of type {1} and may not contain an element of IEnumerable[{2}]", current, current.Type, typeof(T)));
            }
            var enumerator = Expression.Variable(typeof(IEnumerator<T>), "enumerator");
            var endLoop = Expression.Label();
            var ienumeratorCurrent = _memberInfoHelper.GetPropertyInfo((IEnumerator<T> e) => e.Current);
            var ienumerableGetEnumerator = _memberInfoHelper.GetMethodInfo((IEnumerable<T> e) => e.GetEnumerator());
            var @foreach = Expression.Block(new[] { enumerator, },
                Expression.Assign(enumerator, Expression.Call(collection, ienumerableGetEnumerator)),
                Expression.Loop(
                    Expression.IfThenElse(
                        Expression.Call(
                            enumerator,
                            _memberInfoLibrary.IenumeratorMoveNext()),
                        Expression.Block(new[] { current, },
                            Expression.Assign(current, Expression.Property(enumerator, ienumeratorCurrent)),
                            body),
                        Expression.Break(endLoop)),
                    endLoop));
            var @finally = Expression.Block(new[] { enumerator },
                Expression.IfThen(
                    Expression.NotEqual(
                        enumerator,
                        Expression.Constant(null, typeof(object))),
                    Expression.Call(enumerator, _memberInfoLibrary.IdisposableDispose())));
            var tryFinally = Expression.TryFinally(@foreach, @finally);
            return tryFinally;
        }

        /// <summary>
        /// Построить выражение для foreach-цикла
        /// </summary>
        /// <param name="collection">Коллекция, по которой проходит цикл</param>
        /// <param name="current">Параметр цикла</param>
        /// <param name="body">Тело цикла</param>
        /// <param name="elementType">Тип элемента коллекции</param>
        /// <returns>Выражение для цикла</returns>
        public Expression Foreach(Expression collection, ParameterExpression current, Expression body, Type elementType)
        {
            var method = _foreachMethod.MakeGenericMethod(elementType);
            var deleg = DelegateCreator.Create<Func<Expression, ParameterExpression, Expression, Expression>>(this, method);
            var result = deleg(collection, current, body).ThrowIfNull();
            return result;
        }

        /// <summary>
        /// Отфильтровать null-элементы из выражения-коллекции
        /// </summary>
        /// <param name="collection">Выражение-коллекция</param>
        /// <returns>coll.Where(x => x != null)</returns>
        public Expression WhereNotNull(Expression collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            var collectionSingleType = collection.Type.GetGenericEnumerableArgument();
            var whereNotNullParam = Expression.Parameter(collectionSingleType);
            var whereNotNull = Expression.Call(
                _memberInfoLibrary.EnumerableWhere(collectionSingleType),
                collection,
                Expression.Lambda(
                    typeof(Func<,>).MakeGenericType(collectionSingleType, typeof(bool)),
                    Expression.NotEqual(
                        whereNotNullParam,
                        Expression.Constant(null, typeof(object))),
                    whereNotNullParam));
            return whereNotNull;
        }

        private Expression AddNullCheck(
            Expression current,
            [NotNull]Expression toCheck,
            [NotNull]Func<Expression, Expression, Expression> checkAppender,
            [NotNull]Func<Expression, Expression, Expression> singleCheck)
        {
            var checkStep = singleCheck(
                toCheck,
                toCheck.Type.GetTypeInfo().IsValueType ? Expression.Constant(null, toCheck.Type) : Expressions.ObjectNull);
            return current != null
                ? checkAppender(
                    current,
                    checkStep)
                : checkStep;
        }

        private Expression MakeNullCheck(
            [NotNull]ParsedPath path,
            bool checkPathResult,
            [NotNull]Func<Expression, Expression, Expression> checkAppender,
            [NotNull]Func<Expression, Expression, Expression> singleCheck)
        {
            Expression result = null;
            Expression prefix = path.Start;
            bool encounteredNotCheckableStep = false;
            foreach (var element in path.Elements)
            {
                if (element == null)
                {
                    throw new InvalidPathException("Path contains null element");
                }
                result = AddNullCheck(result, prefix, checkAppender, singleCheck);
                if (prefix.Type.IsGenericEnumerable() || !prefix.Type.IsNullableType())
                {
                    encounteredNotCheckableStep = true;
                    break;
                }
                prefix = Expression.MakeMemberAccess(prefix, element.Step.Member);
            }
            if (!encounteredNotCheckableStep && checkPathResult && prefix.Type.IsNullableType())
            {
                result = AddNullCheck(result, prefix, checkAppender, singleCheck);
            }
            return result;
        }

        /// <summary>
        /// Построить условие того, что проход по пути не выбросит <see cref="NullReferenceException"/>
        /// </summary>
        /// <param name="path">Путь</param>
        /// <param name="checkPathResult">Проверить также результат прохода по пути</param>
        public Expression BuildPreconditionExpression(ParsedPath path, bool checkPathResult)
        {
            return MakeNullCheck(path, checkPathResult, Expression.AndAlso, Expression.NotEqual);
        }

        /// <summary>
        /// Построить условие того, что на пути встречается null
        /// </summary>
        /// <param name="path">Путь</param>
        /// <param name="checkPathResult">Проверить также результат прохода по пути</param>
        public Expression BuildPathLeadsToNullExpression(ParsedPath path, bool checkPathResult)
        {
            return MakeNullCheck(path, checkPathResult, Expression.OrElse, Expression.Equal);
        }
    }

    internal static class Expressions
    {
        [NotNull]public static ConstantExpression ObjectNull = Expression.Constant(null, typeof(object));

        [NotNull]public static ConstantExpression True = Expression.Constant(true);
        [NotNull]public static ConstantExpression False = Expression.Constant(false);
    }
}
