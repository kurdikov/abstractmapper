﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Helpers.Visitors;

    internal class ExpressionCompositionHelper : IExpressionCompositionHelper
    {
        [NotNull]private readonly IMemberInfoLibrary _library;

        public ExpressionCompositionHelper([NotNull]IMemberInfoLibrary library)
        {
            if (library == null) throw new ArgumentNullException("library");
            _library = library;
        }

        public Expression<Func<TOuterParameter, TReturn>> Compose<TOuterParameter, TInnerParameter, TReturn>(
            Expression<Func<TOuterParameter, TInnerParameter>> innerLambda,
            Expression<Func<TInnerParameter, TReturn>> outerLambda)
        {
            var outerParameter = outerLambda.Parameters[0].ThrowIfNull("outerLambda.Parameters[0]");
            var result = Expression.Lambda<Func<TOuterParameter, TReturn>>(ParameterReplacerVisitor.Replace(outerLambda.Body, outerParameter, innerLambda.Body), innerLambda.Parameters);
            return result;
        }

        public Expression<Func<TOuterParameter, object>> ComposeWithSelect<TOuterParameter, TInnerParameter>(
            Expression<Func<TOuterParameter, IEnumerable<TInnerParameter>>> outerLambda,
            Expression<Func<TInnerParameter, object>> innerLambda)
        {
            var result = Expression.Lambda<Func<TOuterParameter, object>>(
                Expression.Call(_library.EnumerableSelect(typeof(TInnerParameter), typeof(object)), outerLambda.Body, innerLambda),
                outerLambda.Parameters);
            return result;
        }

        public Expression<Func<TParameter, object>> ReplaceInterfaceWithImplementation<TParameter, TParameterInterface>(
            Expression<Func<TParameterInterface, object>> lambda) where TParameter : TParameterInterface
        {
            var parameter = Expression.Parameter(typeof (TParameter), "p");
            var replacer = new ParameterTypeReplacerVisitor<TParameterInterface>(parameter);
            var result = Expression.Lambda<Func<TParameter, object>>(replacer.Visit(lambda.Body).ThrowIfNull("ParameterTypeReplacerVisitor.Visit"), parameter);
            return result;
        }

        public Expression<Func<TParameter, object>> MakeObjectOutput<TParameter, TOutput>(Expression<Func<TParameter, TOutput>> lambda)
        {
            var result = Expression.Lambda<Func<TParameter, object>>(
                lambda.Body.Type.GetTypeInfo().IsValueType ? Expression.Convert(lambda.Body, typeof(object)) : lambda.Body,
                lambda.Parameters);
            return result;
        }
    }
}
