﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.Models.Path;

    internal class PathParser : IPathParser
    {
        [NotNull]private readonly IMemberInfoLibrary _library;
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;
        [NotNull]private readonly IPathHelper _pathHelper;

        public PathParser(
            [NotNull]IMemberInfoLibrary library, 
            [NotNull]IExpressionBuilder expressionBuilder, 
            [NotNull]IPathHelper pathHelper)
        {
            if (library == null) throw new ArgumentNullException("library");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (pathHelper == null) throw new ArgumentNullException("pathHelper");
            _library = library;
            _expressionBuilder = expressionBuilder;
            _pathHelper = pathHelper;
        }

        public ParsedPath Parse(Expression expression)
        {
            var result = PathParserVisitor.Parse(expression, _library, _expressionBuilder);

            if (result.Elements.Any())
            {
                var lastSingleType = _pathHelper.GetPathSingleType(result);
                var expressionType = GetExpressionType(expression);
                if (!lastSingleType.GetTypeInfo().IsAssignableFrom(expressionType)
                    && !typeof(IEnumerable<>).MakeGenericType(lastSingleType).GetTypeInfo().IsAssignableFrom(expressionType))
                {
                    throw new NotSupportedException(string.Format(
                        "Path type {0} and expression type {1} do not match. This may happen due to nested Select calls or incorrect Select calls in place of SelectMany",
                        lastSingleType,
                        expression.Type));
                }
            }
            return result;
        }

        private Type GetExpressionType([NotNull]Expression expression)
        {
            var lambda = expression as LambdaExpression;
            var body = lambda != null ? lambda.Body : expression;
            if (body.NodeType == ExpressionType.Convert || body.NodeType == ExpressionType.ConvertChecked)
            {
                body = ((UnaryExpression)body).Operand;
            }
            return body.Type;
        }
    }
}
