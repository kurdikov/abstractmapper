﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Concurrent;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

    /// <summary>
    /// Словарь с возможностью одновременного доступа, выполняющий передаваемые в обновляющие методы делегаты не более одного раза
    /// </summary>
    /// <typeparam name="TKey">Тип ключа</typeparam>
    /// <typeparam name="TValue">Тип значения</typeparam>
    public class LazyConcurrentDictionary<TKey, TValue> : ConcurrentDictionary<TKey, Lazy<TValue>>
    {
        /// <summary>
        /// Получить имеющееся или добавить значение
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="factory">Делегат создания значения, выполняется не более одного раза</param>
        public TValue LazyGetOrAdd([NotNull] TKey key, [NotNull] Func<TKey, TValue> factory)
        {
            var result = GetOrAdd(key, k => new Lazy<TValue>(() => factory(k))).ThrowIfNull();
            return result.Value;
        }

        /// <summary>
        /// Обновить имеющееся или добавить значение
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="value">Добавляемое значение</param>
        /// <param name="updateFactory">Делегат обновления значения, выполняется не более одного раза</param>
        public TValue LazyAddOrUpdate([NotNull] TKey key, [NotNull]TValue value, [NotNull]Func<TKey, TValue, TValue> updateFactory)
        {
            var result = AddOrUpdate(
                key,
                new Lazy<TValue>(() => value),
                (k, v) => new Lazy<TValue>(() => updateFactory(k, v.ThrowIfNull().Value)))
                .ThrowIfNull();
            return result.Value;
        }

        /// <summary>
        /// Обновить имеющееся или добавить значение
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="addFactory">Делегат создания значения, выполняется не более одного раза</param>
        /// <param name="updateFactory">Делегат обновления значения, выполняется не более одного раза</param>
        public TValue LazyAddOrUpdate(
            [NotNull] TKey key,
            [NotNull] Func<TKey, TValue> addFactory,
            [NotNull] Func<TKey, TValue, TValue> updateFactory)
        {
            var result = AddOrUpdate(
                key,
                k => new Lazy<TValue>(() => addFactory(k)),
                (k, v) => new Lazy<TValue>(() => updateFactory(k, v.ThrowIfNull().Value))).ThrowIfNull();
            return result.Value;
        }
    }
}
