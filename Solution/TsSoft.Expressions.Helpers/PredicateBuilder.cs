﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Построитель предикатов
    /// </summary>
    public static class PredicateBuilder
    {
        /// <summary>
        /// Предикат ИСТИНА
        /// </summary>
        /// <typeparam name="T">Тип параметра</typeparam>
        /// <returns>Всегда истинный предикат</returns>
        [NotNull]
        public static Expression<Func<T, bool>> True<T>()
        {
            return f => true;
        }

        /// <summary>
        /// Предикат ЛОЖЬ
        /// </summary>
        /// <typeparam name="T">Тип параметра</typeparam>
        /// <returns>Всегда ложный предикат</returns>
        [NotNull]
        public static Expression<Func<T, bool>> False<T>()
        {
            return f => false;
        }
        
        /// <summary>
        /// Объединить предикаты с помощью оператора ИЛИ
        /// </summary>
        /// <typeparam name="T">Тип параметра</typeparam>
        /// <param name="expr1">Первый предикат</param>
        /// <param name="expr2">Второй предикат</param>
        [NotNull]
        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1,
                                                      [NotNull]Expression<Func<T, bool>> expr2)
        {
            if (expr1 == null)
            {
                return expr2;
            }
            return expr1.Compose(expr2, Expression.OrElse);
        }

        /// <summary>
        /// Объединить предикаты с помощью оператора И
        /// </summary>
        /// <typeparam name="T">Тип параметра</typeparam>
        /// <param name="expr1">Первый предикат</param>
        /// <param name="expr2">Второй предикат</param>
        [NotNull]
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1,
                                                       [NotNull]Expression<Func<T, bool>> expr2)
        {
            if (expr1 == null)
            {
                return expr2;
            }
            return expr1.Compose(expr2, Expression.AndAlso);
        }

        /// <summary>
        /// Получить билдер из экспрешена
        /// </summary>
        /// <typeparam name="T">Тип параметра</typeparam>
        /// <param name="expr">Исходный предикат</param>
        /// <returns>Билдер предикатов</returns>
        public static IPredicateBuilder<T> ToBuilder<T>([NotNull] this Expression<Func<T, bool>> expr)
        {
            if (expr == null) throw new ArgumentNullException("expr");
            return new InternalBuilder<T>(expr);
        }

        /// <summary>
        /// Объединить предикаты с помощью оператора
        /// </summary>
        /// <typeparam name="T">Тип параметра</typeparam>
        /// <param name="first">Первый предикат</param>
        /// <param name="second">Второй предикат</param>
        /// <param name="merge">Оператор</param>
        [NotNull]
        public static Expression<T> Compose<T>([NotNull]this Expression<T> first, [NotNull]Expression<T> second, [NotNull]Func<Expression, Expression, Expression> merge)
        {
            var map = first.Parameters.Select((f, i) => new { f, s = second.Parameters[i] }).ToDictionary(p => p.s, p => p.f);
            var secondBody = ParameterRebinder.ReplaceParameters(map, second.Body);
            var merged = merge(first.Body, secondBody);
            if (merged == null)
            {
                throw new InvalidOperationException("merge returned null");
            }
            return Expression.Lambda<T>(merged, first.Parameters);
        }

        internal class InternalBuilder<T> : IPredicateBuilder<T>
        {
            [NotNull] private Expression<Func<T, bool>> _predicate;

            public InternalBuilder([NotNull] Expression<Func<T, bool>> predicate)
            {
                if (predicate == null) throw new ArgumentNullException("predicate");
                _predicate = predicate;
            }

            public IPredicateBuilder<T> Or(Expression<Func<T, bool>> expr)
            {
                _predicate = _predicate.Or(expr);
                return this;
            }

            public IPredicateBuilder<T> And(Expression<Func<T, bool>> expr)
            {
                _predicate = _predicate.And(expr);
                return this;
            }

            public Expression<Func<T, bool>> BuildPredicate()
            {
                return _predicate;
            }
        }

        internal class ParameterRebinder : ExpressionVisitor
        {
            [NotNull]private readonly Dictionary<ParameterExpression, ParameterExpression> _map;

            public ParameterRebinder(Dictionary<ParameterExpression, ParameterExpression> map)
            {
                _map = map ?? new Dictionary<ParameterExpression, ParameterExpression>();
            }

            public static Expression ReplaceParameters(Dictionary<ParameterExpression, ParameterExpression> map, Expression exp)
            {
                return new ParameterRebinder(map).Visit(exp);
            }

            protected override Expression VisitParameter(ParameterExpression p)
            {
                ParameterExpression replacement;
                if (_map.TryGetValue(p, out replacement) && replacement != null)
                {
                    p = replacement;
                }
                return base.VisitParameter(p);
            }
        }
    }
}
