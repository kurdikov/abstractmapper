﻿namespace TsSoft.Expressions.Helpers.Differences
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models.Differences;

    class DiffComputer : IDiffComputer
    {
        [NotNull]
        private readonly ConcurrentDictionary<Type, MulticastDelegate> _diffMakerCache = 
            new ConcurrentDictionary<Type, MulticastDelegate>();

        [NotNull] private readonly MethodInfo _addDiff;
        [NotNull] private readonly MethodInfo _getObjectDiff;
        [NotNull] private readonly MethodInfo _getCollectionDiff;
        [NotNull] private readonly MethodInfo _getPrimitiveDiff;
        [NotNull] private readonly ConstantExpression _this;

        public DiffComputer([NotNull]IMemberInfoHelper memberInfo)
        {
            if (memberInfo == null) throw new ArgumentNullException("memberInfo");
            _addDiff = memberInfo.GetMethodInfo((ObjectDiff od) => od.AddDiff(null, null));
            _getObjectDiff = memberInfo.GetGenericDefinitionMethodInfo(() => GetDiff<object>(null, null, null, null));
            _getCollectionDiff = memberInfo.GetGenericDefinitionMethodInfo(() => GetCollectionDiff<object>(null, null, null, null));
            _getPrimitiveDiff = memberInfo.GetGenericDefinitionMethodInfo(() => GetPrimitiveDiff<object>(null, null));
            _this = Expression.Constant(this, GetType());
        }

        private bool IsPrimitivePropertyType([NotNull] Type type)
        {
            return (type.GetTypeInfo().IsValueType || type == typeof(string));
        }

        private bool IsPrimitiveProperty([NotNull]PropertyInfo property)
        {
            return (property.PropertyType.GetTypeInfo().IsValueType || property.PropertyType == typeof(string));
        }

        private Expression AddDiff([NotNull]Expression diffContainer, [NotNull] PropertyInfo property, [NotNull]Expression diff)
        {
            return Expression.IfThen(
                Expression.NotEqual(diff, Expression.Constant(null)),
                Expression.Call(diffContainer, _addDiff, Expression.Constant(property, typeof(PropertyInfo)), diff));
        }

        private delegate void DiffMaker<T>(T first, T second, IEqualityComparerProvider equality,
            IDictionary<KeyValuePair<object, object>, IDiff> context,
            ObjectDiff<T> result);

        private DiffMaker<T> CreateDiffMaker<T>()
        {
            var expressions = new List<Expression>();
            var first = Expression.Parameter(typeof(T), "first");
            var second = Expression.Parameter(typeof(T), "second");
            var provider = Expression.Parameter(typeof(IEqualityComparerProvider), "equality");
            var context = Expression.Parameter(typeof(IDictionary<KeyValuePair<object, object>, IDiff>), "context");
            var diff = Expression.Parameter(typeof(ObjectDiff<T>), "diff");
            var parameters = new Expression[] {null, null, provider, context};
            foreach (var property in typeof(T).GetTypeInfo().GetProperties())
            {
                if (property == null || !property.CanRead)
                {
                    continue;
                }
                Expression propertyDiff;
                var firstProp = parameters[0] = Expression.Property(first, property);
                var secondProp = parameters[1] = Expression.Property(second, property);
                if (IsPrimitiveProperty(property))
                {
                    propertyDiff = Expression.Condition(
                        Expression.NotEqual(firstProp, secondProp), 
                        Expression.Call(
                            _this, 
                            _getPrimitiveDiff.MakeGenericMethod(property.PropertyType), 
                            firstProp, 
                            secondProp), 
                        Expression.Constant(null, typeof(PrimitiveDiff<>).MakeGenericType(property.PropertyType)));
                }
                else if (property.PropertyType.IsGenericEnumerable())
                {
                    var elementType = property.PropertyType.GetGenericEnumerableArgument();
                    propertyDiff = Expression.Call(_this,
                        _getCollectionDiff.MakeGenericMethod(elementType),
                        parameters);
                }
                else
                {
                    propertyDiff = Expression.Call(
                        _this,
                        _getObjectDiff.MakeGenericMethod(property.PropertyType),
                        parameters);
                }
                expressions.Add(AddDiff(diff, property, propertyDiff));
            }
            var block = Expression.Block(expressions);
            var lambda = Expression.Lambda<DiffMaker<T>>(block, first, second, provider, context, diff);
            return lambda.Compile();
        }

        private PrimitiveDiff<T> GetPrimitiveDiff<T>(T first, T second)
        {
            return new PrimitiveDiff<T> {First = first, Second = second};
        }

        private IObjectDiff<T> GetDiff<T>(T first, T second, 
            [NotNull]IEqualityComparerProvider equality, 
            [NotNull]IDictionary<KeyValuePair<object, object>, IDiff> context)
        {
            if (first == null && second == null)
            {
                return null;
            }
            if (first == null)
            {
                return new ObjectDiff<T> { FirstIsNull = true };
            }
            if (second == null)
            {
                return new ObjectDiff<T> { SecondIsNull = true };
            }
            IDiff computedDiff;
            if (context.TryGetValue(new KeyValuePair<object, object>(first, second), out computedDiff))
            {
                return computedDiff as IObjectDiff<T>;
            }
            var result = new ObjectDiff<T>();
            context.Add(new KeyValuePair<object, object>(first, second), result);
            var diffComputerFunc = _diffMakerCache.GetOrAdd(typeof(T), type => CreateDiffMaker<T>())
                as DiffMaker<T>;
            if (diffComputerFunc == null)
            {
                throw new InvalidOperationException("cache returned null");
            }
            diffComputerFunc(first, second, equality, context, result);
            if (!result.HasDiffs)
            {
                result = null;
                context[new KeyValuePair<object, object>(first, second)] = null;
            }
            return result;
        }

        public IObjectDiff<T> GetDiff<T>(T first, T second, IEqualityComparerProvider equality = null)
        {
            return GetDiff(first, second, equality ?? DefaultEqualityComparerProvider.Instance, new Dictionary<KeyValuePair<object, object>, IDiff>());
        }

        private ICollectionDiff<T> GetCollectionDiff<T>(
            IEnumerable<T> first, 
            IEnumerable<T> second,
            [NotNull]IEqualityComparerProvider equality,
            [NotNull]IDictionary<KeyValuePair<object, object>, IDiff> context)
        {
            if (first == null && second == null)
            {
                return null;
            }
            if (first == null)
            {
                return new CollectionDiff<T> { SecondExcess = second.ToList(), FirstIsNull = true };
            }
            if (second == null)
            {
                return new CollectionDiff<T> { SecondExcess = first.ToList(), SecondIsNull = true };
            }
            IDiff computedDiff;
            if (context.TryGetValue(new KeyValuePair<object, object>(first, second), out computedDiff))
            {
                return computedDiff as ICollectionDiff<T>;
            }
            var result = new CollectionDiff<T>();
            context.Add(new KeyValuePair<object, object>(first, second), result);
            var set = new Dictionary<T, T>(equality.GetEqualityComparer<T>() ?? EqualityComparer<T>.Default);
            foreach (var firstElem in first)
            {
                if (firstElem == null)
                {
                    continue;
                }
                set[firstElem] = firstElem;
            }
            var secondExcess = new List<T>();
            var diffs = new List<IObjectWithDiff<T>>();
            var isPrimitiveElement = IsPrimitivePropertyType(typeof(T));
            foreach (var secondElem in second)
            {
                if (secondElem == null)
                {
                    continue;
                }
                T firstElem;
                var firstContains = set.TryGetValue(secondElem, out firstElem);
                if (!firstContains || firstElem == null)
                {
                    secondExcess.Add(secondElem);
                }
                else
                {
                    set.Remove(firstElem);
                    if (!isPrimitiveElement)
                    {
                        var diff = GetDiff(firstElem, secondElem, equality, context);
                        if (diff != null)
                        {
                            diffs.Add(new ObjectWithDiff<T> { Object = firstElem, Diff = diff });
                        }
                    }
                }
            }
            result.FirstExcess = set.Keys.ToList();
            result.SecondExcess = secondExcess;
            result.Diffs = diffs;
            if (result.FirstExcess.Count == 0 && result.SecondExcess.Count == 0 && result.Diffs.Count == 0)
            {
                result = null;
                context[new KeyValuePair<object, object>(first, second)] = null;
            }
            return result;
        }

        public ICollectionDiff<T> GetCollectionDiff<T>(IEnumerable<T> first, IEnumerable<T> second, IEqualityComparerProvider equality = null)
        {
            return GetCollectionDiff(first, second, 
                equality ?? DefaultEqualityComparerProvider.Instance,
                new Dictionary<KeyValuePair<object, object>, IDiff>());
        }
    }
}
