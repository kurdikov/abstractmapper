﻿namespace TsSoft.Expressions.Helpers.Differences
{
    using System.Collections.Generic;
    using TsSoft.Expressions.Models.Differences;

    /// <summary>
    /// Вычисляет различия между объектами и коллекциями
    /// </summary>
    public interface IDiffComputer
    {
        /// <summary>
        /// Получить различия между объектами
        /// </summary>
        /// <typeparam name="T">Тип объектов</typeparam>
        /// <param name="first">Первый объект</param>
        /// <param name="second">Второй объект</param>
        /// <param name="equality">Как находить совпадающие объекты в коллекциях</param>
        IObjectDiff<T> GetDiff<T>(T first, T second, IEqualityComparerProvider equality = null);

        /// <summary>
        /// Получить различия между коллекциями
        /// </summary>
        /// <typeparam name="T">Тип объектов в коллекции</typeparam>
        /// <param name="first">Первая коллекция</param>
        /// <param name="second">Вторая коллекция</param>
        /// <param name="equality">Как находить совпадающие объекты в коллекциях</param>
        ICollectionDiff<T> GetCollectionDiff<T>(IEnumerable<T> first, IEnumerable<T> second, IEqualityComparerProvider equality = null);
    }
}
