﻿// ReSharper disable CheckNamespace
namespace TsSoft.Expressions.Models.Differences
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Расширения различий в объектах
    /// </summary>
    public static class ObjectDiffExtensions
    {
        [NotNull]
        private static string PropertyName<T, TProp>(Expression<Func<T, TProp>> path)
        {
            if (path == null)
            {
                throw new ArgumentNullException("path");
            }
            var memberPath = path.Body as MemberExpression;
            if (memberPath == null)
            {
                throw new ArgumentException(string.Format("{0} is not a MemberExpression", path));
            }
            return memberPath.Member.Name;
        }

        /// <summary>
        /// Получить различие в значениях простого свойства объекта
        /// </summary>
        /// <typeparam name="TProp">Тип значения свойства</typeparam>
        /// <typeparam name="T">Тип сравнённого объекта</typeparam>
        /// <param name="objectDiff">Результат сравнения</param>
        /// <param name="path">Свойство</param>
        /// <returns>Различие в значениях свойства или null, если различий нет</returns>
        public static IPrimitiveDiff<TProp> PrimitiveDiff<T, TProp>(
            [NotNull]this IObjectDiff<T> objectDiff,
            [NotNull] Expression<Func<T, TProp>> path)
        {
            return objectDiff.PrimitiveDiff<TProp>(PropertyName(path));
        }

        /// <summary>
        /// Получить различие в значениях простого свойства объекта
        /// </summary>
        /// <typeparam name="TProp">Тип значения свойства</typeparam>
        /// <typeparam name="T">Тип сравнённого объекта</typeparam>
        /// <param name="objectDiff">Результат сравнения</param>
        /// <param name="path">Свойство</param>
        /// <returns>Различие в значениях свойства или null, если различий нет</returns>
        public static IObjectDiff<TProp> SubobjectDiff<T, TProp>(
            [NotNull]this IObjectDiff<T> objectDiff,
            [NotNull] Expression<Func<T, TProp>> path)
        {
            return objectDiff.SubobjectDiff<TProp>(PropertyName(path));
        }

        /// <summary>
        /// Получить различие в значениях простого свойства объекта
        /// </summary>
        /// <typeparam name="TPropElem">Тип значения элемента свойства</typeparam>
        /// <typeparam name="T">Тип сравнённого объекта</typeparam>
        /// <param name="path">Свойство</param>
        /// <param name="objectDiff">Результат сравнения</param>
        /// <returns>Различие в значениях свойства или null, если различий нет</returns>
        public static ICollectionDiff<TPropElem> CollectionDiff<T, TPropElem>(
            [NotNull]this IObjectDiff<T> objectDiff,
            [NotNull] Expression<Func<T, IEnumerable<TPropElem>>> path)
        {
            return objectDiff.CollectionDiff<TPropElem>(PropertyName(path));
        }
    }
}
