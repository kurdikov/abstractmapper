﻿namespace TsSoft.Expressions.Helpers.Differences
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    class DefaultEqualityComparerProvider : IEqualityComparerProvider
    {
        [NotNull]
        public static DefaultEqualityComparerProvider Instance = new DefaultEqualityComparerProvider();

        public IEqualityComparer<T> GetEqualityComparer<T>()
        {
            return EqualityComparer<T>.Default;
        }
    }
}
