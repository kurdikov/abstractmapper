﻿namespace TsSoft.Expressions.Helpers.Differences
{
    using System.Collections.Generic;

    /// <summary>
    /// Предоставляет сравнители на равенство для произвольного типа
    /// </summary>
    public interface IEqualityComparerProvider
    {
        /// <summary>
        /// Получить сравнитель на равенство для типа
        /// </summary>
        /// <typeparam name="T">Тип объектов</typeparam>
        /// <returns>Сравнитель на равенство</returns>
        IEqualityComparer<T> GetEqualityComparer<T>();
    }
}
