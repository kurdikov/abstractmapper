﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Хелпер для сборки выражений из частей
    /// </summary>
    public interface IExpressionCompositionHelper
    {
        /// <summary>
        /// Собрать выражение-композицию: f, g -> f(g)
        /// </summary>
        /// <typeparam name="TOuterParameter">Тип параметра внутренней лямбды</typeparam>
        /// <typeparam name="TInnerParameter">Тип выхода внутренней лямбды и тип параметра внешней лямбды</typeparam>
        /// <typeparam name="TReturn">Тип выхода внешней лямбды</typeparam>
        /// <param name="innerLambda">Внутренняя лямбда</param>
        /// <param name="outerLambda">Внешняя лямбда</param>
        /// <returns>Лямбда-композиция</returns>
        [NotNull]
        Expression<Func<TOuterParameter, TReturn>> Compose<TOuterParameter, TInnerParameter, TReturn>(
            [NotNull]Expression<Func<TOuterParameter, TInnerParameter>> innerLambda,
            [NotNull]Expression<Func<TInnerParameter, TReturn>> outerLambda);

        /// <summary>
        /// Собрать выражение-композицию через Select: f, g -> g().Select(f)
        /// </summary>
        /// <typeparam name="TOuterParameter">Тип параметра внутренней лямбды</typeparam>
        /// <typeparam name="TInnerParameter">Тип элемента коллекции-выхода внутренней лямбды и тип параметра внешней лямбды</typeparam>
        /// <param name="outerLambda">Внешяя лямбда</param>
        /// <param name="innerLambda">Внутренняя лямбда</param>
        /// <returns>Лямбда-композиция</returns>
        [NotNull]
        Expression<Func<TOuterParameter, object>> ComposeWithSelect<TOuterParameter, TInnerParameter>(
            [NotNull]Expression<Func<TOuterParameter, IEnumerable<TInnerParameter>>> outerLambda,
            [NotNull]Expression<Func<TInnerParameter, object>> innerLambda);

        /// <summary>
        /// Заменить тип параметра
        /// </summary>
        /// <typeparam name="TParameter">Желаемый тип параметра</typeparam>
        /// <typeparam name="TParameterInterface">Исходный тип параметра</typeparam>
        /// <param name="lambda">Лямбда</param>
        /// <returns>Лямбда с изменённым типом параметра</returns>
        [NotNull]
        Expression<Func<TParameter, object>> ReplaceInterfaceWithImplementation<TParameter, TParameterInterface>(
            [NotNull]Expression<Func<TParameterInterface, object>> lambda)
            where TParameter : TParameterInterface;

        /// <summary>
        /// Заменить тип выхода на object
        /// </summary>
        /// <typeparam name="TParameter">Тип параметра</typeparam>
        /// <typeparam name="TOutput">Исходный тип выхода</typeparam>
        /// <param name="lambda">Лямбда</param>
        /// <returns>Лямбда с типом выхода object</returns>
        [NotNull]Expression<Func<TParameter, object>> MakeObjectOutput<TParameter, TOutput>(
            [NotNull]Expression<Func<TParameter, TOutput>> lambda);
    }
}
