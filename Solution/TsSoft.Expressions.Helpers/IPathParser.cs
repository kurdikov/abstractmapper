﻿namespace TsSoft.Expressions.Helpers
{
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Выделяет путь из выражения
    /// </summary>
    public interface IPathParser
    {
        /// <summary>
        /// Выделить использованный выражением путь
        /// </summary>
        /// <param name="expression">Выражение</param>
        /// <returns>Использованный путь</returns>
        [NotNull]
        ParsedPath Parse([NotNull]Expression expression);
    }
}
