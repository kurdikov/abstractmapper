namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// ������ ����������
    /// </summary>
    /// <typeparam name="T">��� ���������</typeparam>
    public interface IPredicateBuilder<T>
    {
        /// <summary>
        /// �������� ��������� ����� ���
        /// </summary>
        /// <param name="expr">���������</param>
        /// <returns>������</returns>
        IPredicateBuilder<T> Or([NotNull] Expression<Func<T, bool>> expr);

        /// <summary>
        /// �������� ��������� ����� �
        /// </summary>
        /// <param name="expr">���������</param>
        /// <returns>������</returns>
        IPredicateBuilder<T> And([NotNull] Expression<Func<T, bool>> expr);

        /// <summary>
        /// ������� ��������
        /// </summary>
        /// <returns>��������� ��������</returns>
        Expression<Func<T, bool>> BuildPredicate();
    }
}