﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Описание существующего типа делегата
    /// </summary>
    public class MaterializedDelegateTypeDescription : DelegateTypeDescription,
        IEquatable<MaterializedDelegateTypeDescription>
    {
        /// <summary>
        /// Тип делегата
        /// </summary>
        [NotNull]
        public Type DelegateType { get; private set; }

        /// <summary>
        /// Описание существующего типа делегата
        /// </summary>
        public MaterializedDelegateTypeDescription([NotNull] IEnumerable<Type> parameterTypes, [NotNull] Type returnType, [NotNull] Type delegateType)
            : base(parameterTypes, returnType)
        {
            if (delegateType == null) throw new ArgumentNullException("delegateType");
            DelegateType = delegateType;
        }

        /// <summary>
        /// Равны ли описания
        /// </summary>
        public bool Equals(MaterializedDelegateTypeDescription other)
        {
            return other != null && base.Equals(other) && DelegateType == other.DelegateType;
        }

        /// <summary>
        /// Хэш
        /// </summary>
        public override int GetHashCode()
        {
            return base.GetHashCode() * 397 ^ DelegateType.GetHashCode();
        }
    }
}
