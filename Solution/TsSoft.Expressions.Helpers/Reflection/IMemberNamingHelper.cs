﻿using JetBrains.Annotations;

namespace TsSoft.Expressions.Helpers.Reflection
{
    /// <summary>
    /// Определяет именование специальных членов класса
    /// </summary>
    public interface IMemberNamingHelper
    {
        /// <summary>
        /// Получить каноническое имя теневого свойства
        /// </summary>
        /// <param name="virtualPropertyWithoutSetter">Виртуальное свойство без сеттера</param>
        /// <returns>Имя теневого свойства с сеттером</returns>
        [NotNull]
        string WriteableShadow([NotNull]string virtualPropertyWithoutSetter);

        /// <summary>
        /// Получить имя затенённого свойства
        /// </summary>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Имя затенённого свойства</returns>
        string GetShadowedName([NotNull]string propertyName);
    }
}
