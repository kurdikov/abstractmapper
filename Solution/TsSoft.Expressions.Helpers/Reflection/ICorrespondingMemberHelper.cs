﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Хелпер для извлечения соответствующих членов типа
    /// </summary>
    public interface ICorrespondingMemberHelper
    {
        /// <summary>
        /// Получить одноимённое свойство или поле
        /// </summary>
        /// <param name="type">Тип, из которого выделяется свойство или поле</param>
        /// <param name="propertyOrField">Эталонное свойство или поле</param>
        [CanBeNull]
        ValueHoldingMember TryGetCorrespondingMember([NotNull]Type type, [NotNull]MemberInfo propertyOrField);

        /// <summary>
        /// Получить свойство, соответствующее свойству, явно реализующему интерфейс
        /// </summary>
        /// <param name="explicitProperty">Свойство, явно реализующее интерфейс</param>
        [CanBeNull]
        ValueHoldingMember TryGetCorrespondingMemberForExplicitImplementation([NotNull]ValueHoldingMember explicitProperty);

        /// <summary>
        /// Получить одноимённое свойство или поле
        /// </summary>
        /// <param name="type">Тип, из которого выделяется свойство или поле</param>
        /// <param name="propertyOrField">Эталонное свойство или поле</param>
        [NotNull]
        ValueHoldingMember GetCorrespondingMember([NotNull]Type type, [NotNull]MemberInfo propertyOrField);

        /// <summary>
        /// Получить одноимённое свойство
        /// </summary>
        /// <param name="type">Тип, из которого выделяется свойство или поле</param>
        /// <param name="member">Эталонное свойство или поле</param>
        PropertyInfo TryGetCorrespondingProperty([NotNull]Type type, [NotNull]MemberInfo member);

        /// <summary>
        /// Получить реализацию метода
        /// </summary>
        /// <param name="impl">Реализующий интерфейс тип</param>
        /// <param name="interfaceMethod">Метод интерфейса</param>
        /// <returns>Метод-реализация</returns>
        [NotNull]
        MethodInfo GetImplementation([NotNull] Type impl, [NotNull] MethodInfo interfaceMethod);

        /// <summary>
        /// Получить реализацию свойства
        /// </summary>
        /// <param name="impl">Реализующий интерфейс тип</param>
        /// <param name="interfaceProperty">Свойство интерфейса</param>
        /// <returns>Свойство-реализация</returns>
        [NotNull]
        PropertyInfo GetImplementation([NotNull] Type impl, [NotNull] PropertyInfo interfaceProperty);

        /// <summary>
        /// Получить реализацию свойства, если оно определено на интерфейсе, и вернуть исходное свойство в противном случае
        /// </summary>
        /// <param name="targetType">Тип, свойство которого требуется получить</param>
        /// <param name="property">Свойство, возможно, определённое на интерфейсе типа</param>
        [NotNull]
        PropertyInfo GetImplementationIfInterfaceProperty([NotNull] Type targetType, [NotNull] PropertyInfo property);

        /// <summary>
        /// Получить реализацию свойства, если оно определено на интерфейсе, и вернуть исходное свойство в противном случае
        /// </summary>
        /// <param name="targetType">Тип, свойство которого требуется получить</param>
        /// <param name="member">Свойство, возможно, определённое на интерфейсе типа</param>
        [NotNull]
        ValueHoldingMember GetImplementationIfInterfaceProperty([NotNull] Type targetType, [NotNull] ValueHoldingMember member);
    }
}
