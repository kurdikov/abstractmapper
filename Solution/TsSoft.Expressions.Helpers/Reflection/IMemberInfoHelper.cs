﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Выделяет члены типов
    /// </summary>
    public interface IMemberInfoHelper
    {
        /// <summary>
        /// Получить метод
        /// </summary>
        /// <param name="expression">Выражение - вызов метода</param>
        /// <returns>Метод</returns>
        [NotNull]
        MethodInfo GetMethodInfo(LambdaExpression expression);

        /// <summary>
        /// Получить метод
        /// </summary>
        /// <param name="expression">Выражение - вызов метода</param>
        /// <returns>Метод</returns>
        [NotNull]
        MethodInfo GetMethodInfo(Expression<Action> expression);

        /// <summary>
        /// Получить метод
        /// </summary>
        /// <param name="expression">Выражение - вызов метода</param>
        /// <returns>Метод</returns>
        [NotNull]
        MethodInfo GetMethodInfo<T>(Expression<Action<T>> expression);

        /// <summary>
        /// Получить метод
        /// </summary>
        /// <param name="expression">Выражение - вызов метода</param>
        /// <returns>Метод</returns>
        [NotNull]
        MethodInfo GetMethodInfo<T, TResult>(Expression<Func<T, TResult>> expression);

        /// <summary>
        /// Получить заготовку генерик-метода
        /// </summary>
        /// <param name="expression">Выражение - вызов генерик-метода</param>
        /// <returns>Заготовка метода</returns>
        [NotNull]
        MethodInfo GetGenericDefinitionMethodInfo(Expression<Action> expression);

        /// <summary>
        /// Получить заготовку генерик-метода
        /// </summary>
        /// <param name="expression">Выражение - вызов генерик-метода</param>
        /// <returns>Заготовка метода</returns>
        [NotNull]
        MethodInfo GetGenericDefinitionMethodInfo<T>(Expression<Action<T>> expression);

        /// <summary>
        /// Получить заготовку генерик-метода
        /// </summary>
        /// <param name="expression">Выражение - вызов генерик-метода</param>
        /// <returns>Заготовка метода</returns>
        [NotNull]
        MethodInfo GetGenericDefinitionMethodInfo<T, TResult>(Expression<Func<T, TResult>> expression);

        /// <summary>
        /// Получить свойство
        /// </summary>
        /// <param name="expression">Выражение - обращение к свойству</param>
        /// <returns>Свойство</returns>
        [NotNull]
        PropertyInfo GetPropertyInfo(LambdaExpression expression);

        /// <summary>
        /// Получить свойство
        /// </summary>
        /// <param name="expression">Выражение - обращение к свойству</param>
        /// <returns>Свойство</returns>
        [NotNull]
        PropertyInfo GetPropertyInfo<TResult>(Expression<Func<TResult>> expression);

        /// <summary>
        /// Получить свойство
        /// </summary>
        /// <param name="expression">Выражение - обращение к свойству</param>
        /// <returns>Свойство</returns>
        [NotNull]
        PropertyInfo GetPropertyInfo<T, TResult>(Expression<Func<T, TResult>> expression);

        /// <summary>
        /// Получить поле
        /// </summary>
        /// <param name="expression">Выражение - обращение к полю</param>
        /// <returns>Поле</returns>
        [NotNull]
        FieldInfo GetFieldInfo(LambdaExpression expression);

        /// <summary>
        /// Получить поле
        /// </summary>
        /// <param name="expression">Выражение - обращение к полю</param>
        /// <returns>Поле</returns>
        [NotNull]
        FieldInfo GetFieldInfo<TResult>(Expression<Func<TResult>> expression);

        /// <summary>
        /// Получить поле
        /// </summary>
        /// <param name="expression">Выражение - обращение к полю</param>
        /// <returns>Поле</returns>
        [NotNull]
        FieldInfo GetFieldInfo<T, TResult>(Expression<Func<T, TResult>> expression);

        /// <summary>
        /// Получить метод генерик-класса
        /// </summary>
        /// <param name="expression">Выражение - вызов метода</param>
        /// <param name="genericParameters">Новые параметры для генерик-класса</param>
        /// <returns>Метод</returns>
        [NotNull]
        MethodInfo GetGenericClassMethod<T>(Expression<Action<T>> expression, params Type[] genericParameters);

        /// <summary>
        /// Получить конструктор
        /// </summary>
        /// <typeparam name="T">Конструируемый тип</typeparam>
        /// <param name="expression">Выражение - вызов конструктора</param>
        /// <returns>Конструктор</returns>
        [NotNull]
        ConstructorInfo GetConstructorInfo<T>(Expression<Func<T>> expression);

        /// <summary>
        /// Получить свойство или поле
        /// </summary>
        /// <param name="expression">Выражение - обращение к свойству или полю</param>
        /// <returns>Свойство или поле</returns>
        [NotNull]
        ValueHoldingMember GetValueHoldingMember(LambdaExpression expression);

        /// <summary>
        /// Получить свойство или поле
        /// </summary>
        /// <param name="expression">Выражение - обращение к свойству или полю</param>
        /// <returns>Свойство или поле</returns>
        [NotNull]
        ValueHoldingMember GetValueHoldingMember<TResult>(Expression<Func<TResult>> expression);

        /// <summary>
        /// Получить свойство или поле
        /// </summary>
        /// <param name="expression">Выражение - обращение к свойству или полю</param>
        /// <returns>Свойство или поле</returns>
        [NotNull]
        ValueHoldingMember GetValueHoldingMember<T, TResult>(Expression<Func<T, TResult>> expression);
    }
}
