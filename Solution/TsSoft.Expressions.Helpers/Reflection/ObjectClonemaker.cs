﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Reflection;

    /// <summary>
    /// Создаёт глубокие копии объектов
    /// </summary>
    public class ObjectClonemaker : IObjectClonemaker
    {
        [NotNull]private readonly IFlatPathParser _pathMapper;
        [NotNull] private readonly IMemberInfoLibrary _memberInfoLibrary;

        [NotNull]private readonly MethodInfo _cloneOneMethod;
        [NotNull]private readonly MethodInfo _cloneManyMethod;
        [NotNull] private readonly MethodInfo _propertyLookupIndexer;
        [NotNull] private readonly MethodInfo _createLookup;

        [NotNull] private readonly ConstantExpression _this;

        [NotNull]private readonly ConcurrentDictionary<KeyValuePair<Type, ClonedPropertyManager>, MulticastDelegate> _copyDelegateCache =
            new ConcurrentDictionary<KeyValuePair<Type, ClonedPropertyManager>, MulticastDelegate>();

        /// <summary>
        /// Создаёт глубокие копии объектов
        /// </summary>
        public ObjectClonemaker(
            [NotNull]IFlatPathParser pathMapper,
            [NotNull]IMemberInfoHelper memberInfoHelper,
            [NotNull]IMemberInfoLibrary memberInfoLibrary)
        {
            if (pathMapper == null) throw new ArgumentNullException("pathMapper");
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (memberInfoLibrary == null) throw new ArgumentNullException("memberInfoLibrary");
            _pathMapper = pathMapper;
            _memberInfoLibrary = memberInfoLibrary;

            // ReSharper disable RedundantTypeArgumentsOfMethod
            _cloneOneMethod = memberInfoHelper.GetGenericDefinitionMethodInfo(() => Clone<object>(new object(), null, null, 0));
            _cloneManyMethod = memberInfoHelper.GetGenericDefinitionMethodInfo(() => Clone<object>(new object[0], null, null, 0));
            _propertyLookupIndexer = memberInfoHelper.GetMethodInfo((ILookup<ValueHoldingMember, IReadOnlyList<ValueHoldingMember>> l) => l[null]);
            _createLookup = memberInfoHelper.GetMethodInfo(() => CreateLookup(null, 0));
            // ReSharper restore RedundantTypeArgumentsOfMethod

            _this = Expression.Constant(this);
        }

        [NotNull]
        private Expression<Func<T, IEnumerable<IReadOnlyList<ValueHoldingMember>>, int, T>> MakeCloneExpression<T>([NotNull]ClonedPropertyManager manager)
            where T: class, new()
        {
            var expressions = new List<Expression>();
            var properties = ValueHoldingMember.GetValueHoldingMembers(typeof(T));
            var source = Expression.Parameter(typeof(T), "source");
            var paths = Expression.Parameter(typeof(IEnumerable<IReadOnlyList<ValueHoldingMember>>), "paths");
            var index = Expression.Parameter(typeof(int), "index");
            var resultObj = Expression.Variable(typeof(T), "result");
            expressions.Add(Expression.Assign(resultObj, Expression.New(typeof(T))));
            var lookup = Expression.Call(
                _this,
                _createLookup,
                paths,
                index);
            var lookupVariable = Expression.Variable(typeof(ILookup<ValueHoldingMember, IReadOnlyList<ValueHoldingMember>>), "pathLookup");
            var pathsThroughVariable = Expression.Variable(typeof(IEnumerable<IReadOnlyList<ValueHoldingMember>>), "pathsThrough");
            expressions.Add(Expression.Assign(lookupVariable, lookup));
            foreach (var property in properties)
            {
                if (property == null)
                {
                    continue;
                }
                MethodInfo deepMethod = null;
                if (manager.Copy(property))
                {
                    expressions.Add(Expression.Assign(
                        Expression.MakeMemberAccess(resultObj, property.Member),
                        Expression.MakeMemberAccess(source, property.Member)));
                }
                else if (manager.SingleClone(property))
                {
                    deepMethod = _cloneOneMethod.MakeGenericMethod(property.ValueType);

                }
                else if (manager.CollectionClone(property))
                {
                    deepMethod = _cloneManyMethod.MakeGenericMethod(
                        property.ValueType.GetGenericEnumerableArgument());
                }
                if (deepMethod != null)
                {
                    expressions.Add(Expression.Assign(
                        pathsThroughVariable,
                        Expression.Call(
                            lookupVariable,
                            _propertyLookupIndexer,
                            new Expression[] { Expression.Constant(property, typeof(ValueHoldingMember)) })));
                    expressions.Add(Expression.IfThen(
                        Expression.AndAlso(
                            Expression.NotEqual(
                                pathsThroughVariable,
                                Expression.Constant(null)),
                            Expression.Call(
                                _memberInfoLibrary.EnumerableAny(typeof(IReadOnlyList<ValueHoldingMember>)),
                                pathsThroughVariable)),
                        Expression.Assign(
                            Expression.MakeMemberAccess(resultObj, property.Member),
                            Expression.Call(
                                _this,
                                deepMethod,
                                Expression.MakeMemberAccess(source, property.Member),
                                Expression.Constant(manager),
                                pathsThroughVariable,
                                Expression.Add(index, Expression.Constant(1))))));
                }
            }
            expressions.Add(resultObj);
            var block = Expression.Block(new[] { resultObj, lookupVariable, pathsThroughVariable }, expressions);
            var checkNullThenClone = Expression.Condition(
                Expression.NotEqual(source, Expression.Constant(null)),
                block,
                Expression.Constant(null, typeof(T)));
            return Expression.Lambda<Func<T, IEnumerable<IReadOnlyList<ValueHoldingMember>>, int, T>>(
                checkNullThenClone,
                source, paths, index);
        }

        private ILookup<ValueHoldingMember, IReadOnlyList<ValueHoldingMember>> CreateLookup(
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, int index)
        {
            return paths.Where(p => p != null && p.Count > index).ToLookup(p => p[index]);
        }

        private Func<T, IEnumerable<IReadOnlyList<ValueHoldingMember>>, int, T> MakeCloneFunc<T>(
            [NotNull] ClonedPropertyManager manager)
            where T: class, new()
        {
            var result = MakeCloneExpression<T>(manager).Compile();
            return result;
        }

        [NotNull]
        private Func<T, IEnumerable<IReadOnlyList<ValueHoldingMember>>, int, T> GetCloneFunc<T>([NotNull]ClonedPropertyManager manager)
            where T : class, new()
        {
            var result = _copyDelegateCache.GetOrAdd(new KeyValuePair<Type, ClonedPropertyManager>(typeof(T), manager), a => MakeCloneFunc<T>(manager))
                as Func<T, IEnumerable<IReadOnlyList<ValueHoldingMember>>, int, T>;
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Copy delegate cache contains null for {0}", typeof(T)));
            }
            return result;
        }

        private T Clone<T>(
            T source, 
            [NotNull]ClonedPropertyManager manager,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, 
            int index)
            where T: class, new()
        {
            var func = GetCloneFunc<T>(manager);
            return func(source, paths, index);
        }

        [ContractAnnotation("objects:notnull => notnull")]
        private List<T> Clone<T>(
            IEnumerable<T> objects, 
            [NotNull]ClonedPropertyManager manager,
            [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, 
            int index)
            where T: class, new()
        {
            return objects != null ? objects.Select(obj => Clone(obj, manager, paths, index)).ToList() : null;
        }


        /// <summary>
        /// Создать глубокую копию объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="obj">Объект</param>
        /// <param name="paths">Пути, по которым осуществляется глубокое копирование</param>
        /// <param name="manager">Настройки копирования</param>
        /// <returns>Копия</returns>
        public T Clone<T>(T obj, IEnumerable<Expression<Func<T, object>>> paths, ClonedPropertyManager manager) 
            where T: class, new()
        {
            return Clone(obj, manager, paths.Select(_pathMapper.Parse), 0);
        }

        /// <summary>
        /// Создать глубокую копию объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="obj">Объект</param>
        /// <param name="paths">Пути, по которым осуществляется глубокое копирование</param>
        /// <param name="manager">Настройки копирования</param>
        /// <returns>Копия</returns>
        public T Clone<T>(T obj, IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, ClonedPropertyManager manager)
            where T : class, new()
        {
            return Clone(obj, manager, paths, 0);
        }

        /// <summary>
        /// Создать глубокую копию объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="objects">Объекты</param>
        /// <param name="paths">Пути, по которым осуществляется глубокое копирование</param>
        /// <param name="manager">Настройки копирования</param>
        /// <returns>Копия</returns>
        public IReadOnlyCollection<T> Clone<T>(IEnumerable<T> objects, IEnumerable<Expression<Func<T, object>>> paths, ClonedPropertyManager manager)
            where T : class, new()
        {
            return Clone(objects, manager, paths.Select(_pathMapper.Parse), 0);
        }

        /// <summary>
        /// Создать глубокую копию объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="objects">Объекты</param>
        /// <param name="paths">Пути, по которым осуществляется глубокое копирование</param>
        /// <param name="manager">Настройки копирования</param>
        /// <returns>Копия</returns>
        public IReadOnlyCollection<T> Clone<T>(IEnumerable<T> objects, IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, ClonedPropertyManager manager) 
            where T : class, new()
        {
            return Clone(objects, manager, paths, 0);
        }
    }
}
