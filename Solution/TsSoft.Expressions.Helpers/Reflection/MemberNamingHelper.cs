﻿using JetBrains.Annotations;

namespace TsSoft.Expressions.Helpers.Reflection
{
    /// <summary>
    /// Определяет именование специальных членов класса
    /// </summary>
    public class MemberNamingHelper : IMemberNamingHelper
    {
        private const string ShadowSuffix = "<shadow>";

        /// <summary>
        /// Получить имя затенённого свойства
        /// </summary>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Имя затенённого свойства</returns>
        public string GetShadowedName([NotNull] string propertyName)
        {
            if (propertyName.EndsWith(ShadowSuffix))
            {
                return propertyName.Substring(0, propertyName.Length - ShadowSuffix.Length);
            }
            return propertyName;
        }

        /// <summary>
        /// Получить каноническое имя теневого свойства
        /// </summary>
        /// <param name="virtualPropertyWithoutSetter">Виртуальное свойство без сеттера</param>
        /// <returns>Имя теневого свойства с сеттером</returns>
        public string WriteableShadow([NotNull] string virtualPropertyWithoutSetter)
        {
            return virtualPropertyWithoutSetter + ShadowSuffix;
        }
    }
}
