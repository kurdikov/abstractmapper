﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Reflection;

    /// <summary>
    /// Создаёт глубокие копии объектов
    /// </summary>
    public interface IObjectClonemaker
    {
        /// <summary>
        /// Создать глубокую копию объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="obj">Объект</param>
        /// <param name="paths">Пути, по которым осуществляется глубокое копирование</param>
        /// <param name="manager">Настройки копирования</param>
        /// <returns>Копия</returns>
        T Clone<T>(T obj, [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, [NotNull]ClonedPropertyManager manager)
            where T : class, new();

        /// <summary>
        /// Создать глубокую копию объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="obj">Объект</param>
        /// <param name="paths">Пути, по которым осуществляется глубокое копирование</param>
        /// <param name="manager">Настройки копирования</param>
        /// <returns>Копия</returns>
        T Clone<T>(T obj, [NotNull]IEnumerable<Expression<Func<T, object>>> paths, [NotNull]ClonedPropertyManager manager)
            where T : class, new();

        /// <summary>
        /// Создать глубокую копию объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="objects">Объекты</param>
        /// <param name="paths">Пути, по которым осуществляется глубокое копирование</param>
        /// <param name="manager">Настройки копирования</param>
        /// <returns>Копия</returns>
        [NotNull]
        IReadOnlyCollection<T> Clone<T>([NotNull]IEnumerable<T> objects, [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, [NotNull]ClonedPropertyManager manager)
            where T : class, new();

        /// <summary>
        /// Создать глубокую копию объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="objects">Объекты</param>
        /// <param name="paths">Пути, по которым осуществляется глубокое копирование</param>
        /// <param name="manager">Настройки копирования</param>
        /// <returns>Копия</returns>
        [NotNull]
        IReadOnlyCollection<T> Clone<T>([NotNull]IEnumerable<T> objects, [NotNull]IEnumerable<Expression<Func<T, object>>> paths, [NotNull]ClonedPropertyManager manager)
            where T : class, new();
    }
}
