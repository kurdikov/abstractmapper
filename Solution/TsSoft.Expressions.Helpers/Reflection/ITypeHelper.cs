﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Хелпер для типов
    /// </summary>
    public interface ITypeHelper
    {
        /// <summary>
        /// IEnumerable[IEnumerable[IEnumerable[T]]] => T
        /// </summary>
        /// <param name="collectionType">IEnumerable[IEnumerable[IEnumerable[T]]]</param>
        Type GetCollectionUnderlyingType([NotNull]Type collectionType);
    }
}
