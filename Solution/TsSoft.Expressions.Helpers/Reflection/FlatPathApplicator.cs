﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Применяет путь к объекту
    /// </summary>
    public class FlatPathApplicator : IFlatPathApplicator
    {
        /// <summary>
        /// Применить путь (последовательность свойств) к объектам
        /// </summary>
        /// <typeparam name="T">Тип начального объекта</typeparam>
        /// <param name="objects">Объекты</param>
        /// <param name="path">Путь</param>
        public IEnumerable<object> Apply<T>(IEnumerable<T> objects, IEnumerable<ValueHoldingMember> path) where T : class
        {
            var currentType = typeof (T);
            var state = objects as IEnumerable<object>;
            foreach (var step in path)
            {
                var storedStep = step;
                if (storedStep == null)
                {
                    throw new ArgumentException("path contains null step");
                }
                if (!currentType.GetTypeInfo().IsAssignableFrom(storedStep.DeclaringType))
                {
                    throw new InvalidOperationException(string.Format("Error on path application: expected type {0}, got {1}", storedStep.DeclaringType, currentType));
                }
                if (storedStep.ValueType.IsGenericEnumerable())
                {
                    currentType = storedStep.ValueType.GetGenericEnumerableArgument();
                    state = state.SelectMany(obj => storedStep.GetValue(obj) as IEnumerable<object>).Where(obj => obj != null);
                }
                else
                {
                    currentType = storedStep.ValueType;
                    state = state.Select(storedStep.GetValue).Where(obj => obj != null);
                }
            }
            return state;
        }
    }
}
