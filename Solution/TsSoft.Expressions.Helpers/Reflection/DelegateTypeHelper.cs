﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Reflection;
    using System.Reflection.Emit;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

    /// <summary>
    /// Обрабатывает типы делегатов
    /// </summary>
    public class DelegateTypeHelper : IDelegateTypeHelper
    {
        [NotNull]
        static readonly Type[] DelegateCtorSignature = { typeof(object), typeof(IntPtr) };

        /// <summary>
        /// Создать тип делегата
        /// </summary>
        /// <param name="moduleBuilder">Модуль</param>
        /// <param name="typeName">Имя типа</param>
        /// <param name="description">Описание типа делегата</param>
        public Type CreateDelegateType(ModuleBuilder moduleBuilder, string typeName, DelegateTypeDescription description)
        {
            const MethodAttributes ctorAttributes = MethodAttributes.RTSpecialName | MethodAttributes.HideBySig | MethodAttributes.Public;
            const MethodImplAttributes implAttributes = MethodImplAttributes.Runtime | MethodImplAttributes.Managed;
            const MethodAttributes invokeAttributes = MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Virtual;
            const TypeAttributes typeAttributes =
                TypeAttributes.Class
                | TypeAttributes.Public
                | TypeAttributes.Sealed
                | TypeAttributes.AnsiClass
                | TypeAttributes.AutoClass;

            var builder = moduleBuilder.DefineType(
                typeName,
                typeAttributes,
                typeof(MulticastDelegate));
            builder.DefineConstructor(ctorAttributes, CallingConventions.Standard, DelegateCtorSignature).SetImplementationFlags(implAttributes);
            builder.DefineMethod("Invoke", invokeAttributes, description.ReturnType, description.ParameterTypes).SetImplementationFlags(implAttributes);
            return builder.CreateTypeInfo().ThrowIfNull("builder.CreateTypeInfo()").AsType();
        }

        /// <summary>
        /// Получить метод вызова делегата
        /// </summary>
        /// <param name="delegateType">Тип делегата</param>
        public MethodInfo GetDelegateInvokeMethod(Type delegateType)
        {
            var method = delegateType.GetTypeInfo().GetMethod("Invoke");
            if (method == null)
            {
                throw new InvalidOperationException(string.Format("{0} is not a delegate type", delegateType));
            }
            return method;
        }

        /// <summary>
        /// Получить конструктор делегата (с параметрами типов <see cref="Object"/> и <see cref="IntPtr"/>)
        /// </summary>
        /// <param name="delegateType">Тип делегата</param>
        public ConstructorInfo GetDelegateConstructor(Type delegateType)
        {
            var ctor = delegateType.GetTypeInfo().GetConstructor(DelegateCtorSignature);
            if (ctor == null)
            {
                throw new InvalidOperationException(string.Format("{0} is not a delegate type", delegateType));
            }
            return ctor;
        }

        /// <summary>
        /// Получить описание типа делегата - типы параметров и тип возвращаемого значения
        /// </summary>
        /// <param name="delegateType">Тип делегата</param>
        public MaterializedDelegateTypeDescription GetDelegateDescription(Type delegateType)
        {
            var method = GetDelegateInvokeMethod(delegateType);
            var parameters = method.GetParameters();
            var parameterTypes = new Type[parameters.Length];
            for (int i = 0; i < parameters.Length; ++i)
            {
                if (parameters[i] == null)
                {
                    throw new NullReferenceException("method.GetParameters() returned collection with null");
                }
                parameterTypes[i] = parameters[i].ParameterType;
            }
            var returnType = method.ReturnType;
            return new MaterializedDelegateTypeDescription(parameterTypes, returnType, delegateType);
        }

        /// <summary>
        /// Является ли тип конкретным типом делегата
        /// </summary>
        public bool IsDelegateType(Type type)
        {
            return type != null && type.GetTypeInfo().IsSubclassOf(typeof(MulticastDelegate));
        }
    }
}
