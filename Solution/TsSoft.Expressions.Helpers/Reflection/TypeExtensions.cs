﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;

    /// <summary>
    /// Расширения типа
    /// </summary>
    public static class TypeExtensions
    {
        [NotNull]
        private static readonly HashSet<Type> NotTrueEnumerableTypes = new HashSet<Type>
            {
                typeof(string),
            }; 

        private static Type FirstGenericArgument([NotNull]Type t)
        {
            if (t.GenericTypeArguments == null)
            {
                throw new ArgumentException("No generic arguments");
            }
            return t.GenericTypeArguments[0];
        }

        /// <summary>
        /// Является ли Nullable[T]
        /// </summary>
        public static bool IsNullableStruct(this Type t)
        {
            var typeInfo = t?.GetTypeInfo();
            return t != null && typeInfo.IsGenericType && typeInfo.GetGenericTypeDefinition() == typeof (Nullable<>);
        }

        /// <summary>
        /// Получить T из Nullable[T]
        /// </summary>
        /// <returns>Генерик-параметр Nullable[T], если аргумент является Nullable[T], в противном случае - аргумент</returns>
        [ContractAnnotation("t:notnull => notnull")]
        public static Type NullableStructUnderlyingType(this Type t)
        {
            if (t == null)
            {
                return null;
            }
            return t.IsNullableStruct() ? FirstGenericArgument(t) : t;
        }

        /// <summary>
        /// Получить все интерфейсы IEnumerable[T], реализуемые типом
        /// </summary>
        [NotNull]
        public static IEnumerable<Type> GetGenericEnumerableInterfaces(this Type t)
        {
            if (t == null)
            {
                return Enumerable.Empty<Type>();
            }
            return t.GetTypeInfo().GetInterfaces().AppendOne(t)
                    .Where(i => i != null && i.GetTypeInfo().IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>));
        }

        /// <summary>
        /// Реализует ли тип IEnumerable[T]; string не считается IEnumerable
        /// </summary>
        public static bool IsGenericEnumerable(this Type t)
        {
            return t != null && !NotTrueEnumerableTypes.Contains(t) && t.GetGenericEnumerableInterfaces().Any();
        }

        /// <summary>
        /// Получить T по IEnumerable[T]
        /// </summary>
        [NotNull]
        public static Type GetGenericEnumerableArgument(this Type t)
        {
            var inter = t.GetGenericEnumerableInterfaces().FirstOrDefault(i => i != null);
            if (inter == null)
            {
                throw new ArgumentException(string.Format("Type {0} is not a generic enumerable type", t));
            }
            var result = FirstGenericArgument(inter);
            if (result == null)
            {
                throw new InvalidOperationException("FirstGenericArgument returned null");
            }
            return result;
        }

        /// <summary>
        /// Получить T по IEnumerable[T]; себя для типов, не реализующих IEnumerable
        /// </summary>
        [ContractAnnotation("t: null => null; notnull => notnull")]
        public static Type GetGenericEnumerableArgumentOrSelf(this Type t)
        {
            if (t == null || NotTrueEnumerableTypes.Contains(t))
            {
                return t;
            }
            var enumerable = GetGenericEnumerableInterfaces(t).FirstOrDefault();
            return enumerable != null ? FirstGenericArgument(enumerable) : t;
        }

        /// <summary>
        /// Является ли ссылочным типом
        /// </summary>
        public static bool IsReferenceType(this Type t)
        {
            var typeInfo = t?.GetTypeInfo();
            return typeInfo != null && (typeInfo.IsClass || typeInfo.IsInterface);
        }

        /// <summary>
        /// Является ли ссылочным типом, отличным от string
        /// </summary>
        public static bool IsReferenceNotStringType(this Type t)
        {
            var typeInfo = t?.GetTypeInfo();
            return typeInfo != null && (typeInfo.IsClass || typeInfo.IsInterface) && t != typeof(string);
        }

        /// <summary>
        /// Содержит ли null в множестве возможных значений
        /// </summary>
        public static bool IsNullableType(this Type t)
        {
            return IsReferenceType(t) || IsNullableStruct(t);
        }

        /// <summary>
        ///     Попытаться получить тип T из Nullable{T}
        /// </summary>
        /// <param name="type">Исходный тип</param>
        /// <param name="valueType">Результирующий тип T или null, если исходный тип не Nullable{T}</param>
        /// <returns>Является ли исходный тип Nullable{T}</returns>
        public static bool TryGetNullableParameter(this Type type, out Type valueType)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type", "Type can not be null");
            }
            valueType = default(Type);
            var typeInfo = type.GetTypeInfo();
            if (typeInfo.IsGenericType && typeInfo.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                valueType = typeInfo.GetGenericArguments()[0];
                return true;
            }
            return false;
        }
    }
}
