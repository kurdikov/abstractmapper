﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

    /// <summary>
    /// Создаёт коллекции с типами, не известными на момент компиляции
    /// </summary>
    public class CollectionReflectionHelper : ICollectionReflectionHelper
    {
        [NotNull] private readonly MethodInfo _makeList;
        [NotNull] private readonly MethodInfo _makeListStrict;

        /// <summary>
        /// Создаёт коллекции с типами, не известными на момент компиляции
        /// </summary>
        public CollectionReflectionHelper(
            [NotNull]IMemberInfoHelper memberInfoHelper)
        {
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            _makeList = memberInfoHelper.GetGenericDefinitionMethodInfo(() => MakeList<object>(null));
            _makeListStrict = memberInfoHelper.GetGenericDefinitionMethodInfo(() => MakeListStrict<object>(null));
        }

        /// <summary>
        /// Создать типизированную коллекцию List[T] из нетипизированной
        /// </summary>
        /// <param name="listElementType">Тип элемента коллекции</param>
        /// <param name="objects">Нетипизированная коллекция</param>
        public object MakeList(Type listElementType, IEnumerable<object> objects)
        {
            var makeListMethod = _makeList.MakeGenericMethod(listElementType);
            var makeListDelegate = DelegateCreator.Create<Func<IEnumerable<object>, object>>(this, makeListMethod);
            return makeListDelegate(objects).ThrowIfNull();
        }

        /// <summary>
        /// Создать типизированную коллекцию List[T] из нетипизированной
        /// </summary>
        /// <param name="listElementType">Тип элемента коллекции</param>
        /// <param name="objects">Нетипизированная коллекция</param>
        /// <exception cref="InvalidCastException">В исходной коллекции есть элементы, не приводимые к T</exception>
        public object MakeListStrict(Type listElementType, IEnumerable<object> objects)
        {
            var makeListMethod = _makeListStrict.MakeGenericMethod(listElementType);
            var makeListDelegate = DelegateCreator.Create<Func<IEnumerable<object>, object>>(this, makeListMethod);
            return makeListDelegate(objects).ThrowIfNull();
        }
        private List<T> MakeList<T>(IEnumerable<object> objects)
        {
            return (objects ?? Enumerable.Empty<object>()).OfType<T>().ToList();
        }

        private List<T> MakeListStrict<T>(IEnumerable<object> objects)
        {
            return (objects ?? Enumerable.Empty<object>()).Cast<T>().ToList();
        }
    }
}
