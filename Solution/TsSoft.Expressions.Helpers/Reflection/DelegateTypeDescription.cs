﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;

    /// <summary>
    /// Описание типа делегата
    /// </summary>
    public class DelegateTypeDescription : IEquatable<DelegateTypeDescription>
    {
        /// <summary>
        /// Типы параметров
        /// </summary>
        [NotNull]public Type[] ParameterTypes { get; private set; }
        /// <summary>
        /// Тип возвращаемого значения
        /// </summary>
        [NotNull]public Type ReturnType { get; private set; }

        /// <summary>
        /// Описание типа делегата
        /// </summary>
        public DelegateTypeDescription([NotNull] IEnumerable<Type> parameterTypes, [NotNull] Type returnType)
        {
            if (parameterTypes == null) throw new ArgumentNullException("parameterTypes");
            if (returnType == null) throw new ArgumentNullException("returnType");
            ParameterTypes = parameterTypes.ToArrayIfNeeded();
            ReturnType = returnType;
        }

        /// <summary>
        /// Совпадают ли типы параметров и тип возвращаемого значения
        /// </summary>
        public bool Equals(DelegateTypeDescription other)
        {
            return other != null && ParameterTypes.SequenceEqual(other.ParameterTypes) && ReturnType == other.ReturnType;
        }

        /// <summary>
        /// Совпадают ли типы параметров и тип возвращаемого значения
        /// </summary>
        public override bool Equals(object obj)
        {
            return Equals(obj as DelegateTypeDescription);
        }

        /// <summary>
        /// Хэш-код
        /// </summary>
        public override int GetHashCode()
        {
            unchecked
            {
                var result = ReturnType.GetHashCode();
                for (int i = 0; i < ParameterTypes.Length; ++i)
                {
                    result = result * 397 ^ (ParameterTypes[i] != null ? ParameterTypes[i].GetHashCode() : 0);
                }
                return result;
            }
        }
    }
}
