﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Выделяет члены типов
    /// </summary>
    public class MemberInfoHelper : IMemberInfoHelper
    {
        [NotNull]
        private T UnwrapLambda<T>(LambdaExpression expression)
            where T: Expression
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            var unary = expression.Body as UnaryExpression;
            var body = (unary != null ? unary.Operand : expression.Body) as T;
            if (body == null)
            {
                throw new ArgumentException(string.Format("Lambda body should be a {0}", typeof(T).Name));
            }
            return body;
        }

        /// <summary>
        /// Получить метод
        /// </summary>
        /// <param name="expression">Выражение - вызов метода</param>
        /// <returns>Метод</returns>
        public MethodInfo GetMethodInfo(LambdaExpression expression)
        {
            return UnwrapLambda<MethodCallExpression>(expression).Method;
        }

        /// <summary>
        /// Получить метод
        /// </summary>
        /// <param name="expression">Выражение - вызов метода</param>
        /// <returns>Метод</returns>
        public MethodInfo GetMethodInfo(Expression<Action> expression)
        {
            return GetMethodInfo((LambdaExpression) expression);
        }

        /// <summary>
        /// Получить метод
        /// </summary>
        /// <param name="expression">Выражение - вызов метода</param>
        /// <returns>Метод</returns>
        public MethodInfo GetMethodInfo<T>(Expression<Action<T>> expression)
        {
            return GetMethodInfo((LambdaExpression)expression);
        }

        /// <summary>
        /// Получить метод
        /// </summary>
        /// <param name="expression">Выражение - вызов метода</param>
        /// <returns>Метод</returns>
        public MethodInfo GetMethodInfo<T, TResult>(Expression<Func<T, TResult>> expression)
        {
            return GetMethodInfo((LambdaExpression)expression);
        }

        [NotNull]
        private MethodInfo GetGenericDefinitionMethodInfo(LambdaExpression expression)
        {
            var genericMethodInfo = GetMethodInfo(expression);
            return genericMethodInfo.IsGenericMethod
                ? genericMethodInfo.GetGenericMethodDefinition()
                : genericMethodInfo;
        }

        /// <summary>
        /// Получить заготовку генерик-метода
        /// </summary>
        /// <param name="expression">Выражение - вызов генерик-метода</param>
        /// <returns>Заготовка метода</returns>
        public MethodInfo GetGenericDefinitionMethodInfo(Expression<Action> expression)
        {
            return GetGenericDefinitionMethodInfo((LambdaExpression) expression);
        }

        /// <summary>
        /// Получить заготовку генерик-метода
        /// </summary>
        /// <param name="expression">Выражение - вызов генерик-метода</param>
        /// <returns>Заготовка метода</returns>
        public MethodInfo GetGenericDefinitionMethodInfo<T>(Expression<Action<T>> expression)
        {
            return GetGenericDefinitionMethodInfo((LambdaExpression)expression);
        }

        /// <summary>
        /// Получить заготовку генерик-метода
        /// </summary>
        /// <param name="expression">Выражение - вызов генерик-метода</param>
        /// <returns>Заготовка метода</returns>
        public MethodInfo GetGenericDefinitionMethodInfo<T, TResult>(Expression<Func<T, TResult>> expression)
        {
            return GetGenericDefinitionMethodInfo((LambdaExpression)expression);
        }

        /// <summary>
        /// Получить свойство
        /// </summary>
        /// <param name="expression">Выражение - обращение к свойству</param>
        /// <returns>Свойство</returns>
        public PropertyInfo GetPropertyInfo(LambdaExpression expression)
        {
            var result = UnwrapLambda<MemberExpression>(expression).Member as PropertyInfo;
            if (result == null)
            {
                throw new ArgumentException("MemberExpression must contain a property");
            }
            return result;
        }

        /// <summary>
        /// Получить свойство
        /// </summary>
        /// <param name="expression">Выражение - обращение к свойству</param>
        /// <returns>Свойство</returns>
        public PropertyInfo GetPropertyInfo<TResult>(Expression<Func<TResult>> expression)
        {
            return GetPropertyInfo((LambdaExpression) expression);
        }

        /// <summary>
        /// Получить свойство
        /// </summary>
        /// <param name="expression">Выражение - обращение к свойству</param>
        /// <returns>Свойство</returns>
        public PropertyInfo GetPropertyInfo<T, TResult>(Expression<Func<T, TResult>> expression)
        {
            return GetPropertyInfo((LambdaExpression)expression);
        }

        /// <summary>
        /// Получить поле
        /// </summary>
        /// <param name="expression">Выражение - обращение к полю</param>
        /// <returns>Поле</returns>
        public FieldInfo GetFieldInfo(LambdaExpression expression)
        {
            var result = UnwrapLambda<MemberExpression>(expression).Member as FieldInfo;
            if (result == null)
            {
                throw new ArgumentException("MemberExpression must contain a field");
            }
            return result;
        }

        /// <summary>
        /// Получить поле
        /// </summary>
        /// <param name="expression">Выражение - обращение к полю</param>
        /// <returns>Поле</returns>
        public FieldInfo GetFieldInfo<TResult>(Expression<Func<TResult>> expression)
        {
            return GetFieldInfo((LambdaExpression)expression);
        }

        /// <summary>
        /// Получить поле
        /// </summary>
        /// <param name="expression">Выражение - обращение к полю</param>
        /// <returns>Поле</returns>
        public FieldInfo GetFieldInfo<T, TResult>(Expression<Func<T, TResult>> expression)
        {
            return GetFieldInfo((LambdaExpression)expression);
        }

        [NotNull]
        private MethodInfo ChangeGenericParameters([NotNull]MethodInfo method, [NotNull]Type[] parameters)
        {
            var declaringType = method.DeclaringType;
            if (declaringType == null)
            {
                throw new ArgumentException("Method without declaring type");
            }
            var genericTypeDefinition = declaringType.GetGenericTypeDefinition();
            if (genericTypeDefinition == null)
            {
                throw new InvalidOperationException(string.Format("GetGenericTypeDefinition returned null for {0}", declaringType));
            }
            var genericType = genericTypeDefinition.MakeGenericType(parameters);
            var parameterTypes = method.GetParameters().Select(p => p.ThrowIfNull("p").ParameterType).ToArray();
            var result = genericType.GetTypeInfo().GetMethod(method.Name, parameterTypes);
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("GetMethod returned null for type {0}, method name {1} and parameter types {2}",
                    genericType, method.Name, string.Join<Type>(", ", parameterTypes)));
            }
            return result;
        }

        /// <summary>
        /// Получить метод генерик-класса
        /// </summary>
        /// <param name="expression">Выражение - вызов метода</param>
        /// <param name="genericParameters">Новые параметры для генерик-класса</param>
        /// <returns>Метод</returns>
        public MethodInfo GetGenericClassMethod<T>(Expression<Action<T>> expression, params Type[] genericParameters)
        {
            var method = GetMethodInfo(expression);
            return ChangeGenericParameters(method, genericParameters ?? new Type[0]);
        }

        /// <summary>
        /// Получить конструктор
        /// </summary>
        /// <typeparam name="T">Конструируемый тип</typeparam>
        /// <param name="expression">Выражение - вызов конструктора</param>
        /// <returns>Конструктор</returns>
        public ConstructorInfo GetConstructorInfo<T>(Expression<Func<T>> expression)
        {
            return UnwrapLambda<NewExpression>(expression).Constructor;
        }

        /// <summary>
        /// Получить свойство или поле
        /// </summary>
        /// <param name="expression">Выражение - обращение к свойству или полю</param>
        /// <returns>Свойство или поле</returns>
        public ValueHoldingMember GetValueHoldingMember(LambdaExpression expression)
        {
            return ValueHoldingMember.GetFromExpression(UnwrapLambda<MemberExpression>(expression));
        }

        /// <summary>
        /// Получить свойство или поле
        /// </summary>
        /// <param name="expression">Выражение - обращение к свойству или полю</param>
        /// <returns>Свойство или поле</returns>
        public ValueHoldingMember GetValueHoldingMember<TResult>(Expression<Func<TResult>> expression)
        {
            return GetValueHoldingMember((LambdaExpression) expression);
        }

        /// <summary>
        /// Получить свойство или поле
        /// </summary>
        /// <param name="expression">Выражение - обращение к свойству или полю</param>
        /// <returns>Свойство или поле</returns>
        public ValueHoldingMember GetValueHoldingMember<T, TResult>(Expression<Func<T, TResult>> expression)
        {
            return GetValueHoldingMember((LambdaExpression)expression);
        }
    }
}
