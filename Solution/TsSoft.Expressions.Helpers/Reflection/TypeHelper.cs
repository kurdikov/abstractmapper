﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;

    internal class TypeHelper : ITypeHelper
    {
        public Type GetCollectionUnderlyingType(Type collectionType)
        {
            var collectionInterface = GetFirstEnumerableInterface(collectionType);
            while (collectionInterface != null)
            {
                collectionType = collectionInterface.GetTypeInfo().GetGenericArguments()[0];
                collectionInterface = GetFirstEnumerableInterface(collectionType);
            }
            return collectionType;
        }

        [CanBeNull]
        private Type GetFirstEnumerableInterface([NotNull]Type collectionType)
        {
            return collectionType.GetTypeInfo().GetInterfaces().AppendOne(collectionType).FirstOrDefault(
                i => i.GetTypeInfo().IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>));
        }
    }
}
