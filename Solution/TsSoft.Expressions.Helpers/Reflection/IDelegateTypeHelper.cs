﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Reflection;
    using System.Reflection.Emit;
    using JetBrains.Annotations;

    /// <summary>
    /// Обрабатывает типы делегатов
    /// </summary>
    public interface IDelegateTypeHelper
    {
        /// <summary>
        /// Создать тип делегата
        /// </summary>
        /// <param name="moduleBuilder">Модуль</param>
        /// <param name="typeName">Имя типа</param>
        /// <param name="description">Описание типа делегата</param>
        [NotNull]
        Type CreateDelegateType([NotNull] ModuleBuilder moduleBuilder, [NotNull]string typeName, [NotNull] DelegateTypeDescription description);

        /// <summary>
        /// Получить метод вызова делегата
        /// </summary>
        /// <param name="delegateType">Тип делегата</param>
        [NotNull]
        MethodInfo GetDelegateInvokeMethod([NotNull]Type delegateType);

        /// <summary>
        /// Получить конструктор делегата (с параметрами типов <see cref="Object"/> и <see cref="IntPtr"/>)
        /// </summary>
        /// <param name="delegateType">Тип делегата</param>
        [NotNull]
        ConstructorInfo GetDelegateConstructor([NotNull] Type delegateType);

        /// <summary>
        /// Получить описание типа делегата - типы параметров и тип возвращаемого значения
        /// </summary>
        /// <param name="delegateType">Тип делегата</param>
        [NotNull]
        MaterializedDelegateTypeDescription GetDelegateDescription([NotNull]Type delegateType);

        /// <summary>
        /// Является ли тип конкретным типом делегата
        /// </summary>
        [ContractAnnotation("null => false")]
        bool IsDelegateType(Type type);
    }
}
