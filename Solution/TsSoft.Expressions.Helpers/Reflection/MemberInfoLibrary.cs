﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Библиотека методов
    /// </summary>
    public class MemberInfoLibrary : IMemberInfoLibrary
    {
        [NotNull]private readonly MethodInfo _enumerableWhere;
        [NotNull]private readonly MethodInfo _enumerableSelect;
        [NotNull]private readonly MethodInfo _enumerableSelectMany;
        [NotNull]private readonly MethodInfo _enumerableToList;
        [NotNull]private readonly MethodInfo _enumerableToArray;
        [NotNull]private readonly MethodInfo _enumerableSingleOrDefault;
        [NotNull]private readonly MethodInfo _enumerableSingleOrDefaultWithLambda;
        [NotNull]private readonly MethodInfo _enumerableFirstOrDefault;
        [NotNull]private readonly MethodInfo _enumerableFirstOrDefaultWithLambda;
        [NotNull]private readonly MethodInfo _enumerableFirst;
        [NotNull]private readonly MethodInfo _enumerableFirstWithLambda;
        [NotNull]private readonly MethodInfo _enumerableLast;
        [NotNull]private readonly MethodInfo _enumerableLastWithLambda;
        [NotNull]private readonly MethodInfo _enumerableLastOrDefault;
        [NotNull]private readonly MethodInfo _enumerableLastOrDefaultWithLambda;
        [NotNull]private readonly MethodInfo _enumerableSingle;
        [NotNull]private readonly MethodInfo _enumerableSingleWithLambda;
        [NotNull]private readonly MethodInfo _enumerableEmpty;
        [NotNull]private readonly MethodInfo _enumerableAny;
        [NotNull]private readonly MethodInfo _enumerableAnyWithLambda;
        [NotNull]private readonly MethodInfo _enumerableCount;
        [NotNull]private readonly MethodInfo _enumerableCountWithLambda;
        [NotNull]private readonly MethodInfo _enumerableAll;
        [NotNull]private readonly MethodInfo _enumerableAsEnumerable;
        [NotNull]private readonly MethodInfo _enumerableContains;
        [NotNull]private readonly MethodInfo _queryableAsQueryable;
        [NotNull]private readonly MethodInfo _objectToString;
        [NotNull]private readonly MethodInfo _stringContains;
        [NotNull]private readonly MethodInfo _stringStartsWith;
        [NotNull]private readonly MethodInfo _stringEndsWith;
        [NotNull]private readonly MethodInfo _stringConcatTwo;
        [NotNull]private readonly MethodInfo _stringConcatThree;
        [NotNull]private readonly MethodInfo _stringConcatFour;
        [NotNull]private readonly MethodInfo _stringConcatArray;
        [NotNull]private readonly MethodInfo _stringConcatTwoObjects;
        [NotNull]private readonly MethodInfo _stringConcatThreeObjects;
        [NotNull]private readonly MethodInfo _stringConcatFourObjects;
        [NotNull]private readonly MethodInfo _stringConcatArrayObjects;
        [NotNull]private readonly MethodInfo _stringTrim;
        [NotNull]private readonly MethodInfo _stringTrimWithParam;
        [NotNull]private readonly MethodInfo _stringTrimEnd;
        [NotNull]private readonly MethodInfo _stringTrimStart;
        [NotNull]private readonly MethodInfo _stringInsert;
        [NotNull]private readonly MethodInfo _stringRemove;
        [NotNull]private readonly MethodInfo _stringRemoveWithLength;
        [NotNull]private readonly MethodInfo _stringSubstring;
        [NotNull]private readonly MethodInfo _stringSubstringWithLength;
        [NotNull]private readonly MethodInfo _stringIndexOf;
        [NotNull]private readonly MethodInfo _stringToLower;
        [NotNull]private readonly MethodInfo _stringToUpper;
        [NotNull]private readonly MethodInfo _stringToLowerInvariant;
        [NotNull]private readonly MethodInfo _stringToUpperInvariant;
        [NotNull]private readonly MethodInfo _ienumeratorMoveNext;
        [NotNull]private readonly MethodInfo _idisposableDispose;
        [NotNull]private readonly MethodInfo _stringIsNullOrEmpty;
        [NotNull]private readonly MethodInfo _stringJoin;
        [NotNull]private readonly MethodInfo _stringJoinArray;
        [NotNull]private readonly MethodInfo _stringFormatOne;
        [NotNull]private readonly MethodInfo _stringFormatTwo;
        [NotNull]private readonly MethodInfo _stringFormatThree;
        [NotNull]private readonly MethodInfo _stringFormatArray;
        [NotNull]private readonly MethodInfo _enumerableGroupBy;
        [NotNull]private readonly MethodInfo _enumerableToLookup;
        [NotNull]private readonly MethodInfo _enumHasFlag;
        [NotNull]private readonly MethodInfo _mathPow;
        [NotNull]private readonly MethodInfo _mathTruncateDecimal;
        [NotNull]private readonly MethodInfo _mathTruncateDouble;
        [NotNull]private readonly MethodInfo _mathCeilingDecimal;
        [NotNull]private readonly MethodInfo _mathCeilingDouble;
        [NotNull]private readonly MethodInfo _mathFloorDecimal;
        [NotNull]private readonly MethodInfo _mathFloorDouble;
        [NotNull]private readonly MethodInfo _mathRoundDecimal;
        [NotNull]private readonly MethodInfo _mathRoundDouble;
        [NotNull]private readonly MethodInfo _mathRoundDecimalWithPrecision;
        [NotNull]private readonly MethodInfo _mathRoundDoubleWithPrecision;
        [NotNull]private readonly MethodInfo _decimalTruncate;
        [NotNull]private readonly MethodInfo _decimalCeiling;
        [NotNull]private readonly MethodInfo _decimalFloor;
#if NET45
        [NotNull]private readonly MethodInfo _decimalRound;
        [NotNull]private readonly MethodInfo _decimalRoundWithPrecision;
#endif
        [NotNull]private readonly MethodInfo _mathAbsDecimal;
        [NotNull]private readonly MethodInfo _mathAbsDouble;
        [NotNull]private readonly MethodInfo _mathAbsFloat;
        [NotNull]private readonly MethodInfo _mathAbsInt;
        [NotNull]private readonly MethodInfo _mathAbsLong;
        [NotNull]private readonly MethodInfo _mathAbsSbyte;
        [NotNull]private readonly MethodInfo _mathAbsShort;

        //[NotNull]private readonly PropertyInfo _stringLength;
        //[NotNull]private readonly PropertyInfo _dateTimeYear;
        //[NotNull]private readonly PropertyInfo _dateTimeMonth;
        //[NotNull]private readonly PropertyInfo _dateTimeDay;
        //[NotNull]private readonly PropertyInfo _dateTimeHour;
        //[NotNull]private readonly PropertyInfo _dateTimeMinute;
        //[NotNull]private readonly PropertyInfo _dateTimeSecond;
        //[NotNull]private readonly PropertyInfo _dateTimeMillisecond;
        //[NotNull]private readonly PropertyInfo _dateTimeOffsetYear;
        //[NotNull]private readonly PropertyInfo _dateTimeOffsetMonth;
        //[NotNull]private readonly PropertyInfo _dateTimeOffsetDay;
        //[NotNull]private readonly PropertyInfo _dateTimeOffsetHour;
        //[NotNull]private readonly PropertyInfo _dateTimeOffsetMinute;
        //[NotNull]private readonly PropertyInfo _dateTimeOffsetSecond;
        //[NotNull]private readonly PropertyInfo _dateTimeOffsetMillisecond;
        //[NotNull]private readonly PropertyInfo _timeSpanHours;
        //[NotNull]private readonly PropertyInfo _timeSpanMinutes;
        //[NotNull]private readonly PropertyInfo _timeSpanSeconds;
        //[NotNull]private readonly PropertyInfo _timeSpanMilliseconds;


        /// <summary>
        /// Библиотека методов
        /// </summary>
        public MemberInfoLibrary([NotNull]IMemberInfoHelper memberInfo)
        {
            if (memberInfo == null) throw new ArgumentNullException("memberInfo");

            // ReSharper disable ReturnValueOfPureMethodIsNotUsed
            // ReSharper disable RedundantCast
            _enumerableSelect = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.Select(oo => oo));
            _enumerableSelectMany = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.SelectMany(oo => new object[0]));
            _enumerableWhere = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.Where(oo => true));
            _enumerableToList = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.ToList());
            _enumerableToArray = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.ToArray());
            _enumerableSingleOrDefault = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.SingleOrDefault());
            _enumerableSingleOrDefaultWithLambda = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.SingleOrDefault(oo => true));
            _enumerableEmpty = memberInfo.GetGenericDefinitionMethodInfo(() => Enumerable.Empty<object>());
            _enumerableAny = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.Any());
            _enumerableAnyWithLambda = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.Any(oo => true));
            _enumerableCount = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.Count());
            _enumerableCountWithLambda = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.Count(oo => true));
            _enumerableAll = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.All(oo => true));
            _enumerableAsEnumerable = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.AsEnumerable());
            _queryableAsQueryable = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.AsQueryable());
            _objectToString = memberInfo.GetMethodInfo((object o) => o.ToString());
            _stringContains = memberInfo.GetMethodInfo((string s) => s.Contains(null));
            _stringStartsWith = memberInfo.GetMethodInfo((string s) => s.StartsWith(null));
            _stringEndsWith = memberInfo.GetMethodInfo((string s) => s.EndsWith(null));
            _stringConcatTwo = memberInfo.GetMethodInfo(() => string.Concat((string)null, null));
            _stringConcatThree = memberInfo.GetMethodInfo(() => string.Concat((string)null, null, null));
            _stringConcatFour = memberInfo.GetMethodInfo(() => string.Concat((string)null, null, null, null));
            _stringConcatArray = memberInfo.GetMethodInfo(() => string.Concat((string[])null));
            _stringConcatTwoObjects = memberInfo.GetMethodInfo(() => string.Concat((object)null, null));
            _stringConcatThreeObjects = memberInfo.GetMethodInfo(() => string.Concat((object)null, null, null));
            _stringConcatFourObjects = memberInfo.GetMethodInfo(() => string.Concat((object)null, null, null, null));
            _stringConcatArrayObjects = memberInfo.GetMethodInfo(() => string.Concat((object[])null));
            _stringTrim = memberInfo.GetMethodInfo(() => "".Trim());
            _stringTrimWithParam = memberInfo.GetMethodInfo(() => "".Trim(null));
            _stringTrimEnd = memberInfo.GetMethodInfo(() => "".TrimEnd());
            _stringTrimStart = memberInfo.GetMethodInfo(() => "".TrimStart());
            _stringInsert = memberInfo.GetMethodInfo(() => "".Insert(0, ""));
            _stringRemove = memberInfo.GetMethodInfo(() => "".Remove(0));
            _stringRemoveWithLength = memberInfo.GetMethodInfo(() => "".Remove(0, 0));
            _stringSubstring = memberInfo.GetMethodInfo(() => "".Substring(0));
            _stringSubstringWithLength = memberInfo.GetMethodInfo(() => "".Substring(0, 0));
            _stringIndexOf = memberInfo.GetMethodInfo(() => "".IndexOf(' '));
            _stringToLower = memberInfo.GetMethodInfo(() => "".ToLower());
            _stringToUpper = memberInfo.GetMethodInfo(() => "".ToUpper());
            _stringToLowerInvariant = memberInfo.GetMethodInfo(() => "".ToLowerInvariant());
            _stringToUpperInvariant = memberInfo.GetMethodInfo(() => "".ToUpperInvariant());
            _ienumeratorMoveNext = memberInfo.GetMethodInfo((IEnumerator enumerator) => enumerator.MoveNext());
            _idisposableDispose = memberInfo.GetMethodInfo((IDisposable o) => o.Dispose());
            _stringIsNullOrEmpty = memberInfo.GetMethodInfo(() => string.IsNullOrEmpty(""));
            _enumerableContains = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.Contains(null));
            _enumerableFirst = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.First());
            _enumerableFirstOrDefault = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.FirstOrDefault());
            _enumerableSingle = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.Single());
            _enumerableFirstWithLambda = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.First(oo => true));
            _enumerableFirstOrDefaultWithLambda = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.FirstOrDefault(oo => true));
            _enumerableSingleWithLambda = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.Single(oo => true));
            _enumerableLast = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.Last());
            _enumerableLastOrDefault = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.LastOrDefault());
            _enumerableLastWithLambda = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.Last(oo => true));
            _enumerableLastOrDefaultWithLambda = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.LastOrDefault(oo => true));
            _stringJoin = memberInfo.GetMethodInfo(() => string.Join("", Enumerable.Empty<string>()));
            _stringJoinArray = memberInfo.GetMethodInfo(() => string.Join("", new string[0]));
            _stringFormatOne = memberInfo.GetMethodInfo(() => string.Format("{0}", ""));
            _stringFormatTwo = memberInfo.GetMethodInfo(() => string.Format("{0} {1}", "", ""));
            _stringFormatThree = memberInfo.GetMethodInfo(() => string.Format("{0} {1} {2}", "", "", ""));
            _stringFormatArray = memberInfo.GetMethodInfo(() => string.Format("{0} {1} {2} {3}", "", "", "", ""));
            _enumerableGroupBy = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.GroupBy(oo => oo));
            _enumerableToLookup = memberInfo.GetGenericDefinitionMethodInfo((IEnumerable<object> o) => o.ToLookup(oo => oo));
            _enumHasFlag = memberInfo.GetMethodInfo((Enum e) => e.HasFlag(null));
            _mathPow = memberInfo.GetMethodInfo(() => Math.Pow(0, 0));
            _mathTruncateDecimal = memberInfo.GetMethodInfo(() => Math.Truncate(0m));
            _mathTruncateDouble = memberInfo.GetMethodInfo(() => Math.Truncate(0d));
            _mathCeilingDecimal = memberInfo.GetMethodInfo(() => Math.Ceiling(0m));
            _mathCeilingDouble = memberInfo.GetMethodInfo(() => Math.Ceiling(0d));
            _mathFloorDecimal = memberInfo.GetMethodInfo(() => Math.Floor(0m));
            _mathFloorDouble = memberInfo.GetMethodInfo(() => Math.Floor(0d));
            _mathRoundDecimal = memberInfo.GetMethodInfo(() => Math.Round(0m));
            _mathRoundDouble = memberInfo.GetMethodInfo(() => Math.Round(0d));
            _mathRoundDecimalWithPrecision = memberInfo.GetMethodInfo(() => Math.Round(0m, 0));
            _mathRoundDoubleWithPrecision = memberInfo.GetMethodInfo(() => Math.Round(0d, 0));
            _decimalTruncate = memberInfo.GetMethodInfo(() => decimal.Truncate(0));
            _decimalCeiling = memberInfo.GetMethodInfo(() => decimal.Ceiling(0));
            _decimalFloor = memberInfo.GetMethodInfo(() => decimal.Floor(0));
#if NET45
            _decimalRound = memberInfo.GetMethodInfo(() => decimal.Round(0));
            _decimalRoundWithPrecision = memberInfo.GetMethodInfo(() => decimal.Round(0, 0));
#endif
            _mathAbsDecimal = memberInfo.GetMethodInfo(() => Math.Abs(0m));
            _mathAbsDouble = memberInfo.GetMethodInfo(() => Math.Abs(0d));
            _mathAbsFloat = memberInfo.GetMethodInfo(() => Math.Abs(0f));
            _mathAbsInt = memberInfo.GetMethodInfo(() => Math.Abs(0));
            _mathAbsLong = memberInfo.GetMethodInfo(() => Math.Abs(0L));
            _mathAbsSbyte = memberInfo.GetMethodInfo(() => Math.Abs((sbyte)0));
            _mathAbsShort = memberInfo.GetMethodInfo(() => Math.Abs((short)0));
            // ReSharper restore ReturnValueOfPureMethodIsNotUsed
            // ReSharper restore RedundantCast
        }

        /// <summary>
        /// Enumerable.Select с одним параметром
        /// </summary>
        public MethodInfo EnumerableSelect(Type from, Type to)
        {
            return _enumerableSelect.MakeGenericMethod(from, to);
        }

        /// <summary>
        /// Enumerable.SelectMany с одним параметром
        /// </summary>
        public MethodInfo EnumerableSelectMany(Type from, Type to)
        {
            return _enumerableSelectMany.MakeGenericMethod(from, to);
        }

        /// <summary>
        /// Enumerable.Where
        /// </summary>
        public MethodInfo EnumerableWhere(Type source)
        {
            return _enumerableWhere.MakeGenericMethod(source);
        }

        /// <summary>
        /// Enumerable.ToList
        /// </summary>
        public MethodInfo EnumerableToList(Type elem)
        {
            return _enumerableToList.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.ToArray
        /// </summary>
        public MethodInfo EnumerableToArray(Type elem)
        {
            return _enumerableToArray.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.SingleOrDefault без параметров
        /// </summary>
        public MethodInfo EnumerableSingleOrDefault(Type elem)
        {
            return _enumerableSingleOrDefault.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.Empty
        /// </summary>
        public MethodInfo EnumerableEmpty(Type elem)
        {
            return _enumerableEmpty.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Является ли метод Enumerable.Empty
        /// </summary>
        public bool IsEnumerableEmpty(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableEmpty);
        }

        /// <summary>
        /// Enumerable.SingleOrDefault с одним параметром
        /// </summary>
        public MethodInfo EnumerableSingleOrDefaultWithLambda(Type elem)
        {
            return _enumerableSingleOrDefaultWithLambda.MakeGenericMethod(elem);
        }

        /// <summary> 
        /// Object.ToString
        /// </summary>
        public MethodInfo ObjectToString()
        {
            return _objectToString;
        }

        /// <summary>
        /// String.Contains
        /// </summary>
        public MethodInfo StringContains()
        {
            return _stringContains;
        }

        /// <summary>
        /// IEnumerator.MoveNext (IEnumerator - не генерик)
        /// </summary>
        public MethodInfo IenumeratorMoveNext()
        {
            return _ienumeratorMoveNext;
        }

        /// <summary>
        /// IDisposable.Dispose
        /// </summary>
        public MethodInfo IdisposableDispose()
        {
            return _idisposableDispose;
        }

        private bool CompareGeneric(MethodInfo method, MethodInfo etalon)
        {
            return method != null && method.IsGenericMethod
                && MemberInfoComparer.Instance.Equals(method.GetGenericMethodDefinition(), etalon);
        }

        /// <summary>
        /// Является ли метод Enumerable.Where
        /// </summary>
        public bool IsEnumerableWhere(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableWhere);
        }

        /// <summary>
        /// Является ли метод Enumerable.Select с одним параметром
        /// </summary>
        public bool IsEnumerableSelect(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableSelect);
        }

        /// <summary>
        /// Является ли метод Enumerable.SelectMany с одним параметром
        /// </summary>
        public bool IsEnumerableSelectMany(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableSelectMany);
        }

        /// <summary>
        /// Является ли метод Enumerable.ToList
        /// </summary>
        public bool IsEnumerableToList(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableToList);
        }

        /// <summary>
        /// Является ли метод Enumerable.Any без параметров
        /// </summary>
        public bool IsEnumerableAny(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableAny);
        }

        /// <summary>
        /// Является ли метод Enumerable.Count без параметров
        /// </summary>
        public bool IsEnumerableCount(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableCount);
        }

        /// <summary>
        /// Является ли метод Enumerable.Any с одним параметром
        /// </summary>
        public bool IsEnumerableAnyWithLambda(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableAnyWithLambda);
        }

        /// <summary>
        /// Является ли метод Enumerable.Count с одним параметром
        /// </summary>
        public bool IsEnumerableCountWithLambda(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableCountWithLambda);
        }

        /// <summary>
        /// Является ли метод Enumerable.All
        /// </summary>
        public bool IsEnumerableAll(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableAll);
        }

        /// <summary>
        /// Является ли метод Enumerable.ToArray
        /// </summary>
        public bool IsEnumerableToArray(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableToArray);
        }

        /// <summary>
        /// Является ли метод Enumerable.AsEnumerable
        /// </summary>
        public bool IsEnumerableAsEnumerable(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableAsEnumerable);
        }

        /// <summary>
        /// Является ли метод Queryable.AsQueryable
        /// </summary>
        public bool IsQueryableAsQueryable(MethodInfo method)
        {
            return CompareGeneric(method, _queryableAsQueryable);
        }

        /// <summary>
        /// Queryable.AsQueryable
        /// </summary>
        public MethodInfo QueryableAsQueryable(Type elem)
        {
            return _queryableAsQueryable.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Является ли метод String.Contains
        /// </summary>
        public bool IsStringContains(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringContains);
        }

        /// <summary>
        /// Является ли метод String.Concat с двумя параметрами (объектами или строками)
        /// </summary>
        public bool IsStringConcatTwo(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringConcatTwo)
                || MemberInfoComparer.Instance.Equals(method, _stringConcatTwoObjects);
        }

        /// <summary>
        /// Является ли метод String.Concat с тремя параметрами (объектами или строками)
        /// </summary>
        public bool IsStringConcatThree(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringConcatThree)
                || MemberInfoComparer.Instance.Equals(method, _stringConcatThreeObjects);
        }

        /// <summary>
        /// Является ли метод String.Concat с четырьмя параметрами (объектами или строками)
        /// </summary>
        public bool IsStringConcatFour(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringConcatFour)
                || MemberInfoComparer.Instance.Equals(method, _stringConcatFourObjects);
        }

        /// <summary>
        /// Является ли метод String.Concat с params (объектов или строк)
        /// </summary>
        public bool IsStringConcatArray(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringConcatArray)
                || MemberInfoComparer.Instance.Equals(method, _stringConcatArrayObjects);
        }

        /// <summary>
        /// Является ли метод String.Trim
        /// </summary>
        public bool IsStringTrim(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringTrim);
        }
        /// <summary>
        /// Является ли метод String.Trim с параметром
        /// </summary>
        public bool IsStringTrimWithParam(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringTrimWithParam);
        }
        /// <summary>
        /// Является ли метод String.TrimEnd
        /// </summary>
        public bool IsStringTrimEnd(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringTrimEnd);
        }
        /// <summary>
        /// Является ли метод String.TrimStart
        /// </summary>
        public bool IsStringTrimStart(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringTrimStart);
        }
        /// <summary>
        /// Является ли метод String.Insert
        /// </summary>
        public bool IsStringInsert(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringInsert);
        }
        /// <summary>
        /// Является ли метод String.Remove с одним параметром
        /// </summary>
        public bool IsStringRemove(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringRemove);
        }
        /// <summary>
        /// Является ли метод String.Remove с двумя параметрами
        /// </summary>
        public bool IsStringRemoveWithLength(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringRemoveWithLength);
        }
        /// <summary>
        /// Является ли метод String.Substring с одним параметром
        /// </summary>
        public bool IsStringSubstring(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringSubstring);
        }
        /// <summary>
        /// Является ли метод String.Substring с двумя параметрами
        /// </summary>
        public bool IsStringSubstringWithLength(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringSubstringWithLength);
        }
        /// <summary>
        /// Является ли метод String.IndexOf с параметром-строкой
        /// </summary>
        public bool IsStringIndexOf(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringIndexOf);
        }
        /// <summary>
        /// Является ли метод String.ToLower без параметров
        /// </summary>
        public bool IsStringToLower(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringToLower);
        }
        /// <summary>
        /// Является ли метод String.ToUpper без параметров
        /// </summary>
        public bool IsStringToUpper(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringToUpper);
        }
        /// <summary>
        /// Является ли метод String.ToLowerInvariant
        /// </summary>
        public bool IsStringToLowerInvariant(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringToLowerInvariant);
        }
        /// <summary>
        /// Является ли метод String.ToUpperInvariant
        /// </summary>
        public bool IsStringToUpperInvariant(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringToUpperInvariant);
        }

        /// <summary>
        /// Является ли метод Object.ToString
        /// </summary>
        public bool IsObjectToString(MethodInfo method)
        {
            return method != null && MemberInfoComparer.Instance.Equals(method.GetBaseDefinition(), _objectToString);
        }

        /// <summary>
        /// Является ли метод String.StartsWith
        /// </summary>
        public bool IsStringStartsWith(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringStartsWith);
        }

        /// <summary>
        /// Является ли метод String.EndsWith
        /// </summary>
        public bool IsStringEndsWith(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringEndsWith);
        }

        /// <summary>
        /// Является ли метод String.IsNullOrEmpty
        /// </summary>
        public bool IsStringIsNullOrEmpty(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _stringIsNullOrEmpty);
        }

        /// <summary>
        /// String.IsNullOrEmpty
        /// </summary>
        public MethodInfo StringIsNullOrEmpty()
        {
            return _stringIsNullOrEmpty;
        }

        [NotNull]
        private readonly HashSet<Type> _aggregateFunctionsPrimitiveTypes = new HashSet<Type>
            {
                typeof(decimal),
                typeof(decimal?),
                typeof(double),
                typeof(double?),
                typeof(float),
                typeof(float?),
                typeof(int),
                typeof(int?),
                typeof(long),
                typeof(long?),
            };

        private bool IsEnumerableAggregateFunction(MethodInfo method, string name)
        {
            if (method == null)
            {
                return false;
            }
            var parameters = method.GetParameters();
            return method.DeclaringType == typeof(Enumerable)
                && method.Name.Equals(name, StringComparison.Ordinal)
                && parameters.Length == 1
                && parameters[0] != null
                && parameters[0].ParameterType.IsGenericEnumerable()
                && _aggregateFunctionsPrimitiveTypes.Contains(parameters[0].ParameterType.GetGenericEnumerableArgument());
        }
        private bool IsEnumerableAggregateFunctionWithLambda(MethodInfo method, string name)
        {
            if (method == null)
            {
                return false;
            }
            var parameters = method.GetParameters();
            var result = method.DeclaringType == typeof(Enumerable)
                        && method.Name.Equals(name, StringComparison.Ordinal)
                        && parameters.Length == 2
                        && parameters[0] != null && parameters[1] != null
                        && parameters[0].ParameterType.IsGenericEnumerable()
                        && parameters[1].ParameterType.GetTypeInfo().IsGenericType
                        && parameters[1].ParameterType.GetGenericTypeDefinition() == typeof(Func<,>);
            if (result)
            {
                var arguments = parameters[1].ParameterType.GenericTypeArguments;
                result = arguments != null && arguments[1] != null && _aggregateFunctionsPrimitiveTypes.Contains(arguments[1]);
            }
            return result;
        }

        /// <summary>
        /// Является ли метод Enumerable.Max без параметров
        /// </summary>
        public bool IsEnumerableMax(MethodInfo method)
        {
            return IsEnumerableAggregateFunction(method, "Max");
        }

        /// <summary>
        /// Является ли метод Enumerable.Min без параметров
        /// </summary>
        public bool IsEnumerableMin(MethodInfo method)
        {
            return IsEnumerableAggregateFunction(method, "Min");
        }

        /// <summary>
        /// Является ли метод Enumerable.Average без параметров
        /// </summary>
        public bool IsEnumerableAvg(MethodInfo method)
        {
            return IsEnumerableAggregateFunction(method, "Average");
        }

        /// <summary>
        /// Является ли метод Enumerable.Sum без параметров
        /// </summary>
        public bool IsEnumerableSum(MethodInfo method)
        {
            return IsEnumerableAggregateFunction(method, "Sum");
        }

        /// <summary>
        /// Является ли метод Enumerable.Max с одним параметром
        /// </summary>
        public bool IsEnumerableMaxWithLambda(MethodInfo method)
        {
            return IsEnumerableAggregateFunctionWithLambda(method, "Max");
        }

        /// <summary>
        /// Является ли метод Enumerable.Min с одним параметром
        /// </summary>
        public bool IsEnumerableMinWithLambda(MethodInfo method)
        {
            return IsEnumerableAggregateFunctionWithLambda(method, "Min");
        }

        /// <summary>
        /// Является ли метод Enumerable.Average с одним параметром
        /// </summary>
        public bool IsEnumerableAvgWithLambda(MethodInfo method)
        {
            return IsEnumerableAggregateFunctionWithLambda(method, "Average");
        }

        /// <summary>
        /// Является ли метод Enumerable.Sum с одним параметром
        /// </summary>
        public bool IsEnumerableSumWithLambda(MethodInfo method)
        {
            return IsEnumerableAggregateFunctionWithLambda(method, "Sum");
        }

        /// <summary>
        /// Является ли метод Enumerable.Contains без параметров
        /// </summary>
        public bool IsEnumerableContains(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableContains);
        }

        /// <summary>
        /// Enumerable.Contains
        /// </summary>
        public MethodInfo EnumerableContains(Type elem)
        {
            return _enumerableContains.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.All
        /// </summary>
        public MethodInfo EnumerableAll(Type elem)
        {
            return _enumerableAll.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.Any без параметров
        /// </summary>
        public MethodInfo EnumerableAny(Type elem)
        {
            return _enumerableAny.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.Any с одним параметром
        /// </summary>
        public MethodInfo EnumerableAnyWithLambda(Type elem)
        {
            return _enumerableAnyWithLambda.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.Count без параметров
        /// </summary>
        public MethodInfo EnumerableCount(Type elem)
        {
            return _enumerableCount.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.Count с одним параметром
        /// </summary>
        public MethodInfo EnumerableCountWithLambda(Type elem)
        {
            return _enumerableCountWithLambda.MakeGenericMethod(elem);
        }

        /// <summary>
        /// String.StartsWith
        /// </summary>
        public MethodInfo StringStartsWith()
        {
            return _stringStartsWith;
        }

        /// <summary>
        /// String.EndsWith
        /// </summary>
        public MethodInfo StringEndsWith()
        {
            return _stringEndsWith;
        }

        /// <summary>
        /// String.Concat с двумя параметрами-строками
        /// </summary>
        public MethodInfo StringConcatTwo()
        {
            return _stringConcatTwo;
        }

        /// <summary>
        /// String.Concat с тремя параметрами-строками
        /// </summary>
        public MethodInfo StringConcatThree()
        {
            return _stringConcatThree;
        }

        /// <summary>
        /// String.Concat с четырьмя параметрами-строками
        /// </summary>
        public MethodInfo StringConcatFour()
        {
            return _stringConcatFour;
        }

        /// <summary>
        /// String.Concat с params
        /// </summary>
        public MethodInfo StringConcatArray()
        {
            return _stringConcatArray;
        }

        /// <summary>
        /// String.Concat с двумя параметрами-объектами
        /// </summary>
        public MethodInfo StringConcatTwoObjects()
        {
            return _stringConcatTwoObjects;
        }

        /// <summary>
        /// String.Concat с тремя параметрами-объектами
        /// </summary>
        public MethodInfo StringConcatThreeObjects()
        {
            return _stringConcatThreeObjects;
        }

        /// <summary>
        /// String.Concat с четырьмя параметрами-объектами
        /// </summary>
        public MethodInfo StringConcatFourObjects()
        {
            return _stringConcatFourObjects;
        }

        /// <summary>
        /// String.Concat с params объектов
        /// </summary>
        public MethodInfo StringConcatArrayObjects()
        {
            return _stringConcatArrayObjects;
        }

        /// <summary>
        /// String.Trim без параметров
        /// </summary>
        public MethodInfo StringTrim()
        {
            return _stringTrim;
        }

        /// <summary>
        /// String.Trim(char[])
        /// </summary>
        public MethodInfo StringTrimWithParam()
        {
            return _stringTrimWithParam;
        }

        /// <summary>
        /// String.TrimStart
        /// </summary>
        public MethodInfo StringTrimStart()
        {
            return _stringTrimStart;
        }

        /// <summary>
        /// String.TrimEnd
        /// </summary>
        public MethodInfo StringTrimEnd()
        {
            return _stringTrimEnd;
        }

        /// <summary>
        /// String.Insert
        /// </summary>
        public MethodInfo StringInsert()
        {
            return _stringInsert;
        }

        /// <summary>
        /// String.Remove(int)
        /// </summary>
        public MethodInfo StringRemove()
        {
            return _stringRemove;
        }

        /// <summary>
        /// String.Remove(int, int)
        /// </summary>
        public MethodInfo StringRemoveWithLength()
        {
            return _stringRemoveWithLength;
        }

        /// <summary>
        /// String.IndexOf(string)
        /// </summary>
        public MethodInfo StringIndexOf()
        {
            return _stringIndexOf;
        }

        /// <summary>
        /// String.ToLower()
        /// </summary>
        public MethodInfo StringToLower()
        {
            return _stringToLower;
        }

        /// <summary>
        /// String.ToUpper()
        /// </summary>
        public MethodInfo StringToUpper()
        {
            return _stringToUpper;
        }

        /// <summary>
        /// String.ToLowerInvariant()
        /// </summary>
        public MethodInfo StringToLowerInvariant()
        {
            return _stringToLowerInvariant;
        }

        /// <summary>
        /// String.ToUpperInvariant()
        /// </summary>
        public MethodInfo StringToUpperInvariant()
        {
            return _stringToUpperInvariant;
        }

        /// <summary>
        /// String.Substring(int)
        /// </summary>
        public MethodInfo StringSubstring()
        {
            return _stringSubstring;
        }

        /// <summary>
        /// String.Substring(int, int)
        /// </summary>
        public MethodInfo StringSubstringWithLength()
        {
            return _stringSubstringWithLength;
        }

        /// <summary>
        /// Enumerable.First с одним параметром
        /// </summary>
        public bool IsEnumerableFirstWithLambda(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableFirstWithLambda);
        }

        /// <summary>
        /// Enumerable.FirstOrDefault с одним параметром
        /// </summary>
        public bool IsEnumerableFirstOrDefaultWithLambda(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableFirstOrDefaultWithLambda);
        }

        /// <summary>
        /// Enumerable.Single с одним параметром
        /// </summary>
        public bool IsEnumerableSingleWithLambda(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableSingleWithLambda);
        }

        /// <summary>
        /// Enumerable.SingleOrDefault с одним параметром
        /// </summary>
        public bool IsEnumerableSingleOrDefaultWithLambda(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableSingleOrDefaultWithLambda);
        }

        /// <summary>
        /// Enumerable.First без параметров
        /// </summary>
        public bool IsEnumerableFirst(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableFirst);
        }

        /// <summary>
        /// Enumerable.FirstOrDefault без параметров
        /// </summary>
        public bool IsEnumerableFirstOrDefault(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableFirstOrDefault);
        }

        /// <summary>
        /// Enumerable.Single без параметров
        /// </summary>
        public bool IsEnumerableSingle(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableSingle);
        }

        /// <summary>
        /// Enumerable.SingleOrDefault без параметров
        /// </summary>
        public bool IsEnumerableSingleOrDefault(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableSingleOrDefault);
        }

        /// <summary>
        /// Enumerable.FirstOrDefault без параметров
        /// </summary>
        public MethodInfo EnumerableFirstOrDefault(Type elem)
        {
            return _enumerableFirstOrDefault.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.FirstOrDefault с одним параметром
        /// </summary>
        public MethodInfo EnumerableFirstOrDefaultWithLambda(Type elem)
        {
            return _enumerableFirstOrDefaultWithLambda.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.First без параметров
        /// </summary>
        public MethodInfo EnumerableFirst(Type elem)
        {
            return _enumerableFirst.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.First с одним параметром
        /// </summary>
        public MethodInfo EnumerableFirstWithLambda(Type elem)
        {
            return _enumerableFirstWithLambda.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.Single без параметров
        /// </summary>
        public MethodInfo EnumerableSingle(Type elem)
        {
            return _enumerableSingle.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.Single с одним параметром
        /// </summary>
        public MethodInfo EnumerableSingleWithLambda(Type elem)
        {
            return _enumerableSingleWithLambda.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.LastOrDefault без параметров
        /// </summary>
        public MethodInfo EnumerableLastOrDefault(Type elem)
        {
            return _enumerableLastOrDefault.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.LastOrDefault с одним параметром
        /// </summary>
        public MethodInfo EnumerableLastOrDefaultWithLambda(Type elem)
        {
            return _enumerableLastOrDefaultWithLambda.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.Last без параметров
        /// </summary>
        public MethodInfo EnumerableLast(Type elem)
        {
            return _enumerableLast.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enumerable.Last с одним параметром
        /// </summary>
        public MethodInfo EnumerableLastWithLambda(Type elem)
        {
            return _enumerableLastWithLambda.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Является ли метод Enumerable.Last с одним параметром
        /// </summary>
        public bool IsEnumerableLastWithLambda(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableLastWithLambda);
        }

        /// <summary>
        /// Является ли метод Enumerable.LastOrDefault с одним параметром
        /// </summary>
        public bool IsEnumerableLastOrDefaultWithLambda(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableLastOrDefaultWithLambda);
        }

        /// <summary>
        /// Является ли метод Enumerable.Last без параметров
        /// </summary>
        public bool IsEnumerableLast(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableLast);
        }

        /// <summary>
        /// Является ли метод Enumerable.LastOrDefault без параметров
        /// </summary>
        public bool IsEnumerableLastOrDefault(MethodInfo method)
        {
            return CompareGeneric(method, _enumerableLastOrDefault);
        }

        /// <summary>
        /// String.Join(string, IEnumerable{string})
        /// </summary>
        public MethodInfo StringJoinEnumerableStrings()
        {
            return _stringJoin;
        }
        /// <summary>
        /// String.Join(string, string[])
        /// </summary>
        public MethodInfo StringJoinArrayStrings()
        {
            return _stringJoinArray;
        }

        /// <summary>
        /// String.Format("{0}", obj)
        /// </summary>
        public MethodInfo StringFormatOne()
        {
            return _stringFormatOne;
        }

        /// <summary>
        /// String.Format("{0} {1}", obj1, obj2)
        /// </summary>
        public MethodInfo StringFormatTwo()
        {
            return _stringFormatTwo;
        }

        /// <summary>
        /// String.Format(string, object, object, object)
        /// </summary>
        public MethodInfo StringFormatThree()
        {
            return _stringFormatThree;
        }

        /// <summary>
        /// String.Format(string, object[])
        /// </summary>
        public MethodInfo StringFormatArray()
        {
            return _stringFormatArray;
        }

        /// <summary>
        /// enumerable.GroupBy(e => e.Selector)
        /// </summary>
        public MethodInfo EnumerableGroupBy(Type elem, Type selected)
        {
            return _enumerableGroupBy.MakeGenericMethod(elem, selected);
        }

        /// <summary>
        /// enumerable.ToLookup(e => e.Selector)
        /// </summary>
        public MethodInfo EnumerableToLookup(Type elem, Type selected)
        {
            return _enumerableToLookup.MakeGenericMethod(elem, selected);
        }

        /// <summary>
        /// Enumerable.AsEnumerable
        /// </summary>
        public MethodInfo EnumerableAsEnumerable(Type elem)
        {
            return _enumerableAsEnumerable.MakeGenericMethod(elem);
        }

        /// <summary>
        /// Enum.HasFlag(enum)
        /// </summary>
        public MethodInfo EnumHasFlag()
        {
            return _enumHasFlag;
        }
        /// <summary>
        /// Является ли метод Enum.HasFlag(enum)
        /// </summary>
        public bool IsEnumHasFlag(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _enumHasFlag);
        }
        /// <summary>
        /// Math.Pow(double, double)
        /// </summary>
        public MethodInfo MathPow()
        {
            return _mathPow;
        }
        /// <summary>
        /// Является ли метод Math.Pow(double, double)
        /// </summary>
        public bool IsMathPow(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _mathPow);
        }
        /// <summary>
        /// Math.Truncate(decimal)
        /// </summary>
        public MethodInfo MathTruncateDecimal()
        {
            return _mathTruncateDecimal;
        }
        /// <summary>
        /// Math.Truncate(double)
        /// </summary>
        public MethodInfo MathTruncateDouble()
        {
            return _mathTruncateDouble;
        }
        /// <summary>
        /// Decimal.Truncate(decimal)
        /// </summary>
        public MethodInfo DecimalTruncate()
        {
            return _decimalTruncate;
        }
        /// <summary>
        /// Является ли метод Math.Truncate(decimal), Math.Truncate(double) или Decimal.Truncate(decimal)
        /// </summary>
        public bool IsStaticTruncate(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _mathTruncateDecimal)
                || MemberInfoComparer.Instance.Equals(method, _mathTruncateDouble)
                || MemberInfoComparer.Instance.Equals(method, _decimalTruncate);
        }
        /// <summary>
        /// Math.Ceiling(decimal)
        /// </summary>
        public MethodInfo MathCeilingDecimal()
        {
            return _mathCeilingDecimal;
        }
        /// <summary>
        /// Math.Ceiling(double)
        /// </summary>
        public MethodInfo MathCeilingDouble()
        {
            return _mathCeilingDouble;
        }
        /// <summary>
        /// Decimal.Ceiling(decimal)
        /// </summary>
        public MethodInfo DecimalCeiling()
        {
            return _decimalCeiling;
        }
        /// <summary>
        /// Является ли метод Math.Ceiling(decimal), Math.Ceiling(double) или Decimal.Ceiling(decimal)
        /// </summary>
        public bool IsStaticCeiling(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _mathCeilingDecimal)
                || MemberInfoComparer.Instance.Equals(method, _mathCeilingDouble)
                || MemberInfoComparer.Instance.Equals(method, _decimalCeiling);
        }
        /// <summary>
        /// Math.Floor(decimal)
        /// </summary>
        public MethodInfo MathFloorDecimal()
        {
            return _mathFloorDecimal;
        }
        /// <summary>
        /// Math.Floor(double)
        /// </summary>
        public MethodInfo MathFloorDouble()
        {
            return _mathFloorDouble;
        }
        /// <summary>
        /// Decimal.Floor(decimal)
        /// </summary>
        public MethodInfo DecimalFloor()
        {
            return _decimalFloor;
        }
        /// <summary>
        /// Является ли метод Math.Floor(decimal), Math.Floor(double) или Decimal.Floor(decimal)
        /// </summary>
        public bool IsStaticFloor(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _mathFloorDecimal)
                || MemberInfoComparer.Instance.Equals(method, _mathFloorDouble)
                || MemberInfoComparer.Instance.Equals(method, _decimalFloor);
        }
        /// <summary>
        /// Math.Round(decimal)
        /// </summary>
        public MethodInfo MathRoundDecimal()
        {
            return _mathRoundDecimal;
        }
        /// <summary>
        /// Math.Round(double)
        /// </summary>
        public MethodInfo MathRoundDouble()
        {
            return _mathRoundDouble;
        }

#if NET45
        /// <summary>
        /// Decimal.Round(decimal)
        /// </summary>
        public MethodInfo DecimalRound()
        {
            return _decimalRound;
        }
#endif

        /// <summary>
        /// Является ли метод Math.Round(decimal), Math.Round(double) или Decimal.Round(decimal)
        /// </summary>
        public bool IsStaticRound(MethodInfo method)
        {
#if NET45
            return MemberInfoComparer.Instance.Equals(method, _mathRoundDecimal)
                || MemberInfoComparer.Instance.Equals(method, _mathRoundDouble)
                || MemberInfoComparer.Instance.Equals(method, _decimalRound);
#else
            return MemberInfoComparer.Instance.Equals(method, _mathRoundDecimal)
                || MemberInfoComparer.Instance.Equals(method, _mathRoundDouble);
#endif
        }
        /// <summary>
        /// Math.Round(decimal, int)
        /// </summary>
        public MethodInfo MathRoundWithPrecisionDecimal()
        {
            return _mathRoundDecimalWithPrecision;
        }
        /// <summary>
        /// Math.Round(double, int)
        /// </summary>
        public MethodInfo MathRoundWithPrecisionDouble()
        {
            return _mathRoundDoubleWithPrecision;
        }

#if NET45
        /// <summary>
        /// Decimal.Round(decimal, int)
        /// </summary>
        public MethodInfo DecimalRoundWithPrecision()
        {
            return _decimalRoundWithPrecision;
        }
#endif

        /// <summary>
        /// Является ли метод Math.Round(decimal, int), Math.Round(double, int) или Decimal.Round(decimal, int)
        /// </summary>
        public bool IsStaticRoundWithPrecision(MethodInfo method)
        {
#if NET45
            return MemberInfoComparer.Instance.Equals(method, _mathRoundDecimalWithPrecision)
                || MemberInfoComparer.Instance.Equals(method, _mathRoundDoubleWithPrecision)
                || MemberInfoComparer.Instance.Equals(method, _decimalRoundWithPrecision);
#else
            return MemberInfoComparer.Instance.Equals(method, _mathRoundDecimalWithPrecision)
                || MemberInfoComparer.Instance.Equals(method, _mathRoundDoubleWithPrecision);
#endif
        }

        /// <summary>
        /// Math.Abs(number)
        /// </summary>
        /// <param name="type">Знаковый числовой тип</param>
        public MethodInfo MathAbs(Type type)
        {
            if (type == typeof(decimal))
            {
                return _mathAbsDecimal;
            }
            if (type == typeof(double))
            {
                return _mathAbsDouble;
            }
            if (type == typeof(float))
            {
                return _mathAbsFloat;
            }
            if (type == typeof(int))
            {
                return _mathAbsInt;
            }
            if (type == typeof(long))
            {
                return _mathAbsLong;
            }
            if (type == typeof(sbyte))
            {
                return _mathAbsSbyte;
            }
            if (type == typeof(short))
            {
                return _mathAbsShort;
            }
            throw new ArgumentOutOfRangeException("type");
        }
        /// <summary>
        /// Является ли метод Math.Abs
        /// </summary>
        public bool IsMathAbs(MethodInfo method)
        {
            return MemberInfoComparer.Instance.Equals(method, _mathAbsDecimal)
                   || MemberInfoComparer.Instance.Equals(method, _mathAbsDouble)
                   || MemberInfoComparer.Instance.Equals(method, _mathAbsFloat)
                   || MemberInfoComparer.Instance.Equals(method, _mathAbsInt)
                   || MemberInfoComparer.Instance.Equals(method, _mathAbsLong)
                   || MemberInfoComparer.Instance.Equals(method, _mathAbsSbyte)
                   || MemberInfoComparer.Instance.Equals(method, _mathAbsShort);
        }
    }
}
