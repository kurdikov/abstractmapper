﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Хелпер для извлечения соответствующих членов типа
    /// </summary>
    public class CorrespondingMemberHelper : ICorrespondingMemberHelper
    {
        /// <summary>
        /// Получить одноимённое свойство или поле
        /// </summary>
        /// <param name="type">Тип, из которого выделяется свойство или поле</param>
        /// <param name="propertyOrField">Эталонное свойство или поле</param>
        public ValueHoldingMember TryGetCorrespondingMember(Type type, MemberInfo propertyOrField)
        {
            var typeInfo = type.GetTypeInfo();
            var result = (MemberInfo)typeInfo.GetProperty(propertyOrField.Name, Type.EmptyTypes) ?? typeInfo.GetField(propertyOrField.Name);
            return result != null ? new ValueHoldingMember(result) : default(ValueHoldingMember);
        }

        /// <summary>
        /// Получить одноимённое свойство или поле
        /// </summary>
        /// <param name="type">Тип, из которого выделяется свойство или поле</param>
        /// <param name="propertyOrField">Эталонное свойство или поле</param>
        public ValueHoldingMember GetCorrespondingMember(Type type, MemberInfo propertyOrField)
        {
            var result = TryGetCorrespondingMember(type, propertyOrField);
            if (result == null)
            {
                throw new InvalidOperationException(
                    string.Format("The type {0} does not contain property or field {1} corresponding to the member with the same name in type {2}",
                                  type,
                                  propertyOrField.Name,
                                  propertyOrField.DeclaringType));
            }
            return result;
        }

        /// <summary>
        /// Получить свойство, соответствующее свойству, явно реализующему интерфейс
        /// </summary>
        /// <param name="explicitProperty">Свойство, явно реализующее интерфейс</param>
        public ValueHoldingMember TryGetCorrespondingMemberForExplicitImplementation(ValueHoldingMember explicitProperty)
        {
            var name = explicitProperty.Name.Substring(explicitProperty.Name.LastIndexOf('.') + 1);
            var candidate = explicitProperty.DeclaringType.GetTypeInfo().GetProperty(name, Type.EmptyTypes);
            return candidate != null && explicitProperty.ValueType.GetTypeInfo().IsAssignableFrom(candidate.PropertyType)
                ? new ValueHoldingMember(candidate)
                : default(ValueHoldingMember);
        }

        /// <summary>
        /// Получить одноимённое свойство
        /// </summary>
        /// <param name="type">Тип, из которого выделяется свойство или поле</param>
        /// <param name="propertyOrField">Эталонное свойство или поле</param>
        public PropertyInfo TryGetCorrespondingProperty(Type type, MemberInfo propertyOrField)
        {
            var result = type.GetTypeInfo().GetProperty(propertyOrField.Name);
            return result;
        }

        [NotNull]
        private Type GetInterfaceType([NotNull]MemberInfo interfaceMember)
        {
            var type = interfaceMember.DeclaringType;
            if (type == null || !type.GetTypeInfo().IsInterface)
            {
                throw new ArgumentException("Member is not declared on interface", "interfaceMember");
            }
            return type;
        }

        /// <summary>
        /// Получить реализацию метода
        /// </summary>
        /// <param name="impl">Реализующий интерфейс тип</param>
        /// <param name="interfaceMethod">Метод интерфейса</param>
        /// <returns>Метод-реализация</returns>
        public MethodInfo GetImplementation(Type impl, MethodInfo interfaceMethod)
        {
            var interfaceType = GetInterfaceType(interfaceMethod);
            var map = impl.GetTypeInfo().GetRuntimeInterfaceMap(interfaceType);
            var index = Array.IndexOf(map.InterfaceMethods, interfaceMethod);
            if (index == -1)
            {
                throw new InvalidOperationException(string.Format("Method {0} not found in {1}", interfaceMethod, interfaceType));
            }
            var result = map.TargetMethods[index];
            return result;
        }

        /// <summary>
        /// Получить реализацию свойства
        /// </summary>
        /// <param name="impl">Реализующий интерфейс тип</param>
        /// <param name="interfaceProperty">Свойство интерфейса</param>
        /// <returns>Свойство-реализация</returns>
        public PropertyInfo GetImplementation(Type impl, PropertyInfo interfaceProperty)
        {
            var method = interfaceProperty.GetMethod;
            MethodInfo implMethod;
            PropertyInfo result;
            var implInfo = impl.GetTypeInfo();
            if (method != null)
            {
                implMethod = GetImplementation(impl, method);
                result = implInfo.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)
                    .SingleOrDefault(p => p.GetMethod == implMethod);
            }
            else
            {
                method = interfaceProperty.SetMethod;
                if (method != null)
                {
                    implMethod = GetImplementation(impl, method);
                    result = implInfo.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)
                        .SingleOrDefault(p => p.SetMethod == implMethod);
                }
                else
                {
                    throw new InvalidOperationException(string.Format("Property {0} has no methods", interfaceProperty));
                }
            }
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Method {0} not found in {1}", implMethod, impl));
            }
            return result;
        }

        /// <summary>
        /// Получить реализацию свойства, если оно определено на интерфейсе, и вернуть исходное свойство в противном случае
        /// </summary>
        /// <param name="targetType">Тип, свойство которого требуется получить</param>
        /// <param name="property">Свойство, возможно, определённое на интерфейсе типа</param>
        public PropertyInfo GetImplementationIfInterfaceProperty(Type targetType, PropertyInfo property)
        {
            if (property.DeclaringType != null && property.DeclaringType.GetTypeInfo().IsInterface && !targetType.GetTypeInfo().IsInterface)
            {
                return GetImplementation(targetType, property);
            }
            return property;
        }

        /// <summary>
        /// Получить реализацию свойства, если оно определено на интерфейсе, и вернуть исходное свойство в противном случае
        /// </summary>
        /// <param name="targetType">Тип, свойство которого требуется получить</param>
        /// <param name="member">Свойство, возможно, определённое на интерфейсе типа</param>
        public ValueHoldingMember GetImplementationIfInterfaceProperty(Type targetType, ValueHoldingMember member)
        {
            var property = member.Property;
            return property != null
                ? new ValueHoldingMember(GetImplementationIfInterfaceProperty(targetType, property))
                : member;
        }
    }
}
