﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using JetBrains.Annotations;

    /// <summary>
    /// Получает типы из сборки
    /// </summary>
    public class AssemblyTypesRetriever : IAssemblyTypesRetriever
    {
        /// <summary>
        /// Получает типы из сборки
        /// </summary>
        public IEnumerable<Type> GetTypes(Assembly assembly)
        {
            try
            {
                return assembly != null ? assembly.GetTypes() : Enumerable.Empty<Type>();
            }
            catch (ReflectionTypeLoadException ex)
            {
                throw new Exception(GetMessage(assembly, ex), ex);
            }
        }

        [NotNull]
        private string GetMessage(Assembly assembly, [NotNull]ReflectionTypeLoadException reflectionTypeLoadException)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("Unable to load assembly {0}. Failed to load types: ", assembly);
            sb.Append(string.Join(", ", reflectionTypeLoadException.Types ?? Enumerable.Empty<Type>()));
            sb.Append(". Exceptions: ");
            sb.Append(string.Join(
                Environment.NewLine,
                reflectionTypeLoadException.LoaderExceptions ?? Enumerable.Empty<Exception>()));
            return sb.ToString();
        }
    }
}
