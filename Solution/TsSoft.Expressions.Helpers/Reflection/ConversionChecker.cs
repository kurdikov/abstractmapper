namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// ��������� ������������� ������� ��������������
    /// </summary>
    public class ConversionChecker : IConversionChecker
    {
        /// <summary>
        /// ��������� ���������� ������������� ������� ��������������
        /// </summary>
        [NotNull]
        public static ConversionChecker Instance = new ConversionChecker();

        /// <summary>
        /// ���������� �� ������� �������������� (���� ����������� ����� ��� ���������� ����� c#)
        /// </summary>
        /// <param name="source">��� �������������� ���������</param>
        /// <param name="target">��� ���������������� ���������</param>
        public bool IsImplicitlyConvertible(Type source, Type target)
        {
            return target.GetTypeInfo().IsAssignableFrom(source)
                   || IsPrimitiveImplicitlyConvertible(source, target)
                   || HasDefinedImplicitConversion(source, target)
                   || target.IsNullableStruct() && IsImplicitlyConvertible(source.IsNullableStruct() ? source.NullableStructUnderlyingType() : source, target.NullableStructUnderlyingType());
        }

        /// <summary>
        /// ���������� �� ������� ��� ����� �������������� (���� ����������� ����� ��� ���������� ����� c#)
        /// </summary>
        /// <param name="source">��� �������������� ���������</param>
        /// <param name="target">��� ���������������� ���������</param>
        public bool IsConvertible([NotNull]Type source, [NotNull]Type target)
        {
            return IsImplicitlyConvertible(source, target)
                   || IsPrimitiveExplicitlyConvertibleAndNotImplicitlyConvertible(source, target)
                   || HasDefinedExplicitConversion(source, target)
                   || target.IsNullableStruct() && IsConvertible(source.IsNullableStruct() ? source.NullableStructUnderlyingType() : source, target.NullableStructUnderlyingType());
        }

        private bool HasDefinedImplicitConversion([NotNull]Type source, [NotNull]Type target)
        {
            return HasDefinedImplicitConversion(source, source, target)
                   || HasDefinedImplicitConversion(target, source, target);
        }

        private bool HasDefinedImplicitConversion([NotNull] Type definedOn, [NotNull] Type source, [NotNull] Type target)
        {
            return definedOn.GetTypeInfo().GetMember("op_Implicit", MemberTypes.Method, BindingFlags.Public | BindingFlags.Static)
                .OfType<MethodInfo>()
                .Any(mi => mi != null && HasConversionMethodSignature(mi, source, target));
        }

        private bool HasDefinedExplicitConversion([NotNull]Type source, [NotNull]Type target)
        {
            return HasDefinedExplicitConversion(source, source, target)
                   || HasDefinedExplicitConversion(target, source, target);
        }

        private bool HasDefinedExplicitConversion([NotNull] Type definedOn, [NotNull] Type source, [NotNull] Type target)
        {
            return definedOn.GetTypeInfo().GetMember("op_Explicit", MemberTypes.Method, BindingFlags.Public | BindingFlags.Static)
                .OfType<MethodInfo>()
                .Any(mi => mi != null && HasConversionMethodSignature(mi, source, target));
        }

        private bool HasConversionMethodSignature([NotNull]MethodInfo method, [NotNull]Type source, [NotNull]Type target)
        {
            if (method.ReturnType != target)
            {
                return false;
            }
            var param = method.GetParameters();
            return param.Length == 1 && param[0] != null && param[0].ParameterType == source;
        }

        /// <summary>
        /// ���������� �� ������� �������������� ����� � ����� c#
        /// </summary>
        /// <param name="source">��� �������������� ���������</param>
        /// <param name="target">��� ���������������� ���������</param>
        public bool IsPrimitiveImplicitlyConvertible(Type source, Type target)
        {
            if (source == target || target == typeof(object))
            {
                return true;
            }
            ISet<Type> convertibleTo;
            return ImplicitConversions.TryGetValue(source, out convertibleTo)
                   && convertibleTo != null
                   && convertibleTo.Contains(target);
        }

        /// <summary>
        /// ���������� �� �������������� ����� � ����� c#
        /// </summary>
        /// <param name="source">��� �������������� ���������</param>
        /// <param name="target">��� ���������������� ���������</param>
        public bool IsPrimitiveExplicitlyConvertible([NotNull]Type source, [NotNull]Type target)
        {
            return IsPrimitiveImplicitlyConvertible(source, target)
                   || IsPrimitiveExplicitlyConvertibleAndNotImplicitlyConvertible(source, target);
        }

        /// <summary>
        /// ���������� �� ����� �������������� ����� � ����� c#
        /// </summary>
        /// <param name="source">��� �������������� ���������</param>
        /// <param name="target">��� ���������������� ���������</param>
        public bool IsPrimitiveExplicitlyConvertibleAndNotImplicitlyConvertible([NotNull]Type source, [NotNull]Type target)
        {
            ISet<Type> convertibleTo;
            return ExplicitConversions.TryGetValue(source, out convertibleTo)
                   && convertibleTo != null
                   && convertibleTo.Contains(target);
        }

        // https://msdn.microsoft.com/library/y5b434w4.aspx
        [NotNull]
        private static readonly IReadOnlyDictionary<Type, ISet<Type>> ImplicitConversions = new Dictionary<Type, ISet<Type>>
        {
            {typeof(sbyte), new HashSet<Type> {typeof(short), typeof(int), typeof(long), typeof(float), typeof(double), typeof(decimal)}},
            {typeof(byte), new HashSet<Type> {typeof(short), typeof(ushort), typeof(int), typeof(uint), typeof(long), typeof(ulong), typeof(float), typeof(double), typeof(decimal)}},
            {typeof(short), new HashSet<Type> {typeof(int), typeof(long), typeof(float), typeof(double), typeof(decimal)}},
            {typeof(ushort), new HashSet<Type> {typeof(int), typeof(uint), typeof(long), typeof(ulong), typeof(float), typeof(double), typeof(decimal)}},
            {typeof(int), new HashSet<Type> {typeof(long), typeof(float), typeof(double), typeof(decimal)}},
            {typeof(uint), new HashSet<Type> {typeof(long), typeof(ulong), typeof(float), typeof(double), typeof(decimal)}},
            {typeof(long), new HashSet<Type> {typeof(float), typeof(double), typeof(decimal)}},
            {typeof(ulong), new HashSet<Type> {typeof(float), typeof(double), typeof(decimal)}},
            {typeof(char), new HashSet<Type> {typeof(ushort), typeof(int), typeof(uint), typeof(long), typeof(ulong), typeof(float), typeof(double), typeof(decimal)}},
            {typeof(float), new HashSet<Type> {typeof(double)}},
        };

        // https://msdn.microsoft.com/library/yht2cx7b.aspx
        [NotNull]
        private static readonly IReadOnlyDictionary<Type, ISet<Type>> ExplicitConversions = new Dictionary<Type, ISet<Type>>
        {
            {typeof(sbyte), new HashSet<Type> {typeof(byte), typeof(ushort), typeof(uint), typeof(ulong), typeof(char)}},
            {typeof(byte), new HashSet<Type> {typeof(sbyte), typeof(char)}},
            {typeof(short), new HashSet<Type> {typeof(sbyte), typeof(byte), typeof(ushort), typeof(uint), typeof(ulong), typeof(char)}},
            {typeof(ushort), new HashSet<Type> {typeof(sbyte), typeof(byte), typeof(short), typeof(char)}},
            {typeof(int), new HashSet<Type> {typeof(sbyte), typeof(byte), typeof(short), typeof(ushort), typeof(uint), typeof(ulong), typeof(char)}},
            {typeof(uint), new HashSet<Type> {typeof(sbyte), typeof(byte), typeof(short), typeof(ushort), typeof(int), typeof(char)}},
            {typeof(long), new HashSet<Type> {typeof(sbyte), typeof(byte), typeof(short), typeof(ushort), typeof(int), typeof(uint), typeof(ulong), typeof(char)}},
            {typeof(ulong), new HashSet<Type> {typeof(sbyte), typeof(byte), typeof(short), typeof(ushort), typeof(int), typeof(uint), typeof(long), typeof(char)}},
            {typeof(char), new HashSet<Type> {typeof(sbyte), typeof(byte), typeof(short)}},
            {typeof(float), new HashSet<Type> {typeof(sbyte), typeof(byte), typeof(short), typeof(ushort), typeof(int), typeof(uint), typeof(long), typeof(ulong), typeof(char), typeof(decimal)}},
            {typeof(double), new HashSet<Type> {typeof(sbyte), typeof(byte), typeof(short), typeof(ushort), typeof(int), typeof(uint), typeof(long), typeof(ulong), typeof(char), typeof(float), typeof(decimal)}},
            {typeof(decimal), new HashSet<Type> {typeof(sbyte), typeof(byte), typeof(short), typeof(ushort), typeof(int), typeof(uint), typeof(long), typeof(ulong), typeof(char), typeof(float), typeof(double)}},
        };
    }
}
