﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Создаёт коллекции с типами, не известными на момент компиляции
    /// </summary>
    public interface ICollectionReflectionHelper
    {
        /// <summary>
        /// Создать типизированную коллекцию List[T] из нетипизированной
        /// </summary>
        /// <param name="listElementType">Тип элемента коллекции</param>
        /// <param name="objects">Нетипизированная коллекция</param>
        [NotNull]
        object MakeList(Type listElementType, IEnumerable<object> objects);
    }
}
