﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Библиотека методов
    /// </summary>
    public interface IMemberInfoLibrary
    {
        /// <summary>
        /// Enumerable.Select с одним параметром
        /// </summary>
        [NotNull]
        MethodInfo EnumerableSelect(Type from, Type to);

        /// <summary>
        /// Enumerable.SelectMany с одним параметром
        /// </summary>
        [NotNull]
        MethodInfo EnumerableSelectMany(Type from, Type to);

        /// <summary>
        /// Enumerable.Where
        /// </summary>
        [NotNull]
        MethodInfo EnumerableWhere(Type source);

        /// <summary>
        /// Enumerable.ToList
        /// </summary>
        [NotNull]
        MethodInfo EnumerableToList(Type elem);

        /// <summary>
        /// Enumerable.ToArray
        /// </summary>
        [NotNull]
        MethodInfo EnumerableToArray(Type elem);

        /// <summary>
        /// Enumerable.SingleOrDefault без параметров
        /// </summary>
        [NotNull]
        MethodInfo EnumerableSingleOrDefault(Type elem);

        /// <summary>
        /// Enumerable.SingleOrDefault с одним параметром
        /// </summary>
        [NotNull]
        MethodInfo EnumerableSingleOrDefaultWithLambda(Type elem);

        /// <summary>
        /// Enumerable.FirstOrDefault без параметров
        /// </summary>
        [NotNull]
        MethodInfo EnumerableFirstOrDefault(Type elem);

        /// <summary>
        /// Enumerable.FirstOrDefault с одним параметром
        /// </summary>
        [NotNull]
        MethodInfo EnumerableFirstOrDefaultWithLambda(Type elem);

        /// <summary>
        /// Enumerable.LastOrDefault без параметров
        /// </summary>
        [NotNull]
        MethodInfo EnumerableLastOrDefault(Type elem);

        /// <summary>
        /// Enumerable.LastOrDefault с одним параметром
        /// </summary>
        [NotNull]
        MethodInfo EnumerableLastOrDefaultWithLambda(Type elem);

        /// <summary>
        /// Enumerable.First без параметров
        /// </summary>
        [NotNull]
        MethodInfo EnumerableFirst(Type elem);

        /// <summary>
        /// Enumerable.First с одним параметром
        /// </summary>
        [NotNull]
        MethodInfo EnumerableFirstWithLambda(Type elem);

        /// <summary>
        /// Enumerable.Last без параметров
        /// </summary>
        [NotNull]
        MethodInfo EnumerableLast(Type elem);

        /// <summary>
        /// Enumerable.Last с одним параметром
        /// </summary>
        [NotNull]
        MethodInfo EnumerableLastWithLambda(Type elem);

        /// <summary>
        /// Enumerable.Single без параметров
        /// </summary>
        [NotNull]
        MethodInfo EnumerableSingle(Type elem);

        /// <summary>
        /// Enumerable.Single с одним параметром
        /// </summary>
        [NotNull]
        MethodInfo EnumerableSingleWithLambda(Type elem);

        /// <summary>
        /// enumerable.GroupBy(e => e.Selector)
        /// </summary>
        [NotNull]
        MethodInfo EnumerableGroupBy(Type elem, Type selected);

        /// <summary>
        /// enumerable.ToLookup(e => e.Selector)
        /// </summary>
        [NotNull]
        MethodInfo EnumerableToLookup(Type elem, Type selected);

        /// <summary>
        /// Enumerable.AsEnumerable
        /// </summary>
        [NotNull]
        MethodInfo EnumerableAsEnumerable(Type elem);

        /// <summary>
        /// Enumerable.Empty
        /// </summary>
        [NotNull]
        MethodInfo EnumerableEmpty(Type elem);

        /// <summary>
        /// Enumerable.Contains
        /// </summary>
        [NotNull]
        MethodInfo EnumerableContains(Type elem);

        /// <summary>
        /// Enumerable.All
        /// </summary>
        [NotNull]
        MethodInfo EnumerableAll(Type elem);

        /// <summary>
        /// Enumerable.Any без параметров
        /// </summary>
        [NotNull]
        MethodInfo EnumerableAny(Type elem);

        /// <summary>
        /// Enumerable.Any с одним параметром
        /// </summary>
        [NotNull]
        MethodInfo EnumerableAnyWithLambda(Type elem);

        /// <summary>
        /// Enumerable.Count без параметров
        /// </summary>
        [NotNull]
        MethodInfo EnumerableCount(Type elem);

        /// <summary>
        /// Enumerable.Count с одним параметром
        /// </summary>
        [NotNull]
        MethodInfo EnumerableCountWithLambda(Type elem);

        /// <summary> 
        /// Object.ToString
        /// </summary>
        [NotNull]
        MethodInfo ObjectToString();

        /// <summary>
        /// String.Contains
        /// </summary>
        [NotNull]
        MethodInfo StringContains();

        /// <summary>
        /// String.StartsWith
        /// </summary>
        [NotNull]
        MethodInfo StringStartsWith();

        /// <summary>
        /// String.EndsWith
        /// </summary>
        [NotNull]
        MethodInfo StringEndsWith();

        /// <summary>
        /// String.Trim
        /// </summary>
        [NotNull]
        MethodInfo StringTrim();

        /// <summary>
        /// IEnumerator.MoveNext (IEnumerator - не генерик)
        /// </summary>
        [NotNull]
        MethodInfo IenumeratorMoveNext();

        /// <summary>
        /// IDisposable.Dispose
        /// </summary>
        [NotNull]
        MethodInfo IdisposableDispose();

        /// <summary>
        /// String.Concat с двумя параметрами-строками
        /// </summary>
        [NotNull]
        MethodInfo StringConcatTwo();

        /// <summary>
        /// String.Concat с тремя параметрами-строками
        /// </summary>
        [NotNull]
        MethodInfo StringConcatThree();

        /// <summary>
        /// String.Concat с четырьмя параметрами-строками
        /// </summary>
        [NotNull]
        MethodInfo StringConcatFour();

        /// <summary>
        /// String.Concat с params
        /// </summary>
        [NotNull]
        MethodInfo StringConcatArray();

        /// <summary>
        /// String.Concat с двумя параметрами-объектами
        /// </summary>
        [NotNull]
        MethodInfo StringConcatTwoObjects();

        /// <summary>
        /// String.Concat с тремя параметрами-объектами
        /// </summary>
        [NotNull]
        MethodInfo StringConcatThreeObjects();

        /// <summary>
        /// String.Concat с четырьмя параметрами-объектами
        /// </summary>
        [NotNull]
        MethodInfo StringConcatFourObjects();

        /// <summary>
        /// String.Concat с params объектов
        /// </summary>
        [NotNull]
        MethodInfo StringConcatArrayObjects();

        /// <summary>
        /// String.Join(string, IEnumerable{string})
        /// </summary>
        [NotNull]
        MethodInfo StringJoinEnumerableStrings();

        /// <summary>
        /// String.Join(string, string[])
        /// </summary>
        [NotNull]
        MethodInfo StringJoinArrayStrings();

        /// <summary>
        /// String.Format(string, object)
        /// </summary>
        [NotNull]
        MethodInfo StringFormatOne();

        /// <summary>
        /// String.Format(string, object, object)
        /// </summary>
        [NotNull]
        MethodInfo StringFormatTwo();

        /// <summary>
        /// String.Format(string, object, object, object)
        /// </summary>
        [NotNull]
        MethodInfo StringFormatThree();

        /// <summary>
        /// String.Format(string, object[])
        /// </summary>
        [NotNull]
        MethodInfo StringFormatArray();


        /// <summary>
        /// Является ли метод Enumerable.Where
        /// </summary>
        bool IsEnumerableWhere(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Select с одним параметром
        /// </summary>
        bool IsEnumerableSelect(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.SelectMany с одним параметром
        /// </summary>
        bool IsEnumerableSelectMany(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.ToList
        /// </summary>
        bool IsEnumerableToList(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Any без параметров
        /// </summary>
        bool IsEnumerableAny(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Count без параметров
        /// </summary>
        bool IsEnumerableCount(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Any с одним параметром
        /// </summary>
        bool IsEnumerableAnyWithLambda(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Count с одним параметром
        /// </summary>
        bool IsEnumerableCountWithLambda(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.ToArray
        /// </summary>
        bool IsEnumerableToArray(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.AsEnumerable
        /// </summary>
        bool IsEnumerableAsEnumerable(MethodInfo method);
        /// <summary>
        /// Является ли метод Queryable.AsQueryable
        /// </summary>
        bool IsQueryableAsQueryable(MethodInfo method);
        /// <summary>
        /// Является ли метод String.Contains
        /// </summary>
        bool IsStringContains(MethodInfo method);
        /// <summary>
        /// Является ли метод String.Concat с двумя параметрами (объектами или строками)
        /// </summary>
        bool IsStringConcatTwo(MethodInfo method);
        /// <summary>
        /// Является ли метод String.Concat с двумя параметрами (объектами или строками)
        /// </summary>
        bool IsStringConcatThree(MethodInfo method);
        /// <summary>
        /// Является ли метод String.Concat с двумя параметрами (объектами или строками)
        /// </summary>
        bool IsStringConcatFour(MethodInfo method);
        /// <summary>
        /// Является ли метод String.Concat с двумя параметрами (объектами или строками)
        /// </summary>
        bool IsStringConcatArray(MethodInfo method);
        /// <summary>
        /// Является ли метод String.StartsWith
        /// </summary>
        bool IsStringStartsWith(MethodInfo method);
        /// <summary>
        /// Является ли метод String.EndsWith
        /// </summary>
        bool IsStringEndsWith(MethodInfo method);
        /// <summary>
        /// Является ли метод String.IsNullOrEmpty
        /// </summary>
        bool IsStringIsNullOrEmpty(MethodInfo method);
        /// <summary>
        /// Является ли метод String.Trim
        /// </summary>
        bool IsStringTrim(MethodInfo method);
        /// <summary>
        /// Является ли метод String.Trim с параметром
        /// </summary>
        bool IsStringTrimWithParam(MethodInfo method);
        /// <summary>
        /// Является ли метод String.TrimEnd
        /// </summary>
        bool IsStringTrimEnd(MethodInfo method);
        /// <summary>
        /// Является ли метод String.TrimStart
        /// </summary>
        bool IsStringTrimStart(MethodInfo method);
        /// <summary>
        /// Является ли метод String.Insert
        /// </summary>
        bool IsStringInsert(MethodInfo method);
        /// <summary>
        /// Является ли метод String.Remove с одним параметром
        /// </summary>
        bool IsStringRemove(MethodInfo method);
        /// <summary>
        /// Является ли метод String.Remove с двумя параметрами
        /// </summary>
        bool IsStringRemoveWithLength(MethodInfo method);
        /// <summary>
        /// Является ли метод String.Substring с одним параметром
        /// </summary>
        bool IsStringSubstring(MethodInfo method);
        /// <summary>
        /// Является ли метод String.Substring с двумя параметрами
        /// </summary>
        bool IsStringSubstringWithLength(MethodInfo method);
        /// <summary>
        /// Является ли метод String.IndexOf с параметром-строкой
        /// </summary>
        bool IsStringIndexOf(MethodInfo method);
        /// <summary>
        /// Является ли метод String.ToLower без параметров
        /// </summary>
        bool IsStringToLower(MethodInfo method);
        /// <summary>
        /// Является ли метод String.ToUpper без параметров
        /// </summary>
        bool IsStringToUpper(MethodInfo method);
        /// <summary>
        /// Является ли метод String.ToLowerInvariant
        /// </summary>
        bool IsStringToLowerInvariant(MethodInfo method);
        /// <summary>
        /// Является ли метод String.ToUpperInvariant
        /// </summary>
        bool IsStringToUpperInvariant(MethodInfo method);
        /// <summary>
        /// Является ли метод Object.ToString
        /// </summary>
        bool IsObjectToString(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.All
        /// </summary>
        bool IsEnumerableAll(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Max без параметров
        /// </summary>
        bool IsEnumerableMax(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Min без параметров
        /// </summary>
        bool IsEnumerableMin(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Average без параметров
        /// </summary>
        bool IsEnumerableAvg(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Sum без параметров
        /// </summary>
        bool IsEnumerableSum(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Max с одним параметром
        /// </summary>
        bool IsEnumerableMaxWithLambda(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Min с одним параметром
        /// </summary>
        bool IsEnumerableMinWithLambda(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Average с одним параметром
        /// </summary>
        bool IsEnumerableAvgWithLambda(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Sum с одним параметром
        /// </summary>
        bool IsEnumerableSumWithLambda(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Contains без параметров
        /// </summary>
        bool IsEnumerableContains(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.First с одним параметром
        /// </summary>
        bool IsEnumerableFirstWithLambda(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.FirstOrDefault с одним параметром
        /// </summary>
        bool IsEnumerableFirstOrDefaultWithLambda(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Last с одним параметром
        /// </summary>
        bool IsEnumerableLastWithLambda(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.LastOrDefault с одним параметром
        /// </summary>
        bool IsEnumerableLastOrDefaultWithLambda(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Single с одним параметром
        /// </summary>
        bool IsEnumerableSingleWithLambda(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.SingleOrDefault с одним параметром
        /// </summary>
        bool IsEnumerableSingleOrDefaultWithLambda(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.SingleOrDefault без параметров
        /// </summary>
        bool IsEnumerableSingleOrDefault(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.First без параметров
        /// </summary>
        bool IsEnumerableFirst(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.FirstOrDefault без параметров
        /// </summary>
        bool IsEnumerableFirstOrDefault(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Single без параметров
        /// </summary>
        bool IsEnumerableSingle(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Last без параметров
        /// </summary>
        bool IsEnumerableLast(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.LastOrDefault без параметров
        /// </summary>
        bool IsEnumerableLastOrDefault(MethodInfo method);
        /// <summary>
        /// Является ли метод Enumerable.Empty
        /// </summary>
        bool IsEnumerableEmpty(MethodInfo method);


        /// <summary>
        /// Queryable.AsQueryable
        /// </summary>
        [NotNull]
        MethodInfo QueryableAsQueryable(Type elem);

        /// <summary>
        /// String.IsNullOrEmpty
        /// </summary>
        [NotNull]
        MethodInfo StringIsNullOrEmpty();

        /// <summary>
        /// String.Trim(char[])
        /// </summary>
        [NotNull]
        MethodInfo StringTrimWithParam();
        /// <summary>
        /// String.TrimStart
        /// </summary>
        [NotNull]
        MethodInfo StringTrimStart();

        /// <summary>
        /// String.TrimEnd
        /// </summary>
        [NotNull]
        MethodInfo StringTrimEnd();

        /// <summary>
        /// String.Insert
        /// </summary>
        [NotNull]
        MethodInfo StringInsert();

        /// <summary>
        /// String.Remove(int)
        /// </summary>
        [NotNull]
        MethodInfo StringRemove();

        /// <summary>
        /// String.Remove(int, int)
        /// </summary>
        [NotNull]
        MethodInfo StringRemoveWithLength();

        /// <summary>
        /// String.IndexOf(string)
        /// </summary>
        [NotNull]
        MethodInfo StringIndexOf();

        /// <summary>
        /// String.ToLower()
        /// </summary>
        [NotNull]
        MethodInfo StringToLower();

        /// <summary>
        /// String.ToUpper()
        /// </summary>
        [NotNull]
        MethodInfo StringToUpper();

        /// <summary>
        /// String.ToLowerInvariant()
        /// </summary>
        [NotNull]
        MethodInfo StringToLowerInvariant();

        /// <summary>
        /// String.ToUpperInvariant()
        /// </summary>
        [NotNull]
        MethodInfo StringToUpperInvariant();

        /// <summary>
        /// String.Substring(int)
        /// </summary>
        [NotNull]
        MethodInfo StringSubstring();

        /// <summary>
        /// String.Substring(int, int)
        /// </summary>
        [NotNull]
        MethodInfo StringSubstringWithLength();

        /// <summary>
        /// Enum.HasFlag(enum)
        /// </summary>
        [NotNull]
        MethodInfo EnumHasFlag();

        /// <summary>
        /// Является ли метод Enum.HasFlag(enum)
        /// </summary>
        bool IsEnumHasFlag(MethodInfo method);

        /// <summary>
        /// Math.Pow(double, double)
        /// </summary>
        [NotNull]
        MethodInfo MathPow();

        /// <summary>
        /// Является ли метод Math.Pow(double, double)
        /// </summary>
        bool IsMathPow(MethodInfo method);

        /// <summary>
        /// Math.Truncate(decimal)
        /// </summary>
        [NotNull]
        MethodInfo MathTruncateDecimal();

        /// <summary>
        /// Math.Truncate(double)
        /// </summary>
        [NotNull]
        MethodInfo MathTruncateDouble();

        /// <summary>
        /// Decimal.Truncate(decimal)
        /// </summary>
        [NotNull]
        MethodInfo DecimalTruncate();

        /// <summary>
        /// Является ли метод Math.Truncate(decimal), Math.Truncate(double) или Decimal.Truncate(decimal)
        /// </summary>
        bool IsStaticTruncate(MethodInfo method);

        /// <summary>
        /// Math.Ceiling(decimal)
        /// </summary>
        [NotNull]
        MethodInfo MathCeilingDecimal();

        /// <summary>
        /// Math.Ceiling(double)
        /// </summary>
        [NotNull]
        MethodInfo MathCeilingDouble();

        /// <summary>
        /// Decimal.Ceiling(decimal)
        /// </summary>
        [NotNull]
        MethodInfo DecimalCeiling();

        /// <summary>
        /// Является ли метод Math.Ceiling(decimal), Math.Ceiling(double) или Decimal.Ceiling(decimal)
        /// </summary>
        bool IsStaticCeiling(MethodInfo method);

        /// <summary>
        /// Math.Floor(decimal)
        /// </summary>
        [NotNull]
        MethodInfo MathFloorDecimal();

        /// <summary>
        /// Math.Floor(double)
        /// </summary>
        [NotNull]
        MethodInfo MathFloorDouble();

        /// <summary>
        /// Decimal.Floor(decimal)
        /// </summary>
        [NotNull]
        MethodInfo DecimalFloor();

        /// <summary>
        /// Является ли метод Math.Floor(decimal), Math.Floor(double) или Decimal.Floor(decimal)
        /// </summary>
        bool IsStaticFloor(MethodInfo method);

        /// <summary>
        /// Math.Round(decimal)
        /// </summary>
        [NotNull]
        MethodInfo MathRoundDecimal();

        /// <summary>
        /// Math.Round(double)
        /// </summary>
        [NotNull]
        MethodInfo MathRoundDouble();

#if NET45
        /// <summary>
        /// Decimal.Round(decimal)
        /// </summary>
        [NotNull]
        MethodInfo DecimalRound();
#endif

        /// <summary>
        /// Является ли метод Math.Round(decimal), Math.Round(double) или Decimal.Round(decimal)
        /// </summary>
        bool IsStaticRound(MethodInfo method);

        /// <summary>
        /// Math.Round(decimal, int)
        /// </summary>
        [NotNull]
        MethodInfo MathRoundWithPrecisionDecimal();

        /// <summary>
        /// Math.Round(double, int)
        /// </summary>
        [NotNull]
        MethodInfo MathRoundWithPrecisionDouble();

#if NET45
        /// <summary>
        /// Decimal.Round(decimal, int)
        /// </summary>
        [NotNull]
        MethodInfo DecimalRoundWithPrecision();
#endif

        /// <summary>
        /// Является ли метод Math.Round(decimal, int), Math.Round(double, int) или Decimal.Round(decimal, int)
        /// </summary>
        bool IsStaticRoundWithPrecision(MethodInfo method);

        /// <summary>
        /// Math.Abs(number)
        /// </summary>
        /// <param name="type">Знаковый числовой тип</param>
        [NotNull]
        MethodInfo MathAbs(Type type);

        /// <summary>
        /// Является ли метод Math.Abs
        /// </summary>
        bool IsMathAbs(MethodInfo method);
    }
}
