﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Применяет путь к объекту
    /// </summary>
    public interface IFlatPathApplicator
    {
        /// <summary>
        /// Применить путь (последовательность свойств) к объектам
        /// </summary>
        /// <typeparam name="T">Тип начального объекта</typeparam>
        /// <param name="objects">Объекты</param>
        /// <param name="path">Путь</param>
        [NotNull]
        IEnumerable<object> Apply<T>([NotNull]IEnumerable<T> objects, [NotNull]IEnumerable<ValueHoldingMember> path) where T : class;
    }
}
