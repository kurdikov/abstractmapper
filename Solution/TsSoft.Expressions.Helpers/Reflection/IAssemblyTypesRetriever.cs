﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Получает типы из сборки
    /// </summary>
    public interface IAssemblyTypesRetriever
    {
        /// <summary>
        /// Получить типы из сборки
        /// </summary>
        /// <param name="assembly">Сборка</param>
        IEnumerable<Type> GetTypes(Assembly assembly);
    }

    /// <summary>
    /// Расширения получателя типов из сборки
    /// </summary>
    public static class AssemblyTypesRetrieverExtensions
    {
        /// <summary>
        /// Получить типы из сборки
        /// </summary>
        [NotNull]
        public static IEnumerable<Type> SafeGetTypes([CanBeNull]this IAssemblyTypesRetriever retriever, [CanBeNull]Assembly assembly)
        {
            return (retriever ?? new AssemblyTypesRetriever()).GetTypes(assembly) ?? Enumerable.Empty<Type>();
        }
    }
}
