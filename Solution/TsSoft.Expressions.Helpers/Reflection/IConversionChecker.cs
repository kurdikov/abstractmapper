﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Проверяет существование неявных преобразований
    /// </summary>
    public interface IConversionChecker
    {
        /// <summary>
        /// Существует ли неявное преобразование (явно определённый метод или соглашение языка c#)
        /// </summary>
        /// <param name="source">Тип преобразуемого выражения</param>
        /// <param name="target">Тип преобразованного выражения</param>
        bool IsImplicitlyConvertible([NotNull]Type source, [NotNull]Type target);

        /// <summary>
        /// Существует ли неявное преобразование типов в языке c#
        /// </summary>
        /// <param name="source">Тип преобразуемого выражения</param>
        /// <param name="target">Тип преобразованного выражения</param>
        bool IsPrimitiveImplicitlyConvertible([NotNull]Type source, [NotNull]Type target);
    }
}
