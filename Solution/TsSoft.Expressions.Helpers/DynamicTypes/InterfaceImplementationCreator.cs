namespace TsSoft.Expressions.Helpers.DynamicTypes
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

    /// <summary>
    /// ������ ����, ����������� �������� ��������� �� ����������
    /// </summary>
    public class InterfaceImplementationCreator : AssemblyBuilderContainer, IInterfaceImplementationCreator
    {
        [NotNull]private readonly IPropertyCreator _propertyCreator;

        /// <summary>
        /// ������ ����, ����������� �������� ��������� �� ����������
        /// </summary>
        public InterfaceImplementationCreator(
            [NotNull]IPropertyCreator propertyCreator)
            : base("TsSoft.InterfaceImplementationCreator")
        {
            if (propertyCreator == null) throw new ArgumentNullException("propertyCreator");
            _propertyCreator = propertyCreator;
        }

        /// <summary>
        /// ������� ������������ ���������� ����������
        /// </summary>
        /// <typeparam name="TInterface">��������� ���������, � ������� ���������� ������ ��������</typeparam>
        public Type CreateInterfaceImplementation<TInterface>()
        {
            return CreateInterfaceImplementation(typeof(TInterface));
        }

        /// <summary>
        /// �������� � ��� �������� ����������
        /// </summary>
        public void AddInterfaceProperties([NotNull]TypeBuilder typeBuilder, [NotNull]Type interfaceType)
        {
            var interfaceTypeInfo = interfaceType.GetTypeInfo();
            foreach (var property in interfaceTypeInfo.GetProperties().Concat(
                interfaceTypeInfo.GetInterfaces().Where(i => i != null).SelectMany(i => i.GetTypeInfo().GetProperties())))
            {
                if (property == null)
                {
                    continue;
                }
                _propertyCreator.CreateAutoProperty(typeBuilder, property.PropertyType, property.Name);
            }
            typeBuilder.AddInterfaceImplementation(interfaceType);
        }

        /// <summary>
        /// ������� ������������ ���������� ����������
        /// </summary>
        /// <param name="interfaceType">��������� ���������, � ������� ���������� ������ ��������</param>
        public Type CreateInterfaceImplementation(Type interfaceType)
        {
            if (interfaceType == null)
            {
                throw new ArgumentNullException("interfaceType");
            }

            var interfaceTypeInfo = interfaceType.GetTypeInfo();
            if (!interfaceTypeInfo.IsInterface)
            {
                throw new ArgumentException(string.Format("{0} is not an interface", interfaceType), "interfaceType");
            }
            if (!interfaceTypeInfo.IsPublic && !interfaceTypeInfo.IsNestedPublic)
            {
                throw new ArgumentException(string.Format("{0} is not a public type", interfaceType), "interfaceType");
            }

            var typeBuilder = DefineTypeWithGuidSuffix(interfaceType.Name, PublicClass);
            AddInterfaceProperties(typeBuilder, interfaceType);
            return typeBuilder.CreateTypeInfo().ThrowIfNull("typeBuilder.CreateTypeInfo()").AsType();
        }

        /// <summary>
        /// ������� ������������ ���������� ���������� �����������
        /// </summary>
        /// <param name="interfaceTypes">��������� ����������, � ������� ���������� ������ ��������</param>
        public Type CreateInterfaceImplementation(params Type[] interfaceTypes)
        {
            if (interfaceTypes == null)
            {
                throw new ArgumentNullException("interfaceTypes");
            }
            var typeBuilder = DefineTypeWithGuidSuffix(string.Empty, PublicClass);
            foreach (var interfaceType in interfaceTypes)
            {
                if (interfaceType != null)
                {
                    AddInterfaceProperties(typeBuilder, interfaceType);
                }
            }
            return typeBuilder.CreateTypeInfo().ThrowIfNull("typeBuilder.CreateTypeInfo()").AsType();
        }
    }
}
