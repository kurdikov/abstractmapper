﻿namespace TsSoft.Expressions.Helpers.DynamicTypes
{
    using System;
    using System.Reflection;
    using System.Reflection.Emit;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

    /// <summary>
    /// Базовый класс для генератора динамических типов
    /// </summary>
    public class AssemblyBuilderContainer
    {
        [NotNull]private readonly Lazy<ModuleBuilder> _moduleBuilder;
        [NotNull]private readonly Lazy<AssemblyBuilder> _assemblyBuilder;
        [NotNull]private readonly string _dynamicAssemblyName;

        /// <summary>
        /// Динамический модуль
        /// </summary>
        [NotNull]
        protected ModuleBuilder ModuleBuilder {get { return _moduleBuilder.Value.ThrowIfNull("_moduleBuilder.Value"); }}
        /// <summary>
        /// Динамическая сборка
        /// </summary>
        [NotNull]
        protected AssemblyBuilder AssemblyBuilder {get {return _assemblyBuilder.Value.ThrowIfNull("_assemblyBuilder.Value");}}

        private const string DynamicModuleName = "DynamicModule";

        /// <summary>
        /// Создать контейнер динамической сборки
        /// </summary>
        /// <param name="dynamicAssemblyName">Имя динамической сборки</param>
        public AssemblyBuilderContainer([NotNull] string dynamicAssemblyName)
        {
            if (dynamicAssemblyName == null) throw new ArgumentNullException("dynamicAssemblyName");
            var assemblyName = new AssemblyName(dynamicAssemblyName);
            _dynamicAssemblyName = dynamicAssemblyName;
            _assemblyBuilder = new Lazy<AssemblyBuilder>(() =>
                AssemblyBuilder.DefineDynamicAssembly(assemblyName, GetAssemblyBuilderAccess()).ThrowIfNull("DynamicAssemblyBuilder"));
            _moduleBuilder = new Lazy<ModuleBuilder>(() =>
                DefineDynamicModule(dynamicAssemblyName));
        }

        private AssemblyBuilderAccess GetAssemblyBuilderAccess()
        {
#if NET45
            return AssemblyBuilderAccess.RunAndSave;
#else
            return AssemblyBuilderAccess.Run;
#endif
        }

        private ModuleBuilder DefineDynamicModule(string dynamicAssemblyName)
        {
#if NET45
            return AssemblyBuilder.DefineDynamicModule(DynamicModuleName, dynamicAssemblyName + ".dll");
#else
            return AssemblyBuilder.DefineDynamicModule(DynamicModuleName);
#endif
        }

#if NET45
        /// <summary>
        /// Сохранить сборку с динамическими типами
        /// </summary>
        public void SaveAssembly()
        {
            AssemblyBuilder.Save(_dynamicAssemblyName + ".dll");
        }
#endif

        /// <summary>
        /// Атрибуты для публичного класса
        /// </summary>
        protected readonly TypeAttributes PublicClass =
            TypeAttributes.Class |
            TypeAttributes.AutoClass |
            TypeAttributes.AnsiClass |
            TypeAttributes.BeforeFieldInit |
            TypeAttributes.AutoLayout |
            TypeAttributes.Public;

        /// <summary>
        /// Атрибуты для публичного интерфейса
        /// </summary>
        protected readonly TypeAttributes PublicInterface =
            TypeAttributes.Public |
            TypeAttributes.Interface |
            TypeAttributes.Abstract;

        /// <summary>
        /// Создать тип с именем с уникальным суффиксом
        /// </summary>
        [NotNull]
        protected TypeBuilder DefineTypeWithGuidSuffix(string name)
        {
            return ModuleBuilder.DefineType(name + "_" + Guid.NewGuid().ToString("N"));
        }

        /// <summary>
        /// Создать тип с именем с уникальным суффиксом
        /// </summary>
        [NotNull]
        protected TypeBuilder DefineTypeWithGuidSuffix(string name, TypeAttributes attr)
        {
            return ModuleBuilder.DefineType(name + "_" + Guid.NewGuid().ToString("N"), attr);
        }

        /// <summary>
        /// Создать тип с именем с уникальным суффиксом
        /// </summary>
        [NotNull]
        protected TypeBuilder DefineTypeWithGuidSuffix(string name, TypeAttributes attr, Type parent)
        {
            return ModuleBuilder.DefineType(name + "_" + Guid.NewGuid().ToString("N"), attr, parent);
        }
    }
}
