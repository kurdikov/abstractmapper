﻿namespace TsSoft.Expressions.Helpers.DynamicTypes
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Строит типы, реализующие заданный интерфейс со свойствами
    /// </summary>
    public interface IInterfaceImplementationCreator
    {
        /// <summary>
        /// Создать динамическую реализацию интерфейса
        /// </summary>
        /// <typeparam name="TInterface">Публичный интерфейс, в котором содержатся только свойства</typeparam>
        [NotNull]
        Type CreateInterfaceImplementation<TInterface>();

        /// <summary>
        /// Создать динамическую реализацию интерфейса
        /// </summary>
        /// <param name="interfaceType">Публичный интерфейс, в котором содержатся только свойства</param>
        [NotNull]
        Type CreateInterfaceImplementation([NotNull] Type interfaceType);

        /// <summary>
        /// Создать динамическую реализацию нескольких интерфейсов
        /// </summary>
        /// <param name="interfaceTypes">Публичные интерфейсы, в которых содержатся только свойства</param>
        [NotNull]
        Type CreateInterfaceImplementation([NotNull] params Type[] interfaceTypes);
    }
}
