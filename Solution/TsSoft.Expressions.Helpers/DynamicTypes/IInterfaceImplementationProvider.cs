﻿namespace TsSoft.Expressions.Helpers.DynamicTypes
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Предоставляет динамические реализации интерфейсов, кэшируя их
    /// </summary>
    public interface IInterfaceImplementationProvider
    {
        /// <summary>
        /// Получить динамическую реализацию интерфейса
        /// </summary>
        /// <param name="interfaceType">Публичный интерфейс, содержащий только свойства</param>
        [NotNull]
        Type GetInterfaceImplementation([NotNull]Type interfaceType);
    }
}
