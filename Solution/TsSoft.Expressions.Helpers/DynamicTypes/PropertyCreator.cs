﻿namespace TsSoft.Expressions.Helpers.DynamicTypes
{
    using System;
    using System.Reflection;
    using System.Reflection.Emit;
    using JetBrains.Annotations;

    /// <summary>
    /// Создаёт свойства в динамических типах
    /// </summary>
    public class PropertyCreator : IPropertyCreator
    {
        const MethodAttributes PublicMethod = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual;
        const MethodAttributes InterfaceMethod = MethodAttributes.Public | MethodAttributes.Abstract | MethodAttributes.Virtual | MethodAttributes.SpecialName | MethodAttributes.HideBySig;

        [NotNull]
        private string GetBackingFieldName(string propertyName)
        {
            return string.Format("<{0}>k__BackingField", propertyName);
        }

        [NotNull]
        private string GetGetMethodName(string propertyName)
        {
            return string.Format("get_{0}", propertyName);
        }

        [NotNull]
        private string GetSetMethodName(string propertyName)
        {
            return string.Format("set_{0}", propertyName);
        }

        /// <summary>
        /// Создать свойство, скрывающее поле
        /// </summary>
        /// <param name="type">Тип, в котором создаётся свойство</param>
        /// <param name="propertyType">Тип свойства</param>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Построенное свойство</returns>
        public PropertyBuilder CreateAutoProperty(TypeBuilder type, Type propertyType, string propertyName)
        {
            var fieldName = GetBackingFieldName(propertyName);
            var field = type.DefineField(fieldName, propertyType, FieldAttributes.Private);
            
            var getMethod = type.DefineMethod(GetGetMethodName(propertyName), PublicMethod, propertyType, Type.EmptyTypes);
            EmitGetter(getMethod, field);

            var setMethod = type.DefineMethod(GetSetMethodName(propertyName), PublicMethod, null, new[] {propertyType});
            EmitSetter(setMethod, field);

            var prop = type.DefineProperty(propertyName, PropertyAttributes.HasDefault, propertyType, Type.EmptyTypes);
            prop.SetGetMethod(getMethod);
            prop.SetSetMethod(setMethod);

            return prop;
        }

        private void EmitGetter([NotNull]MethodBuilder getMethod, [NotNull]FieldBuilder field)
        {
            var getMethodEmitter = getMethod.GetILGenerator();
            getMethodEmitter.Emit(OpCodes.Ldarg_0);
            getMethodEmitter.Emit(OpCodes.Ldfld, field);
            getMethodEmitter.Emit(OpCodes.Ret);
        }

        private void EmitSetter([NotNull]MethodBuilder setMethod, [NotNull]FieldBuilder field)
        {
            var setMethodEmitter = setMethod.GetILGenerator();
            setMethodEmitter.Emit(OpCodes.Ldarg_0);
            setMethodEmitter.Emit(OpCodes.Ldarg_1);
            setMethodEmitter.Emit(OpCodes.Stfld, field);
            setMethodEmitter.Emit(OpCodes.Ret);
        }

        /// <summary>
        /// Переопределить геттер виртуального свойства (без сеттера), выдавая из него значение другого свойства с сеттером
        /// </summary>
        /// <param name="type">Строящийся тип</param>
        /// <param name="propertyType">Тип свойства</param>
        /// <param name="overriddenPropertyName">Имя виртуального свойства</param>
        /// <param name="newPropertyName">Имя теневого свойства</param>
        /// <returns>Определённое теневое свойство</returns>
        public PropertyBuilder MakeWriteableShadow(
            TypeBuilder type,
            Type propertyType,
            string overriddenPropertyName,
            string newPropertyName)
        {
            var fieldName = GetBackingFieldName(newPropertyName);
            var field = type.DefineField(fieldName, propertyType, FieldAttributes.Private);

            var getMethod = type.DefineMethod(GetGetMethodName(newPropertyName), PublicMethod, propertyType, Type.EmptyTypes);
            EmitGetter(getMethod, field);

            var setMethod = type.DefineMethod(GetSetMethodName(newPropertyName), PublicMethod, null, new[] { propertyType });
            EmitSetter(setMethod, field);

            var newProp = type.DefineProperty(newPropertyName, PropertyAttributes.None, propertyType, Type.EmptyTypes);
            newProp.SetGetMethod(getMethod);
            newProp.SetSetMethod(setMethod);

            var oldProp = type.BaseType.GetTypeInfo().GetProperty(overriddenPropertyName);
            var oldPropGet = type.DefineMethod(oldProp.GetGetMethod().Name, PublicMethod, propertyType, Type.EmptyTypes);
            EmitGetter(oldPropGet, field);

            return newProp;
        }

        /// <summary>
        /// Определить свойство в типе (например, в интерфейсе)
        /// </summary>
        /// <param name="type">Тип</param>
        /// <param name="propertyType">Тип свойства</param>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Определённое свойство</returns>
        public PropertyBuilder DefineProperty(TypeBuilder type, Type propertyType, string propertyName)
        {
            var getMethod = type.DefineMethod(GetGetMethodName(propertyName), InterfaceMethod, propertyType, Type.EmptyTypes);
            var setMethod = type.DefineMethod(GetSetMethodName(propertyName), InterfaceMethod, null, new[] { propertyType });
            var prop = type.DefineProperty(propertyName, PropertyAttributes.None, propertyType, Type.EmptyTypes);
            prop.SetGetMethod(getMethod);
            prop.SetSetMethod(setMethod);
            return prop;
        }
    }
}
