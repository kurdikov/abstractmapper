namespace TsSoft.Expressions.Helpers.DynamicTypes
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

    class InterfaceImplementationProvider : IInterfaceImplementationProvider
    {
        [NotNull] private readonly IInterfaceImplementationCreator _creator;
        [NotNull] private readonly LazyConcurrentDictionary<Type, Type> _cache = new LazyConcurrentDictionary<Type, Type>();

        public InterfaceImplementationProvider([NotNull] IInterfaceImplementationCreator creator)
        {
            if (creator == null) throw new ArgumentNullException("creator");
            _creator = creator;
        }

        public Type GetInterfaceImplementation(Type interfaceType)
        {
            return _cache.LazyGetOrAdd(interfaceType, _creator.CreateInterfaceImplementation).ThrowIfNull();
        }
    }
}
