﻿namespace TsSoft.Expressions.Helpers.DynamicTypes
{
    using System;
    using System.Reflection.Emit;
    using JetBrains.Annotations;

    /// <summary>
    /// Создаёт свойства в динамических типах
    /// </summary>
    public interface IPropertyCreator
    {
        /// <summary>
        /// Создать свойство, скрывающее поле
        /// </summary>
        /// <param name="type">Тип, в котором создаётся свойство</param>
        /// <param name="propertyType">Тип свойства</param>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Построенное свойство</returns>
        [NotNull]
        PropertyBuilder CreateAutoProperty([NotNull]TypeBuilder type, [NotNull]Type propertyType, [NotNull]string propertyName);

        /// <summary>
        /// Определить свойство в типе (например, в интерфейсе)
        /// </summary>
        /// <param name="type">Тип</param>
        /// <param name="propertyType">Тип свойства</param>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Определённое свойство</returns>
        [NotNull]
        PropertyBuilder DefineProperty([NotNull] TypeBuilder type, [NotNull] Type propertyType,
            [NotNull] string propertyName);

        /// <summary>
        /// Переопределить геттер виртуального свойства (без сеттера), выдавая из него значение другого свойства с сеттером
        /// </summary>
        /// <param name="type">Строящийся тип</param>
        /// <param name="propertyType">Тип свойства</param>
        /// <param name="overriddenPropertyName">Имя виртуального свойства</param>
        /// <param name="newPropertyName">Имя теневого свойства</param>
        /// <returns>Определённое теневое свойство</returns>
        PropertyBuilder MakeWriteableShadow(
            TypeBuilder type,
            Type propertyType,
            string overriddenPropertyName,
            string newPropertyName);
    }
}
