﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    internal class PathHelper : IPathHelper
    {
        public Type GetPathSingleType(Type startType, IReadOnlyList<ValueHoldingMember> path)
        {
            var lastPathProperty = path.LastOrDefault();
            var pathType = lastPathProperty != null ? lastPathProperty.ValueType : startType;
            var pathSingleType = pathType.IsGenericEnumerable()
                                     ? pathType.GetGenericEnumerableArgument()
                                     : pathType;
            return pathSingleType;
        }

        public Type GetPathSingleType(Type startType, IReadOnlyList<PropertyInfo> path)
        {
            var lastPathProperty = path.LastOrDefault();
            var pathType = lastPathProperty != null ? lastPathProperty.PropertyType : startType;
            var pathSingleType = pathType.IsGenericEnumerable()
                                     ? pathType.GetGenericEnumerableArgument()
                                     : pathType;
            return pathSingleType;
        }


        public Type GetPathSingleType(ParsedPath path)
        {
            if (path.Elements.Count < 1)
            {
                return path.Start.Type;
            }
            var lastPathElement = path.LastElement();
            var pathType = lastPathElement.Step.ValueType;
            var pathSingleType = pathType.IsGenericEnumerable()
                                     ? pathType.GetGenericEnumerableArgument()
                                     : pathType;
            return pathSingleType;
        }
    }
}
