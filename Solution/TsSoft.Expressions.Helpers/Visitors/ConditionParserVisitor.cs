﻿// ReSharper disable RedundantOverridenMember
namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Обходчик выражения-условия, выделяющий используемые пути
    /// </summary>
    public sealed class ConditionParserVisitor : ExpressionVisitor
    {
        /// <summary>
        /// Выделить используемые пути из выражения с условием
        /// </summary>
        /// <param name="condition">Выражение с условием</param>
        /// <param name="library">Библиотека методов</param>
        /// <param name="expressionBuilder">Построитель выражений</param>
        /// <param name="branches">Ветвления пути</param>
        /// <param name="method">Метод, в котором используется условие</param>
        /// <returns>Модель условия</returns>
        [NotNull]
        public static PathCondition Parse(
            [NotNull]LambdaExpression condition,
            [NotNull]IMemberInfoLibrary library,
            [NotNull]IExpressionBuilder expressionBuilder,
            [NotNull]Multidictionary<ParameterExpression, ParsedPath> branches,
            MethodInfo method = null)
        {
            if (condition.Parameters.Count != 1)
            {
                throw new NotSupportedException(string.Format("Unexpected number of parameters in lambda: {0}", condition));
            }
            var parser = new ConditionParserVisitor(library, expressionBuilder, branches);
            var conditionWithNullCheck = MakeBool(parser.Visit(condition.Body));
            var param = condition.Parameters[0];
            if (param == null)
            {
                throw new InvalidPathException("Lambda with null parameter");
            }
            return new PathCondition(
                start: param,
                dbCondition: condition,
                serverCondition: Expression.Lambda(condition.Type, conditionWithNullCheck, condition.Parameters[0]),
                branches: branches,
                conditionMethod: method);
        }

        [NotNull]private readonly IMemberInfoLibrary _library;
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;
        [NotNull] private readonly Multidictionary<ParameterExpression, ParsedPath> _branches;

        /// <summary>
        /// Обходчик выражения-условия, выделяющий используемые пути
        /// </summary>
        /// <param name="library">Библиотека методов</param>
        /// <param name="expressionBuilder">Построитель выражений</param>
        /// <param name="branches">Ветвления пути</param>
        public ConditionParserVisitor(
            [NotNull]IMemberInfoLibrary library,
            [NotNull]IExpressionBuilder expressionBuilder,
            [NotNull]Multidictionary<ParameterExpression, ParsedPath> branches)
        {
            if (library == null) throw new ArgumentNullException("library");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (branches == null) throw new ArgumentNullException("branches");
            _library = library;
            _expressionBuilder = expressionBuilder;
            _branches = branches;
        }
        
        /// <summary>
        /// Посетить узел
        /// </summary>
        [NotNull]
        public override Expression Visit(Expression node)
        {
            var result = base.Visit(node);
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Visitor returned null for {0}", node));
            }
            return result;
        }

        private static bool IsComplexNode([NotNull] Expression node)
        {
            return node.Type == typeof(NBool) || node is ExpressionWithPrecondition;
        }

        [NotNull]
        private static Expression MakeBool([NotNull] Expression node)
        {
            Expression result = node;
            var withPrecondition = node as ExpressionWithPrecondition;
            if (withPrecondition != null)
            {
                result = Expression.AndAlso(withPrecondition.Precondition, withPrecondition.Expression);
            }
            if (result.Type == typeof(NBool))
            {
                result = Expression.Equal(result, NBool.TrueExpr);
            }
            return result;
        }

        [NotNull]
        private static Expression MakeNBool([NotNull]Expression node)
        {
            if (node.Type == typeof(NBool))
            {
                return node;
            }
            var nodeWithPrecondition = node as ExpressionWithPrecondition;
            if (nodeWithPrecondition != null)
            {
                if (nodeWithPrecondition.Type == typeof(bool))
                {
                    return NBool.MakeNBoolExpression(nodeWithPrecondition);
                }
                if (nodeWithPrecondition.Type == typeof(bool?))
                {
                    return Expression.Condition(
                        nodeWithPrecondition.Precondition, 
                        Expression.Convert(nodeWithPrecondition.Expression, typeof(NBool)),
                        NBool.NullExpr);
                }
                throw new InvalidPathException(string.Format("Unexpected type {0} of condition {1}", nodeWithPrecondition.Type, nodeWithPrecondition.Expression));
            }
            return Expression.Convert(node, typeof(NBool));
        }

        [NotNull]
        private Expression ParseElement([NotNull]Expression elementNode)
        {
            return ConditionElementParserVisitor.Parse(elementNode, _branches, _library, _expressionBuilder);
        }

        /// <summary>
        /// Посетить узел обращения к члену
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Перестроенный узел</returns>
        protected override Expression VisitMember(MemberExpression node)
        {
            return ParseElement(node);
        }

        /// <summary>
        /// Посетить узел вызова метода
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Перестроенный узел</returns>
        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            return ParseElement(node);
        }

        /// <summary>
        /// Посетить узел унарной операции
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Перестроенный узел</returns>
        protected override Expression VisitUnary(UnaryExpression node)
        {
            switch (node.NodeType)
            {
                // boolean expression: move along
                case ExpressionType.Not:
                    var operand = Visit(node.Operand);
                    var withPrecondition = operand as ExpressionWithPrecondition;
                    if (withPrecondition != null)
                    {
                        return new ExpressionWithPrecondition(Expression.Not(withPrecondition.Expression), withPrecondition.Precondition);
                    }
                    return Expression.Not(operand);
            }
            throw Unexpected(node);
        }


        /// <summary>
        /// Посетить узел бинарной операции
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Перестроенный узел</returns>
        protected override Expression VisitBinary(BinaryExpression node)
        {
            Expression left;
            Expression right;
            switch (node.NodeType)
            {
                // comparisons: construct PathExpression and rebuild it with ExpressionBuilder
                case ExpressionType.Equal:
                case ExpressionType.LessThanOrEqual:
                case ExpressionType.LessThan:
                case ExpressionType.NotEqual:
                case ExpressionType.GreaterThan:
                case ExpressionType.GreaterThanOrEqual:
                    return ParseElement(node);

                // boolean expression: move along
                case ExpressionType.AndAlso:
                case ExpressionType.And:
                    left = MakeNBool(Visit(node.Left));
                    right = MakeNBool(Visit(node.Right));
                    return Expression.And(left, right);
                case ExpressionType.OrElse:
                case ExpressionType.Or:
                    left = MakeNBool(Visit(node.Left));
                    right = MakeNBool(Visit(node.Right));
                    return Expression.Or(left, right);

                case ExpressionType.ExclusiveOr:
                    left = Visit(node.Left);
                    right = Visit(node.Right);
                    var leftWithPrecondition = left as ExpressionWithPrecondition;
                    var rightWithPrecondition = right as ExpressionWithPrecondition;
                    var leftExpr = GetExpression(left, leftWithPrecondition);
                    var rightExpr = GetExpression(right, rightWithPrecondition);
                    Expression result = Expression.ExclusiveOr(leftExpr, rightExpr);
                    var precondition = MakeXorPrecondition(leftWithPrecondition, rightWithPrecondition);
                    return precondition != null
                        ? new ExpressionWithPrecondition(result, precondition)
                        : result; 

                // coalesce: process paths separately, both should give false on null dereferencing
                case ExpressionType.Coalesce:
                    left = Visit(node.Left);
                    right = Visit(node.Right);
                    if (IsComplexNode(left) || IsComplexNode(right))
                    {
                        return Expression.Convert(
                            Expression.Coalesce(
                                Expression.Convert(MakeNBool(left), typeof(bool?)),
                                Expression.Convert(MakeNBool(right), typeof(bool?))),
                            typeof(NBool));
                    }
                    return Expression.Coalesce(left, right);
            }
            throw Unexpected(node);
        }

        [NotNull]
        private Expression GetExpression([NotNull]Expression expr, [CanBeNull]ExpressionWithPrecondition exprWithPrecondition)
        {
            return exprWithPrecondition != null ? exprWithPrecondition.Expression : expr;
        }

        [CanBeNull]
        private Expression MakeXorPrecondition(ExpressionWithPrecondition leftWithPrecondition, ExpressionWithPrecondition rightWithPrecondition)
        {
            if (leftWithPrecondition == null && rightWithPrecondition == null)
            {
                return null;
            }
            if (leftWithPrecondition == null)
            {
                return rightWithPrecondition.Precondition;
            }
            if (rightWithPrecondition == null)
            {
                return leftWithPrecondition.Precondition;
            }
            return Expression.AndAlso(leftWithPrecondition.Precondition, rightWithPrecondition.Precondition);
        }

        /// <summary>
        /// Посетить узел бинарной операции с типами
        /// </summary>
        /// <param name="node">Узел</param>
        protected override Expression VisitTypeBinary(TypeBinaryExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Посетить узел бинарной операции с типами
        /// </summary>
        /// <param name="node">Узел</param>
        protected override Expression VisitConditional(ConditionalExpression node)
        {
            // boolean expression: move along
            var test = Visit(node.Test);
            var ifTrue = Visit(node.IfTrue);
            var ifFalse = Visit(node.IfFalse);
            var hasPreconditions = IsComplexNode(ifTrue) || IsComplexNode(ifFalse);
            return Expression.Condition(
                MakeBool(test),
                hasPreconditions ? MakeNBool(ifTrue) : MakeBool(ifTrue),
                hasPreconditions ? MakeNBool(ifFalse) : MakeBool(ifFalse));
        }

        /// <summary>
        /// Посетить узел с константой
        /// </summary>
        /// <param name="node">Узел</param>
        protected override Expression VisitConstant(ConstantExpression node)
        {
            if (node.Type != typeof(bool))
            {
                throw UnexpectedType(node);
            }
            // boolean constant: move along
            return base.VisitConstant(node);
        }

        /// <summary>
        /// Посетить узел с параметром
        /// </summary>
        /// <param name="node">Узел</param>
        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (node.Type != typeof(bool))
            {
                throw UnexpectedType(node);
            }
            // boolean parameter: move along
            return base.VisitParameter(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitInvocation(InvocationExpression node)
        {
            throw Unexpected(node);   // EF always throws on Invoke
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitNew(NewExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitNewArray(NewArrayExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitBlock(BlockExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitDebugInfo(DebugInfoExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitDefault(DefaultExpression node)
        {
            // default boolean: just use false
            throw Unexpected(node);
        }

#if NET45
        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitDynamic(DynamicExpression node)
        {
            throw Unexpected(node);
        }
#endif

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitExtension(Expression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitGoto(GotoExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitIndex(IndexExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitLabel(LabelExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitListInit(ListInitExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitLoop(LoopExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitMemberInit(MemberInitExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitSwitch(SwitchExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitRuntimeVariables(RuntimeVariablesExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitTry(TryExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override CatchBlock VisitCatchBlock(CatchBlock node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override ElementInit VisitElementInit(ElementInit node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override LabelTarget VisitLabelTarget(LabelTarget node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override MemberAssignment VisitMemberAssignment(MemberAssignment node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override MemberBinding VisitMemberBinding(MemberBinding node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override MemberListBinding VisitMemberListBinding(MemberListBinding node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override MemberMemberBinding VisitMemberMemberBinding(MemberMemberBinding node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override SwitchCase VisitSwitchCase(SwitchCase node)
        {
            throw Unexpected(node);
        }

        [NotNull]
        private Exception Unexpected(Expression node)
        {
            if (node == null)
            {
                return new InvalidPathException("Unexpected null node");
            }
            return new InvalidPathException(string.Format("Unexpected node {0} in boolean condition : {1}", node.NodeType, node));
        }

        [NotNull]
        private Exception Unexpected(object node)
        {
            if (node == null)
            {
                return new InvalidPathException("Unexpected null node");
            }
            return new InvalidPathException(string.Format("Unexpected node with type {0} in boolean condition : {1}", node.GetType(), node));
        }

        [NotNull]
        private Exception UnexpectedType(Expression node)
        {
            if (node == null)
            {
                return new InvalidPathException("Unexpected null node");
            }
            return new InvalidPathException(string.Format("Node {0} with unexpected type {1} in boolean condition : {2}", node.NodeType, node.Type, node));
        }
    }
}
// ReSharper restore RedundantOverridenMember
