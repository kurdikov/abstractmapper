﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Заменяет тип параметра лямбды
    /// </summary>
    /// <typeparam name="TParameterInterface">Исходный тип параметра</typeparam>
    public class ParameterTypeReplacerVisitor<TParameterInterface> : ExpressionVisitor
    {
        [NotNull]private readonly ParameterExpression _parameter;

        /// <summary>
        /// Создать заменитель типа параметра лямбды
        /// </summary>
        /// <param name="parameter">Параметр-заменитель</param>
        public ParameterTypeReplacerVisitor([NotNull]ParameterExpression parameter)
        {
            if (parameter == null) throw new ArgumentNullException("parameter");
            _parameter = parameter;
        }

        /// <summary>
        /// Посетить узел обращения к члену
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Преобразованный узел</returns>
        protected override Expression VisitMember(MemberExpression node)
        {
            return (node.Member.DeclaringType == typeof(TParameterInterface) && node.Member.MemberType == MemberTypes.Property)
                ? Expression.Property(Visit(node.Expression), node.Member.Name)
                : base.VisitMember(node);
        }

        /// <summary>
        /// Посетить узел параметра
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Преобразованный узел</returns>
        protected override Expression VisitParameter(ParameterExpression node)
        {
            return (node.Type == typeof(TParameterInterface))
               ? _parameter
               : base.VisitParameter(node);
        }
    }
}
