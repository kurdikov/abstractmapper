﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Обходчик выражений для обрезания инклюдов
    /// </summary>
    public sealed class IncludeExpressionCutterVisitor : ExpressionVisitor
    {
        private ValueHoldingMember _externalMember;
        [NotNull]private readonly Func<ValueHoldingMember, bool> _stopOn;

        /// <summary>
        /// Обходчик выражений для обрезания инклюдов
        /// </summary>
        /// <param name="stopOn">Предикат необходимости обрезать инклюд</param>
        public IncludeExpressionCutterVisitor([NotNull]Func<ValueHoldingMember, bool> stopOn)
        {
            if (stopOn == null) throw new ArgumentNullException("stopOn");
            _stopOn = stopOn;
        }

        /// <summary>
        /// Получить свойство внешней сущности
        /// </summary>
        public ValueHoldingMember GetExternalMember()
        {
            return _externalMember;
        }

        /// <summary>
        /// Получить тип внешней сущности, на свойстве с которой был обрезан инклюд
        /// </summary>
        /// <returns>Тип внешней сущности</returns>
        public Type GetExternalEntityType()
        {
            return _externalMember != null ? _externalMember.ValueType : null;
        }

        /// <summary>
        /// Получить тип сущности, на свойстве которой был обрезан инклюд
        /// </summary>
        /// <returns>Тип-контейнер с внешней сущностью</returns>
        public Type GetDeclaringEntityType()
        {
            return _externalMember != null ? _externalMember.DeclaringType : null;
        }

        /// <summary>
        /// Посетить узел с обращением к члену
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Преобразованный узел</returns>
        protected override Expression VisitMember(MemberExpression node)
        {
            var member = ValueHoldingMember.GetFromExpression(node);
            if (node.Expression == null)
            {
                throw new InvalidOperationException(string.Format("Include expression cannot contain a member expression with null node: {0}", node));
            }
            var result = Visit(node.Expression);
            if (_stopOn(member))
            {
                _externalMember = member;
            }
            else
            {
                result = node;
            }
            return result;
        }

        [NotNull]
        private MethodInfo GetSelectObject([NotNull]MethodInfo select, [NotNull]Type type)
        {
            return select.GetGenericMethodDefinition()
                         .MakeGenericMethod(select.GetGenericArguments()[0], type);
        }

        /// <summary>
        /// Посетить узел с вызовом метода
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Преобразованный узел</returns>
        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            var method = node.Method;
            if (method.DeclaringType != typeof(Enumerable) || method.Name != "Select" || node.Arguments.Count != 2)
            {
                throw new ArgumentException("Include expression may not contain invocations of methods other than Enumerable.Select");
            }
            Expression result;
            var enumerable = Visit(node.Arguments[0]);
            if (enumerable == null)
            {
                throw new InvalidOperationException("ExpressionVisitor.Visit returned null");
            }
            if (_externalMember == null)
            {
                var lambda = Visit(node.Arguments[1]) as LambdaExpression;
                if (lambda == null || lambda.Body.NodeType == ExpressionType.Parameter)
                {
                    result = enumerable;
                }
                else
                {
                    result = Expression.Call(GetSelectObject(method, lambda.Body.Type), enumerable, lambda);
                }
            }
            else
            {
                result = enumerable;
            }
            return result;
        }

        /// <summary>
        /// Посетить узел-лямбду
        /// </summary>
        /// <typeparam name="TDelegate">Тип делегата лямбды</typeparam>
        /// <param name="node">Узел</param>
        /// <returns>Преобразованный узел</returns>
        protected override Expression VisitLambda<TDelegate>(Expression<TDelegate> node)
        {
            var body = Visit(node.Body);
            return Expression.Lambda(typeof (Func<,>).MakeGenericType(node.ParameterAt(0).Type, body.Type),
                                     body,
                                     node.Parameters);
        }

        /// <summary>
        /// Посетить узел
        /// </summary>
        [ContractAnnotation("null => null; notnull => notnull")]
        public override Expression Visit(Expression node)
        {
            return node != null ? base.Visit(node) : null;
        }
    }
}
