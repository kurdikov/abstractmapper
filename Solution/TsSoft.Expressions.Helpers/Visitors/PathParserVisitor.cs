﻿// ReSharper disable RedundantOverridenMember
namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Разбирает выражения пути:
    /// 1) константа - пустой путь
    /// 2) выражения вида p.A.B.Select(b => b.C).SelectMany(c => c.D).Where(d => P(d))
    /// </summary>
    public class PathParserVisitor : ExpressionVisitor
    {
        /// <summary>
        /// Извлекает путь из выражения
        /// </summary>
        [NotNull]
        public static ParsedPath Parse(
            Expression expression,
            [NotNull]IMemberInfoLibrary library, 
            [NotNull]IExpressionBuilder expressionBuilder)
        {
            Expression converted;
            var result = ParseAndConvert(expression, library, expressionBuilder, out converted);
            return result;
        }

        /// <summary>
        /// Извлекает путь из выражения, добавляя Where(x => x != null) перед каждым вызовом Select и SelectMany
        /// </summary>
        [NotNull]
        public static ParsedPath ParseAndConvert(
            Expression expression,
            [NotNull]IMemberInfoLibrary library,
            [NotNull]IExpressionBuilder expressionBuilder,
            out Expression convertedExpression)
        {
            var lambda = expression as LambdaExpression;
            var parser = new PathParserVisitor(library, expressionBuilder);
            if (lambda != null)
            {
                if (lambda.Parameters.Count != 1)
                {
                    throw new InvalidOperationException(string.Format("Unexpected number of lambda parameters in path expression: {0}", lambda));
                }
                parser.SetStartParameter(lambda.Parameters[0]);
                convertedExpression = Expression.Lambda(lambda.Type, parser.Visit(lambda.Body), lambda.Parameters[0]);
            }
            else
            {
                convertedExpression = parser.Visit(expression);
            }
            return parser.GetPath(expression);
        }

        /// <summary>
        /// Извлекает путь из выражения, если он там есть, добавляя Where(x => x != null) перед каждым вызовом Select и SelectMany
        /// </summary>
        [CanBeNull]
        public static ParsedPath TryParseAndConvert(
            Expression expression,
            [NotNull]IMemberInfoLibrary library,
            [NotNull]IExpressionBuilder expressionBuilder,
            [NotNull]Multidictionary<ParameterExpression, ParsedPath> branches,
            [NotNull]out Expression convertedExpression)
        {
            var parser = new PathParserVisitor(library, expressionBuilder, branches);
            var visitedExpression = parser.Visit(expression);
            convertedExpression = visitedExpression;
            if (!parser._path.Any())
            {
                return null;
            }
            return parser.GetPath(expression);
        }

        [NotNull]private readonly IMemberInfoLibrary _library;
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;

        [NotNull]private readonly List<PathElement> _path;
        [NotNull]private readonly Multidictionary<ParameterExpression, ParsedPath> _branches;
        private ParameterExpression _startParameter;
        private bool _afterSelect;

        /// <summary>
        /// Разбирает выражения пути:
        /// 1) константа - пустой путь
        /// 2) выражения вида p.A.B.Select(b => b.C).SelectMany(c => c.D).Where(d => P(d))
        /// </summary>
        public PathParserVisitor(
            [NotNull]IMemberInfoLibrary library, 
            [NotNull]IExpressionBuilder expressionBuilder)
            : this(library, expressionBuilder, new Multidictionary<ParameterExpression, ParsedPath>())
        {
        }

        /// <summary>
        /// Разбирает выражения пути:
        /// 1) константа - пустой путь
        /// 2) выражения вида p.A.B.Select(b => b.C).SelectMany(c => c.D).Where(d => P(d))
        /// </summary>
        public PathParserVisitor(
            [NotNull] IMemberInfoLibrary library,
            [NotNull] IExpressionBuilder expressionBuilder,
            [NotNull] Multidictionary<ParameterExpression, ParsedPath> branches)
        {
            if (library == null) throw new ArgumentNullException("library");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (branches == null) throw new ArgumentNullException("branches");
            _library = library;
            _expressionBuilder = expressionBuilder;

            _path = new List<PathElement>();
            _afterSelect = false;
            _branches = branches;
        }

        [NotNull]
        private ParsedPath GetPath(Expression expression)
        {
            if (_startParameter == null)
            {
                throw new InvalidPathException(string.Format("Cannot find a parameter starting the path: {0}", expression));
            }
            var result = new ParsedPath(
                elements: _path,
                start: _startParameter,
                branches: _branches);
            return result;
        }

        /// <summary>
        /// Посетить узел
        /// </summary>
        [NotNull]
        public override Expression Visit(Expression node)
        {
            var result = base.Visit(node);
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Visitor returned null on {0}", node));
            }
            return result;
        }

        /// <summary>
        /// Установить начальный параметр
        /// </summary>
        public void SetStartParameter(ParameterExpression param)
        {
            _startParameter = param;
        }

        [NotNull]
        private PathElement LastElement(string errorMessageOnZeroLength)
        {
            if (_path.Count == 0)
            {
                throw new InvalidPathException(errorMessageOnZeroLength);
            }
            var result = _path[_path.Count - 1];
            if (result == null)
            {
                throw new InvalidPathException("Null element in path");
            }
            return result;
        }

        /// <summary>
        /// Посетить узел с вызовом метода, обработать часть пути внутри Select и SelectMany, условие внутри Where
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Узел с перестроенным условием и проверкой на null</returns>
        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            MethodCallExpression result;
            var method = node.Method;
            if (_library.IsEnumerableSelect(method) ||
                _library.IsEnumerableSelectMany(method))
            {
                var source = Visit(node.Arguments[0]);
                _afterSelect = true;
                var lambda = node.LambdaArgument(1);
                LastElement("Select method without path").SetParameter(lambda.Parameters[0]);
                var lambdaBody = Visit(lambda.Body);
                result = Expression.Call(method, _expressionBuilder.WhereNotNull(source),
                    Expression.Lambda(lambda.Type, lambdaBody, lambda.Parameters[0]));
            }
            else if (_library.IsEnumerableWhere(method)
                || _library.IsEnumerableFirstOrDefaultWithLambda(method)
                || _library.IsEnumerableFirstWithLambda(method)
                || _library.IsEnumerableSingleOrDefaultWithLambda(method)
                || _library.IsEnumerableSingleWithLambda(method)
                || _library.IsEnumerableLastWithLambda(method)
                || _library.IsEnumerableLastOrDefaultWithLambda(method)
                || _library.IsEnumerableFirstOrDefault(method)
                || _library.IsEnumerableFirst(method)
                || _library.IsEnumerableSingleOrDefault(method)
                || _library.IsEnumerableSingle(method)
                || _library.IsEnumerableLast(method)
                || _library.IsEnumerableLastOrDefault(method))
            {
                result = ParseAndTransformCondition(node, method);
            }
            else
            {
                throw new NotSupportedException(string.Format("Method {0} is not supported in path expressions",
                    method));
            }
            return result;
        }

        private MethodCallExpression ParseAndTransformCondition([NotNull]MethodCallExpression node, [NotNull]MethodInfo method)
        {
            PathCondition currentCondition;
            Expression[] update;
            if (node.Arguments.Count == 2)
            {
                var lambdaArgument = node.LambdaArgument(1);
                currentCondition = ConditionParserVisitor.Parse(
                    lambdaArgument, _library, _expressionBuilder, _branches, method);
                update = new Expression[] { null, currentCondition.ServerCondition };
            }
            else if (node.Arguments.Count == 1)
            {
                currentCondition = new PathCondition(
                    conditionMethod: method,
                    dbCondition: null,
                    serverCondition: null,
                    start: null,
                    branches: _branches);
                update = new Expression[] { null };
            }
            else
            {
                throw new NotSupportedException(string.Format("Method {0} with {1} parameters is not supported in path expressions", method, node.Arguments.Count));
            }
            var source = Visit(node.Arguments[0]);
            var element = LastElement("Condition without path");
            element.AddCondition(currentCondition);
            update[0] = source;
            var result = node.Update(null, update);
            return result;
        }

        /// <summary>
        /// Посетить узел с обращением к члену, обработать часть пути для членов-свойств
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Тот же узел</returns>
        protected override Expression VisitMember(MemberExpression node)
        {
            var member = ValueHoldingMember.GetFromExpression(node);
            Visit(node.Expression);
            if (_startParameter != null)
            {
                _path.Add(new PathElement(member));
            }
            return node;
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitBinary(BinaryExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitBlock(BlockExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitConditional(ConditionalExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitConstant(ConstantExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitDefault(DefaultExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitDebugInfo(DebugInfoExpression node)
        {
            throw Unexpected(node);
        }

#if NET45
        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitDynamic(DynamicExpression node)
        {
            throw Unexpected(node);
        }
#endif

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitExtension(Expression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitGoto(GotoExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitIndex(IndexExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitInvocation(InvocationExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitLabel(LabelExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Посетить узел с лямбда-выражением, обработать часть пути
        /// </summary>
        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitListInit(ListInitExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitLoop(LoopExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitMemberInit(MemberInitExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitNew(NewExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitNewArray(NewArrayExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (!_afterSelect && !_path.Any())
            {
                _startParameter = node;
            }
            return node;
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitRuntimeVariables(RuntimeVariablesExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitSwitch(SwitchExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitTry(TryExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override Expression VisitTypeBinary(TypeBinaryExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле или пропустить Convert
        /// </summary>
        protected override Expression VisitUnary(UnaryExpression node)
        {
            if (node.NodeType == ExpressionType.Convert)
            {
                if (ConversionChecker.Instance.IsImplicitlyConvertible(node.Operand.Type, node.Type))
                {
                    return base.VisitUnary(node);
                }
            }
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override CatchBlock VisitCatchBlock(CatchBlock node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override ElementInit VisitElementInit(ElementInit node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override LabelTarget VisitLabelTarget(LabelTarget node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override MemberAssignment VisitMemberAssignment(MemberAssignment node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override MemberBinding VisitMemberBinding(MemberBinding node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override MemberListBinding VisitMemberListBinding(MemberListBinding node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override MemberMemberBinding VisitMemberMemberBinding(MemberMemberBinding node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в пути узле
        /// </summary>
        protected override SwitchCase VisitSwitchCase(SwitchCase node)
        {
            throw Unexpected(node);
        }

        [NotNull]
        private Exception Unexpected(Expression node)
        {
            throw new NotSupportedException(string.Format("Unexpected node with type {0} in path expression : {1}", node != null ? node.NodeType : 0, node));
        }

        [NotNull]
        private Exception Unexpected(object node)
        {
            throw new NotSupportedException(string.Format("Unexpected node with type {0} in path expression : {1}", node != null ? node.GetType() : null, node));
        }
    }
}
// ReSharper restore RedundantOverridenMember
