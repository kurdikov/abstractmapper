﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Обходчик выражений, преобразующий коллекцию объектов согласно линейному пути
    /// </summary>
    /// <typeparam name="T">Тип входа пути</typeparam>
    public class IncludeExpressionEntitiesSelectorVisitor<T> : ExpressionVisitor where T: class
    {
        [NotNull] private Type _currentType;
        [NotNull]private IEnumerable<object> _currentState;
        private ValueHoldingMember _externalProperty;
        [NotNull]private readonly Func<ValueHoldingMember, bool> _stopOn;

        /// <summary>
        /// Обходчик выражений, преобразующий коллекцию объектов согласно линейному пути
        /// </summary>
        /// <param name="entityCollection">Входные объекты</param>
        /// <param name="stopOn">Предикат остановки</param>
        public IncludeExpressionEntitiesSelectorVisitor([NotNull]IEnumerable<T> entityCollection, [NotNull]Func<ValueHoldingMember, bool> stopOn)
        {
            if (entityCollection == null) throw new ArgumentNullException("entityCollection");
            if (stopOn == null) throw new ArgumentNullException("stopOn");
            _currentState = entityCollection;
            _stopOn = stopOn;
            _currentType = typeof(T);
        }

        /// <summary>
        /// Получить текущее состояние обходчика
        /// </summary>
        /// <returns>Коллекция объектов, обозреваемая обходчиком</returns>
        [NotNull]
        public IEnumerable<object> GetCurrentState()
        {
            return _currentState;
        }

        /// <summary>
        /// Получить тип элементов текущего состояния обходчика
        /// </summary>
        [NotNull]
        public Type GetCurrentType()
        {
            return _currentType;
        }

        /// <summary>
        /// Получить свойство, на котором произошла остановка
        /// </summary>
        /// <returns>Свойство, на котором произошла остановка</returns>
        public ValueHoldingMember GetExternalProperty()
        {
            return _externalProperty;
        }

        private void ApplyProperty([NotNull]ValueHoldingMember property)
        {
            if (_externalProperty != null)
            {
                return;
            }
            if (!_stopOn(property))
            {
                _currentState = !typeof(IEnumerable<object>).GetTypeInfo().IsAssignableFrom(property.ValueType) 
                    ? _currentState.Where(elem => elem != null).Select(property.GetValue) 
                    : _currentState.Where(elem => elem != null).SelectMany(elem => property.GetValue(elem) as IEnumerable<object>);
                _currentType = property.ValueType.GetGenericEnumerableArgumentOrSelf();
            }
            else
            {
                _externalProperty = property;
            }
        }

        /// <summary>
        /// Посетить узел обращения к члену
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Значение не определено</returns>
        protected override Expression VisitMember(MemberExpression node)
        {
            Visit(node.Expression);
            var member = ValueHoldingMember.GetFromExpression(node);
            ApplyProperty(member);
            return node;
        }

        /// <summary>
        /// Посетить узел вызова метода
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Значение не определено</returns>
        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            var method = node.Method;
            if (method.DeclaringType != typeof (Enumerable) || method.Name != "Select" || node.Arguments.Count != 2)
            {
                throw new ArgumentException("Include expression may not contain invocations of methods other than Enumerable.Select");
            }
            Visit(node.Arguments[0]);
            Visit(node.Arguments[1]);
            return Expression.Default(node.Method.ReturnType);
        }
    }
}
