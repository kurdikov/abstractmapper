﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System.Linq.Expressions;
    using System.Reflection;

    /// <summary>
    /// Обходчик выражения, удаляющий приведения к интерфейсам
    /// </summary>
    public sealed class EntityCastInterfaceRemoverVisitor : ExpressionVisitor
    {
        /// <summary>
        /// Удалить приведения к интерфейсам
        /// </summary>
        /// <typeparam name="TExpression">Тип выражения</typeparam>
        /// <param name="predicate">Выражение</param>
        /// <returns>Преобразованное выражение</returns>
        public static TExpression Convert<TExpression>(
            TExpression predicate) where TExpression : LambdaExpression
        {
            var visitor = new EntityCastInterfaceRemoverVisitor();

            var visitedExpression = visitor.Visit(predicate);

            return (TExpression) visitedExpression;
        }

        /// <summary>
        /// Посетить узел с унарной операцией
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Изменённый узел</returns>
        protected override Expression VisitUnary(UnaryExpression node)
        {
            if (node.NodeType == ExpressionType.Convert && node.Type.GetTypeInfo().IsInterface)
            {
                return node.Operand;
            }

            return base.VisitUnary(node);
        }
    }
}
