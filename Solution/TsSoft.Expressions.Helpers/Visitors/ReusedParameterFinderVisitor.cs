﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Находит параметры, встречающиеся в теле лямбды более одного раза
    /// </summary>
    public class ReusedParameterFinderVisitor : ExpressionVisitor
    {
        /// <summary>
        /// Найти параметры, встречающиеся в теле лямбды более одного раза
        /// </summary>
        /// <param name="expression">Лямбда</param>
        [NotNull]
        public static ISet<ParameterExpression> FindReusedParameters([NotNull]LambdaExpression expression)
        {
            var visitor = new ReusedParameterFinderVisitor();
            visitor.Visit(expression.Body);
            return visitor._reusedParameters;
        }

        /// <summary>
        /// Найти параметры, встречающиеся в теле лямбды более одного раза
        /// </summary>
        /// <param name="expression">Лямбда</param>
        [NotNull]
        public static ISet<ParameterExpression> FindReusedParameters<T>([NotNull]Expression<T> expression)
        {
            return FindReusedParameters((LambdaExpression) expression);
        }

        [NotNull]private readonly HashSet<ParameterExpression> _visitedParameters = new HashSet<ParameterExpression>();
        [NotNull]private readonly HashSet<ParameterExpression> _reusedParameters = new HashSet<ParameterExpression>();

        private ReusedParameterFinderVisitor()
        {
        }

        /// <summary>
        /// Обработать параметр
        /// </summary>
        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (!_visitedParameters.Add(node))
            {
                _reusedParameters.Add(node);
            }
            return base.VisitParameter(node);
        }
    }
}
