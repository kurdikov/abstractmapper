﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Находит в лямбде несобственные параметры
    /// </summary>
    public class OpenLambdaParameterFinderVisitor : ExpressionVisitor
    {
        /// <summary>
        /// Получить параметры, встерчающиеся в выражении и не являющиеся внутренними параметрами лямбд
        /// </summary>
        [NotNull]
        public static ISet<ParameterExpression> GetParameters(Expression expr)
        {
            var visitor = new OpenLambdaParameterFinderVisitor();
            visitor.Visit(expr);
            return visitor._openParameters;
        }

        /// <summary>
        /// Получить несобственные параметры, встречающиеся в лямбде
        /// </summary>
        [NotNull]
        public static ISet<ParameterExpression> GetOpenParameters(LambdaExpression lambda)
        {
            var visitor = new OpenLambdaParameterFinderVisitor();
            visitor.Visit(lambda);
            return visitor._openParameters;
        }

        /// <summary>
        /// Получить несобственные параметры, встречающиеся в лямбде
        /// </summary>
        [NotNull]
        public static ISet<ParameterExpression> GetOpenParameters<T>(Expression<T> lambda)
        {
            return GetOpenParameters((LambdaExpression)lambda);
        }

        private OpenLambdaParameterFinderVisitor()
        {
        }

        [NotNull]private readonly HashSet<ParameterExpression> _openParameters = new HashSet<ParameterExpression>();
        [NotNull]private HashSet<ParameterExpression> _closedParameters = new HashSet<ParameterExpression>();

        /// <summary>
        /// Посетить узел
        /// </summary>
        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            var outerClosedParameters = _closedParameters;
            _closedParameters = new HashSet<ParameterExpression>(outerClosedParameters);
            _closedParameters.UnionWith(node.Parameters);
            Visit(node.Body);
            _closedParameters = outerClosedParameters;
            return node;
        }

        /// <summary>
        /// Посетить узел
        /// </summary>
        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (!_closedParameters.Contains(node))
            {
                _openParameters.Add(node);
            }
            return node;
        }

        /// <summary>
        /// Посетить узел
        /// </summary>
        protected override Expression VisitBlock(BlockExpression node)
        {
            if (node.Variables == null)
            {
                return base.VisitBlock(node);
            }

            var outerClosedParameters = _closedParameters;
            _closedParameters = new HashSet<ParameterExpression>(outerClosedParameters);
            _closedParameters.UnionWith(node.Variables);
            base.VisitBlock(node);
            _closedParameters = outerClosedParameters;
            return node;
        }
    }
}
