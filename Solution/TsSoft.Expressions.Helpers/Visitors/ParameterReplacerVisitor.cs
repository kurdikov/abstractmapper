﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Обходчик выражений, заменяющий параметры лямбды
    /// </summary>
    public class ParameterReplacerVisitor : ExpressionVisitor
    {
        [NotNull]private readonly IDictionary<ParameterExpression, Expression> _replacements;

        /// <summary>
        /// Создать обходчик выражений, заменяющий параметр лямбды
        /// </summary>
        /// <param name="inner">На что заменять параметр</param>
        /// <param name="parameter">Заменяемый параметр</param>
        private ParameterReplacerVisitor([NotNull]Expression inner, [NotNull]ParameterExpression parameter)
        {
            _replacements = new Dictionary<ParameterExpression, Expression>
            {
                {parameter, inner},
            };
        }

        /// <summary>
        /// Создать обходчик выражений, заменяющий параметры лямбды
        /// </summary>
        /// <param name="replacements">Параметры и на что их заменять</param>
        private ParameterReplacerVisitor([NotNull]IEnumerable<KeyValuePair<ParameterExpression, Expression>> replacements)
        {
            if (replacements == null)
            {
                throw new ArgumentNullException("replacements");
            }
            _replacements = new Dictionary<ParameterExpression, Expression>();
            foreach (var replacement in replacements)
            {
                _replacements.Add(replacement);
            }
        }

        /// <summary>
        /// Заменить вхождения параметра в выражение
        /// </summary>
        /// <param name="source">Исходное выражение</param>
        /// <param name="parameter">Заменяемый параметр</param>
        /// <param name="replacement">Заменитель</param>
        /// <returns>Изменённое выражение</returns>
        [NotNull]
        public static Expression Replace([NotNull]Expression source, [NotNull]ParameterExpression parameter, [NotNull]Expression replacement)
        {
            var result = new ParameterReplacerVisitor(replacement, parameter).Visit(source);
            if (result == null)
            {
                throw new InvalidOperationException("ParameterReplacerVisitor.Visit returned null");
            }
            return result;
        }

        /// <summary>
        /// Заменить вхождения параметра в тело лямбды
        /// </summary>
        /// <param name="source">Исходное выражение</param>
        /// <param name="parameter">Заменяемый параметр</param>
        /// <param name="replacement">Заменитель</param>
        /// <returns>Изменённое выражение</returns>
        [NotNull]
        public static LambdaExpression ReplaceInBody(
            [NotNull] LambdaExpression source,
            [NotNull] ParameterExpression parameter,
            [NotNull] Expression replacement)
        {
            var result = new ParameterReplacerVisitor(replacement, parameter).Visit(source.Body);
            return Expression.Lambda(source.Type, result, source.Parameters);
        }

        /// <summary>
        /// Заменить вхождения параметра в тело лямбды
        /// </summary>
        /// <param name="source">Исходное выражение</param>
        /// <param name="parameter">Заменяемый параметр</param>
        /// <param name="replacement">Заменитель</param>
        /// <returns>Изменённое выражение</returns>
        [NotNull]
        public static Expression<T> ReplaceInBody<T>(
            [NotNull] Expression<T> source,
            [NotNull] ParameterExpression parameter,
            [NotNull] Expression replacement)
        {
            var result = new ParameterReplacerVisitor(replacement, parameter).Visit(source.Body);
            return Expression.Lambda<T>(result, source.Parameters);
        }

        /// <summary>
        /// Заменить вхождения параметра в выражение
        /// </summary>
        /// <param name="source">Исходное выражение</param>
        /// <param name="replacements">Какие параметры заменять, на что</param>
        /// <returns>Изменённое выражение</returns>
        [NotNull]
        public static Expression Replace([NotNull]Expression source, [NotNull]IEnumerable<KeyValuePair<ParameterExpression, Expression>> replacements)
        {
            if (replacements == null)
            {
                throw new ArgumentNullException("replacements");
            }
            var result = new ParameterReplacerVisitor(replacements).Visit(source);
            return result;
        }

        /// <summary>
        /// Заменить вхождения параметра в тело лямбды
        /// </summary>
        /// <param name="source">Исходное выражение</param>
        /// <param name="replacements">Какие параметры заменять, на что</param>
        /// <returns>Изменённое выражение</returns>
        [NotNull]
        public static Expression ReplaceInBody(
            [NotNull] LambdaExpression source,
            [NotNull]IEnumerable<KeyValuePair<ParameterExpression, Expression>> replacements)
        {
            var result = new ParameterReplacerVisitor(replacements).Visit(source.Body);
            return Expression.Lambda(source.Type, result, source.Parameters);
        }

        /// <summary>
        /// Заменить вхождения параметра в тело лямбды
        /// </summary>
        /// <param name="source">Исходное выражение</param>
        /// <param name="replacements">Какие параметры заменять, на что</param>
        /// <returns>Изменённое выражение</returns>
        [NotNull]
        public static Expression<T> ReplaceInBody<T>(
            [NotNull] Expression<T> source,
            [NotNull]IEnumerable<KeyValuePair<ParameterExpression, Expression>> replacements)
        {
            var result = new ParameterReplacerVisitor(replacements).Visit(source.Body);
            return Expression.Lambda<T>(result, source.Parameters);
        }

        /// <summary>
        /// Посетить узел-параметр
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Заменённый узел</returns>
        protected override Expression VisitParameter(ParameterExpression node)
        {
            Expression replacement;
            return _replacements.TryGetValue(node, out replacement) && replacement != null
                       ? replacement
                       : node;
        }

        /// <summary>
        /// Посетить узел-лямбду
        /// </summary>
        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            var body = _replacements.Keys.Intersect(node.Parameters).Any()
                ? Replace(node.Body, _replacements.Where(r => r.Key != null && !node.Parameters.Contains(r.Key)))
                : Visit(node.Body);
            return node.Update(body, node.Parameters);
        }

        /// <summary>
        /// Посетить узел-блок
        /// </summary>
        protected override Expression VisitBlock(BlockExpression node)
        {
            IEnumerable<Expression> expressions;
            if (node.Expressions == null)
            {
                expressions = new[] {Expression.Empty()};
            }
            else if (node.Variables != null && _replacements.Keys.Intersect(node.Variables).Any())
            {
                var innerVisitor = new ParameterReplacerVisitor(_replacements.Where(r => r.Key != null && !node.Variables.Contains(r.Key)));
                expressions = node.Expressions.Select(innerVisitor.Visit);
            }
            else
            {
                expressions = node.Expressions.Select(Visit);
            }
            return node.Update(node.Variables, expressions);
        }

        /// <summary>
        /// Посетить узел
        /// </summary>
        [ContractAnnotation("null => null; notnull => notnull")]
        public override Expression Visit(Expression node)
        {
            return node != null ? base.Visit(node) : null;
        }
    }
}
