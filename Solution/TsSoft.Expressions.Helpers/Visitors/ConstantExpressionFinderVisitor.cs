﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Ищет в выражении константы заданных типов
    /// </summary>
    public class ConstantExpressionFinderVisitor : ExpressionVisitor
    {
        /// <summary>
        /// Получить все константы ссылочных типов, содержащиеся в выражении
        /// </summary>
        /// <param name="expression">Выражение</param>
        /// <param name="predicate">Константы каких типов выбирать (по умолчанию - ссылочные типы, отличные от <see cref="System.String"/> и <see cref="System.Type"/></param>
        [NotNull]
        public static ISet<object> GetConstants([NotNull]Expression expression, Func<Type, bool> predicate = null)
        {
            var visitor = new ConstantExpressionFinderVisitor(predicate);
            visitor.Visit(expression);
            return visitor._constants;
        }

        /// <summary>
        /// Получить все константы ссылочных типов, содержащиеся в выражении
        /// </summary>
        /// <param name="expression">Выражение</param>
        /// <param name="predicate">Константы каких типов выбирать (по умолчанию - ссылочные типы, отличные от <see cref="System.String"/> и <see cref="System.Type"/></param>
        [NotNull]
        public static ISet<object> GetConstants<T>([NotNull]Expression<T> expression, Func<Type, bool> predicate = null)
        {
            return GetConstants((Expression)expression, predicate);
        }

        [NotNull]private readonly Func<Type, bool> _predicate;
        [NotNull]private readonly HashSet<object> _constants = new HashSet<object>();

        private ConstantExpressionFinderVisitor(Func<Type, bool> predicate)
        {
            _predicate = predicate ?? IsReferenceDifferentFromStringAndType;
        }

        private static bool IsReferenceDifferentFromStringAndType(Type type)
        {
            return type.IsReferenceNotStringType() && !typeof(Type).GetTypeInfo().IsAssignableFrom(type);
        }

        /// <summary>
        /// Обработать узел с константой
        /// </summary>
        protected override Expression VisitConstant(ConstantExpression node)
        {
            if (_predicate(node.Type) && node.Value != null)
            {
                _constants.Add(node.Value);
            }
            return node;
        }
    }

}
