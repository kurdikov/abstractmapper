﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Заменяет константы в выражении
    /// </summary>
    public class ConstantExpressionReplacerVisitor : ExpressionVisitor
    {
        /// <summary>
        /// Заменить константы в выражении
        /// </summary>
        /// <param name="expression">Выражение</param>
        /// <param name="replacements">Замены для констант</param>
        [NotNull]
        public static Expression ReplaceConstants([NotNull] Expression expression, [NotNull]IReadOnlyDictionary<object, Expression> replacements)
        {
            var visitor = new ConstantExpressionReplacerVisitor(replacements);
            return visitor.Visit(expression);
        }

        /// <summary>
        /// Заменить константы в выражении
        /// </summary>
        /// <param name="expression">Выражение</param>
        /// <param name="replacements">Замены для констант</param>
        [NotNull]
        public static Expression<T> ReplaceConstants<T>(
            [NotNull] Expression<T> expression,
            [NotNull] IReadOnlyDictionary<object, Expression> replacements)
        {
            return (Expression<T>)ReplaceConstants((Expression)expression, replacements);
        }

        [NotNull]private readonly IReadOnlyDictionary<object, Expression> _replacements;

        private ConstantExpressionReplacerVisitor([NotNull] IReadOnlyDictionary<object, Expression> replacements)
        {
            if (replacements == null) throw new ArgumentNullException("replacements");
            _replacements = replacements;
        }

        /// <summary>
        /// Посетить узел
        /// </summary>
        [ContractAnnotation("null => null; notnull => notnull")]
        public override Expression Visit(Expression node)
        {
            return node != null ? base.Visit(node) : null;
        }

        /// <summary>
        /// Обработать узел с константой
        /// </summary>
        protected override Expression VisitConstant(ConstantExpression node)
        {
            Expression replacement;
            if (node.Value != null && _replacements.TryGetValue(node.Value, out replacement) && replacement != null)
            {
                return replacement;
            }
            return node;
        }
    }

}
