﻿// ReSharper disable RedundantOverridenMember
namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.DbFunctions;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    class ConditionElementParserVisitor : ExpressionVisitor
    {
        [NotNull]
        public static Expression Parse(
            [NotNull]Expression node,
            [NotNull]Multidictionary<ParameterExpression, ParsedPath> branches,
            [NotNull]IMemberInfoLibrary library,
            [NotNull]IExpressionBuilder expressionBuilder)
        {
            var visitor = new ConditionElementParserVisitor(library, expressionBuilder, branches);
            var result = visitor.Visit(node);
            if (IsNullEqualityComparison(node, library))
            {
                return AnyPrefixIsNullOrExpression(visitor._checkedPaths.Select(p => p.Key), result, expressionBuilder);
            }
            Expression precondition = null;
            foreach (var path in visitor._checkedPaths)
            {
                if (path.Key == null)
                {
                    continue;
                }
                var currentPrecondition = expressionBuilder.BuildPreconditionExpression(path.Key, path.Value);
                if (currentPrecondition != null)
                {
                    precondition = precondition != null
                        ? Expression.AndAlso(precondition, currentPrecondition)
                        : currentPrecondition;
                }
            }
            return precondition != null
                ? new ExpressionWithPrecondition(result, precondition)
                : result;
        }

        [NotNull]
        private static Expression AnyPrefixIsNullOrExpression(
            [NotNull]IEnumerable<ParsedPath> paths,
            [NotNull]Expression expression,
            [NotNull]IExpressionBuilder expressionBuilder)
        {
            Expression result = null;
            foreach (var path in paths)
            {
                if (path == null)
                {
                    continue;
                }
                var current = expressionBuilder.BuildPathLeadsToNullExpression(path, false);
                if (current != null)
                {
                    result = result == null ? current : Expression.OrElse(result, current);
                }
            }
            return result == null ? expression : Expression.OrElse(result, expression);
        }

        private static bool IsNullEqualityComparison(Expression node, [NotNull]IMemberInfoLibrary library)
        {
            var nodeBinary = node as BinaryExpression;
            var nodeMethod = node as MethodCallExpression;
            return nodeBinary != null
                && nodeBinary.NodeType == ExpressionType.Equal
                && (IsNullConstant(nodeBinary.Left) || IsNullConstant(nodeBinary.Right))
                   || nodeMethod != null && library.IsStringIsNullOrEmpty(nodeMethod.Method);
        }

        private static bool IsNullConstant(Expression node)
        {
            var nodeConstant = node as ConstantExpression;
            return nodeConstant != null && nodeConstant.Value == null;
        }

        [NotNull]
        private static readonly MethodInfo[] DbFunctionImplementations = 
            typeof(DbFunctionsImplementation).GetTypeInfo().GetMethods(BindingFlags.Static | BindingFlags.Public);

        [NotNull]private readonly IMemberInfoLibrary _library;
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;
        [NotNull]private readonly Multidictionary<ParameterExpression, ParsedPath> _branches;
        [NotNull]private readonly List<KeyValuePair<ParsedPath, bool>> _checkedPaths;

        public ConditionElementParserVisitor(
            [NotNull]IMemberInfoLibrary library,
            [NotNull]IExpressionBuilder expressionBuilder,
            [NotNull]Multidictionary<ParameterExpression, ParsedPath> branches)
        {
            if (library == null) throw new ArgumentNullException("library");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (branches == null) throw new ArgumentNullException("branches");
            _library = library;
            _expressionBuilder = expressionBuilder;
            _branches = branches;
            _checkedPaths = new List<KeyValuePair<ParsedPath, bool>>();
        }

        [NotNull]
        public override Expression Visit(Expression node)
        {
            var result = base.Visit(node);
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Visitor returned null for {0}", node));
            }
            return result;
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.Negate:
                case ExpressionType.NegateChecked:

                case ExpressionType.Convert:
                case ExpressionType.ConvertChecked:
                    return base.VisitUnary(node);
            }
            throw Unexpected(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            switch (node.NodeType)
            {
                // arithmetics
                case ExpressionType.Add:
                case ExpressionType.AddChecked:
                case ExpressionType.Subtract:
                case ExpressionType.SubtractChecked:
                case ExpressionType.Multiply:
                case ExpressionType.MultiplyChecked:
                case ExpressionType.Divide:
                case ExpressionType.Modulo:

                // bit shifts
                case ExpressionType.RightShift:
                case ExpressionType.LeftShift:

                // bitwise ops
                case ExpressionType.And:
                case ExpressionType.Or:
                case ExpressionType.ExclusiveOr:

                // comparisons
                case ExpressionType.Equal:
                case ExpressionType.NotEqual:
                case ExpressionType.GreaterThan:
                case ExpressionType.GreaterThanOrEqual:
                case ExpressionType.LessThan:
                case ExpressionType.LessThanOrEqual:
                    return base.VisitBinary(node);
            }
            throw Unexpected(node);
        }

        protected override Expression VisitTypeBinary(TypeBinaryExpression node)
        {
            throw Unexpected(node);
        }

        protected override Expression VisitConditional(ConditionalExpression node)
        {
            throw Unexpected(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            return base.VisitConstant(node);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            return base.VisitParameter(node);
        }

        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            throw Unexpected(node);
        }

        private void AddPath(ParsedPath path, bool checkedForNull = true, bool checkPathResult = false)
        {
            if (path != null && path.Elements.Any())
            {
                _branches.Add(path.Start, path);
                if (checkedForNull)
                {
                    _checkedPaths.Add(new KeyValuePair<ParsedPath, bool>(path, checkPathResult));
                }
            }
        }

        private void AddPaths([NotNull]IEnumerable<ParsedPath> paths, bool checkForNull = true, bool checkPathResult = false)
        {
            foreach (var path in paths)
            {
                AddPath(path, checkForNull, checkPathResult);
            }
        }

        /// <summary>
        /// Выделить путь, вставить where not null перед Select и SelectMany, добавить путь в набор путей
        /// </summary>
        [NotNull]
        private Expression ParseAndAddPossiblePath(Expression node)
        {
            return ParseAndAddPossiblePath(node, true);
        }

        [NotNull]
        private Expression ParseAndAddPossiblePath(Expression node, bool checkPathResult)
        {
            if (node is ConstantExpression)
            {
                return node;
            }

            var convert = node as UnaryExpression;
            while (convert != null && convert.NodeType == ExpressionType.Convert)
            {
                if (convert.Operand is ConstantExpression)
                {
                    return node;
                }
                convert = convert.Operand as UnaryExpression;
            }

            var closureMember = node as MemberExpression;
            if (closureMember != null && closureMember.Expression is ConstantExpression)
            {
                return node;
            }

            Expression result;
            var path = PathParserVisitor.TryParseAndConvert(node, _library, _expressionBuilder, _branches, out result);
            AddPath(path, true, checkPathResult);
            return result;
        }

        [NotNull]
        private Expression ParseAndAddPossibleConvertedPath(Expression node)
        {
            var convertNode = node as UnaryExpression;
            if (convertNode != null && convertNode.NodeType == ExpressionType.Convert)
            {
                return convertNode.Update(ParseAndAddPossiblePath(convertNode.Operand));
            }
            throw new NotSupportedException(string.Format("Node with type {0} is not supported in object method calls : {1}", node != null ? node.NodeType.ToString() : "(null)", node));
        }

        private ParsedPath Concat([NotNull]ParsedPath main, [NotNull]ParsedPath continuation)
        {
            return new ParsedPath(
                elements: main.Elements.Take(main.Elements.Count - 1)
                    .AppendOne(new PathElement(main.LastElement().Step).SetParameter(continuation.Start))
                    .Concat(continuation.Elements)
                    .ToList(),
                start: main.Start,
                branches: 
                    (main.Branches != null ? main.Branches.SelectMany(p => p) : Enumerable.Empty<ParsedPath>())
                    .Concat(continuation.Branches != null ? continuation.Branches.SelectMany(p => p) : Enumerable.Empty<ParsedPath>())
                    .Where(p => p != null)
                    .ToLookup(p => p.Start));
        }

        [NotNull]
        private Expression ParseAndAddPathWithCondition([NotNull]MethodCallExpression conditionNode)
        {
            Expression result;
            Expression pathExpression;
            var path = PathParserVisitor.ParseAndConvert(conditionNode.Arguments[0], _library, _expressionBuilder,
                out pathExpression);
            var lambda = conditionNode.LambdaArgument(1);
            var innerCondition = ConditionParserVisitor.Parse(lambda, _library, _expressionBuilder, _branches);
            if (path.Elements.Any())
            {
                if (innerCondition.UsedPaths.Any())
                {
                    AddPaths(innerCondition.UsedPaths.Where(p => p != null && p.Start == innerCondition.Start).Select(inner => Concat(path, inner)));
                    AddPaths(innerCondition.UsedPaths.Where(p => p != null && p.Start != innerCondition.Start), checkForNull: false);
                }
                else
                {
                    AddPath(path);
                }
                result = conditionNode.Update(null, new[] { pathExpression, innerCondition.ServerCondition });
            }
            else if (innerCondition.UsedPaths.Any())
            {
                AddPaths(innerCondition.UsedPaths);
                result = conditionNode.Update(null, new[] { conditionNode.Arguments[0], innerCondition.ServerCondition });
            }
            else
            {
                result = conditionNode;
            }
            return result;
        }

        [NotNull]
        private Expression ParseAndAddPathWithContinuation([NotNull] MethodCallExpression node)
        {
            Expression prefixConverted;
            var firstPart = PathParserVisitor.ParseAndConvert(node.Arguments[0], _library, _expressionBuilder,
                out prefixConverted);
            var lambda = node.LambdaArgument(1);
            if (firstPart.Elements.Count == 0)
            {
                throw new InvalidPathException(string.Format("Path with non-zero length expected in {0}", node.Arguments[0]));
            }
            firstPart.LastElement().SetParameter(lambda.Parameters[0]);
            var secondPart = PathParserVisitor.Parse(lambda.Body, _library, _expressionBuilder);
            AddPath(Concat(firstPart, secondPart));
            return node.Update(null, new[]
            {
                _expressionBuilder.WhereNotNull(prefixConverted), 
                Expression.Lambda(
                    lambda.Type,
                    _expressionBuilder.BuildPathWithNullChecks(secondPart.Start, secondPart.Elements, false),
                    lambda.Parameters[0]),
            });
        }

        [NotNull]
        private Expression ProcessArray(Expression node)
        {
            if (node == null)
            {
                throw new NullReferenceException("node");
            }
            var newArrayNode = node as NewArrayExpression;
            if (newArrayNode != null)
            {
                var initializers = newArrayNode.Expressions.Select(ParseAndAddPossiblePath).ToList();
                return newArrayNode.Update(initializers);
            }
            var constantNode = node as ConstantExpression;
            if (constantNode != null)
            {
                return constantNode;
            }
            throw new NotSupportedException(string.Format("Array argument with node type {0} not supported", node.NodeType));
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            if (node.Expression == null)
            {
                return node;
            }
            bool isNullableStruct = false;
            if (node.Member.DeclaringType == typeof(DateTime)
                || node.Member.DeclaringType == typeof(DateTimeOffset)
                || node.Member.DeclaringType == typeof(string)
                || (isNullableStruct = node.Member.DeclaringType.IsNullableStruct()))
            {
                var checkResult = !isNullableStruct || node.Member.Name != "HasValue";  // HasValue call is valid on nulls, so we shouldn't check
                var path = ParseAndAddPossiblePath(node.Expression, checkResult);
                return node.Update(path);
            }
            return ParseAndAddPossiblePath(node, false);
        }

        private bool HaveSameTypes(ParameterInfo[] first, ParameterInfo[] second)
        {
            if (first == null || second == null)
            {
                return first == null && second == null;
            }
            if (first.Length != second.Length)
            {
                return false;
            }
            for (int i = 0; i < first.Length; ++i)
            {
                if (first[i] == null || second[i] == null || first[i].ParameterType != second[i].ParameterType)
                {
                    return false;
                }
            }
            return true;
        }

        private MethodInfo GetImplementationMethod([NotNull]MethodInfo method)
        {
            var parameters = method.GetParameters();
            foreach (var impl in DbFunctionImplementations)
            {
                if (impl == null || !impl.Name.Equals(method.Name, StringComparison.Ordinal))
                {
                    continue;
                }
                var implParameters = impl.GetParameters();
                if (HaveSameTypes(parameters, implParameters))
                {
                    return impl;
                }
            }
            return null;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            Expression result;
            //static methods with a single path argument
            if (_library.IsEnumerableAny(node.Method)
                || _library.IsEnumerableCount(node.Method)
                || _library.IsEnumerableMax(node.Method)
                || _library.IsEnumerableMin(node.Method)
                || _library.IsEnumerableAvg(node.Method)
                || _library.IsEnumerableSum(node.Method)
                
                || _library.IsStringIsNullOrEmpty(node.Method)
                || _library.IsStaticCeiling(node.Method)
                || _library.IsStaticFloor(node.Method)
                || _library.IsStaticRound(node.Method)
                || _library.IsStaticTruncate(node.Method)
                || _library.IsMathAbs(node.Method))
            {
                var pathExpression = ParseAndAddPossiblePath(node.Arguments[0], true);
                result = node.Update(null, pathExpression.ToEnumerable());
            }
            // static methods with a path argument and a condition argument
            else if (_library.IsEnumerableAnyWithLambda(node.Method)
                     || _library.IsEnumerableCountWithLambda(node.Method)
                     || _library.IsEnumerableAll(node.Method))
            {
                result = ParseAndAddPathWithCondition(node);
            }
            // static methods with two path arguments, the second of which is a continuation of the first
            else if (_library.IsEnumerableAvgWithLambda(node.Method)
                  || _library.IsEnumerableMaxWithLambda(node.Method)
                  || _library.IsEnumerableMinWithLambda(node.Method)
                  || _library.IsEnumerableSumWithLambda(node.Method))
            {
                result = ParseAndAddPathWithContinuation(node);
            }
            // static methods with several independent path arguments
            else if (_library.IsMathPow(node.Method)
                || _library.IsStaticRoundWithPrecision(node.Method)
                || _library.IsStringConcatTwo(node.Method)
                || _library.IsStringConcatThree(node.Method)
                || _library.IsStringConcatFour(node.Method))
            {
                var paths = node.Arguments.Select(ParseAndAddPossiblePath).ToList();
                result = node.Update(null, paths);
            }
            // static methods with path objects in array argument
            else if (_library.IsStringConcatArray(node.Method))
            {
                var arrayExpression = ProcessArray(node.Arguments[0]);
                result = node.Update(null, arrayExpression.ToEnumerable());
            }
            // instance methods with path object and single path argument
            else if (_library.IsStringContains(node.Method)
                || _library.IsStringStartsWith(node.Method)
                || _library.IsStringEndsWith(node.Method)
                || _library.IsStringIndexOf(node.Method))
            {
                var objectExpression = ParseAndAddPossiblePath(node.Object);
                var argumentExpression = ParseAndAddPossiblePath(node.Arguments[0]);
                result = node.Update(objectExpression, argumentExpression.ToEnumerable());
            }
            // instance methods with path object and several independent path arguments
            else if (_library.IsStringInsert(node.Method)
                || _library.IsStringRemove(node.Method)
                || _library.IsStringRemoveWithLength(node.Method)
                || _library.IsStringSubstring(node.Method)
                || _library.IsStringSubstringWithLength(node.Method))
            {
                var objectExpression = ParseAndAddPossiblePath(node.Object);
                var argumentExpressions = node.Arguments.Select(ParseAndAddPossiblePath).ToList();
                result = node.Update(objectExpression, argumentExpressions);
            }
            // instance methods with path object and no arguments which are valid on null Nullable<T>
            else if (_library.IsObjectToString(node.Method))
            {
                var objectExpression = ParseAndAddPossiblePath(node.Object, node.Object != null && !node.Object.Type.IsNullableStruct());
                result = node.Update(objectExpression, Enumerable.Empty<Expression>());
            }
            // instance methods with path object and no arguments
            else if (_library.IsStringTrim(node.Method)
                || _library.IsStringToLower(node.Method)
                || _library.IsStringToUpper(node.Method))
            {
                var objectExpression = ParseAndAddPossiblePath(node.Object);
                result = node.Update(objectExpression, Enumerable.Empty<Expression>());
            }
            else if (_library.IsEnumHasFlag(node.Method))
            {
                var objectExpression = ParseAndAddPossiblePath(node.Object);
                var argumentExpression = ParseAndAddPossibleConvertedPath(node.Arguments[0]);
                result = node.Update(objectExpression, argumentExpression.ToEnumerable());
            }
            // instance methods with path object and non-path argument(s)
            else if (_library.IsStringTrimWithParam(node.Method)
                || _library.IsStringTrimEnd(node.Method)
                || _library.IsStringTrimStart(node.Method))
            {
                var objectExpression = ParseAndAddPossiblePath(node.Object);
                result = node.Update(objectExpression, node.Arguments);
            }
            // DbFunctions
            else if (node.Method.DeclaringType != null
                && node.Method.DeclaringType.Name.Equals("DbFunctions", StringComparison.Ordinal)
                && node.Method.IsStatic)
            {
                var impl = GetImplementationMethod(node.Method);
                if (impl == null)
                {
                    throw NotSupportedMethod(node);
                }
                var argumentExpressions = new List<Expression>(node.Arguments.Count);
                foreach (var argument in node.Arguments)
                {
                    argumentExpressions.Add(ParseAndAddPossiblePath(argument));
                }
                result = Expression.Call(impl, argumentExpressions);
            }
            else
            {
                throw NotSupportedMethod(node);
            }
            return result;
        }


        [NotNull]
        private Exception NotSupportedMethod([NotNull]MethodCallExpression node)
        {
            return new NotSupportedException(string.Format(
                "Method {0} of {1} with arguments ({2}) is not supported in condition expressions",
                node.Method, node.Method.DeclaringType, string.Join(",", node.Arguments.Select(a => a != null ? a.GetType().Name : "(null)"))));
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitBlock(BlockExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitDebugInfo(DebugInfoExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitDefault(DefaultExpression node)
        {
            throw Unexpected(node);
        }

#if NET45
        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitDynamic(DynamicExpression node)
        {
            throw Unexpected(node);
        }
#endif

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitExtension(Expression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitGoto(GotoExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitIndex(IndexExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitInvocation(InvocationExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitLabel(LabelExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitListInit(ListInitExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitLoop(LoopExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitMemberInit(MemberInitExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitNew(NewExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitNewArray(NewArrayExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitRuntimeVariables(RuntimeVariablesExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitSwitch(SwitchExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override Expression VisitTry(TryExpression node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override CatchBlock VisitCatchBlock(CatchBlock node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override ElementInit VisitElementInit(ElementInit node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override LabelTarget VisitLabelTarget(LabelTarget node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override MemberAssignment VisitMemberAssignment(MemberAssignment node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override MemberBinding VisitMemberBinding(MemberBinding node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override MemberListBinding VisitMemberListBinding(MemberListBinding node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override MemberMemberBinding VisitMemberMemberBinding(MemberMemberBinding node)
        {
            throw Unexpected(node);
        }

        /// <summary>
        /// Выбросить исключение на недопустимом в условии узле
        /// </summary>
        protected override SwitchCase VisitSwitchCase(SwitchCase node)
        {
            throw Unexpected(node);
        }

        [NotNull]
        private Exception Unexpected(Expression node)
        {
            if (node == null)
            {
                return new NotSupportedException("Unexpected null node");
            }
            return new NotSupportedException(string.Format("Unexpected node with type {0} in boolean condition : {1}", node.NodeType, node));
        }

        [NotNull]
        private Exception Unexpected(object node)
        {
            if (node == null)
            {
                return new NotSupportedException("Unexpected null node");
            }
            return new NotSupportedException(string.Format("Unexpected node with type {0} in boolean condition : {1}", node.GetType(), node));
        }
    }
}
// ReSharper restore RedundantOverridenMember
