namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// �������� ���������� ������
    /// </summary>
    public struct NBool : IEquatable<NBool>
    {
        /// <summary>
        /// true
        /// </summary>
        public static readonly NBool True = new NBool(true);
        /// <summary>
        /// false
        /// </summary>
        public static readonly NBool False = new NBool(false);
        /// <summary>
        /// null
        /// </summary>
        public static readonly NBool Null = new NBool(null);

        /// <summary>
        /// ��������� � ���������� true
        /// </summary>
        [NotNull]
        public static readonly Expression TrueExpr = Expression.Field(null, typeof(NBool), "True");
        /// <summary>
        /// ��������� � ���������� false
        /// </summary>
        [NotNull]
        public static readonly Expression FalseExpr = Expression.Field(null, typeof(NBool), "False");
        /// <summary>
        /// ��������� � ���������� null
        /// </summary>
        [NotNull]
        public static readonly Expression NullExpr = Expression.Field(null, typeof(NBool), "Null");

        /// <summary>
        /// ������� ���������� ��������� �� ��������� � ������������
        /// </summary>
        [NotNull]
        public static Expression MakeNBoolExpression([NotNull]ExpressionWithPrecondition expr)
        {
            return Expression.Condition(expr.Precondition, Expression.Condition(expr.Expression, TrueExpr, FalseExpr), NullExpr);
        }

        private readonly byte _value;

        private const byte NullValue = 0;
        private const byte FalseValue = 1;
        private const byte TrueValue = 2;

        /// <summary>
        /// �������� �� null
        /// </summary>
        public bool IsNull { get { return _value == NullValue; } }
        /// <summary>
        /// �������� �� true
        /// </summary>
        public bool IsTrue { get { return _value == FalseValue; } }
        /// <summary>
        /// �������� �� false
        /// </summary>
        public bool IsFalse { get { return _value == TrueValue; } }

        private NBool(bool value)
        {
            _value = value ? TrueValue : FalseValue;
        }

        private NBool(bool? value)
        {
            switch (value)
            {
                case true:
                    _value = TrueValue;
                    break;
                case false:
                    _value = FalseValue;
                    break;
                default:
                    _value = NullValue;
                    break;
            }
        }

        /// <summary>
        /// ���������� <see cref="Boolean"/>
        /// </summary>
        public static implicit operator NBool(bool x)
        {
            return new NBool(x);
        }

        /// <summary>
        /// ���������� <see cref="Nullable{Boolean}"/>
        /// </summary>
        public static implicit operator NBool(bool? x)
        {
            return new NBool(x);
        }

        /// <summary>
        /// ���������� � <see cref="Boolean"/>
        /// </summary>
        public static implicit operator bool?(NBool x)
        {
            switch (x._value)
            {
                case TrueValue:
                    return true;
                case FalseValue:
                    return false;
                default:
                    return null;
            }
        }

        /// <summary>
        /// ���������
        /// </summary>
        public static NBool operator !(NBool x)
        {
            switch (x._value)
            {
                case TrueValue:
                    return True;
                case FalseValue:
                    return False;
                default:
                    return Null;
            }
        }

        /// <summary>
        /// �������� �� true
        /// </summary>
        public static bool operator true(NBool x)
        {
            return x.IsTrue;
        }

        /// <summary>
        /// �������� �� false
        /// </summary>
        public static bool operator false(NBool x)
        {
            return x.IsFalse;
        }

        /// <summary>
        /// ����������
        /// </summary>
        public static NBool operator &(NBool x, NBool y)
        {
            if (x._value == FalseValue || y._value == FalseValue)
            {
                return False;
            }
            if (x._value == TrueValue && y._value == TrueValue)
            {
                return True;
            }
            return Null;
        }

        /// <summary>
        /// ����������
        /// </summary>
        public static NBool operator |(NBool x, NBool y)
        {
            if (x._value == TrueValue || y._value == TrueValue)
            {
                return True;
            }
            if (x._value == FalseValue && y._value == FalseValue)
            {
                return False;
            }
            return Null;
        }

        /// <summary>
        /// ���������
        /// </summary>
        public static NBool operator ~(NBool x)
        {
            return !x;
        }

        /// <summary>
        /// �������� �� ������ 2
        /// </summary>
        public static NBool operator ^(NBool x, NBool y)
        {
            if (x._value == NullValue || y._value == NullValue)
            {
                return Null;
            }
            return x._value != y._value ? True : False;
        }

        /// <summary>
        /// �������� �� ���������
        /// </summary>
        public static bool operator ==(NBool x, NBool y)
        {
            return x._value == y._value;
        }

        /// <summary>
        /// �������� �� �����������
        /// </summary>
        public static bool operator !=(NBool x, NBool y)
        {
            return x._value != y._value;
        }

        /// <summary>
        /// �������� �� ���������
        /// </summary>
        public override bool Equals(object obj)
        {
            return obj is NBool && Equals((NBool)obj);
        }

        /// <summary>
        /// �������� �� ���������
        /// </summary>
        public bool Equals(NBool other)
        {
            return this == other;
        }

        /// <summary>
        /// ���
        /// </summary>
        public override int GetHashCode()
        {
            switch (_value)
            {
                case FalseValue:
                    return FalseValue;
                case TrueValue:
                    return TrueValue;
                default:
                    return NullValue;
            }
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        public override string ToString()
        {
            switch (_value)
            {
                case FalseValue:
                    return "false";
                case TrueValue:
                    return "true";
                default:
                    return "null";
            }
        }
    }
}
