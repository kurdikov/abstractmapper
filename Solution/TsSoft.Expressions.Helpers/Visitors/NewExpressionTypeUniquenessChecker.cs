﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Обходчик выражений, проверяющий уникальность типов в NewExpression внутри выражения
    /// </summary>
    public class NewExpressionTypeUniquenessChecker : ExpressionVisitor
    {
        [NotNull]private readonly HashSet<Type> _usedTypes = new HashSet<Type>();
        [CanBeNull]private Type _reusedType;

        /// <summary>
        /// Проверить уникальность типов в NewExpression внутри выражения
        /// </summary>
        /// <param name="checkedExpr">Проверяемое выражение</param>
        /// <returns>Тип, конструктор которого дважды встречается в выражении, или null, если такого нет</returns>
        [CanBeNull]
        public static Type Check(Expression checkedExpr)
        {
            var checker = new NewExpressionTypeUniquenessChecker();
            checker.Visit(checkedExpr);
            return checker._reusedType;
        }

        /// <summary>
        /// Посетить узел-конструктор
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Тот же узел</returns>
        protected override Expression VisitNew(NewExpression node)
        {
            if (_usedTypes.Contains(node.Type))
            {
                _reusedType = node.Type;
            }
            _usedTypes.Add(node.Type);
            return base.VisitNew(node);
        }
    }
}
