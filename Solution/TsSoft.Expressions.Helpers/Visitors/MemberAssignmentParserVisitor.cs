﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models.AbstractMapper;

    class MemberAssignmentParserVisitor : ExpressionVisitor
    {
        [NotNull]private readonly IMemberInfoLibrary _library;
        [NotNull]private readonly INewExpressionHelper _newExpressionHelper;
        [NotNull]private SelectMemberAssignment _current;

        public MemberAssignmentParserVisitor([NotNull]IMemberInfoLibrary library, [NotNull]INewExpressionHelper newExpressionHelper)
        {
            if (library == null) throw new ArgumentNullException("library");
            if (newExpressionHelper == null) throw new ArgumentNullException("newExpressionHelper");
            _library = library;
            _newExpressionHelper = newExpressionHelper;
        }

        [NotNull]
        public SelectMemberAssignment Parse(Expression assignment)
        {
            _current = new SelectMemberAssignment();
            Visit(assignment);
            return _current;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.IsGenericMethod)
            {
                if (_library.IsEnumerableSelect(node.Method))
                {
                    _current.InnerSelect = node.Arguments[1] as LambdaExpression;
                    base.Visit(node.Arguments[0]);
                }
                else if (_library.IsEnumerableWhere(node.Method))
                {
                    if (_current.Condition != null)
                    {
                        throw new InvalidPathException(string.Format("Two conditions in a row are not supported: {0}", node));
                    }
                    _current.Condition = node.Arguments[1] as LambdaExpression;
                }
                else if (_library.IsEnumerableToList(node.Method))
                {
                    base.Visit(node.Arguments[0]);
                }
                else
                {
                    throw new InvalidPathException(string.Format("Only Select, Where and ToList methods are supported for select expressions: {0}", node));
                }
            }
            return node;
        }

        protected override Expression VisitMemberInit(MemberInitExpression node)
        {
            _current.InnerAssignment = _newExpressionHelper.GetAssignments(node).ToList();
            return node;
        }

        protected override Expression VisitNew(NewExpression node)
        {
            _current.InnerAssignment = _newExpressionHelper.GetAssignments(node).ToList();
            return node;
        }
    }
}
