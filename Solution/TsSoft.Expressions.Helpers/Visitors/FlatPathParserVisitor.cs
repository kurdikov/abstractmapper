﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Обходчик выражения, выделяющий линейный путь
    /// </summary>
    public class FlatPathParserVisitor : ExpressionVisitor
    {
        [NotNull]private List<ValueHoldingMember> _propertyInfos;

        /// <summary>
        /// Получить линейный путь по выражению
        /// </summary>
        /// <param name="expression">Выражение</param>
        /// <returns>Линейный путь</returns>
        [NotNull]
        public IReadOnlyList<ValueHoldingMember> GetMemberCollection(Expression expression)
        {
            _propertyInfos = new List<ValueHoldingMember>();
            Visit(expression);
            return _propertyInfos;
        }

        /// <summary>
        /// Посетить узел с вызовом метода
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Значение не определено</returns>
        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            var method = node.Method;
            if (method.DeclaringType != typeof(Enumerable) || 
                (method.Name != "Select" && method.Name != "SelectMany") || 
                node.Arguments.Count != 2)
            {
                throw new ArgumentException("Path expression may not contain invocations of methods other than Enumerable.Select or Enumerable.SelectMany with one lambda parameter");
            }
            Visit(node.Arguments[0]);
            Visit(node.Arguments[1]);
            return Expression.Default(node.Method.ReturnType);
        }

        /// <summary>
        /// Посетить узел с обращением к члену
        /// </summary>
        /// <param name="node">Узел</param>
        /// <returns>Значение не определено</returns>
        protected override Expression VisitMember(MemberExpression node)
        {
            Visit(node.Expression);
            _propertyInfos.Add(ValueHoldingMember.GetFromExpression(node));
            return node;
        }
    }
}
