﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Closures;

    /// <summary>
    /// Компилирует лямбду, предварительно компилируя встречающиеся в ней внутренние лямбды
    /// </summary>
    public sealed class InnerLambdaCompilerVisitor : ExpressionVisitor
    {
        /// <summary>
        /// Компилировать лямбду, предварительно скомпилировав встречающиеся в ней внутренние лямбды
        /// </summary>
        /// <typeparam name="T">Тип делегата лямбды</typeparam>
        /// <param name="expr">Лямбда</param>
        /// <param name="compilationType">Тип компиляции</param>
        /// <param name="innerLambdaCompiler">Компилятор лямбд</param>
        /// <param name="specialInnerLambdaCompiler">Компилятор лямбд</param>
        /// <param name="methodBuilderLambdaCompiler">Компилятор лямбд</param>
        /// <returns>Делегат</returns>
        [NotNull]
        public static T Compile<T>(
            [NotNull] Expression<T> expr,
            LambdaCompilationType compilationType,
            [NotNull]IInnerLambdaCompiler innerLambdaCompiler,
            [NotNull]ISpecialInnerLambdaCompiler specialInnerLambdaCompiler,
            [NotNull]IMethodBuilderLambdaCompiler methodBuilderLambdaCompiler)
        {
            if (compilationType == LambdaCompilationType.DoNotPrecompile)
            {
                return expr.Compile();
            }
            if (compilationType == LambdaCompilationType.CompileToMethod)
            {
                return methodBuilderLambdaCompiler.Compile(expr);
            }
            var visitor = new InnerLambdaCompilerVisitor(compilationType, innerLambdaCompiler, specialInnerLambdaCompiler);
            var result = visitor.CompileInnerLambdas(expr);
            return result.Compile();
        }

        /// <summary>
        /// Компилировать лямбду, предварительно скомпилировав встречающиеся в ней внутренние лямбды
        /// </summary>
        /// <param name="expr">Лямбда</param>
        /// <param name="compilationType">Тип компиляции</param>
        /// <param name="innerLambdaCompiler">Компилятор лямбд</param>
        /// <param name="specialInnerLambdaCompiler">Компилятор лямбд</param>
        /// <param name="methodBuilderLambdaCompiler">Компилятор лямбд</param>
        /// <returns>Делегат</returns>
        [NotNull]
        public static Delegate Compile(
            [NotNull]LambdaExpression expr,
            LambdaCompilationType compilationType,
            [NotNull]IInnerLambdaCompiler innerLambdaCompiler,
            [NotNull]ISpecialInnerLambdaCompiler specialInnerLambdaCompiler,
            [NotNull]IMethodBuilderLambdaCompiler methodBuilderLambdaCompiler)
        {
            if (compilationType == LambdaCompilationType.DoNotPrecompile)
            {
                return expr.Compile();
            }
            if (compilationType == LambdaCompilationType.CompileToMethod)
            {
                return methodBuilderLambdaCompiler.Compile(expr);
            }
            var visitor = new InnerLambdaCompilerVisitor(compilationType, innerLambdaCompiler, specialInnerLambdaCompiler);
            var result = visitor.CompileInnerLambdas(expr);
            return result.Compile();
        }

        private readonly LambdaCompilationType _compilationType;
        [NotNull]private readonly IInnerLambdaCompiler _innerLambdaCompiler;
        [NotNull]private readonly ISpecialInnerLambdaCompiler _specialInnerLambdaCompiler;

        /// <summary>
        /// Компилирует лямбду, предварительно компилируя встречающиеся в ней внутренние лямбды
        /// </summary>
        public InnerLambdaCompilerVisitor(
            LambdaCompilationType compilationType,
            [NotNull]IInnerLambdaCompiler innerLambdaCompiler,
            [NotNull]ISpecialInnerLambdaCompiler specialInnerLambdaCompiler)
        {
            if (innerLambdaCompiler == null) throw new ArgumentNullException("innerLambdaCompiler");
            if (specialInnerLambdaCompiler == null) throw new ArgumentNullException("specialInnerLambdaCompiler");
            _compilationType = compilationType;
            _innerLambdaCompiler = innerLambdaCompiler;
            _specialInnerLambdaCompiler = specialInnerLambdaCompiler;
        }

        /// <summary>
        /// Компилировать встречающиеся в лямбде лямбда-аргументы методов без открытых параметров
        /// </summary>
        /// <typeparam name="T">Тип делегата лямбды</typeparam>
        /// <param name="expr">Лямбда</param>
        [NotNull]
        public Expression<T> CompileInnerLambdas<T>([NotNull]Expression<T> expr)
        {
            var body = Visit(expr.Body);
            return Expression.Lambda<T>(body, expr.Parameters);
        }

        /// <summary>
        /// Компилировать встречающиеся в лямбде лямбда-аргументы методов без открытых параметров
        /// </summary>
        /// <param name="expr">Лямбда</param>
        [NotNull]
        public LambdaExpression CompileInnerLambdas([NotNull]LambdaExpression expr)
        {
            var body = Visit(expr.Body);
            return Expression.Lambda(expr.Type, body, expr.Parameters);
        }

        /// <summary>
        /// Обойти выражение, компилируя встречающиеся в нём лямбда-аргументы методов без открытых параметров
        /// </summary>
        [ContractAnnotation("notnull => notnull; null => null")]
        public override Expression Visit(Expression node)
        {
            return node == null ? null : base.Visit(node);
        }

        /// <summary>
        /// Посетить узел
        /// </summary>
        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            var newBody = Visit(node.Body);
            var newLambda = node.Update(newBody, node.Parameters);
            var openParams = OpenLambdaParameterFinderVisitor.GetOpenParameters(newLambda);
            switch (_compilationType)
            {
                case LambdaCompilationType.PrecompileInner:
                    return _innerLambdaCompiler.GenerateExpressionWithCompiledLambda(newLambda, openParams);
                case LambdaCompilationType.PrecompileInnerSpecial:
                    return _specialInnerLambdaCompiler.TryGenerateExpressionWithCompiledLambda(newLambda, openParams);
                case LambdaCompilationType.PrecompileInnerWithoutOpenParams:
                    return openParams.Count > 0 ? (Expression)newLambda : Expression.Constant(newLambda.Compile(), newLambda.Type);
                default:
                    return newLambda;
            }
        }

        /// <summary>
        /// Обойти узел
        /// </summary>
        protected override Expression VisitUnary(UnaryExpression node)
        {
            if (node.NodeType == ExpressionType.Quote)  // we don't want to compile lambdas passed as expressions
            {
                return node;
            }
            return base.VisitUnary(node);
        }
    }
}
