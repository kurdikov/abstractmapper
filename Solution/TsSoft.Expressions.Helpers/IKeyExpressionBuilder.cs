﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Построитель выражений с ключами
    /// </summary>
    public interface IKeyExpressionBuilder : IExpressionBuilder
    {
        /// <summary>
        /// Получить тип ключа, который можно выделить
        /// </summary>
        Type GetKeyType([NotNull]IReadOnlyList<ValueHoldingMember> key);

        /// <summary>
        /// Получить тип ключа, который можно выделить
        /// </summary>
        Type GetKeyType([NotNull]IReadOnlyList<PropertyInfo> key);

        /// <summary>
        /// Создать выделитель ключа из объекта
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <param name="key">Ключ</param>
        /// <param name="replaceNullsWithDefaults">Заменить nullable-типы на не-nullable, подставляя значение по умолчанию вместо null</param>
        [NotNull]
        Expression CreateExtractKeyExpression([NotNull]Expression obj, [NotNull]IReadOnlyList<ValueHoldingMember> key, bool replaceNullsWithDefaults = false);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="TEntity">Тип параметра</typeparam>
        /// <typeparam name="TValue">Тип композитного ключа</typeparam>
        /// <param name="key">Ключ</param>
        /// <param name="values">Значения</param>
        /// <returns>e => (e.Property1 == values1.Item1 AND e.Property2 = values1.Item2) || (e.Property1 == values2.Item1 AND e.Property2 = values2.Item2) || ...</returns>
        [NotNull]
        Expression<Func<TEntity, bool>> BuildWhereIn<TEntity, TValue>([NotNull]IReadOnlyList<ValueHoldingMember> key, IEnumerable<TValue> values);
    }
}
