﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers.Visitors;

    [TestClass]
    public class ConstantExpressionReplacerVisitorTests
    {
        [TestMethod]
        public void ReplaceOneConstantOfTwo()
        {
            var te = new TestEntity { Prop1 = 1, Prop2 = "2", Prop3 = 3 };
            var te2 = new TestEntity { Prop1 = 1, Prop2 = "2", Prop3 = 3 };
            Expression<Func<TestEntity>> expr = Expression.Lambda<Func<TestEntity>>(
                Expression.MemberInit(
                    Expression.New(typeof(TestEntity)),
                    Expression.Bind(
                        typeof(TestEntity).GetTypeInfo().GetProperty("Prop1"),
                        Expression.Property(
                            Expression.Constant(te),
                            "Prop1")),
                    Expression.Bind(
                        typeof(TestEntity).GetTypeInfo().GetProperty("Prop2"),
                        Expression.Property(
                            Expression.Constant(te2),
                            "Prop2"))));
            var expected = Expression.Lambda<Func<TestEntity>>(
                Expression.MemberInit(
                    Expression.New(typeof(TestEntity)),
                    Expression.Bind(
                        typeof(TestEntity).GetTypeInfo().GetProperty("Prop1"),
                        Expression.Property(
                            Expression.New(typeof(TestEntity)),
                            "Prop1")),
                    Expression.Bind(
                        typeof(TestEntity).GetTypeInfo().GetProperty("Prop2"),
                        Expression.Property(
                            Expression.Constant(te2),
                            "Prop2"))));
            var result = ConstantExpressionReplacerVisitor.ReplaceConstants(expr, new Dictionary<object, Expression>
            {
                {te, Expression.New(typeof(TestEntity))}
            });
            ExprAssert.AreEqual(expected, result);
        }

        [TestMethod]
        public void ReplaceTwoConstantsOfTwo()
        {
            var te = new TestEntity { Prop1 = 1, Prop2 = "2", Prop3 = 3 };
            var te2 = new TestEntity { Prop1 = 1, Prop2 = "2", Prop3 = 3 };
            Expression<Func<TestEntity>> expr = Expression.Lambda<Func<TestEntity>>(
                Expression.MemberInit(
                    Expression.New(typeof(TestEntity)),
                    Expression.Bind(
                        typeof(TestEntity).GetProperty("Prop1"),
                        Expression.Property(
                            Expression.Constant(te),
                            "Prop1")),
                    Expression.Bind(
                        typeof(TestEntity).GetProperty("Prop2"),
                        Expression.Property(
                            Expression.Constant(te2),
                            "Prop2"))));
            var expected = Expression.Lambda<Func<TestEntity>>(
                Expression.MemberInit(
                    Expression.New(typeof(TestEntity)),
                    Expression.Bind(
                        typeof(TestEntity).GetProperty("Prop1"),
                        Expression.Property(
                            Expression.New(typeof(TestEntity)),
                            "Prop1")),
                    Expression.Bind(
                        typeof(TestEntity).GetProperty("Prop2"),
                        Expression.Property(
                            Expression.Constant(null, typeof(TestEntity)),
                            "Prop2"))));
            var result = ConstantExpressionReplacerVisitor.ReplaceConstants(expr, new Dictionary<object, Expression>
            {
                {te, Expression.New(typeof(TestEntity))},
                {te2, Expression.Constant(null, typeof(TestEntity))},
            });
            ExprAssert.AreEqual(expected, result);
        }
    }
}
