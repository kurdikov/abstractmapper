﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers.Visitors;

    [TestClass]
    public class ConstantExpressionFinderVisitorTests
    {
        [TestMethod]
        public void NoReferenceConstants()
        {
            Expression<Func<TestEntity, TestEntity>> expr = te => new TestEntity
            {
                Prop1 = te.Prop1,
                Prop2 = "12",
                Prop3 = 1,
                External = new ExternalEntity(),
            };
            var consts = ConstantExpressionFinderVisitor.GetConstants(expr);
            Assert.AreEqual(0, consts.Count);
        }

        [TestMethod]
        public void OneReferenceConstant()
        {
            var te = new TestEntity {Prop1 = 1, Prop2 = "2", Prop3 = 3};
            Expression<Func<TestEntity>> expr = Expression.Lambda<Func<TestEntity>>(
                Expression.MemberInit(
                    Expression.New(typeof(TestEntity)),
                    Expression.Bind(
                        typeof(TestEntity).GetTypeInfo().GetProperty("Prop1"),
                        Expression.Property(
                            Expression.Constant(te),
                            "Prop1")),
                    Expression.Bind(
                        typeof(TestEntity).GetTypeInfo().GetProperty("Prop2"),
                        Expression.Constant("12"))));
            var consts = ConstantExpressionFinderVisitor.GetConstants(expr);
            Assert.AreEqual(1, consts.Count);
            Assert.IsTrue(consts.Contains(te));
        }

        [TestMethod]
        public void OneReferenceConstantTwice()
        {
            var te = new TestEntity { Prop1 = 1, Prop2 = "2", Prop3 = 3 };
            Expression<Func<TestEntity>> expr = Expression.Lambda<Func<TestEntity>>(
                Expression.MemberInit(
                    Expression.New(typeof(TestEntity)),
                    Expression.Bind(
                        typeof(TestEntity).GetProperty("Prop1"),
                        Expression.Property(
                            Expression.Constant(te),
                            "Prop1")),
                    Expression.Bind(
                        typeof(TestEntity).GetProperty("Prop2"),
                        Expression.Property(
                            Expression.Constant(te),
                            "Prop2"))));
            var consts = ConstantExpressionFinderVisitor.GetConstants(expr);
            Assert.AreEqual(1, consts.Count);
            Assert.IsTrue(consts.Contains(te));
        }

        [TestMethod]
        public void TwoReferenceConstants()
        {
            var te = new TestEntity { Prop1 = 1, Prop2 = "2", Prop3 = 3 };
            var te2 = new TestEntity { Prop1 = 1, Prop2 = "2", Prop3 = 3 };
            Expression<Func<TestEntity>> expr = Expression.Lambda<Func<TestEntity>>(
                Expression.MemberInit(
                    Expression.New(typeof(TestEntity)),
                    Expression.Bind(
                        typeof(TestEntity).GetProperty("Prop1"),
                        Expression.Property(
                            Expression.Constant(te),
                            "Prop1")),
                    Expression.Bind(
                        typeof(TestEntity).GetProperty("Prop2"),
                        Expression.Property(
                            Expression.Constant(te2),
                            "Prop2"))));
            var consts = ConstantExpressionFinderVisitor.GetConstants(expr);
            Assert.AreEqual(2, consts.Count);
            Assert.IsTrue(consts.Contains(te));
            Assert.IsTrue(consts.Contains(te2));
        }
    }
}
