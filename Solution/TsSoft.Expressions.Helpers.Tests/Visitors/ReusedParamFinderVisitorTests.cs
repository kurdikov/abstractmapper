﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Linq.Expressions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Visitors;

    [TestClass]
    public class ReusedParameterFinderVisitorTests
    {
        [TestMethod]
        public void NoReuse()
        {
            Expression<Func<int, int>> expr = x => x + 1;
            var result = ReusedParameterFinderVisitor.FindReusedParameters(expr);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void OneOfOneReused()
        {
            Expression<Func<int, int>> expr = x => x + x * 2;
            var result = ReusedParameterFinderVisitor.FindReusedParameters(expr);
            Assert.AreEqual(1, result.Count);
            Assert.IsTrue(result.Contains(expr.ParameterAt(0)));
        }

        [TestMethod]
        public void OneOfTwoReused()
        {
            Expression<Func<int, int, int>> expr = (x, y) => x + x * y;
            var result = ReusedParameterFinderVisitor.FindReusedParameters(expr);
            Assert.AreEqual(1, result.Count);
            Assert.IsTrue(result.Contains(expr.ParameterAt(0)));
        }

        [TestMethod]
        public void TwoOfThreeReused()
        {
            Expression<Func<int, int, int, int>> expr = (x, y, z) => x * y + x * z + z;
            var result = ReusedParameterFinderVisitor.FindReusedParameters(expr);
            Assert.AreEqual(2, result.Count);
            Assert.IsTrue(result.Contains(expr.ParameterAt(0)));
            Assert.IsTrue(result.Contains(expr.ParameterAt(2)));
        }
    }
}
