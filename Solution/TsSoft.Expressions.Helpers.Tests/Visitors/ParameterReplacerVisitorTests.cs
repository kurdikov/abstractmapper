﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Visitors;

    [TestClass]
    public class ParameterReplacerVisitorTests
    {
        [TestMethod]
        public void ReplaceOne()
        {
            Expression<Func<int, int, int>> expr = (x, y) => x + y;
            var result = ParameterReplacerVisitor.ReplaceInBody(
                expr, expr.ParameterAt(0), Expression.Multiply(expr.ParameterAt(0), expr.ParameterAt(0)));
            ExprAssert.AreEqual((x, y) => x * x + y, result);
        }

        [TestMethod]
        public void ReplaceOneTwice()
        {
            Expression<Func<int, int, int>> expr = (x, y) => x + y + x;
            var result = ParameterReplacerVisitor.ReplaceInBody(
                expr, expr.ParameterAt(0), Expression.Multiply(expr.ParameterAt(0), expr.ParameterAt(0)));
            ExprAssert.AreEqual((x, y) => x * x + y + x * x, result);
        }

        [TestMethod]
        public void ReplaceTwo()
        {
            Expression<Func<int, int, int>> expr = (x, y) => x + y;
            var result = ParameterReplacerVisitor.ReplaceInBody(
                expr,
                new Dictionary<ParameterExpression, Expression> 
                {
                    {expr.ParameterAt(0), Expression.Multiply(expr.ParameterAt(0), expr.ParameterAt(0))},
                    {expr.ParameterAt(1), Expression.Divide(expr.ParameterAt(1), expr.ParameterAt(0))},
                });
            ExprAssert.AreEqual((x, y) => x * x + y / x, result);
        }

        [TestMethod]
        public void ReplaceParameterReusedInInnerLambda()
        {
            var param = Expression.Parameter(typeof(TestEntity));
            var expr = Expression.Lambda<Func<TestEntity, object>>(
                Expression.Coalesce(
                    Expression.Call(
                        StaticHelpers.Library.EnumerableWhere(typeof(TestEntity)),
                        Expression.Property(param, "Children"),
                        Expression.Lambda<Func<TestEntity, bool>>(
                            Expression.Equal(
                                Expression.Property(param, "Prop1"),
                                Expression.Constant(0)),
                            param)),
                    Expression.Property(param, "Children")),
                param);
            var result = ParameterReplacerVisitor.ReplaceInBody(expr, expr.ParameterAt(0), Expression.Property(expr.ParameterAt(0), "Parent"));
            ExprAssert.AreEqual(te => te.Parent.Children.Where(c => c.Prop1 == 0) ?? te.Parent.Children, result);
        }
    }
}
