﻿namespace TsSoft.Expressions.Helpers.Visitors
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Visitors;

    [TestClass]
    public class OpenLambdaParameterFinderVisitorTests
    {
        [TestMethod]
        public void WithoutOpenParametersAndInnerLambdas()
        {
            Expression<Func<TestEntity, object>> lambda = te => te.Children.Count;
            var result = OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void WithoutOpenParametersWithInnerLambda()
        {
            Expression<Func<TestEntity, object>> lambda = te => te.Children.Select(c => c.Parent.Prop1);
            var result = OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void WithoutOpenParametersWithTwoLevelInnerLambda()
        {
            Expression<Func<TestEntity, object>> lambda = te => te.Children.Select(c => c.Children.Select(cc => cc.Prop1).Union(c.Parent.Prop1.ToEnumerable()));
            var result = OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void WithOpenParameterWithoutInnerLambdas()
        {
            Expression<Func<TestEntity, object>> lambda = te => te.Children.Where(c => c.Prop1 == te.Prop1);
            var innerLambda = ((MethodCallExpression) lambda.Body).LambdaArgument(1);
            var result = OpenLambdaParameterFinderVisitor.GetOpenParameters(innerLambda);
            Assert.AreEqual(1, result.Count);
            Assert.IsTrue(result.Contains(lambda.Parameters[0]));
        }

        [TestMethod]
        public void WithOpenParameterOccurringInInnerLambda()
        {
            Expression<Func<TestEntity, object>> lambda = te => te.Children.Where(c => c.Children.Any(cc => cc.Prop1 == te.Prop1));
            var innerLambda = ((MethodCallExpression)lambda.Body).LambdaArgument(1);
            var result = OpenLambdaParameterFinderVisitor.GetOpenParameters(innerLambda);
            Assert.AreEqual(1, result.Count);
            Assert.IsTrue(result.Contains(lambda.Parameters[0]));
        }

        [TestMethod]
        public void LambdaWithParameterReusedInInnerLambda()
        {
            var param = Expression.Parameter(typeof(TestEntity));
            var expr = Expression.Lambda<Func<TestEntity, object>>(
                Expression.Coalesce(
                    Expression.Call(
                        StaticHelpers.Library.EnumerableWhere(typeof(TestEntity)),
                        Expression.Property(param, "Children"),
                        Expression.Lambda<Func<TestEntity, bool>>(
                            Expression.Constant(true, typeof(bool)),
                            param)),
                    Expression.Property(param, "Children")),
                param);
            var result = OpenLambdaParameterFinderVisitor.GetOpenParameters(expr);
            Assert.AreEqual(0, result.Count);
        }
    }
}
