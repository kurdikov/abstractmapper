﻿namespace TsSoft.Expressions.Helpers.Collections
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers.Collections;

    [TestClass]
    public class TreeHelperTests
    {
        private readonly HierarchyEntity[] _hierarchyEntities =
        {
            new HierarchyEntity
            {
                Id = 0,
            },
            new HierarchyEntity
            {
                Id = 4,
                ParentId = 1,
            },
            new HierarchyEntity
            {
                Id = 2,
                ParentId = 1,
            },
            new HierarchyEntity
            {
                Id = 1,
            },
            new HierarchyEntity
            {
                Id = 3,
            },

            new HierarchyEntity
            {
                Id = 5,
                ParentId = 2,
            },
            new HierarchyEntity
            {
                Id = 6,
                ParentId = 5
            },
        };

        private TreeHelper _helper;

        [TestInitialize]
        public void Initialize()
        {
            _helper = new TreeHelper();
        }

        [TestMethod]
        public void ConstructTreeStructureTest()
        {
            var childCount = new[] {0, 2, 1, 0, 0, 1, 0};
            var result =
                _helper.ConstructTreeStructure(
                    _hierarchyEntities,
                    he => he.Id,
                    entity => entity.ParentId,
                    (child, parent) => child.Parent = parent,
                    entity => entity.Children).ToArray();
            foreach (var entity in result)
            {
                if (entity.ParentId == null)
                {
                    Assert.IsNull(entity.Parent);
                }
                else if (_hierarchyEntities.Any(e => e.Id == entity.ParentId.Value))
                {
                    Assert.IsNotNull(entity.Parent);
                    Assert.AreEqual(entity.ParentId, entity.Parent.Id);
                }
                else
                {
                    Assert.IsNull(entity.Parent);
                }
                Assert.AreEqual(childCount[entity.Id], entity.Children.Count);
            }
        }

        [TestMethod]
        public void ConstructTreeStructureStableTest()
        {
            var childCount = new[] { 0, 2, 1, 0, 0, 1, 0 };
            var hehe = _hierarchyEntities.Reverse().ToList();
            var result =
                _helper.ConstructTreeStructureStable(
                    hehe,
                    he => he.Id,
                    entity => entity.ParentId,
                    (child, parent) => child.Parent = parent,
                    entity => entity.Children).ToArray();
            int i = 0;
            foreach (var entity in result)
            {
                Assert.AreEqual(entity.Id, hehe[i++].Id);
                if (entity.ParentId == null)
                {
                    Assert.IsNull(entity.Parent);
                }
                else if (_hierarchyEntities.Any(e => e.Id == entity.ParentId.Value))
                {
                    Assert.IsNotNull(entity.Parent);
                    Assert.AreEqual(entity.ParentId, entity.Parent.Id);
                }
                else
                {
                    Assert.IsNull(entity.Parent);
                }
                Assert.AreEqual(childCount[entity.Id], entity.Children.Count);
            }
        }

        [TestMethod]
        public void FindTest()
        {
            var treeStructure = HierarchyEntities();
            var findResult = _helper.Find(treeStructure, entity => entity.Children, entity => entity.Id == 2).Single();
            Assert.IsNotNull(findResult);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 2), findResult);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 5), findResult.Children.Single());
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 6), findResult.Children.Single().Children.Single());
        }

        [TestMethod]
        public void OrderByHierarchy()
        {
            var orderedResult = _helper.OrderByHierarchy(
                _hierarchyEntities,
                entity => entity.Id,
                entity => entity.ParentId,
                entity => entity.Children,
                (child, parent) => child.Parent = parent,
                entity => entity.Id,
                null,
                new HierarchyEntity()).ToArray();
            Assert.IsNotNull(orderedResult);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 0), orderedResult[0]);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 1), orderedResult[1]);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 2), orderedResult[2]);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 5), orderedResult[3]);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 6), orderedResult[4]);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 4), orderedResult[5]);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 3), orderedResult[6]);

            var treeStructure = new[]
                {
                    new HierarchyEntity {Id = 10, ParentId = null},
                    new HierarchyEntity {Id = 4, ParentId = 10},
                    new HierarchyEntity {Id = 0, ParentId = 10},
                    new HierarchyEntity {Id = 3, ParentId = 10},
                    new HierarchyEntity {Id = 1, ParentId = 10},
                    new HierarchyEntity {Id = 2, ParentId = 10},
                };
            orderedResult = _helper.OrderByHierarchy(
                treeStructure,
                entity => entity.Id,
                entity => entity.ParentId,
                entity => entity.Children,
                (child, parent) => child.Parent = parent,
                entity => entity.Id,
                null,
                new HierarchyEntity()).ToArray();
            Assert.IsNotNull(orderedResult);
            Assert.AreSame(treeStructure[0], orderedResult[0]);
            Assert.AreSame(treeStructure[2], orderedResult[1]);
            Assert.AreSame(treeStructure[4], orderedResult[2]);
            Assert.AreSame(treeStructure[5], orderedResult[3]);
            Assert.AreSame(treeStructure[3], orderedResult[4]);
            Assert.AreSame(treeStructure[1], orderedResult[5]);
        }

        [TestMethod]
        public void Flat()
        {
            var treeStructure = HierarchyEntities().ToArray();
            var orderedResult = _helper.Flat(
                treeStructure,
                entity => entity.Id,
                entity => entity.ParentId,
                entity => entity.Children,
                (child, parent) => child.Parent = parent).ToArray();
            Assert.IsNotNull(orderedResult);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 0), orderedResult[0]);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 1), orderedResult[1]);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 4), orderedResult[2]);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 2), orderedResult[3]);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 5), orderedResult[4]);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 6), orderedResult[5]);
            Assert.AreSame(_hierarchyEntities.Single(x => x.Id == 3), orderedResult[6]);
        }

        private IEnumerable<HierarchyEntity> HierarchyEntities()
        {
            return _helper.ConstructTreeStructure(
                _hierarchyEntities,
                he => he.Id,
                entity => entity.ParentId,
                (child, parent) => child.Parent = parent,
                entity => entity.Children).Where(x => x.ParentId == null).ToArray();
        }
    }

    public class HierarchyEntity : IComparer<int>
    {
        public HierarchyEntity()
        {
            Children = new HashSet<HierarchyEntity>();
        }

        public int Id { get; set; }

        public int? ParentId { get; set; }

        public HierarchyEntity Parent { get; set; }

        public ICollection<HierarchyEntity> Children { get; set; }

        public int Compare(int x, int y)
        {
            return x == y
                       ? 0
                       : x < y
                             ? -1
                             : 1;
        }
    }
}