﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Linq.Expressions;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers;

    [TestClass()]
    public class ExpressionCompositionHelperTests
    {
        [NotNull]
        private ExpressionCompositionHelper _helper;

        [TestInitialize]
        public void Init()
        {
            _helper = new ExpressionCompositionHelper(StaticHelpers.Library);
        }

        public class Parent
        {
            public Child Firstborn { get; set; }
            public ICollection<Child> Children { get; set; } 
        }

        public class Child : IChild
        {
            public Child Element { get; set; }
        }

        public interface IChild
        {
            Child Element { get; set; }
        }

        [TestMethod()]
        public void ComposeTest()
        {
            Expression<Func<Parent, Child>> inner = p => p.Firstborn;
            Expression<Func<Child, Child>> outer = c => c.Element;
            var result = _helper.Compose(inner, outer);
            Expression<Func<Parent, Child>> expected = p => p.Firstborn.Element;
            Assert.IsNotNull(result);
            ExprAssert.AreEqual(expected, result);
        }

        [TestMethod()]
        public void ComposeWithSelectTest()
        {
            Expression<Func<Parent, IEnumerable<Child>>> outer = p => p.Children;
            Expression<Func<Child, object>> inner = c => c.Element;
            var result = _helper.ComposeWithSelect(outer, inner);
            Expression<Func<Parent, object>> expected = p => p.Children.Select<Child, object>(c => c.Element);

            Assert.IsNotNull(result);
            ExprAssert.AreEqual(expected, result);
        }

        [TestMethod()]
        public void ReplaceInterfaceWithImplementationTest()
        {
            Expression<Func<IChild, object>> expr = c => c.Element;
            var result = _helper.ReplaceInterfaceWithImplementation<Child, IChild>(expr);
            Expression<Func<Child, object>> expected = p => p.Element;
            Assert.IsNotNull(result);
            ExprAssert.AreEqual(expected, result);
        }

        [TestMethod]
        public void MakeObjectOutputTest()
        {
            Expression<Func<Child, Child>> expr = c => c.Element;
            Expression<Func<Child, object>> expected = c => c.Element;
            var result = _helper.MakeObjectOutput(expr);
            Assert.IsNotNull(result);
            ExprAssert.AreEqual(expected, result);

            Expression<Func<Parent, IEnumerable<Child>>> expr2 = c => c.Children.Select(cc => cc.Element);
            Expression<Func<Parent, object>> expected2 = c => c.Children.Select(cc => cc.Element);
            var result2 = _helper.MakeObjectOutput(expr2);
            Assert.IsNotNull(result2);
            ExprAssert.AreEqual(expected2, result2);
        }
    }
}
