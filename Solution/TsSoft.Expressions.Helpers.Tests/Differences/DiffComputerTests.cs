﻿namespace TsSoft.Expressions.Helpers.Differences
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.Expressions.Helpers.Differences;
    using TsSoft.Expressions.Models.Differences;

    [TestClass]
    public class DiffComputerTests
    {
        [NotNull]
        private DiffComputer _diffComputer;

        [TestInitialize]
        public void Init()
        {
            _diffComputer = new DiffComputer(StaticHelpers.MemberInfo);
        }

        [TestMethod]
        public void TestDifferingPrimiviteFields()
        {
            var first = new From {Guid = Guid.NewGuid(), Id = 3, Name = "fld"};
            var second = new From {Guid = Guid.NewGuid(), Id = 3, Name = "not_fld"};
            var diff = _diffComputer.GetDiff(first, second);
            Assert.IsNotNull(diff);
            Assert.IsNotNull(diff.PrimitiveDiff<Guid>("Guid"));
            Assert.AreEqual(first.Guid, diff.PrimitiveDiff<Guid>("Guid").First);
            Assert.AreEqual(second.Guid, diff.PrimitiveDiff<Guid>("Guid").Second);
            Assert.IsNotNull(diff.PrimitiveDiff<string>("Name"));
            Assert.AreEqual(first.Name, diff.PrimitiveDiff<string>("Name").First);
            Assert.AreEqual(second.Name, diff.PrimitiveDiff<string>("Name").Second);
            Assert.IsNull(diff.PrimitiveDiff<int>("Id"));
            Assert.IsNull(diff.SubobjectDiff<From>("Parent"));
            Assert.IsNull(diff.CollectionDiff<From>("Children"));
        }

        [TestMethod]
        public void TestDifferingObjects()
        {
            var first = new From
            {
                Guid = Guid.NewGuid(),
                Id = 3,
                Name = "fld",
                Parent = new From {Guid = Guid.NewGuid(), Id = 4, Name = "prn"}
            };
            var second = new From
            {
                Guid = Guid.NewGuid(),
                Id = 3,
                Name = "not_fld",
                Parent = new From {Guid = Guid.NewGuid(), Id = 5, Name = "prn"}
            };
            var diff = _diffComputer.GetDiff(first, second);
            Assert.IsNotNull(diff);
            Assert.IsNotNull(diff.PrimitiveDiff<Guid>("Guid"));
            Assert.AreEqual(first.Guid, diff.PrimitiveDiff<Guid>("Guid").First);
            Assert.AreEqual(second.Guid, diff.PrimitiveDiff<Guid>("Guid").Second);
            Assert.IsNotNull(diff.PrimitiveDiff<string>("Name"));
            Assert.AreEqual(first.Name, diff.PrimitiveDiff<string>("Name").First);
            Assert.AreEqual(second.Name, diff.PrimitiveDiff<string>("Name").Second);
            Assert.IsNull(diff.PrimitiveDiff<int>("Id"));
            var subdiff = diff.SubobjectDiff<From>("Parent");
            Assert.IsNotNull(subdiff);
            Assert.IsNotNull(subdiff.PrimitiveDiff<Guid>("Guid"));
            Assert.AreEqual(first.Parent.Guid, subdiff.PrimitiveDiff<Guid>("Guid").First);
            Assert.AreEqual(second.Parent.Guid, subdiff.PrimitiveDiff<Guid>("Guid").Second);
            Assert.IsNotNull(subdiff.PrimitiveDiff<int>("Id"));
            Assert.AreEqual(first.Parent.Id, subdiff.PrimitiveDiff<int>("Id").First);
            Assert.AreEqual(second.Parent.Id, subdiff.PrimitiveDiff<int>("Id").Second);
            Assert.IsNull(subdiff.PrimitiveDiff<int>("Name"));
            Assert.IsNull(subdiff.SubobjectDiff<From>("Parent"));
            Assert.IsNull(subdiff.CollectionDiff<string>("Children"));
            Assert.IsNull(diff.CollectionDiff<From>("Children"));
        }

        [TestMethod]
        public void TestDifferingCollections()
        {
            var first = new From
            {
                Children =
                    new[]
                    {
                        new From {Id = 1, Name = "unchanged"}, new From {Id = 2, Name = "changed"},
                        new From {Id = 3, Name = "deleted"}
                    }
            };
            var second = new From
            {
                Children =
                    new[]
                    {
                        new From {Id = 1, Name = "unchanged"}, new From {Id = 2, Name = "CHANGED"},
                        new From {Id = 4, Name = "added"}
                    }
            };
            var diff = _diffComputer.GetDiff(first, second, new FromProvider());
            Assert.IsNotNull(diff);
            var coldiff = diff.CollectionDiff(f => f.Children);
            Assert.IsNotNull(coldiff);
            Assert.AreEqual(1, coldiff.Diffs.Count);
            var objDiff = coldiff.Diffs.First();
            Assert.AreSame(first.Children.Single(c => c.Id == 2), objDiff.Object);
            Assert.AreEqual("changed", objDiff.Diff.PrimitiveDiff<string>("Name").First);
            Assert.AreEqual("CHANGED", objDiff.Diff.PrimitiveDiff<string>("Name").Second);
            Assert.AreEqual(1, coldiff.FirstExcess.Count);
            Assert.AreSame(first.Children.Single(c => c.Id == 3), coldiff.FirstExcess.First());
            Assert.AreEqual(1, coldiff.SecondExcess.Count);
            Assert.AreSame(second.Children.First(c => c.Id == 4), coldiff.SecondExcess.First());
        }


        public void TestDifferingCollectionsWithParentReference()
        {
            var first = new From
            {
                Children =
                    new[]
                    {
                        new From {Id = 1, Name = "unchanged"}, new From {Id = 2, Name = "changed"},
                        new From {Id = 3, Name = "deleted"}
                    }
            };
            var second = new From
            {
                Children =
                    new[]
                    {
                        new From {Id = 1, Name = "unchanged"}, new From {Id = 2, Name = "CHANGED"},
                        new From {Id = 4, Name = "added"}
                    }
            };
            foreach (var child in first.Children)
            {
                child.Parent = first;
            }
            foreach (var child in second.Children)
            {
                child.Parent = second;
            }

            var diff = _diffComputer.GetDiff(first, second, new FromProvider());
            Assert.IsNotNull(diff);
            var coldiff = diff.CollectionDiff(f => f.Children);
            Assert.IsNotNull(coldiff);
            Assert.AreEqual(2, coldiff.Diffs.Count);
            var objDiff = coldiff.Diffs.First(c => c.Object.Id == 1);
            Assert.AreSame(first.Children.Single(c => c.Id == 1), objDiff.Object);
            Assert.AreSame(diff, objDiff.Diff.SubobjectDiff(f => f.Parent));
            objDiff = coldiff.Diffs.First(c => c.Object.Id == 2);
            Assert.AreSame(first.Children.Single(c => c.Id == 2), objDiff.Object);
            Assert.AreSame(diff, objDiff.Diff.SubobjectDiff(f => f.Parent));
            Assert.AreEqual("changed", objDiff.Diff.PrimitiveDiff<string>("Name").First);
            Assert.AreEqual("CHANGED", objDiff.Diff.PrimitiveDiff<string>("Name").Second);
            Assert.AreEqual(1, coldiff.FirstExcess.Count);
            Assert.AreSame(first.Children.Single(c => c.Id == 3), coldiff.FirstExcess.First());
            Assert.AreEqual(1, coldiff.SecondExcess.Count);
            Assert.AreSame(second.Children.First(c => c.Id == 4), coldiff.SecondExcess.First());
        }

        [TestMethod]
        public void TestDifferingPrimitiveCollections()
        {
            var firstPrim = new PrimitiveCollectionContainer {Id = 1, Array = new[] {1, 2, 3}};
            var secondPrim = new PrimitiveCollectionContainer {Id = 2, Array = new[] {5, 3, 2, 4}};
            var primDiff = _diffComputer.GetDiff(firstPrim, secondPrim);
            Assert.IsNotNull(primDiff);
            Assert.AreEqual(firstPrim.Id, primDiff.PrimitiveDiff(c => c.Id).First);
            Assert.AreEqual(secondPrim.Id, primDiff.PrimitiveDiff(c => c.Id).Second);
            var primColDiff = primDiff.CollectionDiff(c => c.Array);
            Assert.AreEqual(1, primColDiff.FirstExcess.Count);
            Assert.IsTrue(primColDiff.FirstExcess.Contains(1));
            Assert.AreEqual(2, primColDiff.SecondExcess.Count);
            Assert.IsTrue(primColDiff.SecondExcess.Contains(4));
            Assert.IsTrue(primColDiff.SecondExcess.Contains(5));
        }

        private class FromProvider : IEqualityComparerProvider
        {
            public IEqualityComparer<T> GetEqualityComparer<T>()
            {
                if (typeof(T) == typeof(From))
                {
                    return EntityComparer<From, int>.Instance as IEqualityComparer<T>;
                }
                return null;
            }
        }

        private class PrimitiveCollectionContainer
        {
            public int Id { get; set; }

            public int[] Array { get; set; }
        }
    }
}
