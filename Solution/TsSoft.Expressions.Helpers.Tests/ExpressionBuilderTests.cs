﻿namespace TsSoft.Expressions.Helpers
{
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.Models.Path;

    [TestClass()]
    public class ExpressionBuilderTests
    {
        [NotNull]
        private IExpressionBuilder _builder;

        [TestInitialize]
        public void Init()
        {
            _builder = new ExpressionBuilder(StaticHelpers.MemberInfo, StaticHelpers.Library);
        }

        [TestMethod()]
        public void BuildPathTest()
        {
            Expression<Func<Test1, object>> expression =
                t => t.Coll.Select(tt => tt.Prop.Coll.Select(ttt => ttt.Coll.Select(tttt => tttt.Prop)));
            var properties = new FlatPathParserVisitor().GetMemberCollection(expression);
            var expr = _builder.BuildIncludeByPath<Test1>(properties);
            ExprAssert.AreEqual(expression, expr);

            var untypedExpr = _builder.BuildIncludeByPath(typeof(Test1), properties);
            ExprAssert.AreEqual(expression, untypedExpr);
        }

        [TestMethod]
        public void BuildPropertyExpressionTest()
        {
            var prop = typeof(Test1).GetProperty("Int");
            var prop2 = typeof(Test1).GetProperty("Prop");
            var prop3 = typeof(Test1).GetProperty("Coll");

            Expression<Func<Test1, int>> expected = e => e.Int;
            Expression<Func<Test1, Test1>> expected2 = e => e.Prop;
            Expression<Func<Test1, ICollection<Test2>>> expected3 = e => e.Coll;
            var res = _builder.BuildPropertyExpression(prop);
            ExprAssert.AreEqual(expected, res);
            res = _builder.BuildPropertyExpression(prop2);
            ExprAssert.AreEqual(expected2, res);
            res = _builder.BuildPropertyExpression(prop3);
            ExprAssert.AreEqual(expected3, res);

            res = _builder.BuildPropertyExpression(typeof(Test1), "Int");
            ExprAssert.AreEqual(expected, res);
            res = _builder.BuildPropertyExpression(typeof(Test1), "Prop");
            ExprAssert.AreEqual(expected2, res);
            res = _builder.BuildPropertyExpression(typeof(Test1), "Coll");
            ExprAssert.AreEqual(expected3, res);

            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildPropertyExpression(typeof(Test1), "NoSuchProperty"));
        }

        [TestMethod]
        public void BuildWhereContainsTest()
        {
            var prop = typeof(Test1).GetProperty("Int");
            var prop2 = typeof(Test1).GetProperty("Prop");
            var prop3 = typeof(Test1).GetProperty("Coll");
            var prop4 = typeof(Test1).GetProperty("String");
            Expression<Func<Test1, bool>> expected;

            expected = t => t.Int.ToString().Contains("Cont");
            expected = MethodReplacerVisitor.Replace<object, int>(expected, "ToString") as Expression<Func<Test1, bool>>;
            var res = _builder.BuildWhereContains<Test1>(prop, "Cont");
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereContains<Test1>("Int", "Cont");
            ExprAssert.AreEqual(res, expected);

            expected = t => t.Prop.ToString().Contains("Cont2");
            res = _builder.BuildWhereContains<Test1>(prop2, "Cont2");
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereContains<Test1>("Prop", "Cont2");
            ExprAssert.AreEqual(res, expected);

            expected = t => t.String.Contains("ContZ");
            res = _builder.BuildWhereContains<Test1>(prop4, "ContZ");
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereContains<Test1>("String", "ContZ");
            ExprAssert.AreEqual(res, expected);

            expected = t => false;
            res = _builder.BuildWhereAnyContains<Test1>(new PropertyInfo[0], "str");
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereAnyContains<Test1>(new string[0], "str");
            ExprAssert.AreEqual(res, expected);

            expected = t => t.Int.ToString().Contains("123");
            expected = MethodReplacerVisitor.Replace<object, int>(expected, "ToString") as Expression<Func<Test1, bool>>;
            res = _builder.BuildWhereAnyContains<Test1>(new PropertyInfo[] { prop }, "123");
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereAnyContains<Test1>(new string[] { prop.Name }, "123");
            ExprAssert.AreEqual(res, expected);

            expected = t => t.Prop.ToString().Contains("str") ||
                            t.String.Contains("str");
            res = _builder.BuildWhereAnyContains<Test1>(new PropertyInfo[] { prop2, prop4, }, "str");
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereAnyContains<Test1>(new string[] { prop2.Name, prop4.Name }, "str");
            ExprAssert.AreEqual(res, expected);

            expected = t => t.Prop.ToString().Contains("str") ||
                            t.Coll.ToString().Contains("str") ||
                            t.String.Contains("str") ||
                            t.Prop.ToString().Contains("str");
            res = _builder.BuildWhereAnyContains<Test1>(new PropertyInfo[] { prop2, prop3, prop4, prop2 }, "str");
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereAnyContains<Test1>(new string[] { prop2.Name, prop3.Name, prop4.Name, prop2.Name }, "str");
            ExprAssert.AreEqual(res, expected);
        }

        [TestMethod]
        public void BuildWhereEqualsTest()
        {
            var prop = typeof(Test1).GetProperty("Int");
            var prop4 = typeof(Test1).GetProperty("String");
            Expression<Func<Test1, bool>> res;
            Expression<Func<Test1, bool>> expected;

            expected = t => t.Int == 2;
            res = _builder.BuildWhereEquals<Test1, int>(prop, 2);
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereEquals<Test1, int>(prop.Name, 2);
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereEquals<Test1>(prop, 2);
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereEquals<Test1>(prop.Name, 2);
            ExprAssert.AreEqual(res, expected);
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1, string>(prop, "2"));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1, long>(prop, 2L));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1, string>(prop.Name, "2"));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1, long>(prop.Name, 2L));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1>(prop, "2"));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1>(prop, 2L));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1>(prop.Name, "2"));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1>(prop.Name, 2L));

            expected = t => t.String == "2";
            res = _builder.BuildWhereEquals<Test1, string>(prop4, "2");
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereEquals<Test1, string>(prop4.Name, "2");
            ExprAssert.AreEqual(res, expected);
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1, int>(prop4, 2));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1, int>(prop4.Name, 2));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1>(prop4, 2));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1>(prop4.Name, 2));

            expected = t => t.Int == 3 && t.String == "3";
            res = _builder.BuildWhereEquals<Test1>(new Dictionary<PropertyInfo, object>
                {
                    {prop, 3},
                    {prop4, "3"},
                });
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereEquals<Test1>(new Dictionary<string, object>
                {
                    {prop.Name, 3},
                    {prop4.Name, "3"},
                });
            ExprAssert.AreEqual(res, expected);
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1>(new Dictionary<PropertyInfo, object>
                {
                    {prop, "3"},
                    {prop4, 3},
                }));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1>(new Dictionary<string, object>
                {
                    {prop.Name, "3"},
                    {prop4.Name, 3},
                }));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1>(new Dictionary<PropertyInfo, object>
                {
                    {prop, 3},
                    {prop4, 3},
                }));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereEquals<Test1>(new Dictionary<string, object>
                {
                    {prop.Name, 3},
                    {prop4.Name, 3},
                }));
        }

        [TestMethod]
        public void BuildWhereInTest()
        {
            var prop = typeof(Test1).GetProperty("Int");
            var prop4 = typeof(Test1).GetProperty("String");
            Expression<Func<Test1, bool>> res;
            Expression<Func<Test1, bool>> expected;

            expected = t => false;
            res = _builder.BuildWhereIn<Test1, int>(prop, new int[0]);
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereIn<Test1>(prop, new object[0]);
            ExprAssert.AreEqual(res, expected);

            expected = t => t.Int == 1 || t.Int == 2;
            res = _builder.BuildWhereIn<Test1, int>(prop, new int[2] {1,2});
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereIn<Test1>(prop, new object[2] {1,2});
            ExprAssert.AreEqual(res, expected);
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereIn<Test1>(prop, new object[2] {1, "2"}));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereIn<Test1, string>(prop, new string[2] { "1", "2" }));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereIn<Test1>(prop, new object[3] { 1, null, "2" }));

            expected = t => t.String == "1" || t.String == "2";
            res = _builder.BuildWhereIn<Test1, string>(prop4, new string[2] { "1", "2" });
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereIn<Test1>(prop4, new object[2] { "1", "2" });
            ExprAssert.AreEqual(res, expected);
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereIn<Test1>(prop4, new object[2] { 1, "2" }));
            ExceptAssert.Throws<ArgumentException>(() => _builder.BuildWhereIn<Test1, int>(prop4, new int[2] { 1, 2 }));

            expected = t => t.String == "1" || t.String == null || t.String == "2";
            expected = NullConstantTypeReplacerVisitor.Replace<string>(expected) as Expression<Func<Test1, bool>>;
            expected = NullConstantComparisonMethodReplacerVisitor.ReplaceWithNull(expected) as Expression<Func<Test1, bool>>;
            res = _builder.BuildWhereIn<Test1, string>(prop4, new string[] { "1", null, "2" });
            ExprAssert.AreEqual(res, expected);
            res = _builder.BuildWhereIn<Test1>(prop4, new object[] { "1", null, "2" });
            ExprAssert.AreEqual(res, expected);
        }

        [TestMethod]
        public void BuildGetByPathWithNullChecksTest()
        {
            Expression<Func<Test1, object>> expression =
                t => t.Prop.Coll.Select(tt => tt.Prop.Coll.Select(ttt => ttt.Coll.Select(tttt => tttt.Prop)));
            var properties = new FlatPathParserVisitor().GetMemberCollection(expression);
            var builder = new ExpressionBuilder(new MemberInfoHelper(), new MemberInfoLibrary(new MemberInfoHelper()));
            var param = Expression.Parameter(typeof (Test1));
            var expr = builder.BuildPathWithNullChecks(param, properties);
            Expression<Func<Test1, IEnumerable<Test1>>> expected =
                t => t != (Test1)null
                    ? t.Prop != (Test1)null
                        ? t.Prop.Coll != (ICollection<Test2>)null
                            ? t.Prop.Coll
                                .Select(tt => tt != (Test2)null ? tt.Prop : default(Test2))
                                .SelectMany(ttt => ttt != (Test2)null ? ttt.Coll : Enumerable.Empty<Test3>())
                                .SelectMany(tttt => tttt != (Test3)null ? tttt.Coll : Enumerable.Empty<Test3>())
                                .Select(ttttt => ttttt != (Test3)null ? ttttt.Prop : default(Test1))
                            : default(IEnumerable<Test1>)
                        : default(IEnumerable<Test1>)
                    : default(IEnumerable<Test1>);
            var lambda = Expression.Lambda<Func<Test1, IEnumerable<Test1>>>(expr, param);
            ExprAssert.AreEqual(lambda, expected);
        }

        [TestMethod]
        public void ForTest()
        {
            var builder = new ExpressionBuilder(StaticHelpers.MemberInfo, StaticHelpers.Library);
            var to = new SimpleTo();
            var prop = StaticHelpers.MemberInfo.GetPropertyInfo(() => to.Id);
            var propExpr = Expression.Property(Expression.Constant(to), prop);
            var @for = builder.For(
                Expression.Assign(propExpr, Expression.Constant(0)),
                Expression.LessThanOrEqual(propExpr, Expression.Constant(10)),
                Expression.Assign(propExpr, Expression.Add(propExpr, Expression.Constant(3))),
                null);
            var lambda = Expression.Lambda<Action>(@for);
            lambda.Compile()();
            Assert.AreEqual(12, to.Id);
        }

        [TestMethod]
        public void ForRangeTest()
        {
            var builder = new ExpressionBuilder(StaticHelpers.MemberInfo, StaticHelpers.Library);
            var array = new Test1[] { new Test1 { Int = 1 }, new Test1 { Int = 2 }, new Test1 { Int = 3 } };
            var forVar = Expression.Variable(typeof(int));
            var body = Expression.Assign(
                Expression.Property(Expression.ArrayAccess(Expression.Constant(array), forVar), "Int"),
                Expression.Add(Expression.Property(Expression.ArrayAccess(Expression.Constant(array), forVar), "Int"),
                Expression.Constant(1)));
            var forRange = builder.ForRange(forVar, Expression.Constant(0), Expression.Constant(3), body);
            var lambda = Expression.Lambda<Action>(forRange);
            lambda.Compile()();
            Assert.AreEqual(2, array[0].Int);
            Assert.AreEqual(3, array[1].Int);
            Assert.AreEqual(4, array[2].Int);
        }

        [TestMethod]
        public void ForeachTest()
        {
            var builder = new ExpressionBuilder(StaticHelpers.MemberInfo, StaticHelpers.Library);
            var array = new Test1[] { new Test1 { Int = 1 }, new Test1 { Int = 2 }, new Test1 { Int = 3 } };
            var current = Expression.Variable(typeof(Test1), "current");
            var body = Expression.Assign(Expression.Property(current, "Int"), Expression.Add(Expression.Property(current, "Int"), Expression.Constant(1)));
            var @foreach = builder.Foreach<Test1>(Expression.Constant(array), current, body);
            var lambda = Expression.Lambda<Action>(@foreach);
            lambda.Compile()();
            Assert.AreEqual(2, array[0].Int);
            Assert.AreEqual(3, array[1].Int);
            Assert.AreEqual(4, array[2].Int);
        }

        [TestMethod]
        public void ForeachDeferredTest()
        {
            var builder = new ExpressionBuilder(StaticHelpers.MemberInfo, StaticHelpers.Library);
            var array = new Test1[] { new Test1 { Int = 1 }, new Test1 { Int = 2 }, new Test1 { Int = 3 } };
            var current = Expression.Variable(typeof(Test1), "current");
            var list = Expression.Variable(typeof(List<Action>), "actions");
            var addMethod = StaticHelpers.MemberInfo.GetMethodInfo((List<Action> l) => l.Add(null));
            var body = Expression.Call(
                list,
                addMethod,
                new Expression[]
                {
                    Expression.Lambda<Action>(Expression.AddAssign(Expression.Property(current, "Int"), Expression.Constant(1)))
                });
            var @foreachProper = builder.Foreach<Test1>(Expression.Constant(array), current, body);
            var @foreach = Expression.Lambda<Func<List<Action>>>(
                Expression.Block(
                    new ParameterExpression[] {list},
                    Expression.Assign(list, Expression.New(list.Type)),
                    @foreachProper,
                    list));
            @foreach.Compile()().ForEach(a => a());
            Assert.AreEqual(2, array[0].Int);
            Assert.AreEqual(3, array[1].Int);
            Assert.AreEqual(4, array[2].Int);
        }


        [TestMethod]
        public void WhereNotNullTest()
        {
            Expression<Func<Test1, object>> lambda = t => t.Coll;
            Expression<Func<Test1, object>> expectedLambda = t => t.Coll.Where(e => e != null);
            var result = _builder.WhereNotNull(lambda.Body);
            ExprAssert.AreEqual(expectedLambda.Body, result, new Dictionary<ParameterExpression, ParameterExpression>
                {
                    { expectedLambda.Parameters[0], lambda.Parameters[0] },
                });

            lambda = t => t.Coll.Select(c => c.Prop);
            expectedLambda = t => t.Coll.Select(c => c.Prop).Where(e => e != null);
            result = _builder.WhereNotNull(lambda.Body);
            ExprAssert.AreEqual(expectedLambda.Body, result, new Dictionary<ParameterExpression, ParameterExpression>
                {
                    { expectedLambda.Parameters[0], lambda.Parameters[0] },
                });

            lambda = t => t.Prop;
            ExceptAssert.Throws<ArgumentException>(() => _builder.WhereNotNull(lambda.Body));
            ExceptAssert.Throws<ArgumentException>(() => _builder.WhereNotNull(lambda));
        }

        [NotNull]
        private ParsedPath Parse<T>(Expression<Func<Test1, T>> path)
        {
            return StaticHelpers.PathParser.Parse(path);
        }

        [TestMethod]
        public void TestBuildPreconditionForPathWithSelect()
        {
            var path = Parse(t => t.Prop.Coll.Select(c => c.Prop));
            var result = _builder.BuildPreconditionExpression(path, false);
            Expression<Func<Test1, bool>> expected = t => t != null && t.Prop != null && t.Prop.Coll != null;
            ExprAssert.AreEqual(expected.Body, result, new Dictionary<ParameterExpression, ParameterExpression> {{expected.Parameters[0], path.Start}});
        }

        [TestMethod]
        public void TestBuildPreconditionForPathWithoutSelect()
        {
            var path = Parse(t => t.Prop.Prop.Coll);
            var result = _builder.BuildPreconditionExpression(path, false);
            Expression<Func<Test1, bool>> expected = t => t != null && t.Prop != null && t.Prop.Prop != null;
            ExprAssert.AreEqual(expected.Body, result, new Dictionary<ParameterExpression, ParameterExpression> { { expected.Parameters[0], path.Start } });
        }

        [TestMethod]
        public void TestBuildPreconditionForEmptyPath()
        {
            var path = Parse(t => t);
            var result = _builder.BuildPreconditionExpression(path, false);
            Assert.IsNull(result);
        }

        [TestMethod]
        public void TestBuildPreconditionForPathWithSelectAndContinuation()
        {
            var path = Parse(t => t.Prop.Coll.Select(c => c.Prop).SelectMany(c => c.Coll));
            var result = _builder.BuildPreconditionExpression(path, false);
            Expression<Func<Test1, bool>> expected = t => t != null && t.Prop != null && t.Prop.Coll != null;
            ExprAssert.AreEqual(expected.Body, result, new Dictionary<ParameterExpression, ParameterExpression> { { expected.Parameters[0], path.Start } });
        }

        public class Test1
        {
            public int Int { get; set; }

            public Test1 Prop { get; set; }

            public ICollection<Test2> Coll { get; set; }

            public string String { get; set; }
        }

        public class Test2
        {
            public Test2 Prop { get; set; }

            public ICollection<Test3> Coll { get; set; }
        }

        public class Test3
        {
            public ICollection<Test3> Coll { get; set; }

            public Test1 Prop { get; set; }
        }

        public class NullConstantComparisonMethodReplacerVisitor : ExpressionVisitor
        {
            public static Expression ReplaceWithNull([NotNull] Expression expression)
            {
                return new NullConstantComparisonMethodReplacerVisitor().Visit(expression);
            }

            private bool IsNullConstant(Expression node)
            {
                var nodeConstant = node as ConstantExpression;
                return nodeConstant != null && nodeConstant.Value == null;
            }

            protected override Expression VisitBinary(BinaryExpression node)
            {
                if ((node.NodeType == ExpressionType.Equal || node.NodeType == ExpressionType.NotEqual)
                    && (IsNullConstant(node.Left) || IsNullConstant(node.Right)))
                {
                    return Expression.Equal(node.Left, node.Right, node.IsLiftedToNull, null);
                }
                return base.VisitBinary(node);
            }
        }

        public class NullConstantTypeReplacerVisitor : ExpressionVisitor
        {
            public static Expression Replace<T>([NotNull]Expression expression)
            {
                return new NullConstantTypeReplacerVisitor(typeof(T)).Visit(expression);
            }

            [NotNull]
            private readonly Type _desiredType;

            public NullConstantTypeReplacerVisitor([NotNull]Type desiredType)
            {
                _desiredType = desiredType;
            }

            protected override Expression VisitConstant(ConstantExpression node)
            {
                if (node.Value == null)
                {
                    return Expression.Constant(null, _desiredType);
                }
                return base.VisitConstant(node);
            }
        }

        public class MethodReplacerVisitor : ExpressionVisitor
        {
            public static Expression Replace<TBase, TDerived>([NotNull]Expression expression, [NotNull]string methodName)
                where TDerived: TBase
            {
                return new MethodReplacerVisitor(methodName, typeof(TDerived), typeof(TBase)).Visit(expression);
            }

            [NotNull]
            private readonly string _methodName;
            [NotNull]
            private readonly Type _from;
            [NotNull]
            private readonly Type _to;

            private MethodReplacerVisitor([NotNull]string methodName, [NotNull]Type @from, [NotNull]Type to)
            {
                _methodName = methodName;
                _from = @from;
                _to = to;
            }

            protected override Expression VisitMethodCall(MethodCallExpression node)
            {
                if (node.Object != null && node.Object.Type == _from && node.Method.Name.Equals(_methodName, StringComparison.Ordinal))
                {
                    return Expression.Call(
                        Visit(node.Object),
                        _to.GetMethod(node.Method.Name, node.Arguments.Select(a => a.Type).ToArray()),
                        node.Arguments.Select(Visit));
                }
                return base.VisitMethodCall(node);
            }
        }
    }
}
