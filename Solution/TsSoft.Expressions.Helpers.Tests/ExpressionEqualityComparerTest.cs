﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections;
    using System.Linq;
    using System.Linq.Expressions;
    using BaseTestClasses;
    using Helpers.Reflection;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ExpressionEqualityComparerTest
    {
        [TestMethod]
        public void TestOnePrimitive()
        {
            Expression<Func<TestEntity, object>> actual;
            Expression<Func<TestEntity, object>> expected;

            actual = e => new TestEntity
            {
                Prop1 = e.Prop1,
            };

            expected = e => new TestEntity
            {
                Prop1 = e.Prop1,
            };
            Assert.IsTrue(ExpressionEqualityComparer.TypeUnawareIgnoreOrderInstance.Equals(expected, actual));
        }

        [TestMethod]
        public void TestTwoPrimitives()
        {
            Expression<Func<TestEntity, object>> actual;
            Expression<Func<TestEntity, object>> expected;

            actual = e => new TestEntity
            {
                Prop2 = e.Prop2,
                Prop1 = e.Prop1,
            };

            expected = e => new TestEntity
            {
                Prop1 = e.Prop1,
                Prop2 = e.Prop2,
            };
            Assert.IsTrue(ExpressionEqualityComparer.TypeUnawareIgnoreOrderInstance.Equals(expected, actual));

        }

        [TestMethod]
        public void TestPrimitiveAndObject()
        {
            Expression<Func<TestEntity, object>> actual;
            Expression<Func<TestEntity, object>> expected;

            actual = e => new TestEntity
            {
                Prop1 = e.Prop1,
                Parent = new TestEntity
                {
                    Prop2 = e.Parent.Prop2,
                    Prop1 = e.Parent.Prop1,
                }
            };

            expected = e => new TestEntity
            {
                Prop1 = e.Prop1,
                Parent = new TestEntity
                {
                    Prop1 = e.Parent.Prop1,
                    Prop2 = e.Parent.Prop2,
                }
            };
            Assert.IsTrue(ExpressionEqualityComparer.TypeUnawareIgnoreOrderInstance.Equals(expected, actual));

        }

        [TestMethod]
        public void TestPrimitiveAndObjectWithCollection()
        {
            Expression<Func<TestEntity, object>> actual;
            Expression<Func<TestEntity, object>> expected;

            actual = e => new TestEntity
            {
                Prop1 = e.Prop1,
                Parent = new TestEntity
                {
                    Children = e.Children.Select(
                        c => new TestEntity
                        {
                            Prop2 = c.Prop2,
                            Prop1 = c.Prop1,
                        }).ToList(),
                    Prop2 = e.Parent.Prop2,
                    Prop1 = e.Parent.Prop1,
                },

            };

            expected = e => new TestEntity
            {
                Prop1 = e.Prop1,
                Parent = new TestEntity
                {
                    Prop1 = e.Parent.Prop1,
                    Prop2 = e.Parent.Prop2,
                    Children = e.Children.Select(
                        c => new TestEntity
                        {
                            Prop1 = c.Prop1,
                            Prop2 = c.Prop2,
                        }).ToList(),
                }
            };
            Assert.IsTrue(ExpressionEqualityComparer.TypeUnawareIgnoreOrderInstance.Equals(expected, actual));
        }

        [TestMethod]
        public void TestNotEqualPrimitives()
        {
            // notEquals
            Expression<Func<TestEntity, object>> actual;
            Expression<Func<TestEntity, object>> expected;

            actual = e => new TestEntity
            {
                Prop1 = e.Prop1,
            };
            expected = e => new TestEntity
            {
                Prop3 = e.Prop3,
            };
            Assert.IsFalse(ExpressionEqualityComparer.TypeUnawareIgnoreOrderInstance.Equals(expected, actual));
        }

        [TestMethod]
        public void TestPrimitiveAndNotEqualObject()
        {
            Expression<Func<TestEntity, object>> actual;
            Expression<Func<TestEntity, object>> expected;

            actual = e => new TestEntity
            {
                Prop1 = e.Prop1,
                Parent = new TestEntity
                {
                    Prop2 = e.Parent.Prop2,
                    Prop1 = e.Parent.Prop1,
                }
            };

            expected = e => new TestEntity
            {
                Prop1 = e.Prop1,
                Parent = new TestEntity
                {
                    Prop1 = e.Parent.Prop1,
                }
            };
            Assert.IsFalse(ExpressionEqualityComparer.TypeUnawareIgnoreOrderInstance.Equals(expected, actual));
        }

        [TestMethod]
        public void TestPrimitiveAndNotEqualCollection()
        {
            Expression<Func<TestEntity, object>> actual;
            Expression<Func<TestEntity, object>> expected;

            actual = e => new TestEntity
            {
                Prop1 = e.Prop1,
                Children = e.Children.Select(
                    c => new TestEntity
                    {
                        Prop1 = c.Prop1,
                    }).ToList()
            };

            expected = e => new TestEntity
            {
                Prop1 = e.Prop1,
                Children = e.Children != null
                    ? e.Children.Select(
                        c => new TestEntity
                        {
                            Prop1 = c.Prop1,
                        }).ToList()
                    : null
            };
            Assert.IsFalse(ExpressionEqualityComparer.TypeUnawareIgnoreOrderInstance.Equals(expected, actual));
        }

        [TestMethod]
        public void TestNotEqualDeepObjects()
        {
            Expression<Func<TestEntity, object>> actual;
            Expression<Func<TestEntity, object>> expected; 
            
            actual = e => new TestEntity
            {
                Prop1 = e.Prop1,
                Parent = new TestEntity
                {
                    Prop1 = e.Parent.Prop1,
                    Prop2 = e.Parent.Prop2,
                    Parent = e != null
                                 ? e.Parent != null
                                       ? e.Parent.Parent != null
                                             ? new TestEntity
                                                 {
                                                     Prop1 = e.Parent.Parent.Prop1
                                                 }
                                             : null
                                       : null
                                 : null,
                }
            };

            expected = e => new TestEntity
            {
                Prop1 = e.Prop1,
                Parent = new TestEntity
                {
                    Prop1 = e.Parent.Prop1,
                    Prop2 = e.Parent.Prop2,
                    Parent = e != null
                                 ? e.Parent != null
                                       ? e.Parent.Parent != null
                                             ? new TestEntity
                                             {
                                                 Prop2 = e.Parent.Parent.Prop2
                                             }
                                             : null
                                       : null
                                 : null
                }
            };
            Assert.IsFalse(ExpressionEqualityComparer.TypeUnawareIgnoreOrderInstance.Equals(expected, actual));
        }

        [TestMethod]
        public void TestDeepConstantComputing()
        {
            var obj = new { Child = new { Int = 1 } };
            Expression<Func<int>> e1 = () => 1;
            Expression<Func<int>> e2 = () => obj.Child.Int;
            Assert.IsTrue(ExpressionEqualityComparer.ConstantComputingInstance.Equals(e1, e2));
        }

        [TestMethod]
        public void TestCollectionElementConstantComparer()
        {
            var iter1 = new[] { 1, 2, 3 }.Select(i => i);
            var iter2 = new[] { 1, 2, 3 }.Select(i => i);
            var iter3 = new[] { 1, 3, 3 }.Select(i => i);
            Expression<Func<bool>> e1 = () => iter1.Contains(1);
            Expression<Func<bool>> e2 = () => iter2.Contains(1);
            Expression<Func<bool>> e3 = () => iter3.Contains(1);
            ExpressionDifference diff;
            var result = ExpressionEqualityComparer.ConstantComputingInstance.AreEqual(e1, e2, new ExpressionEqualityComparer.Context
            {
                ConstantComparer = ExpressionEqualityComparer.Context.CollectionElementComparer
            }, out diff);
            Assert.IsTrue(result);

            result = ExpressionEqualityComparer.ConstantComputingInstance.AreEqual(e1, e3, new ExpressionEqualityComparer.Context
            {
                ConstantComparer = ExpressionEqualityComparer.Context.CollectionElementComparer
            }, out diff);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestCollectionElementDeepConstantComparer()
        {
            var iter1 = new[] { new[] { 1, 2 }, new[] { 3 } }.Select(i => i);
            var iter2 = new[] { new[] { 1, 2 }, new[] { 3 } }.Select(i => i);
            var iter3 = new[] { new[] { 1, 3 }, new[] { 3 } }.Select(i => i);
            Expression<Func<bool>> e1 = () => iter1.Any();
            Expression<Func<bool>> e2 = () => iter2.Any();
            Expression<Func<bool>> e3 = () => iter3.Any();
            ExpressionDifference diff;
            var result = ExpressionEqualityComparer.ConstantComputingInstance.AreEqual(e1, e2, new ExpressionEqualityComparer.Context
            {
                ConstantComparer = ExpressionEqualityComparer.Context.CollectionElementComparer
            }, out diff);
            Assert.IsTrue(result);

            result = ExpressionEqualityComparer.ConstantComputingInstance.AreEqual(e1, e3, new ExpressionEqualityComparer.Context
            {
                ConstantComparer = ExpressionEqualityComparer.Context.CollectionElementComparer
            }, out diff);
            Assert.IsFalse(result);
        }
    }
}
