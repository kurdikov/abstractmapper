﻿namespace TsSoft.Expressions.Helpers.Closures
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;

#if NETCORE
    [Ignore("Expression<TDelegate>.CompileToMethod is missing in .net standard")]
#endif
    [TestClass]
    public class MethodBuilderLambdaCompilerTests
    {
        [NotNull] private MethodBuilderLambdaCompiler _compiler;

        [TestInitialize]
        public void Init()
        {
            _compiler = new MethodBuilderLambdaCompiler(StaticHelpers.MemberInfo);
        }

        [TestMethod]
        public void WithoutConstants()
        {
            Expression<Func<TestEntity, TestEntity>> expr = te => new TestEntity
            {
                Prop1 = te.Prop1,
                Prop2 = "12",
                Prop3 = 1,
                External = new ExternalEntity(),
            };
            var result = _compiler.Compile(expr);
            var product = result(new TestEntity {Prop1 = 2});
            Assert.IsNotNull(product);
            Assert.AreEqual(2, product.Prop1);
            Assert.AreEqual("12", product.Prop2);
            Assert.AreEqual(1, product.Prop3);
            Assert.IsNotNull(product.External);
        }

        [TestMethod]
        public void OneConstant()
        {
            var te = new TestEntity { Prop1 = 1, Prop2 = "2", Prop3 = 3 };
            Expression<Func<TestEntity>> expr = Expression.Lambda<Func<TestEntity>>(
                Expression.MemberInit(
                    Expression.New(typeof(TestEntity)),
                    Expression.Bind(
                        typeof(TestEntity).GetTypeInfo().GetProperty("Prop1"),
                        Expression.Property(
                            Expression.Constant(te),
                            "Prop1")),
                    Expression.Bind(
                        typeof(TestEntity).GetTypeInfo().GetProperty("Prop2"),
                        Expression.Constant("12"))));
            var result = _compiler.Compile(expr);
            var product = result();
            Assert.IsNotNull(product);
            Assert.AreEqual(te.Prop1, product.Prop1);
            Assert.AreEqual("12", product.Prop2);
        }

        [TestMethod]
        public void TwoConstants()
        {
            var te = new TestEntity { Prop1 = 1, Prop2 = "2", Prop3 = 3 };
            var te2 = new TestEntity { Prop1 = 11, Prop2 = "22", Prop3 = 33 };
            Expression<Func<TestEntity>> expr = Expression.Lambda<Func<TestEntity>>(
                Expression.MemberInit(
                    Expression.New(typeof(TestEntity)),
                    Expression.Bind(
                        typeof(TestEntity).GetProperty("Prop1"),
                        Expression.Property(
                            Expression.Constant(te),
                            "Prop1")),
                    Expression.Bind(
                        typeof(TestEntity).GetProperty("Prop2"),
                        Expression.Property(
                            Expression.Constant(te2),
                            "Prop2"))));
            var result = _compiler.Compile(expr);
            var product = result();
            Assert.IsNotNull(product);
            Assert.AreEqual(te.Prop1, product.Prop1);
            Assert.AreEqual(te2.Prop2, product.Prop2);
        }
    }
}
