﻿namespace TsSoft.Expressions.Helpers.Closures
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers.Closures;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Helpers.Visitors;

    [TestClass]
    public class InnerLambdaCompilerTests
    {
        [NotNull]
        private InnerLambdaCompiler _helper;

        [TestInitialize]
        public void Init()
        {
            _helper = new InnerLambdaCompiler(new DelegateTypeHelper());
        }

        [TestMethod]
        public void FuncWithOneProperParameterAndOneOpenParameter()
        {
            Expression<Func<TestEntity, int>> ex = te => te.Children.Count(c => c.Prop1 == te.Prop1);
            var methodBody = (MethodCallExpression)ex.Body;
            var lambda = methodBody.LambdaArgument(1);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var updatedBody = methodBody.Update(null, new[] { methodBody.Arguments[0], result });
            var updatedLambda = ex.Update(updatedBody, ex.Parameters);
            var compiledUpdatedLambda = updatedLambda.Compile();
            var tep = new TestEntity
            {
                Prop1 = 1,
                Children =
                    new[]
                    {
                        new TestEntity {Prop1 = 2}, new TestEntity {Prop1 = 1}, new TestEntity {Prop1 = 2},
                        new TestEntity {Prop1 = 1},
                    }
            };
            Assert.AreEqual(2, compiledUpdatedLambda(tep));
        }

        [TestMethod]
        public void FuncWithOneProperParameterAndNoOpenParameters()
        {
            Expression<Func<TestEntity, int>> ex = te => te.Children.Sum(c => c.Prop1 * 2);
            var methodBody = (MethodCallExpression)ex.Body;
            var lambda = methodBody.LambdaArgument(1);
            var result = _helper.GenerateExpressionWithCompiledLambda(lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Func<TestEntity, int>>(
                Expression.Invoke(result, ex.Parameters[0]), ex.Parameters[0]).Compile();
            Assert.AreEqual(124, outerLambda(new TestEntity { Prop1 = 62 }));
        }

        [TestMethod]
        public void FuncWithOneProperParameterAndTwoOpenParameters()
        {
            var open1 = Expression.Parameter(typeof(int), "open1");
            var open2 = Expression.Parameter(typeof(int), "open2");
            var proper = Expression.Parameter(typeof(int), "proper");
            var lambda = Expression.Lambda<Func<int, int>>(Expression.Add(Expression.Add(proper, open1), open2), proper);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Func<int, int, int>>(
                Expression.Invoke(result, Expression.Constant(1)),
                open1,
                open2).Compile();

            Assert.AreEqual(7, outerLambda(4, 2));
        }

        [TestMethod]
        public void FuncWithNoParameters()
        {
            Expression<Func<int>> lambda = () => int.Parse("124");
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Func<int>>(Expression.Invoke(result)).Compile();
            Assert.AreEqual(124, outerLambda());
        }

        [TestMethod]
        public void FuncWithNoProperParametersAndOneOpenParameter()
        {
            var open1 = Expression.Parameter(typeof(int), "open1");
            var lambda = Expression.Lambda<Func<int>>(Expression.Add(open1, open1));
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Func<int, int>>(
                Expression.Invoke(result),
                open1).Compile();
            Assert.AreEqual(2, outerLambda(1));
        }

        [TestMethod]
        public void FuncWithNoProperParametersAndTwoOpenParameters()
        {
            var open1 = Expression.Parameter(typeof(int), "open1");
            var open2 = Expression.Parameter(typeof(int), "open2");
            var lambda = Expression.Lambda<Func<int>>(Expression.Add(open1, open2));
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Func<int, int, int>>(
                Expression.Invoke(result),
                open1, open2).Compile();
            Assert.AreEqual(3, outerLambda(1, 2));
        }

        [TestMethod]
        public void FuncWithTwoProperParametersAndNoOpenParameters()
        {
            Expression<Func<int, int, int>> lambda = (i1, i2) => i1 + i2;
            var result = _helper.GenerateExpressionWithCompiledLambda(lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Func<int, int, int>>(
                Expression.Invoke(result, lambda.Parameters[0], lambda.Parameters[1]), lambda.Parameters[0],
                lambda.Parameters[1]).Compile();
            Assert.AreEqual(124, outerLambda(120, 4));
        }

        [TestMethod]
        public void FuncWithTwoProperParametersAndOneOpenParameter()
        {
            var open1 = Expression.Parameter(typeof(int), "open1");
            var proper1 = Expression.Parameter(typeof(int), "proper1");
            var proper2 = Expression.Parameter(typeof(int), "proper2");
            var lambda = Expression.Lambda<Func<int, int, int>>(Expression.Subtract(Expression.Add(proper1, open1), proper2), proper1, proper2);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Func<int, int>>(
                Expression.Invoke(result, Expression.Constant(1), Expression.Constant(2)),
                open1).Compile();
            Assert.AreEqual(3, outerLambda(4));
        }

        [TestMethod]
        public void FuncWithTwoProperParametersAndTwoOpenParameters()
        {
            var open1 = Expression.Parameter(typeof(int), "open1");
            var open2 = Expression.Parameter(typeof(int), "open2");
            var proper1 = Expression.Parameter(typeof(int), "proper1");
            var proper2 = Expression.Parameter(typeof(int), "proper2");
            var lambda = Expression.Lambda<Func<int, int, int>>(Expression.Add(Expression.Multiply(proper1, open1), Expression.Multiply(proper2, open2)), proper1, proper2);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Func<int, int, int>>(
                Expression.Invoke(result, Expression.Constant(1), Expression.Constant(2)),
                open1, open2).Compile();

            Assert.AreEqual(20, outerLambda(4, 8));
        }

        [TestMethod]
        public void ActionWithNoParameters()
        {
            Expression<Action> lambda = Expression.Lambda<Action>(Expression.Divide(Expression.Constant(1), Expression.Constant(0)));
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action>(
                Expression.Invoke(result)).Compile();
            ExceptAssert.Throws<DivideByZeroException>(outerLambda);
        }

        [TestMethod]
        public void ActionWithNoProperParametersAndOneOpenParameter()
        {
            var open1 = Expression.Parameter(typeof(int), "open1");
            Expression<Action> lambda = Expression.Lambda<Action>(Expression.Divide(Expression.Constant(1), open1));
            var result = _helper.GenerateExpressionWithCompiledLambda(lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action<int>>(
                Expression.Invoke(result),
                open1).Compile();

            ExceptAssert.Throws<DivideByZeroException>(() => outerLambda(0));
        }

        [TestMethod]
        public void ActionWithNoProperParametersAndTwoOpenParameters()
        {
            var open1 = Expression.Parameter(typeof(int), "open1");
            var open2 = Expression.Parameter(typeof(int), "open2");
            Expression<Action> lambda = Expression.Lambda<Action>(Expression.Divide(open2, open1));
            var result = _helper.GenerateExpressionWithCompiledLambda(lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action<int, int>>(
                Expression.Invoke(result),
                open1, open2).Compile();

            ExceptAssert.Throws<DivideByZeroException>(() => outerLambda(0, 1));
        }

        [TestMethod]
        public void ActionWithOneProperParameterAndNoOpenParameters()
        {
            var proper1 = Expression.Parameter(typeof(TestEntity), "proper1");
            var lambda = Expression.Lambda<Action<TestEntity>>(
                Expression.PostIncrementAssign(Expression.Property(proper1, "Prop1")),
                proper1);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action<TestEntity>>(Expression.Invoke(result, proper1), proper1).Compile();

            var te = new TestEntity { Prop1 = 1 };
            outerLambda(te);
            Assert.AreEqual(2, te.Prop1);
        }

        [TestMethod]
        public void ActionWithOneProperParameterAndOneOpenParameter()
        {
            var proper1 = Expression.Parameter(typeof(TestEntity), "proper1");
            var open1 = Expression.Parameter(typeof(int), "open1");
            var lambda = Expression.Lambda<Action<TestEntity>>(
                Expression.AddAssign(Expression.Property(proper1, "Prop1"), open1),
                proper1);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action<TestEntity, int>>(Expression.Invoke(result, proper1), proper1, open1).Compile();

            var te = new TestEntity { Prop1 = 1 };
            outerLambda(te, 2);
            Assert.AreEqual(3, te.Prop1);
        }

        [TestMethod]
        public void ActionWithOneProperParameterAndTwoOpenParameters()
        {
            var proper1 = Expression.Parameter(typeof(TestEntity), "proper1");
            var open1 = Expression.Parameter(typeof(int), "open1");
            var open2 = Expression.Parameter(typeof(int), "open2");
            var lambda = Expression.Lambda<Action<TestEntity>>(
                Expression.AddAssign(Expression.Property(proper1, "Prop1"), Expression.Multiply(open1, open2)),
                proper1);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action<TestEntity, int, int>>(Expression.Invoke(result, proper1), proper1, open1, open2).Compile();

            var te = new TestEntity { Prop1 = 1 };
            outerLambda(te, 2, 3);
            Assert.AreEqual(7, te.Prop1);
        }

        [TestMethod]
        public void ActionWithTwoProperParametersAndNoOpenParameters()
        {
            var proper1 = Expression.Parameter(typeof(TestEntity), "proper1");
            var proper2 = Expression.Parameter(typeof(int), "proper2");
            var lambda = Expression.Lambda<Action<TestEntity, int>>(
                Expression.AddAssign(Expression.Property(proper1, "Prop1"), proper2),
                proper1, proper2);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action<TestEntity, int>>(Expression.Invoke(result, proper1, proper2), proper1, proper2).Compile();

            var te = new TestEntity { Prop1 = 1 };
            outerLambda(te, 2);
            Assert.AreEqual(3, te.Prop1);
        }

        [TestMethod]
        public void ActionWithTwoProperParametersAndOneOpenParameter()
        {
            var proper1 = Expression.Parameter(typeof(TestEntity), "proper1");
            var proper2 = Expression.Parameter(typeof(int), "proper2");
            var open1 = Expression.Parameter(typeof(int), "open1");
            var lambda = Expression.Lambda<Action<TestEntity, int>>(
                Expression.AddAssign(Expression.Property(proper1, "Prop1"), Expression.Multiply(proper2, open1)),
                proper1, proper2);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action<TestEntity, int, int>>(Expression.Invoke(result, proper1, proper2), proper1, proper2, open1).Compile();

            var te = new TestEntity { Prop1 = 1 };
            outerLambda(te, 2, 3);
            Assert.AreEqual(7, te.Prop1);
        }

        [TestMethod]
        public void ActionWithTwoProperParametersAndTwoOpenParameters()
        {
            var proper1 = Expression.Parameter(typeof(TestEntity), "proper1");
            var proper2 = Expression.Parameter(typeof(int), "proper2");
            var open1 = Expression.Parameter(typeof(int), "open1");
            var open2 = Expression.Parameter(typeof(int), "open2");
            var lambda = Expression.Lambda<Action<TestEntity, int>>(
                Expression.AddAssign(Expression.Property(proper1, "Prop1"), Expression.Multiply(proper2, Expression.Add(open1, open2))),
                proper1, proper2);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action<TestEntity, int, int, int>>(Expression.Invoke(result, proper1, proper2), proper1, proper2, open1, open2).Compile();

            var te = new TestEntity { Prop1 = 1 };
            outerLambda(te, 2, 3, 4);
            Assert.AreEqual(15, te.Prop1);
        }

        [TestMethod]
        public void CustomPrivateDelegateWithoutOpenParameters()
        {
            Expression<CustomPrivate> lambda = Expression.Lambda<CustomPrivate>(Expression.Divide(Expression.Constant(1), Expression.Constant(0)));
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action>(
                Expression.Invoke(result)).Compile();
            ExceptAssert.Throws<DivideByZeroException>(outerLambda);
        }

        [TestMethod]
        public void CustomPrivateDelegateWithOneOpenParameter()
        {
            var open1 = Expression.Parameter(typeof(int), "open1");
            Expression<CustomPrivate> lambda = Expression.Lambda<CustomPrivate>(Expression.Divide(Expression.Constant(1), open1));
            var result = _helper.GenerateExpressionWithCompiledLambda(lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action<int>>(
                Expression.Invoke(result),
                open1).Compile();

            ExceptAssert.Throws<DivideByZeroException>(() => outerLambda(0));
        }

        [TestMethod]
        public void CustomPublicDelegateWithoutOpenParameters()
        {
            Expression<CustomPublic> lambda = Expression.Lambda<CustomPublic>(Expression.Divide(Expression.Constant(1), Expression.Constant(0)));
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action>(
                Expression.Invoke(result)).Compile();
            ExceptAssert.Throws<DivideByZeroException>(outerLambda);
        }

        [TestMethod]
        public void CustomPublicDelegateWithOneOpenParameter()
        {
            var open1 = Expression.Parameter(typeof(int), "open1");
            Expression<CustomPublic> lambda = Expression.Lambda<CustomPublic>(Expression.Divide(Expression.Constant(1), open1));
            var result = _helper.GenerateExpressionWithCompiledLambda(lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action<int>>(
                Expression.Invoke(result),
                open1).Compile();

            ExceptAssert.Throws<DivideByZeroException>(() => outerLambda(0));
        }

        [TestMethod]
        public void CustomNestedPrivateDelegateWithoutOpenParameters()
        {
            Expression<CustomNestedPrivate> lambda = Expression.Lambda<CustomNestedPrivate>(Expression.Divide(Expression.Constant(1), Expression.Constant(0)));
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action>(
                Expression.Invoke(result)).Compile();
            ExceptAssert.Throws<DivideByZeroException>(outerLambda);
        }

        [TestMethod]
        public void CustomNestedPrivateDelegateWithOneOpenParameter()
        {
            var open1 = Expression.Parameter(typeof(int), "open1");
            Expression<CustomNestedPrivate> lambda = Expression.Lambda<CustomNestedPrivate>(Expression.Divide(Expression.Constant(1), open1));
            var result = _helper.GenerateExpressionWithCompiledLambda(lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action<int>>(
                Expression.Invoke(result),
                open1).Compile();

            ExceptAssert.Throws<DivideByZeroException>(() => outerLambda(0));
        }

        [TestMethod]
        public void CustomNestedPublicDelegateWithoutOpenParameters()
        {
            Expression<CustomNestedPublic> lambda = Expression.Lambda<CustomNestedPublic>(Expression.Divide(Expression.Constant(1), Expression.Constant(0)));
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action>(
                Expression.Invoke(result)).Compile();
            ExceptAssert.Throws<DivideByZeroException>(outerLambda);
        }

        [TestMethod]
        public void CustomNestedPublicDelegateWithOneOpenParameter()
        {
            var open1 = Expression.Parameter(typeof(int), "open1");
            Expression<CustomNestedPublic> lambda = Expression.Lambda<CustomNestedPublic>(Expression.Divide(Expression.Constant(1), open1));
            var result = _helper.GenerateExpressionWithCompiledLambda(lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Action<int>>(
                Expression.Invoke(result),
                open1).Compile();

            ExceptAssert.Throws<DivideByZeroException>(() => outerLambda(0));
        }

        [TestMethod]
        public void FuncWithProperParameterWithPrivateTypeAndOpenParameterWithPublicType()
        {
            var proper1 = Expression.Parameter(typeof(PrivateNestedType), "proper1");
            var open1 = Expression.Parameter(typeof(int), "open1");
            var lambda = Expression.Lambda<Func<PrivateNestedType, int>>(Expression.Add(Expression.Property(proper1, "Property"), open1), proper1);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Func<PrivateNestedType, int, int>>(
                Expression.Invoke(result, proper1), proper1, open1).Compile();
            Assert.AreEqual(3, outerLambda(new PrivateNestedType(1), 2));
        }

        [TestMethod]
        public void FuncWithProperParameterWithPublicTypeAndOpenParameterWithPrivateType()
        {
            var proper1 = Expression.Parameter(typeof(int), "proper1");
            var open1 = Expression.Parameter(typeof(PrivateNestedType), "open1");
            var lambda = Expression.Lambda<Func<int, int>>(Expression.Add(Expression.Property(open1, "Property"), proper1), proper1);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Func<PrivateNestedType, int, int>>(
                Expression.Invoke(result, proper1), open1, proper1).Compile();
            Assert.AreEqual(3, outerLambda(new PrivateNestedType(1), 2));
        }

        [TestMethod]
        public void FuncWithProperParameterWithPrivateTypeAndOpenParameterWithPrivateType()
        {
            var proper1 = Expression.Parameter(typeof(PrivateNestedType), "proper1");
            var open1 = Expression.Parameter(typeof(PrivateNestedType), "open1");
            var lambda = Expression.Lambda<Func<PrivateNestedType, int>>(Expression.Add(Expression.Property(open1, "Property"), Expression.Property(proper1, "Property")), proper1);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Func<PrivateNestedType, PrivateNestedType, int>>(
                Expression.Invoke(result, proper1), open1, proper1).Compile();
            Assert.AreEqual(3, outerLambda(new PrivateNestedType(1), new PrivateNestedType(2)));
        }

        [TestMethod]
        public void FuncWithPrivateTypeOutput()
        {
            var proper1 = Expression.Parameter(typeof(int), "proper1");
            var open1 = Expression.Parameter(typeof(int), "open1");
            var lambda = Expression.Lambda<Func<int, PrivateNestedType>>(
                Expression.MemberInit(
                    Expression.New(typeof(PrivateNestedType)),
                    Expression.Bind(
                        typeof(PrivateNestedType).GetTypeInfo().GetProperty("Property"),
                        Expression.Add(open1, proper1))),
                proper1);
            var result = _helper.GenerateExpressionWithCompiledLambda(
                lambda, OpenLambdaParameterFinderVisitor.GetOpenParameters(lambda));
            ExprAssert.NoInnerLambdas(result);
            var outerLambda = Expression.Lambda<Func<int, int, PrivateNestedType>>(
                Expression.Invoke(result, proper1), open1, proper1).Compile();
            var lambdaResult = outerLambda(1, 2);
            Assert.IsNotNull(lambdaResult);
            Assert.AreEqual(3, lambdaResult.Property);
        }

        private delegate void CustomNestedPrivate();
        public delegate void CustomNestedPublic();

        private class PrivateNestedType
        {
            [UsedImplicitly]
            public int Property { get; set; }

            public PrivateNestedType()
            {
            }

            public PrivateNestedType(int property)
            {
                Property = property;
            }
        }
    }

    internal delegate void CustomPrivate();
    public delegate void CustomPublic();
}
