﻿namespace TsSoft.Expressions.Helpers.Closures
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.Expressions.Helpers.Closures;
    using TsSoft.Expressions.Helpers.Extensions;

    [TestClass]
    public class LambdaCompilerTests
    {
        [NotNull] private Mock<IInnerLambdaCompiler> _innerLambdaCompiler;
        [NotNull] private Mock<ISpecialInnerLambdaCompiler> _specialInnerLambdaCompiler;
        [NotNull] private Mock<IMethodBuilderLambdaCompiler> _methodBuilderLambdaCompiler;
        [NotNull] private LambdaCompiler _compiler;

        [TestInitialize]
        public void Init()
        {
            _innerLambdaCompiler = new Mock<IInnerLambdaCompiler>(MockBehavior.Strict);
            _specialInnerLambdaCompiler = new Mock<ISpecialInnerLambdaCompiler>(MockBehavior.Strict);
            _methodBuilderLambdaCompiler = new Mock<IMethodBuilderLambdaCompiler>(MockBehavior.Strict);
            _compiler = new LambdaCompiler(_innerLambdaCompiler.Object, _specialInnerLambdaCompiler.Object, _methodBuilderLambdaCompiler.Object);
        }

        [TestMethod]
        public void WithoutPrecompilationNoInnerLambdas()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Prop1;
            var result = _compiler.Compile(expr, LambdaCompilationType.DoNotPrecompile);
            Assert.AreEqual(1, result(new TestEntity {Prop1 = 1}));
        }

        [TestMethod]
        public void WithoutPrecompilationWithPlainInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Select(c => c.Prop1).Sum();
            var result = _compiler.Compile(expr, LambdaCompilationType.DoNotPrecompile);
            Assert.AreEqual(3, result(new TestEntity { Children = new[] {new TestEntity {Prop1 = 1}, new TestEntity {Prop1 = 2}, }}));
        }

        [TestMethod]
        public void WithoutPrecompilationWithTrickyInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Count(c => c.Prop1 == te.Prop1);
            var result = _compiler.Compile(expr, LambdaCompilationType.DoNotPrecompile);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1, Children = new[] { new TestEntity { Prop1 = 1 }, new TestEntity { Prop1 = 2 }, } }));
        }

        [TestMethod]
        public void MethodBuilderCompilationNoInnerLambdas()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Prop1;
            _methodBuilderLambdaCompiler.Setup(
                silc => silc.Compile(
                    It.Is<Expression<Func<TestEntity, int>>>(lexpr => lexpr == expr)))
                .Returns((Expression<Func<TestEntity, int>> input) => input.Compile());
            var result = _compiler.Compile(expr, LambdaCompilationType.CompileToMethod);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1 }));
        }

        [TestMethod]
        public void MethodBuilderCompilationWithPlainInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Select(c => c.Prop1).Sum();
            _methodBuilderLambdaCompiler.Setup(
                silc => silc.Compile(
                    It.Is<Expression<Func<TestEntity, int>>>(lexpr => lexpr == expr)))
                .Returns((Expression<Func<TestEntity, int>> input) => input.Compile());
            var result = _compiler.Compile(expr, LambdaCompilationType.CompileToMethod);
            Assert.AreEqual(3, result(new TestEntity { Children = new[] { new TestEntity { Prop1 = 1 }, new TestEntity { Prop1 = 2 }, } }));
        }

        [TestMethod]
        public void MethodBuilderCompilationWithTrickyInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Count(c => c.Prop1 == te.Prop1);
            _methodBuilderLambdaCompiler.Setup(
                silc => silc.Compile(
                    It.Is<Expression<Func<TestEntity, int>>>(lexpr => lexpr == expr)))
                .Returns((Expression<Func<TestEntity, int>> input) => input.Compile());
            var result = _compiler.Compile(expr, LambdaCompilationType.CompileToMethod);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1, Children = new[] { new TestEntity { Prop1 = 1 }, new TestEntity { Prop1 = 2 }, } }));
        }

        [TestMethod]
        public void SimplePrecompilationNoInnerLambdas()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Prop1;
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInnerWithoutOpenParams);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1 }));
        }

        [TestMethod]
        public void SimplePrecompilationWithPlainInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Select(c => c.Prop1).Sum();
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInnerWithoutOpenParams);
            Assert.AreEqual(3, result(new TestEntity { Children = new[] { new TestEntity { Prop1 = 1 }, new TestEntity { Prop1 = 2 }, } }));
        }

        [TestMethod]
        public void SimplePrecompilationWithTrickyInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Count(c => c.Prop1 == te.Prop1);
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInnerWithoutOpenParams);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1, Children = new[] { new TestEntity { Prop1 = 1 }, new TestEntity { Prop1 = 2 }, } }));
        }

        [TestMethod]
        public void SimplePrecompilationWithQuotedInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.AsQueryable().Count(c => c.Prop1 == 1);
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInnerWithoutOpenParams);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1, Children = new[] { new TestEntity { Prop1 = 1 }, new TestEntity { Prop1 = 2 }, } }));
        }

        [TestMethod]
        public void SpecialPrecompilationNoInnerLambdas()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Prop1;
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInnerSpecial);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1 }));
        }

        [TestMethod]
        public void SpecialPrecompilationWithPlainInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Select(c => c.Prop1).Sum();
            Expression<Func<TestEntity, int>> inner = c => c.Prop1;
            _specialInnerLambdaCompiler.Setup(
                silc => silc.TryGenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => ExpressionEqualityComparer.Instance.Equals(lexpr, inner)),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 0)))
                .Returns((LambdaExpression input, ISet<ParameterExpression> p) => input);
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInnerSpecial);
            _specialInnerLambdaCompiler.Verify(
                silc => silc.TryGenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => ExpressionEqualityComparer.Instance.Equals(lexpr, inner)),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 0)), Times.Once);
            Assert.AreEqual(3, result(new TestEntity { Children = new[] { new TestEntity { Prop1 = 1 }, new TestEntity { Prop1 = 2 }, } }));
        }

        [TestMethod]
        public void SpecialPrecompilationWithTrickyInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Count(c => c.Prop1 == te.Prop1);
            var inner = ((MethodCallExpression)expr.Body).LambdaArgument(1);
            _specialInnerLambdaCompiler.Setup(
                silc => silc.TryGenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == inner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 1 && s.Any(p => p.Name == "te"))))
                .Returns((LambdaExpression input, ISet<ParameterExpression> p) => input);
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInnerSpecial);
            _specialInnerLambdaCompiler.Verify(
                silc => silc.TryGenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == inner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 1 && s.Any(p => p.Name == "te"))), Times.Once);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1, Children = new[] { new TestEntity { Prop1 = 1 }, new TestEntity { Prop1 = 2 }, } }));
        }

        [TestMethod]
        public void SpecialPrecompilationWithPlainSecondLevelInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Count(c => c.Children.Any(cc => cc.Prop1 == 1));
            var inner = ((MethodCallExpression)expr.Body).LambdaArgument(1);
            var innerInner = ((MethodCallExpression)inner.Body).LambdaArgument(1);
            _specialInnerLambdaCompiler.Setup(
                silc => silc.TryGenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == inner || lexpr == innerInner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 0)))
                .Returns((LambdaExpression input, ISet<ParameterExpression> p) => input).Verifiable();
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInnerSpecial);
            _specialInnerLambdaCompiler.Verify(
                silc => silc.TryGenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == inner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 0)), Times.Once);
            _specialInnerLambdaCompiler.Verify(
                silc => silc.TryGenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == innerInner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 0)), Times.Once);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1, Children = new[] { new TestEntity { Children = new[] { new TestEntity { Prop1 = 2 }, } }, new TestEntity { Children = new[] { new TestEntity { Prop1 = 1 }, } }, } }));
        }

        [TestMethod]
        public void SpecialPrecompilationWithTrickySecondLevelInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Count(c => c.Prop1 == te.Prop1 && c.Children.Any(cc => cc.Prop1 == c.Prop1 + te.Prop1));
            var inner = ((MethodCallExpression)expr.Body).LambdaArgument(1);
            var innerInner = ((MethodCallExpression)((BinaryExpression)inner.Body).Right).LambdaArgument(1);
            _specialInnerLambdaCompiler.Setup(
                silc => silc.TryGenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == inner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 1 && s.Any(p => p.Name == "te"))))
                .Returns((LambdaExpression input, ISet<ParameterExpression> p) => input).Verifiable();
            _specialInnerLambdaCompiler.Setup(
                silc => silc.TryGenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == innerInner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 2 && s.Any(p => p.Name == "te") && s.Any(p => p.Name == "c"))))
                .Returns((LambdaExpression input, ISet<ParameterExpression> p) => input).Verifiable();
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInnerSpecial);
            _specialInnerLambdaCompiler.Verify(
                silc => silc.TryGenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == inner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 1 && s.Any(p => p.Name == "te"))), Times.Once);
            _specialInnerLambdaCompiler.Verify(
                silc => silc.TryGenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == innerInner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 2 && s.Any(p => p.Name == "te") && s.Any(p => p.Name == "c"))), Times.Once);
            Assert.AreEqual(1, result(new TestEntity
            {
                Prop1 = 1,
                Children = new[]
                {
                    new TestEntity
                    {
                        Prop1 = 1,
                        Children = new[] { new TestEntity { Prop1 = 2 }, }
                    },
                    new TestEntity
                    {
                        Prop1 = 1,
                        Children = new[] { new TestEntity { Prop1 = 1 }, }
                    },
                    new TestEntity
                    {
                        Prop1 = 2,
                        Children = new[] { new TestEntity { Prop1 = 3 }, }
                    },
                    new TestEntity
                    {
                        Prop1 = 2,
                        Children = new[] { new TestEntity { Prop1 = 2 }, }
                    },
                }
            }));
        }

        [TestMethod]
        public void SpecialPrecompilationWithQuotedInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.AsQueryable().Count(c => c.Prop1 == 1);
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInnerSpecial);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1, Children = new[] { new TestEntity { Prop1 = 1 }, new TestEntity { Prop1 = 2 }, } }));
        }

        [TestMethod]
        public void FullPrecompilationNoInnerLambdas()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Prop1;
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInner);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1 }));
        }

        [TestMethod]
        public void FullPrecompilationWithPlainInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Select(c => c.Prop1).Sum();
            Expression<Func<TestEntity, int>> inner = c => c.Prop1;
            _innerLambdaCompiler.Setup(
                silc => silc.GenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => ExpressionEqualityComparer.Instance.Equals(lexpr, inner)),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 0)))
                .Returns((LambdaExpression input, ISet<ParameterExpression> p) => input);
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInner);
            _innerLambdaCompiler.Verify(
                silc => silc.GenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => ExpressionEqualityComparer.Instance.Equals(lexpr, inner)),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 0)), Times.Once);
            Assert.AreEqual(3, result(new TestEntity { Children = new[] { new TestEntity { Prop1 = 1 }, new TestEntity { Prop1 = 2 }, } }));
        }

        [TestMethod]
        public void FullPrecompilationWithTrickyInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Count(c => c.Prop1 == te.Prop1);
            var inner = ((MethodCallExpression)expr.Body).LambdaArgument(1);
            _innerLambdaCompiler.Setup(
                silc => silc.GenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == inner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 1 && s.Any(p => p.Name == "te"))))
                .Returns((LambdaExpression input, ISet<ParameterExpression> p) => input);
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInner);
            _innerLambdaCompiler.Verify(
                silc => silc.GenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == inner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 1 && s.Any(p => p.Name == "te"))), Times.Once);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1, Children = new[] { new TestEntity { Prop1 = 1 }, new TestEntity { Prop1 = 2 }, } }));
        }

        [TestMethod]
        public void FullPrecompilationWithPlainSecondLevelInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Count(c => c.Children.Any(cc => cc.Prop1 == 1));
            var inner = ((MethodCallExpression)expr.Body).LambdaArgument(1);
            var innerInner = ((MethodCallExpression)inner.Body).LambdaArgument(1);
            _innerLambdaCompiler.Setup(
                silc => silc.GenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == inner || lexpr == innerInner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 0)))
                .Returns((LambdaExpression input, ISet<ParameterExpression> p) => input).Verifiable();
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInner);
            _innerLambdaCompiler.Verify(
                silc => silc.GenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == inner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 0)), Times.Once);
            _innerLambdaCompiler.Verify(
                silc => silc.GenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == innerInner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 0)), Times.Once);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1, Children = new[] { new TestEntity { Children = new[] { new TestEntity { Prop1 = 2 }, } }, new TestEntity { Children = new[] { new TestEntity { Prop1 = 1 }, } }, } }));
        }

        [TestMethod]
        public void FullPrecompilationWithTrickySecondLevelInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.Count(c => c.Prop1 == te.Prop1 && c.Children.Any(cc => cc.Prop1 == c.Prop1 + te.Prop1));
            var inner = ((MethodCallExpression)expr.Body).LambdaArgument(1);
            var innerInner = ((MethodCallExpression)((BinaryExpression)inner.Body).Right).LambdaArgument(1);
            _innerLambdaCompiler.Setup(
                silc => silc.GenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == inner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 1 && s.Any(p => p.Name == "te"))))
                .Returns((LambdaExpression input, ISet<ParameterExpression> p) => input).Verifiable();
            _innerLambdaCompiler.Setup(
                silc => silc.GenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == innerInner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 2 && s.Any(p => p.Name == "te") && s.Any(p => p.Name == "c"))))
                .Returns((LambdaExpression input, ISet<ParameterExpression> p) => input).Verifiable();
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInner);
            _innerLambdaCompiler.Verify(
                silc => silc.GenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == inner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 1 && s.Any(p => p.Name == "te"))), Times.Once);
            _innerLambdaCompiler.Verify(
                silc => silc.GenerateExpressionWithCompiledLambda(
                    It.Is<LambdaExpression>(lexpr => lexpr == innerInner),
                    It.Is<ISet<ParameterExpression>>(s => s.Count == 2 && s.Any(p => p.Name == "te") && s.Any(p => p.Name == "c"))), Times.Once);
            Assert.AreEqual(1, result(new TestEntity
            {
                Prop1 = 1,
                Children = new[]
                {
                    new TestEntity
                    {
                        Prop1 = 1,
                        Children = new[] { new TestEntity { Prop1 = 2 }, }
                    },
                    new TestEntity
                    {
                        Prop1 = 1,
                        Children = new[] { new TestEntity { Prop1 = 1 }, }
                    },
                    new TestEntity
                    {
                        Prop1 = 2,
                        Children = new[] { new TestEntity { Prop1 = 3 }, }
                    },
                    new TestEntity
                    {
                        Prop1 = 2,
                        Children = new[] { new TestEntity { Prop1 = 2 }, }
                    },
                }
            }));
        }
        [TestMethod]
        public void FullPrecompilationWithQuotedInnerLambda()
        {
            Expression<Func<TestEntity, int>> expr = te => te.Children.AsQueryable().Count(c => c.Prop1 == 1);
            var result = _compiler.Compile(expr, LambdaCompilationType.PrecompileInner);
            Assert.AreEqual(1, result(new TestEntity { Prop1 = 1, Children = new[] { new TestEntity { Prop1 = 1 }, new TestEntity { Prop1 = 2 }, } }));
        }
    }
}
