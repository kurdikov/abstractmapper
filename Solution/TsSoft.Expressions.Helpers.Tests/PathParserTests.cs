﻿// ReSharper disable ConvertToConstant.Local
namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.Models;

    [TestClass]
    public class PathParserTests
    {
        [NotNull]private PathParser _parser;

        [TestInitialize]
        public void Init()
        {
            _parser = new PathParser(StaticHelpers.Library, StaticHelpers.ExpressionBuilder, StaticHelpers.PathHelper);
        }

        [TestMethod]
        public void ParseCollectionWithEqualOpCondition()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1 == 1);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Prop1 == 1;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithGreaterOpCondition()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1 > 1);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Prop1 > 1;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithLessOpCondition()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1 < 1);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Prop1 < 1;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithGreaterOrEqualOpCondition()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1 >= 1);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Prop1 >= 1;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithLessOrEqualOpCondition()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1 <= 1);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Prop1 <= 1;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithNotEqualOpCondition()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1 != 1);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Prop1 != 1;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithEqualOpConditionUsingClosure()
        {
            int id = 1;
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1 == id);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Prop1 == id;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithGreaterOpConditionUsingClosure()
        {
            int id = 1;
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1 > id);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Prop1 > id;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithLessOpConditionUsingClosure()
        {
            int id = 1;
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1 < id);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Prop1 < id;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithGreaterOrEqualOpConditionUsingClosure()
        {
            int id = 1;
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1 >= id);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Prop1 >= id;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithLessOrEqualOpConditionUsingClosure()
        {
            int id = 1;
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1 <= id);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Prop1 <= id;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithNotEqualOpConditionUsingClosure()
        {
            int id = 1;
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1 != id);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Prop1 != id;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithAny()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Children.Any());
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Children != null && p.Children.Any();
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithCount()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Children.Count() == 3);
            Expression<Func<TestEntity, bool>> expectedServerCondition = p => p != null && p.Children != null && p.Children.Count() == 3;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithAnyWithLambda()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Children.Any(c => c.Prop1 == 2));
            Expression<Func<TestEntity, bool>> expectedServerCondition = x => x != null && x.Children != null && x.Children.Any(c => c != null && c.Prop1 == 2);
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(2, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[1].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithCountWithLambda()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Children.Count(c => c.Prop1 == 2) == 3);
            Expression<Func<TestEntity, bool>> expectedServerCondition = x => x != null && x.Children != null && x.Children.Count(c => c != null && c.Prop1 == 2) == 3;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(2, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[1].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithMaxWithLambda()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Children.Max(c => c.Prop1) == 3);
            Expression<Func<TestEntity, bool>> expectedServerCondition = x => x != null && x.Children != null && x.Children.Where(c => c != null).Max(c => c.Prop1) == 3;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(2, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[1].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithMinWithLambda()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Children.Min(c => c.Prop1) == 3);
            Expression<Func<TestEntity, bool>> expectedServerCondition = x => x != null && x.Children != null && x.Children.Where(c => c != null).Min(c => c.Prop1) == 3;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(2, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[1].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithAverageWithLambda()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Children.Average(c => c.Prop1) == 3);
            Expression<Func<TestEntity, bool>> expectedServerCondition = x => x != null && x.Children != null && x.Children.Where(c => c != null).Average(c => c.Prop1) == 3;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(2, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[1].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithSumWithLambda()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Children.Sum(c => c.Prop1) == 3);
            Expression<Func<TestEntity, bool>> expectedServerCondition = x => x != null && x.Children != null && x.Children.Where(c => c != null).Sum(c => c.Prop1) == 3;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(2, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[1].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithAnyAndStringContains()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Children.Any(c => c.Prop2.Contains("test")));
            Expression<Func<TestEntity, bool>> expectedServerCondition =
                x => x != null && x.Children != null && x.Children.Any(c => c != null && c.Prop2 != (object)null && c.Prop2.Contains("test"));
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(2, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
            MemberAssert.AreEqual(TestEntityVh.Prop2, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[1].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithIntToString()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop1.ToString() == "test");
            Expression<Func<TestEntity, bool>> expectedServerCondition = x => x != null && x.Prop1.ToString() == "test";
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithNullableIntToString()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Prop3.ToString() == "test");
            Expression<Func<TestEntity, bool>> expectedServerCondition = x => x != null && x.Prop3.ToString() == "test";
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Prop3, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithAnyWithReferenceToOuterParameter()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(p => p.Children.Any(c => c.Prop1 == p.Prop1));
            Expression<Func<TestEntity, bool>> expectedServerCondition = x => x != null && x.Children != null && x.Children.Any(c => c != null && x != null && c.Prop1 == x.Prop1);
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(2, result.Elements[0].Conditions.First().UsedPaths.Count());
            var oneStepPath = result.Elements[0].Conditions.First().UsedPaths.FirstOrDefault(p => p.Elements.Count == 1);
            var twoStepPath = result.Elements[0].Conditions.First().UsedPaths.FirstOrDefault(p => p.Elements.Count == 2);
            Assert.IsNotNull(oneStepPath);
            Assert.IsNotNull(twoStepPath);
            MemberAssert.AreEqual(TestEntityVh.Children, twoStepPath.Elements[0].Step);
            MemberAssert.AreEqual(TestEntityVh.Prop1, twoStepPath.Elements[1].Step);
            MemberAssert.AreEqual(TestEntityVh.Prop1, oneStepPath.Elements[0].Step);
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithAndOperator()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(c => c.Prop1 == 1 && c.Prop2 == "test");
            Expression<Func<TestEntity, bool>> expectedServerCondition = 
                x => 
                    (
                        ((x != null) ? (x.Prop1 == 1 ? NBool.True : NBool.False) : NBool.Null)
                        & ((x != null) ? (x.Prop2 == "test" ? NBool.True : NBool.False) : NBool.Null)
                    )
                    == NBool.True;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(2, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(1).Elements.Count);
            Assert.IsNotNull(result.Elements[0].Conditions.First().UsedPaths.Where(e => MemberInfoComparer.Instance.Equals(TestEntityVh.Prop1, e.Elements[0].Step)));
            Assert.IsNotNull(result.Elements[0].Conditions.First().UsedPaths.Where(e => MemberInfoComparer.Instance.Equals(TestEntityVh.Prop2, e.Elements[0].Step)));
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithOrOperator()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(c => c.Prop1 == 1 || c.Prop2 == "test");
            Expression<Func<TestEntity, bool>> expectedServerCondition = 
                x => 
                    (
                        (x != null ? (x.Prop1 == 1 ? NBool.True : NBool.False) : NBool.Null)
                        | (x != null ? (x.Prop2 == "test" ? NBool.True : NBool.False) : NBool.Null)
                    ) == NBool.True;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(2, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(1).Elements.Count);
            Assert.IsNotNull(result.Elements[0].Conditions.First().UsedPaths.Where(e => MemberInfoComparer.Instance.Equals(TestEntityVh.Prop1, e.Elements[0].Step)));
            Assert.IsNotNull(result.Elements[0].Conditions.First().UsedPaths.Where(e => MemberInfoComparer.Instance.Equals(TestEntityVh.Prop2, e.Elements[0].Step)));
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithXorOperator()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(c => c.Prop1 == 1 ^ c.Prop2 == "test");
            Expression<Func<TestEntity, bool>> expectedServerCondition = x => (x != null) && (x != null) && (x.Prop1 == 1 ^ x.Prop2 == "test");
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(2, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(1).Elements.Count);
            Assert.IsNotNull(result.Elements[0].Conditions.First().UsedPaths.Where(e => MemberInfoComparer.Instance.Equals(TestEntityVh.Prop1, e.Elements[0].Step)));
            Assert.IsNotNull(result.Elements[0].Conditions.First().UsedPaths.Where(e => MemberInfoComparer.Instance.Equals(TestEntityVh.Prop2, e.Elements[0].Step)));
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithCoalesce()
        {
            Expression<Func<TestEntityWithNullableBool, object>> path = t => t.NChildren.Where(c => c.Prop4 ?? c.Prop1 == 1);
            Expression<Func<TestEntityWithNullableBool, bool>> expectedServerCondition = x => (NBool)
                (
                    (bool?)(x != null ? (NBool)x.Prop4 : NBool.Null)
                    ?? (bool?)(x != null ? (x.Prop1 == 1 ? NBool.True : NBool.False) : NBool.Null)
                )
                == NBool.True;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(new ValueHoldingMember(typeof(TestEntityWithNullableBool).GetTypeInfo().GetProperty("NChildren")), result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(2, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(1).Elements.Count);
            Assert.IsNotNull(result.Elements[0].Conditions.First().UsedPaths.Where(e => MemberInfoComparer.Instance.Equals(TestEntityVh.Prop1, e.Elements[0].Step)));
            Assert.IsNotNull(result.Elements[0].Conditions.First().UsedPaths.Where(e => MemberInfoComparer.Instance.Equals(TestEntityVh.Prop4, e.Elements[0].Step)));
        }

        [TestMethod]
        public void ParseCollectionWithConditionWithNullComparison()
        {
            Expression<Func<TestEntity, object>> path = t => t.Children.Where(c => c.Prop2 == null);
            Expression<Func<TestEntity, bool>> expectedServerCondition = x => x == null || x.Prop2 == null;
            var result = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], result.Start);
            Assert.AreEqual(1, result.Elements.Count);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNull(result.Elements[0].Parameter);
            MemberAssert.IsWhere(result.Elements[0].Conditions.First().ConditionMethod);
            ExprAssert.AreEqual(expectedServerCondition, result.Elements[0].Conditions.First().ServerCondition);
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, result.Elements[0].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            Assert.IsNotNull(result.Elements[0].Conditions.First().UsedPaths.Where(e => MemberInfoComparer.Instance.Equals(TestEntityVh.Prop2, e.Elements[0].Step)));
        }

        [TestMethod]
        public void ParseTwoCollectionSteps()
        {
            Expression<Func<TestEntity, object>> pathSelectMany = e => e.Children.SelectMany(c => c.Children);
            var result = _parser.Parse(pathSelectMany);
            Assert.AreSame(pathSelectMany.Parameters[0], result.Start);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNotNull(result.Elements[0].Parameter);
            Assert.IsNull(result.Elements[0].Conditions);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[1].Step);
            Assert.IsNull(result.Elements[1].Parameter);
            Assert.IsNull(result.Elements[1].Conditions);

            result = _parser.Parse(pathSelectMany.Body);
            Assert.AreSame(pathSelectMany.Parameters[0], result.Start);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNotNull(result.Elements[0].Parameter);
            Assert.IsNull(result.Elements[0].Conditions);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[1].Step);
            Assert.IsNull(result.Elements[1].Parameter);
            Assert.IsNull(result.Elements[1].Conditions);
        }

        [TestMethod]
        public void ParseTwoCollectionStepsWithConditionOnSecondStep()
        {
            Expression<Func<TestEntity, object>> pathSelectManyWithWhere = e => e.Children.SelectMany(c => c.Children)
                .Where(c => c.Prop3 != null);
            Expression<Func<TestEntity, bool>> condition = c => c.Prop3 != null;
            var result = _parser.Parse(pathSelectManyWithWhere);
            Assert.AreSame(pathSelectManyWithWhere.Parameters[0], result.Start);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNotNull(result.Elements[0].Parameter);
            Assert.IsNull(result.Elements[0].Conditions);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[1].Step);
            Assert.IsNull(result.Elements[1].Parameter);
            ExprAssert.AreEqual(condition, result.Elements[1].Conditions.First().DbCondition);

            result = _parser.Parse(pathSelectManyWithWhere.Body);
            Assert.AreSame(pathSelectManyWithWhere.Parameters[0], result.Start);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[0].Step);
            Assert.IsNotNull(result.Elements[0].Parameter);
            Assert.IsNull(result.Elements[0].Conditions);
            MemberAssert.AreEqual(TestEntityVh.Children, result.Elements[1].Step);
            Assert.IsNull(result.Elements[1].Parameter);
            ExprAssert.AreEqual(condition, result.Elements[1].Conditions.First().DbCondition);
        }

        [TestMethod]
        public void ThrowOnPathWithToListMethod()
        {
            Expression<Func<TestEntity, object>> pathSelectManyToList = e => e.Children.SelectMany(c => c.Children).ToList();
            Expression<Func<TestEntity, object>> pathSelectManyToArray = e => e.Children.SelectMany(c => c.Children).ToArray();
            Expression<Func<TestEntity, object>> pathSelectManyAsEnumearble = e => e.Children.SelectMany(c => c.Children).AsEnumerable();
            Expression<Func<TestEntity, object>> pathSelectManyAsQueryable = e => e.Children.SelectMany(c => c.Children).AsQueryable();

            ExceptAssert.Throws<NotSupportedException>(() => _parser.Parse(pathSelectManyToList));
            ExceptAssert.Throws<NotSupportedException>(() => _parser.Parse(pathSelectManyToArray));
            ExceptAssert.Throws<NotSupportedException>(() => _parser.Parse(pathSelectManyAsEnumearble));
            ExceptAssert.Throws<NotSupportedException>(() => _parser.Parse(pathSelectManyAsQueryable));

            ExceptAssert.Throws<NotSupportedException>(() => _parser.Parse(pathSelectManyToList.Body));
            ExceptAssert.Throws<NotSupportedException>(() => _parser.Parse(pathSelectManyToArray.Body));
            ExceptAssert.Throws<NotSupportedException>(() => _parser.Parse(pathSelectManyAsEnumearble.Body));
            ExceptAssert.Throws<NotSupportedException>(() => _parser.Parse(pathSelectManyAsQueryable.Body));
        }

        [TestMethod]
        public void ThrowOnPathYieldingNestedEnumerable()
        {
            Expression<Func<TestEntity, object>> nestedEnumerablePath = e => e.Children.Select(c => c.Children);

            ExceptAssert.Throws<NotSupportedException>(() => _parser.Parse(nestedEnumerablePath));

            ExceptAssert.Throws<NotSupportedException>(() => _parser.Parse(nestedEnumerablePath.Body));
        }


        [TestMethod]
        public void ParseObjectThenCollectionWithCondition()
        {
            Expression<Func<TestEntity, object>> path = (TestEntity e) => e.Parent.Children.Where(c => c.Prop1 == 11);
            var actual = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], actual.Start);
            Assert.AreEqual(2, actual.Elements.Count);
            Assert.AreEqual("Parent", actual.Elements[0].Step.Name);
            Assert.IsNull(actual.Elements[0].Conditions);
            Assert.AreEqual("Children", actual.Elements[1].Step.Name);
            Assert.IsNotNull(actual.Elements[1].Conditions);
            Assert.IsTrue(StaticHelpers.Library.IsEnumerableWhere(actual.Elements[1].Conditions.First().ConditionMethod));
            ExprAssert.AreEqual((Expression<Func<TestEntity, bool>>)actual.Elements[1].Conditions.First().DbCondition, (TestEntity c) => c.Prop1 == 11);
            ExprAssert.AreEqual((Expression<Func<TestEntity, bool>>)actual.Elements[1].Conditions.First().ServerCondition, (TestEntity c) => c != null && c.Prop1 == 11);
            Assert.AreEqual("c", actual.Elements[1].Conditions.First().Start.Name);
            Assert.AreEqual(1, actual.Elements[1].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, actual.Elements[1].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            Assert.AreEqual("Prop1", actual.Elements[1].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step.Name);
        }

        [TestMethod]
        public void ParseCollectionThenObjectWithCondition()
        {
            Expression<Func<TestEntity, object>> path = (TestEntity e) => e.Children.Select(c => c.Parent).Where(c => c.Prop1 == 11);
            var actual = _parser.Parse(path);
            Assert.AreSame(path.Parameters[0], actual.Start);
            Assert.AreEqual(2, actual.Elements.Count);
            Assert.AreEqual("Children", actual.Elements[0].Step.Name);
            Assert.IsNull(actual.Elements[0].Conditions);
            Assert.AreEqual("Parent", actual.Elements[1].Step.Name);
            Assert.IsNotNull(actual.Elements[1].Conditions);
            Assert.IsTrue(StaticHelpers.Library.IsEnumerableWhere(actual.Elements[1].Conditions.First().ConditionMethod));
            ExprAssert.AreEqual((Expression<Func<TestEntity, bool>>)actual.Elements[1].Conditions.First().DbCondition, (TestEntity c) => c.Prop1 == 11);
            ExprAssert.AreEqual((Expression<Func<TestEntity, bool>>)actual.Elements[1].Conditions.First().ServerCondition, (TestEntity c) => c != null && c.Prop1 == 11);
            Assert.AreEqual("c", actual.Elements[1].Conditions.First().Start.Name);
            Assert.AreEqual(1, actual.Elements[1].Conditions.First().UsedPaths.Count());
            Assert.AreEqual(1, actual.Elements[1].Conditions.First().UsedPaths.ElementAt(0).Elements.Count);
            Assert.AreEqual("Prop1", actual.Elements[1].Conditions.First().UsedPaths.ElementAt(0).Elements[0].Step.Name);
        }
    }
}
// ReSharper restore ConvertToConstant.Local
