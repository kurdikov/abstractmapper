﻿namespace TsSoft.Expressions.Helpers
{
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Models;

    [TestClass]
    public class FlatPathParserTests
    {
        private static readonly ValueHoldingMember T1A = new ValueHoldingMember(typeof(T1).GetProperty("A"));
        private static readonly ValueHoldingMember T1B = new ValueHoldingMember(typeof(T1).GetProperty("B"));
        private static readonly ValueHoldingMember T2A = new ValueHoldingMember(typeof(T2).GetProperty("A"));
        private static readonly ValueHoldingMember T2B = new ValueHoldingMember(typeof(T2).GetProperty("B"));
        private static readonly ValueHoldingMember T3A = new ValueHoldingMember(typeof(T3).GetProperty("A"));
        private static readonly ValueHoldingMember T3B = new ValueHoldingMember(typeof(T3).GetProperty("B"));


        readonly Expression<Func<T1, object>> _eIdentity = e => e;
        readonly Expression<Func<T1, object>> _ea = e => e.A;
        readonly Expression<Func<T1, object>> _eb = e => e.B;
        readonly Expression<Func<T1, object>> _eaa = e => e.A.A;
        readonly Expression<Func<T1, object>> _eab = e => e.A.B;
        readonly Expression<Func<T1, object>> _eba = e => e.B.Select(ee => ee.A);
        readonly Expression<Func<T1, object>> _ebb = e => e.B.Select(ee => ee.B);
        readonly Expression<Func<T1, object>> _ebaa = e => e.B.Select(ee => ee.A.A);
        readonly Expression<Func<T1, object>> _ebbb = e => e.B.Select(ee => ee.B.Select(eee => eee.B));

        [NotNull]
        private FlatPathParser _parser;

        [TestInitialize]
        public void Init()
        {
            _parser = new FlatPathParser();
        }

        [TestMethod]
        public void NullPath()
        {
            var result = _parser.Parse(null);
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void NoSteps()
        {
            var result = _parser.Parse(_eIdentity);
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void Object()
        {
            var result = _parser.Parse(_ea);
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(T1A, result[0]);
        }

        [TestMethod]
        public void Collection()
        {
            var result = _parser.Parse(_eb);
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(T1B, result[0]);
        }

        [TestMethod]
        public void ObjectObject()
        {
            var result = _parser.Parse(_eaa);
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(T1A, result[0]);
            Assert.AreEqual(T2A, result[1]);
        }

        [TestMethod]
        public void ObjectCollection()
        {
            var result = _parser.Parse(_eab);
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(T1A, result[0]);
            Assert.AreEqual(T2B, result[1]);
        }

        [TestMethod]
        public void CollectionObject()
        {
            var result = _parser.Parse(_eba);
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(T1B, result[0]);
            Assert.AreEqual(T2A, result[1]);
        }

        [TestMethod]
        public void TwoCollections()
        {
            var result = _parser.Parse(_ebb);
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(T1B, result[0]);
            Assert.AreEqual(T2B, result[1]);
        }

        [TestMethod]
        public void CollectionObjectObject()
        {
            var result = _parser.Parse(_ebaa);
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(T1B, result[0]);
            Assert.AreEqual(T2A, result[1]);
            Assert.AreEqual(T3A, result[2]);
        }

        [TestMethod]
        public void ThreeCollections()
        {
            var result = _parser.Parse(_ebbb);
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(T1B, result[0]);
            Assert.AreEqual(T2B, result[1]);
            Assert.AreEqual(T3B, result[2]);
        }

        [TestMethod]
        public void PathWithSelectMany()
        {
            Expression<Func<T1, object>> ex = (T1 e) => e.B.SelectMany(ee => ee.B);
            var result = _parser.Parse(ex);
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(T1B, result[0]);
            Assert.AreEqual(T2B, result[1]);
        }

        [TestMethod]
        public void ThrowsOnPathWithCondition()
        {
            Expression<Func<T1, object>> exx = (T1 e) => e.B.Where(ee => ee.B != null);
            ExceptAssert.Throws<ArgumentException>(() => _parser.Parse(exx));
        }

        public class T1
        {
            public T2 A { get; set; }

            public ICollection<T2> B { get; set; }
        }

        public class T2
        {
            public T3 A { get; set; }

            public ICollection<T3> B { get; set; }
        }

        public class T3
        {
            public T3 A { get; set; }

            public ICollection<T3> B { get; set; } 
        }
    }
}
