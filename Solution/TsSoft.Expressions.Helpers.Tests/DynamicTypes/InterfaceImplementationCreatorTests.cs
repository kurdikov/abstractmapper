﻿namespace TsSoft.Expressions.Helpers.DynamicTypes
{
    using System.Reflection;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers.DynamicTypes;

    [TestClass]
    public class InterfaceImplementationCreatorTests
    {
        public interface INoProp
        {
        }

        public interface IOneValueProp
        {
            int Prop1 { get; set; }
        }

        public interface IOneInterfaceProp
        {
            IOneValueProp Prop2 { get; set; }
        }

        public interface IInheritedInterface : IOneValueProp, IOneInterfaceProp
        {
        }

        [NotNull]
        private InterfaceImplementationCreator _creator;

        [TestInitialize]
        public void Init()
        {
            _creator = new InterfaceImplementationCreator(new PropertyCreator());
        }

        [TestMethod]
        public void NoProp()
        {
            var type = _creator.CreateInterfaceImplementation<INoProp>();
            Assert.IsTrue(typeof(INoProp).GetTypeInfo().IsAssignableFrom(type));
        }

        [TestMethod]
        public void OneValueProp()
        {
            var type = _creator.CreateInterfaceImplementation<IOneValueProp>();
            Assert.IsTrue(typeof(IOneValueProp).GetTypeInfo().IsAssignableFrom(type));
        }

        [TestMethod]
        public void OneInterfaceProp()
        {
            var type = _creator.CreateInterfaceImplementation<IOneInterfaceProp>();
            Assert.IsTrue(typeof(IOneInterfaceProp).GetTypeInfo().IsAssignableFrom(type));
        }

        [TestMethod]
        public void InheritedInterface()
        {
            var type = _creator.CreateInterfaceImplementation<IInheritedInterface>();
            Assert.IsTrue(typeof(IOneValueProp).GetTypeInfo().IsAssignableFrom(type));
            Assert.IsTrue(typeof(IOneInterfaceProp).GetTypeInfo().IsAssignableFrom(type));
            Assert.IsTrue(typeof(IInheritedInterface).GetTypeInfo().IsAssignableFrom(type));
        }
    }
}
