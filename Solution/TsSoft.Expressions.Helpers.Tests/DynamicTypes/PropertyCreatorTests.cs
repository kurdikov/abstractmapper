﻿namespace TsSoft.Expressions.Helpers.DynamicTypes
{
    using System;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.DynamicTypes;

    [TestClass]
    public class PropertyCreatorTests
    {
        [TestMethod]
        public void CreateAutoPropertyTest()
        {
            var creator = new PropertyCreator();
            var typeBuilder = SelectStaticHelpers.TypeBuilderCreator.CreateWithDefaultConstructor("TestType");

            const string propName = "StringProp";
            const string vtPropName = "IntProp";
            const string collPropName = "CollProp";
            creator.CreateAutoProperty(typeBuilder, typeof(string), propName);
            creator.CreateAutoProperty(typeBuilder, typeof(int), vtPropName);
            creator.CreateAutoProperty(typeBuilder, typeof(IEnumerable<string>), collPropName);
            var type = typeBuilder.CreateTypeInfo();
            Assert.IsNotNull(type);
            var stringProp = type.GetProperty(propName);
            var intProp = type.GetProperty(vtPropName);
            var collProp = type.GetProperty(collPropName);

            var instance = Activator.CreateInstance(type.AsType());

            Assert.IsNotNull(stringProp);
            Assert.IsTrue(stringProp.CanRead);
            Assert.IsTrue(stringProp.CanWrite);
            stringProp.SetValue(instance, "123");
            var val = stringProp.GetValue(instance);
            Assert.AreEqual("123", val);

            Assert.IsNotNull(intProp);
            Assert.IsTrue(intProp.CanRead);
            Assert.IsTrue(intProp.CanWrite);
            intProp.SetValue(instance, 123);
            val = intProp.GetValue(instance);
            Assert.AreEqual(123, val);

            Assert.IsNotNull(collProp);
            Assert.IsTrue(collProp.CanRead);
            Assert.IsTrue(collProp.CanWrite);
            var arr = new[] {"1", "2", "3"};
            collProp.SetValue(instance, arr);
            val = collProp.GetValue(instance);
            Assert.AreSame(arr, val);
        }

        [TestMethod]
        public void MakeWriteableShadow()
        {
            var creator = new PropertyCreator();
            var typeBuilder = SelectStaticHelpers.TypeBuilderCreator.CreateWithDefaultConstructor("Derived", typeof(Base));

            creator.MakeWriteableShadow(typeBuilder, typeof(int), "WithoutSetter", "WithSetter");

            var type = typeBuilder.CreateTypeInfo();
            Assert.IsNotNull(type);
            var virtProp = type.GetProperty("WithoutSetter");
            var overProp = type.GetProperty("WithSetter");

            var instance = Activator.CreateInstance(type.AsType());

            Assert.IsNotNull(virtProp);
            Assert.IsNotNull(overProp);
            Assert.IsTrue(overProp.CanRead);
            Assert.IsTrue(overProp.CanWrite);
            overProp.SetValue(instance, 123);
            var val = overProp.GetValue(instance);
            Assert.AreEqual(123, val);
            val = virtProp.GetValue(instance);
            Assert.AreEqual(123, val);
            Assert.AreEqual(123, ((Base)instance).WithoutSetter);
        }

        public class Base
        {
            public virtual int WithoutSetter { get; private set; }
        }
    }
}
