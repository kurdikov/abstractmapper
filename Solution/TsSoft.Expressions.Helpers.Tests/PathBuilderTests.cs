﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using BaseTestClasses.Database;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    [TestClass]
    public class PathBuilderTests
    {
        private ValueHoldingMember Get([NotNull]string propertyName)
        {
            return new ValueHoldingMember(typeof(TestEntity).GetProperty(propertyName));
        }

        [TestMethod]
        public void TestFlatPathBuilding()
        {
            var builder = new PathBuilder(StaticHelpers.Library);
            var path = new List<ValueHoldingMember>
            {
                Get("Parent"),
                Get("Children"),
                Get("Parent"),
                Get("Children"),
                Get("Parent"),
            };
            Expression<Func<TestEntity, IEnumerable<TestEntity>>> expected =
                e => e.Parent.Children.Select(c => c.Parent).SelectMany(c => c.Children).Select(c => c.Parent);
            var actual = builder.Build(path);
            var fixedActual = CollectionReplacerVisitor.Convert(actual);
            ExprAssert.AreEqual(expected, fixedActual);
        }

        [TestMethod]
        public void TestFlatPathBuildingWithDifferentTypes()
        {
            var builder = new PathBuilder(StaticHelpers.Library);
            var path = new List<ValueHoldingMember>
            {
                new ValueHoldingMember(typeof(Second).GetProperty("First")),
                new ValueHoldingMember(typeof(First).GetProperty("SecondWithFkPks")),
                new ValueHoldingMember(typeof(SecondWithFkPk).GetProperty("Third")),
            };
            Expression<Func<Second, IEnumerable<ThirdWithPkPk>>> expected =
                e => e.First.SecondWithFkPks.Select(c => c.Third);
            var actual = builder.Build(path);
            var fixedActual = CollectionReplacerVisitor.Convert(actual);
            ExprAssert.AreEqual(expected, fixedActual);
        }

        [TestMethod]
        public void TestPathBuilding()
        {
            var builder = new PathBuilder(StaticHelpers.Library);
            Expression<Func<TestEntity, bool>> condition1 = e => e.Prop1 == 1;
            Expression<Func<TestEntity, bool>> condition2 = e => e.Prop1 == 2;
            var path = new ParsedPath(
                new List<PathElement>
                {
                    new PathElement(Get("Parent")),
                    new PathElement(Get("Children"), new PathCondition(Expression.Parameter(typeof(TestEntity)), condition1, condition1, new Multidictionary<ParameterExpression, ParsedPath>(), null)),
                    new PathElement(Get("Children"), new PathCondition(Expression.Parameter(typeof(TestEntity)), condition2, condition2, new Multidictionary<ParameterExpression, ParsedPath>(), null)),
                    new PathElement(Get("Parent")),
                    new PathElement(Get("Children")),
                    new PathElement(Get("Parent")),
                    new PathElement(Get("Prop1")),
                },
                Expression.Parameter(typeof(TestEntity)));
            Expression<Func<TestEntity, IEnumerable<int>>> expected =
                e => e.Parent.Children.Where(c => c.Prop1 == 1).SelectMany(c => c.Children).Where(c => c.Prop1 == 2).Select(c => c.Parent).SelectMany(c => c.Children).Select(c => c.Parent)
                .Select(c => c.Prop1);
            var actual = builder.Build(path);
            var fixedActual = CollectionReplacerVisitor.Convert(actual);
            ExprAssert.AreEqual(expected, fixedActual);

        }

        private class CollectionReplacerVisitor : ExpressionVisitor
        {
            public static Expression Convert(Expression input)
            {
                return new CollectionReplacerVisitor().Visit(input);
            }

            protected override Expression VisitMethodCall(MethodCallExpression node)
            {
                var args = node.Arguments;
                if (node.Object == null && args.Count == 2)
                {
                    var lambda = args[1] as LambdaExpression;
                    if (lambda != null)
                    {
                        var lambdaDelegate = lambda.Type;
                        if (lambdaDelegate.GetTypeInfo().IsGenericType && lambdaDelegate.GetGenericTypeDefinition() == typeof(Func<,>))
                        {
                            var returnType = lambdaDelegate.GenericTypeArguments[1];
                            if (returnType.GetTypeInfo().IsGenericType
                                && returnType.GetGenericTypeDefinition() == typeof(ICollection<>))
                            {
                                return node.Update(
                                    null,
                                    new[]
                                    {
                                        base.Visit(args[0]), 
                                        Expression.Lambda(
                                            lambdaDelegate.GetGenericTypeDefinition().MakeGenericType(
                                                lambdaDelegate.GenericTypeArguments[0],
                                                typeof(IEnumerable<>).MakeGenericType(returnType.GenericTypeArguments[0])), 
                                            lambda.Body,
                                            lambda.Parameters),
                                    });
                            }
                        }
                    }
                }
                return base.VisitMethodCall(node);
            }
        }
    }
}
