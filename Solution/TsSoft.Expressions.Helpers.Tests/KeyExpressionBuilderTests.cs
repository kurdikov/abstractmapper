﻿namespace TsSoft.Expressions.Helpers
{
    using System.Collections.Generic;
    using System.Reflection;
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Tuples;

    [TestClass]
    public class KeyExpressionBuilderTests
    {
        [TestMethod]
        public void TestSinglePropertyNonEmptyBuildWhereIn()
        {
            var builder = new KeyExpressionBuilder(StaticHelpers.MemberInfo, StaticHelpers.Library);
            var actual = builder.BuildWhereIn<TestEntity, int>(new List<ValueHoldingMember> {new ValueHoldingMember(typeof(TestEntity).GetProperty("Prop1"))}, new int[] {1, 2});
            ExprAssert.AreEqualWithConstants(e => e.Prop1 == 1 || e.Prop1 == 2, actual);
        }

        [TestMethod]
        public void TestSinglePropertyEmptyBuildWhereIn()
        {
            var builder = new KeyExpressionBuilder(StaticHelpers.MemberInfo, StaticHelpers.Library);
            var actual = builder.BuildWhereIn<TestEntity, int>(new List<ValueHoldingMember> { new ValueHoldingMember(typeof(TestEntity).GetProperty("Prop1")) }, new int[0]);
            ExprAssert.AreEqualWithConstants(e => false, actual);
        }

        [TestMethod]
        public void TestTwoPropertiesNonEmptyBuildWhereIn()
        {
            var builder = new KeyExpressionBuilder(StaticHelpers.MemberInfo, StaticHelpers.Library);
            var actual = builder.BuildWhereIn<TestEntity, EquatableTuple<int, string>>(
                new List<ValueHoldingMember>
                {
                    new ValueHoldingMember(typeof(TestEntity).GetProperty("Prop1")),
                    new ValueHoldingMember(typeof(TestEntity).GetProperty("Prop2")),
                }, 
                new EquatableTuple<int, string>[] {new EquatableTuple<int, string>(1, "1"), new EquatableTuple<int, string>(2, "2"), });
            ExprAssert.AreEqualWithConstants(e => e.Prop1 == 1 && e.Prop2 == "1" || e.Prop1 == 2 && e.Prop2 == "2", actual);
        }

        [TestMethod]
        public void TestTwoPropertiesEmptyBuildWhereIn()
        {
            var builder = new KeyExpressionBuilder(StaticHelpers.MemberInfo, StaticHelpers.Library);
            var actual = builder.BuildWhereIn<TestEntity, EquatableTuple<int, string>>(
                new List<ValueHoldingMember>
                {
                    new ValueHoldingMember(typeof(TestEntity).GetProperty("Prop1")),
                    new ValueHoldingMember(typeof(TestEntity).GetProperty("Prop2")),
                },
                new EquatableTuple<int, string>[0]);
            ExprAssert.AreEqualWithConstants(e => false, actual);
        }
    }
}
