﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BaseTestClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TsSoft.Expressions.Helpers.Async
{
    [TestClass]
    public class AsyncHelperTests
    {
        private SynchronizationContext _context;
        private SynchronizationContext _oldContext;

        [TestInitialize]
        public void Init()
        {
            _context = new TestSynchronizationContext();
            _oldContext = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(_context);
        }

        [TestCleanup]
        public void Cleanup()
        {
            SynchronizationContext.SetSynchronizationContext(_oldContext);
        }

        [TestMethod]
        public void RunSync_Void_WithoutException()
        {
            bool flag = false;
            AsyncHelper.RunSync(async () =>
            {
                await Task.Delay(1);
                flag = true;
            });
            Assert.IsTrue(flag);
            Assert.AreSame(_context, SynchronizationContext.Current);
        }

        [TestMethod]
        public void RunSync_Void_WithException()
        {
            bool flag = false;
            ExceptAssert.Throws<AggregateException>(() => AsyncHelper.RunSync(async () =>
            {
                await Task.Delay(1);
                flag = true;
                throw new Exception();
            }));
            Assert.IsTrue(flag);
            Assert.AreSame(_context, SynchronizationContext.Current);
        }

        [TestMethod]
        public void RunSync_Result_WithoutException()
        {
            var result = AsyncHelper.RunSync(async () =>
            {
                await Task.Delay(1);
                return 1;
            });
            Assert.AreEqual(1, result);
            Assert.AreSame(_context, SynchronizationContext.Current);
        }

        [TestMethod]
        public void RunSync_Result_WithException()
        {
            bool flag = false;
            ExceptAssert.Throws<AggregateException>(() => AsyncHelper.RunSync<int>(async () =>
            {
                await Task.Delay(1);
                flag = true;
                throw new Exception();
            }));
            Assert.IsTrue(flag);
            Assert.AreSame(_context, SynchronizationContext.Current);
        }

        public class TestSynchronizationContext : SynchronizationContext
        {
        }
    }
}
