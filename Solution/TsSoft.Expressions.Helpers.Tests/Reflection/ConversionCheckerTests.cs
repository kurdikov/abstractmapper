﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers.Reflection;

    [TestClass]
    public class ConversionCheckerTests
    {
        [NotNull]
        private ConversionChecker _checker;

        [TestInitialize]
        public void Init()
        {
            _checker = new ConversionChecker();
        }

        private interface IBaseBase
        {
        }

        private interface IBase : IBaseBase
        {
        }

        private class Base : IBase
        {
        }

        private class Derived : Base
        {
        }

        private class ImplicitSource1WithOperator
        {
            public static implicit operator ImplicitTarget1WithoutOperator(ImplicitSource1WithOperator obj)
            {
                return new ImplicitTarget1WithoutOperator();
            }
        }

        private class ImplicitTarget1WithoutOperator
        {
        }

        private class ImplicitSource2WithoutOperator
        {
        }

        private class ImplicitTarget2WithOperator
        {
            public static implicit operator ImplicitTarget2WithOperator(ImplicitSource2WithoutOperator obj)
            {
                return new ImplicitTarget2WithOperator();
            }
        }

        private class ExplicitSource3WithOperator
        {
            public static explicit operator ExplicitTarget3WithoutOperator(ExplicitSource3WithOperator obj)
            {
                return new ExplicitTarget3WithoutOperator();
            }
        }

        private class ExplicitTarget3WithoutOperator
        {
        }

        private class ExplicitSource4WithoutOperator
        {
        }

        private class ExplicitTarget4WithOperator
        {
            public static explicit operator ExplicitTarget4WithOperator(ExplicitSource4WithoutOperator obj)
            {
                return new ExplicitTarget4WithOperator();
            }
        }

        [TestMethod]
        public void TestPrimitiveImplicit()
        {
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(short)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(short)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(short)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(int)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(short)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(int)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(double)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(short)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(int)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(long)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(double)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(short)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(int)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(long)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(float)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(short)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(int)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(long)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(float)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(double)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(char)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(string), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(short)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(int)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(long)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(float)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(double)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(object), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(short)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(int)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(long)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(float)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(double)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(object)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool), typeof(bool)));
        }

        [TestMethod]
        public void TestPrimitiveExplicitOrImplicit()
        {
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(bool)));

            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(byte), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(byte), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(byte), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(byte), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(byte), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(byte), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(byte), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(byte), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(byte), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(byte), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(byte), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(byte), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(sbyte), typeof(bool)));

            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(short), typeof(bool)));

            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(ushort), typeof(bool)));

            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(int), typeof(bool)));

            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(uint), typeof(bool)));
            
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(long), typeof(bool)));

            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(ulong), typeof(bool)));

            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(char), typeof(bool)));

            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(float), typeof(bool)));

            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(double), typeof(bool)));

            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(sbyte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(short)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(int)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(long)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(float)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(double)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(decimal)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(byte)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(ushort)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(uint)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(ulong)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(decimal), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(short)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(int)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(long)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(float)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(double)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(char)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(string), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(short)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(int)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(long)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(float)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(double)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(object)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(object), typeof(bool)));

            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(sbyte)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(short)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(int)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(long)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(float)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(double)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(decimal)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(byte)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(ushort)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(uint)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(ulong)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(char)));
            Assert.IsFalse(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(string)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(object)));
            Assert.IsTrue(_checker.IsPrimitiveExplicitlyConvertible(typeof(bool), typeof(bool)));
        }

        [TestMethod]
        public void TestImplicitOperators()
        {
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ImplicitSource1WithOperator), typeof(ImplicitTarget1WithoutOperator)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ImplicitSource2WithoutOperator), typeof(ImplicitTarget2WithOperator)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ExplicitSource3WithOperator), typeof(ExplicitTarget3WithoutOperator)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ExplicitSource4WithoutOperator), typeof(ExplicitTarget4WithOperator)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ImplicitTarget1WithoutOperator), typeof(ImplicitSource1WithOperator)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ImplicitTarget2WithOperator), typeof(ImplicitSource2WithoutOperator)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ExplicitTarget3WithoutOperator), typeof(ExplicitSource3WithOperator)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ExplicitTarget4WithOperator), typeof(ExplicitSource4WithoutOperator)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ImplicitTarget2WithOperator), typeof(ImplicitSource1WithOperator)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ExplicitTarget4WithOperator), typeof(ExplicitSource3WithOperator)));
        }

        [TestMethod]
        public void TestImplicitHierarchyConverts()
        {
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(IBaseBase), typeof(IBaseBase)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(IBaseBase), typeof(IBase)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(IBaseBase), typeof(Base)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(IBaseBase), typeof(Derived)));

            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(IBase), typeof(IBaseBase)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(IBase), typeof(IBase)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(IBase), typeof(Base)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(IBase), typeof(Derived)));

            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(Base), typeof(IBaseBase)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(Base), typeof(IBase)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(Base), typeof(Base)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(Base), typeof(Derived)));

            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(Derived), typeof(IBaseBase)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(Derived), typeof(IBase)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(Derived), typeof(Base)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(Derived), typeof(Derived)));
        }

        [TestMethod]
        public void TestImplicitNullableConverts()
        {
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(sbyte?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(short?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(int?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(long?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(float?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(double?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(byte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(uint?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(char?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(sbyte?), typeof(bool?)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(sbyte?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(short?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(int?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(long?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(float?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(double?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(decimal?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(byte?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(ushort?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(uint?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(char?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(byte?), typeof(bool?)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(short?), typeof(sbyte?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(short?), typeof(short?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(short?), typeof(int?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(short?), typeof(long?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(short?), typeof(float?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(short?), typeof(double?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(short?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(short?), typeof(byte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(short?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(short?), typeof(uint?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(short?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(short?), typeof(char?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(short?), typeof(bool?)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(short?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(int?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(long?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(float?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(double?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(byte?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(ushort?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(uint?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(char?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ushort?), typeof(bool?)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(int?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(int?), typeof(short?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(int?), typeof(int?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(int?), typeof(long?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(int?), typeof(float?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(int?), typeof(double?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(int?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(int?), typeof(byte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(int?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(int?), typeof(uint?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(int?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(int?), typeof(char?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(int?), typeof(bool?)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(short?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(int?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(long?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(float?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(double?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(byte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(ushort?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(uint?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(char?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(uint?), typeof(bool?)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(long?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(long?), typeof(short?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(long?), typeof(int?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(long?), typeof(long?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(long?), typeof(float?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(long?), typeof(double?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(long?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(long?), typeof(byte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(long?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(long?), typeof(uint?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(long?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(long?), typeof(char?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(long?), typeof(bool?)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(short?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(int?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(long?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(float?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(double?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(byte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(uint?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(char?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(ulong?), typeof(bool?)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(char?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(char?), typeof(short?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(char?), typeof(int?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(char?), typeof(long?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(char?), typeof(float?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(char?), typeof(double?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(char?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(char?), typeof(byte?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(char?), typeof(ushort?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(char?), typeof(uint?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(char?), typeof(ulong?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(char?), typeof(char?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(char?), typeof(bool?)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(float?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(float?), typeof(short?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(float?), typeof(int?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(float?), typeof(long?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(float?), typeof(float?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(float?), typeof(double?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(float?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(float?), typeof(byte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(float?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(float?), typeof(uint?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(float?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(float?), typeof(char?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(float?), typeof(bool?)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(double?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(double?), typeof(short?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(double?), typeof(int?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(double?), typeof(long?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(double?), typeof(float?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(double?), typeof(double?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(double?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(double?), typeof(byte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(double?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(double?), typeof(uint?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(double?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(double?), typeof(char?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(double?), typeof(bool?)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(short?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(int?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(long?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(float?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(double?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(byte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(uint?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(char?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(decimal?), typeof(bool?)));

            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(short?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(int?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(long?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(float?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(double?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(byte?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(uint?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(char?)));
            Assert.IsTrue(_checker.IsImplicitlyConvertible(typeof(bool?), typeof(bool?)));
        }

        [TestMethod]
        public void TestPrimitiveImplicitNullableConverts()
        {
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(short?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(int?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(long?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(float?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(double?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(byte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(uint?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(char?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(sbyte?), typeof(bool?)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(short?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(int?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(long?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(float?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(double?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(decimal?)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(byte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(uint?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(char?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(byte?), typeof(bool?)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(sbyte?)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(short?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(int?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(long?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(float?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(double?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(byte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(uint?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(char?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(short?), typeof(bool?)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(short?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(int?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(long?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(float?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(double?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(byte?)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(uint?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(char?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ushort?), typeof(bool?)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(short?)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(int?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(long?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(float?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(double?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(byte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(uint?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(char?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(int?), typeof(bool?)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(short?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(int?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(long?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(float?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(double?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(byte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(ushort?)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(uint?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(char?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(uint?), typeof(bool?)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(short?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(int?)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(long?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(float?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(double?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(byte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(uint?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(char?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(long?), typeof(bool?)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(short?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(int?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(long?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(float?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(double?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(byte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(uint?)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(char?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(ulong?), typeof(bool?)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(short?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(int?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(long?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(float?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(double?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(byte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(uint?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(ulong?)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(char?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(char?), typeof(bool?)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(short?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(int?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(long?)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(float?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(double?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(byte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(uint?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(char?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(float?), typeof(bool?)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(short?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(int?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(long?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(float?)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(double?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(byte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(uint?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(char?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(double?), typeof(bool?)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(short?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(int?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(long?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(float?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(double?)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(byte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(uint?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(char?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(decimal?), typeof(bool?)));

            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(sbyte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(short?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(int?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(long?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(float?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(double?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(decimal?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(byte?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(ushort?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(uint?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(ulong?)));
            Assert.IsFalse(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(char?)));
            Assert.IsTrue(_checker.IsPrimitiveImplicitlyConvertible(typeof(bool?), typeof(bool?)));
        }

        [TestMethod]
        public void TestExplicitOperators()
        {
            Assert.IsTrue(_checker.IsConvertible(typeof(ImplicitSource1WithOperator), typeof(ImplicitTarget1WithoutOperator)));
            Assert.IsTrue(_checker.IsConvertible(typeof(ImplicitSource2WithoutOperator), typeof(ImplicitTarget2WithOperator)));
            Assert.IsTrue(_checker.IsConvertible(typeof(ExplicitSource3WithOperator), typeof(ExplicitTarget3WithoutOperator)));
            Assert.IsTrue(_checker.IsConvertible(typeof(ExplicitSource4WithoutOperator), typeof(ExplicitTarget4WithOperator)));

            Assert.IsFalse(_checker.IsConvertible(typeof(ImplicitTarget1WithoutOperator), typeof(ImplicitSource1WithOperator)));
            Assert.IsFalse(_checker.IsConvertible(typeof(ImplicitTarget2WithOperator), typeof(ImplicitSource2WithoutOperator)));
            Assert.IsFalse(_checker.IsConvertible(typeof(ExplicitTarget3WithoutOperator), typeof(ExplicitSource3WithOperator)));
            Assert.IsFalse(_checker.IsConvertible(typeof(ExplicitTarget4WithOperator), typeof(ExplicitSource4WithoutOperator)));

            Assert.IsFalse(_checker.IsConvertible(typeof(ImplicitTarget2WithOperator), typeof(ImplicitSource1WithOperator)));
            Assert.IsFalse(_checker.IsConvertible(typeof(ExplicitTarget4WithOperator), typeof(ExplicitSource3WithOperator)));
        }
    }
}
