﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers.Reflection;

    [TestClass()]
    public class MethodInfoHelperTests
    {
        public class Test
        {
            public void Method1() {}

            public int Method2(int x)
            {
                return x;
            }

            public static void Method3() {}

            public T GenericMethod1<T>() { return default(T); }

            public T1 GenericMethod2<T1, T2>(T2 x) { return default(T1); }

            public static void GenericMethod3<T>() {}
        }

        [TestMethod()]
        public void GetMethodInfoTest()
        {
            var helper = new MemberInfoHelper();
            Assert.AreEqual("Method1", helper.GetMethodInfo((Test t) => t.Method1()).Name);
            Assert.AreEqual("Method2", helper.GetMethodInfo((Test t) => t.Method2(0)).Name);
            Assert.AreEqual("Method3", helper.GetMethodInfo(() => Test.Method3()).Name);

            var generic1 = helper.GetGenericDefinitionMethodInfo((Test t) => t.GenericMethod1<int>());
            var generic2 = helper.GetGenericDefinitionMethodInfo((Test t) => t.GenericMethod2<int, int>(0));
            var generic3 = helper.GetGenericDefinitionMethodInfo(() => Test.GenericMethod3<int>());

            Assert.AreEqual("GenericMethod1", generic1.Name);
            Assert.IsTrue(generic1.IsGenericMethod);
            Assert.AreEqual("GenericMethod2", generic2.Name);
            Assert.IsTrue(generic2.IsGenericMethod);
            Assert.AreEqual("GenericMethod3", generic3.Name);
            Assert.IsTrue(generic3.IsGenericMethod);

            ExceptAssert.Throws<ArgumentException>(() => helper.GetMethodInfo((Test t) => t.Method2(0) + t.Method2(1)));
        }

        [TestMethod()]
        public void GetGenericDefinitionMethodInfoTest()
        {
            var helper = new MemberInfoHelper();
            Assert.AreEqual("Method1", helper.GetGenericDefinitionMethodInfo((Test t) => t.Method1()).Name);
            Assert.AreEqual("Method2", helper.GetGenericDefinitionMethodInfo((Test t) => t.Method2(0)).Name);
            Assert.AreEqual("Method3", helper.GetGenericDefinitionMethodInfo(() => Test.Method3()).Name);

            var generic1 = helper.GetGenericDefinitionMethodInfo((Test t) => t.GenericMethod1<int>());
            var generic2 = helper.GetGenericDefinitionMethodInfo((Test t) => t.GenericMethod2<int, int>(0));
            var generic3 = helper.GetGenericDefinitionMethodInfo(() => Test.GenericMethod3<int>());

            Assert.AreEqual("GenericMethod1", generic1.Name);
            Assert.IsTrue(generic1.IsGenericMethodDefinition);
            Assert.AreEqual("GenericMethod2", generic2.Name);
            Assert.IsTrue(generic2.IsGenericMethodDefinition);
            Assert.AreEqual("GenericMethod3", generic3.Name);
            Assert.IsTrue(generic3.IsGenericMethodDefinition);

            ExceptAssert.Throws<ArgumentException>(() => helper.GetGenericDefinitionMethodInfo((Test t) => t.Method2(0) + t.Method2(1)));
        }
    }
}
