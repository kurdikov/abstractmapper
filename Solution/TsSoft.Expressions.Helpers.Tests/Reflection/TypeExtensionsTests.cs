﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Collections;
    using TsSoft.Expressions.Helpers.Reflection;

    [TestClass()]
    public class TypeExtensionsTests
    {
        [TestMethod()]
        public void IsNullableTest()
        {
            Assert.IsTrue(typeof(Guid?).IsNullableStruct());
            Assert.IsTrue(typeof(int?).IsNullableStruct());
            Assert.IsFalse(typeof(string).IsNullableStruct());
            Assert.IsFalse(typeof(TypeExtensionsTests).IsNullableStruct());
            Assert.IsFalse(typeof(int).IsNullableStruct());
            Assert.IsFalse(typeof(Guid).IsNullableStruct());
        }

        [TestMethod()]
        public void IsGenericEnumerableTest()
        {
            Assert.IsTrue(typeof(IEnumerable<int>).IsGenericEnumerable());
            Assert.IsTrue(typeof(IEnumerable<string>).IsGenericEnumerable());
            Assert.IsTrue(typeof(ICollection<TypeExtensionsTests>).IsGenericEnumerable());
            Assert.IsTrue(typeof(HashSet<Guid?>).IsGenericEnumerable());
            Assert.IsTrue(typeof(Guid?[]).IsGenericEnumerable());
            Assert.IsTrue(typeof(Guid[]).IsGenericEnumerable());
            Assert.IsTrue(typeof(TypeExtensionsTests[]).IsGenericEnumerable());
            Assert.IsFalse(typeof(int).IsGenericEnumerable());
            Assert.IsFalse(typeof(Guid?).IsGenericEnumerable());
            Assert.IsFalse(typeof(TypeExtensionsTests).IsGenericEnumerable());
            Assert.IsFalse(typeof(IEnumerable).IsGenericEnumerable());
            Assert.IsFalse(typeof(string).IsGenericEnumerable());

            Assert.IsTrue(new List<TypeExtensionsTests>().Where(o => o != null).GetType().IsGenericEnumerable());
            Assert.IsTrue(new List<int>().Where(o => o > 0).GetType().IsGenericEnumerable());
            Assert.IsTrue(new List<TypeExtensionsTests>().Where(o => o != null).Select(o => o.ToString()).GetType().IsGenericEnumerable());
            Assert.IsTrue(new List<int>().Where(o => o > 0).Select(o => o.ToString()).GetType().IsGenericEnumerable());
            Assert.IsTrue(new List<TypeExtensionsTests>().Where(o => o != null).GroupBy(o => o.ToString()).GetType().IsGenericEnumerable());
            Assert.IsTrue(new List<int>().Where(o => o > 0).GroupBy(o => o.ToString()).GetType().IsGenericEnumerable());
        }

        [TestMethod()]
        public void GetGenericEnumerableArgumentTest()
        {
            Assert.AreEqual(typeof(int), typeof(int[]).GetGenericEnumerableArgument());
            Assert.AreEqual(typeof(string), typeof(string[]).GetGenericEnumerableArgument());
            Assert.AreEqual(typeof(TypeExtensionsTests), typeof(TypeExtensionsTests[]).GetGenericEnumerableArgument());

            Assert.AreEqual(typeof(int), typeof(ICollection<int>).GetGenericEnumerableArgument());
            Assert.AreEqual(typeof(string), typeof(ICollection<string>).GetGenericEnumerableArgument());
            Assert.AreEqual(typeof(TypeExtensionsTests), typeof(ICollection<TypeExtensionsTests>).GetGenericEnumerableArgument());

            Assert.AreEqual(typeof(int), typeof(HashSet<int>).GetGenericEnumerableArgument());
            Assert.AreEqual(typeof(string), typeof(HashSet<string>).GetGenericEnumerableArgument());
            Assert.AreEqual(typeof(TypeExtensionsTests), typeof(HashSet<TypeExtensionsTests>).GetGenericEnumerableArgument());

            Assert.AreEqual(typeof(TypeExtensionsTests), new List<TypeExtensionsTests>().Where(o => o != null).GetType().GetGenericEnumerableArgument());
            Assert.AreEqual(typeof(int), new List<int>().Where(o => o > 0).GetType().GetGenericEnumerableArgument());
            Assert.AreEqual(typeof(string), new List<TypeExtensionsTests>().Where(o => o != null).Select(o => o.ToString()).GetType().GetGenericEnumerableArgument());
            Assert.AreEqual(typeof(string), new List<int>().Where(o => o > 0).Select(o => o.ToString()).GetType().GetGenericEnumerableArgument());
        }
    }
}
