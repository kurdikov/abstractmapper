﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers.Reflection;

    [TestClass()]
    public class TypeHelperTests
    {
        private class Test
        {}

        [TestMethod()]
        public void GetUnderlyingTypeTest()
        {
            var helper = new TypeHelper();
            Assert.AreEqual(typeof(Test), helper.GetCollectionUnderlyingType(typeof(Test)));
            Assert.AreEqual(typeof(Test), helper.GetCollectionUnderlyingType(typeof(IEnumerable<Test>)));
            Assert.AreEqual(typeof(Test), helper.GetCollectionUnderlyingType(typeof(ICollection<Test>)));
            Assert.AreEqual(typeof(Test), helper.GetCollectionUnderlyingType(typeof(IEnumerable<IEnumerable<Test>>)));
            Assert.AreEqual(typeof(Test), helper.GetCollectionUnderlyingType(typeof(IEnumerable<IEnumerable<IEnumerable<Test>>>)));
        }
    }
}
