﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Reflection;

    [TestClass]
    public class CollectionReflectionHelperTests
    {
        [NotNull]
        private CollectionReflectionHelper _helper;

        [TestInitialize]
        public void Init()
        {
            _helper = new CollectionReflectionHelper(StaticHelpers.MemberInfo);
        }

        [TestMethod]
        public void MakeListWhereAllObjectsAreValid()
        {
            object[] array = {1,2,3};
            var list = _helper.MakeList(typeof(int), array) as List<int>;
            Assert.IsNotNull(list);
            Assert.AreEqual(3, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(2, list[1]);
            Assert.AreEqual(3, list[2]);
        }

        [TestMethod]
        public void MakeListWhereSomeObjectsAreInvalid()
        {
            object[] array = { 1, new object(), 3 };
            var list = _helper.MakeList(typeof(int), array) as List<int>;
            Assert.IsNotNull(list);
            Assert.AreEqual(2, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(3, list[1]);
        }

        [TestMethod]
        public void MakeListWhereAllObjectsAreInvalid()
        {
            object[] array = { new object(), new object(), new object() };
            var list = _helper.MakeList(typeof(int), array) as List<int>;
            Assert.IsNotNull(list);
            Assert.AreEqual(0, list.Count);
        }

        [TestMethod]
        public void MakeListStrictWhereAllObjectsAreValid()
        {
            object[] array = { 1, 2, 3 };
            var list = _helper.MakeListStrict(typeof(int), array) as List<int>;
            Assert.IsNotNull(list);
            Assert.AreEqual(3, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(2, list[1]);
            Assert.AreEqual(3, list[2]);
        }

        [TestMethod]
        public void MakeListStrictWhereSomeObjectsAreInvalid()
        {
            object[] array = { 1, new object(), 3 };
            ExceptAssert.Throws<InvalidCastException>(() => _helper.MakeListStrict(typeof(int), array));
        }

        [TestMethod]
        public void MakeListStrictWhereAllObjectsAreInvalid()
        {
            object[] array = { new object(), new object(), new object() };
            ExceptAssert.Throws<InvalidCastException>(() => _helper.MakeListStrict(typeof(int), array));
        }
    }
}
