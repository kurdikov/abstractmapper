﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses.Database;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models.Reflection;

    [TestClass()]
    public class ObjectClonemakerTests
    {
        public class T1
        {
            public int Id { get; set; }
            public bool Bool { get; set; }
            public DateTime Date { get; set; }
            public Guid Guid { get; set; }
            public string String { get; set; }
            public ICollection<T2> ChildrenWithChildren { get; set; }
            public ICollection<T5> ChildrenWithSingleChild { get; set; }
            public ICollection<T6> NotInterestingChildren { get; set; } 
        }
        public class T2
        {
            public int Id { get; set; }
            public ICollection<T3> Children { get; set; } 
        }
        public class T3
        {
            public Guid Id { get; set; }
            public E1 Enum { get; set; }
            public T4 Child { get; set; } 
        }
        public enum E1
        {
            One,
            Two,
            Three
        }
        public class T4
        {
            public int Id { get; set; }
        }
        public class T5
        {
            public Guid Id { get; set; }
            public E1 Enum { get; set; }
            public T4 Child { get; set; }
        }
        public class T6
        {
        }

        [TestMethod()]
        public void CloneTest()
        {
            var entity = new T1
            {
                Id = 1,
                Bool = true,
                Date = new DateTime(1993, 10, 4),
                Guid = Guid.Parse("33333333-3333-3333-3333-333333333333"),
                String = "План",
                ChildrenWithChildren = new[]
                {
                    new T2
                    {
                        Id = 2,
                        Children = new[]
                        {
                            new T3
                            {
                                Id = Guid.Parse("22222222-2222-2222-2222-222222222222"),
                                Enum = E1.One,
                                Child = new T4
                                {
                                    Id = 3,
                                }
                            }
                        }
                    },
                    new T2
                    {
                        Id = 3,
                    }
                },
                ChildrenWithSingleChild = new[]
                {
                    new T5
                    {
                        Id = Guid.Parse("11111111-1111-1111-1111-111111111111"),
                        Enum = E1.Three,
                        Child = new T4
                        {
                            Id = 2,
                        }
                    },
                    new T5
                    {
                        Id = Guid.Parse("44444444-4444-4444-4444-444444444444"),
                        Enum = E1.Three,
                    },
                },
                NotInterestingChildren = new[]
                {
                    new T6(),
                }
            };
            var clonemaker = new ObjectClonemaker(new FlatPathParser(), new MemberInfoHelper(), EntityStaticHelpers.Library);
            var cloneableTypes = new[] {typeof (T1), typeof (T2), typeof (T3), typeof (T4), typeof (T5), typeof (T6)};
            var cloneableCollectionTypes = new[]
                {
                    typeof(ICollection<T1>),
                    typeof(ICollection<T2>),
                    typeof(ICollection<T3>),
                    typeof(ICollection<T4>),
                    typeof(ICollection<T5>),
                    typeof(ICollection<T6>)
                };
            var clones = clonemaker.Clone(new[] { entity }, new Expression<Func<T1, object>>[]
                {
                    p => p.ChildrenWithChildren.Select(pe => pe.Children),
                    p => p.ChildrenWithSingleChild.Select(idp => idp.Child)
                }, new ClonedPropertyManager
                    (
                        copy: pi => pi.PropertyType.GetTypeInfo().IsValueType || pi.PropertyType == typeof(string),
                        singleClone: pi => cloneableTypes.Contains(pi.PropertyType),
                        collectionClone: pi => cloneableCollectionTypes.Contains(pi.PropertyType)
                    )).ToList();


            Assert.AreEqual(1, clones.Count);
            Assert.AreNotEqual(entity, clones[0]);
            Assert.AreEqual(1, clones[0].Id);
            Assert.AreEqual(true, clones[0].Bool);
            Assert.AreEqual(new DateTime(1993, 10, 4), clones[0].Date);
            Assert.AreEqual(Guid.Parse("33333333-3333-3333-3333-333333333333"), clones[0].Guid);
            Assert.AreEqual("План", clones[0].String);
            Assert.AreEqual(null, clones[0].NotInterestingChildren);

            var clonedEvents = clones[0].ChildrenWithChildren.ToList();
            Assert.AreEqual(2, clonedEvents.Count);
            Assert.AreEqual(2, clonedEvents[0].Id);
            Assert.AreEqual(3, clonedEvents[1].Id);
            Assert.AreEqual(1, clonedEvents[0].Children.Count);
            Assert.IsNull(clonedEvents[0].Children.First().Child);
            Assert.AreEqual(null, clonedEvents[1].Children);

            var clonedDocuments = clones[0].ChildrenWithSingleChild.ToList();
            Assert.AreEqual(2, clonedDocuments.Count);
            Assert.AreEqual(Guid.Parse("11111111-1111-1111-1111-111111111111"), clonedDocuments[0].Id);
            Assert.AreEqual(E1.Three, clonedDocuments[0].Enum);
            Assert.IsNotNull(clonedDocuments[0].Child);
            Assert.AreEqual(2, clonedDocuments[0].Child.Id);
            Assert.AreEqual(Guid.Parse("44444444-4444-4444-4444-444444444444"), clonedDocuments[1].Id);
            Assert.AreEqual(E1.Three, clonedDocuments[1].Enum);
            Assert.IsNull(clonedDocuments[1].Child);
        }
    }
}
