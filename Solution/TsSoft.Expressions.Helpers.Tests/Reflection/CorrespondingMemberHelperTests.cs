﻿namespace TsSoft.Expressions.Helpers.Reflection
{
    using System.Linq;
    using System.Reflection;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    [TestClass]
    public class CorrespondingMemberHelperTests
    {
        private PropertyInfo _interGetter = StaticHelpers.MemberInfo.GetPropertyInfo((IInter i) => i.Getter);
        private PropertyInfo _interSetter = typeof(IInter).GetProperty("Setter");
        private PropertyInfo _interFull = StaticHelpers.MemberInfo.GetPropertyInfo((IInter i) => i.Full);
        private PropertyInfo _normalGetter = StaticHelpers.MemberInfo.GetPropertyInfo((Normal i) => i.Getter);
        private PropertyInfo _normalSetter = typeof(Normal).GetProperty("Setter");
        private PropertyInfo _normalFull = StaticHelpers.MemberInfo.GetPropertyInfo((Normal i) => i.Full);
        private PropertyInfo _autoGetter = StaticHelpers.MemberInfo.GetPropertyInfo((Auto i) => i.Getter);
        private PropertyInfo _autoSetter = typeof(Auto).GetProperty("Setter");
        private PropertyInfo _autoFull = StaticHelpers.MemberInfo.GetPropertyInfo((Auto i) => i.Full);
        private PropertyInfo _fullGetter = StaticHelpers.MemberInfo.GetPropertyInfo((FullImpl i) => i.Getter);
        private PropertyInfo _fullSetter = StaticHelpers.MemberInfo.GetPropertyInfo((FullImpl i) => i.Setter);
        private PropertyInfo _fullFull = StaticHelpers.MemberInfo.GetPropertyInfo((FullImpl i) => i.Full);
        private PropertyInfo _explicitGetter = typeof(Explicit).GetProperties(BindingFlags.NonPublic | BindingFlags.Instance).FirstOrDefault(m => m.Name.EndsWith("Getter"));
        private PropertyInfo _explicitSetter = typeof(Explicit).GetProperties(BindingFlags.NonPublic | BindingFlags.Instance).FirstOrDefault(m => m.Name.EndsWith("Setter"));
        private PropertyInfo _explicitFull = typeof(Explicit).GetProperties(BindingFlags.NonPublic | BindingFlags.Instance).FirstOrDefault(m => m.Name.EndsWith("Full"));

        private MethodInfo _interMethod = StaticHelpers.MemberInfo.GetMethodInfo((IInter i) => i.Method());
        private MethodInfo _normalMethod = StaticHelpers.MemberInfo.GetMethodInfo((Normal i) => i.Method());
        private MethodInfo _explicitMethod = typeof(Explicit).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).FirstOrDefault(m => m.Name.EndsWith("Method"));

        private FieldInfo _fieldsGetter = typeof(Fields).GetField("Getter");
        private FieldInfo _fieldsSetter = typeof(Fields).GetField("Setter");
        private FieldInfo _fieldsFull = typeof(Fields).GetField("Full");

        [TestMethod]
        public void GetCorrespondingMemberTest()
        {
            var helper = new CorrespondingMemberHelper();
            Assert.AreEqual(new ValueHoldingMember(_normalGetter), helper.GetCorrespondingMember(typeof(Normal), _interGetter));
            Assert.AreEqual(new ValueHoldingMember(_normalSetter), helper.GetCorrespondingMember(typeof(Normal), _interSetter));
            Assert.AreEqual(new ValueHoldingMember(_normalFull), helper.GetCorrespondingMember(typeof(Normal), _interFull));
            Assert.AreEqual(new ValueHoldingMember(_interGetter), helper.GetCorrespondingMember(typeof(IInter), _normalGetter));
            Assert.AreEqual(new ValueHoldingMember(_interSetter), helper.GetCorrespondingMember(typeof(IInter), _normalSetter));
            Assert.AreEqual(new ValueHoldingMember(_interFull), helper.GetCorrespondingMember(typeof(IInter), _normalFull));
            Assert.AreEqual(new ValueHoldingMember(_fieldsGetter), helper.GetCorrespondingMember(typeof(Fields), _interGetter));
            Assert.AreEqual(new ValueHoldingMember(_fieldsSetter), helper.GetCorrespondingMember(typeof(Fields), _interSetter));
            Assert.AreEqual(new ValueHoldingMember(_fieldsFull), helper.GetCorrespondingMember(typeof(Fields), _interFull));
            Assert.AreEqual(new ValueHoldingMember(_interGetter), helper.GetCorrespondingMember(typeof(IInter), _interGetter));
            Assert.AreEqual(new ValueHoldingMember(_interSetter), helper.GetCorrespondingMember(typeof(IInter), _interSetter));
            Assert.AreEqual(new ValueHoldingMember(_interFull), helper.GetCorrespondingMember(typeof(IInter), _interFull));
            Assert.AreEqual(new ValueHoldingMember(_normalGetter), helper.GetCorrespondingMember(typeof(Normal), _fieldsGetter));
            Assert.AreEqual(new ValueHoldingMember(_normalSetter), helper.GetCorrespondingMember(typeof(Normal), _fieldsSetter));
            Assert.AreEqual(new ValueHoldingMember(_normalFull), helper.GetCorrespondingMember(typeof(Normal), _fieldsFull));
            Assert.AreEqual(new ValueHoldingMember(_fieldsGetter), helper.GetCorrespondingMember(typeof(Fields), _normalGetter));
            Assert.AreEqual(new ValueHoldingMember(_fieldsSetter), helper.GetCorrespondingMember(typeof(Fields), _normalSetter));
            Assert.AreEqual(new ValueHoldingMember(_fieldsFull), helper.GetCorrespondingMember(typeof(Fields), _normalFull));
        }

        [TestMethod]
        public void GetImplementationMethodTest()
        {
            var helper = new CorrespondingMemberHelper();
            Assert.AreEqual(typeof(IInter), _interMethod.DeclaringType);
            Assert.AreEqual(typeof(Normal), _normalMethod.DeclaringType);
            Assert.AreEqual(typeof(Explicit), _explicitMethod.DeclaringType);

            Assert.AreEqual(_normalMethod, helper.GetImplementation(typeof(Normal), _interMethod));
            Assert.AreEqual(_explicitMethod, helper.GetImplementation(typeof(Explicit), _interMethod));
        }

        [TestMethod]
        public void GetImplementationPropertyTest()
        {
            var helper = new CorrespondingMemberHelper();
            Assert.AreEqual(typeof(IInter), _interGetter.DeclaringType);
            Assert.AreEqual(typeof(IInter), _interSetter.DeclaringType);
            Assert.AreEqual(typeof(IInter), _interFull.DeclaringType);
            Assert.AreEqual(typeof(Normal), _normalGetter.DeclaringType);
            Assert.AreEqual(typeof(Normal), _normalSetter.DeclaringType);
            Assert.AreEqual(typeof(Normal), _normalFull.DeclaringType);
            Assert.AreEqual(typeof(Auto), _autoGetter.DeclaringType);
            Assert.AreEqual(typeof(Auto), _autoSetter.DeclaringType);
            Assert.AreEqual(typeof(Auto), _autoFull.DeclaringType);
            Assert.AreEqual(typeof(FullImpl), _fullGetter.DeclaringType);
            Assert.AreEqual(typeof(FullImpl), _fullSetter.DeclaringType);
            Assert.AreEqual(typeof(FullImpl), _fullFull.DeclaringType);
            Assert.AreEqual(typeof(Explicit), _explicitGetter.DeclaringType);
            Assert.AreEqual(typeof(Explicit), _explicitSetter.DeclaringType);
            Assert.AreEqual(typeof(Explicit), _explicitFull.DeclaringType);

            Assert.AreEqual(_normalGetter, helper.GetImplementation(typeof(Normal), _interGetter));
            Assert.AreEqual(_normalSetter, helper.GetImplementation(typeof(Normal), _interSetter));
            Assert.AreEqual(_normalFull, helper.GetImplementation(typeof(Normal), _interFull));
            Assert.AreEqual(_autoGetter, helper.GetImplementation(typeof(Auto), _interGetter));
            Assert.AreEqual(_autoSetter, helper.GetImplementation(typeof(Auto), _interSetter));
            Assert.AreEqual(_autoFull, helper.GetImplementation(typeof(Auto), _interFull));
            Assert.AreEqual(_fullGetter, helper.GetImplementation(typeof(FullImpl), _interGetter));
            Assert.AreEqual(_fullSetter, helper.GetImplementation(typeof(FullImpl), _interSetter));
            Assert.AreEqual(_fullFull, helper.GetImplementation(typeof(FullImpl), _interFull));
            Assert.AreEqual(_explicitGetter, helper.GetImplementation(typeof(Explicit), _interGetter));
            Assert.AreEqual(_explicitSetter, helper.GetImplementation(typeof(Explicit), _interSetter));
            Assert.AreEqual(_explicitFull, helper.GetImplementation(typeof(Explicit), _interFull));
        }

        [TestMethod]
        public void GetImplementationIfInterfacePropertyTest()
        {
            var helper = new CorrespondingMemberHelper();

            Assert.AreEqual(_normalGetter, helper.GetImplementationIfInterfaceProperty(typeof(Normal), _interGetter));
            Assert.AreEqual(_normalSetter, helper.GetImplementationIfInterfaceProperty(typeof(Normal), _interSetter));
            Assert.AreEqual(_normalFull, helper.GetImplementationIfInterfaceProperty(typeof(Normal), _interFull));
            Assert.AreEqual(_autoGetter, helper.GetImplementationIfInterfaceProperty(typeof(Auto), _interGetter));
            Assert.AreEqual(_autoSetter, helper.GetImplementationIfInterfaceProperty(typeof(Auto), _interSetter));
            Assert.AreEqual(_autoFull, helper.GetImplementationIfInterfaceProperty(typeof(Auto), _interFull));
            Assert.AreEqual(_fullGetter, helper.GetImplementationIfInterfaceProperty(typeof(FullImpl), _interGetter));
            Assert.AreEqual(_fullSetter, helper.GetImplementationIfInterfaceProperty(typeof(FullImpl), _interSetter));
            Assert.AreEqual(_fullFull, helper.GetImplementationIfInterfaceProperty(typeof(FullImpl), _interFull));
            Assert.AreEqual(_explicitGetter, helper.GetImplementationIfInterfaceProperty(typeof(Explicit), _interGetter));
            Assert.AreEqual(_explicitSetter, helper.GetImplementationIfInterfaceProperty(typeof(Explicit), _interSetter));
            Assert.AreEqual(_explicitFull, helper.GetImplementationIfInterfaceProperty(typeof(Explicit), _interFull));

            Assert.AreEqual(_normalGetter, helper.GetImplementationIfInterfaceProperty(typeof(Normal), _normalGetter));
            Assert.AreEqual(_normalSetter, helper.GetImplementationIfInterfaceProperty(typeof(Normal), _normalSetter));
            Assert.AreEqual(_normalFull, helper.GetImplementationIfInterfaceProperty(typeof(Normal), _normalFull));
            Assert.AreEqual(_autoGetter, helper.GetImplementationIfInterfaceProperty(typeof(Auto), _autoGetter));
            Assert.AreEqual(_autoSetter, helper.GetImplementationIfInterfaceProperty(typeof(Auto), _autoSetter));
            Assert.AreEqual(_autoFull, helper.GetImplementationIfInterfaceProperty(typeof(Auto), _autoFull));
            Assert.AreEqual(_fullGetter, helper.GetImplementationIfInterfaceProperty(typeof(FullImpl), _fullGetter));
            Assert.AreEqual(_fullSetter, helper.GetImplementationIfInterfaceProperty(typeof(FullImpl), _fullSetter));
            Assert.AreEqual(_fullFull, helper.GetImplementationIfInterfaceProperty(typeof(FullImpl), _fullFull));
            Assert.AreEqual(_explicitGetter, helper.GetImplementationIfInterfaceProperty(typeof(Explicit), _explicitGetter));
            Assert.AreEqual(_explicitSetter, helper.GetImplementationIfInterfaceProperty(typeof(Explicit), _explicitSetter));
            Assert.AreEqual(_explicitFull, helper.GetImplementationIfInterfaceProperty(typeof(Explicit), _explicitFull));
        }

        public interface IInter
        {
            string Getter { get; }
            string Setter { set; }
            string Full { get; set; }
            void Method();
        }

        public class Normal : IInter
        {
            public string Getter { get { return null; } }
            public string Setter { set {} }
            public string Full { get; set; }
            public void Method() {}
        }

        public class Auto : IInter
        {
            public string Getter { get; private set; }
            public string Setter { set; private get; }
            public string Full { get; set; }
            public void Method() { }
        }

        public class FullImpl : IInter
        {
            public string Getter { get; set; }
            public string Setter { set; get; }
            public string Full { get; set; }
            public void Method() { }
        }

        public class Explicit : IInter
        {
            string IInter.Getter { get { return null; } }
            string IInter.Setter { set {} }
            string IInter.Full { get; set; }
            void IInter.Method() { }
        }

        public class Fields
        {
            public string Getter;
            public string Setter;
            public string Full;
        }
    }
}
