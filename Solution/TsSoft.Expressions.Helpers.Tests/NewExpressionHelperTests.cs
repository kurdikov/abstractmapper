﻿namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    [TestClass()]
    public class NewExpressionHelperTests
    {
        [NotNull]
        private NewExpressionHelper _helper;

        TestEntity _etalon = new TestEntity
        {
            Prop1 = 1,
            Prop2 = "2",
            ExternalId = 3,
            External = new ExternalEntity(),
            Parent = new TestEntity(),
            Children = new List<TestEntity>()
        };

        [TestInitialize]
        public void Init()
        {
            var correspondingMemberHelperMock = new Mock<ICorrespondingMemberHelper>(MockBehavior.Strict);
            correspondingMemberHelperMock.Setup(m => m.GetCorrespondingMember(It.IsAny<Type>(), It.IsAny<MemberInfo>()))
                .Returns((Type t, MemberInfo m) => GetProp(m.Name));

            _helper = new NewExpressionHelper(correspondingMemberHelperMock.Object, StaticHelpers.Library, StaticHelpers.MemberNamingHelper);
        }

        private ValueHoldingMember GetProp(string name)
        {
            return new ValueHoldingMember(typeof (TestEntity).GetProperty(name));
        }

        private Func<TestEntity, TestEntity> Compile(
            Expression<Func<TestEntity, object>> original,
            IEnumerable<KeyValuePair<MemberInfo, Expression>> assignments)
        {
            var expr = Expression.Lambda<Func<TestEntity, TestEntity>>(
                Expression.MemberInit(Expression.New(typeof(TestEntity)), assignments.Select(a => Expression.Bind(TestEntity.GetProp(a.Key.Name), a.Value))),
                original != null ? original.Parameters[0] : Expression.Parameter(typeof(TestEntity)));
            return expr.Compile();
        }

        [TestMethod()]
        public void GetAssignmentsFromNull()
        {
            Expression<Func<TestEntity, object>> expr = null;
            var result = _helper.GetAssignments(expr).ToList();
            Assert.AreEqual(0, result.Count);
            var compiledResult = Compile(expr, result);
            var compiledResultResult = compiledResult(_etalon);
            Assert.AreEqual(default(int), compiledResultResult.Prop1);
            Assert.AreEqual(default(string), compiledResultResult.Prop2);
            Assert.AreEqual(default(int), compiledResultResult.ExternalId);
            Assert.AreEqual(default(ExternalEntity), compiledResultResult.External);
            Assert.AreEqual(default(TestEntity), compiledResultResult.Parent);
            Assert.AreEqual(default(ICollection<TestEntity>), compiledResultResult.Children);
        }

        [TestMethod()]
        public void GetAssignmentsFromEmptySelect()
        {
            Expression<Func<TestEntity, object>> expr = e => new object();
            var result = _helper.GetAssignments(expr).ToList();
            Assert.AreEqual(0, result.Count);
            var compiledResult = Compile(expr, result);
            var compiledResultResult = compiledResult(_etalon);
            Assert.AreEqual(default(int), compiledResultResult.Prop1);
            Assert.AreEqual(default(string), compiledResultResult.Prop2);
            Assert.AreEqual(default(int), compiledResultResult.ExternalId);
            Assert.AreEqual(default(ExternalEntity), compiledResultResult.External);
            Assert.AreEqual(default(TestEntity), compiledResultResult.Parent);
            Assert.AreEqual(default(ICollection<TestEntity>), compiledResultResult.Children);
        }

        [TestMethod()]
        public void GetAssignmentsNewPrimitiveProperty()
        {
            Expression<Func<TestEntity, object>> expr = e => new { e.Prop1 };
            var result = _helper.GetAssignments(expr).ToList();
            Assert.AreEqual(1, result.Count);
            var compiledResult = Compile(expr, result);
            var compiledResultResult = compiledResult(_etalon);
            Assert.AreEqual(_etalon.Prop1, compiledResultResult.Prop1);
            Assert.AreEqual(default(string), compiledResultResult.Prop2);
            Assert.AreEqual(default(int), compiledResultResult.ExternalId);
            Assert.AreEqual(default(ExternalEntity), compiledResultResult.External);
            Assert.AreEqual(default(TestEntity), compiledResultResult.Parent);
            Assert.AreEqual(default(ICollection<TestEntity>), compiledResultResult.Children);
        }

        [TestMethod()]
        public void GetAssignmentsNewBareNavigationAndExternalProperties()
        {
            Expression<Func<TestEntity, object>> expr = e => new {e.Prop2, e.External, e.Parent, e.Children};
            var result = _helper.GetAssignments(expr).ToList();
            Assert.AreEqual(4, result.Count);
            var compiledResult = Compile(expr, result);
            var compiledResultResult = compiledResult(_etalon);
            Assert.AreEqual(default(int), compiledResultResult.Prop1);
            Assert.AreEqual(_etalon.Prop2, compiledResultResult.Prop2);
            Assert.AreEqual(default(int), compiledResultResult.ExternalId);
            Assert.AreSame(_etalon.External, compiledResultResult.External);
            Assert.AreSame(_etalon.Parent, compiledResultResult.Parent);
            Assert.AreSame(_etalon.Children, compiledResultResult.Children);
        }

        [TestMethod()]
        public void GetAssignmentsMemberInitPrimitiveProperty()
        {
            Expression<Func<TestEntity, object>> expr = e => new TestEntity { Prop1 = e.Prop1 };
            var result = _helper.GetAssignments(expr).ToList();
            Assert.AreEqual(1, result.Count);
            var compiledResult = Compile(expr, result);
            var compiledResultResult = compiledResult(_etalon);
            Assert.AreEqual(_etalon.Prop1, compiledResultResult.Prop1);
            Assert.AreEqual(default(string), compiledResultResult.Prop2);
            Assert.AreEqual(default(int), compiledResultResult.ExternalId);
            Assert.AreEqual(default(ExternalEntity), compiledResultResult.External);
            Assert.AreEqual(default(TestEntity), compiledResultResult.Parent);
            Assert.AreEqual(default(ICollection<TestEntity>), compiledResultResult.Children);
        }

        [TestMethod()]
        public void GetAssignmentsMemberInitBareNavigationAndExternalProperties()
        {
            Expression<Func<TestEntity, object>> expr = e => new TestEntity { Prop2 = e.Prop2, External = e.External, Parent = e.Parent, Children = e.Children };
            var result = _helper.GetAssignments(expr).ToList();
            Assert.AreEqual(4, result.Count);
            var compiledResult = Compile(expr, result);
            var compiledResultResult = compiledResult(_etalon);
            Assert.AreEqual(default(int), compiledResultResult.Prop1);
            Assert.AreEqual(_etalon.Prop2, compiledResultResult.Prop2);
            Assert.AreEqual(default(int), compiledResultResult.ExternalId);
            Assert.AreSame(_etalon.External, compiledResultResult.External);
            Assert.AreSame(_etalon.Parent, compiledResultResult.Parent);
            Assert.AreSame(_etalon.Children, compiledResultResult.Children);
        }

        [NotNull]
        private MemberAssignment ExtractAssignment([NotNull]Expression<Func<TestEntity, object>> expr)
        {
            return ((MemberInitExpression) expr.Body).Bindings.Cast<MemberAssignment>().Single();
        }

        [NotNull]
        private MemberAssignment ExtractAssignment([NotNull]Expression expr)
        {
            return ((MemberInitExpression)expr).Bindings.Cast<MemberAssignment>().Single();
        }

        [TestMethod()]
        public void ParsePrimitiveAssignment()
        {
            var parsed = _helper.ParseAssignment(ExtractAssignment(e => new TestEntity {Prop1 = e.Prop1}), typeof (TestEntity));
            MemberAssert.AreEqual(GetProp("Prop1"), parsed.UsedMember);
            Assert.IsNull(parsed.InnerSelect);
            Assert.IsNull(parsed.Condition);
            Assert.IsNull(parsed.InnerAssignment);
        }

        [TestMethod()]
        public void ParseObjectAssignment()
        {
            var assignment = ExtractAssignment(
                e => new TestEntity {Parent = new TestEntity {Prop2 = e.Parent.Prop2}});
            var parsed = _helper.ParseAssignment(assignment, typeof(TestEntity));
            MemberAssert.AreEqual(GetProp("Parent"), parsed.UsedMember);
            Assert.IsNull(parsed.InnerSelect);
            Assert.IsNull(parsed.Condition);
            Assert.AreEqual(1, parsed.InnerAssignment.Count);
            Assert.AreEqual(TestEntity.GetProp("Prop2"), parsed.InnerAssignment.First().Key);
            Assert.AreSame(
                ExtractAssignment(assignment.Expression).Expression, 
                parsed.InnerAssignment.First().Value);
        }

        [TestMethod()]
        public void ParseCollectionAssignment()
        {
            var assignment = ExtractAssignment(
                e => new TestEntity
                {
                    Children = e.Children.Select(
                        c => new TestEntity
                        {
                            ExternalId = c.ExternalId,
                            Children = c.Children.Where(cc => cc.Prop1 == 2).Select(cc => new TestEntity()).ToList(),
                        }).ToList()
                });
            var parsed = _helper.ParseAssignment(assignment, typeof(TestEntity));
            MemberAssert.AreEqual(GetProp("Children"), parsed.UsedMember);
            Expression<Func<TestEntity, TestEntity>> innerSelect = c => new TestEntity
            {
                ExternalId = c.ExternalId,
                Children = c.Children.Where(cc => cc.Prop1 == 2).Select(cc => new TestEntity()).ToList(),
            };
            ExprAssert.AreEqual(innerSelect, parsed.InnerSelect);
            Assert.IsNull(parsed.Condition);
            Assert.IsNull(parsed.InnerAssignment);
        }

        [TestMethod()]
        public void ParseCollectionAssignmentWithCondition()
        {
            var assignment = ExtractAssignment(e => new TestEntity { Children = e.Children.Where(cc => cc.Prop1 == 2).Select(cc => new TestEntity()).ToList()});
            var parsed = _helper.ParseAssignment(assignment, typeof(TestEntity));
            Expression<Func<TestEntity, TestEntity>> innerInnerSelect = cc => new TestEntity();
            Expression<Func<TestEntity, bool>> innerWhere = cc => cc.Prop1 == 2;
            MemberAssert.AreEqual(GetProp("Children"), parsed.UsedMember);
            ExprAssert.AreEqual(parsed.InnerSelect, innerInnerSelect);
            ExprAssert.AreEqual(parsed.Condition, innerWhere);
            Assert.AreEqual(null, parsed.InnerAssignment);
        }
    }
}
