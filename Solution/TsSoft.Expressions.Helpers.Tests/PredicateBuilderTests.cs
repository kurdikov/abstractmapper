// ReSharper disable RedundantLogicalConditionalExpressionOperand
namespace TsSoft.Expressions.Helpers
{
    using System;
    using System.Linq.Expressions;
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class PredicateBuilderTests
    {
        private readonly Expression<Func<string, bool>> _exprTrue = s => true;
        private readonly Expression<Func<string, bool>> _exprFalse = s => false;

        [TestMethod]
        public void TestAnd()
        {
            Expression<Func<string, bool>> expr = s => true && s.Length > 0 && s[0] == '1';
            ExprAssert.AreEqualWithConstants(expr, _exprTrue.And(x => x.Length > 0).And(x => x[0] == '1'));
        }

        [TestMethod]
        public void TestOr()
        {
            Expression<Func<string, bool>> expr = s => false || s.Length > 0 || s[0] == '1';
            ExprAssert.AreEqualWithConstants(expr, _exprFalse.Or(x => x.Length > 0).Or(x => x[0] == '1'));
        }

        [TestMethod]
        public void TestOrAnd()
        {
            Expression<Func<string, bool>> expr = s => (false || s.Length > 0 || s[0] == '1') && s.StartsWith("11");
            ExprAssert.AreEqualWithConstants(expr, _exprFalse.Or(x => x.Length > 0).Or(x => x[0] == '1').And(x => x.StartsWith("11")));
        }
    }
}
// ReSharper restore RedundantLogicalConditionalExpressionOperand
