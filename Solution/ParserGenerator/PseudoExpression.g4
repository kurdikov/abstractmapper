grammar PseudoExpression;

@parser::header {#pragma warning disable 3021}
@lexer::header {#pragma warning disable 3021}

/*
 * Parser Rules
 */

expression : 
	expression DOT ID typeparams? BRACKETOPEN BRACKETCLOSE									# MethodCall
	| expression DOT ID typeparams? BRACKETOPEN argument+=expression (COMMA argument+=expression)* COMMA? BRACKETCLOSE	# MethodWithParametersCall
	| expression DOT ID																# MemberAccess
	| expression IN expression														# InOperator
	| expression NOTIN expression													# NotInOperator
	| expression ISEMPTY															# IsEmptyOperator
	| expression ISNOTEMPTY															# IsNotEmptyOperator
	| expression CONTAINS expression												# ContainsOperator
	| expression STARTSWITH expression												# StartsWithOperator
	| expression ENDSWITH expression												# EndsWithOperator
	| expression op=CAST type														# TypeOperator
	| expression op=TYPEAS type														# TypeOperator
	| expression op=TYPEIS type														# TypeOperator
	| sign=(PLUS | MINUS | NOT | COMPLEMENT) expression								# Unary
	| expression sign=(TIMES | DIVIDE | MODULO) expression							# Binary
	| expression sign=(PLUS | MINUS) expression										# Binary
	| expression sign=(LEFTSHIFT | RIGHTSHIFT) expression							# Binary
	| expression sign=(LESS | GREATER | GREATEREQUALS | LESSEQUALS) expression		# Binary
	| expression sign=(EQUALS | NOTEQUALS) expression								# Binary
	| expression sign=BITAND expression												# Binary
	| expression sign=BITXOR expression												# Binary
	| expression sign=BITOR expression												# Binary
	| expression sign=AND expression												# Binary
	| expression sign=OR expression													# Binary
	| expression sign=COALESCE expression											# Binary
	| expression SQBRACKETOPEN expression SQBRACKETCLOSE							# ArrayAccess
	| ID LAMBDAARROW expression							# Lambda
	| BRACKETOPEN lambdaparam (COMMA lambdaparam)* COMMA? BRACKETCLOSE LAMBDAARROW expression # MultipleParametersLambda
	| SQBRACKETOPEN expression (COMMA expression)* COMMA? SQBRACKETCLOSE # ArrayConstant
	| closure=CLOSURE? BOOLEAN							# BooleanConstant
	| closure=CLOSURE? DECIMAL							# DecimalConstant
	| closure=CLOSURE? DOUBLE							# DoubleConstant
	| closure=CLOSURE? SINGLE							# FloatConstant
	| closure=CLOSURE? HEXLONGINTEGER					# HexLongIntegerConstant
	| closure=CLOSURE? HEXBYTEINTEGER					# HexByteIntegerConstant
	| closure=CLOSURE? HEXSHORTINTEGER					# HexShortIntegerConstant
	| closure=CLOSURE? HEXINTEGER						# HexIntegerConstant
	| closure=CLOSURE? LONGINTEGER						# LongIntegerConstant
	| closure=CLOSURE? BYTEINTEGER						# ByteIntegerConstant
	| closure=CLOSURE? SHORTINTEGER						# ShortIntegerConstant
	| closure=CLOSURE? INTEGER							# IntegerConstant
	| closure=CLOSURE? DATE								# DateConstant
	| closure=CLOSURE? TIME								# TimeConstant
	| closure=CLOSURE? UUID								# UuidConstant
	| closure=CLOSURE? CHAR								# CharConstant
	| closure=CLOSURE? STRING							# StringConstant
	| TYPE												# TypeConstant
	| STATICTYPE										# StaticMembersType
	| NULL												# Null
	| ID												# Identifier
	| BRACKETOPEN expression BRACKETCLOSE				# Parens;

lambdaparam:
	type? name=ID #LambdaParameter;

typeparams:
	LESS (type COMMA)* (type)? GREATER #TypeParameters;

type:
	ID ((DOT | PLUS) ID)* (DIVIDE ID (DOT ID)*)?;

/*
 * Lexer Rules
 */

IN : 'in';
NOTIN : 'not in';
ISEMPTY : 'is empty';
ISNOTEMPTY : 'is not empty';
CONTAINS : 'contains';
STARTSWITH : 'starts with';
ENDSWITH : 'ends with';
CAST: 'cast';
TYPEAS: 'as';
TYPEIS: 'is';

OR : ('||' | 'or');
AND : ('&&' | 'and');
NOT : ('!' | 'not');
BITOR : '|';
BITAND : '&';
BITXOR : ('^' | 'xor');
EQUALS : '==';
NOTEQUALS : '!=';
LEFTSHIFT : '<<';
RIGHTSHIFT : '>>';
LESSEQUALS : '<=';
LESS : '<';
GREATEREQUALS : '>=';
GREATER : '>';

PLUS : '+';
MINUS : '-';
TIMES : '*';
DIVIDE : '/';
MODULO : '%';
COMPLEMENT : '~';

DOT : '.';
COMMA : ',';
BRACKETOPEN : '(';
BRACKETCLOSE : ')';
SQBRACKETOPEN : '[';
SQBRACKETCLOSE : ']';

COALESCE : '??';
CLOSURE: '`';


LAMBDAARROW : '=>';

NULL : 'null';
BOOLEAN	: 'false' | 'true';
DATE	: 'date(' ~[)]* ')';
TIME	: 'time(' ~[)]* ')';
UUID	: 'uuid(' ~[)]+ ')';
TYPE	: 'type(' ~[)]+ ')';
STATICTYPE	: 'static(' ~[)]+ ')';
DECIMAL: FLOAT 'm' | INTEGER 'm';
DOUBLE: FLOAT 'd'? | INTEGER 'd';
SINGLE: FLOAT 'f' | INTEGER 'f';

LONGINTEGER: INTEGER 'l';
BYTEINTEGER: INTEGER 'y';
SHORTINTEGER: INTEGER 's';
HEXLONGINTEGER: HEXINTEGER 'l';
HEXBYTEINTEGER: HEXINTEGER 'y';
HEXSHORTINTEGER: HEXINTEGER 's';

HEXINTEGER : '0x' HEXDIGIT+;
INTEGER	: [0-9]+;
FLOAT	: [0-9]+ '.' [0-9]+;
STRING	: '"' (ESCAPED | ~[\\"])* '"';
ESCAPED	: (ESCAPEDSYMBOL | ESCAPEDUNICODE);
ESCAPEDSYMBOL : '\\' ['"\\0abfnrt];
ESCAPEDUNICODE : '\\u' HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT | '\\u' HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT;
ID		: [A-Za-z] [A-Za-z0-9]*;
CHAR	: '\'' (ESCAPED | ~[\\"])* '\'';
WHITESPACE	:	(' ' | '\t' | '\n' | '\r') -> channel(HIDDEN) ;

fragment HEXDIGIT : [0-9A-Fa-f];
