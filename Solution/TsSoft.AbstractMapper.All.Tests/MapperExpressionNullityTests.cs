﻿namespace TsSoft.AbstractMapper.All.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Threading.Tasks;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.AbstractMapper.Mappers;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.EntityService.Mapper;
    using TsSoft.Expressions.Helpers.Closures;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.Models.Reflection;

    /// <summary>
    /// Проверяет очистку MapperExpression после построения всех объектов маппера
    /// </summary>
    [TestClass]
    public class MapperExpressionNullityTests
    {
        private static readonly FieldInfo Field = typeof(Mapper<From, SimpleTo>).GetField(
            "_mapperExpression", BindingFlags.Instance | BindingFlags.NonPublic);

        private object ExtractExpression([NotNull]Mapper<From, SimpleTo> mapper)
        {
            Assert.IsNotNull(Field);
            return Field.GetValue(mapper);
        }

        [TestMethod]
        public void TestMapper()
        {
            var mapper = AbstractMapperFactory.Create<Mapper<From, SimpleTo>>(MockObjects.None, Entities.Are(typeof(From)));
            Assert.IsNotNull(ExtractExpression(mapper));
            mapper.Map(new From());
            Assert.IsNull(ExtractExpression(mapper));
        }

        [TestMethod]
        public void TestLooseMapper()
        {
            var mapper = AbstractMapperFactory.Create<LooseMapper<From, SimpleTo>>(MockObjects.None, Entities.Are(typeof(From)));
            Assert.IsNotNull(ExtractExpression(mapper));
            mapper.Map(new From());
            Assert.IsNull(ExtractExpression(mapper));
        }

        [TestMethod]
        public void TestDynamicMapper()
        {
            var mapper = AbstractMapperFactory.Create<DynamicMapper<From, SimpleTo>>(MockObjects.None, Entities.Are(typeof(From)));
            Assert.IsNotNull(ExtractExpression(mapper));
            mapper.Map(new From());
            Assert.IsNull(ExtractExpression(mapper));
        }

        [TestMethod]
        public void TestSelectMapperMapThenSelect()
        {
            var mapper = AbstractMapperFactory.Create<SelectProviderMapper<From, SimpleTo>>(
                MockObjects.None, Entities.Are(typeof(From)));
            Assert.IsNotNull(ExtractExpression(mapper));
            mapper.Map(new From());
            Assert.IsNotNull(ExtractExpression(mapper));
            Assert.IsNotNull(mapper.Select);
            Assert.IsNull(ExtractExpression(mapper));
        }

        [TestMethod]
        public void TestSelectMapperSelectThenMap()
        {
            var mapper = AbstractMapperFactory.Create<SelectProviderMapper<From, SimpleTo>>(MockObjects.None, Entities.Are(typeof(From)));
            Assert.IsNotNull(ExtractExpression(mapper));
            Assert.IsNotNull(mapper.Select);
            Assert.IsNotNull(ExtractExpression(mapper));
            mapper.Map(new From());
            Assert.IsNull(ExtractExpression(mapper));
        }

        [TestMethod]
        public void TestIncludeMapperMapThenIncludes()
        {
            var mapper = AbstractMapperFactory.Create<IncludeProviderMapper<From, SimpleTo>>(
                MockObjects.None, Entities.Are(typeof(From)));
            Assert.IsNotNull(ExtractExpression(mapper));
            mapper.Map(new From());
            Assert.IsNotNull(ExtractExpression(mapper));
            Assert.IsNotNull(mapper.Includes);
            Assert.IsNull(ExtractExpression(mapper));

        }

        [TestMethod]
        public void TestIncludeMapperIncludesThenMap()
        {
            var mapper = AbstractMapperFactory.Create<IncludeProviderMapper<From, SimpleTo>>(MockObjects.None, Entities.Are(typeof(From)));
            Assert.IsNotNull(ExtractExpression(mapper));
            Assert.IsNotNull(mapper.Includes);
            Assert.IsNotNull(ExtractExpression(mapper));
            mapper.Map(new From());
            Assert.IsNull(ExtractExpression(mapper));
        }

        private IAbstractUpdateEntityMapperHelper GetMockHelper()
        {
            var mockObj = new Mock<IAbstractUpdateEntityMapperHelper>(MockBehavior.Loose);
            mockObj.Setup(o => o.MakeFullRules(It.IsAny<IMapRules<SimpleTo, From>>(), It.IsAny<IgnoreRules<SimpleTo>>(), It.IsAny<AutoPropertiesBehavior>(), It.IsAny<InnerMapperStrategy>()))
                .Returns(new FullMapRules(typeof(SimpleTo)));
            mockObj.Setup(o => o.CreateMapperExpression<From, SimpleTo>(It.IsAny<IFullMapRules>(), It.IsAny<bool>(), It.IsAny<string>()))
                .Returns(new MapperExpression<From, SimpleTo> { Expression = from => new SimpleTo() });
            mockObj.Setup(o => o.GetUpdateFunc(It.IsAny<IEnumerable<Expression<Func<SimpleTo, object>>>>(), It.IsAny<IObjectUpdateManager>(), It.IsAny<IEnumerable<Expression<Func<SimpleTo, object>>>>()))
                .Returns((a, b) => { });
            mockObj.Setup(o => o.GetAsyncUpdateFunc(It.IsAny<IEnumerable<Expression<Func<SimpleTo, object>>>>(), It.IsAny<IObjectUpdateManager>(), It.IsAny<IEnumerable<Expression<Func<SimpleTo, object>>>>()))
                .Returns((a, b) => Enumerable.Empty<Func<Task>>());
            mockObj.Setup(o => o.GetUpdatePaths<SimpleTo>(It.IsAny<IEnumerable<MappedPathDescription>>(), It.IsAny<bool>(), It.IsAny<GeneratorContext>()))
                .Returns(new Expression<Func<SimpleTo, object>>[0]);
            mockObj.Setup(o => o.GetIncludes(It.IsAny<IEnumerable<Expression<Func<SimpleTo, object>>>>(), It.IsAny<IEnumerable<Expression<Func<SimpleTo, object>>>>()))
                .Returns(new Includes<SimpleTo>());
            mockObj.Setup(o => o.Compile(It.IsAny<Expression<Func<From, SimpleTo>>>(), It.IsAny<LambdaCompilationType>()))
                .Returns(new Func<From, SimpleTo>((from) => new SimpleTo()));
            return mockObj.Object;
        }

        [TestMethod]
        public void TestUpdateFuncMapperMapThenIncludes()
        {
            var mapper = AbstractMapperFactory.Create<UpdateEntityMapper<From, SimpleTo>>(MockObjects.Are(GetMockHelper()), Entities.Are(typeof(From)),
                useMocksForHelper: true);
            Assert.IsNotNull(ExtractExpression(mapper));
            mapper.Map(new From());
            Assert.IsNotNull(ExtractExpression(mapper));
            Assert.IsNotNull(mapper.Includes);
            Assert.IsNull(ExtractExpression(mapper));
            Assert.IsNotNull(mapper.Updater);
            Assert.IsNotNull(mapper.AsyncUpdater);
        }

        [TestMethod]
        public void TestUpdateFuncMapperIncludesThenMap()
        {
            var mapper = AbstractMapperFactory.Create<UpdateEntityMapper<From, SimpleTo>>(MockObjects.Are(GetMockHelper()), Entities.Are(typeof(From)),
                useMocksForHelper: true);
            Assert.IsNotNull(ExtractExpression(mapper));
            Assert.IsNotNull(mapper.Includes);
            Assert.IsNotNull(ExtractExpression(mapper));
            mapper.Map(new From());
            Assert.IsNull(ExtractExpression(mapper));
            Assert.IsNotNull(mapper.Updater);
            Assert.IsNotNull(mapper.AsyncUpdater);
        }

        [TestMethod]
        public void TestUpdatePathMapperMapThenPaths()
        {
            var mapper = AbstractMapperFactory.Create<UpdatePathProviderMapper<From, SimpleTo>>(MockObjects.None, Entities.Are(typeof(From)));
            Assert.IsNotNull(ExtractExpression(mapper));
            mapper.Map(new From());
            Assert.IsNotNull(ExtractExpression(mapper));
            Assert.IsNotNull(mapper.Paths);
            Assert.IsNull(ExtractExpression(mapper));
        }

        [TestMethod]
        public void TestUpdatePathMapperPathsThenMap()
        {
            var mapper = AbstractMapperFactory.Create<UpdatePathProviderMapper<From, SimpleTo>>(MockObjects.None, Entities.Are(typeof(From)));
            Assert.IsNotNull(ExtractExpression(mapper));
            Assert.IsNotNull(mapper.Paths);
            Assert.IsNotNull(ExtractExpression(mapper));
            mapper.Map(new From());
            Assert.IsNull(ExtractExpression(mapper));
        }
    }
}
