﻿#if NET45
namespace TsSoft.AbstractMapper.All.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Ninject;
    using Ninject.Modules;
    using TsSoft.AbstractMapper.NinjectResolver;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.BindingsDescription;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;

    [TestClass]
    public class KernelFactoryTests
    {
        public class ThisAssemblyBindings : BindingsDescription
        {
            public ThisAssemblyBindings()
            {
                Bind<IThisAssembly, ThisAssembly>();
            }
        }

        public interface IThisAssembly
        {
        }
        public class ThisAssembly : IThisAssembly
        {   
        }

        public interface IThisAssemblyFromModule
        {
        }
        public class ThisAssemblyFromModule : IThisAssemblyFromModule
        {
        }

        public class ThisAssemblyModule : NinjectModule
        {
            public override void Load()
            {
                Bind<IThisAssemblyFromModule>().To<ThisAssemblyFromModule>().InThreadScope();
            }
        }

        public void ShouldNotBeBound<T>(IKernel kernel, [CallerLineNumber] int line = 0)
        {
            ExceptAssert.Throws<ActivationException>(() => kernel.Get<T>(), "Type {0} is bound while it shouldn't be. Called from line {1}", typeof(T), line);
        }

        [NotNull] private IAssemblyTypesRetriever _assemblyTypesRetriever;
        [NotNull] private IEntityTypesRetriever _entityTypesRetriever;

        [TestInitialize]
        public void Init()
        {
            var mockAssemblyTypesRetriever = new Mock<IAssemblyTypesRetriever>(MockBehavior.Strict);
            mockAssemblyTypesRetriever.Setup(ar => ar.GetTypes(Assembly.GetAssembly(GetType())))
                .Returns(new[]
                    {
                       typeof(ThisAssemblyBindings), 
                    });
            _assemblyTypesRetriever = mockAssemblyTypesRetriever.Object;

            var mockEntityTypesRetriever = new Mock<IEntityTypesRetriever>(MockBehavior.Strict);
            mockEntityTypesRetriever.Setup(mr => mr.IsEntityType(It.IsAny<Type>()))
                                    .Returns((Type t) => t == typeof(From));
            mockEntityTypesRetriever.Setup(mr => mr.IsEntityCollectionType(It.IsAny<Type>()))
                                    .Returns((Type t) => t == typeof(ICollection<From>));
            mockEntityTypesRetriever.Setup(mr => mr.GetEntityTypes())
                                    .Returns(new HashSet<Type> { typeof(From) });

            _entityTypesRetriever = mockEntityTypesRetriever.Object;
        }


        [TestMethod]
        public void TestCreateKernelWithNoAssembliesWithoutMapperBinding()
        {
            var kernel = KernelFactory.CreateKernel(
                _assemblyTypesRetriever,
                null,
                null,
                false,
                null);
            kernel.Get<IMemberInfoHelper>();
            kernel.Get<IMapperResolver>();
            ShouldNotBeBound<IMapper<From, To>>(kernel);
            ShouldNotBeBound<IThisAssembly>(kernel);
            ShouldNotBeBound<IThisAssemblyFromModule>(kernel);
        }

        [TestMethod]
        public void TestCreateKernelWithNoAssembliesWithMapperBinding()
        {
            var kernel = KernelFactory.CreateKernel(
                _assemblyTypesRetriever,
                null,
                null,
                true,
                null);
            kernel.Get<IMemberInfoHelper>();
            kernel.Get<IMapperResolver>();
            var mapperType = kernel.Get<IMapper<From, To>>().GetType();
            Assert.AreEqual(typeof(Mapper<From, To>), mapperType);
            ShouldNotBeBound<IThisAssembly>(kernel);
            ShouldNotBeBound<IThisAssemblyFromModule>(kernel);
        }

        [TestMethod]
        public void TestCreateKernelWithOneAssemblyWithMapperBinding()
        {
            var kernel = KernelFactory.CreateKernel(
                _assemblyTypesRetriever,
                new[] { typeof(ThisAssembly) },
                null,
                true,
                null);
            kernel.Get<IMemberInfoHelper>();
            kernel.Get<IMapperResolver>();
            var mapperType = kernel.Get<IMapper<From, To>>().GetType();
            Assert.AreEqual(typeof(Mapper<From, To>), mapperType);
            kernel.Get<IThisAssembly>();
            ShouldNotBeBound<IThisAssemblyFromModule>(kernel);
        }

        [TestMethod]
        public void TestCreateKernelWithOneAssemblyWithMapperBindingWithModule()
        {
            var kernel = KernelFactory.CreateKernel(
                _assemblyTypesRetriever,
                new[] { typeof(ThisAssembly) },
                new INinjectModule[] { new ThisAssemblyModule(), },
                true,
                null);
            kernel.Get<IMemberInfoHelper>();
            kernel.Get<IMapperResolver>();
            var mapperType = kernel.Get<IMapper<From, To>>().GetType();
            Assert.AreEqual(typeof(Mapper<From, To>), mapperType);
            kernel.Get<IThisAssembly>();
            kernel.Get<IThisAssemblyFromModule>();
        }

        [TestMethod]
        public void CreateKernelTestWithOneAssemblyWithMapperBindingWithModuleWithEntities()
        {
            var kernel = KernelFactory.CreateKernel(
                _assemblyTypesRetriever,
                new[] { typeof(ThisAssembly) },
                new INinjectModule[] { new ThisAssemblyModule(), },
                true,
                _entityTypesRetriever);
            kernel.Bind<IEntityTypesRetriever>().ToConstant(_entityTypesRetriever).InThreadScope();
            kernel.Get<IMemberInfoHelper>();
            kernel.Get<IMapperResolver>();
            var mapperType = kernel.Get<IMapper<TestEntity, To>>().GetType();
            Assert.AreEqual(typeof(Mapper<TestEntity, To>), mapperType);
            mapperType = kernel.Get<IMapper<From, To>>().GetType();
            Assert.AreEqual(typeof(SelectProviderMapper<From, To>), mapperType);
            ShouldNotBeBound<IMapper<To, From>>(kernel);
            kernel.Get<IThisAssembly>();
            kernel.Get<IThisAssemblyFromModule>();
        }
    }
}
#endif
