﻿namespace TsSoft.EntityRepository.Tests.ComplexEntityRepository
{
    using System.Collections.Generic;
    using BaseTestClasses.Database;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.EntityRepository.Helpers;

    [TestClass()]
    public class ComplexUpdateRepositoryTests : RepositoryTestBase
    {
        private readonly IEnumerable<ComplexEntity> _items = new[]
        {
            new ComplexEntity
            {
                Id = 1,
                IsSaved = false,
            },
            new ComplexEntity
            {
                Id = 2,
                IsSaved = false,
            },
        };

        [TestInitialize]
        public void Init()
        {
            TestInit();
            Seed(_items);
        }

        [TestMethod()]
        public void TestUpdateSingle()
        {
            var repository = new ComplexUpdateRepository<ComplexEntity>(
                ContextWrapper, 
                IncludeProcessor,
                new DatabaseCommitter(ContextWrapper),
                new Transactor(),
                new Detacher(),
                new RepositoryHelper());

            var updatedNothing = repository.UpdateSingle(entity => { }, entity => entity.Id == 1);
            Assert.IsTrue(updatedNothing.IsSaved);

            var updatedIsSaved = repository.UpdateSingle(entity => entity.IsSaved = false, entity => entity.Id == 1);
            Assert.IsFalse(updatedIsSaved.IsSaved);
        }
    }
}
