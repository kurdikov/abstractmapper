﻿namespace TsSoft.EntityRepository.Tests.ComplexEntityRepository
{
    using System.Linq;
    using BaseTestClasses.Database;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.EntityRepository.Helpers;

    [TestClass]
    public class ComplexEntityReadRepositoryTests : RepositoryTestBase
    {
        private ComplexEntityReadRepository<ComplexEntity> _readRepository;

        [TestInitialize]
        public void Init()
        {

            TestInit();
            Seed(new []
                {
                    new ComplexEntity
                        {
                            Id = 1,
                            IsSaved = false,
                        },
                    new ComplexEntity
                        {
                            Id = 2,
                            IsSaved = true
                        },
                    new ComplexEntity
                        {
                            Id = 3,
                            IsSaved = true
                        }
                });
            _readRepository = new ComplexEntityReadRepository<ComplexEntity>(ContextWrapper, IncludeProcessor,
                                                        EntityStaticHelpers.MapperResolver.GetTypeDeterminingMapper<ComplexEntity>(),
                                                        EntityStaticHelpers.IncludeDescriptionHelper, new RepositoryHelper());
        }

        [TestMethod]
        public void TestGet()
        {

            var res = _readRepository.Get(entity => entity.Id == 1);
            Assert.AreEqual(0, res.Count);
            res = _readRepository.Get(entity => entity.Id < 4);
            Assert.AreEqual(2, res.Count);

        }
        
        [TestMethod]
        public void TestGetSingle()
        {

            var res = _readRepository.GetSingle(entity => entity.Id == 1);
            Assert.IsNull(res);

            res = _readRepository.GetSingle(entity => entity.Id == 2);
            Assert.IsNotNull(res);
            Assert.IsTrue(res.IsSaved);


        }

        [TestMethod]
        public void TestGetSingleWithStubs()
        {

            var res = _readRepository.GetSingleWithStubs(entity => entity.Id == 1);
            Assert.IsNotNull(res);
            Assert.IsFalse(res.IsSaved);
        }

        [TestMethod]
        public void TestGetAll()
        {
            var res = _readRepository.GetAll();
            Assert.AreEqual(2, res.Count);
            Assert.IsNotNull(res.SingleOrDefault(x => x.Id == 2));
            Assert.IsNotNull(res.SingleOrDefault(x => x.Id == 3));
        }


    }
}