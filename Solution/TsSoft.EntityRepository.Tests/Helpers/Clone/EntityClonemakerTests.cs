﻿namespace TsSoft.EntityRepository.Tests.Helpers.Clone
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using BaseTestClasses.Database;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.EntityRepository.Helpers.Clone;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;

    [TestClass]
    public class EntityClonemakerTests : RepositoryTestBase
    {
        [TestMethod]
        public void CloneTest()
        {
            var data = new[]
            {
                new First
                {
                    ClientId = 1,
                    IsDeleted = true,
                    ImportantMoment = new DateTime(1993, 10, 4),
                    Name = "Name",
                    Seconds = new[]
                    {
                        new Second {ClientId = 2, Type = "MustBeCloned 1",},
                        new Second {ClientId = 3, Type = "MustBeCloned 2"},
                    },
                    SecondWithFkPks = new[]
                    {
                        new SecondWithFkPk {Id = Guid.Parse("11111111-1111-1111-1111-111111111111"), Type = "One", 
                            Third = new ThirdWithPkPk {Id = Guid.Parse("11111111-1111-1111-1111-111111111111"), Data = 2}},
                        new SecondWithFkPk {Id = Guid.Parse("22222222-2222-2222-2222-222222222222"), Type = "Two",
                            Third = new ThirdWithPkPk {Id = Guid.Parse("22222222-2222-2222-2222-222222222222"), Data = 1}},
                    },
                    NotInterestingSeconds = new[]
                    {
                        new NotInterestingSecond {ClientId = 1},      
                        new NotInterestingSecond {ClientId = 2},      
                        new NotInterestingSecond {ClientId = 3},      
                    },
                },
                new First
                {
                    ClientId = 2,
                    IsDeleted = false,
                    Name = "Name",
                    Seconds = new[]
                    {
                        new Second {ClientId = 4, Type = "MustNotBeCloned 1"},
                        new Second {ClientId = 5, Type = "MustNotBeCloned 2"},
                    },
                    SecondWithFkPks = new[]
                    {
                        new SecondWithFkPk {Id = Guid.Empty, Type = "Null"},
                    },
                    NotInterestingSeconds = new[]
                    {
                        new NotInterestingSecond {ClientId = 4},      
                        new NotInterestingSecond {ClientId = 5},      
                    },
                }
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<First>();
            var clonemaker = new EntityClonemaker<First>(
                repository,
                EntityStaticHelpers.EntityTypesHelper,
                EntityStaticHelpers.FlatPathParser,
                EntityStaticHelpers.MemberInfo,
                new ObjectClonemaker(EntityStaticHelpers.FlatPathParser, EntityStaticHelpers.MemberInfo, EntityStaticHelpers.Library));
            var paths = new Includes<First>
                {
                    p => p.Seconds,
                    p => p.SecondWithFkPks.Select(idp => idp.Third)
                };
            var clones = clonemaker.Clone(p => p.ClientId == 1, paths,
                                          p =>
                                          {
                                              p.ClientId = 3;
                                              p.Name = "Clone";
                                              p.IsDeleted = false;
                                          }).ToList();

            Assert.AreEqual(1, clones.Count);

            var cloneId = clones[0].Id;
            clones = repository.Get(p => p.Id == cloneId, paths).ToList();

            Assert.AreNotEqual(data[0].Id, clones[0].Id);
            Assert.AreNotEqual(data[1].Id, clones[0].Id);
            Assert.AreEqual(false, clones[0].IsDeleted);
            Assert.AreEqual(3, clones[0].ClientId);
            Assert.AreEqual(data[0].ImportantMoment, clones[0].ImportantMoment);
            Assert.AreEqual("Clone", clones[0].Name);
            Assert.AreEqual(0, clones[0].NotInterestingSeconds.Count);

            var clonedSeconds = clones[0].Seconds.ToList();
            Assert.AreEqual(2, clonedSeconds.Count);
            Assert.IsTrue(clonedSeconds.Any(e => e.Type == "MustBeCloned 1"));
            Assert.IsTrue(clonedSeconds.Any(e => e.Type == "MustBeCloned 2"));
            Assert.IsFalse(clonedSeconds.Any(e => data.SelectMany(d => d.Seconds).Select(s => s.Id).Contains(e.Id)));

            var clonedDocuments = clones[0].SecondWithFkPks.ToList();
            Assert.AreEqual(2, clonedDocuments.Count);
            var firstDoc = clonedDocuments.SingleOrDefault(cd => cd.Type == "One");
            Assert.IsNotNull(firstDoc);
            Assert.AreNotEqual(Guid.Parse("11111111-1111-1111-1111-111111111111"), firstDoc.Id);
            Assert.IsNotNull(firstDoc.Third);
            Assert.AreEqual(2, firstDoc.Third.Data);
            var secondDoc = clonedDocuments.SingleOrDefault(cd => cd.Type == "Two");
            Assert.IsNotNull(secondDoc);
            Assert.AreNotEqual(Guid.Parse("22222222-2222-2222-2222-222222222222"), secondDoc.Id);
            Assert.IsNotNull(secondDoc.Third);
            Assert.AreEqual(1, secondDoc.Third.Data);
        }

        [TestMethod]
        public void NullFkTest()
        {
            var data = new[]
                {
                    new ThirdWithPkPk
                        {
                            Data = 1,
                            Id = Guid.NewGuid(),
                            Second = new SecondWithFkPk
                                {
                                    Type = "fawdf"
                                }
                        },
                    new ThirdWithPkPk
                        {
                            Data = 2,
                            Id = Guid.NewGuid()
                        },
                };
            Seed(data);
            var repo = RepositoryFactory.GetBaseRepository<ThirdWithPkPk>();
            var clonemaker = new EntityClonemaker<ThirdWithPkPk>(repo,
                                                        EntityStaticHelpers.EntityTypesHelper,
                                                        EntityStaticHelpers.FlatPathParser,
                                                        EntityStaticHelpers.MemberInfo,
                                                        new ObjectClonemaker(EntityStaticHelpers.FlatPathParser,
                                                                             EntityStaticHelpers.MemberInfo, EntityStaticHelpers.Library));
            var clones = clonemaker.Clone(t => true, 
                new Includes<ThirdWithPkPk>
                    {
                        e => e.Second
                    }, 
                null).ToList();
            Assert.AreEqual(2, clones.Count);
            Assert.AreEqual(data[0].Second.Type, clones.Single(d => d.Data == 1).Second.Type);
            Assert.IsNull(clones.Single(d => d.Data == 2).Second);

        }

        [TestMethod]
        public void TreeFkTest()
        {
            var data = new[]
                {
                    new Tree {Data = "1", Children = new[] {new Tree { Data = "1.1", Children = new[] {new Tree {Data = "1.1.1"}, }}, new Tree {Data = "1.2"}}},
                    new Tree {Data = "2"},
                    new Tree {Data = "3", Children = new[] {new Tree {Data = "3.1"}, }}
                };
            Seed(data);

            var repo = RepositoryFactory.GetBaseRepository<Tree>();
            var clonemaker = new EntityClonemaker<Tree>(repo,
                                                        EntityStaticHelpers.EntityTypesHelper,
                                                        EntityStaticHelpers.FlatPathParser,
                                                        EntityStaticHelpers.MemberInfo,
                                                        new ObjectClonemaker(EntityStaticHelpers.FlatPathParser,
                                                                             EntityStaticHelpers.MemberInfo, EntityStaticHelpers.Library));
            var clones = clonemaker.Clone(t => t.Data.StartsWith("1") || t.Data.StartsWith("2"), null, t => { t.Data = "new" + t.Data; }, new ICyclicForeignKey<Tree>[]
                {
                    new TreeForeignKeyDescription<Tree, int>(),
                }).ToList();


            Assert.AreEqual(5, clones.Count);
            var first = clones.FirstOrDefault(c => c.Data == "new1");
            Assert.IsNotNull(first);
            var second = clones.FirstOrDefault(c => c.Data == "new2");
            Assert.IsNotNull(second);

            clones = repo.Get(t => t.Data.StartsWith("new") && t.ParentId == null, new Expression<Func<Tree, object>>[] { t => t.Children.Select(c => c.Children) }).ToList();
            Assert.AreEqual(2, clones.Count);
            first = clones.FirstOrDefault(c => c.Data == "new1");
            Assert.IsNotNull(first);
            Assert.AreEqual(2, first.Children.Count);
            var firstChildWithChild = first.Children.FirstOrDefault(c => c.Data == "new1.1");
            Assert.IsNotNull(firstChildWithChild);
            Assert.AreEqual(1, firstChildWithChild.Children.Count);
            Assert.IsNotNull(firstChildWithChild.Children.FirstOrDefault(c => c.Data == "new1.1.1"));
            Assert.IsNotNull(first.Children.FirstOrDefault(c => c.Data == "new1.2"));
            second = clones.FirstOrDefault(c => c.Data == "new2");
            Assert.IsNotNull(second);
            Assert.AreEqual(0, second.Children.Count);
        }

        [TestMethod]
        public void CloneAndSetFksTest()
        {
            var diamondOnes = new[]
                {
                    new DiamondOne {Data = 1},
                    new DiamondOne {Data = 2},
                    new DiamondOne {Data = 3},
                    new DiamondOne {Data = 4},
                    new DiamondOne {Data = 5},
                };
            var diamondTwos = new[]
                {
                    new DiamondTwo {Data = 1},
                    new DiamondTwo {Data = 2},
                    new DiamondTwo {Data = 3},
                    new DiamondTwo {Data = 4},
                    new DiamondTwo {Data = 5},
                    new DiamondTwo {Data = 6},
                };
            var data = new[]
                {
                    new First()
                        {
                            Name = "Name",
                            IsDeleted = false,
                            DiamondOnes = diamondOnes,
                            DiamondTwos = diamondTwos,
                            DiamondBottoms = new[]
                                {
                                    new DiamondBottom {Data = 1},
                                    new DiamondBottom 
                                        {
                                            Data = 2, 
                                            DiamondTwos = new[] {diamondTwos[1]}
                                        },
                                    new DiamondBottom 
                                        {
                                            Data = 3, 
                                            DiamondOnes = new[] {diamondOnes[1]}
                                        },
                                    new DiamondBottom 
                                        {
                                            Data = 4, 
                                            DiamondOnes = new[] {diamondOnes[2], diamondOnes[4]}, 
                                            DiamondTwos = new[] {diamondTwos[2], diamondTwos[5]}
                                        },
                                    new DiamondBottom 
                                        {
                                            Data = 5, 
                                            DiamondOnes = new[] {diamondOnes[3]}, 
                                            DiamondTwos = new[] {diamondTwos[3], diamondTwos[4]}
                                        },
                                },
                        }
                };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<First>();
            var clonemaker = new EntityClonemaker<First>(
                repository,
                EntityStaticHelpers.EntityTypesHelper,
                EntityStaticHelpers.FlatPathParser,
                EntityStaticHelpers.MemberInfo,
                new ObjectClonemaker(EntityStaticHelpers.FlatPathParser, EntityStaticHelpers.MemberInfo, EntityStaticHelpers.Library));

            var paths = new Includes<First>
                {
                    f => f.DiamondOnes,
                    f => f.DiamondTwos,
                    f => f.DiamondBottoms,
                };

            var id = data[0].Id;
            var before = repository.GetSingle(p => p.Id == id, paths);
            Assert.IsNotNull(before);
            var oneIds = diamondOnes.Select(o => o.Id).ToList();
            var twoIds = diamondTwos.Select(t => t.Id).ToList();
            var bIds = data[0].DiamondBottoms.Select(b => b.Id).ToList();

            var clones = clonemaker.Clone(p => p.Id == id, paths, spi => { spi.IsDeleted = true; },
                                          new ICyclicForeignKey<First>[]
                                              {
                                                  new CyclicForeignKeyDescription<First, DiamondOne, DiamondBottom, int>
                                                      {
                                                          ReferencedEntitySelector = s => s.DiamondBottoms,
                                                          ContainerSelector = s => s.DiamondOnes,
                                                          FkSelector = s => s.DiamondBottomId,
                                                          FkNavigationSetter = (se, b) => se.DiamondBottom = b,
                                                      },
                                                  new CyclicForeignKeyDescription<First, DiamondTwo, DiamondBottom, int>
                                                      {
                                                          ReferencedEntitySelector = s => s.DiamondBottoms,
                                                          ContainerSelector = s => s.DiamondTwos,
                                                          FkSelector = s => s.DiamondBottomId,
                                                          FkNavigationSetter = (se, b) => se.DiamondBottom = b,
                                                      },
                                              }).ToList();
            Assert.IsNotNull(clones);
            Assert.AreEqual(1, clones.Count);

            var clone = clones.First();
            Assert.IsNotNull(clone);
            Assert.AreEqual(true, clone.IsDeleted);

            clone = repository.GetSingle(sp => sp.Id == clone.Id, paths);
            Assert.AreNotEqual(data[0].Id, clone.Id);
            Assert.AreNotEqual(8, clone.Id);
            var oneClones = clone.DiamondOnes;
            var twoClones = clone.DiamondTwos;
            var bClones = clone.DiamondBottoms;
            Assert.AreEqual(5, oneClones.Count);
            Assert.AreEqual(6, twoClones.Count);
            Assert.AreEqual(5, bClones.Count);
            Assert.IsFalse(oneClones.Any(d => oneIds.Contains(d.Id)));
            Assert.IsFalse(twoClones.Any(d => twoIds.Contains(d.Id)));
            Assert.IsFalse(bClones.Any(d => bIds.Contains(d.Id)));
            var sortedOnes = Enumerable.Range(1, 5).Select(i => oneClones.SingleOrDefault(c => c.Data == i)).ToList();
            var sortedTwos = Enumerable.Range(1, 6).Select(i => twoClones.SingleOrDefault(c => c.Data == i)).ToList();
            var sortedBs = Enumerable.Range(1, 5).Select(i => bClones.SingleOrDefault(c => c.Data == i)).ToList();
            foreach (var one in sortedOnes)
            {
                Assert.IsNotNull(one);
            }
            foreach (var two in sortedTwos)
            {
                Assert.IsNotNull(two);
            }
            foreach (var b in sortedBs)
            {
                Assert.IsNotNull(b);
            }

            Assert.AreEqual(null, sortedTwos[0].DiamondBottomId);
            Assert.AreEqual(sortedBs[1].Id, sortedTwos[1].DiamondBottomId);
            Assert.AreEqual(sortedBs[3].Id, sortedTwos[2].DiamondBottomId);
            Assert.AreEqual(sortedBs[4].Id, sortedTwos[3].DiamondBottomId);
            Assert.AreEqual(sortedBs[4].Id, sortedTwos[4].DiamondBottomId);
            Assert.AreEqual(sortedBs[3].Id, sortedTwos[5].DiamondBottomId);

            Assert.AreEqual(null, sortedOnes[0].DiamondBottomId);
            Assert.AreEqual(sortedBs[2].Id, sortedOnes[1].DiamondBottomId);
            Assert.AreEqual(sortedBs[3].Id, sortedOnes[2].DiamondBottomId);
            Assert.AreEqual(sortedBs[4].Id, sortedOnes[3].DiamondBottomId);
            Assert.AreEqual(sortedBs[3].Id, sortedOnes[4].DiamondBottomId);
        }


        [TestMethod]
        public async Task CloneAsyncTest()
        {
            var data = new[]
            {
                new First
                {
                    ClientId = 1,
                    IsDeleted = true,
                    ImportantMoment = new DateTime(1993, 10, 4),
                    Name = "Name",
                    Seconds = new[]
                    {
                        new Second {ClientId = 2, Type = "MustBeCloned 1",},
                        new Second {ClientId = 3, Type = "MustBeCloned 2"},
                    },
                    SecondWithFkPks = new[]
                    {
                        new SecondWithFkPk {Id = Guid.Parse("11111111-1111-1111-1111-111111111111"), Type = "One", 
                            Third = new ThirdWithPkPk {Id = Guid.Parse("11111111-1111-1111-1111-111111111111"), Data = 2}},
                        new SecondWithFkPk {Id = Guid.Parse("22222222-2222-2222-2222-222222222222"), Type = "Two",
                            Third = new ThirdWithPkPk {Id = Guid.Parse("22222222-2222-2222-2222-222222222222"), Data = 1}},
                    },
                    NotInterestingSeconds = new[]
                    {
                        new NotInterestingSecond {ClientId = 1},      
                        new NotInterestingSecond {ClientId = 2},      
                        new NotInterestingSecond {ClientId = 3},      
                    },
                },
                new First
                {
                    ClientId = 2,
                    IsDeleted = false,
                    Name = "Name",
                    Seconds = new[]
                    {
                        new Second {ClientId = 4, Type = "MustNotBeCloned 1"},
                        new Second {ClientId = 5, Type = "MustNotBeCloned 2"},
                    },
                    SecondWithFkPks = new[]
                    {
                        new SecondWithFkPk {Id = Guid.Empty, Type = "Null"},
                    },
                    NotInterestingSeconds = new[]
                    {
                        new NotInterestingSecond {ClientId = 4},      
                        new NotInterestingSecond {ClientId = 5},      
                    },
                }
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<First>();
            var clonemaker = new EntityClonemaker<First>(
                repository,
                EntityStaticHelpers.EntityTypesHelper,
                EntityStaticHelpers.FlatPathParser,
                EntityStaticHelpers.MemberInfo,
                new ObjectClonemaker(EntityStaticHelpers.FlatPathParser, EntityStaticHelpers.MemberInfo, EntityStaticHelpers.Library));
            var paths = new Includes<First>
                {
                    p => p.Seconds,
                    p => p.SecondWithFkPks.Select(idp => idp.Third)
                };
            var clones = (await clonemaker.CloneAsync(p => p.ClientId == 1, paths,
                                          p =>
                                          {
                                              p.ClientId = 3;
                                              p.Name = "Clone";
                                              p.IsDeleted = false;
                                          })).ToList();

            Assert.AreEqual(1, clones.Count);

            var cloneId = clones[0].Id;
            clones = repository.Get(p => p.Id == cloneId, paths).ToList();

            Assert.AreNotEqual(data[0].Id, clones[0].Id);
            Assert.AreNotEqual(data[1].Id, clones[0].Id);
            Assert.AreEqual(false, clones[0].IsDeleted);
            Assert.AreEqual(3, clones[0].ClientId);
            Assert.AreEqual(data[0].ImportantMoment, clones[0].ImportantMoment);
            Assert.AreEqual("Clone", clones[0].Name);
            Assert.AreEqual(0, clones[0].NotInterestingSeconds.Count);

            var clonedSeconds = clones[0].Seconds.ToList();
            Assert.AreEqual(2, clonedSeconds.Count);
            Assert.IsTrue(clonedSeconds.Any(e => e.Type == "MustBeCloned 1"));
            Assert.IsTrue(clonedSeconds.Any(e => e.Type == "MustBeCloned 2"));
            Assert.IsFalse(clonedSeconds.Any(e => data.SelectMany(d => d.Seconds).Select(s => s.Id).Contains(e.Id)));

            var clonedDocuments = clones[0].SecondWithFkPks.ToList();
            Assert.AreEqual(2, clonedDocuments.Count);
            var firstDoc = clonedDocuments.SingleOrDefault(cd => cd.Type == "One");
            Assert.IsNotNull(firstDoc);
            Assert.AreNotEqual(Guid.Parse("11111111-1111-1111-1111-111111111111"), firstDoc.Id);
            Assert.IsNotNull(firstDoc.Third);
            Assert.AreEqual(2, firstDoc.Third.Data);
            var secondDoc = clonedDocuments.SingleOrDefault(cd => cd.Type == "Two");
            Assert.IsNotNull(secondDoc);
            Assert.AreNotEqual(Guid.Parse("22222222-2222-2222-2222-222222222222"), secondDoc.Id);
            Assert.IsNotNull(secondDoc.Third);
            Assert.AreEqual(1, secondDoc.Third.Data);
        }

        [TestMethod]
        public async Task NullFkAsyncTest()
        {
            var data = new[]
                {
                    new ThirdWithPkPk
                        {
                            Data = 1,
                            Id = Guid.NewGuid(),
                            Second = new SecondWithFkPk
                                {
                                    Type = "fawdf"
                                }
                        },
                    new ThirdWithPkPk
                        {
                            Data = 2,
                            Id = Guid.NewGuid()
                        },
                };
            Seed(data);
            var repo = RepositoryFactory.GetBaseRepository<ThirdWithPkPk>();
            var clonemaker = new EntityClonemaker<ThirdWithPkPk>(repo,
                                                        EntityStaticHelpers.EntityTypesHelper,
                                                        EntityStaticHelpers.FlatPathParser,
                                                        EntityStaticHelpers.MemberInfo,
                                                        new ObjectClonemaker(EntityStaticHelpers.FlatPathParser,
                                                                             EntityStaticHelpers.MemberInfo, EntityStaticHelpers.Library));
            var clones = (await clonemaker.CloneAsync(t => true,
                new Includes<ThirdWithPkPk>
                    {
                        e => e.Second
                    },
                null)).ToList();
            Assert.AreEqual(2, clones.Count);
            Assert.AreEqual(data[0].Second.Type, clones.Single(d => d.Data == 1).Second.Type);
            Assert.IsNull(clones.Single(d => d.Data == 2).Second);

        }

        [TestMethod]
        public async Task TreeFkAsyncTest()
        {
            var data = new[]
                {
                    new Tree {Data = "1", Children = new[] {new Tree { Data = "1.1", Children = new[] {new Tree {Data = "1.1.1"}, }}, new Tree {Data = "1.2"}}},
                    new Tree {Data = "2"},
                    new Tree {Data = "3", Children = new[] {new Tree {Data = "3.1"}, }}
                };
            Seed(data);

            var repo = RepositoryFactory.GetBaseRepository<Tree>();
            var clonemaker = new EntityClonemaker<Tree>(repo,
                                                        EntityStaticHelpers.EntityTypesHelper,
                                                        EntityStaticHelpers.FlatPathParser,
                                                        EntityStaticHelpers.MemberInfo,
                                                        new ObjectClonemaker(EntityStaticHelpers.FlatPathParser,
                                                                             EntityStaticHelpers.MemberInfo, EntityStaticHelpers.Library));
            var clones = (await clonemaker.CloneAsync(t => t.Data.StartsWith("1") || t.Data.StartsWith("2"), null, t => { t.Data = "new" + t.Data; }, new ICyclicForeignKey<Tree>[]
                {
                    new TreeForeignKeyDescription<Tree, int>(),
                })).ToList();


            Assert.AreEqual(5, clones.Count);
            var first = clones.FirstOrDefault(c => c.Data == "new1");
            Assert.IsNotNull(first);
            var second = clones.FirstOrDefault(c => c.Data == "new2");
            Assert.IsNotNull(second);

            clones = repo.Get(t => t.Data.StartsWith("new") && t.ParentId == null, new Expression<Func<Tree, object>>[] { t => t.Children.Select(c => c.Children) }).ToList();
            Assert.AreEqual(2, clones.Count);
            first = clones.FirstOrDefault(c => c.Data == "new1");
            Assert.IsNotNull(first);
            Assert.AreEqual(2, first.Children.Count);
            var firstChildWithChild = first.Children.FirstOrDefault(c => c.Data == "new1.1");
            Assert.IsNotNull(firstChildWithChild);
            Assert.AreEqual(1, firstChildWithChild.Children.Count);
            Assert.IsNotNull(firstChildWithChild.Children.FirstOrDefault(c => c.Data == "new1.1.1"));
            Assert.IsNotNull(first.Children.FirstOrDefault(c => c.Data == "new1.2"));
            second = clones.FirstOrDefault(c => c.Data == "new2");
            Assert.IsNotNull(second);
            Assert.AreEqual(0, second.Children.Count);
        }

        [TestMethod]
        public async Task CloneAndSetFksAsyncTest()
        {
            var diamondOnes = new[]
                {
                    new DiamondOne {Data = 1},
                    new DiamondOne {Data = 2},
                    new DiamondOne {Data = 3},
                    new DiamondOne {Data = 4},
                    new DiamondOne {Data = 5},
                };
            var diamondTwos = new[]
                {
                    new DiamondTwo {Data = 1},
                    new DiamondTwo {Data = 2},
                    new DiamondTwo {Data = 3},
                    new DiamondTwo {Data = 4},
                    new DiamondTwo {Data = 5},
                    new DiamondTwo {Data = 6},
                };
            var data = new[]
                {
                    new First()
                        {
                            Name = "Name",
                            IsDeleted = false,
                            DiamondOnes = diamondOnes,
                            DiamondTwos = diamondTwos,
                            DiamondBottoms = new[]
                                {
                                    new DiamondBottom {Data = 1},
                                    new DiamondBottom 
                                        {
                                            Data = 2, 
                                            DiamondTwos = new[] {diamondTwos[1]}
                                        },
                                    new DiamondBottom 
                                        {
                                            Data = 3, 
                                            DiamondOnes = new[] {diamondOnes[1]}
                                        },
                                    new DiamondBottom 
                                        {
                                            Data = 4, 
                                            DiamondOnes = new[] {diamondOnes[2], diamondOnes[4]}, 
                                            DiamondTwos = new[] {diamondTwos[2], diamondTwos[5]}
                                        },
                                    new DiamondBottom 
                                        {
                                            Data = 5, 
                                            DiamondOnes = new[] {diamondOnes[3]}, 
                                            DiamondTwos = new[] {diamondTwos[3], diamondTwos[4]}
                                        },
                                },
                        }
                };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<First>();
            var clonemaker = new EntityClonemaker<First>(
                repository,
                EntityStaticHelpers.EntityTypesHelper,
                EntityStaticHelpers.FlatPathParser,
                EntityStaticHelpers.MemberInfo,
                new ObjectClonemaker(EntityStaticHelpers.FlatPathParser, EntityStaticHelpers.MemberInfo, EntityStaticHelpers.Library));

            var paths = new Includes<First>
                {
                    f => f.DiamondOnes,
                    f => f.DiamondTwos,
                    f => f.DiamondBottoms,
                };

            var id = data[0].Id;
            var before = repository.GetSingle(p => p.Id == id, paths);
            Assert.IsNotNull(before);
            var oneIds = diamondOnes.Select(o => o.Id).ToList();
            var twoIds = diamondTwos.Select(t => t.Id).ToList();
            var bIds = data[0].DiamondBottoms.Select(b => b.Id).ToList();

            var clones = (await clonemaker.CloneAsync(p => p.Id == id, paths, spi => { spi.IsDeleted = true; },
                                          new ICyclicForeignKey<First>[]
                                              {
                                                  new CyclicForeignKeyDescription<First, DiamondOne, DiamondBottom, int>
                                                      {
                                                          ReferencedEntitySelector = s => s.DiamondBottoms,
                                                          ContainerSelector = s => s.DiamondOnes,
                                                          FkSelector = s => s.DiamondBottomId,
                                                          FkNavigationSetter = (se, b) => se.DiamondBottom = b,
                                                      },
                                                  new CyclicForeignKeyDescription<First, DiamondTwo, DiamondBottom, int>
                                                      {
                                                          ReferencedEntitySelector = s => s.DiamondBottoms,
                                                          ContainerSelector = s => s.DiamondTwos,
                                                          FkSelector = s => s.DiamondBottomId,
                                                          FkNavigationSetter = (se, b) => se.DiamondBottom = b,
                                                      },
                                              })).ToList();
            Assert.IsNotNull(clones);
            Assert.AreEqual(1, clones.Count);

            var clone = clones.First();
            Assert.IsNotNull(clone);
            Assert.AreEqual(true, clone.IsDeleted);

            clone = repository.GetSingle(sp => sp.Id == clone.Id, paths);
            Assert.AreNotEqual(data[0].Id, clone.Id);
            Assert.AreNotEqual(8, clone.Id);
            var oneClones = clone.DiamondOnes;
            var twoClones = clone.DiamondTwos;
            var bClones = clone.DiamondBottoms;
            Assert.AreEqual(5, oneClones.Count);
            Assert.AreEqual(6, twoClones.Count);
            Assert.AreEqual(5, bClones.Count);
            Assert.IsFalse(oneClones.Any(d => oneIds.Contains(d.Id)));
            Assert.IsFalse(twoClones.Any(d => twoIds.Contains(d.Id)));
            Assert.IsFalse(bClones.Any(d => bIds.Contains(d.Id)));
            var sortedOnes = Enumerable.Range(1, 5).Select(i => oneClones.SingleOrDefault(c => c.Data == i)).ToList();
            var sortedTwos = Enumerable.Range(1, 6).Select(i => twoClones.SingleOrDefault(c => c.Data == i)).ToList();
            var sortedBs = Enumerable.Range(1, 5).Select(i => bClones.SingleOrDefault(c => c.Data == i)).ToList();
            foreach (var one in sortedOnes)
            {
                Assert.IsNotNull(one);
            }
            foreach (var two in sortedTwos)
            {
                Assert.IsNotNull(two);
            }
            foreach (var b in sortedBs)
            {
                Assert.IsNotNull(b);
            }

            Assert.AreEqual(null, sortedTwos[0].DiamondBottomId);
            Assert.AreEqual(sortedBs[1].Id, sortedTwos[1].DiamondBottomId);
            Assert.AreEqual(sortedBs[3].Id, sortedTwos[2].DiamondBottomId);
            Assert.AreEqual(sortedBs[4].Id, sortedTwos[3].DiamondBottomId);
            Assert.AreEqual(sortedBs[4].Id, sortedTwos[4].DiamondBottomId);
            Assert.AreEqual(sortedBs[3].Id, sortedTwos[5].DiamondBottomId);

            Assert.AreEqual(null, sortedOnes[0].DiamondBottomId);
            Assert.AreEqual(sortedBs[2].Id, sortedOnes[1].DiamondBottomId);
            Assert.AreEqual(sortedBs[3].Id, sortedOnes[2].DiamondBottomId);
            Assert.AreEqual(sortedBs[4].Id, sortedOnes[3].DiamondBottomId);
            Assert.AreEqual(sortedBs[3].Id, sortedOnes[4].DiamondBottomId);
        }

    }
}
