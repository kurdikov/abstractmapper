﻿namespace TsSoft.EntityRepository.Helpers.Remove
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using BaseTestClasses.Database;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.EntityRepository.Helpers.Remove;

    [TestClass]
    public class EntityRemoverIntegrationTests : RepositoryTestBase
    {
        [NotNull]
        private EntityRemover _remover;

        [TestInitialize]
        public void Init()
        {
            _remover = new EntityRemover(
                new EntityRemoverSelectCreator(
                    StaticHelpers.MemberInfo,
                    EntityStaticHelpers.SelectHelper,
                    StaticHelpers.ExpressionBuilder,
                    EntityStaticHelpers.EntityDependencyHelper),
                RepositoryFactory,
                new DeletedEntitiesDescriptionCreator(
                    EntityStaticHelpers.EntityTypesHelper,
                    EntityStaticHelpers.EntityDependencyHelper,
                    StaticHelpers.ExpressionBuilder,
                    StaticHelpers.MemberInfo),
                new EntityRemoverSqlCreator(
                    StaticHelpers.MemberInfo,
                    StaticHelpers.Library,
                    EntityStaticHelpers.EntityTypesHelper,
                    SqliteIdStringifier.Create()));
        }


        [TestMethod]
        public void RemoveEntityWithoutClassChildrenTest()
        {
            var data = new[]
            {
                new Fifth {Type = "ToDelete"},
                new Fifth {Type = "ToPreserve"},
            };
            Seed(data);
            _remover.Remove<Fifth>(f => f.Type == "ToDelete", IsolationLevel.Unspecified);
            var repository = RepositoryFactory.GetBaseRepository<Fifth>();
            Assert.AreEqual(0, repository.Count(f => f.Type == "ToDelete"));
        }

        [TestMethod]
        public void RemoveEntityWithSingleChildrenCollectionTest()
        {
            var data = new[]
            {
                new Second
                {
                    Thirds = new List<Third> {new Third {Type = "1.1"}, new Third {Type = "1.2"},},
                    Type = "1"
                },
                new Second {Thirds = new List<Third>(), Type = "2"},
                new Second {Thirds = new List<Third> {new Third {Type = "3.1"}}, Type = "3"},
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<Second>();
            var tRepository = RepositoryFactory.GetBaseRepository<Third>();

            Assert.AreEqual(1, repository.Count(f => f.Type == "1"));
            Assert.AreEqual(2, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(1, repository.Count(f => f.Type == "2"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(1, repository.Count(f => f.Type == "3"));
            Assert.AreEqual(1, tRepository.Count(f => f.Type.StartsWith("3")));

            _remover.Remove<Second>(f => f.Type == "1", IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Type == "1"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(1, repository.Count(f => f.Type == "2"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(1, repository.Count(f => f.Type == "3"));
            Assert.AreEqual(1, tRepository.Count(f => f.Type.StartsWith("3")));

            _remover.Remove<Second>(f => f.Type == "2", IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Type == "1"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, repository.Count(f => f.Type == "2"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(1, repository.Count(f => f.Type == "3"));
            Assert.AreEqual(1, tRepository.Count(f => f.Type.StartsWith("3")));

            _remover.Remove<Second>(f => f.Type == "3", IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Type == "1"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, repository.Count(f => f.Type == "2"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, repository.Count(f => f.Type == "3"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("3")));

            _remover.Remove<Second>(f => f.Type == "4", IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Type == "1"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, repository.Count(f => f.Type == "2"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, repository.Count(f => f.Type == "3"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("3")));
        }

        [TestMethod]
        public void RemoveEntityWithTwoChildrenCollectionsTest()
        {
            var data = new[]
            {
                new Fourth
                {
                    Thirds = new List<Third> {new Third {Type = "1.1"}, new Third {Type = "1.2"},},
                    Fifths = new List<Fifth> {new Fifth {Type = "1.1"} },
                    Id = Guid.NewGuid()
                },
                new Fourth
                {
                    Thirds = new List<Third>(), 
                    Fifths = new List<Fifth> {new Fifth {Type = "2.1"}, new Fifth {Type = "2.2"} },
                    Id = Guid.NewGuid(),
                },
                new Fourth
                {
                    Thirds = new List<Third> {new Third {Type = "3.1"}}, 
                    Fifths = new List<Fifth>(),
                    Id = Guid.NewGuid()
                },
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<Fourth>();
            var tRepository = RepositoryFactory.GetBaseRepository<Third>();
            var fRepository = RepositoryFactory.GetBaseRepository<Fifth>();

            var id1 = data[0].Id;
            var id2 = data[1].Id;
            var id3 = data[2].Id;

            Assert.AreEqual(1, repository.Count(f => f.Id == id1));
            Assert.AreEqual(2, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(1, repository.Count(f => f.Id == id2));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(1, repository.Count(f => f.Id == id3));
            Assert.AreEqual(1, tRepository.Count(f => f.Type.StartsWith("3")));
            Assert.AreEqual(1, fRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(2, fRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("3")));

            _remover.Remove<Fourth>(f => f.Id == id1, IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Id == id1));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(1, repository.Count(f => f.Id == id2));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(1, repository.Count(f => f.Id == id3));
            Assert.AreEqual(1, tRepository.Count(f => f.Type.StartsWith("3")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(2, fRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("3")));

            _remover.Remove<Fourth>(f => f.Id == id2, IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Id == id1));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, repository.Count(f => f.Id == id2));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(1, repository.Count(f => f.Id == id3));
            Assert.AreEqual(1, tRepository.Count(f => f.Type.StartsWith("3")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("3")));

            _remover.Remove<Fourth>(f => f.Id == id3, IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Id == id1));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, repository.Count(f => f.Id == id2));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, repository.Count(f => f.Id == id3));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("3")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("3")));
        }

        [TestMethod]
        public void RemoveTreeTest()
        {
            var data = new[]
            {
                new Tree
                {
                    Data= "1", 
                    Children = new List<Tree>
                    {
                        new Tree{Data = "1.1"}, 
                        new Tree {Data = "1.2", Children = new List<Tree>{new Tree {Data = "1.2.1"}, new Tree{Data= "1.2.2"}}},
                    }
                },
                new Tree
                {
                    Data= "2", 
                    Children = new List<Tree>
                    {
                        new Tree{Data = "2.1"}, 
                        new Tree {Data = "2.2", Children = new List<Tree>{new Tree {Data = "2.2.1"}, new Tree{Data= "2.2.2"}}},
                    }
                },
                new Tree
                {
                    Data= "3", 
                    Children = new List<Tree>
                    {
                        new Tree{Data = "3.1"}, 
                        new Tree {Data = "3.2", Children = new List<Tree>{new Tree {Data = "3.2.1"}, new Tree{Data= "3.2.2"}}},
                    }
                },
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<Tree>();
            Assert.AreEqual(5, repository.Count(t => t.Data.StartsWith("1")));
            Assert.AreEqual(5, repository.Count(t => t.Data.StartsWith("2")));
            Assert.AreEqual(5, repository.Count(t => t.Data.StartsWith("3")));

            var settings = new Dictionary<Type, int> {{typeof(Tree), 2}};

            _remover.Remove<Tree>(t => t.Data == "2", IsolationLevel.Unspecified, settings);
            Assert.AreEqual(5, repository.Count(t => t.Data.StartsWith("1")));
            Assert.AreEqual(0, repository.Count(t => t.Data.StartsWith("2")));
            Assert.AreEqual(5, repository.Count(t => t.Data.StartsWith("3")));
        }

        [TestMethod]
        public void RemoveDependentPkFkTest()
        {
            var data = new[]
            {
                new ThirdWithPkPk {Id = Guid.NewGuid(), Data = 1, Second = new SecondWithFkPk {Type = "1.1"}},
                new ThirdWithPkPk {Id = Guid.NewGuid(), Data = 2},
                new ThirdWithPkPk {Id = Guid.NewGuid(), Data = 3, Second = new SecondWithFkPk {Type = "3.1"}},
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<ThirdWithPkPk>();
            var sRepository = RepositoryFactory.GetBaseRepository<SecondWithFkPk>();
            Assert.AreEqual(1, repository.Count(t => t.Data == 1));
            Assert.AreEqual(1, repository.Count(t => t.Data == 2));
            Assert.AreEqual(1, repository.Count(t => t.Data == 3));
            Assert.AreEqual(1, sRepository.Count(t => t.Type.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("2")));
            Assert.AreEqual(1, sRepository.Count(t => t.Type.StartsWith("3")));

            _remover.Remove<ThirdWithPkPk>(t => t.Data == 1, IsolationLevel.Unspecified);
            Assert.AreEqual(0, repository.Count(t => t.Data == 1));
            Assert.AreEqual(1, repository.Count(t => t.Data == 2));
            Assert.AreEqual(1, repository.Count(t => t.Data == 3));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("2")));
            Assert.AreEqual(1, sRepository.Count(t => t.Type.StartsWith("3")));

            _remover.Remove<ThirdWithPkPk>(t => t.Data == 2, IsolationLevel.Unspecified);
            Assert.AreEqual(0, repository.Count(t => t.Data == 1));
            Assert.AreEqual(0, repository.Count(t => t.Data == 2));
            Assert.AreEqual(1, repository.Count(t => t.Data == 3));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("2")));
            Assert.AreEqual(1, sRepository.Count(t => t.Type.StartsWith("3")));

            _remover.Remove<ThirdWithPkPk>(t => t.Data == 3, IsolationLevel.Unspecified);
            Assert.AreEqual(0, repository.Count(t => t.Data == 1));
            Assert.AreEqual(0, repository.Count(t => t.Data == 2));
            Assert.AreEqual(0, repository.Count(t => t.Data == 3));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("2")));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("3")));
        }

        [TestMethod]
        public void RemoveDependentCompositePkTest()
        {
            var firstId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var secondId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var thirdId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var data = new[]
            {
                new CompositeRoot {Data = "1", CompositePks = new List<CompositePk> {new CompositePk {Part1 = firstId.Item1, Part2 = firstId.Item2, Data = "1.1"}}},
                new CompositeRoot {Data = "2", CompositePks = new List<CompositePk> {}},
                new CompositeRoot {Data = "3", CompositePks = new List<CompositePk>
                {
                    new CompositePk {Part1 = secondId.Item1, Part2 = secondId.Item2, Data = "3.1"},
                    new CompositePk {Part1 = thirdId.Item1, Part2 = thirdId.Item2, Data = "3.2"},
                }}
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<CompositeRoot>();
            var sRepository = RepositoryFactory.GetBaseRepository<CompositePk>();
            Assert.AreEqual(1, repository.Count(t => t.Data == "1"));
            Assert.AreEqual(1, repository.Count(t => t.Data == "2"));
            Assert.AreEqual(1, repository.Count(t => t.Data == "3"));
            Assert.AreEqual(1, sRepository.Count(t => t.Data.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("2")));
            Assert.AreEqual(2, sRepository.Count(t => t.Data.StartsWith("3")));

            _remover.Remove<CompositeRoot>(r => r.Data == "1", IsolationLevel.Unspecified);
            Assert.AreEqual(0, repository.Count(t => t.Data == "1"));
            Assert.AreEqual(1, repository.Count(t => t.Data == "2"));
            Assert.AreEqual(1, repository.Count(t => t.Data == "3"));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("2")));
            Assert.AreEqual(2, sRepository.Count(t => t.Data.StartsWith("3")));

            _remover.Remove<CompositeRoot>(r => r.Data == "2", IsolationLevel.Unspecified);
            Assert.AreEqual(0, repository.Count(t => t.Data == "1"));
            Assert.AreEqual(0, repository.Count(t => t.Data == "2"));
            Assert.AreEqual(1, repository.Count(t => t.Data == "3"));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("2")));
            Assert.AreEqual(2, sRepository.Count(t => t.Data.StartsWith("3")));

            _remover.Remove<CompositeRoot>(r => r.Data == "3", IsolationLevel.Unspecified);
            Assert.AreEqual(0, repository.Count(t => t.Data == "1"));
            Assert.AreEqual(0, repository.Count(t => t.Data == "2"));
            Assert.AreEqual(0, repository.Count(t => t.Data == "3"));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("2")));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("3")));
        }


        [TestMethod]
        public async Task RemoveEntityWithoutClassChildrenAsyncAsyncTest()
        {
            var data = new[]
            {
                new Fifth {Type = "ToDelete"},
                new Fifth {Type = "ToPreserve"},
            };
            Seed(data);
            await _remover.RemoveAsync<Fifth>(f => f.Type == "ToDelete", IsolationLevel.Unspecified);
            var repository = RepositoryFactory.GetBaseRepository<Fifth>();
            Assert.AreEqual(0, repository.Count(f => f.Type == "ToDelete"));
        }

        [TestMethod]
        public async Task RemoveEntityWithSingleChildrenCollectionAsyncTest()
        {
            var data = new[]
            {
                new Second
                {
                    Thirds = new List<Third> {new Third {Type = "1.1"}, new Third {Type = "1.2"},},
                    Type = "1"
                },
                new Second {Thirds = new List<Third>(), Type = "2"},
                new Second {Thirds = new List<Third> {new Third {Type = "3.1"}}, Type = "3"},
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<Second>();
            var tRepository = RepositoryFactory.GetBaseRepository<Third>();

            Assert.AreEqual(1, repository.Count(f => f.Type == "1"));
            Assert.AreEqual(2, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(1, repository.Count(f => f.Type == "2"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(1, repository.Count(f => f.Type == "3"));
            Assert.AreEqual(1, tRepository.Count(f => f.Type.StartsWith("3")));

            await _remover.RemoveAsync<Second>(f => f.Type == "1", IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Type == "1"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(1, repository.Count(f => f.Type == "2"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(1, repository.Count(f => f.Type == "3"));
            Assert.AreEqual(1, tRepository.Count(f => f.Type.StartsWith("3")));

            await _remover.RemoveAsync<Second>(f => f.Type == "2", IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Type == "1"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, repository.Count(f => f.Type == "2"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(1, repository.Count(f => f.Type == "3"));
            Assert.AreEqual(1, tRepository.Count(f => f.Type.StartsWith("3")));

            await _remover.RemoveAsync<Second>(f => f.Type == "3", IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Type == "1"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, repository.Count(f => f.Type == "2"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, repository.Count(f => f.Type == "3"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("3")));

            await _remover.RemoveAsync<Second>(f => f.Type == "4", IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Type == "1"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, repository.Count(f => f.Type == "2"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, repository.Count(f => f.Type == "3"));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("3")));
        }

        [TestMethod]
        public async Task RemoveEntityWithTwoChildrenCollectionsAsyncTest()
        {
            var data = new[]
            {
                new Fourth
                {
                    Thirds = new List<Third> {new Third {Type = "1.1"}, new Third {Type = "1.2"},},
                    Fifths = new List<Fifth> {new Fifth {Type = "1.1"} },
                    Id = Guid.NewGuid()
                },
                new Fourth
                {
                    Thirds = new List<Third>(), 
                    Fifths = new List<Fifth> {new Fifth {Type = "2.1"}, new Fifth {Type = "2.2"} },
                    Id = Guid.NewGuid(),
                },
                new Fourth
                {
                    Thirds = new List<Third> {new Third {Type = "3.1"}}, 
                    Fifths = new List<Fifth>(),
                    Id = Guid.NewGuid()
                },
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<Fourth>();
            var tRepository = RepositoryFactory.GetBaseRepository<Third>();
            var fRepository = RepositoryFactory.GetBaseRepository<Fifth>();

            var id1 = data[0].Id;
            var id2 = data[1].Id;
            var id3 = data[2].Id;

            Assert.AreEqual(1, repository.Count(f => f.Id == id1));
            Assert.AreEqual(2, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(1, repository.Count(f => f.Id == id2));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(1, repository.Count(f => f.Id == id3));
            Assert.AreEqual(1, tRepository.Count(f => f.Type.StartsWith("3")));
            Assert.AreEqual(1, fRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(2, fRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("3")));

            await _remover.RemoveAsync<Fourth>(f => f.Id == id1, IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Id == id1));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(1, repository.Count(f => f.Id == id2));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(1, repository.Count(f => f.Id == id3));
            Assert.AreEqual(1, tRepository.Count(f => f.Type.StartsWith("3")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(2, fRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("3")));

            await _remover.RemoveAsync<Fourth>(f => f.Id == id2, IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Id == id1));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, repository.Count(f => f.Id == id2));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(1, repository.Count(f => f.Id == id3));
            Assert.AreEqual(1, tRepository.Count(f => f.Type.StartsWith("3")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("3")));

            await _remover.RemoveAsync<Fourth>(f => f.Id == id3, IsolationLevel.Unspecified);

            Assert.AreEqual(0, repository.Count(f => f.Id == id1));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, repository.Count(f => f.Id == id2));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, repository.Count(f => f.Id == id3));
            Assert.AreEqual(0, tRepository.Count(f => f.Type.StartsWith("3")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("2")));
            Assert.AreEqual(0, fRepository.Count(f => f.Type.StartsWith("3")));
        }

        [TestMethod]
        public async Task RemoveTreeAsyncTest()
        {
            var data = new[]
            {
                new Tree
                {
                    Data= "1", 
                    Children = new List<Tree>
                    {
                        new Tree{Data = "1.1"}, 
                        new Tree {Data = "1.2", Children = new List<Tree>{new Tree {Data = "1.2.1"}, new Tree{Data= "1.2.2"}}},
                    }
                },
                new Tree
                {
                    Data= "2", 
                    Children = new List<Tree>
                    {
                        new Tree{Data = "2.1"}, 
                        new Tree {Data = "2.2", Children = new List<Tree>{new Tree {Data = "2.2.1"}, new Tree{Data= "2.2.2"}}},
                    }
                },
                new Tree
                {
                    Data= "3", 
                    Children = new List<Tree>
                    {
                        new Tree{Data = "3.1"}, 
                        new Tree {Data = "3.2", Children = new List<Tree>{new Tree {Data = "3.2.1"}, new Tree{Data= "3.2.2"}}},
                    }
                },
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<Tree>();
            Assert.AreEqual(5, repository.Count(t => t.Data.StartsWith("1")));
            Assert.AreEqual(5, repository.Count(t => t.Data.StartsWith("2")));
            Assert.AreEqual(5, repository.Count(t => t.Data.StartsWith("3")));

            var settings = new Dictionary<Type, int> { { typeof(Tree), 2 } };

            await _remover.RemoveAsync<Tree>(t => t.Data == "2", IsolationLevel.Unspecified, settings);
            Assert.AreEqual(5, repository.Count(t => t.Data.StartsWith("1")));
            Assert.AreEqual(0, repository.Count(t => t.Data.StartsWith("2")));
            Assert.AreEqual(5, repository.Count(t => t.Data.StartsWith("3")));
        }

        [TestMethod]
        public async Task RemoveDependentPkFkAsyncTest()
        {
            var data = new[]
            {
                new ThirdWithPkPk {Id = Guid.NewGuid(), Data = 1, Second = new SecondWithFkPk {Type = "1.1"}},
                new ThirdWithPkPk {Id = Guid.NewGuid(), Data = 2},
                new ThirdWithPkPk {Id = Guid.NewGuid(), Data = 3, Second = new SecondWithFkPk {Type = "3.1"}},
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<ThirdWithPkPk>();
            var sRepository = RepositoryFactory.GetBaseRepository<SecondWithFkPk>();
            Assert.AreEqual(1, repository.Count(t => t.Data == 1));
            Assert.AreEqual(1, repository.Count(t => t.Data == 2));
            Assert.AreEqual(1, repository.Count(t => t.Data == 3));
            Assert.AreEqual(1, sRepository.Count(t => t.Type.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("2")));
            Assert.AreEqual(1, sRepository.Count(t => t.Type.StartsWith("3")));

            await _remover.RemoveAsync<ThirdWithPkPk>(t => t.Data == 1, IsolationLevel.Unspecified);
            Assert.AreEqual(0, repository.Count(t => t.Data == 1));
            Assert.AreEqual(1, repository.Count(t => t.Data == 2));
            Assert.AreEqual(1, repository.Count(t => t.Data == 3));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("2")));
            Assert.AreEqual(1, sRepository.Count(t => t.Type.StartsWith("3")));

            await _remover.RemoveAsync<ThirdWithPkPk>(t => t.Data == 2, IsolationLevel.Unspecified);
            Assert.AreEqual(0, repository.Count(t => t.Data == 1));
            Assert.AreEqual(0, repository.Count(t => t.Data == 2));
            Assert.AreEqual(1, repository.Count(t => t.Data == 3));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("2")));
            Assert.AreEqual(1, sRepository.Count(t => t.Type.StartsWith("3")));

            await _remover.RemoveAsync<ThirdWithPkPk>(t => t.Data == 3, IsolationLevel.Unspecified);
            Assert.AreEqual(0, repository.Count(t => t.Data == 1));
            Assert.AreEqual(0, repository.Count(t => t.Data == 2));
            Assert.AreEqual(0, repository.Count(t => t.Data == 3));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("2")));
            Assert.AreEqual(0, sRepository.Count(t => t.Type.StartsWith("3")));
        }

        [TestMethod]
        public async Task RemoveDependentCompositePkAsyncTest()
        {
            var firstId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var secondId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var thirdId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var data = new[]
            {
                new CompositeRoot {Data = "1", CompositePks = new List<CompositePk> {new CompositePk {Part1 = firstId.Item1, Part2 = firstId.Item2, Data = "1.1"}}},
                new CompositeRoot {Data = "2", CompositePks = new List<CompositePk> {}},
                new CompositeRoot {Data = "3", CompositePks = new List<CompositePk>
                {
                    new CompositePk {Part1 = secondId.Item1, Part2 = secondId.Item2, Data = "3.1"},
                    new CompositePk {Part1 = thirdId.Item1, Part2 = thirdId.Item2, Data = "3.2"},
                }}
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<CompositeRoot>();
            var sRepository = RepositoryFactory.GetBaseRepository<CompositePk>();
            Assert.AreEqual(1, repository.Count(t => t.Data == "1"));
            Assert.AreEqual(1, repository.Count(t => t.Data == "2"));
            Assert.AreEqual(1, repository.Count(t => t.Data == "3"));
            Assert.AreEqual(1, sRepository.Count(t => t.Data.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("2")));
            Assert.AreEqual(2, sRepository.Count(t => t.Data.StartsWith("3")));

            await _remover.RemoveAsync<CompositeRoot>(r => r.Data == "1", IsolationLevel.Unspecified);
            Assert.AreEqual(0, repository.Count(t => t.Data == "1"));
            Assert.AreEqual(1, repository.Count(t => t.Data == "2"));
            Assert.AreEqual(1, repository.Count(t => t.Data == "3"));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("2")));
            Assert.AreEqual(2, sRepository.Count(t => t.Data.StartsWith("3")));

            await _remover.RemoveAsync<CompositeRoot>(r => r.Data == "2", IsolationLevel.Unspecified);
            Assert.AreEqual(0, repository.Count(t => t.Data == "1"));
            Assert.AreEqual(0, repository.Count(t => t.Data == "2"));
            Assert.AreEqual(1, repository.Count(t => t.Data == "3"));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("2")));
            Assert.AreEqual(2, sRepository.Count(t => t.Data.StartsWith("3")));

            await _remover.RemoveAsync<CompositeRoot>(r => r.Data == "3", IsolationLevel.Unspecified);
            Assert.AreEqual(0, repository.Count(t => t.Data == "1"));
            Assert.AreEqual(0, repository.Count(t => t.Data == "2"));
            Assert.AreEqual(0, repository.Count(t => t.Data == "3"));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("1")));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("2")));
            Assert.AreEqual(0, sRepository.Count(t => t.Data.StartsWith("3")));
        }
    }
}
