﻿namespace TsSoft.EntityRepository.Tests.Helpers.Remove
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using BaseTestClasses;
    using BaseTestClasses.Database;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.EntityRepository.Helpers.Remove;

    [TestClass]
    public class EntityRemoverSelectCreatorTests
    {
        [NotNull]
        private EntityRemoverSelectCreator _creator;

        [TestInitialize]
        public void Init()
        {
            _creator = new EntityRemoverSelectCreator(
                StaticHelpers.MemberInfo,
                EntityStaticHelpers.SelectHelper,
                StaticHelpers.ExpressionBuilder,
                EntityStaticHelpers.EntityDependencyHelper);
        }

        [TestMethod]
        public void SelectWithoutChildren()
        {
            var actual = _creator.GetSelect<Fifth>(new Dictionary<Type, int>());
            Expression<Func<Fifth, object>> expected = f =>
                new Fifth
                {
                    Id = f.Id,
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }

        [TestMethod]
        public void SelectWithSingleChildrenCollection()
        {
            var actual = _creator.GetSelect<Second>(new Dictionary<Type, int>());
            Expression<Func<Second, object>> expected = f =>
                new Second
                {
                    Id = f.Id,
                    Thirds = f.Thirds.Select(s => new Third {Id = s.Id}).ToList(),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }

        [TestMethod]
        public void SelectWithTwoChildrenCollections()
        {
            var actual = _creator.GetSelect<Fourth>(new Dictionary<Type, int>());
            Expression<Func<Fourth, object>> expected = f =>
                new Fourth
                {
                    Id = f.Id,
                    Thirds = f.Thirds.Select(s => new Third { Id = s.Id }).ToList(),
                    Fifths = f.Fifths.Select(s => new Fifth { Id = s.Id }).ToList(),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }

        [TestMethod]
        public void SelectTreeNoLevels()
        {
            var actual = _creator.GetSelect<Tree>(new Dictionary<Type, int>());
            Expression<Func<Tree, object>> expected = f =>
                new Tree
                {
                    Id = f.Id,
                    Children = f.Children.Select(cc => new Tree
                    {
                        Id = cc.Id,
                    }).ToList(),

                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }

        [TestMethod]
        public void SelectTreeOneLevel()
        {
            var actual = _creator.GetSelect<Tree>(new Dictionary<Type, int> { { typeof(Tree), 1 } });
            Expression<Func<Tree, object>> expected = f =>
                new Tree
                {
                    Id = f.Id,
                    Children = f.Children.Select(c => new Tree
                    {
                        Id = c.Id,
                        Children = c.Children.Select(cc => new Tree
                        {
                            Id = cc.Id,
                        }).ToList(),
                    }).ToList(),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }

        [TestMethod]
        public void SelectTreeTwoLevels()
        {
            var actual = _creator.GetSelect<Tree>(new Dictionary<Type, int> {{typeof(Tree), 2}});
            Expression<Func<Tree, object>> expected = f =>
                new Tree
                {
                    Id = f.Id,
                    Children = f.Children.Select(c => new Tree
                    {
                        Id = c.Id,
                        Children = c.Children.Select(cc => new Tree
                        {
                            Id = cc.Id,
                            Children = cc.Children.Select(ccc => new Tree
                            {
                                Id = ccc.Id,
                            }).ToList(),
                        }).ToList(),
                    }).ToList(),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }

        [TestMethod]
        public void SelectDependentPkFk()
        {
            var actual = _creator.GetSelect<ThirdWithPkPk>(new Dictionary<Type, int>());
            Expression<Func<ThirdWithPkPk, object>> expected = f =>
                new ThirdWithPkPk
                {
                    Id = f.Id,
                    Second = f.Second != null ? new SecondWithFkPk {Id = f.Second.Id} : null,
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }

        [TestMethod]
        public void SelectDependentCompositePk()
        {
            var actual = _creator.GetSelect<CompositeRoot>(new Dictionary<Type, int>());
            Expression<Func<CompositeRoot, object>> expected = f =>
                new CompositeRoot
                {
                    Id = f.Id,
                    CompositePks = f.CompositePks.Select(c => new CompositePk
                    {
                        Part1 = c.Part1,
                        Part2 = c.Part2,
                        CompositeFks = c.CompositeFks.Select(cfk => new CompositeFk
                        {
                            Id = cfk.Id,
                        }).ToList(),
                    }).ToList(),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }

        [TestMethod]
        public void SelectBigStructure()
        {
            var actual = _creator.GetSelect<First>(new Dictionary<Type, int>());
            Expression<Func<First, object>> expected = f =>
                new First
                {
                    Id = f.Id,
                    Seconds = f.Seconds.Select(s => new Second
                    {
                        Id = s.Id,
                        Thirds = s.Thirds.Select(t => new Third
                        {
                            Id = t.Id,
                        }).ToList(),
                    }).ToList(),
                    SecondWithFkPks = f.SecondWithFkPks.Select(s => new SecondWithFkPk
                    {
                        Id = s.Id,
                    }).ToList(),
                    NotInterestingSeconds = f.NotInterestingSeconds.Select(s => new NotInterestingSecond
                    {
                        Id = s.Id,
                    }).ToList(),
                    DiamondOnes = f.DiamondOnes.Select(d => new DiamondOne
                    {
                        Id = d.Id,
                    }).ToList(),
                    DiamondTwos = f.DiamondTwos.Select(d => new DiamondTwo
                    {
                        Id = d.Id,
                    }).ToList(),
                    DiamondBottoms = f.DiamondBottoms.Select(d => new DiamondBottom
                    {
                        Id = d.Id,
                        DiamondOnes = d.DiamondOnes.Select(dd => new DiamondOne
                        {
                            Id = dd.Id,
                        }).ToList(),
                        DiamondTwos = d.DiamondTwos.Select(dd => new DiamondTwo
                        {
                            Id = dd.Id,
                        }).ToList(),
                    }).ToList(),
                    Trees = f.Trees.Select(t => new Tree
                    {
                        Id = t.Id,
                    }).ToList(),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }

        [TestMethod]
        public void SelectBigStructureWithTree()
        {
            var actual = _creator.GetSelect<First>(new Dictionary<Type, int>{{typeof(Tree), 1}});
            Expression<Func<First, object>> expected = f =>
                new First
                {
                    Id = f.Id,
                    Seconds = f.Seconds.Select(s => new Second
                    {
                        Id = s.Id,
                        Thirds = s.Thirds.Select(t => new Third
                        {
                            Id = t.Id,
                        }).ToList(),
                    }).ToList(),
                    SecondWithFkPks = f.SecondWithFkPks.Select(s => new SecondWithFkPk
                    {
                        Id = s.Id,
                    }).ToList(),
                    NotInterestingSeconds = f.NotInterestingSeconds.Select(s => new NotInterestingSecond
                    {
                        Id = s.Id,
                    }).ToList(),
                    DiamondOnes = f.DiamondOnes.Select(d => new DiamondOne
                    {
                        Id = d.Id,
                    }).ToList(),
                    DiamondTwos = f.DiamondTwos.Select(d => new DiamondTwo
                    {
                        Id = d.Id,
                    }).ToList(),
                    DiamondBottoms = f.DiamondBottoms.Select(d => new DiamondBottom
                    {
                        Id = d.Id,
                        DiamondOnes = d.DiamondOnes.Select(dd => new DiamondOne
                        {
                            Id = dd.Id,
                        }).ToList(),
                        DiamondTwos = d.DiamondTwos.Select(dd => new DiamondTwo
                        {
                            Id = dd.Id,
                        }).ToList(),
                    }).ToList(),
                    Trees = f.Trees.Select(t => new Tree
                    {
                        Id = t.Id,
                        Children = t.Children.Select(c => new Tree
                        {
                            Id = c.Id,
                        }).ToList(),
                    }).ToList(),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }
    }
}
