﻿namespace TsSoft.EntityRepository.Tests.Helpers.Remove
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using BaseTestClasses.Database;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.EntityRepository.Helpers.Remove;
    using TsSoft.Expressions.SelectBuilder.Models;

    [TestClass]
    public class EntityRemoverIntegrationWithMockedSelectTests : RepositoryTestBase
    {
        private Mock<IEntityRemoverSelectCreator> _mockSelectCreator;
        private EntityRemover _remover;

        [TestInitialize]
        public void Init()
        {
            _mockSelectCreator = new Mock<IEntityRemoverSelectCreator>(MockBehavior.Strict);
            _remover = new EntityRemover(
                _mockSelectCreator.Object,
                RepositoryFactory,
                new DeletedEntitiesDescriptionCreator(
                    EntityStaticHelpers.EntityTypesHelper,
                    EntityStaticHelpers.EntityDependencyHelper,
                    StaticHelpers.ExpressionBuilder,
                    StaticHelpers.MemberInfo),
                new EntityRemoverSqlCreator(
                    StaticHelpers.MemberInfo,
                    StaticHelpers.Library,
                    EntityStaticHelpers.EntityTypesHelper,
                    SqliteIdStringifier.Create()));
        }

        public static SelectExpression<First> MakeSelect()
        {
            var result = new SelectExpression<First>
            {
                Select = f =>
                new First
                {
                    Id = f.Id,
                    Seconds = f.Seconds.Select(s => new Second
                    {
                        Id = s.Id,
                        Thirds = s.Thirds.Select(t => new Third
                        {
                            Id = t.Id,
                        }).ToList(),
                    }).ToList(),
                    SecondWithFkPks = f.SecondWithFkPks.Select(s => new SecondWithFkPk
                    {
                        Id = s.Id,
                    }).ToList(),
                    NotInterestingSeconds = f.NotInterestingSeconds.Select(s => new NotInterestingSecond
                    {
                        Id = s.Id,
                    }).ToList(),
                    DiamondOnes = f.DiamondOnes.Select(d => new DiamondOne
                    {
                        Id = d.Id,
                    }).ToList(),
                    DiamondTwos = f.DiamondTwos.Select(d => new DiamondTwo
                    {
                        Id = d.Id,
                    }).ToList(),
                    DiamondBottoms = f.DiamondBottoms.Select(d => new DiamondBottom
                    {
                        Id = d.Id,
                        DiamondOnes = d.DiamondOnes.Select(dd => new DiamondOne
                        {
                            Id = dd.Id,
                        }).ToList(),
                        // there is no DiamondTwos here because it would generate DbApplyExpression in EF tree, and that is not supported by SQLite
                    }).ToList(),
                },
            };
            return EntityStaticHelpers.SelectHelper.Merge(result, new SelectExpression<First>());
        }

        [TestMethod]
        public void RemoveBigStructureTest()
        {
            var id41 = Guid.NewGuid();
            var id42 = Guid.NewGuid();
            var id21 = Guid.NewGuid();
            var id22 = Guid.NewGuid();
            var d1 = new[]
            {
                 new DiamondOne {Data = 1},
                 new DiamondOne {Data = 2},
                 new DiamondOne {Data = 3},
                 new DiamondOne {Data = 4},
                 new DiamondOne {Data = 5},
                 new DiamondOne {Data = 6},
                 new DiamondOne {Data = 7},
                 new DiamondOne {Data = 8},
                 new DiamondOne {Data = 9},
            };
            var d2 = new[]
            {
                new DiamondTwo {Data = 1},
                new DiamondTwo {Data = 2},
                new DiamondTwo {Data = 3},
                new DiamondTwo {Data = 4},
                new DiamondTwo {Data = 5},
                new DiamondTwo {Data = 6},
                new DiamondTwo {Data = 7},
                new DiamondTwo {Data = 8},
                new DiamondTwo {Data = 9},
            };
            var data = new[]
            {
                new First
                {
                    Id = 1,
                    Name = "1",
                    Seconds = new List<Second>
                    {
                        new Second {Type = "1.1"},
                        new Second
                        {
                            Type = "1.2", 
                            Thirds = new List<Third>
                            {
                                new Third {Type = "1.2.1", Fourth = new Fourth {Id = id41}},
                                new Third {Type = "1.2.2"},
                            }
                        },
                        new Second
                        {
                            Type = "1.3",
                            Thirds = new List<Third>
                            {
                                new Third {Type = "1.3.1", Fourth = new Fourth {Id = id42, Fifths = new List<Fifth> {new Fifth {Type = "1.3.1.1"}}}},
                            }
                        }
                    },
                    NotInterestingSeconds = new List<NotInterestingSecond>
                    {
                        new NotInterestingSecond {Guid = id21},
                        new NotInterestingSecond{Guid = id22},
                    },
                    SecondWithFkPks = new List<SecondWithFkPk>
                    {
                        new SecondWithFkPk {Type = "1.1", Third = new ThirdWithPkPk {Id = Guid.NewGuid(), Data = 1}},
                        new SecondWithFkPk {Type = "1.2", Third = new ThirdWithPkPk {Id = Guid.NewGuid(), Data = 2}},
                    },
                    DiamondOnes = new List<DiamondOne>
                    {
                        d1[0],
                        d1[1],
                    },
                    DiamondTwos = new List<DiamondTwo>
                    {
                        d2[0],
                        d2[1],
                    },
                    DiamondBottoms = new List<DiamondBottom>
                    {
                        new DiamondBottom
                        {
                            Data = 1,
                            DiamondOnes = new List<DiamondOne> {d1[0], d1[2]},
                            DiamondTwos = new List<DiamondTwo> {d2[0], d2[2]},
                        },
                        new DiamondBottom
                        {
                            Data = 2,
                            DiamondOnes = new List<DiamondOne> {d1[3], d1[4]},
                            DiamondTwos = new List<DiamondTwo> {d2[1], d2[3]},
                        },
                        new DiamondBottom {Data = 3},
                    },
                },
            };
            Seed(data);

            var repo = RepositoryFactory.GetBaseRepository<First>();
            var sRepo = RepositoryFactory.GetBaseRepository<Second>();
            var nisRepo = RepositoryFactory.GetBaseRepository<NotInterestingSecond>();
            var sfkRepo = RepositoryFactory.GetBaseRepository<SecondWithFkPk>();
            var d1Repo = RepositoryFactory.GetBaseRepository<DiamondOne>();
            var d2Repo = RepositoryFactory.GetBaseRepository<DiamondTwo>();
            var dbRepo = RepositoryFactory.GetBaseRepository<DiamondBottom>();
            var tRepo = RepositoryFactory.GetBaseRepository<Third>();
            var fRepo = RepositoryFactory.GetBaseRepository<Fourth>();
            var ffRepo = RepositoryFactory.GetBaseRepository<Fifth>();

            Assert.AreEqual(1, repo.Count(f => f.Name == "1"));
            Assert.AreEqual(3, sRepo.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(3, tRepo.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(2, fRepo.Count(f => f.Id == id41 || f.Id == id42));
            Assert.AreEqual(1, ffRepo.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(2, nisRepo.Count(f => f.Guid == id21 || f.Guid == id22));
            Assert.AreEqual(2, sfkRepo.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(5, d1Repo.Count(f => f.Data < 10));
            Assert.AreEqual(4, d2Repo.Count(f => f.Data < 10));
            Assert.AreEqual(3, dbRepo.Count(f => f.Data < 10));

            var settings = new Dictionary<Type, int>();
            _mockSelectCreator.Setup(c => c.GetSelect<First>(settings))
                .Returns(MakeSelect());

            _remover.Remove<First>(f => f.Name == "1", IsolationLevel.Unspecified, settings);

            Assert.AreEqual(0, repo.Count(f => f.Name == "1"));
            Assert.AreEqual(0, sRepo.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, tRepo.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(2, fRepo.Count(f => f.Id == id41 || f.Id == id42));
            Assert.AreEqual(1, ffRepo.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, nisRepo.Count(f => f.Guid == id21 || f.Guid == id22));
            Assert.AreEqual(0, sfkRepo.Count(f => f.Type.StartsWith("1")));
            Assert.AreEqual(0, d1Repo.Count(f => f.Data < 10));
            Assert.AreEqual(2, d2Repo.Count(f => f.Data < 10));
            Assert.AreEqual(0, d2Repo.Count(f => f.Data == 1 || f.Data == 2));
            Assert.AreEqual(2, d2Repo.Count(f => f.Data == 3 || f.Data == 4));
            Assert.AreEqual(0, dbRepo.Count(f => f.Data < 10));
        }
    }
}
