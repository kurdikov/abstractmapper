﻿using System;
using System.Text;

namespace TsSoft.EntityRepository.Helpers.Remove
{
    class SqliteIdStringifier : IdStringifier, IIdStringifier
    {
        string IIdStringifier.Stringify<TId>(TId id)
        {
            if (id is Guid)
            {
                var sb = new StringBuilder(35);
                sb.Append("x'");
                foreach (var @byte in ((Guid)(object)id).ToByteArray())
                {
                    sb.Append(@byte.ToString("X2"));
                }
                sb.Append("'");
                return sb.ToString();
            }
            return Stringify(id);
        }

        public static IIdStringifier Create()
        {
#if NET45
            return new IdStringifier();
#elif NETCORE
            return new SqliteIdStringifier();
#endif
        }
    }
}
