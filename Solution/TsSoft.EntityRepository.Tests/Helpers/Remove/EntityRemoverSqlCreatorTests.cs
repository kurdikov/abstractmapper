﻿namespace TsSoft.EntityRepository.Helpers.Remove
{
    using System;
    using BaseTestClasses.Database;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;

    [TestClass]
    public class EntityRemoverSqlCreatorTests
    {
        [NotNull]
        private EntityRemoverSqlCreator _creator;

        [TestInitialize]
        public void Init()
        {
            _creator = new EntityRemoverSqlCreator(
                StaticHelpers.MemberInfo,
                StaticHelpers.Library,
                EntityStaticHelpers.EntityTypesHelper,
                new IdStringifier());
        }

        [TestMethod]
        public void NoEntities()
        {
            var result = _creator.GenerateDeleteSql(typeof(First), new DeletedEntitiesDescription
            {
                TableName = "table",
                PrimaryKey = EntityStaticHelpers.EntityTypesHelper.GetPrimaryKey(typeof(First)),
                Ids = new int[0],
            });
            Assert.AreEqual("", result);
        }

        [TestMethod]
        public void OneEntity()
        {
            var result = _creator.GenerateDeleteSql(typeof(First), new DeletedEntitiesDescription
            {
                TableName = "table",
                PrimaryKey = EntityStaticHelpers.EntityTypesHelper.GetPrimaryKey(typeof(First)),
                Ids = new int[] {1},
            });
            Assert.AreEqual("delete from table where [Id] in ('1');", result);
        }

        [TestMethod]
        public void TwoEntities()
        {
            var result = _creator.GenerateDeleteSql(typeof(First), new DeletedEntitiesDescription
            {
                TableName = "table",
                PrimaryKey = EntityStaticHelpers.EntityTypesHelper.GetPrimaryKey(typeof(First)),
                Ids = new int[] { 1, 2 },
            });
            Assert.AreEqual("delete from table where [Id] in ('1','2');", result);
        }

        [TestMethod]
        public void NoEntitiesCompositeKey()
        {
            var result = _creator.GenerateDeleteSql(typeof(CompositePk), new DeletedEntitiesDescription
            {
                TableName = "table",
                PrimaryKey = EntityStaticHelpers.EntityTypesHelper.GetPrimaryKey(typeof(CompositePk)),
                Ids = new CompositePk[0],
            });
            Assert.AreEqual("", result);
        }

        [TestMethod]
        public void OneEntityCompositeKey()
        {
            var result = _creator.GenerateDeleteSql(typeof(CompositePk), new DeletedEntitiesDescription
            {
                TableName = "table",
                PrimaryKey = EntityStaticHelpers.EntityTypesHelper.GetPrimaryKey(typeof(CompositePk)),
                Ids = new CompositePk[]
                {
                    new CompositePk {Part1 = Guid.Parse("00000000-0000-0000-0000-000000000000"), Part2 = Guid.Parse("11111111-1111-1111-1111-111111111111")},
                },
            });
            Assert.AreEqual("delete from table where ([Part1] = '00000000-0000-0000-0000-000000000000' and [Part2] = '11111111-1111-1111-1111-111111111111');", result);
        }

        [TestMethod]
        public void TwoEntitiesCompositeKey()
        {
            var result = _creator.GenerateDeleteSql(typeof(CompositePk), new DeletedEntitiesDescription
            {
                TableName = "table",
                PrimaryKey = EntityStaticHelpers.EntityTypesHelper.GetPrimaryKey(typeof(CompositePk)),
                Ids = new CompositePk[]
                {
                    new CompositePk {Part1 = Guid.Parse("00000000-0000-0000-0000-000000000000"), Part2 = Guid.Parse("11111111-1111-1111-1111-111111111111")},
                    new CompositePk {Part1 = Guid.Parse("22222222-2222-2222-2222-222222222222"), Part2 = Guid.Parse("33333333-3333-3333-3333-333333333333")},
                },
            });
            Assert.AreEqual("delete from table where ([Part1] = '00000000-0000-0000-0000-000000000000' and [Part2] = '11111111-1111-1111-1111-111111111111') or ([Part1] = '22222222-2222-2222-2222-222222222222' and [Part2] = '33333333-3333-3333-3333-333333333333');", result);
        }
    }
}
