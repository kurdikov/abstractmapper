﻿namespace TsSoft.EntityRepository.Tests.ComplexDeletableEntityRepository
{
    using System.Collections.Generic;
    using System.Linq;
    using BaseTestClasses.Database;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.EntityRepository.Helpers;

    [TestClass()]
    public class ComplexDeletableEntityDeleteRepositoryTests : RepositoryTestBase
    {

        [NotNull]
        private IComplexDeletableEntityDeleteRepository<First> _repository;
        [NotNull]
        private IBaseRepository<First> _baseRepository;

        private IEnumerable<First> _items = new[]
            {
                new First
                {
                    Id = 1,
                    IsDeleted = true,
                    IsSaved = true,
                },
                new First
                {
                    Id = 2,
                    IsDeleted = false,
                    IsSaved = true,
                },
                new First
                {
                    Id = 3,
                    IsDeleted = true,
                    IsSaved = false,
                },
                new First
                {
                    Id = 4,
                    IsDeleted = false,
                    IsSaved = false,
                },
                new First
                {
                    Id = 5,
                    IsDeleted = false,
                    IsSaved = true,
                },
            };

        [TestInitialize]
        public void Init()
        {
            TestInit();

            _baseRepository = RepositoryFactory.GetBaseRepository<First>();

            var updateRepository = new UpdateRepository<First>(
                ContextWrapper,
                IncludeProcessor,
                new DatabaseCommitter(ContextWrapper),
                new Transactor(),
                new Detacher()
                );

            _repository = new ComplexDeletableEntityDeleteRepository<First>(
                 ContextWrapper,
                IncludeProcessor,
                new DatabaseCommitter(ContextWrapper),
                new Transactor(),
                new Detacher(),
                new RepositoryHelper(),
                updateRepository);
        }

        [TestMethod()]
        public void DeleteStubsTest()
        {
            _baseRepository.Delete(first => true);
            Seed(_items);

            var itemsBefore = _baseRepository.GetAll();

            Assert.AreEqual(itemsBefore.Count, 5);
            Assert.AreEqual(itemsBefore.Count(first => !first.IsSaved), 2);
            
            _repository.DeleteStubs();
            
            var itemsAfter = _baseRepository.GetAll();


            Assert.AreEqual(itemsAfter.Count, 3);
            Assert.AreEqual(itemsAfter.Count(first => !first.IsSaved), 0);

            Assert.IsTrue(itemsAfter.All(x => new []{1,2,5}.Contains(x.Id)));
        }

        [TestMethod()]
        public void DeleteTest()
        {

            _baseRepository.Delete(first => true);
            Seed(_items);

            var itemsBefore = _baseRepository.GetAll();
            Assert.AreEqual(itemsBefore.Count(first => first.IsDeleted), 2);

            _repository.Delete(first => true);

            var itemsAfter = _baseRepository.GetAll();

            Assert.AreEqual(itemsAfter.Count(first => first.IsDeleted), 4);

        }

        [TestMethod()]
        public void UndeleteTest()
        {
            _baseRepository.Delete(first => true);
            Seed(_items);

            var itemsBefore = _baseRepository.GetAll();
            Assert.AreEqual(itemsBefore.Count(first => first.IsDeleted), 2);

            _repository.Undelete(first => true);

            var itemsAfter = _baseRepository.GetAll();

            Assert.AreEqual(itemsAfter.Count(first => first.IsDeleted), 1);
        }

    }
}
