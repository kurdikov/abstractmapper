﻿namespace TsSoft.EntityRepository.Tests.ComplexDeletableEntityRepository
{
    using System.Collections.Generic;
    using BaseTestClasses.Database;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.EntityRepository.Helpers;

    [TestClass()]
    public class ComplexDeletableEntityUpdateRepositoryTests : RepositoryTestBase
    {
        private IEnumerable<First> _items = new[]
            {
                new First
                {
                    Id = 1,
                    IsDeleted = true,
                    IsSaved = true,
                },
                new First
                {
                    Id = 2,
                    IsDeleted = false,
                    IsSaved = true,
                },
                new First
                {
                    Id = 3,
                    IsDeleted = true,
                    IsSaved = false,
                },
            };

        [TestInitialize]
        public void Init()
        {
            TestInit();
            
        }

        [TestMethod()]
        public void ComplexDeletableEntityUpdateRepositoryTest()
        {
            Seed(_items);
            var repository = new ComplexDeletableEntityUpdateRepository<First>(
               ContextWrapper,
               IncludeProcessor,
               new DatabaseCommitter(ContextWrapper),
               new Transactor(),
               new Detacher(),
               new RepositoryHelper());

            var updatedByNothig = repository.UpdateSingle(entity => entity.IsDeleted = false, entity => entity.Id == 1);
            Assert.IsTrue(updatedByNothig.IsSaved);
            Assert.IsFalse(updatedByNothig.IsDeleted);

            var updatedByChangeIsSaved = repository.UpdateSingle(entity => entity.IsSaved = false, entity => entity.Id == 1);
            Assert.IsFalse(updatedByChangeIsSaved.IsSaved);
        }
    }
}
