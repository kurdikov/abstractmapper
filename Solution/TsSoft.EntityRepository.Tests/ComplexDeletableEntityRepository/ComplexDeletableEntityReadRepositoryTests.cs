﻿namespace TsSoft.EntityRepository.Tests.ComplexDeletableEntityRepository
{
    using System.Collections.Generic;
    using BaseTestClasses.Database;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.EntityRepository.Helpers;

    [TestClass()]
    public class ComplexDeletableEntityReadRepositoryTests : RepositoryTestBase
    {

        private ComplexDeletableEntityReadRepository<First> _repository;

        private IEnumerable<First> _items = new[]
            {
                new First
                {
                    Id = 1,
                    IsDeleted = true,
                    IsSaved = true,
                },
                new First
                {
                    Id = 2,
                    IsDeleted = false,
                    IsSaved = true,
                },
                new First
                {
                    Id = 3,
                    IsDeleted = true,
                    IsSaved = false,
                },
                new First
                {
                    Id = 4,
                    IsDeleted = false,
                    IsSaved = false,
                },
                new First
                {
                    Id = 5,
                    IsDeleted = false,
                    IsSaved = true,
                },
            };

        [TestInitialize]
        public void Init()
        {
            TestInit();
            Seed(_items);
            _repository = new ComplexDeletableEntityReadRepository<First>(
                ContextWrapper,
                IncludeProcessor,
                EntityStaticHelpers.MapperResolver.GetTypeDeterminingMapper<First>(),
                EntityStaticHelpers.IncludeDescriptionHelper,
                new RepositoryHelper()
                );
        }

        [TestMethod()]
        public void GetAllTest()
        {
            var items = _repository.GetAll();

            Assert.AreEqual(items.Count, 2);
        }

        [TestMethod()]
        public void GetSingleWithStubsTest()
        {
            var deletedAndNotSavedWithStubs = _repository.GetSingleWithStubs(first => first.Id == 3);
            Assert.IsNull(deletedAndNotSavedWithStubs);

            var onlyNotSavedWithStubs = _repository.GetSingleWithStubs(first => first.Id == 4);
            Assert.IsNotNull(onlyNotSavedWithStubs);
            Assert.IsFalse(onlyNotSavedWithStubs.IsSaved);
            Assert.IsFalse(onlyNotSavedWithStubs.IsDeleted);

            var deleteAndNotSaved = _repository.GetSingle(first => first.Id == 3);
            Assert.IsNull(deleteAndNotSaved);

            var onlyNotSaved = _repository.GetSingle(first => first.Id == 4);
            Assert.IsNull(onlyNotSaved);

        }

    }
}
