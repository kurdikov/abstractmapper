﻿namespace TsSoft.EntityRepository.Tests.DeletableRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using BaseTestClasses.Database;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.Expressions.SelectBuilder.Models;

    [TestClass]
    public class DeletableEntityReadRepositoryTests : RepositoryTestBase
    {
        private static readonly SelectExpression<First> SecondsSelect = new SelectExpression<First>
        {
            Select = first => new
            {
                first.Id,
                first.Seconds
            }
        };

        private static readonly Expression<Func<First, object>>[] SecondsInclude = {first => first.Seconds};

        private readonly IEnumerable<First> _items = new[]
        {
            new First
            {
                Id = 1,
                IsDeleted = false,
                Seconds = new List<Second>
                {
                    new Second
                    {
                        IsDeleted = true,
                        Id = 11
                    },
                    new Second
                    {
                        IsDeleted = false,
                        Id = 12
                    }
                }
            },
            new First
            {
                Id = 2,
                IsDeleted = true,
                Seconds = new List<Second>
                {
                    new Second
                    {
                        IsDeleted = true,
                        Id = 21
                    },
                    new Second
                    {
                        IsDeleted = false,
                        Id = 22
                    }
                }
            },
            new First
            {
                Id = 3,
                IsDeleted = true,
                Seconds = new List<Second>
                {
                    new Second
                    {
                        IsDeleted = true,
                        Id = 31
                    },
                    new Second
                    {
                        IsDeleted = false,
                        Id = 32
                    }
                }
            }
        };

        private DeletableEntityReadRepository<First> _readRepository;

        [TestInitialize]
        public void Init()
        {
            TestInit();
            Seed(_items);
            _readRepository = new DeletableEntityReadRepository<First>(ContextWrapper, IncludeProcessor,
                EntityStaticHelpers.MapperResolver.GetTypeDeterminingMapper<First>(),
                EntityStaticHelpers.IncludeDescriptionHelper, new RepositoryHelper());
        }

        [TestMethod]
        public void GetSingleDeletedTest()
        {
            var result = _readRepository.GetSingle(first => first.Id == 2);

            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetSingleNotDeletedTest()
        {
            const int id = 1;
            var result = _readRepository.GetSingle(first => first.Id == id);

            Assert.IsNotNull(result);
            Assert.AreEqual(id, result.Id);
        }

        [TestMethod]
        public void GetSingleNotDeletedSelectTest()
        {
            var result = _readRepository.GetSingle(first => first.Id == 1, SecondsSelect);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Seconds);
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual(2, result.Seconds.Count);
        }

        [TestMethod]
        public void GetSingleNotDeletedIncludesTest()
        {
            var result = _readRepository.GetSingle(first => first.Id == 1, SecondsInclude);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Seconds);
            Assert.AreEqual(2, result.Seconds.Count);
        }

        [TestMethod]
        public void GetSingleDeletedSelectTest()
        {
            var result = _readRepository.GetSingle(first => first.Id == 2, SecondsSelect);
            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetSingleDeletedIncludesTest()
        {
            var result = _readRepository.GetSingle(first => first.Id == 2, SecondsInclude);

            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetSingleWithDeletedNotDeletedSelectTest()
        {
            var result = _readRepository.GetSingleWithDeleted(first => first.Id == 1, SecondsSelect);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Seconds);
            Assert.AreEqual(2, result.Seconds.Count);
            Assert.AreEqual(1, result.Id);
        }

        [TestMethod]
        public void GetSingleWithDeletedNotDeletedIncludesTest()
        {
            var result = _readRepository.GetSingleWithDeleted(first => first.Id == 1, SecondsInclude);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Seconds);
            Assert.AreEqual(2, result.Seconds.Count);
        }

        [TestMethod]
        public void GetSingleWithDeletedDeletedSelectTest()
        {
            var result = _readRepository.GetSingleWithDeleted(first => first.Id == 2, SecondsSelect);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Seconds);
            Assert.AreEqual(2, result.Seconds.Count);
            Assert.AreEqual(2, result.Id);
        }

        [TestMethod]
        public void GetSingleWithDeletedDeletedIncludesTest()
        {
            var result = _readRepository.GetSingleWithDeleted(first => first.Id == 2, SecondsInclude);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Seconds);
            Assert.AreEqual(2, result.Seconds.Count);
        }

        [TestMethod]
        public void GetSingleWithDeletedDeletedTest()
        {
            const int expected = 2;
            var result = _readRepository.GetSingleWithDeleted(first => first.Id == expected);

            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.Id);
        }

        [TestMethod]
        public void GetSingleWithDeletedNotDeletedTest()
        {
            const int expected = 1;
            var result = _readRepository.GetSingleWithDeleted(first => first.Id == expected);

            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.Id);
        }

        [TestMethod]
        public void GetAllTest()
        {
            var result = _readRepository.GetAll();

            Assert.AreEqual(result.Count, 1);
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 1));
        }

        [TestMethod]
        public void GetWithDeletedTest()
        {
            var result = _readRepository.GetWithDeleted(first => true);

            Assert.AreEqual(result.Count, 3);
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 1));
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 2));
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 3));
        }

        [TestMethod]
        public void GetWithDeletedIncludesTest()
        {
            var result = _readRepository.GetWithDeleted(first => true, SecondsInclude);

            Assert.AreEqual(result.Count, 3);
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 1 && x.Seconds != null && x.Seconds.Count == 2));
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 2 && x.Seconds != null && x.Seconds.Count == 2));
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 3 && x.Seconds != null && x.Seconds.Count == 2));
        }

        [TestMethod]
        public void GetWithDeletedSelectTest()
        {
            var result = _readRepository.GetWithDeleted(first => true, SecondsSelect);

            Assert.AreEqual(result.Count, 3);
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 1 && x.Seconds != null && x.Seconds.Count == 2));
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 2 && x.Seconds != null && x.Seconds.Count == 2));
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 3 && x.Seconds != null && x.Seconds.Count == 2));
        }

        [TestMethod]
        public void GetTest()
        {
            var result = _readRepository.Get(first => true);

            Assert.AreEqual(result.Count, 1);
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 1));
        }

        [TestMethod]
        public void GetIncludesTest()
        {
            var result = _readRepository.Get(first => true, SecondsInclude);

            Assert.AreEqual(result.Count, 1);
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 1 && x.Seconds != null && x.Seconds.Count == 2));
        }

        [TestMethod]
        public void GetSelectTest()
        {
            var result = _readRepository.Get(first => true, SecondsSelect);

            Assert.AreEqual(result.Count, 1);
            Assert.IsNotNull(result.SingleOrDefault(x => x.Id == 1 && x.Seconds != null && x.Seconds.Count == 2));
        }

        [TestMethod]
        public void CountTest()
        {
            var result = _readRepository.Count(first => true);

            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void CountWithDeletedTest()
        {
            var result = _readRepository.CountWithDeleted(first => true);

            Assert.AreEqual(result, 3);
        }

        [TestMethod]
        public void ExistsTest()
        {
            var result = _readRepository.Exists(first => true);
            Assert.IsTrue(result);

            result = _readRepository.Exists(first => first.Id == 2);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ExistsWithDeletedTest()
        {
            var result = _readRepository.ExistsWithDeleted(first => true);
            Assert.IsTrue(result);

            result = _readRepository.ExistsWithDeleted(first => first.Id == 2);
            Assert.IsTrue(result);
        }
    }
}