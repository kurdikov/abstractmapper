﻿namespace TsSoft.EntityRepository.Tests.DeletableRepository
{
    using System.Collections.Generic;
    using System.Linq;
    using BaseTestClasses.Database;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass()]
    public class DeletableEntityDeleteRepositoryTests : RepositoryTestBase
    {
        [NotNull]
        private IDeletableEntityDeleteRepository<First> _repository;
        [NotNull]
        private IReadRepository<First> _baseRepository; 

        private readonly IEnumerable<First> _items = new[]
        {
            new First
            {
                Id = 1,
                IsDeleted = false,
            },
            new First
            {
                Id = 2,
                IsDeleted = true,
            },
            new First
            {
                Id = 3,
                IsDeleted = false,
            },
        };

        [TestInitialize]
        public void Init()
        {
            TestInit();
            Seed(_items);

            _baseRepository = RepositoryFactory.GetBaseRepository<First>();

            var updateRepository = new UpdateRepository<First>(
                ContextWrapper,
                IncludeProcessor,
                new DatabaseCommitter(ContextWrapper),
                new Transactor(),
                new Detacher()
                );
           
            _repository = new DeletableEntityDeleteRepository<First>(
                 ContextWrapper,
                IncludeProcessor,
                new DatabaseCommitter(ContextWrapper),
                new Transactor(),
                new Detacher(),
                updateRepository);
        }

        [TestMethod()]
        public void DeletableEntityDeleteRepositoryTest()
        {
            var allItems = _baseRepository.GetAll();

            Assert.AreEqual(allItems.Count, 3);
            Assert.AreEqual(allItems.Count(x => x.IsDeleted), 1);


            //Delete test
            _repository.DeleteSingle(first => first.Id == 3);

            var itemsAfterRemove = _baseRepository.GetAll();
            
            Assert.AreEqual(itemsAfterRemove.Count, 3);
            Assert.AreEqual(itemsAfterRemove.Count(x => x.IsDeleted), 2);
            
            var justDeleted = itemsAfterRemove.SingleOrDefault(x => x.Id == 3);
            Assert.IsNotNull(justDeleted);
            Assert.IsTrue(justDeleted.IsDeleted);

            //Undelete test
            _repository.Undelete(first => first.Id == 2 || first.Id == 3);
            var itemsAfterUndelete = _baseRepository.GetAll();
            Assert.AreEqual(itemsAfterUndelete.Count(x => x.IsDeleted), 0);
        }

    }
}
