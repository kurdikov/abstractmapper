﻿namespace TsSoft.EntityRepository.Tests.BaseRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETCORE
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq;
    using System.Threading.Tasks;
    using BaseTestClasses;
    using BaseTestClasses.Database;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    [TestClass]
    public class BaseRepositoryTests : RepositoryTestBase
    {
        private readonly First[] _data;
        public BaseRepositoryTests()
        {
            var impMoment = DateTime.Now;
            _data = new[]
                {
                    new First
                        {
                            ClientId = 5000,
                            IsDeleted = false,
                            ImportantMoment = impMoment,
                        },
                    new First
                        {
                            ClientId = 5000,
                            IsDeleted = false,
                            ImportantMoment = impMoment,
                        },
                    new First
                        {
                            ClientId = 9000,
                            IsDeleted = false,
                            ImportantMoment = impMoment.AddYears(1),
                        }
                };
        }

        [TestMethod]
        public void CreateTest()
        {
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var expected = new First
                {
                    ClientId = 5000,
                    IsDeleted = false,
                };

            var actual = repository.Create(expected);

            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.ClientId, actual.ClientId);
            Assert.AreEqual(expected.IsDeleted, actual.IsDeleted);
            Assert.IsNull(expected.Name);
            Assert.IsNotNull(expected.ImportantMoment);

            Assert.AreEqual(EntityState.Unchanged, Context.Entry(actual).State);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void CreateDetachedTest()
        {
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var expected = new First
            {
                ClientId = 5000,
                IsDeleted = false,
            };

            var actual = repository.CreateDetached(expected);

            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.ClientId, actual.ClientId);
            Assert.AreEqual(expected.IsDeleted, actual.IsDeleted);
            Assert.IsNull(expected.Name);
            Assert.IsNotNull(expected.ImportantMoment);

            Assert.AreEqual(EntityState.Detached, Context.Entry(actual).State);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void CreateDetachedWithSubentitiesTest()
        {
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var seconds = new[]
            {
                new Second {Type = "1"},
                new Second {Type = "2"},
            };
            var zero = new Zero {String = "3"};
            var expected = new First
            {
                ClientId = 5000,
                IsDeleted = false,
                Seconds = new List<Second>(seconds),
                Zero = zero,
            };

            var actual = repository.CreateDetached(expected, c => c.Seconds, c => c.Zero);

            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.ClientId, actual.ClientId);
            Assert.AreEqual(expected.IsDeleted, actual.IsDeleted);
            Assert.IsNull(expected.Name);
            Assert.IsNotNull(expected.ImportantMoment);

            Assert.AreEqual(EntityState.Detached, Context.Entry(actual).State);
            Assert.AreEqual(EntityState.Detached, Context.Entry(zero).State);
            Assert.IsTrue(seconds.All(s => Context.Entry(s).State == EntityState.Detached));

            actual = repository.GetSingle(f => f.ClientId == 5000, new Includes<First> {f => f.Seconds, f => f.Zero});

            Assert.AreEqual(2, actual.Seconds.Count);
            Assert.IsTrue(actual.Seconds.Any(s => s.Type == "1"));
            Assert.IsTrue(actual.Seconds.Any(s => s.Type == "2"));
            Assert.IsNotNull(actual.Zero);
            Assert.AreEqual("3", actual.Zero.String);

            CheckContextRetrievals(2);

        }

        [TestMethod]
        public void UpdateSingleTest()
        {
            var repository = RepositoryFactory.GetBaseRepository<First>();
            Seed(_data);
            var expected = new First
                {
                    Id = 1,
                    IsDeleted = true,
                    Name = "TestMethod",
                };

            var actual = repository.UpdateSingle(
                entity =>
                {
                    entity.ClientId = expected.ClientId;
                    entity.IsDeleted = expected.IsDeleted;
                    entity.Name = expected.Name;
                    entity.ImportantMoment = expected.ImportantMoment;
                }, first => first.Id == expected.Id);

            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.ClientId, actual.ClientId);
            Assert.AreEqual(expected.IsDeleted, actual.IsDeleted);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.ImportantMoment, actual.ImportantMoment);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void UpdateTest()
        {
            var repository = RepositoryFactory.GetBaseRepository<First>();
            Seed(_data);
            var expected = new First
            {
                Id = 1,
                IsDeleted = true,
                Name = "TestMethod",
                ClientId = 5000,
            };

            var actual = repository.Update(
                entity =>
                {
                    entity.IsDeleted = expected.IsDeleted;
                    entity.Name = expected.Name;
                }, first => first.ClientId <= expected.ClientId).ToArray();

            Assert.IsNotNull(actual);
            Assert.AreEqual(2, actual.Length);
            Assert.AreEqual(expected.IsDeleted, actual[0].IsDeleted);
            Assert.AreEqual(expected.Name, actual[0].Name);
            Assert.AreEqual(expected.IsDeleted, actual[1].IsDeleted);
            Assert.AreEqual(expected.Name, actual[1].Name);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void GetTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = repository.Get(first => first.ClientId == 5000).ToArray();
            Assert.IsNotNull(actual);
            Assert.AreEqual(2, actual.Length);
            Assert.AreEqual(_data[0].ClientId, actual[0].ClientId);
            Assert.AreEqual(_data[0].IsDeleted, actual[0].IsDeleted);
            Assert.IsNotNull(actual[0].ImportantMoment);
            Assert.AreEqual(_data[1].ClientId, actual[1].ClientId);
            Assert.AreEqual(_data[1].IsDeleted, actual[1].IsDeleted);
            Assert.IsNotNull(actual[1].ImportantMoment);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void GetWithOrderDescendingTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = repository.Get(first => true, new Includes<First>(), new[] {OrderByClause<First>.Create(f => f.ClientId, true)}).ToArray();
            Assert.IsNotNull(actual);
            Assert.AreEqual(3, actual.Length);
            Assert.AreEqual(_data[2].ClientId, actual[0].ClientId);
            Assert.AreEqual(_data[2].IsDeleted, actual[0].IsDeleted);
            Assert.IsNotNull(actual[2].ImportantMoment);
            Assert.AreEqual(_data[0].ClientId, actual[1].ClientId);
            Assert.AreEqual(_data[0].IsDeleted, actual[1].IsDeleted);
            Assert.IsNotNull(actual[1].ImportantMoment);
            Assert.AreEqual(_data[1].ClientId, actual[2].ClientId);
            Assert.AreEqual(_data[1].IsDeleted, actual[2].IsDeleted);
            Assert.IsNotNull(actual[2].ImportantMoment);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void GetWithOrderAscendingTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = repository.Get(first => true, new Includes<First>(), new[] { OrderByClause<First>.Create(f => f.ClientId) }).ToArray();
            Assert.IsNotNull(actual);
            Assert.AreEqual(3, actual.Length);
            Assert.AreEqual(_data[1].ClientId, actual[0].ClientId);
            Assert.AreEqual(_data[1].IsDeleted, actual[0].IsDeleted);
            Assert.IsNotNull(actual[0].ImportantMoment);
            Assert.AreEqual(_data[0].ClientId, actual[1].ClientId);
            Assert.AreEqual(_data[0].IsDeleted, actual[1].IsDeleted);
            Assert.IsNotNull(actual[1].ImportantMoment);
            Assert.AreEqual(_data[2].ClientId, actual[2].ClientId);
            Assert.AreEqual(_data[2].IsDeleted, actual[2].IsDeleted);
            Assert.IsNotNull(actual[2].ImportantMoment);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void GetWithMultipleOrderingsTest()
        {
            var data = new[]
            {
                new First {ClientId = 3, Name = "2", ImportantMoment = new DateTime(2010, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 3, Name = "1", ImportantMoment = new DateTime(2010, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 2, Name = "2", ImportantMoment = new DateTime(2009, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 2, Name = "2", ImportantMoment = new DateTime(2010, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 3, Name = "1", ImportantMoment = new DateTime(2009, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 3, Name = "2", ImportantMoment = new DateTime(2009, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 2, Name = "1", ImportantMoment = new DateTime(2009, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 2, Name = "1", ImportantMoment = new DateTime(2010, 1, 1), ExternalEntityId = Guid.NewGuid()},
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = repository.Get(
                first => true,
                new Includes<First>(),
                new IOrderByClause<First>[]
                {
                    OrderByClause<First>.Create(f => f.ClientId, true),
                    OrderByClause<First>.Create(f => f.Name),
                    OrderByClause<First>.Create(f => f.ImportantMoment, true),
                }).ToArray();
            Assert.IsNotNull(actual);
            Assert.AreEqual(8, actual.Length);
            Assert.AreEqual(data[1].Id, actual[0].Id);
            Assert.AreEqual(data[4].Id, actual[1].Id);
            Assert.AreEqual(data[0].Id, actual[2].Id);
            Assert.AreEqual(data[5].Id, actual[3].Id);
            Assert.AreEqual(data[7].Id, actual[4].Id);
            Assert.AreEqual(data[6].Id, actual[5].Id);
            Assert.AreEqual(data[3].Id, actual[6].Id);
            Assert.AreEqual(data[2].Id, actual[7].Id);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void GetSingleTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = repository.GetSingle(first => first.Id == 1);
            Assert.IsNotNull(actual);
            Assert.AreEqual(_data[0].ClientId, actual.ClientId);
            Assert.AreEqual(_data[0].IsDeleted, actual.IsDeleted);
            Assert.IsNotNull(actual.ImportantMoment);
            actual = repository.GetSingle(1);
            Assert.IsNotNull(actual);
            Assert.AreEqual(_data[0].ClientId, actual.ClientId);
            Assert.AreEqual(_data[0].IsDeleted, actual.IsDeleted);
            Assert.IsNotNull(actual.ImportantMoment);
            actual = repository.GetSingle(first => first.Id == 100);
            Assert.IsNull(actual);
            actual = repository.GetSingle(100);
            Assert.IsNull(actual);

            CheckContextRetrievals(4);
        }

        [TestMethod]
        public void DeleteSingleThrowsTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            ExceptAssert.Throws<InvalidOperationException>(() => repository.DeleteSingle(first => first.ClientId == 5000));

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void GetSingleSelectTest()
        {
            var data = new[]
            {
                new First
                {
                    ClientId = 1,
                    Zero = new Zero {String = "1"},
                    NotInterestingSeconds = new[] {new NotInterestingSecond {ClientId = 2}}
                },
            };
            Seed(data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = repository.GetSingle(first => first.Id == 1, new SelectExpression<First>
            {
                Select = f => new
                {
                    f.ClientId, 
                    Zero = new {f.Zero.String, Firsts = f.Zero.Firsts.Select(ff => new {ff.ClientId})}, 
                    NotInterestingSeconds = f.NotInterestingSeconds.Select(s => new {s.ClientId})
                }
            });
            Assert.IsNotNull(actual);
            Assert.AreEqual(data[0].ClientId, actual.ClientId);
            Assert.AreEqual(data[0].Zero.String, actual.Zero.String);
            Assert.AreEqual(data[0].ClientId, actual.Zero.Firsts.First().ClientId);
            Assert.AreEqual(data[0].NotInterestingSeconds.First().ClientId, actual.NotInterestingSeconds.First().ClientId);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void GetAllTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = repository.GetAll().ToArray();
            Assert.IsNotNull(actual);
            Assert.AreEqual(3, actual.Length);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void CountTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var count = repository.Count(first => first.ClientId == 5000);
            Assert.AreEqual(2, count);
            count = repository.Count(first => first.ClientId == 15000);
            Assert.AreEqual(0, count);

            CheckContextRetrievals(2);
        }

        [TestMethod]
        public void ExistsTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var exists = repository.Exists(first => first.ClientId == 5000);
            Assert.IsTrue(exists);
            exists = repository.Exists(first => first.ClientId == 15000);
            Assert.IsFalse(exists);

            CheckContextRetrievals(2);
        }

        [TestMethod]
        public void DeleteTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = repository.GetAll().ToArray();
            Assert.AreEqual(3, actual.Length);
            repository.Delete(first => first.ClientId == 5000);
            actual = repository.GetAll().ToArray();
            Assert.AreEqual(1, actual.Length);

            CheckContextRetrievals(3);
        }

        [TestMethod]
        public void MaxTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = repository.Max(f => f.Id, f => f.ClientId == 5000);
            Assert.AreEqual(2, actual);
            actual = repository.Max(f => f.Id, f => f.Id == 0);
            Assert.AreEqual(null, actual);

            CheckContextRetrievals(2);
        }

        [TestMethod]
        public void MinTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = repository.Min(f => f.Id, f => f.ClientId == 5000);
            Assert.AreEqual(1, actual);
            actual = repository.Min(f => f.Id, f => f.Id == 0);
            Assert.AreEqual(null, actual);

            CheckContextRetrievals(2);
        }

        [TestMethod]
        public void CreateWithLinksTest()
        {
            var repository = RepositoryFactory.GetBaseRepository<Second>();
            var first = new First
            {
                Name = "TestRecord",
                IsDeleted = false,
                ImportantMoment = DateTime.Now,
            };

            var data = new[]
                {
                    new Second
                        {
                            Type = "TestType1",
                            First = first,
                        },
                    new Second
                        {
                            Type = "TestType2",
                            First = first,
                        },
                    new Second
                        {
                            Type = "TestType3",
                            First = first,
                        }
                };
            foreach (var second in data)
            {
                repository.Create(second);
            }

            var seconds = repository.Get(sec => sec.FirstId == 1).ToArray();
            Assert.IsNotNull(seconds);
            Assert.AreEqual(3, seconds.Length);
            Assert.AreEqual(1, seconds[0].FirstId);
            Assert.AreEqual(data[0].Type, seconds[0].Type);
            Assert.AreEqual(1, seconds[1].FirstId);
            Assert.AreEqual(data[1].Type, seconds[1].Type);
            Assert.AreEqual(1, seconds[2].FirstId);
            Assert.AreEqual(data[2].Type, seconds[2].Type);

            CheckContextRetrievals(1 + data.Length);
        }

        [TestMethod]
        public void UpdateLinksTest()
        {
            var data = new[]
                {
                    new First
                        {
                            ClientId = 1,
                            Name = "FirstName1",
                            Seconds = new[]
                                {
                                    new Second
                                        {
                                            ClientId = 1,
                                            Type = "TestType1",
                                        },
                                    new Second
                                        {
                                            ClientId = 2,
                                            Type = "TestType2",
                                        },
                                    new Second
                                        {
                                            ClientId = 3,
                                            Type = "TestType1",
                                        },
                                }
                        },
                    new First
                        {
                            ClientId = 2,
                            Name = "FirstName2",
                            Seconds = new[]
                                {
                                    new Second
                                        {
                                            ClientId = 4,
                                            Type = "TestType2"
                                        },
                                }
                        }
                };
            Seed(data);
            var repository = RepositoryFactory.GetBaseRepository<Second>();
            var first = new First
            {
                Name = "TestRecord",
                IsDeleted = false,
                ImportantMoment = DateTime.Now,
            };

            repository.Update(entity => { entity.First = first; }, sec => sec.Id <= 2).ToArray();
            var includes = new Includes<Second> { s => s.First };
            var actual = repository.Get(sec => sec.FirstId == 3, includes).ToArray();
            Assert.IsNotNull(actual);
            Assert.AreEqual(2, actual.Length);
            Assert.AreEqual(3, actual[0].First.Id);
            Assert.AreEqual(first.Name, actual[0].First.Name);
            Assert.AreEqual(3, actual[1].First.Id);
            Assert.AreEqual(first.Name, actual[1].First.Name);

            CheckContextRetrievals(2);
        }


        [TestMethod]
        public async Task CreateAsyncTest()
        {
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var expected = new First
            {
                ClientId = 5000,
                IsDeleted = false,
            };

            var actual = await repository.CreateAsync(expected);

            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.ClientId, actual.ClientId);
            Assert.AreEqual(expected.IsDeleted, actual.IsDeleted);
            Assert.IsNull(expected.Name);
            Assert.IsNotNull(expected.ImportantMoment);

            Assert.AreEqual(EntityState.Unchanged, Context.Entry(actual).State);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task CreateDetachedAsyncTest()
        {
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var expected = new First
            {
                ClientId = 5000,
                IsDeleted = false,
            };

            var actual = await repository.CreateDetachedAsync(expected);

            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.ClientId, actual.ClientId);
            Assert.AreEqual(expected.IsDeleted, actual.IsDeleted);
            Assert.IsNull(expected.Name);
            Assert.IsNotNull(expected.ImportantMoment);

            Assert.AreEqual(EntityState.Detached, Context.Entry(actual).State);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task CreateDetachedWithSubentitiesAsyncTest()
        {
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var seconds = new[]
            {
                new Second {Type = "1"},
                new Second {Type = "2"},
            };
            var zero = new Zero { String = "3" };
            var expected = new First
            {
                ClientId = 5000,
                IsDeleted = false,
                Seconds = new List<Second>(seconds),
                Zero = zero,
            };

            var actual = await repository.CreateDetachedAsync(expected, c => c.Seconds, c => c.Zero);

            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.ClientId, actual.ClientId);
            Assert.AreEqual(expected.IsDeleted, actual.IsDeleted);
            Assert.IsNull(expected.Name);
            Assert.IsNotNull(expected.ImportantMoment);

            Assert.AreEqual(EntityState.Detached, Context.Entry(actual).State);
            Assert.AreEqual(EntityState.Detached, Context.Entry(zero).State);
            Assert.IsTrue(seconds.All(s => Context.Entry(s).State == EntityState.Detached));

            actual = await repository.GetSingleAsync(f => f.ClientId == 5000, new Includes<First> { f => f.Seconds, f => f.Zero });

            Assert.AreEqual(2, actual.Seconds.Count);
            Assert.IsTrue(actual.Seconds.Any(s => s.Type == "1"));
            Assert.IsTrue(actual.Seconds.Any(s => s.Type == "2"));
            Assert.IsNotNull(actual.Zero);
            Assert.AreEqual("3", actual.Zero.String);

            CheckContextRetrievals(2);

        }

        [TestMethod]
        public async Task UpdateSingleAsyncTest()
        {
            var repository = RepositoryFactory.GetBaseRepository<First>();
            Seed(_data);
            var expected = new First
            {
                Id = 1,
                IsDeleted = true,
                Name = "TestMethod",
            };

            var actual = await repository.UpdateSingleAsync(
                entity =>
                {
                    entity.ClientId = expected.ClientId;
                    entity.IsDeleted = expected.IsDeleted;
                    entity.Name = expected.Name;
                    entity.ImportantMoment = expected.ImportantMoment;
                }, first => first.Id == expected.Id);

            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.ClientId, actual.ClientId);
            Assert.AreEqual(expected.IsDeleted, actual.IsDeleted);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.ImportantMoment, actual.ImportantMoment);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task UpdateAsyncTest()
        {
            var repository = RepositoryFactory.GetBaseRepository<First>();
            Seed(_data);
            var expected = new First
            {
                Id = 1,
                IsDeleted = true,
                Name = "TestMethod",
                ClientId = 5000,
            };

            var actual = (await repository.UpdateAsync(
                entity =>
                {
                    entity.IsDeleted = expected.IsDeleted;
                    entity.Name = expected.Name;
                }, first => first.ClientId <= expected.ClientId)).ToArray();

            Assert.IsNotNull(actual);
            Assert.AreEqual(2, actual.Length);
            Assert.AreEqual(expected.IsDeleted, actual[0].IsDeleted);
            Assert.AreEqual(expected.Name, actual[0].Name);
            Assert.AreEqual(expected.IsDeleted, actual[1].IsDeleted);
            Assert.AreEqual(expected.Name, actual[1].Name);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task GetAsyncTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = (await repository.GetAsync(first => first.ClientId == 5000)).ToArray();
            Assert.IsNotNull(actual);
            Assert.AreEqual(2, actual.Length);
            Assert.AreEqual(_data[0].ClientId, actual[0].ClientId);
            Assert.AreEqual(_data[0].IsDeleted, actual[0].IsDeleted);
            Assert.IsNotNull(actual[0].ImportantMoment);
            Assert.AreEqual(_data[1].ClientId, actual[1].ClientId);
            Assert.AreEqual(_data[1].IsDeleted, actual[1].IsDeleted);
            Assert.IsNotNull(actual[1].ImportantMoment);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task GetWithOrderDescendingAsyncTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = (await repository.GetAsync(first => true, new Includes<First>(), new[] { OrderByClause<First>.Create(f => f.ClientId, true) })).ToArray();
            Assert.IsNotNull(actual);
            Assert.AreEqual(3, actual.Length);
            Assert.AreEqual(_data[2].ClientId, actual[0].ClientId);
            Assert.AreEqual(_data[2].IsDeleted, actual[0].IsDeleted);
            Assert.IsNotNull(actual[2].ImportantMoment);
            Assert.AreEqual(_data[0].ClientId, actual[1].ClientId);
            Assert.AreEqual(_data[0].IsDeleted, actual[1].IsDeleted);
            Assert.IsNotNull(actual[1].ImportantMoment);
            Assert.AreEqual(_data[1].ClientId, actual[2].ClientId);
            Assert.AreEqual(_data[1].IsDeleted, actual[2].IsDeleted);
            Assert.IsNotNull(actual[2].ImportantMoment);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task GetWithOrderAscendingAsyncTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = (await repository.GetAsync(first => true, new Includes<First>(), new[] { OrderByClause<First>.Create(f => f.ClientId) })).ToArray();
            Assert.IsNotNull(actual);
            Assert.AreEqual(3, actual.Length);
            Assert.AreEqual(_data[1].ClientId, actual[0].ClientId);
            Assert.AreEqual(_data[1].IsDeleted, actual[0].IsDeleted);
            Assert.IsNotNull(actual[0].ImportantMoment);
            Assert.AreEqual(_data[0].ClientId, actual[1].ClientId);
            Assert.AreEqual(_data[0].IsDeleted, actual[1].IsDeleted);
            Assert.IsNotNull(actual[1].ImportantMoment);
            Assert.AreEqual(_data[2].ClientId, actual[2].ClientId);
            Assert.AreEqual(_data[2].IsDeleted, actual[2].IsDeleted);
            Assert.IsNotNull(actual[2].ImportantMoment);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task GetWithMultipleOrderingsAsyncTest()
        {
            var data = new[]
            {
                new First {ClientId = 3, Name = "2", ImportantMoment = new DateTime(2010, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 3, Name = "1", ImportantMoment = new DateTime(2010, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 2, Name = "2", ImportantMoment = new DateTime(2009, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 2, Name = "2", ImportantMoment = new DateTime(2010, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 3, Name = "1", ImportantMoment = new DateTime(2009, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 3, Name = "2", ImportantMoment = new DateTime(2009, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 2, Name = "1", ImportantMoment = new DateTime(2009, 1, 1), ExternalEntityId = Guid.NewGuid()},
                new First {ClientId = 2, Name = "1", ImportantMoment = new DateTime(2010, 1, 1), ExternalEntityId = Guid.NewGuid()},
            };
            Seed(data);

            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = (await repository.GetAsync(
                first => true,
                new Includes<First>(),
                new IOrderByClause<First>[]
                {
                    OrderByClause<First>.Create(f => f.ClientId, true),
                    OrderByClause<First>.Create(f => f.Name),
                    OrderByClause<First>.Create(f => f.ImportantMoment, true),
                })).ToArray();
            Assert.IsNotNull(actual);
            Assert.AreEqual(8, actual.Length);
            Assert.AreEqual(data[1].Id, actual[0].Id);
            Assert.AreEqual(data[4].Id, actual[1].Id);
            Assert.AreEqual(data[0].Id, actual[2].Id);
            Assert.AreEqual(data[5].Id, actual[3].Id);
            Assert.AreEqual(data[7].Id, actual[4].Id);
            Assert.AreEqual(data[6].Id, actual[5].Id);
            Assert.AreEqual(data[3].Id, actual[6].Id);
            Assert.AreEqual(data[2].Id, actual[7].Id);

            CheckContextRetrievals(1);
        }
        [TestMethod]
        public async Task GetSingleAsyncTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = await repository.GetSingleAsync(first => first.Id == 1);
            Assert.IsNotNull(actual);
            Assert.AreEqual(_data[0].ClientId, actual.ClientId);
            Assert.AreEqual(_data[0].IsDeleted, actual.IsDeleted);
            Assert.IsNotNull(actual.ImportantMoment);
            actual = await repository.GetSingleAsync(1);
            Assert.IsNotNull(actual);
            Assert.AreEqual(_data[0].ClientId, actual.ClientId);
            Assert.AreEqual(_data[0].IsDeleted, actual.IsDeleted);
            Assert.IsNotNull(actual.ImportantMoment);
            actual = await repository.GetSingleAsync(first => first.Id == 100);
            Assert.IsNull(actual);
            actual = await repository.GetSingleAsync(100);
            Assert.IsNull(actual);

            CheckContextRetrievals(4);
        }

        [TestMethod]
        public async Task DeleteSingleThrowsAsyncTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            await ExceptAssert.ThrowsAsync<InvalidOperationException>(() => repository.DeleteSingleAsync(first => first.ClientId == 5000));

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task GetSingleSelectAsyncTest()
        {
            var data = new[]
            {
                new First
                {
                    ClientId = 1,
                    Zero = new Zero {String = "1"},
                    NotInterestingSeconds = new[] {new NotInterestingSecond {ClientId = 2}}
                },
            };
            Seed(data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = await repository.GetSingleAsync(first => first.Id == 1, new SelectExpression<First>
            {
                Select = f => new
                {
                    f.ClientId,
                    Zero = new { f.Zero.String, Firsts = f.Zero.Firsts.Select(ff => new { ff.ClientId }) },
                    NotInterestingSeconds = f.NotInterestingSeconds.Select(s => new { s.ClientId })
                }
            });
            Assert.IsNotNull(actual);
            Assert.AreEqual(data[0].ClientId, actual.ClientId);
            Assert.AreEqual(data[0].Zero.String, actual.Zero.String);
            Assert.AreEqual(data[0].ClientId, actual.Zero.Firsts.First().ClientId);
            Assert.AreEqual(data[0].NotInterestingSeconds.First().ClientId, actual.NotInterestingSeconds.First().ClientId);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task GetAllAsyncTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = await repository.GetAllAsync();
            Assert.IsNotNull(actual);
            Assert.AreEqual(3, actual.Count);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task CountAsyncTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var count = await repository.CountAsync(first => first.ClientId == 5000);
            Assert.AreEqual(2, count);
            count = await repository.CountAsync(first => first.ClientId == 15000);
            Assert.AreEqual(0, count);

            CheckContextRetrievals(2);
        }

        [TestMethod]
        public async Task ExistsAsyncTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var exists = await repository.ExistsAsync(first => first.ClientId == 5000);
            Assert.IsTrue(exists);
            exists = await repository.ExistsAsync(first => first.ClientId == 15000);
            Assert.IsFalse(exists);

            CheckContextRetrievals(2);
        }

        [TestMethod]
        public async Task DeleteAsyncTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = (await repository.GetAllAsync()).ToArray();
            Assert.AreEqual(3, actual.Length);
            await repository.DeleteAsync(first => first.ClientId == 5000);
            actual = (await repository.GetAllAsync()).ToArray();
            Assert.AreEqual(1, actual.Length);

            CheckContextRetrievals(3);
        }

        [TestMethod]
        public async Task MaxAsyncTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = await repository.MaxAsync(f => f.Id, f => f.ClientId == 5000);
            Assert.AreEqual(2, actual);
            actual = await repository.MaxAsync(f => f.Id, f => f.Id == 0);
            Assert.AreEqual(null, actual);

            CheckContextRetrievals(2);
        }

        [TestMethod]
        public async Task MinAsyncTest()
        {
            Seed(_data);
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var actual = await repository.MinAsync(f => f.Id, f => f.ClientId == 5000);
            Assert.AreEqual(1, actual);
            actual = await repository.MinAsync(f => f.Id, f => f.Id == 0);
            Assert.AreEqual(null, actual);

            CheckContextRetrievals(2);
        }

        [TestMethod]
        public async Task CreateWithLinksAsyncTest()
        {
            var repository = RepositoryFactory.GetBaseRepository<Second>();
            var first = new First
            {
                Name = "TestRecord",
                IsDeleted = false,
                ImportantMoment = DateTime.Now,
            };

            var data = new[]
                {
                    new Second
                        {
                            Type = "TestType1",
                            First = first,
                        },
                    new Second
                        {
                            Type = "TestType2",
                            First = first,
                        },
                    new Second
                        {
                            Type = "TestType3",
                            First = first,
                        }
                };
            foreach (var second in data)
            {
                await repository.CreateAsync(second);
            }

            var seconds = (await repository.GetAsync(sec => sec.FirstId == 1)).ToArray();
            Assert.IsNotNull(seconds);
            Assert.AreEqual(3, seconds.Length);
            Assert.AreEqual(1, seconds[0].FirstId);
            Assert.AreEqual(data[0].Type, seconds[0].Type);
            Assert.AreEqual(1, seconds[1].FirstId);
            Assert.AreEqual(data[1].Type, seconds[1].Type);
            Assert.AreEqual(1, seconds[2].FirstId);
            Assert.AreEqual(data[2].Type, seconds[2].Type);

            CheckContextRetrievals(1 + data.Length);
        }

        [TestMethod]
        public async Task UpdateLinksAsyncTest()
        {
            var data = new[]
                {
                    new First
                        {
                            ClientId = 1,
                            Name = "FirstName1",
                            Seconds = new[]
                                {
                                    new Second
                                        {
                                            ClientId = 1,
                                            Type = "TestType1",
                                        },
                                    new Second
                                        {
                                            ClientId = 2,
                                            Type = "TestType2",
                                        },
                                    new Second
                                        {
                                            ClientId = 3,
                                            Type = "TestType1",
                                        },
                                }
                        },
                    new First
                        {
                            ClientId = 2,
                            Name = "FirstName2",
                            Seconds = new[]
                                {
                                    new Second
                                        {
                                            ClientId = 4,
                                            Type = "TestType2"
                                        },
                                }
                        }
                };
            Seed(data);
            var repository = RepositoryFactory.GetBaseRepository<Second>();
            var first = new First
            {
                Name = "TestRecord",
                IsDeleted = false,
                ImportantMoment = DateTime.Now,
            };

            (await repository.UpdateAsync(entity => { entity.First = first; }, sec => sec.Id <= 2)).ToArray();
            var includes = new Includes<Second> { s => s.First };
            var actual = (await repository.GetAsync(sec => sec.FirstId == 3, includes)).ToArray();
            Assert.IsNotNull(actual);
            Assert.AreEqual(2, actual.Length);
            Assert.AreEqual(3, actual[0].First.Id);
            Assert.AreEqual(first.Name, actual[0].First.Name);
            Assert.AreEqual(3, actual[1].First.Id);
            Assert.AreEqual(first.Name, actual[1].First.Name);

            CheckContextRetrievals(2);
        }
    }
}
