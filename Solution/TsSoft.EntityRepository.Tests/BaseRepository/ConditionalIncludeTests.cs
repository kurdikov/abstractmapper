﻿namespace TsSoft.EntityRepository.Tests.BaseRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using BaseTestClasses.Database;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.IncludeBuilder.Models;

    [TestClass]
    public class ConditionalIncludeTests : RepositoryTestBase
    {
        private void SeedData()
        {
            var fourths = new[]
            {
                new Fourth
                {
                    Id = Guid.NewGuid(),
                    ClientId = 1,
                    Fifths = new[]
                        {
                            new Fifth {ClientId = 1, Type = "Satisfies filter"},
                            new Fifth {ClientId = 2, Type = "Does not satisfy filter"},
                        }
                },
                new Fourth
                {
                    Id = Guid.NewGuid(),
                    ClientId = 2,
                    Fifths = new[]
                        {
                            new Fifth {ClientId = 3, Type = "Does not satisfy filter"},
                            new Fifth {ClientId = 4, Type = "Satisfies filter"},
                        }
                },
                new Fourth
                {
                    Id = Guid.NewGuid(),
                    ClientId = 3,
                    Fifths = new[]
                        {
                            new Fifth {ClientId = 5, Type = "Satisfies filter"},
                        }
                },
            };
            var data = new[]
            {
                new First
                {
                    ClientId = 1,
                    Name = "Name",
                    Seconds = new[]
                    {
                        new Second {ClientId = 1, Type = "One", Thirds = new[]
                            {
                                new Third {ClientId = 1, Type = "One", Fourth = fourths[0]},
                                new Third {ClientId = 2, Type = "Two", Fourth = fourths[1]}
                            }},
                        new Second {ClientId = 2, Type = "One", Thirds = new[]
                            {
                                new Third {ClientId = 3, Type = "One", Fourth = fourths[2]}
                            }},
                        new Second {ClientId = 3, Type = "Two", Thirds = new[]
                            {
                                new Third {ClientId = 4, Type = "Two", Fourth = fourths[2]},
                                new Third {ClientId = 5, Type = "One", Fourth = fourths[2]},
                            }},
                    }
                },
                new First
                {
                    ClientId = 2,
                    Name = "Name",
                    Seconds = new[]
                    {
                        new Second {ClientId = 4, Type = "Two"}, 
                    }
                }
            };
            Seed(data);
        }

        [TestMethod]
        public void TestConditionOnFirstStep()
        {
            SeedData();
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var result = repository.GetSingle(
                p => p.ClientId == 1,
                new Includes<First>
                {
                    p => p.Seconds.Select(pe => pe.Thirds),
                },
                includeConditions: new[]
                {
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds,
                        e => e.FirstId,
                        e => e.Type == "One")
                });
            var seconds = result.Seconds.ToList();

            Assert.AreEqual(2, seconds.Count);
            Assert.IsTrue(seconds.All(e => e.Type == "One"));
            Assert.AreEqual(1, seconds[0].ClientId);
            Assert.AreEqual(2, seconds[0].Thirds.Count);
            Assert.AreEqual(2, seconds[1].ClientId);
            Assert.AreEqual(1, seconds[1].Thirds.Count);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void TestConditionOnSecondStep()
        {
            SeedData();
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var result = repository.GetSingle(
                p => p.ClientId == 1,
                new Includes<First>
                {
                    p => p.Seconds.Select(pe => pe.Thirds.Select(o => o.Fourth)),
                },
                includeConditions: new[]
                {
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds.SelectMany(e => e.Thirds),
                        o => o.SecondId,
                        o => o.Type == "One")
                });
            var seconds = result.Seconds.ToList();

            Assert.AreEqual(3, seconds.Count);
            Assert.AreEqual(1, seconds[0].ClientId);
            Assert.AreEqual(1, seconds[0].Thirds.Count);
            Assert.AreEqual(2, seconds[1].ClientId);
            Assert.AreEqual(1, seconds[1].Thirds.Count);
            Assert.AreEqual(3, seconds[2].ClientId);
            Assert.AreEqual(1, seconds[2].Thirds.Count);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void TestNestedConditionsOnFirstAndSecondSteps()
        {
            SeedData();
            var repository = RepositoryFactory.GetBaseRepository<First>();
            // nested
            var result = repository.GetSingle(
                p => p.ClientId == 1,
                new Includes<First>
                {
                    p => p.Seconds.Select(pe => pe.Thirds.Select(peo => peo.Fourth)),
                },
                includeConditions: new[]
                {
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds,
                        e => e.FirstId,
                        e => e.Type == "One"),
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds.SelectMany(e => e.Thirds),
                        o => o.SecondId,
                        o => o.Type == "One"),
                });
            var seconds = result.Seconds.ToList();
            Assert.AreEqual(2, seconds.Count);
            Assert.IsTrue(seconds.All(e => e.Type == "One"));
            Assert.AreEqual(1, seconds[0].ClientId);
            Assert.AreEqual(1, seconds[0].Thirds.Count);
            Assert.IsNotNull(seconds[0].Thirds.First().Fourth);
            Assert.AreEqual(2, seconds[1].ClientId);
            Assert.AreEqual(1, seconds[1].Thirds.Count);
            Assert.IsNotNull(seconds[1].Thirds.First().Fourth);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void TestTwoIdenticalNestedConditions()
        {
            SeedData();
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var result = repository.GetSingle(
                p => p.ClientId == 1,
                new Includes<First>
                {
                    p => p.Seconds.Select(pe => pe.Thirds.Select(peo => peo.Fourth)),
                },
                includeConditions: new[]
                {
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds,
                        e => e.FirstId,
                        e => e.Type == "One"),
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds.SelectMany(e => e.Thirds),
                        o => o.SecondId,
                        o => o.Type == "One"),
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds.SelectMany(e => e.Thirds),
                        o => o.SecondId,
                        o => o.Type == "One"),
                });
            var seconds = result.Seconds.ToList();
            Assert.AreEqual(2, seconds.Count);
            Assert.IsTrue(seconds.All(e => e.Type == "One"));
            Assert.AreEqual(1, seconds[0].ClientId);
            Assert.AreEqual(1, seconds[0].Thirds.Count);
            Assert.IsNotNull(seconds[0].Thirds.First().Fourth);
            Assert.AreEqual(2, seconds[1].ClientId);
            Assert.AreEqual(1, seconds[1].Thirds.Count);
            Assert.IsNotNull(seconds[1].Thirds.First().Fourth);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public void TestNestedConditionsOnSecondAndFourthSteps()
        {
            SeedData();
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var result = repository.GetSingle(
                p => p.ClientId == 1,
                new Includes<First>
                    {
                        p => p.Seconds.Select(pe => pe.Thirds.Select(peo => peo.Fourth.Fifths))
                    },
                includeConditions: new[]
                    {
                        EntityStaticHelpers.ConditionalIncludeCreator.Create(
                            (First p) => p.Seconds.SelectMany(e => e.Thirds).SelectMany(o => o.Fourth.Fifths),
                            rp => rp.FourthId,
                            rp => rp.Type.StartsWith("Satisfies")),
                        EntityStaticHelpers.ConditionalIncludeCreator.Create(
                            (First p) => p.Seconds.SelectMany(e => e.Thirds),
                            o => o.SecondId,
                            o => new[] {1,2,4}.Contains(o.ClientId)),
                    });
            var seconds = result.Seconds.ToList();
            Assert.AreEqual(3, seconds.Count);
            Assert.AreEqual(1, seconds[0].ClientId);
            Assert.AreEqual(2, seconds[0].Thirds.Count);
            var org = seconds[0].Thirds.ElementAt(0);
            Assert.AreEqual(1, org.ClientId);
            Assert.AreEqual(1, org.Fourth.Fifths.Count);
            Assert.AreEqual(1, org.Fourth.Fifths.First().ClientId);
            Assert.IsTrue(org.Fourth.Fifths.All(rp => rp.Type.StartsWith("Satisfies")));
            org = seconds[0].Thirds.ElementAt(1);
            Assert.AreEqual(2, org.ClientId);
            Assert.AreEqual(1, org.Fourth.Fifths.Count);
            Assert.AreEqual(4, org.Fourth.Fifths.First().ClientId);
            Assert.IsTrue(org.Fourth.Fifths.All(rp => rp.Type.StartsWith("Satisfies")));
            Assert.IsNotNull(seconds[0].Thirds.First().Fourth);

            Assert.AreEqual(2, seconds[1].ClientId);
            Assert.AreEqual(0, seconds[1].Thirds.Count);

            Assert.AreEqual(3, seconds[2].ClientId);
            Assert.AreEqual(1, seconds[2].Thirds.Count);
            org = seconds[2].Thirds.ElementAt(0);
            Assert.AreEqual(4, org.ClientId);
            Assert.AreEqual(1, org.Fourth.Fifths.Count);
            Assert.AreEqual(5, org.Fourth.Fifths.First().ClientId);
            Assert.IsTrue(org.Fourth.Fifths.All(rp => rp.Type.StartsWith("Satisfies")));

             result = repository.GetSingle(
                p => p.ClientId == 2,
                new Includes<First>
                                {
                                    p => p.Seconds.Select(pe => pe.Thirds.Select(peo => peo.Fourth.Fifths))
                                },
                includeConditions: new[]
                                {
                                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                                        (First p) => p.Seconds.SelectMany(e => e.Thirds).SelectMany(o => o.Fourth.Fifths),
                                        rp => rp.FourthId,
                                        rp => rp.Type.StartsWith("Satisfies")),
                                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                                        (First p) => p.Seconds.SelectMany(e => e.Thirds),
                                        o => o.SecondId,
                                        o => new[] {1,2,4}.Contains(o.ClientId)),
                                });
            Assert.IsNotNull(result);

            CheckContextRetrievals(2);
        }

        [TestMethod]
        public void TestConditionWithCompositeKeys()
        {
            var id1 = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var id2 = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var data = new[]
            {
                new CompositePk
                {
                    Part1 = id1.Item1,
                    Part2 = id1.Item2,
                    Data = "1",
                    CompositeFks = new List<CompositeFk>
                    {
                        new CompositeFk{Data = "1.1"},
                        new CompositeFk{Data = "1.2"},
                        new CompositeFk{Data = "1.3"},
                    }
                },
                new CompositePk
                {
                    Part1 = id2.Item1,
                    Part2 = id2.Item2,
                    Data = "2",
                    CompositeFks = new List<CompositeFk>
                    {
                        new CompositeFk{Data = "2.1"},
                        new CompositeFk{Data = "2.2"},
                        new CompositeFk{Data = "2.3"},
                    }
                },
            };
            Seed(data);

            var repo = RepositoryFactory.GetBaseRepository<CompositePk>();
            var res = repo.GetAll(
                new Includes<CompositePk>
                {
                    p => p.CompositeFks
                },
                includeConditions: new[]
                {
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (CompositePk e) => e.CompositeFks,
                        ee => ee.Data.EndsWith("2"))
                });
            Assert.AreEqual(2, res.Count);
            var first = res.SingleOrDefault(f => f.Part1 == id1.Item1 && f.Part2 == id1.Item2);
            Assert.IsNotNull(first);
            Assert.IsNotNull(first.CompositeFks);
            Assert.AreEqual(1, first.CompositeFks.Count);
            Assert.AreEqual("1.2", first.CompositeFks.ElementAt(0).Data);
            var second = res.SingleOrDefault(f => f.Part1 == id2.Item1 && f.Part2 == id2.Item2);
            Assert.IsNotNull(second);
            Assert.IsNotNull(second.CompositeFks);
            Assert.AreEqual(1, second.CompositeFks.Count);
            Assert.AreEqual("2.2", second.CompositeFks.ElementAt(0).Data);

            CheckContextRetrievals(1);
        }



        [TestMethod]
        public async Task TestAsyncConditionOnFirstStep()
        {
            SeedData();
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var result = await repository.GetSingleAsync(
                p => p.ClientId == 1,
                new Includes<First>
                {
                    p => p.Seconds.Select(pe => pe.Thirds),
                },
                includeConditions: new[]
                {
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds,
                        e => e.FirstId,
                        e => e.Type == "One")
                });
            var seconds = result.Seconds.ToList();

            Assert.AreEqual(2, seconds.Count);
            Assert.IsTrue(seconds.All(e => e.Type == "One"));
            Assert.AreEqual(1, seconds[0].ClientId);
            Assert.AreEqual(2, seconds[0].Thirds.Count);
            Assert.AreEqual(2, seconds[1].ClientId);
            Assert.AreEqual(1, seconds[1].Thirds.Count);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task TestAsyncConditionOnSecondStep()
        {
            SeedData();
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var result = await repository.GetSingleAsync(
                p => p.ClientId == 1,
                new Includes<First>
                {
                    p => p.Seconds.Select(pe => pe.Thirds.Select(o => o.Fourth)),
                },
                includeConditions: new[]
                {
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds.SelectMany(e => e.Thirds),
                        o => o.SecondId,
                        o => o.Type == "One")
                });
            var seconds = result.Seconds.ToList();

            Assert.AreEqual(3, seconds.Count);
            Assert.AreEqual(1, seconds[0].ClientId);
            Assert.AreEqual(1, seconds[0].Thirds.Count);
            Assert.AreEqual(2, seconds[1].ClientId);
            Assert.AreEqual(1, seconds[1].Thirds.Count);
            Assert.AreEqual(3, seconds[2].ClientId);
            Assert.AreEqual(1, seconds[2].Thirds.Count);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task TestAsyncNestedConditionsOnFirstAndSecondSteps()
        {
            SeedData();
            var repository = RepositoryFactory.GetBaseRepository<First>();
            // nested
            var result = await repository.GetSingleAsync(
                p => p.ClientId == 1,
                new Includes<First>
                {
                    p => p.Seconds.Select(pe => pe.Thirds.Select(peo => peo.Fourth)),
                },
                includeConditions: new[]
                {
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds,
                        e => e.FirstId,
                        e => e.Type == "One"),
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds.SelectMany(e => e.Thirds),
                        o => o.SecondId,
                        o => o.Type == "One"),
                });
            var seconds = result.Seconds.ToList();
            Assert.AreEqual(2, seconds.Count);
            Assert.IsTrue(seconds.All(e => e.Type == "One"));
            Assert.AreEqual(1, seconds[0].ClientId);
            Assert.AreEqual(1, seconds[0].Thirds.Count);
            Assert.IsNotNull(seconds[0].Thirds.First().Fourth);
            Assert.AreEqual(2, seconds[1].ClientId);
            Assert.AreEqual(1, seconds[1].Thirds.Count);
            Assert.IsNotNull(seconds[1].Thirds.First().Fourth);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task TestAsyncTwoIdenticalNestedConditions()
        {
            SeedData();
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var result = await repository.GetSingleAsync(
                p => p.ClientId == 1,
                new Includes<First>
                {
                    p => p.Seconds.Select(pe => pe.Thirds.Select(peo => peo.Fourth)),
                },
                includeConditions: new[]
                {
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds,
                        e => e.FirstId,
                        e => e.Type == "One"),
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds.SelectMany(e => e.Thirds),
                        o => o.SecondId,
                        o => o.Type == "One"),
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (First p) => p.Seconds.SelectMany(e => e.Thirds),
                        o => o.SecondId,
                        o => o.Type == "One"),
                });
            var seconds = result.Seconds.ToList();
            Assert.AreEqual(2, seconds.Count);
            Assert.IsTrue(seconds.All(e => e.Type == "One"));
            Assert.AreEqual(1, seconds[0].ClientId);
            Assert.AreEqual(1, seconds[0].Thirds.Count);
            Assert.IsNotNull(seconds[0].Thirds.First().Fourth);
            Assert.AreEqual(2, seconds[1].ClientId);
            Assert.AreEqual(1, seconds[1].Thirds.Count);
            Assert.IsNotNull(seconds[1].Thirds.First().Fourth);

            CheckContextRetrievals(1);
        }

        [TestMethod]
        public async Task TestAsyncNestedConditionsOnSecondAndFourthSteps()
        {
            SeedData();
            var repository = RepositoryFactory.GetBaseRepository<First>();

            var result = await repository.GetSingleAsync(
                p => p.ClientId == 1,
                new Includes<First>
                    {
                        p => p.Seconds.Select(pe => pe.Thirds.Select(peo => peo.Fourth.Fifths))
                    },
                includeConditions: new[]
                    {
                        EntityStaticHelpers.ConditionalIncludeCreator.Create(
                            (First p) => p.Seconds.SelectMany(e => e.Thirds).SelectMany(o => o.Fourth.Fifths),
                            rp => rp.FourthId,
                            rp => rp.Type.StartsWith("Satisfies")),
                        EntityStaticHelpers.ConditionalIncludeCreator.Create(
                            (First p) => p.Seconds.SelectMany(e => e.Thirds),
                            o => o.SecondId,
                            o => new[] {1,2,4}.Contains(o.ClientId)),
                    });
            var seconds = result.Seconds.ToList();
            Assert.AreEqual(3, seconds.Count);
            Assert.AreEqual(1, seconds[0].ClientId);
            Assert.AreEqual(2, seconds[0].Thirds.Count);
            var org = seconds[0].Thirds.ElementAt(0);
            Assert.AreEqual(1, org.ClientId);
            Assert.AreEqual(1, org.Fourth.Fifths.Count);
            Assert.AreEqual(1, org.Fourth.Fifths.First().ClientId);
            Assert.IsTrue(org.Fourth.Fifths.All(rp => rp.Type.StartsWith("Satisfies")));
            org = seconds[0].Thirds.ElementAt(1);
            Assert.AreEqual(2, org.ClientId);
            Assert.AreEqual(1, org.Fourth.Fifths.Count);
            Assert.AreEqual(4, org.Fourth.Fifths.First().ClientId);
            Assert.IsTrue(org.Fourth.Fifths.All(rp => rp.Type.StartsWith("Satisfies")));
            Assert.IsNotNull(seconds[0].Thirds.First().Fourth);

            Assert.AreEqual(2, seconds[1].ClientId);
            Assert.AreEqual(0, seconds[1].Thirds.Count);

            Assert.AreEqual(3, seconds[2].ClientId);
            Assert.AreEqual(1, seconds[2].Thirds.Count);
            org = seconds[2].Thirds.ElementAt(0);
            Assert.AreEqual(4, org.ClientId);
            Assert.AreEqual(1, org.Fourth.Fifths.Count);
            Assert.AreEqual(5, org.Fourth.Fifths.First().ClientId);
            Assert.IsTrue(org.Fourth.Fifths.All(rp => rp.Type.StartsWith("Satisfies")));

            result = repository.GetSingle(
               p => p.ClientId == 2,
               new Includes<First>
                                {
                                    p => p.Seconds.Select(pe => pe.Thirds.Select(peo => peo.Fourth.Fifths))
                                },
               includeConditions: new[]
                                {
                                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                                        (First p) => p.Seconds.SelectMany(e => e.Thirds).SelectMany(o => o.Fourth.Fifths),
                                        rp => rp.FourthId,
                                        rp => rp.Type.StartsWith("Satisfies")),
                                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                                        (First p) => p.Seconds.SelectMany(e => e.Thirds),
                                        o => o.SecondId,
                                        o => new[] {1,2,4}.Contains(o.ClientId)),
                                });
            Assert.IsNotNull(result);

            CheckContextRetrievals(2);
        }

        [TestMethod]
        public async Task TestAsyncConditionWithCompositeKeys()
        {
            var id1 = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var id2 = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var data = new[]
            {
                new CompositePk
                {
                    Part1 = id1.Item1,
                    Part2 = id1.Item2,
                    Data = "1",
                    CompositeFks = new List<CompositeFk>
                    {
                        new CompositeFk{Data = "1.1"},
                        new CompositeFk{Data = "1.2"},
                        new CompositeFk{Data = "1.3"},
                    }
                },
                new CompositePk
                {
                    Part1 = id2.Item1,
                    Part2 = id2.Item2,
                    Data = "2",
                    CompositeFks = new List<CompositeFk>
                    {
                        new CompositeFk{Data = "2.1"},
                        new CompositeFk{Data = "2.2"},
                        new CompositeFk{Data = "2.3"},
                    }
                },
            };
            Seed(data);

            var repo = RepositoryFactory.GetBaseRepository<CompositePk>();
            var res = await repo.GetAllAsync(
                new Includes<CompositePk>
                {
                    p => p.CompositeFks
                },
                includeConditions: new[]
                {
                    EntityStaticHelpers.ConditionalIncludeCreator.Create(
                        (CompositePk e) => e.CompositeFks,
                        ee => ee.Data.EndsWith("2"))
                });
            Assert.AreEqual(2, res.Count);
            var first = res.SingleOrDefault(f => f.Part1 == id1.Item1 && f.Part2 == id1.Item2);
            Assert.IsNotNull(first);
            Assert.IsNotNull(first.CompositeFks);
            Assert.AreEqual(1, first.CompositeFks.Count);
            Assert.AreEqual("1.2", first.CompositeFks.ElementAt(0).Data);
            var second = res.SingleOrDefault(f => f.Part1 == id2.Item1 && f.Part2 == id2.Item2);
            Assert.IsNotNull(second);
            Assert.IsNotNull(second.CompositeFks);
            Assert.AreEqual(1, second.CompositeFks.Count);
            Assert.AreEqual("2.2", second.CompositeFks.ElementAt(0).Data);

            CheckContextRetrievals(1);
        }
    }
}
