﻿namespace TsSoft.EntityRepository.Tests.BaseRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using BaseTestClasses.Database;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.EntityRepository.External;
    using TsSoft.Expressions.IncludeBuilder.Models;

    [TestClass]
    public class ExternalIncludeTests : RepositoryTestBase
    {
        [TestMethod]
        public void TestExternalIncludes()
        {
            var ext = Enumerable.Range(0, 10).Select(i => new TestExternalEntity {Id = Guid.NewGuid()}).ToArray();
            var data = new[]
            {
                new First { ExternalEntityId = ext[0].Id, Name = "1"},
                new First {ExternalEntityNullableId = ext[1].Id, ExternalEntityId = ext[0].Id, Name = "2"},
                new First {ExternalEntityNullableId = ext[2].Id, ExternalEntityId = ext[2].Id, Name = "4"},
            };

            Seed(data);
            var mockExt = new Mock<IExternalRepository<TestExternalEntity>>(MockBehavior.Strict);
            mockExt.Setup(e => e.Get(It.IsAny<IEnumerable<Guid>>()))
                .Returns((IEnumerable<Guid> ids) => ids.Select(id => ext.FirstOrDefault(e => e.Id == id)).Where(e => e != null));
            MockRepositoryFactory.Setup(r => r.GetExternalRepository<TestExternalEntity, Guid>())
                .Returns(mockExt.Object);
            var repo = RepositoryFactory.GetBaseRepository<First>();

            var result = repo.GetSingle(r => r.Name == "1", new Includes<First> {f => f.ExternalEntity, f => f.ExternalEntityNullable });
            Assert.AreSame(ext[0], result.ExternalEntity);
            Assert.IsNull(result.ExternalEntityNullable);
            mockExt.Verify(e => e.Get(It.IsAny<IEnumerable<Guid>>()), Times.Once);

            result = repo.GetSingle(r => r.Name == "2", new Includes<First> { f => f.ExternalEntity, f => f.ExternalEntityNullable });
            Assert.AreSame(ext[0], result.ExternalEntity);
            Assert.AreSame(ext[1], result.ExternalEntityNullable);
            mockExt.Verify(e => e.Get(It.IsAny<IEnumerable<Guid>>()), Times.Exactly(2));

            result = repo.GetSingle(r => r.Name == "4", new Includes<First> { f => f.ExternalEntity, f => f.ExternalEntityNullable });
            Assert.AreSame(ext[2], result.ExternalEntity);
            Assert.AreSame(ext[2], result.ExternalEntityNullable);
            mockExt.Verify(e => e.Get(It.IsAny<IEnumerable<Guid>>()), Times.Exactly(3));

            var many = repo.GetAll(new Includes<First> {f => f.ExternalEntity, f => f.ExternalEntityNullable}).ToDictionary(r => r.Name);
            Assert.AreSame(ext[0], many["1"].ExternalEntity);
            Assert.IsNull(many["1"].ExternalEntityNullable);
            Assert.AreSame(ext[0], many["2"].ExternalEntity);
            Assert.AreSame(ext[1], many["2"].ExternalEntityNullable);
            Assert.AreSame(ext[2], many["4"].ExternalEntity);
            Assert.AreSame(ext[2], many["4"].ExternalEntityNullable);
            mockExt.Verify(e => e.Get(It.IsAny<IEnumerable<Guid>>()), Times.Exactly(4));

            CheckContextRetrievals(4);
        }

        [TestMethod]
        public async Task TestAsyncExternalIncludes()
        {
            var ext = Enumerable.Range(0, 10).Select(i => new TestExternalEntity { Id = Guid.NewGuid() }).ToArray();
            var data = new[]
            {
                new First { ExternalEntityId = ext[0].Id, Name = "1"},
                new First {ExternalEntityNullableId = ext[1].Id, ExternalEntityId = ext[0].Id, Name = "2"},
                new First {ExternalEntityNullableId = ext[2].Id, ExternalEntityId = ext[2].Id, Name = "4"},
            };

            Seed(data);
            var mockExt = new Mock<IExternalRepository<TestExternalEntity>>(MockBehavior.Strict);
            mockExt.Setup(e => e.Get(It.IsAny<IEnumerable<Guid>>()))
                .Returns((IEnumerable<Guid> ids) => ids.Select(id => ext.FirstOrDefault(e => e.Id == id)).Where(e => e != null));
            MockRepositoryFactory.Setup(r => r.GetExternalRepository<TestExternalEntity, Guid>())
                .Returns(mockExt.Object);
            var repo = RepositoryFactory.GetBaseRepository<First>();

            var result = await repo.GetSingleAsync(r => r.Name == "1", new Includes<First> { f => f.ExternalEntity, f => f.ExternalEntityNullable });
            Assert.AreSame(ext[0], result.ExternalEntity);
            Assert.IsNull(result.ExternalEntityNullable);
            mockExt.Verify(e => e.Get(It.IsAny<IEnumerable<Guid>>()), Times.Once);

            result = await repo.GetSingleAsync(r => r.Name == "2", new Includes<First> { f => f.ExternalEntity, f => f.ExternalEntityNullable });
            Assert.AreSame(ext[0], result.ExternalEntity);
            Assert.AreSame(ext[1], result.ExternalEntityNullable);
            mockExt.Verify(e => e.Get(It.IsAny<IEnumerable<Guid>>()), Times.Exactly(2));

            result = await repo.GetSingleAsync(r => r.Name == "4", new Includes<First> { f => f.ExternalEntity, f => f.ExternalEntityNullable });
            Assert.AreSame(ext[2], result.ExternalEntity);
            Assert.AreSame(ext[2], result.ExternalEntityNullable);
            mockExt.Verify(e => e.Get(It.IsAny<IEnumerable<Guid>>()), Times.Exactly(3));

            var many = (await repo.GetAllAsync(new Includes<First> { f => f.ExternalEntity, f => f.ExternalEntityNullable })).ToDictionary(r => r.Name);
            Assert.AreSame(ext[0], many["1"].ExternalEntity);
            Assert.IsNull(many["1"].ExternalEntityNullable);
            Assert.AreSame(ext[0], many["2"].ExternalEntity);
            Assert.AreSame(ext[1], many["2"].ExternalEntityNullable);
            Assert.AreSame(ext[2], many["4"].ExternalEntity);
            Assert.AreSame(ext[2], many["4"].ExternalEntityNullable);
            mockExt.Verify(e => e.Get(It.IsAny<IEnumerable<Guid>>()), Times.Exactly(4));

            CheckContextRetrievals(4);
        }
    }
}
