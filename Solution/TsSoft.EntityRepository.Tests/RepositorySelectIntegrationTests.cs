﻿namespace TsSoft.EntityRepository.Tests
{
    using System.Linq;
    using BaseTestClasses.Database;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.SelectBuilder;
    using TsSoft.Expressions.SelectBuilder.Models;

    public static class SecondExtensions
    {
        public static int? Id(this Second second)
        {
            return second != null ? second.Id : (int?) null;
        }
    }

    [TestClass]
    public class RepositorySelectIntegrationTests : RepositoryTestBase
    {
        [TestMethod]
        public void TestSelectWithFirst()
        {
            var data = new[]
            {
                new First {Name = "1", Seconds = new[] {new Second {Type = "1.1", Bool = true}}},
                new First {Name = "2", Seconds = new[] {new Second {Type = "2.1", Bool = false}, new Second {Type = "2.2", Bool = true}, new Second {Type = "2.3", Bool = true}}},
                new First {Name = "3"}
            };
            Seed(data);
            var mapper = AbstractMapperFactory.Create<SelectWithFirstMapper>(
                MapperTypes.Are(typeof(InnerMapper)), Entities.Are(typeof(First), typeof(Second)));
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var entities = repository.GetAll(mapper.Select);
            Assert.AreEqual(1, entities.Single(e => e.Id == data[0].Id).Seconds.Count);
            Assert.AreEqual(2, entities.Single(e => e.Id == data[1].Id).Seconds.Count);
            Assert.AreEqual(0, entities.Single(e => e.Id == data[2].Id).Seconds.Count);
            var mapped = entities.Select(mapper.Map).ToList();
            Assert.AreEqual(entities.ElementAt(0).Id, mapped[0].Id);
            Assert.AreEqual(entities.ElementAt(1).Id, mapped[1].Id);
            Assert.AreEqual(entities.ElementAt(2).Id, mapped[2].Id);
            Assert.AreEqual(entities.ElementAt(0).Seconds.FirstOrDefault(s => s.Bool).Id(), mapped[0].SecondId);
            Assert.AreEqual(entities.ElementAt(1).Seconds.FirstOrDefault(s => s.Bool).Id(), mapped[1].SecondId);
            Assert.AreEqual(entities.ElementAt(2).Seconds.FirstOrDefault(s => s.Bool).Id(), mapped[2].SecondId);
            Assert.AreEqual(1, mapped.Count(m => m.SecondId == null));
        }

        [TestMethod]
        public void TestSelectWithFirstWithoutParameters()
        {
            var data = new[]
            {
                new First {Name = "1", Seconds = new[] {new Second {Type = "1.1", Bool = true}}},
                new First {Name = "2", Seconds = new[] {new Second {Type = "2.1", Bool = true}, new Second {Type = "2.2", Bool = false}, new Second {Type = "2.3", Bool = true}}},
                new First {Name = "3"}
            };
            Seed(data);
            var mapper = AbstractMapperFactory.Create<SelectWithFirstAndNoParamsMapper>(
                MapperTypes.Are(typeof(InnerMapper)), Entities.Are(typeof(First), typeof(Second)));
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var entities = repository.GetAll(mapper.Select);
            Assert.AreEqual(1, entities.Single(e => e.Id == data[0].Id).Seconds.Count);
            Assert.AreEqual(3, entities.Single(e => e.Id == data[1].Id).Seconds.Count);
            Assert.AreEqual(0, entities.Single(e => e.Id == data[2].Id).Seconds.Count);
            var mapped = entities.Select(mapper.Map).ToList();
            Assert.AreEqual(entities.ElementAt(0).Id, mapped[0].Id);
            Assert.AreEqual(entities.ElementAt(1).Id, mapped[1].Id);
            Assert.AreEqual(entities.ElementAt(2).Id, mapped[2].Id);
            Assert.AreEqual(entities.ElementAt(0).Seconds.FirstOrDefault().Id(), mapped[0].SecondId);
            Assert.AreEqual(entities.ElementAt(1).Seconds.FirstOrDefault().Id(), mapped[1].SecondId);
            Assert.AreEqual(entities.ElementAt(2).Seconds.FirstOrDefault().Id(), mapped[2].SecondId);
            Assert.AreEqual(1, mapped.Count(m => m.SecondId == null));
        }

        [TestMethod]
        public void TestSelectFromInterface()
        {
            var data = new[]
            {
                new First {Name = "1", Seconds = new[] {new Second {Type = "1.1", Bool = true}}},
                new First {Name = "2", Seconds = new[] {new Second {Type = "2.1", Bool = true}, new Second {Type = "2.2", Bool = false}, new Second {Type = "2.3", Bool = true}}},
                new First {Name = "3", NullableBool = true}
            };
            Seed(data);
            var selectHelper = AbstractMapperDependenciesFactory.GetDependency<ISelectHelper>(
                MockObjects.None, Entities.Are(EntityStaticHelpers.EntityTypesRetriever.GetEntityTypes().ToList()),
                External.None);
            var select = selectHelper.CreateFromInterface<First, IFirst>();
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var entities = repository.GetAll(select).Cast<IFirst>().OrderBy(f => f.Name).ToList();
            Assert.AreEqual(3, entities.Count);
            Assert.AreEqual(1, entities[0].SecondsWrapper.Count());
            Assert.IsTrue(entities[0].SecondsWrapper.Any(w => w.Type == "1.1"));
            Assert.IsFalse(entities[0].NullableBoolIsTrue);
            Assert.AreEqual(3, entities[1].SecondsWrapper.Count());
            Assert.IsTrue(entities[1].SecondsWrapper.Any(w => w.Type == "2.1"));
            Assert.IsTrue(entities[1].SecondsWrapper.Any(w => w.Type == "2.2"));
            Assert.IsTrue(entities[1].SecondsWrapper.Any(w => w.Type == "2.3"));
            Assert.IsFalse(entities[1].NullableBoolIsTrue);
            Assert.AreEqual(0, entities[2].SecondsWrapper.Count());
            Assert.IsTrue(entities[2].NullableBoolIsTrue);
        }

        [TestMethod]
        public void TestSelectWithNavigationWrapper()
        {
            var data = new[]
            {
                new First {Name = "1", Seconds = new[] {new Second {Type = "1.1", Bool = true}}},
                new First {Name = "2", Seconds = new[] {new Second {Type = "2.1", Bool = true}, new Second {Type = "2.2", Bool = false}, new Second {Type = "2.3", Bool = true}}},
                new First {Name = "3", NullableBool = true}
            };
            Seed(data);
            var mapper = AbstractMapperFactory.Create<SelectWithNavigationWrapperConditionMapper>(
                MapperTypes.Are(typeof(InnerMapper)), Entities.Are(typeof(First), typeof(Second)));
            var repository = RepositoryFactory.GetBaseRepository<First>();
            var entities = repository.GetAll(mapper.Select);
            Assert.AreEqual(0, entities.Single(e => e.Id == data[0].Id).Seconds.Count);
            Assert.AreEqual(1, entities.Single(e => e.Id == data[1].Id).Seconds.Count);
            Assert.AreEqual(0, entities.Single(e => e.Id == data[2].Id).Seconds.Count);
            var mapped = entities.Select(mapper.Map).ToList();
            Assert.AreEqual(entities.ElementAt(0).Id, mapped[0].Id);
            Assert.AreEqual(entities.ElementAt(1).Id, mapped[1].Id);
            Assert.AreEqual(entities.ElementAt(2).Id, mapped[2].Id);
            Assert.AreEqual(entities.ElementAt(0).Seconds.FirstOrDefault(x => x.Type == "2.1").Id() ?? 0, mapped[0].SecondId);
            Assert.AreEqual(entities.ElementAt(1).Seconds.FirstOrDefault(x => x.Type == "2.1").Id() ?? 0, mapped[1].SecondId);
            Assert.AreEqual(entities.ElementAt(2).Seconds.FirstOrDefault(x => x.Type == "2.1").Id() ?? 0, mapped[2].SecondId);
            Assert.AreEqual(2, mapped.Count(m => m.SecondId == 0));
        }


        public class FirstIntegrationModel
        {
            public int Id { get; set; }

            public int? SecondId { get; set; }
        }

        public class SelectWithFirstMapper : SelectProviderMapper<First, FirstIntegrationModel>
        {
            public SelectWithFirstMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<FirstIntegrationModel, First> MapRules
            {
                get
                {
                    return new MapRules<FirstIntegrationModel, First>
                    {
                        {f => f.SecondId, f => f.Seconds.FirstOrDefault(s => s.Bool)}
                    };
                }
            }
        }

        public class SelectWithFirstAndNoParamsMapper : SelectProviderMapper<First, FirstIntegrationModel>
        {
            public SelectWithFirstAndNoParamsMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<FirstIntegrationModel, First> MapRules
            {
                get
                {
                    return new MapRules<FirstIntegrationModel, First>
                    {
                        {f => f.SecondId, f => f.Seconds.FirstOrDefault()}
                    };
                }
            }
        }
        public class InnerMapper : ISelectProviderMapper<Second, int?>
        {
            public int? Map(Second @from)
            {
                return from != null ? from.Id : (int?)null;
            }

            public IReadOnlySelectExpression<Second> Select { get {return new SelectExpression<Second> {Select = s => new {s.Id}};} }
        }

        public class SelectWithNavigationWrapperConditionMapper : SelectProviderMapper<First, FirstIntegrationModel>
        {
            public SelectWithNavigationWrapperConditionMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<FirstIntegrationModel, First> MapRules
            {
                get
                {
                    return new MapRules<FirstIntegrationModel, First>
                    {
                        {m => m.SecondId, e => e.SecondsWrapper.FirstOrDefault(w => w.Type == "2.1").Id}
                    };
                }
            }
        }
    }
}
