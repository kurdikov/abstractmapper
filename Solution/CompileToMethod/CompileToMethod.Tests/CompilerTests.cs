// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class CompilerTests
    {
        [Fact]
        // "Takes over a minute to complete"
        public static void CompileDeepTree_NoStackOverflow()
        {
            var e = (Expression)Expression.Constant(0);

            int n = 10000;

            for (var i = 0; i < n; i++)
                e = Expression.Add(e, Expression.Constant(1));

            Func<int> f = Expression.Lambda<Func<int>>(e).CompileToTestMethodSimple();

            Assert.Equal(n, f());
        }

#if NETCOREAPP2_0
        [Fact]
        // "May fail with SO on Debug JIT"
        public static void CompileDeepTree_NoStackOverflowFast()
        {
            Expression e = Expression.Constant(0);

            int n = 100;

            for (int i = 0; i < n; i++)
                e = Expression.Add(e, Expression.Constant(1));

            Func<int> f = null;
            // Request a stack size of 128KiB to get very small stack.
            // This reduces the size of tree needed to risk a stack overflow.
            // This though will only risk overflow once, so the outerloop test
            // above is still needed.
            Thread t = new Thread(() => f = Expression.Lambda<Func<int>>(e).CompileToTestMethod(), 128 * 1024);
            t.Start();
            t.Join();

            Assert.Equal(n, f());
        }
#endif

        [Fact]
        public static void EmitConstantsToIL_NonNullableValueTypes()
        {
            VerifyEmitConstantsToIL((bool)true);

            VerifyEmitConstantsToIL((char)'a');

            VerifyEmitConstantsToIL((sbyte)42);
            VerifyEmitConstantsToIL((byte)42);
            VerifyEmitConstantsToIL((short)42);
            VerifyEmitConstantsToIL((ushort)42);
            VerifyEmitConstantsToIL((int)42);
            VerifyEmitConstantsToIL((uint)42);
            VerifyEmitConstantsToIL((long)42);
            VerifyEmitConstantsToIL((ulong)42);

            VerifyEmitConstantsToIL((float)3.14);
            VerifyEmitConstantsToIL((double)3.14);
            VerifyEmitConstantsToIL((decimal)49.95m);
        }

        [Fact]
        public static void EmitConstantsToIL_NullableValueTypes()
        {
            VerifyEmitConstantsToIL((bool?)null);
            VerifyEmitConstantsToIL((bool?)true);

            VerifyEmitConstantsToIL((char?)null);
            VerifyEmitConstantsToIL((char?)'a');

            VerifyEmitConstantsToIL((sbyte?)null);
            VerifyEmitConstantsToIL((sbyte?)42);
            VerifyEmitConstantsToIL((byte?)null);
            VerifyEmitConstantsToIL((byte?)42);
            VerifyEmitConstantsToIL((short?)null);
            VerifyEmitConstantsToIL((short?)42);
            VerifyEmitConstantsToIL((ushort?)null);
            VerifyEmitConstantsToIL((ushort?)42);
            VerifyEmitConstantsToIL((int?)null);
            VerifyEmitConstantsToIL((int?)42);
            VerifyEmitConstantsToIL((uint?)null);
            VerifyEmitConstantsToIL((uint?)42);
            VerifyEmitConstantsToIL((long?)null);
            VerifyEmitConstantsToIL((long?)42);
            VerifyEmitConstantsToIL((ulong?)null);
            VerifyEmitConstantsToIL((ulong?)42);

            VerifyEmitConstantsToIL((float?)null);
            VerifyEmitConstantsToIL((float?)3.14);
            VerifyEmitConstantsToIL((double?)null);
            VerifyEmitConstantsToIL((double?)3.14);
            VerifyEmitConstantsToIL((decimal?)null);
            VerifyEmitConstantsToIL((decimal?)49.95m);

            VerifyEmitConstantsToIL((DateTime?)null);
        }

        [Fact]
        public static void EmitConstantsToIL_ReferenceTypes()
        {
            VerifyEmitConstantsToIL((string)null);
            VerifyEmitConstantsToIL((string)"bar");
        }

        [Fact]
        public static void EmitConstantsToIL_Enums()
        {
            VerifyEmitConstantsToIL(ConstantsEnum.A);
            VerifyEmitConstantsToIL((ConstantsEnum?)null);
            VerifyEmitConstantsToIL((ConstantsEnum?)ConstantsEnum.A);
        }

        [Fact]
        public static void EmitConstantsToIL_ShareReferences()
        {
            var o = new object();
            VerifyEmitConstantsToIL(Expression.Equal(Expression.Constant(o), Expression.Constant(o)), 1, true);
        }

        [Fact]
        public static void EmitConstantsToIL_LiftedToClosure()
        {
            VerifyEmitConstantsToIL(DateTime.Now, 1);
            VerifyEmitConstantsToIL((DateTime?)DateTime.Now, 1);
        }

        [Fact]
        public static void VariableBinder_CatchBlock_Filter1()
        {
            // See https://github.com/dotnet/corefx/issues/11994 for reported issue

            Verify_VariableBinder_CatchBlock_Filter(
                Expression.Catch(
                    Expression.Parameter(typeof(Exception), "ex"),
                    Expression.Empty(),
                    Expression.Parameter(typeof(bool), "???")
                )
            );
        }

        [Fact]
        public static void VariableBinder_CatchBlock_Filter2()
        {
            // See https://github.com/dotnet/corefx/issues/11994 for reported issue

            Verify_VariableBinder_CatchBlock_Filter(
                Expression.Catch(
                    typeof(Exception),
                    Expression.Empty(),
                    Expression.Parameter(typeof(bool), "???")
                )
            );
        }

        [Fact]
        public static void VerifyIL_Simple()
        {
            Expression<Func<int>> f = () => Math.Abs(42);

            f.VerifyIL(
                @".method int32 class [CompiledExpressionsVerifyIL_Simple]VerifyIL_Simple::M()
                  {
                    .maxstack 1

                    IL_0000: ldc.i4.s   42
                    IL_0002: call       int32 class [System.Private.CoreLib]System.Math::Abs(int32)
                    IL_0007: ret
                  }", "VerifyIL_Simple");
        }

        [Fact]
        public static void VerifyIL_Exceptions()
        {
            ParameterExpression x = Expression.Parameter(typeof(int), "x");
            Expression<Func<int, int>> f =
                Expression.Lambda<Func<int, int>>(
                    Expression.TryCatchFinally(
                        Expression.Call(
                            typeof(Math).GetMethod(nameof(Math.Abs), new[] { typeof(int) }),
                            Expression.Divide(
                                Expression.Constant(42),
                                x
                            )
                        ),
                        Expression.Empty(),
                        Expression.Catch(
                            typeof(DivideByZeroException),
                            Expression.Constant(-1)
                        )
                    ),
                    x
                );

            f.VerifyIL(
                @".method int32 class [CompiledExpressionsVerifyIL_Exceptions]VerifyIL_Exceptions::M(int32)
                  {
                    .maxstack 4
                    .locals init (
                      [0] int32
                    )

                    .try
                    {
                      .try
                      {
                        IL_0000: ldc.i4.s   42
                        IL_0002: ldarg.0
                        IL_0003: div
                        IL_0004: call       int32 class [System.Private.CoreLib]System.Math::Abs(int32)
                        IL_0009: stloc.0
                        IL_000a: leave      IL_0017
                      }
                      catch (class [System.Private.CoreLib]System.DivideByZeroException)
                      {
                        IL_000f: pop
                        IL_0010: ldc.i4.m1
                        IL_0011: stloc.0
                        IL_0012: leave      IL_0017
                      }
                      IL_0017: leave      IL_001d
                    }
                    finally
                    {
                      IL_001c: endfinally
                    }
                    IL_001d: ldloc.0
                    IL_001e: ret
                  }", "VerifyIL_Exceptions");
        }

        [Fact]
        public static void VerifyIL_Closure1()
        {
            Expression<Func<Func<int>>> f = () => () => 42;

            f.VerifyIL(
                @".method class [System.Private.CoreLib]System.Func`1<int32> class [CompiledExpressionsVerifyIL_Closure1]VerifyIL_Closure1::M()
                  {
                      .maxstack 3
                  
                      IL_0000: ldnull     
                      IL_0001: ldftn      int32 class [CompiledExpressionsVerifyIL_Closure1]VerifyIL_Closure1::lambda_method()
                      IL_0007: newobj     instance void class [System.Private.CoreLib]System.Func`1<int32>::.ctor(object,native int)
                      IL_000c: ret        
                  }
                  
                  .method int32 class [CompiledExpressionsVerifyIL_Closure1]VerifyIL_Closure1::lambda_method()
                  {
                      .maxstack 1
                  
                      IL_0000: ldc.i4.s   42
                      IL_0002: ret        
                  }
                ", "VerifyIL_Closure1", true);
        }

        [Fact]
        public static void VerifyIL_Closure2()
        {
            Expression<Func<int, Func<int>>> f = x => () => x;

            f.VerifyIL(
                @".method class [System.Private.CoreLib]System.Func`1<int32> class [CompiledExpressionsVerifyIL_Closure2]VerifyIL_Closure2::M(int32)
                  {
                    .maxstack 5
                    .locals init (
                      [0] object[]
                    )

                    IL_0000: ldc.i4.1
                    IL_0001: newarr     object
                    IL_0006: dup
                    IL_0007: ldc.i4.0
                    IL_0008: ldarg.0
                    IL_0009: newobj     instance void class [" + Assemblies.StrongBoxAssembly + @"]System.Runtime.CompilerServices.StrongBox`1<int32>::.ctor(int32)
                    IL_000e: stelem.ref
                    IL_000f: stloc.0
                    IL_0010: ldloc.0    
                    IL_0011: newobj     instance void class [CompileToMethod]System.Runtime.CompilerServices.CTM_Closure::.ctor(object[])
                    IL_0016: ldftn      int32 class [CompiledExpressionsVerifyIL_Closure2]VerifyIL_Closure2::lambda_method(class [CompileToMethod]System.Runtime.CompilerServices.CTM_Closure)
                    IL_001c: newobj     instance void class [System.Private.CoreLib]System.Func`1<int32>::.ctor(object,native int)
                    IL_0021: ret        
                  }

                  .method int32 class [CompiledExpressionsVerifyIL_Closure2]VerifyIL_Closure2::lambda_method(class [CompileToMethod]System.Runtime.CompilerServices.CTM_Closure)
                  {
                    .maxstack 2
                    .locals init (
                      [0] object[]
                    )

                    IL_0000: ldarg.0    
                    IL_0001: ldfld      class [CompileToMethod]System.Runtime.CompilerServices.CTM_Closure::Locals
                    IL_0006: stloc.0    
                    IL_0007: ldloc.0    
                    IL_0008: ldc.i4.0   
                    IL_0009: ldelem.ref 
                    IL_000a: castclass  class [" + Assemblies.StrongBoxAssembly + @"]System.Runtime.CompilerServices.StrongBox`1<int32>
                    IL_000f: ldfld      class [" + Assemblies.StrongBoxAssembly + @"]System.Runtime.CompilerServices.StrongBox`1<int32>::Value
                    IL_0014: ret        
                  }", "VerifyIL_Closure2", true);
        }

        [Fact]
        public static void VerifyIL_Closure3()
        {
            // Using an unchecked addition to ensure that an add instruction is emitted (and not add.ovf)
            Expression<Func<int, Func<int, int>>> f = x => y => unchecked(x + y);

            f.VerifyIL(
                @".method class [System.Private.CoreLib]System.Func`2<int32,int32> class [CompiledExpressionsVerifyIL_Closure3]VerifyIL_Closure3::M(int32)
                  {
                    .maxstack 5
                    .locals init (
                      [0] object[]
                    )

                    IL_0000: ldc.i4.1   
                    IL_0001: newarr     object
                    IL_0006: dup        
                    IL_0007: ldc.i4.0   
                    IL_0008: ldarg.0    
                    IL_0009: newobj     instance void class [" + Assemblies.StrongBoxAssembly + @"]System.Runtime.CompilerServices.StrongBox`1<int32>::.ctor(int32)
                    IL_000e: stelem.ref 
                    IL_000f: stloc.0    
                    IL_0010: ldloc.0    
                    IL_0011: newobj     instance void class [CompileToMethod]System.Runtime.CompilerServices.CTM_Closure::.ctor(object[])
                    IL_0016: ldftn      int32 class [CompiledExpressionsVerifyIL_Closure3]VerifyIL_Closure3::lambda_method(class [CompileToMethod]System.Runtime.CompilerServices.CTM_Closure,int32)
                    IL_001c: newobj     instance void class [System.Private.CoreLib]System.Func`2<int32,int32>::.ctor(object,native int)
                    IL_0021: ret        
                  }

                  .method int32 class [CompiledExpressionsVerifyIL_Closure3]VerifyIL_Closure3::lambda_method(class [CompileToMethod]System.Runtime.CompilerServices.CTM_Closure,int32)
                  {
                    .maxstack 2
                    .locals init (
                      [0] object[]
                    )

                    IL_0000: ldarg.0    
                    IL_0001: ldfld      class [CompileToMethod]System.Runtime.CompilerServices.CTM_Closure::Locals
                    IL_0006: stloc.0    
                    IL_0007: ldloc.0    
                    IL_0008: ldc.i4.0   
                    IL_0009: ldelem.ref 
                    IL_000a: castclass  class [" + Assemblies.StrongBoxAssembly + @"]System.Runtime.CompilerServices.StrongBox`1<int32>
                    IL_000f: ldfld      class [" + Assemblies.StrongBoxAssembly + @"]System.Runtime.CompilerServices.StrongBox`1<int32>::Value
                    IL_0014: ldarg.1    
                    IL_0015: add        
                    IL_0016: ret        
                  }", "VerifyIL_Closure3", true);
        }

        public static void VerifyIL(this LambdaExpression expression, string expected, string className, bool appendInnerLambdas = false)
        {
            string actual = expression.GetIL(className, appendInnerLambdas);

            string nExpected = Normalize(expected);
            string nActual = Normalize(actual);

            Assert.Equal(nExpected, nActual);
        }

        private static string Normalize(string s)
        {
            Collections.Generic.IEnumerable<string> lines =
                s
                .Replace("\r\n", "\n")
                .Split(new[] { '\n' })
                .Select(line => line.Trim())
                .Select(line => Regex.Replace(line, @"<ExpressionCompilerImplementationDetails>\{\d+\}", ""))
                .Where(line => line != "" && !line.StartsWith("//"));

            return string.Join("\n", lines);
        }

        private static void VerifyEmitConstantsToIL<T>(T value)
        {
            VerifyEmitConstantsToIL<T>(value, 0);
        }

        private static void VerifyEmitConstantsToIL<T>(T value, int expectedCount)
        {
            VerifyEmitConstantsToIL(Expression.Constant(value, typeof(T)), expectedCount, value);
        }

        private static void VerifyEmitConstantsToIL(Expression e, int expectedCount, object expectedValue)
        {
            Delegate f = Expression.Lambda(e).CompileToTestMethod();

            object o = f.DynamicInvoke();
            Assert.Equal(expectedValue, o);
        }

        private static void Verify_VariableBinder_CatchBlock_Filter(CatchBlock @catch)
        {
            Expression<Action> e =
                Expression.Lambda<Action>(
                    Expression.TryCatch(
                        Expression.Empty(),
                        @catch
                    )
                );

            Assert.Throws<InvalidOperationException>(() => e.CompileToTestMethod());
        }
    }

    public enum ConstantsEnum
    {
        A
    }
}
