// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class UnaryDecrementNullableTests : IncrementDecrementTests
    {
        public struct DecrementableWhenNullable
        {
            public DecrementableWhenNullable(int value)
            {
                Value = value;
            }

            public int Value { get; }

            public static DecrementableWhenNullable? operator --(DecrementableWhenNullable? operand)
            {
                if (operand.HasValue)
                {
                    int dec = unchecked(operand.GetValueOrDefault().Value - 1);
                    if (dec == 0)
                    {
                        return null;
                    }

                    return new DecrementableWhenNullable(dec);
                }

                return new DecrementableWhenNullable(-1);
            }
        }

        private static IEnumerable<object[]> DecrementableWhenNullableValues()
        {
            yield return new object[] { new DecrementableWhenNullable(0), new DecrementableWhenNullable(-1) };
            yield return new object[] { new DecrementableWhenNullable(1), null };
            yield return new object[] { new DecrementableWhenNullable(int.MaxValue), new DecrementableWhenNullable(int.MaxValue - 1) };
            yield return new object[] { new DecrementableWhenNullable(int.MinValue), new DecrementableWhenNullable(int.MaxValue) };
            yield return new object[] { null, new DecrementableWhenNullable(-1) };
        }

        #region Test methods

        [Fact]
        public static void CheckUnaryDecrementNullableShortTest()
        {
            short?[] values = new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                VerifyDecrementNullableShort(values[i]);
            }
        }

        [Fact]
        public static void CheckUnaryDecrementNullableUShortTest()
        {
            ushort?[] values = new ushort?[] { null, 0, 1, ushort.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                VerifyDecrementNullableUShort(values[i]);
            }
        }

        [Fact]
        public static void CheckUnaryDecrementNullableIntTest()
        {
            int?[] values = new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                VerifyDecrementNullableInt(values[i]);
            }
        }

        [Fact]
        public static void CheckUnaryDecrementNullableUIntTest()
        {
            uint?[] values = new uint?[] { null, 0, 1, uint.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                VerifyDecrementNullableUInt(values[i]);
            }
        }

        [Fact]
        public static void CheckUnaryDecrementNullableLongTest()
        {
            long?[] values = new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                VerifyDecrementNullableLong(values[i]);
            }
        }

        [Fact]
        public static void CheckUnaryDecrementNullableULongTest()
        {
            ulong?[] values = new ulong?[] { null, 0, 1, ulong.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                VerifyDecrementNullableULong(values[i]);
            }
        }

        [Fact]
        public static void CheckDecrementFloatTest()
        {
            float?[] values = new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN };
            for (int i = 0; i < values.Length; i++)
            {
                VerifyDecrementNullableFloat(values[i]);
            }
        }

        [Fact]
        public static void CheckDecrementDoubleTest()
        {
            double?[] values = new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN };
            for (int i = 0; i < values.Length; i++)
            {
                VerifyDecrementNullableDouble(values[i]);
            }
        }

        [Theory, MemberData(nameof(NonArithmeticObjects), false)]
        public static void DecrementNonArithmetic(object value)
        {
            Expression ex = Expression.Constant(value, typeof(Nullable<>).MakeGenericType(value.GetType()));
            Assert.Throws<InvalidOperationException>(() => Expression.Decrement(ex));
        }

        [Theory, MemberData(nameof(DecrementableValues), true)]
        public static void CustomOpDecrement(Decrementable? operand, Decrementable? expected)
        {
            Func<Decrementable?> func = Expression.Lambda<Func<Decrementable?>>(
                Expression.Decrement(Expression.Constant(operand, typeof(Decrementable?)))).CompileToTestMethod();
            Assert.Equal(expected, func());
        }

        [Theory, MemberData(nameof(DecrementableWhenNullableValues))]
        public static void NonLiftedNullableOpDecrement(
            DecrementableWhenNullable? operand, DecrementableWhenNullable? expected)
        {
            Func<DecrementableWhenNullable?> func = Expression.Lambda<Func<DecrementableWhenNullable?>>(
                Expression.Decrement(Expression.Constant(operand, typeof(DecrementableWhenNullable?)))).CompileToTestMethod();
            Assert.Equal(expected, func());
        }

        [Theory, MemberData(nameof(DoublyDecrementedDecrementableValues), true)]
        public static void UserDefinedOpDecrement(Decrementable? operand, Decrementable? expected)
        {
            MethodInfo method = typeof(IncrementDecrementTests).GetMethod(nameof(DoublyDecrement));
            Func<Decrementable?> func = Expression.Lambda<Func<Decrementable?>>(
                Expression.Decrement(Expression.Constant(operand, typeof(Decrementable?)), method)).CompileToTestMethod();
            Assert.Equal(expected, func());
        }

        [Theory, MemberData(nameof(DoublyDecrementedInt32s), true)]
        public static void UserDefinedOpDecrementArithmeticType(int? operand, int? expected)
        {
            MethodInfo method = typeof(IncrementDecrementTests).GetMethod(nameof(DoublyDecrementInt32));
            Func<int?> func = Expression.Lambda<Func<int?>>(
                Expression.Decrement(Expression.Constant(operand, typeof(int?)), method)).CompileToTestMethod();
            Assert.Equal(expected, func());
        }

        #endregion

        #region Test verifiers

        private static void VerifyDecrementNullableShort(short? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Decrement(Expression.Constant(value, typeof(short?))),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();
            Assert.Equal(unchecked((short?)(--value)), f());
        }

        private static void VerifyDecrementNullableUShort(ushort? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Decrement(Expression.Constant(value, typeof(ushort?))),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();
            Assert.Equal(unchecked((ushort?)(--value)), f());
        }

        private static void VerifyDecrementNullableInt(int? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Decrement(Expression.Constant(value, typeof(int?))),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();
            Assert.Equal(unchecked((int?)(--value)), f());
        }

        private static void VerifyDecrementNullableUInt(uint? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Decrement(Expression.Constant(value, typeof(uint?))),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();
            Assert.Equal(unchecked((uint?)(--value)), f());
        }

        private static void VerifyDecrementNullableLong(long? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Decrement(Expression.Constant(value, typeof(long?))),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();
            Assert.Equal(unchecked((long?)(--value)), f());
        }

        private static void VerifyDecrementNullableULong(ulong? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Decrement(Expression.Constant(value, typeof(ulong?))),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();
            Assert.Equal(unchecked((ulong?)(--value)), f());
        }

        private static void VerifyDecrementNullableFloat(float? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Decrement(Expression.Constant(value, typeof(float?))),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();
            Assert.Equal((float?)(--value), f());
        }

        private static void VerifyDecrementNullableDouble(double? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Decrement(Expression.Constant(value, typeof(double?))),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();
            Assert.Equal((double?)(--value), f());
        }

        #endregion
    }
}
