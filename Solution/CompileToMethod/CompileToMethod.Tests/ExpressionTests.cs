// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    // Tests for features of the Expression class, rather than any derived types,
    // including how it acts with custom derived types.
    //
    // Unfortunately there is slightly different internal behaviour depending on whether
    // a derived Expression uses the new constructor, uses the old constructor, or uses
    // the new constructor after at least one use of the old constructor has been made,
    // due to static state being affected. For this reason some tests have to be done
    // in a particular order, with those for the old constructor coming after most of
    // the tests, and those affected by this being repeated after that.
    [TestCaseOrderer("System.Linq.Expressions.Tests.TestOrderer", "System.Linq.Expressions.Tests")]
    public class ExpressionTests
    {

        private class ReducesFromStrangeNodeType : Expression
        {
            public override Type Type => typeof(int);

            public override ExpressionType NodeType => (ExpressionType)(-1);

            public override bool CanReduce => true;

            public override Expression Reduce() => Constant(3);
        }

        private class IrreducibleWithTypeAndNodeType : Expression
        {
            public override Type Type => typeof(void);

            public override ExpressionType NodeType => ExpressionType.Extension;
        }

        private class IrreducibleWithTypeAndStrangeNodeType : Expression
        {
            public override Type Type => typeof(void);

            public override ExpressionType NodeType => (ExpressionType)(-1);
        }


        [Fact]
        public void CompileIrreducibleExtension()
        {
            Expression<Action> exp = Expression.Lambda<Action>(new IrreducibleWithTypeAndNodeType());
            AssertExtensions.Throws<ArgumentException>(null, () => exp.CompileToTestMethod());
        }

        [Fact]
        public void CompileIrreducibleStrangeNodeTypeExtension()
        {
            Expression<Action> exp = Expression.Lambda<Action>(new IrreducibleWithTypeAndStrangeNodeType());
            AssertExtensions.Throws<ArgumentException>(null, () => exp.CompileToTestMethod());
        }

        [Fact]
        public void CompileReducibleStrangeNodeTypeExtension()
        {
            Expression<Func<int>> exp = Expression.Lambda<Func<int>>(new ReducesFromStrangeNodeType());
            Assert.Equal(3, exp.CompileToTestMethod()());
        }
    }
}
