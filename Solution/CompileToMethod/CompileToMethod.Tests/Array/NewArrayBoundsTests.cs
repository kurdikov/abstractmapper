// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class ArrayBoundsTests
    {
        private const int MaxArraySize = 0X7FEFFFFF;

        public static IEnumerable<object> Bounds_TestData()
        {
            yield return new byte[] { 0, 1, byte.MaxValue };
            yield return new int[] { 0, 1, -1, int.MinValue, int.MaxValue };
            yield return new long[] { 0, 1, -1, long.MinValue, long.MaxValue };
            yield return new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue };
            yield return new short[] { 0, 1, -1, short.MinValue, short.MaxValue };
            yield return new uint[] { 0, 1, uint.MaxValue };
            yield return new ulong[] { 0, 1, ulong.MaxValue };
            yield return new ushort[] { 0, 1, ushort.MaxValue };

            yield return new byte?[] { null, 0, 1, byte.MaxValue };
            yield return new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue };
            yield return new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue };
            yield return new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue };
            yield return new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue };
            yield return new uint?[] { null, 0, 1, uint.MaxValue };
            yield return new ulong?[] { null, 0, 1, ulong.MaxValue };
            yield return new ushort?[] { null, 0, 1, ushort.MaxValue };
        }

        public static IEnumerable<object[]> TestData()
        {
            foreach (Array sizes in Bounds_TestData())
            {
                Type sizeType = sizes.GetType().GetElementType();
                foreach (object size in sizes)
                {
                    // ValueType
                    yield return new object[4] { typeof(bool), size, sizeType, false };
                    yield return new object[4] { typeof(byte), size, sizeType, (byte)0 };
                    yield return new object[4] { typeof(char), size, sizeType, (char)0 };
                    yield return new object[4] { typeof(decimal), size, sizeType, (decimal)0 };
                    yield return new object[4] { typeof(double), size, sizeType, (double)0 };
                    yield return new object[4] { typeof(float), size, sizeType, (float)0 };
                    yield return new object[4] { typeof(int), size, sizeType, 0 };
                    yield return new object[4] { typeof(long), size, sizeType, (long)0 };
                    yield return new object[4] { typeof(S), size, sizeType, new S() };
                    yield return new object[4] { typeof(sbyte), size, sizeType, (sbyte)0 };
                    yield return new object[4] { typeof(Sc), size, sizeType, new Sc() };
                    yield return new object[4] { typeof(Scs), size, sizeType, new Scs() };
                    yield return new object[4] { typeof(short), size, sizeType, (short)0 };
                    yield return new object[4] { typeof(Sp), size, sizeType, new Sp() };
                    yield return new object[4] { typeof(Ss), size, sizeType, new Ss() };
                    yield return new object[4] { typeof(uint), size, sizeType, (uint)0 };
                    yield return new object[4] { typeof(ulong), size, sizeType, (ulong)0 };
                    yield return new object[4] { typeof(ushort), size, sizeType, (ushort)0 };

                    // Object
                    yield return new object[4] { typeof(C), size, sizeType, null };
                    yield return new object[4] { typeof(D), size, sizeType, null };
                    yield return new object[4] { typeof(Delegate), size, sizeType, null };
                    yield return new object[4] { typeof(E), size, sizeType, (E)0 };
                    yield return new object[4] { typeof(El), size, sizeType, (El)0 };
                    yield return new object[4] { typeof(Func<object>), size, sizeType, null };
                    yield return new object[4] { typeof(I), size, sizeType, null };
                    yield return new object[4] { typeof(IEquatable<C>), size, sizeType, null };
                    yield return new object[4] { typeof(IEquatable<D>), size, sizeType, null };
                    yield return new object[4] { typeof(object), size, sizeType, null };
                    yield return new object[4] { typeof(string), size, sizeType, null };
                }
            }
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public static void NewArrayBounds(Type arrayType, object size, Type sizeType, object defaultValue)
        {
            Expression newArrayExpression = Expression.NewArrayBounds(arrayType, Expression.Constant(size, sizeType));
            Expression <Func<Array>> e = Expression.Lambda<Func<Array>>(newArrayExpression);
            Func<Array> f = e.CompileToTestMethod();

            if (sizeType == typeof(sbyte) || sizeType == typeof(short) || sizeType == typeof(int) || sizeType == typeof(long) ||
                sizeType == typeof(sbyte?) || sizeType == typeof(short?) || sizeType == typeof(int?) || sizeType == typeof(long?))
            {
                VerifyArrayGenerator(f, arrayType, size == null ? (long?)null : Convert.ToInt64(size), defaultValue);
            }
            else
            {
                VerifyArrayGenerator(f, arrayType, size == null ? (long?)null : unchecked((long)Convert.ToUInt64(size)), defaultValue);
            }
        }

        private static void VerifyArrayGenerator(Func<Array> func, Type arrayType, long? size, object defaultValue)
        {
            if (!size.HasValue)
            {
                Assert.Throws<InvalidOperationException>(() => func());
            }
            else if (unchecked((ulong)size) > int.MaxValue)
            {
                Assert.Throws<OverflowException>(() => func());
            }
            else if (size > MaxArraySize)
            {
                Assert.Throws<OutOfMemoryException>(() => func());
            }
            else
            {
                Array array = func();
                Assert.Equal(arrayType, array.GetType().GetElementType());
                Assert.Equal(0, array.GetLowerBound(0));
                Assert.Equal(size, array.Length);
                foreach (object value in array)
                {
                    Assert.Equal(defaultValue, value);
                }
            }
        }
    }
}
