// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class ArrayBoundsOneOffTests
    {
        [Fact]
        public void CompileWithCastTest()
        {
            Expression<Func<object[]>> expr = () => (object[])new BaseClass[1];
            expr.CompileToTestMethod();
        }

        [Fact]
        public void ToStringTest()
        {
            Expression<Func<int, object>> x = c => new double[c, c];
            object y = x.CompileToTestMethod()(2);
            Assert.Equal("System.Double[,]", y.ToString());
        }

        [Fact]
        public void ArrayBoundsVectorNegativeThrowsOverflowException()
        {
            Expression<Func<int, int[]>> e = a => new int[a];
            Func<int, int[]> f = e.CompileToTestMethod();

            Assert.Throws<OverflowException>(() => f(-1));
        }

        [Fact]
        public void ArrayBoundsMultiDimensionalNegativeThrowsOverflowException()
        {
            Expression<Func<int, int, int[,]>> e = (a, b) => new int[a, b];
            Func<int, int, int[,]> f = e.CompileToTestMethod();

            Assert.Throws<OverflowException>(() => f(-1, 1));
            Assert.Throws<OverflowException>(() => f(1, -1));
        }
    }
}
