﻿using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;

namespace System.Linq.Expressions
{
    class ConstantReplacerVisitor : ExpressionVisitor
    {
        private readonly List<LiveConstant> _constants = new List<LiveConstant>();
        public IReadOnlyCollection<LiveConstant> Constants => _constants;

        private ParameterExpression AddConstant(object value, Type type)
        {
            var lc = _constants.FirstOrDefault(c => c.Parameter.Type == type && c.Value.Value == value);
            if (lc != null)
            {
                return lc.Parameter;
            }
            lc = new LiveConstant(Expression.Parameter(type), Expression.Constant(value, type));
            _constants.Add(lc);
            return lc.Parameter;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            var typeInfo = node.Type.GetTypeInfo();
            if (typeInfo.IsPrimitive
                || node.Type == typeof(decimal)
                || node.Type == typeof(string)
                || typeof(Type).GetTypeInfo().IsAssignableFrom(node.Type)
                || (typeInfo.IsGenericType && typeInfo.GetGenericTypeDefinition() == typeof(Nullable<>) && typeInfo.GetGenericArguments()[0].GetTypeInfo().IsPrimitive))
            {
                return node;
            }
            if (node.Value == null)
            {
                return node;
            }
            return AddConstant(node.Value, node.Type);
        }

        protected override Expression VisitInvocation(InvocationExpression node)
        {
            if (node.Expression.NodeType == ExpressionType.Quote)
            {
                return node.Update(node.Expression, Visit(node.Arguments));
            }
            return base.VisitInvocation(node);
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            if (node.NodeType == ExpressionType.Quote)
            {
                var lambda = (LambdaExpression)node.Operand;
                return AddConstant(lambda, node.Type);
            }
            return base.VisitUnary(node);
        }
    }

    class LiveConstant
    {
        public LiveConstant(ParameterExpression parameter, ConstantExpression value)
        {
            Parameter = parameter;
            Value = value;
        }

        public ParameterExpression Parameter { get; }

        public ConstantExpression Value { get; }
    }

    static class Compiler
    {
        private static readonly ModuleBuilder ModuleBuilder;
        private static int Counter = 0;

        static Compiler()
        {
            var assemblyName = new AssemblyName("CompiledExpressions");
            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            ModuleBuilder = assemblyBuilder.DefineDynamicModule("CompiledExpressions");
        }

        public static Expression Constant<T>(Expression<Func<T>> constant)
        {
            return constant.Body;
        }

        public static T CompileToTestMethod<T>(this Expression<T> expression)
            where T: class
        {
            return CompileToTestMethod((LambdaExpression)expression) as T;
        }

        public static Delegate CompileToTestMethod(this LambdaExpression expression)
        {
            var current = Interlocked.Increment(ref Counter);
            var assemblyName = new AssemblyName("CompiledExpressions" + current);
            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            var moduleBuilder = assemblyBuilder.DefineDynamicModule("CompiledExpressions");
            var typeBuilder = moduleBuilder.DefineType("C" + current);
            var methodBuilder = typeBuilder.DefineMethod("M", MethodAttributes.Public | MethodAttributes.Static);
            var visitor = new ConstantReplacerVisitor();
            var expressionBodyWithoutConstants = visitor.Visit(expression.Body);
            if (visitor.Constants.Count == 0)
            {
                return CompileSimple(expression, typeBuilder, methodBuilder);
            }
            var parameters = expression.Parameters.Concat(visitor.Constants.Select(c => c.Parameter)).ToList();
            var expressionWithoutConstants = Expression.Lambda(expressionBodyWithoutConstants, parameters);
            expressionWithoutConstants.CompileToMethod(methodBuilder);
            var typeInfo = typeBuilder.CreateTypeInfo();
            var type = typeInfo.AsType();
            var method = type.GetMethod(methodBuilder.Name);
            var expr = Expression.Lambda(
                expression.Type,
                Expression.Call(method, expression.Parameters.Concat(visitor.Constants.Select(c => (Expression)c.Value))),
                expression.Parameters);
            return expr.Compile();
        }

        public static MethodBuilder CompileToMethodBuilder(this LambdaExpression expression, string className)
        {
            var assemblyName = new AssemblyName("CompiledExpressions" + className);
            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            var moduleBuilder = assemblyBuilder.DefineDynamicModule("CompiledExpressions");
            var typeBuilder = moduleBuilder.DefineType(className);
            var methodBuilder = typeBuilder.DefineMethod("M", MethodAttributes.Public | MethodAttributes.Static);
            expression.CompileToMethod(methodBuilder);
            typeBuilder.CreateTypeInfo();
            return methodBuilder;
        }

        public static T CompileToTestMethodSimple<T>(this Expression<T> expression)
            where T: class
        {
            var current = Interlocked.Increment(ref Counter);
            var assemblyName = new AssemblyName("CompiledExpressions" + current);
            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            var moduleBuilder = assemblyBuilder.DefineDynamicModule("CompiledExpressions");
            var typeBuilder = moduleBuilder.DefineType("C" + current);
            var methodBuilder = typeBuilder.DefineMethod("M", MethodAttributes.Public | MethodAttributes.Static);
            return CompileSimple(expression, typeBuilder, methodBuilder) as T;
        }

        private static Delegate CompileSimple(LambdaExpression expression, TypeBuilder typeBuilder, MethodBuilder methodBuilder)
        {
            expression.CompileToMethod(methodBuilder);
            var typeInfo = typeBuilder.CreateTypeInfo();
            var type = typeInfo.AsType();
            var method = type.GetMethod(methodBuilder.Name);
            var expr = Expression.Lambda(
                expression.Type,
                Expression.Call(method, expression.Parameters),
                expression.Parameters);
            return expr.Compile();
        }
    }
}
