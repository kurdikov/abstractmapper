﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class ParameterTests : ParameterExpressionTests
    {
        [Theory]
        [MemberData(nameof(ParameterTests.ValueData))]
        public void CanWriteAndReadBack(object value)
        {
            Type type = value.GetType();
            ParameterExpression param = Expression.Parameter(type);
            Assert.True(
                Expression.Lambda<Func<bool>>(
                    Expression.Equal(
                        Expression.Constant(value),
                        Expression.Block(
                            type,
                            new[] { param },
                            Expression.Assign(param, Expression.Constant(value)),
                            param
                            )
                        )
                    ).CompileToTestMethod()()
                );
        }

        [Fact]
        public void CanUseAsLambdaParameter()
        {
            ParameterExpression param = Expression.Parameter(typeof(int));
            Func<int, int> addOne = Expression.Lambda<Func<int, int>>(
                Expression.Add(param, Expression.Constant(1)),
                param
                ).CompileToTestMethod();
            Assert.Equal(3, addOne(2));
        }

        public delegate void ByRefFunc<T>(ref T arg);

        [Fact]
        public void CanUseAsLambdaByRefParameter()
        {
            ParameterExpression param = Expression.Parameter(typeof(int).MakeByRefType());
            ByRefFunc<int> addOneInPlace = Expression.Lambda<ByRefFunc<int>>(
                Expression.PreIncrementAssign(param),
                param
                ).CompileToTestMethod();
            int argument = 5;
            addOneInPlace(ref argument);
            Assert.Equal(6, argument);
        }

        [Fact]
        public void CanUseAsLambdaByRefParameter_String()
        {
            ParameterExpression param = Expression.Parameter(typeof(string).MakeByRefType());
            ByRefFunc<string> f = Expression.Lambda<ByRefFunc<string>>(
                Expression.Assign(param, Expression.Call(param, typeof(string).GetMethod(nameof(string.ToUpper), Type.EmptyTypes))),
                param
                ).CompileToTestMethod();
            string argument = "bar";
            f(ref argument);
            Assert.Equal("BAR", argument);
        }

        [Fact]
        public void CanUseAsLambdaByRefParameter_Char()
        {
            ParameterExpression param = Expression.Parameter(typeof(char).MakeByRefType());
            ByRefFunc<char> f = Expression.Lambda<ByRefFunc<char>>(
                Expression.Assign(param, Expression.Call(typeof(char).GetMethod(nameof(char.ToUpper), new[] { typeof(char) }), param)),
                param
                ).CompileToTestMethod();
            char argument = 'a';
            f(ref argument);
            Assert.Equal('A', argument);
        }

        [Fact]
        public void CanUseAsLambdaByRefParameter_Bool()
        {
            ParameterExpression param = Expression.Parameter(typeof(bool).MakeByRefType());
            ByRefFunc<bool> f = Expression.Lambda<ByRefFunc<bool>>(
                Expression.ExclusiveOrAssign(param, Expression.Constant(true)),
                param
                ).CompileToTestMethod();

            bool b1 = false;
            f(ref b1);
            Assert.Equal(false ^ true, b1);

            bool b2 = true;
            f(ref b2);
            Assert.Equal(true ^ true, b2);
        }

        [Fact]
        public void CanReadFromRefParameter()
        {
            AssertCanReadFromRefParameter<byte>(byte.MaxValue);
            AssertCanReadFromRefParameter<sbyte>(sbyte.MaxValue);
            AssertCanReadFromRefParameter<short>(short.MaxValue);
            AssertCanReadFromRefParameter<ushort>(ushort.MaxValue);
            AssertCanReadFromRefParameter<int>(int.MaxValue);
            AssertCanReadFromRefParameter<uint>(uint.MaxValue);
            AssertCanReadFromRefParameter<long>(long.MaxValue);
            AssertCanReadFromRefParameter<ulong>(ulong.MaxValue);

            AssertCanReadFromRefParameter<decimal>(49.94m);
            AssertCanReadFromRefParameter<float>(3.1415926535897931f);
            AssertCanReadFromRefParameter<double>(2.7182818284590451);

            AssertCanReadFromRefParameter('a');

            AssertCanReadFromRefParameter(ByteEnum.A);
            AssertCanReadFromRefParameter(SByteEnum.A);
            AssertCanReadFromRefParameter(Int16Enum.A);
            AssertCanReadFromRefParameter(UInt16Enum.A);
            AssertCanReadFromRefParameter(Int32Enum.A);
            AssertCanReadFromRefParameter(UInt32Enum.A);
            AssertCanReadFromRefParameter(Int64Enum.A);
            AssertCanReadFromRefParameter(UInt64Enum.A);

            AssertCanReadFromRefParameter(new DateTime(1983, 2, 11));

            AssertCanReadFromRefParameter<object>(null);
            AssertCanReadFromRefParameter<object>(new object());
            AssertCanReadFromRefParameter<string>("bar");

            AssertCanReadFromRefParameter<int?>(null);
            AssertCanReadFromRefParameter<int?>(int.MaxValue);
            AssertCanReadFromRefParameter<Int64Enum?>(null);
            AssertCanReadFromRefParameter<Int64Enum?>(Int64Enum.A);
            AssertCanReadFromRefParameter<DateTime?>(null);
            AssertCanReadFromRefParameter<DateTime?>(new DateTime(1983, 2, 11));
        }

        public delegate T ByRefReadFunc<T>(ref T arg);

        private void AssertCanReadFromRefParameter<T>(T value)
        {
            ParameterExpression @ref = Expression.Parameter(typeof(T).MakeByRefType());

            ByRefReadFunc<T> f =
                Expression.Lambda<ByRefReadFunc<T>>(
                    @ref,
                    @ref
                ).CompileToTestMethod();

            Assert.Equal(value, f(ref value));
        }

        public delegate void ByRefWriteAction<T>(ref T arg, T value);

        [Fact]
        public void CanWriteToRefParameter()
        {
            AssertCanWriteToRefParameter<byte>(byte.MaxValue);
            AssertCanWriteToRefParameter<sbyte>(sbyte.MaxValue);
            AssertCanWriteToRefParameter<short>(short.MaxValue);
            AssertCanWriteToRefParameter<ushort>(ushort.MaxValue);
            AssertCanWriteToRefParameter<int>(int.MaxValue);
            AssertCanWriteToRefParameter<uint>(uint.MaxValue);
            AssertCanWriteToRefParameter<long>(long.MaxValue);
            AssertCanWriteToRefParameter<ulong>(ulong.MaxValue);

            AssertCanWriteToRefParameter<decimal>(49.94m);
            AssertCanWriteToRefParameter<float>(3.1415926535897931f);
            AssertCanWriteToRefParameter<double>(2.7182818284590451);

            AssertCanWriteToRefParameter('a');

            AssertCanWriteToRefParameter(ByteEnum.A);
            AssertCanWriteToRefParameter(SByteEnum.A);
            AssertCanWriteToRefParameter(Int16Enum.A);
            AssertCanWriteToRefParameter(UInt16Enum.A);
            AssertCanWriteToRefParameter(Int32Enum.A);
            AssertCanWriteToRefParameter(UInt32Enum.A);
            AssertCanWriteToRefParameter(Int64Enum.A);
            AssertCanWriteToRefParameter(UInt64Enum.A);

            AssertCanWriteToRefParameter(new DateTime(1983, 2, 11));

            AssertCanWriteToRefParameter<object>(null);
            AssertCanWriteToRefParameter<object>(new object());
            AssertCanWriteToRefParameter<string>("bar");

            AssertCanWriteToRefParameter<int?>(null, original: 42);
            AssertCanWriteToRefParameter<int?>(int.MaxValue);
            AssertCanWriteToRefParameter<Int64Enum?>(null, original: Int64Enum.A);
            AssertCanWriteToRefParameter<Int64Enum?>(Int64Enum.A);
            AssertCanWriteToRefParameter<DateTime?>(null, original: new DateTime(1983, 2, 11));
            AssertCanWriteToRefParameter<DateTime?>(new DateTime(1983, 2, 11));
        }

        private void AssertCanWriteToRefParameter<T>(T value, T original = default(T))
        {
            ParameterExpression @ref = Expression.Parameter(typeof(T).MakeByRefType());
            ParameterExpression val = Expression.Parameter(typeof(T));

            ByRefWriteAction<T> f =
                Expression.Lambda<ByRefWriteAction<T>>(
                    Expression.Assign(@ref, val),
                    @ref, val
                ).CompileToTestMethod();

            T res = original;
            f(ref res, value);

            Assert.Equal(res, value);
        }

        [Theory]
        [MemberData(nameof(ReadAndWriteRefCases))]
        public void ReadAndWriteRefParameters(object value, object increment, object result)
        {
            Type type = value.GetType();

            MethodInfo method = typeof(ParameterTests).GetMethod(nameof(AssertReadAndWriteRefParameters), BindingFlags.NonPublic | BindingFlags.Static);

            method.MakeGenericMethod(type).Invoke(null, new object[] { value, increment, result });
        }

        private static void AssertReadAndWriteRefParameters<T>(T value, T increment, T result)
        {
            ParameterExpression param = Expression.Parameter(typeof(T).MakeByRefType());
            ByRefFunc<T> addOneInPlace = Expression.Lambda<ByRefFunc<T>>(
                Expression.AddAssign(param, Expression.Constant(increment, typeof(T))),
                param
                ).CompileToTestMethod();
            T argument = value;
            addOneInPlace(ref argument);
            Assert.Equal(result, argument);
        }

        public static IEnumerable<object[]> ReadAndWriteRefCases()
        {
            yield return new object[] { (short)41, (short)1, (short)42 };
            yield return new object[] { (ushort)41, (ushort)1, (ushort)42 };
            yield return new object[] { 41, 1, 42 };
            yield return new object[] { 41U, 1U, 42U };
            yield return new object[] { 41L, 1L, 42L };
            yield return new object[] { 41UL, 1UL, 42UL };
            yield return new object[] { 41.0F, 1.0F, Apply((x, y) => x + y, 41.0F, 1.0F) };
            yield return new object[] { 41.0D, 1.0D, Apply((x, y) => x + y, 41.0D, 1.0D) };
            yield return new object[] { TimeSpan.FromSeconds(41), TimeSpan.FromSeconds(1), Apply((x, y) => x + y, TimeSpan.FromSeconds(41), TimeSpan.FromSeconds(1)) };
        }

        private static T Apply<T>(Func<T, T, T> f, T x, T y) => f(x, y);
    }
}
