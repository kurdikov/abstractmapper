// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class ConditionalTests
    {
        [Fact]
        public void Conditional()
        {
            Expression<Func<int, int, int>> f = (x, y) => x > 5 ? x : y;
            Func<int, int, int> d = f.CompileToTestMethod();
            Assert.Equal(7, d(7, 4));
            Assert.Equal(6, d(3, 6));
        }

        [Fact]
        public void AnyTypesAllowedWithExplicitVoid()
        {
            Action act = Expression.Lambda<Action>(
                Expression.Condition(Expression.Constant(true), Expression.Constant(0), Expression.Constant(0L), typeof(void))
                ).CompileToTestMethod();
            act();

            act = Expression.Lambda<Action>(
                Expression.Condition(Expression.Constant(true), Expression.Constant(0L), Expression.Constant(0), typeof(void))
                ).CompileToTestMethod();
            act();
        }

        [Theory, MemberData(nameof(ConditionalValues))]
        public void ConditionalSelectsCorrectExpression(bool test, object ifTrue, object ifFalse, object expected)
        {
            Func<object> func = Expression.Lambda<Func<object>>(
                Expression.Convert(
                    Expression.Condition(
                        Expression.Constant(test),
                        Expression.Constant(ifTrue),
                        Expression.Constant(ifFalse)
                        ),
                        typeof(object)
                    )
                ).CompileToTestMethod();

            Assert.Equal(expected, func());
        }

        [Theory, MemberData(nameof(ConditionalValuesWithTypes))]
        public void ConditionalSelectsCorrectExpressionWithType(bool test, object ifTrue, object ifFalse, object expected, Type type)
        {
            Func<object> func = Expression.Lambda<Func<object>>(
                Expression.Condition(
                    Expression.Constant(test),
                    Expression.Constant(ifTrue),
                    Expression.Constant(ifFalse),
                    type
                    )
                ).CompileToTestMethod();

            Assert.Same(expected, func());
        }

        private static IEnumerable<object[]> ConditionalValues()
        {
            yield return new object[] { true, "yes", "no", "yes" };
            yield return new object[] { false, "yes", "no", "no" };
            yield return new object[] { true, 42, 12, 42 };
            yield return new object[] { false, 42L, 12L, 12L };
        }

        private static IEnumerable<object[]> ConditionalValuesWithTypes()
        {
            ConstantExpression ce = Expression.Constant(98);
            BinaryExpression be = Expression.And(Expression.Constant(2), Expression.Constant(3));
            yield return new object[] { true, ce, be, ce, typeof(Expression) };
            yield return new object[] { false, ce, be, be, typeof(Expression) };
        }

        private static class Unreadable<T>
        {
            public static T WriteOnly
            {
                set { }
            }
        }

        private static void Nop()
        {
        }

        private class Visitor : ExpressionVisitor
        {
        }
    }
}
