// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class MemberInitTests
    {
        #region Test methods

        [Fact]
        public static void CheckMemberInitTest()
        {
            VerifyMemberInit(() => new X { Y = { Z = 42, YS = { 2, 3 } }, XS = { 5, 7 } }, x => x.Y.Z == 42 && x.XS.Sum() == 5 + 7 && x.Y.YS.Sum() == 2 + 3);
        }

#if NETCOREAPP1_1
        [Fact(Skip = "ReduceAndCheck produces invalid expression on .net core 1.1, fixed in 2.0 commit 0c9438602a7b8ff609e48c5c1c69e1c1337bd6d2")]
#else
        [Fact]
#endif
        public static void Reduce()
        {
            Expression<Func<X>> l = () => new X {Y = {Z = 42, YS = {2, 3}}, XS = {5, 7}};
            MemberInitExpression e = l.Body as MemberInitExpression;
            l = Expression.Lambda<Func<X>>(e.ReduceAndCheck());
            VerifyMemberInit(l, x => x.Y.Z == 42 && x.XS.Sum() == 5 + 7 && x.Y.YS.Sum() == 2 + 3);
        }


#endregion

#region Test verifiers

        private static void VerifyMemberInit<T>(Expression<Func<T>> expr, Func<T, bool> check)
        {
            Func<T> c = expr.CompileToTestMethod();
            Assert.True(check(c()));
        }

#endregion

#region Helpers

        public class X
        {
            private readonly Y _y = new Y();
            private readonly List<int> _xs = new List<int>();

            public Y Y
            {
                get { return _y; }
            }

            public List<int> XS { get { return _xs; } }
        }

        public class Y
        {
            private readonly List<int> _ys = new List<int>();

            public int Z { get; set; }
            public int A { get; set; }

            public List<int> YS { get { return _ys; } }
        }

#endregion
    }
}

