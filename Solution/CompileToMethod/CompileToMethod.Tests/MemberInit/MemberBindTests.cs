// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class MemberBindTests
    {
        private class PropertyAndFields
        {
#pragma warning disable 649 // Assigned through expressions.
            public string StringProperty { get; set; }
            public string StringField;
            public readonly string ReadonlyStringField;
            public string ReadonlyStringProperty { get { return ""; } }
            public static string StaticStringProperty { get; set; }
            public static string StaticStringField;
            public const string ConstantString = "Constant";
#pragma warning restore 649
        }

        public class Inner
        {
            public int Value { get; set; }
        }

        public class Outer
        {
            public Inner InnerProperty { get; set; } = new Inner();
            public Inner InnerField = new Inner();
            public Inner ReadonlyInnerProperty { get; } = new Inner();
            public Inner WriteonlyInnerProperty { set { } }
            public readonly Inner ReadonlyInnerField = new Inner();
            public static Inner StaticInnerProperty { get; set; } = new Inner();
            public static Inner StaticInnerField = new Inner();
            public static Inner StaticReadonlyInnerProperty { get; } = new Inner();
            public static readonly Inner StaticReadonlyInnerField = new Inner();
            public static Inner StaticWriteonlyInnerProperty { set { } }
        }

        [Fact]
        public void InnerProperty()
        {
            Expression<Func<Outer>> exp = Expression.Lambda<Func<Outer>>(
                Expression.MemberInit(
                    Expression.New(typeof(Outer)),
                    Expression.MemberBind(
                        typeof(Outer).GetProperty(nameof(Outer.InnerProperty)),
                        Expression.Bind(typeof(Inner).GetProperty(nameof(Inner.Value)), Expression.Constant(3))
                        )
                    )
                );
            Func<Outer> func = exp.CompileToTestMethod();
            Assert.Equal(3, func().InnerProperty.Value);
        }

        [Fact]
        public void InnerField()
        {
            Expression<Func<Outer>> exp = Expression.Lambda<Func<Outer>>(
                Expression.MemberInit(
                    Expression.New(typeof(Outer)),
                    Expression.MemberBind(
                        typeof(Outer).GetField(nameof(Outer.InnerField)),
                        Expression.Bind(typeof(Inner).GetProperty(nameof(Inner.Value)), Expression.Constant(4))
                        )
                    )
                );
            Func<Outer> func = exp.CompileToTestMethod();
            Assert.Equal(4, func().InnerField.Value);
        }

        [Fact]
        public void StaticInnerProperty()
        {
            Expression<Func<Outer>> exp = Expression.Lambda<Func<Outer>>(
                Expression.MemberInit(
                    Expression.New(typeof(Outer)),
                    Expression.MemberBind(
                        typeof(Outer).GetProperty(nameof(Outer.StaticInnerProperty)),
                        Expression.Bind(typeof(Inner).GetProperty(nameof(Inner.Value)), Expression.Constant(5))
                        )
                    )
                );
            Assert.Throws<InvalidProgramException>(() => exp.CompileToTestMethod());
        }

        [Fact]
        public void StaticInnerField()
        {
            Expression<Func<Outer>> exp = Expression.Lambda<Func<Outer>>(
                Expression.MemberInit(
                    Expression.New(typeof(Outer)),
                    Expression.MemberBind(
                        typeof(Outer).GetField(nameof(Outer.StaticInnerField)),
                        Expression.Bind(typeof(Inner).GetProperty(nameof(Inner.Value)), Expression.Constant(6))
                        )
                    )
                );
            Assert.Throws<InvalidProgramException>(() => exp.CompileToTestMethod());
        }

        [Fact]
        public void ReadonlyInnerProperty()
        {
            Expression<Func<Outer>> exp = Expression.Lambda<Func<Outer>>(
                Expression.MemberInit(
                    Expression.New(typeof(Outer)),
                    Expression.MemberBind(
                        typeof(Outer).GetProperty(nameof(Outer.ReadonlyInnerProperty)),
                        Expression.Bind(typeof(Inner).GetProperty(nameof(Inner.Value)), Expression.Constant(7))
                        )
                    )
                );
            Func<Outer> func = exp.CompileToTestMethod();
            Assert.Equal(7, func().ReadonlyInnerProperty.Value);
        }

        [Fact]
        public void ReadonlyInnerField()
        {
            Expression<Func<Outer>> exp = Expression.Lambda<Func<Outer>>(
                Expression.MemberInit(
                    Expression.New(typeof(Outer)),
                    Expression.MemberBind(
                        typeof(Outer).GetField(nameof(Outer.ReadonlyInnerField)),
                        Expression.Bind(typeof(Inner).GetProperty(nameof(Inner.Value)), Expression.Constant(8))
                        )
                    )
                );
            Func<Outer> func = exp.CompileToTestMethod();
            Assert.Equal(8, func().ReadonlyInnerField.Value);
        }

        [Fact]
        public void StaticReadonlyInnerProperty()
        {
            Expression<Func<Outer>> exp = Expression.Lambda<Func<Outer>>(
                Expression.MemberInit(
                    Expression.New(typeof(Outer)),
                    Expression.MemberBind(
                        typeof(Outer).GetProperty(nameof(Outer.StaticReadonlyInnerProperty)),
                        Expression.Bind(typeof(Inner).GetProperty(nameof(Inner.Value)), Expression.Constant(5))
                        )
                    )
                );
            Assert.Throws<InvalidProgramException>(() => exp.CompileToTestMethod());
        }

        [Fact]
        public void StaticReadonlyInnerField()
        {
            Expression<Func<Outer>> exp = Expression.Lambda<Func<Outer>>(
                Expression.MemberInit(
                    Expression.New(typeof(Outer)),
                    Expression.MemberBind(
                        typeof(Outer).GetField(nameof(Outer.StaticReadonlyInnerField)),
                        Expression.Bind(typeof(Inner).GetProperty(nameof(Inner.Value)), Expression.Constant(6))
                        )
                    )
                );
            Assert.Throws<InvalidProgramException>(() => exp.CompileToTestMethod());
        }


    }
}
