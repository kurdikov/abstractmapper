// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class BindTests
    {
        public class PropertyAndFields
        {
#pragma warning disable 649 // Assigned through expressions.
            public string StringProperty { get; set; }
            public string StringField;
            public readonly string ReadonlyStringField;
            public string ReadonlyStringProperty => "";
            public static string StaticStringProperty { get; set; }
            public static string StaticStringField;
            public static string StaticReadonlyStringProperty => "";
            public static readonly string StaticReadonlyStringField = "";
            public const string ConstantString = "Constant";
#pragma warning restore 649
        }

        private class Unreadable<T>
        {
            public T WriteOnly
            {
                set { }
            }
        }

        private class GenericType<T>
        {
            public int AlwaysInt32 { get; set; }
        }

        [Fact]
        public void MemberAssignmentFromMember()
        {
            PropertyAndFields result = Expression.Lambda<Func<PropertyAndFields>>(
                Expression.MemberInit(
                    Expression.New(typeof(PropertyAndFields)),
                    Expression.Bind(
                        typeof(PropertyAndFields).GetMember("StringProperty")[0],
                        Expression.Constant("Hello Property")
                        ),
                    Expression.Bind(
                        typeof(PropertyAndFields).GetMember("StringField")[0],
                        Expression.Constant("Hello Field")
                        )
                )
            ).CompileToTestMethod()();

            Assert.Equal("Hello Property", result.StringProperty);
            Assert.Equal("Hello Field", result.StringField);
        }

        [Fact]
        public void MemberAssignmentFromMethodInfo()
        {
            PropertyAndFields result = Expression.Lambda<Func<PropertyAndFields>>(
                Expression.MemberInit(
                    Expression.New(typeof(PropertyAndFields)),
                    Expression.Bind(
                        typeof(PropertyAndFields).GetProperty("StringProperty"),
                        Expression.Constant("Hello Property")
                        )
                )
            ).CompileToTestMethod()();

            Assert.Equal("Hello Property", result.StringProperty);
        }

        [Fact]
        public void ConstantField()
        {
            MemberInfo member = typeof(PropertyAndFields).GetMember(nameof(PropertyAndFields.ConstantString))[0];
            Expression<Func<PropertyAndFields>> attemptAssignToConstant = Expression.Lambda<Func<PropertyAndFields>>(
                Expression.MemberInit(
                    Expression.New(typeof(PropertyAndFields)),
                    Expression.Bind(member, Expression.Constant(""))
                    )
                );

            Assert.Throws<NotSupportedException>(() => attemptAssignToConstant.CompileToTestMethod());
        }

        [Fact]
        public void ReadonlyField()
        {
            MemberInfo member = typeof(PropertyAndFields).GetMember(nameof(PropertyAndFields.ReadonlyStringField))[0];
            Expression<Func<PropertyAndFields>> assignToReadonly = Expression.Lambda<Func<PropertyAndFields>>(
                Expression.MemberInit(
                    Expression.New(typeof(PropertyAndFields)),
                    Expression.Bind(member, Expression.Constant("ABC"))
                    )
                );
            Func<PropertyAndFields> func = assignToReadonly.CompileToTestMethod();
            Assert.Equal("ABC", func().ReadonlyStringField);
        }

        [Fact]
        public void StaticField()
        {
            MemberInfo member = typeof(PropertyAndFields).GetMember(nameof(PropertyAndFields.StaticStringField))[0];
            Expression<Func<PropertyAndFields>> assignToReadonly = Expression.Lambda<Func<PropertyAndFields>>(
                Expression.MemberInit(
                    Expression.New(typeof(PropertyAndFields)),
                    Expression.Bind(member, Expression.Constant("ABC"))
                    )
                );
            Func<PropertyAndFields> func = assignToReadonly.CompileToTestMethod();
            PropertyAndFields.StaticStringField = "123";
            func();
            Assert.Equal("ABC", PropertyAndFields.StaticStringField);
        }

        [Fact]
        public void StaticReadonlyField()
        {
            MemberInfo member = typeof(PropertyAndFields).GetMember(nameof(PropertyAndFields.StaticReadonlyStringField))[0];
            Expression<Func<PropertyAndFields>> assignToReadonly = Expression.Lambda<Func<PropertyAndFields>>(
                Expression.MemberInit(
                    Expression.New(typeof(PropertyAndFields)),
                    Expression.Bind(member, Expression.Constant("ABC"))
                    )
                );
            Func<PropertyAndFields> func = assignToReadonly.CompileToTestMethod();
            func();
            Assert.Equal("ABC", PropertyAndFields.StaticReadonlyStringField);
        }

        [Fact]
        public void StaticProperty()
        {
            MemberInfo member = typeof(PropertyAndFields).GetMember(nameof(PropertyAndFields.StaticStringProperty))[0];
            Expression<Func<PropertyAndFields>> assignToStaticProperty = Expression.Lambda<Func<PropertyAndFields>>(
                Expression.MemberInit(
                    Expression.New(typeof(PropertyAndFields)),
                    Expression.Bind(member, Expression.Constant("ABC"))
                    )
                );
            Assert.Throws<InvalidProgramException>(() => assignToStaticProperty.CompileToTestMethod());
        }

    }
}
