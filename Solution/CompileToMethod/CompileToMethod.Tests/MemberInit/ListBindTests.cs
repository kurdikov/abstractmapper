// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class ListBindTests
    {
        public class ListWrapper<T>
        {
            public static List<T> StaticListField = new List<T>();
            public static List<T> StaticListProperty { get; set; }
            public readonly List<T> ListField = new List<T>();

            public List<T> ListProperty
            {
                get { return ListField; }
            }

            public HashSet<T> HashSetField = new HashSet<T>();

            public List<T> WriteOnlyList
            {
                set { }
            }

            public List<T> GetList()
            {
                return ListField;
            }

            public IEnumerable<T> EnumerableProperty
            {
                get { return ListField; }
            }
        }

        private static IEnumerable<object> NonAddableListExpressions()
        {
            PropertyInfo property = typeof(ListWrapper<int>).GetProperty(nameof(ListWrapper<int>.EnumerableProperty));
            MemberInfo member = typeof(ListWrapper<int>).GetMember(nameof(ListWrapper<int>.EnumerableProperty))[0];
            yield return new object[] { Expression.ListBind(property) };
            yield return new object[] { Expression.ListBind(property, Enumerable.Empty<ElementInit>()) };
            yield return new object[] { Expression.ListBind(member) };
            yield return new object[] { Expression.ListBind(member, Enumerable.Empty<ElementInit>()) };
        }

        [Theory, MemberData(nameof(NonAddableListExpressions))]
        public void NonAddableListType(MemberListBinding listBinding)
        {
            Func<ListWrapper<int>> func = Expression.Lambda<Func<ListWrapper<int>>>(
                Expression.MemberInit(
                    Expression.New(typeof(ListWrapper<int>)),
                    listBinding
                )
            ).CompileToTestMethod();
            Assert.Empty(func().EnumerableProperty);
        }

        public static IEnumerable<object[]> ZeroInitializerExpressions()
        {
            PropertyInfo property = typeof(ListWrapper<int>).GetProperty(nameof(ListWrapper<int>.ListProperty));
            MemberInfo member = typeof(ListWrapper<int>).GetMember(nameof(ListWrapper<int>.ListProperty))[0];
            MemberInfo fieldMember = typeof(ListWrapper<int>).GetMember(nameof(ListWrapper<int>.ListField))[0];
            yield return new object[] { Expression.ListBind(property) };
            yield return new object[] { Expression.ListBind(property, Enumerable.Empty<ElementInit>()) };
            yield return new object[] { Expression.ListBind(member) };
            yield return new object[] { Expression.ListBind(member, Enumerable.Empty<ElementInit>()) };
            yield return new object[] { Expression.ListBind(fieldMember) };
            yield return new object[] { Expression.ListBind(fieldMember, Enumerable.Empty<ElementInit>()) };
        }


        [Theory]
        [MemberData("ZeroInitializerExpressions")]
        public void ZeroInitializersIsValid(MemberListBinding binding)
        {
            Func<ListWrapper<int>> func = Expression.Lambda<Func<ListWrapper<int>>>(
                Expression.MemberInit(
                    Expression.New(typeof(ListWrapper<int>)),
                    binding
                )
            ).CompileToTestMethod();
            Assert.Empty(func().ListProperty);
        }

        [Fact]
        public void StaticListProperty()
        {
            PropertyInfo property = typeof(ListWrapper<int>).GetProperty(nameof(ListWrapper<int>.StaticListProperty));
            Expression<Func<ListWrapper<int>>> exp = Expression.Lambda<Func<ListWrapper<int>>>(
                Expression.MemberInit(
                    Expression.New(typeof(ListWrapper<int>)),
                    Expression.ListBind(
                        property,
                        Expression.ElementInit(
                            typeof(List<int>).GetMethod(nameof(List<int>.Add)),
                            Expression.Constant(0)
                        )
                    )
                )
            );

            Assert.Throws<InvalidProgramException>(() => exp.CompileToTestMethod());
        }

        [Fact]
        public void StaticListField()
        {
            FieldInfo field = typeof(ListWrapper<int>).GetField(nameof(ListWrapper<int>.StaticListField));
            Expression<Func<ListWrapper<int>>> exp = Expression.Lambda<Func<ListWrapper<int>>>(
                Expression.MemberInit(
                    Expression.New(typeof(ListWrapper<int>)),
                    Expression.ListBind(
                        field,
                        Expression.ElementInit(
                            typeof(List<int>).GetMethod(nameof(List<int>.Add)),
                            Expression.Constant(0)
                        )
                    )
                )
            );

            Assert.Throws<InvalidProgramException>(() => exp.CompileToTestMethod());
        }


        [Fact]
        public void InitializeNonVoidAdd()
        {
            Expression<Func<ListWrapper<int>>> hashInit = () => new ListWrapper<int> { HashSetField = { 1, 4, 9, 16 } };
            Func<ListWrapper<int>> func = hashInit.CompileToTestMethod();
            Assert.Equal(new[] { 1, 4, 9, 16 }, func().HashSetField.OrderBy(i => i));
        }

    }
}
