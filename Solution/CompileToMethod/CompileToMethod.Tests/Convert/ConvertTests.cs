// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class ConvertTests
    {
        #region Test methods

        [Fact]
        public static void ConvertByteToByteTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToByte(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableByteTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertByteToCharTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToChar(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableCharTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertByteToDecimalTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableDecimalTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertByteToDoubleTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToDouble(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableDoubleTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertByteToEnumTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToEnum(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableEnumTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertByteToEnumLongTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableEnumLongTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertByteToFloatTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToFloat(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableFloatTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertByteToIntTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToInt(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableIntTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertByteToLongTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToLong(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableLongTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertByteToSByteTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToSByte(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableSByteTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertByteToShortTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToShort(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableShortTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertByteToUIntTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToUInt(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableUIntTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertByteToULongTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToULong(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableULongTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertByteToUShortTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToUShort(value);
            }
        }

        [Fact]
        public static void ConvertByteToNullableUShortTest()
        {
            foreach (byte value in new byte[] { 0, 1, byte.MaxValue })
            {
                VerifyByteToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToByteTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableByteTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToCharTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableCharTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToDecimalTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableDecimalTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToDoubleTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableDoubleTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToEnumTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableEnumTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToEnumLongTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableEnumLongTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToFloatTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableFloatTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToIntTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableIntTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToLongTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableLongTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToSByteTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableSByteTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToShortTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableShortTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToUIntTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableUIntTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToULongTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableULongTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToUShortTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableByteToNullableUShortTest()
        {
            foreach (byte? value in new byte?[] { null, 0, 1, byte.MaxValue })
            {
                VerifyNullableByteToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertCharToByteTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToByte(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableByteTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertCharToCharTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToChar(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableCharTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertCharToDecimalTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableDecimalTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertCharToDoubleTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToDouble(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableDoubleTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertCharToEnumTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToEnum(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableEnumTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertCharToEnumLongTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableEnumLongTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertCharToFloatTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToFloat(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableFloatTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertCharToIntTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToInt(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableIntTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertCharToLongTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToLong(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableLongTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertCharToSByteTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToSByte(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableSByteTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertCharToShortTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToShort(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableShortTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertCharToUIntTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToUInt(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableUIntTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertCharToULongTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToULong(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableULongTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertCharToUShortTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToUShort(value);
            }
        }

        [Fact]
        public static void ConvertCharToNullableUShortTest()
        {
            foreach (char value in new char[] { '\0', '\b', 'A', '\uffff' })
            {
                VerifyCharToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToByteTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableByteTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToCharTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableCharTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToDecimalTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableDecimalTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToDoubleTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableDoubleTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToEnumTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableEnumTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToEnumLongTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableEnumLongTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToFloatTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableFloatTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToIntTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableIntTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToLongTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableLongTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToSByteTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableSByteTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToShortTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableShortTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToUIntTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableUIntTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToULongTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableULongTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToUShortTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableCharToNullableUShortTest()
        {
            foreach (char? value in new char?[] { null, '\0', '\b', 'A', '\uffff' })
            {
                VerifyNullableCharToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToByteTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToByte(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToNullableByteTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToCharTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToChar(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToNullableCharTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToDecimalTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToNullableDecimalTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToDoubleTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToDouble(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToNullableDoubleTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToFloatTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToFloat(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToNullableFloatTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToIntTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToInt(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToNullableIntTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToLongTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToLong(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToNullableLongTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToSByteTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToSByte(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToNullableSByteTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToShortTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToShort(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToNullableShortTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToUIntTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToUInt(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToNullableUIntTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToULongTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToULong(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToNullableULongTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToUShortTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToUShort(value);
            }
        }

        [Fact]
        public static void ConvertDecimalToNullableUShortTest()
        {
            foreach (decimal value in new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyDecimalToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToByteTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToNullableByteTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToCharTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToNullableCharTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToDecimalTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToNullableDecimalTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToDoubleTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToNullableDoubleTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToFloatTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToNullableFloatTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToIntTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToNullableIntTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToLongTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToNullableLongTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToSByteTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToNullableSByteTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToShortTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToNullableShortTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToUIntTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToNullableUIntTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToULongTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToNullableULongTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToUShortTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableDecimalToNullableUShortTest()
        {
            foreach (decimal? value in new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue })
            {
                VerifyNullableDecimalToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToByteTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToByte(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableByteTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToCharTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToChar(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableCharTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToDecimalTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableDecimalTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToDoubleTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToDouble(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableDoubleTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToEnumTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToEnum(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableEnumTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToEnumLongTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableEnumLongTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToFloatTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToFloat(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableFloatTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToIntTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToInt(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableIntTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToLongTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToLong(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableLongTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToSByteTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToSByte(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableSByteTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToShortTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToShort(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableShortTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToUIntTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToUInt(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableUIntTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToULongTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToULong(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableULongTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToUShortTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToUShort(value);
            }
        }

        [Fact]
        public static void ConvertDoubleToNullableUShortTest()
        {
            foreach (double value in new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyDoubleToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToByteTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableByteTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToCharTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableCharTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToDecimalTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableDecimalTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToDoubleTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableDoubleTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToEnumTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableEnumTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToEnumLongTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableEnumLongTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToFloatTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableFloatTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToIntTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableIntTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToLongTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableLongTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToSByteTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableSByteTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToShortTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableShortTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToUIntTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableUIntTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToULongTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableULongTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToUShortTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableDoubleToNullableUShortTest()
        {
            foreach (double? value in new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN })
            {
                VerifyNullableDoubleToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertEnumToByteTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToByte(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableByteTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertEnumToCharTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToChar(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableCharTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertEnumToDoubleTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToDouble(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableDoubleTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertEnumToEnumTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToEnum(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableEnumTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertEnumToEnumLongTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableEnumLongTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertEnumToFloatTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToFloat(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableFloatTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertEnumToIntTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToInt(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableIntTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertEnumToLongTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToLong(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableLongTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertEnumToSByteTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToSByte(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableSByteTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertEnumToShortTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToShort(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableShortTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertEnumToUIntTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToUInt(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableUIntTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertEnumToULongTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToULong(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableULongTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertEnumToUShortTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToUShort(value);
            }
        }

        [Fact]
        public static void ConvertEnumToNullableUShortTest()
        {
            foreach (E value in new E[] { (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyEnumToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToByteTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableByteTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToCharTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableCharTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToDoubleTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableDoubleTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToEnumTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableEnumTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToEnumLongTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableEnumLongTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToFloatTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableFloatTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToIntTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableIntTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToLongTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableLongTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToSByteTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableSByteTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToShortTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableShortTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToUIntTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableUIntTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToULongTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableULongTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToUShortTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumToNullableUShortTest()
        {
            foreach (E? value in new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue })
            {
                VerifyNullableEnumToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToByteTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToByte(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableByteTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToCharTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToChar(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableCharTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToDoubleTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToDouble(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableDoubleTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToEnumTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToEnum(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableEnumTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToEnumLongTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableEnumLongTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToFloatTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToFloat(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableFloatTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToIntTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToInt(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableIntTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToLongTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToLong(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableLongTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToSByteTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToSByte(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableSByteTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToShortTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToShort(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableShortTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToUIntTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToUInt(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableUIntTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToULongTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToULong(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableULongTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToUShortTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToUShort(value);
            }
        }

        [Fact]
        public static void ConvertEnumLongToNullableUShortTest()
        {
            foreach (El value in new El[] { (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyEnumLongToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToByteTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableByteTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToCharTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableCharTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToDoubleTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableDoubleTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToEnumTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableEnumTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToEnumLongTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableEnumLongTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToFloatTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableFloatTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToIntTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableIntTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToLongTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableLongTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToSByteTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableSByteTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToShortTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableShortTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToUIntTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableUIntTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToULongTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableULongTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToUShortTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableEnumLongToNullableUShortTest()
        {
            foreach (El? value in new El?[] { null, (El)0, El.A, El.B, (El)long.MaxValue, (El)long.MinValue })
            {
                VerifyNullableEnumLongToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertFloatToByteTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToByte(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableByteTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertFloatToCharTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToChar(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableCharTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertFloatToDecimalTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableDecimalTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertFloatToDoubleTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToDouble(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableDoubleTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertFloatToEnumTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToEnum(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableEnumTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertFloatToEnumLongTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableEnumLongTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertFloatToFloatTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToFloat(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableFloatTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertFloatToIntTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToInt(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableIntTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertFloatToLongTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToLong(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableLongTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertFloatToSByteTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToSByte(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableSByteTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertFloatToShortTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToShort(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableShortTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertFloatToUIntTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToUInt(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableUIntTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertFloatToULongTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToULong(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableULongTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertFloatToUShortTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToUShort(value);
            }
        }

        [Fact]
        public static void ConvertFloatToNullableUShortTest()
        {
            foreach (float value in new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyFloatToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToByteTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableByteTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToCharTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableCharTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToDecimalTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableDecimalTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToDoubleTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableDoubleTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToEnumTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableEnumTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToEnumLongTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableEnumLongTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToFloatTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableFloatTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToIntTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableIntTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToLongTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableLongTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToSByteTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableSByteTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToShortTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableShortTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToUIntTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableUIntTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToULongTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableULongTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToUShortTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableFloatToNullableUShortTest()
        {
            foreach (float? value in new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN })
            {
                VerifyNullableFloatToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertIntToByteTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToByte(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableByteTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertIntToCharTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToChar(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableCharTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertIntToDecimalTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableDecimalTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertIntToDoubleTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToDouble(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableDoubleTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertIntToEnumTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToEnum(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableEnumTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertIntToEnumLongTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableEnumLongTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertIntToFloatTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToFloat(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableFloatTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertIntToIntTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToInt(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableIntTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertIntToLongTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToLong(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableLongTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertIntToSByteTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToSByte(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableSByteTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertIntToShortTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToShort(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableShortTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertIntToUIntTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToUInt(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableUIntTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertIntToULongTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToULong(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableULongTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertIntToUShortTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToUShort(value);
            }
        }

        [Fact]
        public static void ConvertIntToNullableUShortTest()
        {
            foreach (int value in new int[] { 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyIntToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToByteTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableByteTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToCharTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableCharTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToDecimalTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableDecimalTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToDoubleTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableDoubleTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToEnumTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableEnumTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToEnumLongTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableEnumLongTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToFloatTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableFloatTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToIntTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableIntTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToLongTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableLongTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToSByteTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableSByteTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToShortTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableShortTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToUIntTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableUIntTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToULongTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableULongTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToUShortTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableIntToNullableUShortTest()
        {
            foreach (int? value in new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue })
            {
                VerifyNullableIntToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertLongToByteTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToByte(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableByteTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertLongToCharTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToChar(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableCharTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertLongToDecimalTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableDecimalTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertLongToDoubleTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToDouble(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableDoubleTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertLongToEnumTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToEnum(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableEnumTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertLongToEnumLongTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableEnumLongTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertLongToFloatTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToFloat(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableFloatTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertLongToIntTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToInt(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableIntTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertLongToLongTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToLong(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableLongTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertLongToSByteTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToSByte(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableSByteTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertLongToShortTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToShort(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableShortTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertLongToUIntTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToUInt(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableUIntTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertLongToULongTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToULong(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableULongTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertLongToUShortTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToUShort(value);
            }
        }

        [Fact]
        public static void ConvertLongToNullableUShortTest()
        {
            foreach (long value in new long[] { 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyLongToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToByteTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableByteTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToCharTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableCharTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToDecimalTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableDecimalTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToDoubleTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableDoubleTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToEnumTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableEnumTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToEnumLongTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableEnumLongTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToFloatTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableFloatTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToIntTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableIntTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToLongTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableLongTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToSByteTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableSByteTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToShortTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableShortTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToUIntTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableUIntTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToULongTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableULongTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToUShortTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableLongToNullableUShortTest()
        {
            foreach (long? value in new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue })
            {
                VerifyNullableLongToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertSByteToByteTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToByte(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableByteTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertSByteToCharTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToChar(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableCharTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertSByteToDecimalTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableDecimalTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertSByteToDoubleTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToDouble(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableDoubleTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertSByteToEnumTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToEnum(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableEnumTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertSByteToEnumLongTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableEnumLongTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertSByteToFloatTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToFloat(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableFloatTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertSByteToIntTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToInt(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableIntTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertSByteToLongTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToLong(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableLongTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertSByteToSByteTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToSByte(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableSByteTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertSByteToShortTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToShort(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableShortTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertSByteToUIntTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToUInt(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableUIntTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertSByteToULongTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToULong(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableULongTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertSByteToUShortTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToUShort(value);
            }
        }

        [Fact]
        public static void ConvertSByteToNullableUShortTest()
        {
            foreach (sbyte value in new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifySByteToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToByteTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableByteTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToCharTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableCharTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToDecimalTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableDecimalTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToDoubleTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableDoubleTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToEnumTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableEnumTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToEnumLongTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableEnumLongTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToFloatTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableFloatTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToIntTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableIntTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToLongTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableLongTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToSByteTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableSByteTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToShortTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableShortTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToUIntTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableUIntTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToULongTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableULongTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToUShortTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableSByteToNullableUShortTest()
        {
            foreach (sbyte? value in new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue })
            {
                VerifyNullableSByteToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertShortToByteTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToByte(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableByteTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertShortToCharTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToChar(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableCharTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertShortToDecimalTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableDecimalTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertShortToDoubleTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToDouble(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableDoubleTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertShortToEnumTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToEnum(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableEnumTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertShortToEnumLongTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableEnumLongTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertShortToFloatTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToFloat(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableFloatTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertShortToIntTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToInt(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableIntTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertShortToLongTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToLong(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableLongTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertShortToSByteTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToSByte(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableSByteTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertShortToShortTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToShort(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableShortTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertShortToUIntTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToUInt(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableUIntTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertShortToULongTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToULong(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableULongTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertShortToUShortTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToUShort(value);
            }
        }

        [Fact]
        public static void ConvertShortToNullableUShortTest()
        {
            foreach (short value in new short[] { 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyShortToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToByteTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableByteTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToCharTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableCharTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToDecimalTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableDecimalTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToDoubleTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableDoubleTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToEnumTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableEnumTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToEnumLongTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableEnumLongTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToFloatTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableFloatTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToIntTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableIntTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToLongTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableLongTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToSByteTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableSByteTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToShortTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableShortTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToUIntTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableUIntTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToULongTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableULongTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToUShortTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableShortToNullableUShortTest()
        {
            foreach (short? value in new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue })
            {
                VerifyNullableShortToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertUIntToByteTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToByte(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableByteTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertUIntToCharTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToChar(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableCharTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertUIntToDecimalTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableDecimalTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertUIntToDoubleTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToDouble(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableDoubleTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertUIntToEnumTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToEnum(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableEnumTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertUIntToEnumLongTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableEnumLongTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertUIntToFloatTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToFloat(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableFloatTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertUIntToIntTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToInt(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableIntTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertUIntToLongTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToLong(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableLongTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertUIntToSByteTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToSByte(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableSByteTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertUIntToShortTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToShort(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableShortTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertUIntToUIntTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToUInt(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableUIntTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertUIntToULongTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToULong(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableULongTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertUIntToUShortTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToUShort(value);
            }
        }

        [Fact]
        public static void ConvertUIntToNullableUShortTest()
        {
            foreach (uint value in new uint[] { 0, 1, uint.MaxValue })
            {
                VerifyUIntToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToByteTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableByteTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToCharTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableCharTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToDecimalTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableDecimalTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToDoubleTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableDoubleTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToEnumTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableEnumTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToEnumLongTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableEnumLongTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToFloatTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableFloatTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToIntTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableIntTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToLongTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableLongTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToSByteTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableSByteTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToShortTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableShortTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToUIntTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableUIntTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToULongTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableULongTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToUShortTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableUIntToNullableUShortTest()
        {
            foreach (uint? value in new uint?[] { null, 0, 1, uint.MaxValue })
            {
                VerifyNullableUIntToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertULongToByteTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToByte(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableByteTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertULongToCharTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToChar(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableCharTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertULongToDecimalTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableDecimalTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertULongToDoubleTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToDouble(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableDoubleTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertULongToEnumTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToEnum(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableEnumTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertULongToEnumLongTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableEnumLongTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertULongToFloatTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToFloat(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableFloatTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertULongToIntTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToInt(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableIntTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertULongToLongTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToLong(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableLongTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertULongToSByteTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToSByte(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableSByteTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertULongToShortTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToShort(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableShortTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertULongToUIntTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToUInt(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableUIntTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertULongToULongTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToULong(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableULongTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertULongToUShortTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToUShort(value);
            }
        }

        [Fact]
        public static void ConvertULongToNullableUShortTest()
        {
            foreach (ulong value in new ulong[] { 0, 1, ulong.MaxValue })
            {
                VerifyULongToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToByteTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableByteTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToCharTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableCharTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToDecimalTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableDecimalTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToDoubleTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableDoubleTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToEnumTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableEnumTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToEnumLongTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableEnumLongTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToFloatTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableFloatTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToIntTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableIntTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToLongTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableLongTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToSByteTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableSByteTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToShortTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableShortTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToUIntTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableUIntTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToULongTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableULongTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToUShortTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableULongToNullableUShortTest()
        {
            foreach (ulong? value in new ulong?[] { null, 0, 1, ulong.MaxValue })
            {
                VerifyNullableULongToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertUShortToByteTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToByte(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableByteTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertUShortToCharTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToChar(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableCharTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertUShortToDecimalTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableDecimalTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertUShortToDoubleTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToDouble(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableDoubleTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertUShortToEnumTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToEnum(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableEnumTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertUShortToEnumLongTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableEnumLongTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertUShortToFloatTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToFloat(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableFloatTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertUShortToIntTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToInt(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableIntTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertUShortToLongTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToLong(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableLongTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertUShortToSByteTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToSByte(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableSByteTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertUShortToShortTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToShort(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableShortTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertUShortToUIntTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToUInt(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableUIntTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertUShortToULongTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToULong(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableULongTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertUShortToUShortTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToUShort(value);
            }
        }

        [Fact]
        public static void ConvertUShortToNullableUShortTest()
        {
            foreach (ushort value in new ushort[] { 0, 1, ushort.MaxValue })
            {
                VerifyUShortToNullableUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToByteTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableByteTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToCharTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableCharTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableChar(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToDecimalTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableDecimalTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableDecimal(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToDoubleTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableDoubleTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableDouble(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToEnumTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableEnumTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableEnum(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToEnumLongTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableEnumLongTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableEnumLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToFloatTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableFloatTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableFloat(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToIntTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableIntTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToLongTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableLongTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableLong(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToSByteTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableSByteTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableSByte(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToShortTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableShortTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToUIntTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableUIntTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableUInt(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToULongTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableULongTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableULong(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToUShortTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToUShort(value);
            }
        }

        [Fact]
        public static void ConvertNullableUShortToNullableUShortTest()
        {
            foreach (ushort? value in new ushort?[] { null, 0, 1, ushort.MaxValue })
            {
                VerifyNullableUShortToNullableUShort(value);
            }
        }

        #endregion

        #region Test verifiers

        private static void VerifyByteToByte(byte value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToNullableByte(byte value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToChar(byte value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal((char)value, f());
        }

        private static void VerifyByteToNullableChar(byte value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal((char)value, f());
        }

        private static void VerifyByteToDecimal(byte value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToNullableDecimal(byte value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToDouble(byte value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToNullableDouble(byte value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToEnum(byte value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal((E)value, f());
        }

        private static void VerifyByteToNullableEnum(byte value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal((E)value, f());
        }

        private static void VerifyByteToEnumLong(byte value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyByteToNullableEnumLong(byte value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyByteToFloat(byte value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToNullableFloat(byte value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToInt(byte value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToNullableInt(byte value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToLong(byte value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToNullableLong(byte value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToSByte(byte value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyByteToNullableSByte(byte value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyByteToShort(byte value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToNullableShort(byte value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToUInt(byte value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToNullableUInt(byte value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToULong(byte value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToNullableULong(byte value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToUShort(byte value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyByteToNullableUShort(byte value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableByteToByte(byte? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableByte(byte? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableByteToChar(byte? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((char)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableChar(byte? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal((char?)value, f());
        }

        private static void VerifyNullableByteToDecimal(byte? value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableDecimal(byte? value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableByteToDouble(byte? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableDouble(byte? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableByteToEnum(byte? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((E)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableEnum(byte? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal((E?)value, f());
        }

        private static void VerifyNullableByteToEnumLong(byte? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((El)value, f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableEnumLong(byte? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El?)value, f());
        }

        private static void VerifyNullableByteToFloat(byte? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableFloat(byte? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableByteToInt(byte? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableInt(byte? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableByteToLong(byte? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableLong(byte? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableByteToSByte(byte? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((sbyte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableSByte(byte? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)value), f());
        }

        private static void VerifyNullableByteToShort(byte? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableShort(byte? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableByteToUInt(byte? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableUInt(byte? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableByteToULong(byte? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableULong(byte? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableByteToUShort(byte? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableByteToNullableUShort(byte? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(byte?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToByte(char value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyCharToNullableByte(char value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyCharToChar(char value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToNullableChar(char value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToDecimal(char value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToNullableDecimal(char value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToDouble(char value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToNullableDouble(char value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToEnum(char value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal((E)value, f());
        }

        private static void VerifyCharToNullableEnum(char value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal((E)value, f());
        }

        private static void VerifyCharToEnumLong(char value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyCharToNullableEnumLong(char value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyCharToFloat(char value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToNullableFloat(char value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToInt(char value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToNullableInt(char value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToLong(char value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToNullableLong(char value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToSByte(char value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyCharToNullableSByte(char value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyCharToShort(char value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyCharToNullableShort(char value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyCharToUInt(char value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToNullableUInt(char value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToULong(char value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToNullableULong(char value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToUShort(char value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyCharToNullableUShort(char value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableCharToByte(char? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((byte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableByte(char? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)value), f());
        }

        private static void VerifyNullableCharToChar(char? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableChar(char? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableCharToDecimal(char? value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableDecimal(char? value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableCharToDouble(char? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableDouble(char? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableCharToEnum(char? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((E)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableEnum(char? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal((E?)value, f());
        }

        private static void VerifyNullableCharToEnumLong(char? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((El)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableEnumLong(char? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El?)value, f());
        }

        private static void VerifyNullableCharToFloat(char? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableFloat(char? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableCharToInt(char? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableInt(char? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableCharToLong(char? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableLong(char? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableCharToSByte(char? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((sbyte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableSByte(char? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)value), f());
        }

        private static void VerifyNullableCharToShort(char? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((short)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableShort(char? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)value), f());
        }

        private static void VerifyNullableCharToUInt(char? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableUInt(char? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableCharToULong(char? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableULong(char? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableCharToUShort(char? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableCharToNullableUShort(char? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(char?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyDecimalToByte(decimal value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            byte expected = 0;
            try
            {
                expected = (byte)value;
            }
            catch(OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToNullableByte(decimal value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            byte expected = 0;
            try
            {
                expected = (byte)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToChar(decimal value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            char expected = '\0';
            try
            {
                expected = (char)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToNullableChar(decimal value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            char expected = '\0';
            try
            {
                expected = (char)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToDecimal(decimal value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyDecimalToNullableDecimal(decimal value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyDecimalToDouble(decimal value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal((double)value, f());
        }

        private static void VerifyDecimalToNullableDouble(decimal value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal((double)value, f());
        }

        private static void VerifyDecimalToFloat(decimal value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal((float)value, f());
        }

        private static void VerifyDecimalToNullableFloat(decimal value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal((float)value, f());
        }

        private static void VerifyDecimalToInt(decimal value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            int expected = 0;
            try
            {
                expected = (int)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToNullableInt(decimal value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            int expected = 0;
            try
            {
                expected = (int)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToLong(decimal value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            long expected = 0;
            try
            {
                expected = (long)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToNullableLong(decimal value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            long expected = 0;
            try
            {
                expected = (long)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToSByte(decimal value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            sbyte expected = 0;
            try
            {
                expected = (sbyte)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToNullableSByte(decimal value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            sbyte expected = 0;
            try
            {
                expected = (sbyte)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToShort(decimal value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            short expected = 0;
            try
            {
                expected = (short)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToNullableShort(decimal value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            short expected = 0;
            try
            {
                expected = (short)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToUInt(decimal value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            uint expected = 0;
            try
            {
                expected = (uint)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToNullableUInt(decimal value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            uint expected = 0;
            try
            {
                expected = (uint)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToULong(decimal value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            ulong expected = 0;
            try
            {
                expected = (ulong)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToNullableULong(decimal value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            ulong expected = 0;
            try
            {
                expected = (ulong)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToUShort(decimal value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            ushort expected = 0;
            try
            {
                expected = (ushort)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDecimalToNullableUShort(decimal value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            ushort expected = 0;
            try
            {
                expected = (ushort)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyNullableDecimalToByte(decimal? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
            {
                byte expected = 0;
                try
                {
                    expected = (byte)value.GetValueOrDefault();
                }
                catch (OverflowException)
                {
                    Assert.Throws<OverflowException>(() => f());
                    return;
                }

                Assert.Equal(expected, f());
            }
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDecimalToNullableByte(decimal? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            byte? expected = null;
            try
            {
                expected = (byte?)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyNullableDecimalToChar(decimal? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
            {
                char expected = '\0';
                try
                {
                    expected = (char)value.GetValueOrDefault();
                }
                catch (OverflowException)
                {
                    Assert.Throws<OverflowException>(() => f());
                    return;
                }

                Assert.Equal(expected, f());
            }
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDecimalToNullableChar(decimal? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            char? expected = null;
            try
            {
                expected = (char?)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyNullableDecimalToDecimal(decimal? value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDecimalToNullableDecimal(decimal? value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableDecimalToDouble(decimal? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((double)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDecimalToNullableDouble(decimal? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal((double?)value, f());
        }

        private static void VerifyNullableDecimalToFloat(decimal? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((float)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDecimalToNullableFloat(decimal? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal((float?)value, f());
        }

        private static void VerifyNullableDecimalToInt(decimal? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
            {
                int expected = 0;
                try
                {
                    expected = (int)value.GetValueOrDefault();
                }
                catch (OverflowException)
                {
                    Assert.Throws<OverflowException>(() => f());
                    return;
                }

                Assert.Equal(expected, f());
            }
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDecimalToNullableInt(decimal? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            int? expected = null;
            try
            {
                expected = (int?)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyNullableDecimalToLong(decimal? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
            {
                long expected = 0;
                try
                {
                    expected = (long)value.GetValueOrDefault();
                }
                catch (OverflowException)
                {
                    Assert.Throws<OverflowException>(() => f());
                    return;
                }

                Assert.Equal(expected, f());
            }
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDecimalToNullableLong(decimal? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            long? expected = null;
            try
            {
                expected = (long?)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyNullableDecimalToSByte(decimal? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
            {
                sbyte expected = 0;
                try
                {
                    expected = (sbyte)value.GetValueOrDefault();
                }
                catch (OverflowException)
                {
                    Assert.Throws<OverflowException>(() => f());
                    return;
                }

                Assert.Equal(expected, f());
            }
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDecimalToNullableSByte(decimal? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            sbyte? expected = null;
            try
            {
                expected = (sbyte?)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyNullableDecimalToShort(decimal? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
            {
                short expected = 0;
                try
                {
                    expected = (short)value.GetValueOrDefault();
                }
                catch (OverflowException)
                {
                    Assert.Throws<OverflowException>(() => f());
                    return;
                }

                Assert.Equal(expected, f());
            }
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDecimalToNullableShort(decimal? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            short? expected = null;
            try
            {
                expected = (short?)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyNullableDecimalToUInt(decimal? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
            {
                uint expected = 0;
                try
                {
                    expected = (uint)value.GetValueOrDefault();
                }
                catch (OverflowException)
                {
                    Assert.Throws<OverflowException>(() => f());
                    return;
                }

                Assert.Equal(expected, f());
            }
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDecimalToNullableUInt(decimal? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            uint? expected = null;
            try
            {
                expected = (uint?)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyNullableDecimalToULong(decimal? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
            {
                ulong expected = 0;
                try
                {
                    expected = (ulong)value.GetValueOrDefault();
                }
                catch (OverflowException)
                {
                    Assert.Throws<OverflowException>(() => f());
                    return;
                }

                Assert.Equal(expected, f());
            }
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDecimalToNullableULong(decimal? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            ulong? expected = null;
            try
            {
                expected = (ulong?)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyNullableDecimalToUShort(decimal? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
            {
                ushort expected = 0;
                try
                {
                    expected = (ushort)value.GetValueOrDefault();
                }
                catch (OverflowException)
                {
                    Assert.Throws<OverflowException>(() => f());
                    return;
                }

                Assert.Equal(expected, f());
            }
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDecimalToNullableUShort(decimal? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(decimal?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            ushort? expected = null;
            try
            {
                expected = (ushort?)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDoubleToByte(double value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyDoubleToNullableByte(double value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyDoubleToChar(double value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char)value), f());
        }

        private static void VerifyDoubleToNullableChar(double value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyDoubleToDecimal(double value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            decimal expected = 0;
            try
            {
                expected = (decimal)value;
            }
            catch(OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDoubleToNullableDecimal(double value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            decimal expected = 0;
            try
            {
                expected = (decimal)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyDoubleToDouble(double value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyDoubleToNullableDouble(double value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyDoubleToEnum(double value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E)value), f());
        }

        private static void VerifyDoubleToNullableEnum(double value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E)value), f());
        }

        private static void VerifyDoubleToEnumLong(double value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((El)value), f());
        }

        private static void VerifyDoubleToNullableEnumLong(double value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((El)value), f());
        }

        private static void VerifyDoubleToFloat(double value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal((float)value, f());
        }

        private static void VerifyDoubleToNullableFloat(double value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal((float)value, f());
        }

        private static void VerifyDoubleToInt(double value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int)value), f());
        }

        private static void VerifyDoubleToNullableInt(double value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int)value), f());
        }

        private static void VerifyDoubleToLong(double value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((long)value), f());
        }

        private static void VerifyDoubleToNullableLong(double value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((long)value), f());
        }

        private static void VerifyDoubleToSByte(double value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyDoubleToNullableSByte(double value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyDoubleToShort(double value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyDoubleToNullableShort(double value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyDoubleToUInt(double value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyDoubleToNullableUInt(double value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyDoubleToULong(double value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyDoubleToNullableULong(double value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyDoubleToUShort(double value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyDoubleToNullableUShort(double value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyNullableDoubleToByte(double? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((byte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableByte(double? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)value), f());
        }

        private static void VerifyNullableDoubleToChar(double? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((char)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableChar(double? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyNullableDoubleToDecimal(double? value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            if (value.HasValue)
            {
                decimal expected = 0;
                try
                {
                    expected = (decimal)value.GetValueOrDefault();
                }
                catch (OverflowException)
                {
                    Assert.Throws<OverflowException>(() => f());
                    return;
                }

                Assert.Equal(expected, f());
            }
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableDecimal(double? value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            decimal? expected = null;
            try
            {
                expected = (decimal?)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyNullableDoubleToDouble(double? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableDouble(double? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableDoubleToEnum(double? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((E)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableEnum(double? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E?)value), f());
        }

        private static void VerifyNullableDoubleToEnumLong(double? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((El)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableEnumLong(double? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((El?)value), f());
        }

        private static void VerifyNullableDoubleToFloat(double? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((float)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableFloat(double? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal((float?)value, f());
        }

        private static void VerifyNullableDoubleToInt(double? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((int)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableInt(double? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int?)value), f());
        }

        private static void VerifyNullableDoubleToLong(double? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((long)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableLong(double? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((long?)value), f());
        }

        private static void VerifyNullableDoubleToSByte(double? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((sbyte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableSByte(double? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)value), f());
        }

        private static void VerifyNullableDoubleToShort(double? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((short)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableShort(double? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)value), f());
        }

        private static void VerifyNullableDoubleToUInt(double? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((uint)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableUInt(double? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint?)value), f());
        }

        private static void VerifyNullableDoubleToULong(double? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ulong)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableULong(double? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong?)value), f());
        }

        private static void VerifyNullableDoubleToUShort(double? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ushort)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableDoubleToNullableUShort(double? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(double?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)value), f());
        }

        private static void VerifyEnumToByte(E value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyEnumToNullableByte(E value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyEnumToChar(E value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char)value), f());
        }

        private static void VerifyEnumToNullableChar(E value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyEnumToDouble(E value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal((double)value, f());
        }

        private static void VerifyEnumToNullableDouble(E value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal((double)value, f());
        }

        private static void VerifyEnumToEnum(E value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyEnumToNullableEnum(E value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyEnumToEnumLong(E value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyEnumToNullableEnumLong(E value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyEnumToFloat(E value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal((float)value, f());
        }

        private static void VerifyEnumToNullableFloat(E value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal((float)value, f());
        }

        private static void VerifyEnumToInt(E value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal((int)value, f());
        }

        private static void VerifyEnumToNullableInt(E value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal((int)value, f());
        }

        private static void VerifyEnumToLong(E value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal((long)value, f());
        }

        private static void VerifyEnumToNullableLong(E value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal((long)value, f());
        }

        private static void VerifyEnumToSByte(E value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyEnumToNullableSByte(E value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyEnumToShort(E value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyEnumToNullableShort(E value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyEnumToUInt(E value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyEnumToNullableUInt(E value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint?)value), f());
        }

        private static void VerifyEnumToULong(E value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyEnumToNullableULong(E value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyEnumToUShort(E value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyEnumToNullableUShort(E value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyNullableEnumToByte(E? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((byte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableByte(E? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)value), f());
        }

        private static void VerifyNullableEnumToChar(E? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((char)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableChar(E? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyNullableEnumToDouble(E? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((double)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableDouble(E? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal((double?)value, f());
        }

        private static void VerifyNullableEnumToEnum(E? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableEnum(E? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableEnumToEnumLong(E? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((El)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableEnumLong(E? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El?)value, f());
        }

        private static void VerifyNullableEnumToFloat(E? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((float)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableFloat(E? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal((float?)value, f());
        }

        private static void VerifyNullableEnumToInt(E? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((int)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableInt(E? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal((int?)value, f());
        }

        private static void VerifyNullableEnumToLong(E? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((long)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableLong(E? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal((long?)value, f());
        }

        private static void VerifyNullableEnumToSByte(E? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((sbyte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableSByte(E? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)value), f());
        }

        private static void VerifyNullableEnumToShort(E? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((short)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableShort(E? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)value), f());
        }

        private static void VerifyNullableEnumToUInt(E? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((uint)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableUInt(E? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint?)value), f());
        }

        private static void VerifyNullableEnumToULong(E? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ulong)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableULong(E? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong?)value), f());
        }

        private static void VerifyNullableEnumToUShort(E? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ushort)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumToNullableUShort(E? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(E?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)value), f());
        }

        private static void VerifyEnumLongToByte(El value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyEnumLongToNullableByte(El value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyEnumLongToChar(El value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char)value), f());
        }

        private static void VerifyEnumLongToNullableChar(El value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyEnumLongToDouble(El value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal((double)value, f());
        }

        private static void VerifyEnumLongToNullableDouble(El value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal((double)value, f());
        }

        private static void VerifyEnumLongToEnum(El value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E)value), f());
        }

        private static void VerifyEnumLongToNullableEnum(El value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E)value), f());
        }

        private static void VerifyEnumLongToEnumLong(El value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyEnumLongToNullableEnumLong(El value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyEnumLongToFloat(El value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal((float)value, f());
        }

        private static void VerifyEnumLongToNullableFloat(El value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal((float)value, f());
        }

        private static void VerifyEnumLongToInt(El value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int)value), f());
        }

        private static void VerifyEnumLongToNullableInt(El value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int)value), f());
        }

        private static void VerifyEnumLongToLong(El value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal((long)value, f());
        }

        private static void VerifyEnumLongToNullableLong(El value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal((long)value, f());
        }

        private static void VerifyEnumLongToSByte(El value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyEnumLongToNullableSByte(El value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyEnumLongToShort(El value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyEnumLongToNullableShort(El value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyEnumLongToUInt(El value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyEnumLongToNullableUInt(El value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyEnumLongToULong(El value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyEnumLongToNullableULong(El value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyEnumLongToUShort(El value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyEnumLongToNullableUShort(El value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyNullableEnumLongToByte(El? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((byte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableByte(El? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)value), f());
        }

        private static void VerifyNullableEnumLongToChar(El? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((char)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableChar(El? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyNullableEnumLongToDouble(El? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((double)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableDouble(El? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal((double?)value, f());
        }

        private static void VerifyNullableEnumLongToEnum(El? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((E)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableEnum(El? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E?)value), f());
        }

        private static void VerifyNullableEnumLongToEnumLong(El? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableEnumLong(El? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableEnumLongToFloat(El? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((float)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableFloat(El? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal((float?)value, f());
        }

        private static void VerifyNullableEnumLongToInt(El? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((int)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableInt(El? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int?)value), f());
        }

        private static void VerifyNullableEnumLongToLong(El? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((long)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableLong(El? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal((long?)value, f());
        }

        private static void VerifyNullableEnumLongToSByte(El? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((sbyte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableSByte(El? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)value), f());
        }

        private static void VerifyNullableEnumLongToShort(El? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((short)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableShort(El? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)value), f());
        }

        private static void VerifyNullableEnumLongToUInt(El? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((uint)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableUInt(El? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint?)value), f());
        }

        private static void VerifyNullableEnumLongToULong(El? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ulong)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableULong(El? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong?)value), f());
        }

        private static void VerifyNullableEnumLongToUShort(El? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ushort)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableEnumLongToNullableUShort(El? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(El?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)value), f());
        }

        private static void VerifyFloatToByte(float value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyFloatToNullableByte(float value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyFloatToChar(float value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char)value), f());
        }

        private static void VerifyFloatToNullableChar(float value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyFloatToDecimal(float value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            decimal expected = 0;
            try
            {
                expected = (decimal)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyFloatToNullableDecimal(float value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            decimal expected = 0;
            try
            {
                expected = (decimal)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyFloatToDouble(float value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal((double)value, f());
        }

        private static void VerifyFloatToNullableDouble(float value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal((double)value, f());
        }

        private static void VerifyFloatToEnum(float value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E)value), f());
        }

        private static void VerifyFloatToNullableEnum(float value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E)value), f());
        }

        private static void VerifyFloatToEnumLong(float value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((El)value), f());
        }

        private static void VerifyFloatToNullableEnumLong(float value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((El)value), f());
        }

        private static void VerifyFloatToFloat(float value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyFloatToNullableFloat(float value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyFloatToInt(float value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int)value), f());
        }

        private static void VerifyFloatToNullableInt(float value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int)value), f());
        }

        private static void VerifyFloatToLong(float value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((long)value), f());
        }

        private static void VerifyFloatToNullableLong(float value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((long)value), f());
        }

        private static void VerifyFloatToSByte(float value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyFloatToNullableSByte(float value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyFloatToShort(float value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyFloatToNullableShort(float value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyFloatToUInt(float value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyFloatToNullableUInt(float value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyFloatToULong(float value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyFloatToNullableULong(float value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyFloatToUShort(float value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyFloatToNullableUShort(float value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyNullableFloatToByte(float? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((byte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableByte(float? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)value), f());
        }

        private static void VerifyNullableFloatToChar(float? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((char)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableChar(float? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyNullableFloatToDecimal(float? value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            if (value.HasValue)
            {
                decimal expected = 0;
                try
                {
                    expected = (decimal)value.GetValueOrDefault();
                }
                catch (OverflowException)
                {
                    Assert.Throws<OverflowException>(() => f());
                    return;
                }

                Assert.Equal(expected, f());
            }
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableDecimal(float? value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            decimal? expected = null;
            try
            {
                expected = (decimal?)value;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyNullableFloatToDouble(float? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableDouble(float? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableFloatToEnum(float? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((E)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableEnum(float? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E?)value), f());
        }

        private static void VerifyNullableFloatToEnumLong(float? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((El)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableEnumLong(float? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((El?)value), f());
        }

        private static void VerifyNullableFloatToFloat(float? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableFloat(float? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableFloatToInt(float? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((int)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableInt(float? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int?)value), f());
        }

        private static void VerifyNullableFloatToLong(float? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((long)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableLong(float? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((long?)value), f());
        }

        private static void VerifyNullableFloatToSByte(float? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((sbyte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableSByte(float? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)value), f());
        }

        private static void VerifyNullableFloatToShort(float? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((short)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableShort(float? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)value), f());
        }

        private static void VerifyNullableFloatToUInt(float? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((uint)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableUInt(float? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint?)value), f());
        }

        private static void VerifyNullableFloatToULong(float? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ulong)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableULong(float? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong?)value), f());
        }

        private static void VerifyNullableFloatToUShort(float? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ushort)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableFloatToNullableUShort(float? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(float?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)value), f());
        }

        private static void VerifyIntToByte(int value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyIntToNullableByte(int value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyIntToChar(int value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char)value), f());
        }

        private static void VerifyIntToNullableChar(int value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyIntToDecimal(int value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyIntToNullableDecimal(int value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyIntToDouble(int value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyIntToNullableDouble(int value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyIntToEnum(int value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal((E)value, f());
        }

        private static void VerifyIntToNullableEnum(int value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal((E)value, f());
        }

        private static void VerifyIntToEnumLong(int value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyIntToNullableEnumLong(int value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyIntToFloat(int value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyIntToNullableFloat(int value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyIntToInt(int value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyIntToNullableInt(int value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyIntToLong(int value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyIntToNullableLong(int value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyIntToSByte(int value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyIntToNullableSByte(int value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyIntToShort(int value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyIntToNullableShort(int value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyIntToUInt(int value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyIntToNullableUInt(int value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyIntToULong(int value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyIntToNullableULong(int value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyIntToUShort(int value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyIntToNullableUShort(int value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyNullableIntToByte(int? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((byte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableByte(int? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)value), f());
        }

        private static void VerifyNullableIntToChar(int? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((char)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableChar(int? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyNullableIntToDecimal(int? value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableDecimal(int? value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableIntToDouble(int? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableDouble(int? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableIntToEnum(int? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((E)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableEnum(int? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal((E?)value, f());
        }

        private static void VerifyNullableIntToEnumLong(int? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((El)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableEnumLong(int? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El?)value, f());
        }

        private static void VerifyNullableIntToFloat(int? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableFloat(int? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableIntToInt(int? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableInt(int? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableIntToLong(int? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableLong(int? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableIntToSByte(int? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((sbyte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableSByte(int? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)value), f());
        }

        private static void VerifyNullableIntToShort(int? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((short)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableShort(int? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)value), f());
        }

        private static void VerifyNullableIntToUInt(int? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((uint)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableUInt(int? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint?)value), f());
        }

        private static void VerifyNullableIntToULong(int? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ulong)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableULong(int? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong?)value), f());
        }

        private static void VerifyNullableIntToUShort(int? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ushort)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableIntToNullableUShort(int? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(int?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)value), f());
        }

        private static void VerifyLongToByte(long value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyLongToNullableByte(long value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyLongToChar(long value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char)value), f());
        }

        private static void VerifyLongToNullableChar(long value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyLongToDecimal(long value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyLongToNullableDecimal(long value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyLongToDouble(long value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyLongToNullableDouble(long value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyLongToEnum(long value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E)value), f());
        }

        private static void VerifyLongToNullableEnum(long value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E)value), f());
        }

        private static void VerifyLongToEnumLong(long value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyLongToNullableEnumLong(long value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyLongToFloat(long value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyLongToNullableFloat(long value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyLongToInt(long value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int)value), f());
        }

        private static void VerifyLongToNullableInt(long value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int)value), f());
        }

        private static void VerifyLongToLong(long value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyLongToNullableLong(long value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyLongToSByte(long value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyLongToNullableSByte(long value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyLongToShort(long value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyLongToNullableShort(long value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyLongToUInt(long value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyLongToNullableUInt(long value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyLongToULong(long value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyLongToNullableULong(long value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyLongToUShort(long value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyLongToNullableUShort(long value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyNullableLongToByte(long? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((byte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableByte(long? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)value), f());
        }

        private static void VerifyNullableLongToChar(long? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((char)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableChar(long? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyNullableLongToDecimal(long? value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableDecimal(long? value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableLongToDouble(long? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableDouble(long? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableLongToEnum(long? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((E)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableEnum(long? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E?)value), f());
        }

        private static void VerifyNullableLongToEnumLong(long? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((El)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableEnumLong(long? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El?)value, f());
        }

        private static void VerifyNullableLongToFloat(long? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableFloat(long? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableLongToInt(long? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((int)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableInt(long? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int?)value), f());
        }

        private static void VerifyNullableLongToLong(long? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableLong(long? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableLongToSByte(long? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((sbyte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableSByte(long? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)value), f());
        }

        private static void VerifyNullableLongToShort(long? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((short)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableShort(long? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)value), f());
        }

        private static void VerifyNullableLongToUInt(long? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((uint)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableUInt(long? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint?)value), f());
        }

        private static void VerifyNullableLongToULong(long? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ulong)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableULong(long? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong?)value), f());
        }

        private static void VerifyNullableLongToUShort(long? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ushort)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableLongToNullableUShort(long? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(long?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)value), f());
        }

        private static void VerifySByteToByte(sbyte value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifySByteToNullableByte(sbyte value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifySByteToChar(sbyte value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char)value), f());
        }

        private static void VerifySByteToNullableChar(sbyte value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char)value), f());
        }

        private static void VerifySByteToDecimal(sbyte value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToNullableDecimal(sbyte value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToDouble(sbyte value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToNullableDouble(sbyte value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToEnum(sbyte value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal((E)value, f());
        }

        private static void VerifySByteToNullableEnum(sbyte value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal((E)value, f());
        }

        private static void VerifySByteToEnumLong(sbyte value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifySByteToNullableEnumLong(sbyte value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifySByteToFloat(sbyte value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToNullableFloat(sbyte value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToInt(sbyte value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToNullableInt(sbyte value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToLong(sbyte value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToNullableLong(sbyte value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToSByte(sbyte value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToNullableSByte(sbyte value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToShort(sbyte value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToNullableShort(sbyte value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifySByteToUInt(sbyte value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifySByteToNullableUInt(sbyte value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifySByteToULong(sbyte value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifySByteToNullableULong(sbyte value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifySByteToUShort(sbyte value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifySByteToNullableUShort(sbyte value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyNullableSByteToByte(sbyte? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((byte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableByte(sbyte? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)value), f());
        }

        private static void VerifyNullableSByteToChar(sbyte? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((char)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableChar(sbyte? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyNullableSByteToDecimal(sbyte? value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableDecimal(sbyte? value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableSByteToDouble(sbyte? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableDouble(sbyte? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableSByteToEnum(sbyte? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((E)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableEnum(sbyte? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal((E?)value, f());
        }

        private static void VerifyNullableSByteToEnumLong(sbyte? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((El)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableEnumLong(sbyte? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El?)value, f());
        }

        private static void VerifyNullableSByteToFloat(sbyte? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableFloat(sbyte? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableSByteToInt(sbyte? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableInt(sbyte? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableSByteToLong(sbyte? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableLong(sbyte? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableSByteToSByte(sbyte? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableSByte(sbyte? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableSByteToShort(sbyte? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableShort(sbyte? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableSByteToUInt(sbyte? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((uint)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableUInt(sbyte? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint?)value), f());
        }

        private static void VerifyNullableSByteToULong(sbyte? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ulong)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableULong(sbyte? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong?)value), f());
        }

        private static void VerifyNullableSByteToUShort(sbyte? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ushort)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableSByteToNullableUShort(sbyte? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(sbyte?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)value), f());
        }

        private static void VerifyShortToByte(short value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyShortToNullableByte(short value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyShortToChar(short value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char)value), f());
        }

        private static void VerifyShortToNullableChar(short value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyShortToDecimal(short value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyShortToNullableDecimal(short value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyShortToDouble(short value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyShortToNullableDouble(short value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyShortToEnum(short value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal((E)value, f());
        }

        private static void VerifyShortToNullableEnum(short value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal((E)value, f());
        }

        private static void VerifyShortToEnumLong(short value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyShortToNullableEnumLong(short value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyShortToFloat(short value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyShortToNullableFloat(short value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyShortToInt(short value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyShortToNullableInt(short value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyShortToLong(short value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyShortToNullableLong(short value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyShortToSByte(short value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyShortToNullableSByte(short value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyShortToShort(short value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyShortToNullableShort(short value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyShortToUInt(short value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyShortToNullableUInt(short value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyShortToULong(short value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyShortToNullableULong(short value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong)value), f());
        }

        private static void VerifyShortToUShort(short value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyShortToNullableUShort(short value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyNullableShortToByte(short? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((byte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableByte(short? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)value), f());
        }

        private static void VerifyNullableShortToChar(short? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((char)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableChar(short? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyNullableShortToDecimal(short? value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableDecimal(short? value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableShortToDouble(short? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableDouble(short? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableShortToEnum(short? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((E)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableEnum(short? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal((E?)value, f());
        }

        private static void VerifyNullableShortToEnumLong(short? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((El)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableEnumLong(short? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El?)value, f());
        }

        private static void VerifyNullableShortToFloat(short? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableFloat(short? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableShortToInt(short? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableInt(short? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableShortToLong(short? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableLong(short? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableShortToSByte(short? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((sbyte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableSByte(short? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)value), f());
        }

        private static void VerifyNullableShortToShort(short? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableShort(short? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableShortToUInt(short? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((uint)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableUInt(short? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint?)value), f());
        }

        private static void VerifyNullableShortToULong(short? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ulong)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableULong(short? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ulong?)value), f());
        }

        private static void VerifyNullableShortToUShort(short? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ushort)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableShortToNullableUShort(short? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(short?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)value), f());
        }

        private static void VerifyUIntToByte(uint value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyUIntToNullableByte(uint value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyUIntToChar(uint value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char)value), f());
        }

        private static void VerifyUIntToNullableChar(uint value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyUIntToDecimal(uint value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUIntToNullableDecimal(uint value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUIntToDouble(uint value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUIntToNullableDouble(uint value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUIntToEnum(uint value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E)value), f());
        }

        private static void VerifyUIntToNullableEnum(uint value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E)value), f());
        }

        private static void VerifyUIntToEnumLong(uint value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyUIntToNullableEnumLong(uint value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyUIntToFloat(uint value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUIntToNullableFloat(uint value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUIntToInt(uint value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int)value), f());
        }

        private static void VerifyUIntToNullableInt(uint value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int)value), f());
        }

        private static void VerifyUIntToLong(uint value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUIntToNullableLong(uint value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUIntToSByte(uint value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyUIntToNullableSByte(uint value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyUIntToShort(uint value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyUIntToNullableShort(uint value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyUIntToUInt(uint value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUIntToNullableUInt(uint value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUIntToULong(uint value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUIntToNullableULong(uint value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUIntToUShort(uint value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyUIntToNullableUShort(uint value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyNullableUIntToByte(uint? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((byte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableByte(uint? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)value), f());
        }

        private static void VerifyNullableUIntToChar(uint? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((char)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableChar(uint? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyNullableUIntToDecimal(uint? value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableDecimal(uint? value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUIntToDouble(uint? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableDouble(uint? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUIntToEnum(uint? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((E)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableEnum(uint? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E?)value), f());
        }

        private static void VerifyNullableUIntToEnumLong(uint? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((El)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableEnumLong(uint? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El?)value, f());
        }

        private static void VerifyNullableUIntToFloat(uint? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableFloat(uint? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUIntToInt(uint? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((int)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableInt(uint? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int?)value), f());
        }

        private static void VerifyNullableUIntToLong(uint? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableLong(uint? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal((long?)value, f());
        }

        private static void VerifyNullableUIntToSByte(uint? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((sbyte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableSByte(uint? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)value), f());
        }

        private static void VerifyNullableUIntToShort(uint? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((short)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableShort(uint? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)value), f());
        }

        private static void VerifyNullableUIntToUInt(uint? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableUInt(uint? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUIntToULong(uint? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableULong(uint? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUIntToUShort(uint? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ushort)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUIntToNullableUShort(uint? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(uint?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)value), f());
        }

        private static void VerifyULongToByte(ulong value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyULongToNullableByte(ulong value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyULongToChar(ulong value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char)value), f());
        }

        private static void VerifyULongToNullableChar(ulong value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyULongToDecimal(ulong value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyULongToNullableDecimal(ulong value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyULongToDouble(ulong value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyULongToNullableDouble(ulong value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyULongToEnum(ulong value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E)value), f());
        }

        private static void VerifyULongToNullableEnum(ulong value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E)value), f());
        }

        private static void VerifyULongToEnumLong(ulong value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((El)value), f());
        }

        private static void VerifyULongToNullableEnumLong(ulong value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((El)value), f());
        }

        private static void VerifyULongToFloat(ulong value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyULongToNullableFloat(ulong value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyULongToInt(ulong value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int)value), f());
        }

        private static void VerifyULongToNullableInt(ulong value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int)value), f());
        }

        private static void VerifyULongToLong(ulong value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((long)value), f());
        }

        private static void VerifyULongToNullableLong(ulong value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((long)value), f());
        }

        private static void VerifyULongToSByte(ulong value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyULongToNullableSByte(ulong value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyULongToShort(ulong value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyULongToNullableShort(ulong value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyULongToUInt(ulong value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyULongToNullableUInt(ulong value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint)value), f());
        }

        private static void VerifyULongToULong(ulong value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyULongToNullableULong(ulong value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyULongToUShort(ulong value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyULongToNullableUShort(ulong value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)value), f());
        }

        private static void VerifyNullableULongToByte(ulong? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((byte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableByte(ulong? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)value), f());
        }

        private static void VerifyNullableULongToChar(ulong? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((char)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableChar(ulong? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)value), f());
        }

        private static void VerifyNullableULongToDecimal(ulong? value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableDecimal(ulong? value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableULongToDouble(ulong? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableDouble(ulong? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableULongToEnum(ulong? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((E)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableEnum(ulong? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((E?)value), f());
        }

        private static void VerifyNullableULongToEnumLong(ulong? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((El)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableEnumLong(ulong? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((El?)value), f());
        }

        private static void VerifyNullableULongToFloat(ulong? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableFloat(ulong? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableULongToInt(ulong? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((int)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableInt(ulong? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((int?)value), f());
        }

        private static void VerifyNullableULongToLong(ulong? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((long)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableLong(ulong? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((long?)value), f());
        }

        private static void VerifyNullableULongToSByte(ulong? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((sbyte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableSByte(ulong? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)value), f());
        }

        private static void VerifyNullableULongToShort(ulong? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((short)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableShort(ulong? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)value), f());
        }

        private static void VerifyNullableULongToUInt(ulong? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((uint)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableUInt(ulong? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((uint?)value), f());
        }

        private static void VerifyNullableULongToULong(ulong? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableULong(ulong? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableULongToUShort(ulong? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((ushort)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableULongToNullableUShort(ulong? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ulong?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)value), f());
        }

        private static void VerifyUShortToByte(ushort value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyUShortToNullableByte(ushort value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)value), f());
        }

        private static void VerifyUShortToChar(ushort value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            Assert.Equal((char)value, f());
        }

        private static void VerifyUShortToNullableChar(ushort value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal((char?)value, f());
        }

        private static void VerifyUShortToDecimal(ushort value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToNullableDecimal(ushort value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToDouble(ushort value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToNullableDouble(ushort value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToEnum(ushort value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            Assert.Equal((E)value, f());
        }

        private static void VerifyUShortToNullableEnum(ushort value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal((E)value, f());
        }

        private static void VerifyUShortToEnumLong(ushort value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyUShortToNullableEnumLong(ushort value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El)value, f());
        }

        private static void VerifyUShortToFloat(ushort value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToNullableFloat(ushort value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToInt(ushort value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToNullableInt(ushort value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToLong(ushort value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToNullableLong(ushort value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToSByte(ushort value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyUShortToNullableSByte(ushort value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)value), f());
        }

        private static void VerifyUShortToShort(ushort value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyUShortToNullableShort(ushort value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)value), f());
        }

        private static void VerifyUShortToUInt(ushort value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToNullableUInt(ushort value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToULong(ushort value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToNullableULong(ushort value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToUShort(ushort value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyUShortToNullableUShort(ushort value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUShortToByte(ushort? value)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(byte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((byte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableByte(ushort? value)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(byte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)value), f());
        }

        private static void VerifyNullableUShortToChar(ushort? value)
        {
            Expression<Func<char>> e =
                Expression.Lambda<Func<char>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(char)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((char)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableChar(ushort? value)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(char?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal((char?)value, f());
        }

        private static void VerifyNullableUShortToDecimal(ushort? value)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(decimal)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableDecimal(ushort? value)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(decimal?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUShortToDouble(ushort? value)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(double)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableDouble(ushort? value)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(double?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUShortToEnum(ushort? value)
        {
            Expression<Func<E>> e =
                Expression.Lambda<Func<E>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(E)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((E)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableEnum(ushort? value)
        {
            Expression<Func<E?>> e =
                Expression.Lambda<Func<E?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(E?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<E?> f = e.CompileToTestMethod();

            Assert.Equal((E?)value, f());
        }

        private static void VerifyNullableUShortToEnumLong(ushort? value)
        {
            Expression<Func<El>> e =
                Expression.Lambda<Func<El>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(El)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal((El)value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableEnumLong(ushort? value)
        {
            Expression<Func<El?>> e =
                Expression.Lambda<Func<El?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(El?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<El?> f = e.CompileToTestMethod();

            Assert.Equal((El?)value, f());
        }

        private static void VerifyNullableUShortToFloat(ushort? value)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(float)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableFloat(ushort? value)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(float?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUShortToInt(ushort? value)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(int)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableInt(ushort? value)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(int?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUShortToLong(ushort? value)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(long)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableLong(ushort? value)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(long?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUShortToSByte(ushort? value)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(sbyte)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((sbyte)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableSByte(ushort? value)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(sbyte?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)value), f());
        }

        private static void VerifyNullableUShortToShort(ushort? value)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(short)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(unchecked((short)value.GetValueOrDefault()), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableShort(ushort? value)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(short?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)value), f());
        }

        private static void VerifyNullableUShortToUInt(ushort? value)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(uint)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableUInt(ushort? value)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(uint?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUShortToULong(ushort? value)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(ulong)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value.GetValueOrDefault(), f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableULong(ushort? value)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(ulong?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        private static void VerifyNullableUShortToUShort(ushort? value)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(ushort)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            if (value.HasValue)
                Assert.Equal(value, f());
            else
                Assert.Throws<InvalidOperationException>(() => f());
        }

        private static void VerifyNullableUShortToNullableUShort(ushort? value)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Convert(Expression.Constant(value, typeof(ushort?)), typeof(ushort?)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(value, f());
        }

        #endregion

        public class PerverselyNamedMembers
        {
            public readonly uint Field;

            public PerverselyNamedMembers(uint value)
            {
                Field = value;
            }

            public static uint op_Implicit()
            {
                return 0x8BADF00D;
            }
        }

        public struct HalfLiftedTo
        {
        }

        public struct ImplicitHalfLiftedFrom
        {
            public bool NullEquiv { get; set; }

            public static implicit operator HalfLiftedTo? (ImplicitHalfLiftedFrom source) =>
                source.NullEquiv ? default(HalfLiftedTo?) : new HalfLiftedTo();
        }

        public struct ExplicitHalfLiftedFrom
        {
            public bool NullEquiv { get; set; }

            public static explicit operator HalfLiftedTo? (ExplicitHalfLiftedFrom source) =>
                source.NullEquiv ? default(HalfLiftedTo?) : new HalfLiftedTo();
        }

        public struct ImplicitHalfLiftedOverloaded
        {
            public static implicit operator HalfLiftedTo?(ImplicitHalfLiftedOverloaded source) => new HalfLiftedTo();

            public static implicit operator HalfLiftedTo?(ImplicitHalfLiftedOverloaded? source) => new HalfLiftedTo();
        }

        public struct ImplicitHalfLiftedFromReverse
        {
            public static implicit operator HalfLiftedTo(ImplicitHalfLiftedFromReverse? source) => new HalfLiftedTo();
        }

        public struct HalfLiftedToTargetOperator
        {
            public static implicit operator HalfLiftedToTargetOperator?(HalfLiftedFromTargetOperator source) =>
                source.NullEquiv ? default(HalfLiftedToTargetOperator?) : new HalfLiftedToTargetOperator();
        }

        public struct HalfLiftedFromTargetOperator
        {
            public bool NullEquiv { get; set; }
        }

#if NETCOREAPP1_1
        [Fact(Skip = "problems with nullable converts (fails at Expression.Convert)")]
#else
        [Fact]
#endif
        public static void ImplicitHalfLiftedConversionFromCSCompiler()
        {
            Expression<Func<ImplicitHalfLiftedFrom?, HalfLiftedTo?>> e = x => x;
            Func<ImplicitHalfLiftedFrom?, HalfLiftedTo?> f = e.CompileToTestMethod();
            Assert.NotNull(f(new ImplicitHalfLiftedFrom()));
            Assert.Null(f(new ImplicitHalfLiftedFrom { NullEquiv = true }));
            Assert.Null(f(null));
        }

#if NETCOREAPP1_1
        [Fact(Skip = "problems with nullable converts (fails at Expression.Convert)")]
#else
        [Fact]
#endif
        public static void ExplicitHalfLiftedConversionFromCSCompiler()
        {
            Expression<Func<ExplicitHalfLiftedFrom?, HalfLiftedTo?>> e = x => (HalfLiftedTo?)x;
            Func<ExplicitHalfLiftedFrom?, HalfLiftedTo?> f = e.CompileToTestMethod();
            Assert.NotNull(f(new ExplicitHalfLiftedFrom()));
            Assert.Null(f(new ExplicitHalfLiftedFrom { NullEquiv = true }));
            Assert.Null(f(null));
        }

        [Fact]
        public static void ImplicitHalfLiftedOverloadedConversionFromCSCompiler()
        {
            Expression<Func<ImplicitHalfLiftedOverloaded?, HalfLiftedTo?>> e = x => x;
            Func<ImplicitHalfLiftedOverloaded?, HalfLiftedTo?> f = e.CompileToTestMethod();
            Assert.NotNull(f(new ImplicitHalfLiftedOverloaded()));
            Assert.NotNull(f(null));
        }

#if NETCOREAPP1_1
        [Fact(Skip = "problems with nullable converts (fails at Expression.Convert)")]
#else
        [Fact]
#endif
        public static void ImplicitHalfLiftedConversion()
        {
            ParameterExpression x = Expression.Parameter(typeof(ImplicitHalfLiftedFrom?));
            Expression<Func<ImplicitHalfLiftedFrom?, HalfLiftedTo?>> e =
                Expression.Lambda<Func<ImplicitHalfLiftedFrom?, HalfLiftedTo?>>(
                    Expression.Convert(x, typeof(HalfLiftedTo?)),
                    x);
            Func<ImplicitHalfLiftedFrom?, HalfLiftedTo?> f = e.CompileToTestMethod();
            Assert.NotNull(f(new ImplicitHalfLiftedFrom()));
            Assert.Null(f(new ImplicitHalfLiftedFrom { NullEquiv = true }));
            Assert.Null(f(null));
        }

#if NETCOREAPP1_1
        [Fact(Skip = "problems with nullable converts (fails at Expression.Convert)")]
#else
        [Fact]
#endif
        public static void ExplicitHalfLiftedConversion()
        {
            ParameterExpression x = Expression.Parameter(typeof(ExplicitHalfLiftedFrom?));
            Expression<Func<ExplicitHalfLiftedFrom?, HalfLiftedTo?>> e =
                Expression.Lambda<Func<ExplicitHalfLiftedFrom?, HalfLiftedTo?>>(
                    Expression.Convert(x, typeof(HalfLiftedTo?)),
                    x);
            Func<ExplicitHalfLiftedFrom?, HalfLiftedTo?> f = e.CompileToTestMethod();
            Assert.NotNull(f(new ExplicitHalfLiftedFrom()));
            Assert.Null(f(new ExplicitHalfLiftedFrom { NullEquiv = true }));
            Assert.Null(f(null));
        }

        [Fact]
        public static void ImplicitHalfLiftedOverloadedConversion()
        {
            ParameterExpression x = Expression.Parameter(typeof(ImplicitHalfLiftedOverloaded?));
            Expression<Func<ImplicitHalfLiftedOverloaded?, HalfLiftedTo?>> e =
                Expression.Lambda<Func<ImplicitHalfLiftedOverloaded?, HalfLiftedTo?>>(
                    Expression.Convert(x, typeof(HalfLiftedTo?)),
                    x);
            Func<ImplicitHalfLiftedOverloaded?, HalfLiftedTo?> f = e.CompileToTestMethod();
            Assert.NotNull(f(new ImplicitHalfLiftedOverloaded()));
            Assert.NotNull(f(null));
        }

#if NETCOREAPP1_1
        [Fact(Skip = "problems with nullable converts (fails at Expression.Convert)")]
#else
        [Fact]
#endif
        public static void ImplicitHalfLiftedConversionExplicitlySetMethod()
        {
            ParameterExpression x = Expression.Parameter(typeof(ImplicitHalfLiftedFrom?));
            Expression<Func<ImplicitHalfLiftedFrom?, HalfLiftedTo?>> e =
                Expression.Lambda<Func<ImplicitHalfLiftedFrom?, HalfLiftedTo?>>(
                    Expression.Convert(x, typeof(HalfLiftedTo?), typeof(ImplicitHalfLiftedFrom).GetMethod("op_Implicit")),
                    x);
            Func<ImplicitHalfLiftedFrom?, HalfLiftedTo?> f = e.CompileToTestMethod();
            Assert.NotNull(f(new ImplicitHalfLiftedFrom()));
            Assert.Null(f(new ImplicitHalfLiftedFrom { NullEquiv = true }));
            Assert.Null(f(null));
        }

#if NETCOREAPP1_1
        [Fact(Skip = "problems with nullable converts (fails at Expression.Convert)")]
#else
        [Fact]
#endif
        public static void ExplicitHalfLiftedConversionExplicitlySetMethod()
        {
            ParameterExpression x = Expression.Parameter(typeof(ExplicitHalfLiftedFrom?));
            Expression<Func<ExplicitHalfLiftedFrom?, HalfLiftedTo?>> e =
                Expression.Lambda<Func<ExplicitHalfLiftedFrom?, HalfLiftedTo?>>(
                    Expression.Convert(x, typeof(HalfLiftedTo?), typeof(ExplicitHalfLiftedFrom).GetMethod("op_Explicit")),
                    x);
            Func<ExplicitHalfLiftedFrom?, HalfLiftedTo?> f = e.CompileToTestMethod();
            Assert.NotNull(f(new ExplicitHalfLiftedFrom()));
            Assert.Null(f(new ExplicitHalfLiftedFrom { NullEquiv = true }));
            Assert.Null(f(null));
        }

#if NETCOREAPP1_1
        [Fact(Skip = "problems with nullable converts (fails at Expression.Convert)")]
#else
        [Fact]
#endif
        public static void ImplicitHalfLiftedOverloadedConversionExplicitlySetMethod()
        {
            List<MethodInfo> opMethods =
                typeof(ImplicitHalfLiftedOverloaded).GetMethods().Where(m => m.Name == "op_Implicit").ToList();
            MethodInfo direct =
                opMethods.First(m => m.GetParameters()[0].ParameterType == typeof(ImplicitHalfLiftedOverloaded?));
            MethodInfo liftNeeded =
                opMethods.First(m => m.GetParameters()[0].ParameterType == typeof(ImplicitHalfLiftedOverloaded));
            ParameterExpression x = Expression.Parameter(typeof(ImplicitHalfLiftedOverloaded?));
            Expression<Func<ImplicitHalfLiftedOverloaded?, HalfLiftedTo?>> e =
                Expression.Lambda<Func<ImplicitHalfLiftedOverloaded?, HalfLiftedTo?>>(
                    Expression.Convert(x, typeof(HalfLiftedTo?), direct),
                    x);
            Func<ImplicitHalfLiftedOverloaded?, HalfLiftedTo?> f = e.CompileToTestMethod();
            Assert.NotNull(f(new ImplicitHalfLiftedOverloaded()));
            Assert.NotNull(f(null));
            e = Expression.Lambda<Func<ImplicitHalfLiftedOverloaded?, HalfLiftedTo?>>(
                Expression.Convert(x, typeof(HalfLiftedTo?), liftNeeded), x);
            f = e.CompileToTestMethod();
            Assert.NotNull(f(new ImplicitHalfLiftedOverloaded()));
            Assert.Null(f(null));
        }

#if NETCOREAPP1_1
        [Fact(Skip = "problems with nullable converts (fails at Expression.Convert)")]
#else
        [Fact]
#endif
        public static void ImplicitHalfLiftedConversionOpOnTargetFromCSCompiler()
        {
            Expression<Func<HalfLiftedFromTargetOperator?, HalfLiftedToTargetOperator?>> e = x => x;
            Func<HalfLiftedFromTargetOperator?, HalfLiftedToTargetOperator?> f = e.CompileToTestMethod();
            Assert.NotNull(f(new HalfLiftedFromTargetOperator()));
            Assert.Null(f(new HalfLiftedFromTargetOperator { NullEquiv = true }));
            Assert.Null(f(null));
        }

#if NETCOREAPP1_1
        [Fact(Skip = "problems with nullable converts (fails at Expression.Convert)")]
#else
        [Fact]
#endif
        public static void ImplicitHalfLiftedConversionOpOnTarget()
        {
            ParameterExpression x = Expression.Parameter(typeof(HalfLiftedFromTargetOperator?));
            Expression<Func<HalfLiftedFromTargetOperator?, HalfLiftedToTargetOperator?>> e =
                Expression.Lambda<Func<HalfLiftedFromTargetOperator?, HalfLiftedToTargetOperator?>>(
                    Expression.Convert(x, typeof(HalfLiftedToTargetOperator?)),
                    x);
            Func<HalfLiftedFromTargetOperator?, HalfLiftedToTargetOperator?> f = e.CompileToTestMethod();
            Assert.NotNull(f(new HalfLiftedFromTargetOperator()));
            Assert.Null(f(new HalfLiftedFromTargetOperator { NullEquiv = true }));
            Assert.Null(f(null));
        }

        [Fact]
        public static void ExplicitOpImplicit()
        {
            Assert.Throws<InvalidOperationException>(() => Expression.Convert(Expression.Constant(new PerverselyNamedMembers(0)), typeof(uint)));
        }

        [Fact]
        public static void OpenGenericnType()
        {
            AssertExtensions.Throws<ArgumentException>("type", () => Expression.Convert(Expression.Constant(null), typeof(List<>)));
        }

        [Fact]
        public static void TypeContainingGenericParameters()
        {
            AssertExtensions.Throws<ArgumentException>("type", () => Expression.Convert(Expression.Constant(null), typeof(List<>.Enumerator)));
            AssertExtensions.Throws<ArgumentException>("type", () => Expression.Convert(Expression.Constant(null), typeof(List<>).MakeGenericType(typeof(List<>))));
        }

        [Fact]
        public static void ByRefType()
        {
            AssertExtensions.Throws<ArgumentException>("type", () => Expression.Convert(Expression.Constant(null), typeof(object).MakeByRefType()));
        }

        [Fact]
        public static void PointerType()
        {
            AssertExtensions.Throws<ArgumentException>("type", () => Expression.Convert(Expression.Constant(null), typeof(int*)));
        }

        public static IEnumerable<object[]> Conversions()
        {
            yield return new object[] { 3, 3 };
            yield return new object[] { (byte)3, 3 };
            yield return new object[] { 3, 3.0 };
            yield return new object[] { 3.0, 3 };
            yield return new object[] { 12345678, (short)24910 };
        }

        [Theory, MemberData(nameof(Conversions))]
        public static void ConvertMakeUnary(object source, object result)
        {
            LambdaExpression lambda = Expression.Lambda(
                Expression.MakeUnary(ExpressionType.Convert, Expression.Constant(source), result.GetType())
                );
            Delegate del = lambda.CompileToTestMethod();
            Assert.Equal(result, del.DynamicInvoke());
        }

        public class CustomConversions
        {
            public int Value { get; set; }

            public static int ConvertToInt(CustomConversions cc) => cc.Value;

            public static CustomConversions ConvertFromInt(int x) => new CustomConversions {Value = x};

            public static CustomConversions ConvertFromRefInt(ref int x) => new CustomConversions { Value = x++ };

            public static void DoNothing(CustomConversions cc)
            {
            }

            public static CustomConversions Create() => new CustomConversions();

            public static CustomConversions FromAddition(int x, int y) => new CustomConversions {Value = x + y};
        }

        [Fact]
        public static void CustomConversionNotStandardNameTo()
        {
            Expression operand = Expression.Constant(new CustomConversions { Value = 9 });
            MethodInfo method = typeof(CustomConversions).GetMethod(nameof(CustomConversions.ConvertToInt));
            Expression<Func<int>> lambda = Expression.Lambda<Func<int>>(
                Expression.Convert(operand, typeof(int), method));
            Func<int> func = lambda.CompileToTestMethod();
            Assert.Equal(9, func());
        }

        [Fact]
        public static void CustomConversionNotStandardNameFrom()
        {
            Expression operand = Expression.Constant(4);
            MethodInfo method = typeof(CustomConversions).GetMethod(nameof(CustomConversions.ConvertFromInt));
            Expression<Func<CustomConversions>> lambda = Expression.Lambda<Func<CustomConversions>>(
                Expression.Convert(operand, typeof(CustomConversions), method));
            Func<CustomConversions> func = lambda.CompileToTestMethod();
            Assert.Equal(4, func().Value);
        }

        [Fact]
        public static void CustomConversionNotStandardNameFromLifted()
        {
            Expression operand = Expression.Constant(4, typeof(int?));
            MethodInfo method = typeof(CustomConversions).GetMethod(nameof(CustomConversions.ConvertFromInt));
            Expression<Func<CustomConversions>> lambda = Expression.Lambda<Func<CustomConversions>>(
                Expression.Convert(operand, typeof(CustomConversions), method));
            Func<CustomConversions> func = lambda.CompileToTestMethod();
            Assert.Equal(4, func().Value);
        }

        [Fact]
        public static void CustomConversionNotStandardNameFromLiftedNullOperand()
        {
            Expression operand = Expression.Constant(null, typeof(int?));
            MethodInfo method = typeof(CustomConversions).GetMethod(nameof(CustomConversions.ConvertFromInt));
            Expression<Func<CustomConversions>> lambda = Expression.Lambda<Func<CustomConversions>>(
                Expression.Convert(operand, typeof(CustomConversions), method));
            Func<CustomConversions> func = lambda.CompileToTestMethod();
            Assert.Throws<InvalidOperationException>(() => func());
        }

        public delegate TResult ByRefFunc<T, TResult>(ref T arg);

        [Fact]
        public static void CustomConversionNotStandardNameFromLiftedByRef()
        {
            var param = Expression.Parameter(typeof(int?).MakeByRefType());
            MethodInfo method = typeof(CustomConversions).GetMethod(nameof(CustomConversions.ConvertFromRefInt));
            Expression<ByRefFunc<int?, CustomConversions>> lambda = Expression.Lambda<ByRefFunc<int?, CustomConversions>>(
                Expression.Convert(param, typeof(CustomConversions), method), param);
            ByRefFunc<int?, CustomConversions> func = lambda.CompileToTestMethod();
            int? x = 5;
            Assert.Equal(5, func(ref x).Value);
            Assert.Equal(5, x); // Refness is lost on lifting.
        }

        private static void CustomConversionNotStandardNameFromByRef()
        {
            var param = Expression.Parameter(typeof(int).MakeByRefType());
            MethodInfo method = typeof(CustomConversions).GetMethod(nameof(CustomConversions.ConvertFromRefInt));
            Expression<ByRefFunc<int, CustomConversions>> lambda = Expression.Lambda<ByRefFunc<int, CustomConversions>>(
                Expression.Convert(param, typeof(CustomConversions), method), param);
            ByRefFunc<int, CustomConversions> func = lambda.CompileToTestMethod();
            int x = 5;
            Assert.Equal(5, func(ref x).Value);
            Assert.Equal(6, x);
        }

        [Fact]
        public static void CustomConversionNotStandardNameFromByRefCompiler()
        {
            CustomConversionNotStandardNameFromByRef();
        }

        [Fact]
        public static void ConvertVoidToVoid()
        {
            Action act = Expression.Lambda<Action>(Expression.Convert(Expression.Empty(), typeof(void)))
                .CompileToTestMethod();
            act();
        }
    }
}
