// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class SwitchTests
    {
        [Fact]
        public void IntSwitch1()
        {
            ParameterExpression p = Expression.Parameter(typeof(int));
            ParameterExpression p1 = Expression.Parameter(typeof(string));
            SwitchExpression s = Expression.Switch(p,
                Expression.Assign(p1, Expression.Constant("default")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("hello")), Expression.Constant(1)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("two")), Expression.Constant(2)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("lala")), Expression.Constant(1)));

            BlockExpression block = Expression.Block(new ParameterExpression[] { p1 }, s, p1);

            Func<int, string> f = Expression.Lambda<Func<int, string>>(block, p).CompileToTestMethod();

            Assert.Equal("hello", f(1));
            Assert.Equal("two", f(2));
            Assert.Equal("default", f(3));
        }

        [Fact]
        public void NullableIntSwitch1()
        {
            ParameterExpression p = Expression.Parameter(typeof(int?));
            ParameterExpression p1 = Expression.Parameter(typeof(string));
            SwitchExpression s = Expression.Switch(p,
                Expression.Assign(p1, Expression.Constant("default")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("hello")), Expression.Constant((int?)1, typeof(int?))),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("two")), Expression.Constant((int?)2, typeof(int?))),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("lala")), Expression.Constant((int?)1, typeof(int?))));

            BlockExpression block = Expression.Block(new ParameterExpression[] { p1 }, s, p1);

            Func<int?, string> f = Expression.Lambda<Func<int?, string>>(block, p).CompileToTestMethod();

            Assert.Equal("hello", f(1));
            Assert.Equal("two", f(2));
            Assert.Equal("default", f(null));
            Assert.Equal("default", f(3));
        }

        [Fact]
        public void SwitchToGotos()
        {
            ParameterExpression p = Expression.Parameter(typeof(int));
            ParameterExpression p1 = Expression.Parameter(typeof(string));
            LabelTarget end = Expression.Label();
            LabelTarget lala = Expression.Label();
            LabelTarget hello = Expression.Label();
            BlockExpression block =Expression.Block(
                new [] { p1 },
                Expression.Switch(
                    p,
                    Expression.Block(
                        Expression.Assign(p1, Expression.Constant("default")),
                        Expression.Goto(end)
                        ),
                    Expression.SwitchCase(Expression.Goto(hello), Expression.Constant(1)),
                    Expression.SwitchCase(Expression.Block(
                        Expression.Assign(p1, Expression.Constant("two")),
                        Expression.Goto(end)
                        ), Expression.Constant(2)),
                    Expression.SwitchCase(Expression.Goto(lala), Expression.Constant(4))
                    ),
                Expression.Label(hello),
                Expression.Assign(p1, Expression.Constant("hello")),
                Expression.Goto(end),
                Expression.Label(lala),
                Expression.Assign(p1, Expression.Constant("lala")),
                Expression.Label(end),
                p1
                );

            Func<int, string> f = Expression.Lambda<Func<int, string>>(block, p).CompileToTestMethod();

            Assert.Equal("hello", f(1));
            Assert.Equal("two", f(2));
            Assert.Equal("default", f(3));
            Assert.Equal("lala", f(4));
        }

        [Fact]
        public void SwitchToGotosOutOfTry()
        {
            ParameterExpression p = Expression.Parameter(typeof(char));
            ParameterExpression p1 = Expression.Parameter(typeof(string));
            LabelTarget end = Expression.Label();
            LabelTarget lala = Expression.Label();
            LabelTarget hello = Expression.Label();
            BlockExpression block = Expression.Block(
                new[] { p1 },
                Expression.TryFinally(
                    Expression.Switch(
                        p,
                        Expression.Block(
                            Expression.Assign(p1, Expression.Constant("default")),
                            Expression.Goto(end)
                            ),
                        Expression.SwitchCase(Expression.Goto(hello), Expression.Constant('a')),
                        Expression.SwitchCase(Expression.Block(
                            Expression.Assign(p1, Expression.Constant("two")),
                            Expression.Goto(end)
                            ), Expression.Constant('b')),
                        Expression.SwitchCase(Expression.Goto(lala), Expression.Constant('d'))
                        ),
                        Expression.Empty()
                    ),
                Expression.Label(hello),
                Expression.Assign(p1, Expression.Constant("hello")),
                Expression.Goto(end),
                Expression.Label(lala),
                Expression.Assign(p1, Expression.Constant("lala")),
                Expression.Label(end),
                p1
                );

            Func<char, string> f = Expression.Lambda<Func<char, string>>(block, p).CompileToTestMethod();

            Assert.Equal("hello", f('a'));
            Assert.Equal("two", f('b'));
            Assert.Equal("default", f('c'));
            Assert.Equal("lala", f('d'));
        }

        [Fact]
        public void NullableIntSwitch2()
        {
            ParameterExpression p = Expression.Parameter(typeof(int?));
            ParameterExpression p1 = Expression.Parameter(typeof(string));
            SwitchExpression s = Expression.Switch(p,
                Expression.Assign(p1, Expression.Constant("default")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("hello")), Expression.Constant((int?)1, typeof(int?))),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("two")), Expression.Constant((int?)2, typeof(int?))),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("null")), Expression.Constant((int?)null, typeof(int?))),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("lala")), Expression.Constant((int?)1, typeof(int?))));

            BlockExpression block = Expression.Block(new ParameterExpression[] { p1 }, s, p1);

            Func<int?, string> f = Expression.Lambda<Func<int?, string>>(block, p).CompileToTestMethod();

            Assert.Equal("hello", f(1));
            Assert.Equal("two", f(2));
            Assert.Equal("null", f(null));
            Assert.Equal("default", f(3));
        }

        [Fact]
        public void IntSwitch2()
        {
            ParameterExpression p = Expression.Parameter(typeof(byte));
            ParameterExpression p1 = Expression.Parameter(typeof(string));
            SwitchExpression s = Expression.Switch(p,
                Expression.Assign(p1, Expression.Constant("default")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("hello")), Expression.Constant((byte)1)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("two")), Expression.Constant((byte)2)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("lala")), Expression.Constant((byte)1)));

            BlockExpression block = Expression.Block(new ParameterExpression[] { p1 }, s, p1);

            Func<byte, string> f = Expression.Lambda<Func<byte, string>>(block, p).CompileToTestMethod();

            Assert.Equal("hello", f(1));
            Assert.Equal("two", f(2));
            Assert.Equal("default", f(3));
        }

        [Fact]
        public void IntSwitch3()
        {
            ParameterExpression p = Expression.Parameter(typeof(uint));
            ParameterExpression p1 = Expression.Parameter(typeof(string));
            SwitchExpression s = Expression.Switch(p,
                Expression.Assign(p1, Expression.Constant("default")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("hello")), Expression.Constant((uint)1)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("two")), Expression.Constant((uint)2)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("lala")), Expression.Constant((uint)1)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("wow")), Expression.Constant(uint.MaxValue)));

            BlockExpression block = Expression.Block(new ParameterExpression[] { p1 }, s, p1);

            Func<uint, string> f = Expression.Lambda<Func<uint, string>>(block, p).CompileToTestMethod();

            Assert.Equal("hello", f(1));
            Assert.Equal("wow", f(uint.MaxValue));
            Assert.Equal("two", f(2));
            Assert.Equal("default", f(3));
        }

        [Fact]
        public void LongSwitch()
        {
            ParameterExpression p = Expression.Parameter(typeof(long));
            ParameterExpression p1 = Expression.Parameter(typeof(string));
            SwitchExpression s = Expression.Switch(p,
                Expression.Assign(p1, Expression.Constant("default")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("hello")), Expression.Constant(1L)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("two")), Expression.Constant(2L)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("lala")), Expression.Constant(1L)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("wow")), Expression.Constant(long.MaxValue)));

            BlockExpression block = Expression.Block(new [] { p1 }, s, p1);

            Func<long, string> f = Expression.Lambda<Func<long, string>>(block, p).CompileToTestMethod();

            Assert.Equal("hello", f(1));
            Assert.Equal("wow", f(long.MaxValue));
            Assert.Equal("two", f(2));
            Assert.Equal("default", f(3));
        }

        [Fact]
        public void ULongSwitch()
        {
            ParameterExpression p = Expression.Parameter(typeof(ulong));
            ParameterExpression p1 = Expression.Parameter(typeof(string));
            SwitchExpression s = Expression.Switch(p,
                Expression.Assign(p1, Expression.Constant("default")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("hello")), Expression.Constant(1UL)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("two")), Expression.Constant(2UL)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("lala")), Expression.Constant(1UL)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("wow")), Expression.Constant(ulong.MaxValue)));

            BlockExpression block = Expression.Block(new [] { p1 }, s, p1);

            Func<ulong, string> f = Expression.Lambda<Func<ulong, string>>(block, p).CompileToTestMethod();

            Assert.Equal("hello", f(1));
            Assert.Equal("wow", f(ulong.MaxValue));
            Assert.Equal("two", f(2));
            Assert.Equal("default", f(3));
        }

        [Fact]
        public void StringSwitch()
        {
            ParameterExpression p = Expression.Parameter(typeof(string));
            SwitchExpression s = Expression.Switch(p,
                Expression.Constant("default"),
                Expression.SwitchCase(Expression.Constant("hello"), Expression.Constant("hi")),
                Expression.SwitchCase(Expression.Constant("lala"), Expression.Constant("bye")));

            Func<string, string> f = Expression.Lambda<Func<string, string>>(s, p).CompileToTestMethod();

            Assert.Equal("hello", f("hi"));
            Assert.Equal("lala", f("bye"));
            Assert.Equal("default", f("hi2"));
            Assert.Equal("default", f(null));
        }

        [Fact]
        public void StringSwitch1()
        {
            ParameterExpression p = Expression.Parameter(typeof(string));
            ParameterExpression p1 = Expression.Parameter(typeof(string));
            SwitchExpression s = Expression.Switch(p,
                Expression.Assign(p1, Expression.Constant("default")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("hello")), Expression.Constant("hi")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("null")), Expression.Constant(null, typeof(string))),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("lala")), Expression.Constant("bye")));

            BlockExpression block = Expression.Block(new ParameterExpression[] { p1 }, s, p1);

            Func<string, string> f = Expression.Lambda<Func<string, string>>(block, p).CompileToTestMethod();

            Assert.Equal("hello", f("hi"));
            Assert.Equal("lala", f("bye"));
            Assert.Equal("default", f("hi2"));
            Assert.Equal("null", f(null));
        }

        [Fact]
        public void StringSwitchNotConstant()
        {
            Expression<Func<string>> expr1 = () => new string('a', 5);
            Expression<Func<string>> expr2 = () => new string('q', 5);

            ParameterExpression p = Expression.Parameter(typeof(string));
            SwitchExpression s = Expression.Switch(p,
                Expression.Constant("default"),
                Expression.SwitchCase(Expression.Invoke(expr1), Expression.Invoke(expr2)),
                Expression.SwitchCase(Expression.Constant("lala"), Expression.Constant("bye")));

            Func<string, string> f = Expression.Lambda<Func<string, string>>(s, p).CompileToTestMethod();

            Assert.Equal("aaaaa", f("qqqqq"));
            Assert.Equal("lala", f("bye"));
            Assert.Equal("default", f("hi2"));
            Assert.Equal("default", f(null));
        }

        [Fact]
        public void ObjectSwitch1()
        {
            ParameterExpression p = Expression.Parameter(typeof(object));
            ParameterExpression p1 = Expression.Parameter(typeof(string));
            SwitchExpression s = Expression.Switch(p,
                Expression.Assign(p1, Expression.Constant("default")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("hello")), Expression.Constant("hi")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("null")), Expression.Constant(null, typeof(string))),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("lala")), Expression.Constant("bye")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("lalala")), Expression.Constant("hi")));

            BlockExpression block = Expression.Block(new ParameterExpression[] { p1 }, s, p1);

            Func<object, string> f = Expression.Lambda<Func<object, string>>(block, p).CompileToTestMethod();

            Assert.Equal("hello", f("hi"));
            Assert.Equal("lala", f("bye"));
            Assert.Equal("default", f("hi2"));
            Assert.Equal("default", f("HI"));
            Assert.Equal("null", f(null));
        }

        [Fact]
        public void DefaultOnlySwitch()
        {
            ParameterExpression p = Expression.Parameter(typeof(int));
            SwitchExpression s = Expression.Switch(p, Expression.Constant(42));

            Func<int, int> fInt32Int32 = Expression.Lambda<Func<int, int>>(s, p).CompileToTestMethod();

            Assert.Equal(42, fInt32Int32(0));
            Assert.Equal(42, fInt32Int32(1));
            Assert.Equal(42, fInt32Int32(-1));

            s = Expression.Switch(typeof(object), p, Expression.Constant("A test string"), null);

            Func<int, object> fInt32Object = Expression.Lambda<Func<int, object>>(s, p).CompileToTestMethod();

            Assert.Equal("A test string", fInt32Object(0));
            Assert.Equal("A test string", fInt32Object(1));
            Assert.Equal("A test string", fInt32Object(-1));

            p = Expression.Parameter(typeof(string));
            s = Expression.Switch(p, Expression.Constant("foo"));

            Func<string, string> fStringString = Expression.Lambda<Func<string, string>>(s, p).CompileToTestMethod();

            Assert.Equal("foo", fStringString("bar"));
            Assert.Equal("foo", fStringString(null));
            Assert.Equal("foo", fStringString("foo"));
        }

        [Fact]
        public void NoDefaultOrCasesSwitch()
        {
            ParameterExpression p = Expression.Parameter(typeof(int));
            SwitchExpression s = Expression.Switch(p);

            Action<int> f = Expression.Lambda<Action<int>>(s, p).CompileToTestMethod();

            f(0);

            Assert.Equal(s.Type, typeof(void));
        }

        [Fact]
        public void TypedNoDefaultOrCasesSwitch()
        {
            ParameterExpression p = Expression.Parameter(typeof(int));
            // A SwitchExpression with neither a defaultBody nor any cases can not be any type except void.
            AssertExtensions.Throws<ArgumentException>("defaultBody", () => Expression.Switch(typeof(int), p, null, null));
        }

        private delegate int RefSettingDelegate(ref bool changed);

        private delegate void JustRefSettingDelegate(ref bool changed);

        public static int QuestionMeaning(ref bool changed)
        {
            changed = true;
            return 42;
        }

        [Fact]
        public void DefaultOnlySwitchWithSideEffect()
        {
            bool changed = false;
            ParameterExpression pOut = Expression.Parameter(typeof(bool).MakeByRefType(), "changed");
            MethodCallExpression switchValue = Expression.Call(typeof(SwitchTests).GetMethod(nameof(QuestionMeaning)), pOut);
            SwitchExpression s = Expression.Switch(switchValue, Expression.Constant(42));

            RefSettingDelegate fInt32Int32 = Expression.Lambda<RefSettingDelegate>(s, pOut).CompileToTestMethod();

            Assert.False(changed);
            Assert.Equal(42, fInt32Int32(ref changed));
            Assert.True(changed);
            changed = false;
            Assert.Equal(42, fInt32Int32(ref changed));
            Assert.True(changed);
        }

        [Fact]
        public void NoDefaultOrCasesSwitchWithSideEffect()
        {
            bool changed = false;
            ParameterExpression pOut = Expression.Parameter(typeof(bool).MakeByRefType(), "changed");
            MethodCallExpression switchValue = Expression.Call(typeof(SwitchTests).GetMethod(nameof(QuestionMeaning)), pOut);
            SwitchExpression s = Expression.Switch(switchValue, (Expression)null);

            JustRefSettingDelegate f = Expression.Lambda<JustRefSettingDelegate>(s, pOut).CompileToTestMethod();

            Assert.False(changed);
            f(ref changed);
            Assert.True(changed);
        }

        public class TestComparers
        {
            public static bool CaseInsensitiveStringCompare(string s1, string s2)
            {
                return StringComparer.OrdinalIgnoreCase.Equals(s1, s2);
            }
        }

        [Fact]
        public void SwitchWithComparison()
        {
            ParameterExpression p = Expression.Parameter(typeof(string));
            ParameterExpression p1 = Expression.Parameter(typeof(string));
            SwitchExpression s = Expression.Switch(p,
                Expression.Assign(p1, Expression.Constant("default")),
                typeof(TestComparers).GetMethod(nameof(TestComparers.CaseInsensitiveStringCompare)),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("hello")), Expression.Constant("hi")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("null")), Expression.Constant(null, typeof(string))),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("lala")), Expression.Constant("bye")),
                Expression.SwitchCase(Expression.Assign(p1, Expression.Constant("lalala")), Expression.Constant("hi")));

            BlockExpression block = Expression.Block(new ParameterExpression[] { p1 }, s, p1);

            Func<string, string> f = Expression.Lambda<Func<string, string>>(block, p).CompileToTestMethod();

            Assert.Equal("hello", f("hi"));
            Assert.Equal("lala", f("bYe"));
            Assert.Equal("default", f("hi2"));
            Assert.Equal("hello", f("HI"));
            Assert.Equal("null", f(null));
        }

        private static IEnumerable<object[]> ComparisonsWithInvalidParmeterCounts()
        {
            Func<bool> nullary = () => true;
            yield return new object[] { nullary.GetMethodInfo() };
            Func<int, bool> unary = x => x % 2 == 0;
            yield return new object[] { unary.GetMethodInfo() };
            Func<int, int, int, bool> ternary = (x, y, z) => (x == y) == (y == z);
            yield return new object[] { ternary.GetMethodInfo() };
            Func<int, int, int, int, bool> quaternary = (a, b, c, d) => (a == b) == (c == d);
            yield return new object[] { quaternary.GetMethodInfo() };
        }

        private class GenClass<T>
        {
            public static bool WithinTwo(int x, int y) => Math.Abs(x - y) < 2;
        }

        public static bool WithinTen(int x, int y) => Math.Abs(x - y) < 10;

        [Fact]
        public void LiftedCall()
        {
            Func<int> f = Expression.Lambda<Func<int>>(
                Expression.Switch(
                    Expression.Constant(30, typeof(int?)),
                    Expression.Constant(0),
                    typeof(SwitchTests).GetMethod(nameof(WithinTen), BindingFlags.Static | BindingFlags.Public),
                    Expression.SwitchCase(Expression.Constant(1), Expression.Constant(2, typeof(int?))),
                    Expression.SwitchCase(Expression.Constant(2), Expression.Constant(9, typeof(int?)), Expression.Constant(28, typeof(int?))),
                    Expression.SwitchCase(Expression.Constant(3), Expression.Constant(49, typeof(int?)))
                    )
                ).CompileToTestMethod();

            Assert.Equal(2, f());
        }

        static int NonBooleanMethod(int x, int y) => x + y;
        [Fact]
        public void MismatchingAllowedIfExplicitlyVoidIntgralValueType()
        {
            Expression.Lambda<Action>(
                Expression.Switch(
                    typeof(void),
                    Expression.Constant(0),
                    Expression.Constant(1),
                    null,
                    Expression.SwitchCase(Expression.Constant("Foo"), Expression.Constant(2)),
                    Expression.SwitchCase(Expression.Constant(DateTime.MinValue), Expression.Constant(3))
                    )
                ).CompileToTestMethod()();
        }

        [Fact]
        public void MismatchingAllowedIfExplicitlyVoidStringValueType()
        {
            Expression.Lambda<Action>(
                Expression.Switch(
                    typeof(void),
                    Expression.Constant("Foo"),
                    Expression.Constant(1),
                    null,
                    Expression.SwitchCase(Expression.Constant("Foo"), Expression.Constant("Bar")),
                    Expression.SwitchCase(Expression.Constant(DateTime.MinValue), Expression.Constant("Foo"))
                    )
                ).CompileToTestMethod()();
        }

        [Fact]
        public void MismatchingAllowedIfExplicitlyVoidDateTimeValueType()
        {
            Expression.Lambda<Action>(
                Expression.Switch(
                    typeof(void),
                    Expression.Constant(DateTime.MinValue),
                    Expression.Constant(1),
                    null,
                    Expression.SwitchCase(Expression.Constant("Foo"), Expression.Constant(DateTime.MinValue)),
                    Expression.SwitchCase(Expression.Constant(DateTime.MinValue), Expression.Constant(DateTime.MaxValue))
                    )
                ).CompileToTestMethod()();
        }


        [Fact]
        public void SwitchOnString()
        {
            var values = new string[] { "foobar", "foo", "bar", "baz", "qux", "quux", "corge", "grault", "garply", "waldo", "fred", "plugh", "xyzzy", "thud" };

            for (var i = 1; i <= values.Length; i++)
            {
                SwitchCase[] cases = values.Take(i).Select((s, j) => Expression.SwitchCase(Expression.Constant(j), Expression.Constant(values[j]))).ToArray();
                ParameterExpression value = Expression.Parameter(typeof(string));
                Expression<Func<string, int>> e = Expression.Lambda<Func<string, int>>(Expression.Switch(value, Expression.Constant(-1), cases), value);
                Func<string, int> f = e.CompileToTestMethod();

                int k = 0;
                foreach (var str in values.Take(i))
                {
                    Assert.Equal(k, f(str));
                    k++;
                }

                foreach (var str in values.Skip(i).Concat(new[] { default(string), "whatever", "FOO" }))
                {
                    Assert.Equal(-1, f(str));
                    k++;
                }
            }
        }


        [Fact]
        public void SwitchOnStringEqualsMethod()
        {
            var values = new string[] { "foobar", "foo", "bar", "baz", "qux", "quux", "corge", "grault", "garply", "waldo", "fred", "plugh", "xyzzy", "thud", null };

            for (var i = 1; i <= values.Length; i++)
            {
                SwitchCase[] cases = values.Take(i).Select((s, j) => Expression.SwitchCase(Expression.Constant(j), Expression.Constant(values[j], typeof(string)))).ToArray();
                ParameterExpression value = Expression.Parameter(typeof(string));
                Expression<Func<string, int>> e = Expression.Lambda<Func<string, int>>(Expression.Switch(value, Expression.Constant(-1), typeof(string).GetMethod("Equals", new[] { typeof(string), typeof(string) }), cases), value);
                Func<string, int> f = e.CompileToTestMethod();

                int k = 0;
                foreach (var str in values.Take(i))
                {
                    Assert.Equal(k, f(str));
                    k++;
                }

                foreach (var str in values.Skip(i).Concat(new[] { "whatever", "FOO" }))
                {
                    Assert.Equal(-1, f(str));
                    k++;
                }
            }
        }

    }
}
