// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Reflection.Emit;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    [TestCaseOrderer("System.Linq.Expressions.Tests.TestOrderer", "System.Linq.Expressions.Tests")]
    public class LambdaTests
    {
        private delegate int IcosanaryInt32Func(
            int arg1,
            int arg2,
            int arg3,
            int arg4,
            int arg5,
            int arg6,
            int arg7,
            int arg8,
            int arg9,
            int arg10,
            int arg11,
            int arg12,
            int arg13,
            int arg14,
            int arg15,
            int arg16,
            int arg17,
            int arg18,
            int arg19,
            int arg20);

        [Fact]
        public void Lambda()
        {
            ParameterExpression paramI = Expression.Parameter(typeof(int), "i");
            ParameterExpression paramJ = Expression.Parameter(typeof(double), "j");
            ParameterExpression paramK = Expression.Parameter(typeof(decimal), "k");
            ParameterExpression paramL = Expression.Parameter(typeof(short), "l");

            Expression lambda = (Expression<Func<int, double, decimal, int>>)((i, j, k) => i);

            Assert.Equal(typeof(Func<int, double, decimal, Func<int, double, decimal, int>>), Expression.Lambda(lambda, paramI, paramJ, paramK).Type);

            lambda = (Expression<Func<int, double, decimal, short, int>>)((i, j, k, l) => i);

            Assert.IsType<Func<int, double, decimal, short, Func<int, double, decimal, short, int>>>(Expression.Lambda(lambda, paramI, paramJ, paramK, paramL).CompileToTestMethod());

            Assert.Equal("(i, j, k, l) => i", lambda.ToString());
        }

        // Possible issue with AOT? See https://github.com/dotnet/corefx/pull/8116/files#r61346743
        [Fact]
        public void InvokeComputedLambda()
        {
            ParameterExpression x = Expression.Parameter(typeof(int), "x");
            ParameterExpression y = Expression.Parameter(typeof(int), "y");
            Expression call = Expression.Call(null, GetType().GetMethod(nameof(ComputeLambda), BindingFlags.Static | BindingFlags.Public), y);
            InvocationExpression ie = Expression.Invoke(call, x);
            Expression<Func<int, int, int>> lambda = Expression.Lambda<Func<int, int, int>>(ie, x, y);

            Func<int, int, int> d = lambda.CompileToTestMethod();
            Assert.Equal(14, d(5, 9));
            Assert.Equal(40, d(5, 8));
        }

        public static Expression<Func<int, int>> ComputeLambda(int y)
        {
            if ((y & 1) != 0)
            {
                return x => x + y;
            }

            return x => x * y;
        }

        [Fact]
        public void HighArityDelegate()
        {
            ParameterExpression[] paramList = Enumerable.Range(0, 20).Select(_ => Expression.Variable(typeof(int))).ToArray();
            Expression<IcosanaryInt32Func> exp = Expression.Lambda<IcosanaryInt32Func>(
                Expression.Add(
                    paramList[0],
                    Expression.Add(
                        paramList[1],
                        Expression.Add(
                            paramList[2],
                            Expression.Add(
                                paramList[3],
                                Expression.Add(
                                    paramList[4],
                                    Expression.Add(
                                        paramList[5],
                                        Expression.Add(
                                            paramList[6],
                                            Expression.Add(
                                                paramList[7],
                                                Expression.Add(
                                                    paramList[8],
                                                    Expression.Add(
                                                        paramList[9],
                                                        Expression.Add(
                                                            paramList[10],
                                                            Expression.Add(
                                                                paramList[11],
                                                                Expression.Add(
                                                                    paramList[12],
                                                                    Expression.Add(
                                                                        paramList[13],
                                                                        Expression.Add(
                                                                            paramList[14],
                                                                            Expression.Add(
                                                                                paramList[15],
                                                                                Expression.Add(
                                                                                    paramList[16],
                                                                                    Expression.Add(
                                                                                        paramList[17],
                                                                                        Expression.Add(
                                                                                            paramList[18],
                                                                                            paramList[19]
                                                                                            )
                                                                                        )
                                                                                    )
                                                                                )
                                                                            )
                                                                        )
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                paramList
                );
            IcosanaryInt32Func f = exp.CompileToTestMethod();
            Assert.Equal(210, f(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20));
        }



        [Fact]
        public void NoPreferenceCompile()
        {
            // The two compilation options are given plenty of exercise between here and elsewhere
            // Make sure the no-preference approach keeps working.

            ParameterExpression param = Expression.Parameter(typeof(int));
            Expression<Func<int, int>> typedExp = Expression.Lambda<Func<int, int>>(
                Expression.Add(param, Expression.Constant(2)),
                param
                );
            Assert.Equal(5, typedExp.CompileToTestMethod()(3));

            LambdaExpression exp = Expression.Lambda(
                Expression.Add(param, Expression.Constant(7)),
                param
                );
            Assert.Equal(19, exp.CompileToTestMethod().DynamicInvoke(12));
        }


        [Fact]
        public void AutoQuote()
        {
            ParameterExpression param = Expression.Parameter(typeof(int));
            Expression<Func<int, int>> inner = Expression.Lambda<Func<int, int>>(
                Expression.Multiply(param, Expression.Constant(2)),
                param
                );
            Expression<Func<Expression<Func<int, int>>>> outer = Expression.Lambda<Func<Expression<Func<int, int>>>>(inner);
            Assert.IsType<UnaryExpression>(outer.Body);
            Assert.Equal(ExpressionType.Quote, outer.Body.NodeType);
            Func<Expression<Func<int, int>>> outerDel = outer.CompileToTestMethod();
            Func<int, int> innerDel = outerDel().CompileToTestMethod();
            Assert.Equal(16, innerDel(8));
        }

        [Fact]
        public void NestedCompile()
        {
            ParameterExpression param = Expression.Parameter(typeof(int));
            Expression<Func<int, int>> inner = Expression.Lambda<Func<int, int>>(
                Expression.Multiply(param, Expression.Constant(2)),
                param
                );
            Expression<Func<Func<int, int>>> outer = Expression.Lambda<Func<Func<int, int>>>(inner);
            Assert.Same(inner, outer.Body);
            Func<Func<int, int>> outerDel = outer.CompileToTestMethod();
            Func<int, int> innerDel = outerDel();
            Assert.Equal(16, innerDel(8));
        }

        [Fact]
        public void AnyTypeCanBeReturnedVoid()
        {
            Action act = Expression.Lambda<Action>(Expression.Constant("foo")).CompileToTestMethod();
            act();
        }

        [Fact]
        public void NameNeedNotBeValidCSharpLabel()
        {
            string name = "1, 2, 3, 4. This is not a valid C♯ label!\"'<>.\uffff";
            var exp = (Expression<Func<int>>)Expression.Lambda(Expression.Constant(21), name, Array.Empty<ParameterExpression>());
            Assert.Equal(name, exp.Name);
            Assert.Equal(21, exp.CompileToTestMethod()());

            exp = (Expression<Func<int>>)Expression.Lambda(typeof(Func<int>), Expression.Constant(22), name, Array.Empty<ParameterExpression>());
            Assert.Equal(name, exp.Name);
            Assert.Equal(22, exp.CompileToTestMethod()());

            exp = Expression.Lambda<Func<int>>(Expression.Constant(23), name, Array.Empty<ParameterExpression>());
            Assert.Equal(name, exp.Name);
            Assert.Equal(23, exp.CompileToTestMethod()());
        }


        [Fact]
        public void CurriedFunctions()
        {
            Expression<Func<int, Func<int>>> f1 = x => () => x;
            Func<int, Func<int>> d1 = f1.CompileToTestMethod();
            Func<int> c1 = d1(42);
            Assert.Equal(42, c1());
            Assert.Equal(42, c1());

            Expression<Func<int, Func<int, int>>> f2 = x => y => x - y;
            Func<int, Func<int, int>> d2 = f2.CompileToTestMethod();
            Func<int, int> c2 = d2(42);
            Assert.Equal(41, c2(1));
            Assert.Equal(40, c2(2));

            Expression<Func<int, Func<int, Func<int, int>>>> f3 = x => y => z => x * y - z;
            Func<int, Func<int, Func<int, int>>> d3 = f3.CompileToTestMethod();
            Func<int, Func<int, int>> c3 = d3(2);
            Func<int, int> c31 = c3(21);
            Assert.Equal(41, c31(1));
            Assert.Equal(40, c31(2));
            Func<int, int> c32 = c3(22);
            Assert.Equal(41, c32(3));
            Assert.Equal(40, c32(4));
        }

        [Fact]
        public void CurriedFunctionsReadWrite()
        {
            // Generate code like:
            //
            // val => () =>
            // {
            //     sum = 0;
            //
            //     val = 1;
            //     sum += val;
            //     ...
            //     val = n;
            //     sum += val;
            //
            //     return sum;
            // }
            //
            // This introduces repeated reads and writes for a hoisted local, which may be subject
            // to optimizations for closure storage access.
            //
            for (var i = 0; i < 10; i++)
            {
                ParameterExpression val = Expression.Parameter(typeof(int), "val");
                ParameterExpression sum = Expression.Parameter(typeof(int), "sum");

                var addExprs = new List<Expression>();

                for (var j = 1; j <= i; j++)
                {
                    addExprs.Add(Expression.Assign(val, Expression.Constant(j)));
                    addExprs.Add(Expression.AddAssign(sum, val));
                }

                BlockExpression adds = Expression.Block(addExprs);
                BlockExpression body = Expression.Block(new[] { sum }, Expression.Assign(sum, Expression.Constant(0)), adds, sum);

                Expression<Func<int, Func<int>>> e = Expression.Lambda<Func<int, Func<int>>>(Expression.Lambda<Func<int>>(body), val);
                Func<int, Func<int>> f = e.CompileToTestMethodSimple();

                Assert.Equal(i * (i + 1) / 2, f(i)());
            }
        }

        [Fact]
        public void CurriedFunctionsUsingRef()
        {
            Expression<Func<int, Func<int>>> f1 = x => () => x * Add(ref x, 1);
            Func<int, Func<int>> d1 = f1.CompileToTestMethod();
            Assert.Equal(3 * 4, d1(3)());

            Expression<Func<int, Func<int>>> f2 = x => () => x * Add(ref x, 1) * Add(ref x, 2);
            Func<int, Func<int>> d2 = f2.CompileToTestMethod();
            Assert.Equal(3 * 4 * 6, d2(3)());

            Expression<Func<int, Func<int>>> f3 = x => () => x * Add(ref x, 1) * Add(ref x, 2) * x;
            Func<int, Func<int>> d3 = f3.CompileToTestMethod();
            Assert.Equal(3 * 4 * 6 * 6, d3(3)());
        }

        [Fact]
        public void CurriedFunctionsReadWriteThroughRef()
        {
            // Generate code like:
            //
            // val => () =>
            // {
            //     sum = 0;
            //
            //     val = 1;
            //     Add(ref sum, val);
            //     ...
            //     val = n;
            //     Add(ref sum, val);
            //
            //     return sum;
            // }
            //
            // This introduces repeated reads and writes for a hoisted local, which may be subject
            // to optimizations for closure storage access.
            //

            MethodInfo add = typeof(LambdaTests).GetMethod(nameof(LambdaTests.Add), BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);

            for (var i = 0; i < 10; i++)
            {
                ParameterExpression val = Expression.Parameter(typeof(int));
                ParameterExpression sum = Expression.Parameter(typeof(int));

                var addExprs = new List<Expression>();

                for (var j = 1; j <= i; j++)
                {
                    addExprs.Add(Expression.Assign(val, Expression.Constant(j)));
                    addExprs.Add(Expression.Call(add, sum, val));
                }

                BlockExpression adds = Expression.Block(addExprs);
                BlockExpression body = Expression.Block(new[] { sum }, Expression.Assign(sum, Expression.Constant(0)), adds, sum);

                Expression<Func<int, Func<int>>> e = Expression.Lambda<Func<int, Func<int>>>(Expression.Lambda<Func<int>>(body), val);
                Func<int, Func<int>> f = e.CompileToTestMethod();

                Assert.Equal(i * (i + 1) / 2, f(i)());
            }
        }

        [Fact]
        public void CurriedFunctionsVariableCaptureSemantics()
        {
            ParameterExpression x = Expression.Parameter(typeof(int), "x");
            ParameterExpression f = Expression.Parameter(typeof(Func<int>), "f");

            Expression<Func<Func<int>>> e =
                Expression.Lambda<Func<Func<int>>>(
                    Expression.Block(
                        new[] { f, x },
                        Expression.Assign(x, Expression.Constant(-1)),
                        Expression.Assign(
                            f,
                            Expression.Lambda<Func<int>>(
                                Expression.MultiplyAssign(x, Expression.Constant(2))
                            )
                        ),
                        Expression.Assign(x, Expression.Constant(20)),
                        Expression.AddAssign(x, Expression.Constant(1)),
                        f
                    )
                );

            Func<Func<int>> d = e.CompileToTestMethodSimple();

            Func<int> i = d();

            Assert.Equal(42, i());
            Assert.Equal(84, i());
        }

        private static IEnumerable<object[]> LambdaTypes() =>
            from parCount in Enumerable.Range(0, 6)
            from name in new[] {null, "Lambda"}
            from tailCall in new[] {false, true}
            select new object[] {parCount, name, tailCall};


        public static int Add(ref int var, int val)
        {
            return var += val;
        }



#if NETCOREAPP1_1
        [Fact(Skip = "Expression.Lambda fails")]
#else
        [Fact]
#endif
        public void PrivateDelegate()
        {
            AssemblyBuilder assembly = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName("Name"), AssemblyBuilderAccess.Run);
            ModuleBuilder module = assembly.DefineDynamicModule("Name");
            TypeBuilder builder = module.DefineType("Type", TypeAttributes.Class | TypeAttributes.NotPublic | TypeAttributes.Sealed | TypeAttributes.AnsiClass | TypeAttributes.AutoClass, typeof(MulticastDelegate));
            builder.DefineConstructor(MethodAttributes.RTSpecialName | MethodAttributes.HideBySig | MethodAttributes.Public, CallingConventions.Standard, new[] { typeof(object), typeof(IntPtr) }).SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);
            builder.DefineMethod("Invoke", MethodAttributes.Private | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Virtual, typeof(int), Type.EmptyTypes).SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);
            Type delType = builder.CreateTypeInfo().AsType();
            LambdaExpression lambda = Expression.Lambda(delType, Expression.Constant(42));
            Delegate del = lambda.CompileToTestMethod();
            Assert.IsType(delType, del);
            Assert.Equal(42, del.DynamicInvoke());
        }



    }
}
