// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class BinaryLogicalTests
    {
        //TODO: Need tests on the short-circuit and non-short-circuit nature of the two forms.

        #region Test methods

        [Fact]
        public static void CheckBoolAndTest()
        {
            bool[] array = new bool[] { true, false };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyBoolAnd(array[i], array[j]);
                }
            }
        }

        [Fact]
        public static void CheckBoolAndAlsoTest()
        {
            bool[] array = new bool[] { true, false };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyBoolAndAlso(array[i], array[j]);
                }
            }
        }

        [Fact]
        public static void CheckBoolOrTest()
        {
            bool[] array = new bool[] { true, false };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyBoolOr(array[i], array[j]);
                }
            }
        }

        [Fact]
        public static void CheckBoolOrElseTest()
        {
            bool[] array = new bool[] { true, false };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyBoolOrElse(array[i], array[j]);
                }
            }
        }

        #endregion

        #region Test verifiers

        private static void VerifyBoolAnd(bool a, bool b)
        {
            Expression<Func<bool>> e =
                Expression.Lambda<Func<bool>>(
                    Expression.And(
                        Expression.Constant(a, typeof(bool)),
                        Expression.Constant(b, typeof(bool))),
                    Enumerable.Empty<ParameterExpression>());
            Func<bool> f = e.CompileToTestMethod();

            Assert.Equal(a & b, f());
        }

        private static void VerifyBoolAndAlso(bool a, bool b)
        {
            Expression<Func<bool>> e =
                Expression.Lambda<Func<bool>>(
                    Expression.AndAlso(
                        Expression.Constant(a, typeof(bool)),
                        Expression.Constant(b, typeof(bool))),
                    Enumerable.Empty<ParameterExpression>());
            Func<bool> f = e.CompileToTestMethod();

            Assert.Equal(a && b, f());
        }

        private static void VerifyBoolOr(bool a, bool b)
        {
            Expression<Func<bool>> e =
                Expression.Lambda<Func<bool>>(
                    Expression.Or(
                        Expression.Constant(a, typeof(bool)),
                        Expression.Constant(b, typeof(bool))),
                    Enumerable.Empty<ParameterExpression>());
            Func<bool> f = e.CompileToTestMethod();

            Assert.Equal(a | b, f());
        }

        private static void VerifyBoolOrElse(bool a, bool b)
        {
            Expression<Func<bool>> e =
                Expression.Lambda<Func<bool>>(
                    Expression.OrElse(
                        Expression.Constant(a, typeof(bool)),
                        Expression.Constant(b, typeof(bool))),
                    Enumerable.Empty<ParameterExpression>());
            Func<bool> f = e.CompileToTestMethod();

            Assert.Equal(a || b, f());
        }

        #endregion

        public static IEnumerable<object[]> AndAlso_TestData()
        {
            yield return new object[] { 5, 3, 1, true };
            yield return new object[] { 0, 3, 0, false };
            yield return new object[] { 5, 0, 0, true };
        }

        [Theory]
        [MemberData(nameof(AndAlso_TestData))]
        public static void AndAlso_UserDefinedOperator(int leftValue, int rightValue, int expectedValue, bool calledMethod)
        {
            TrueFalseClass left = new TrueFalseClass(leftValue);
            TrueFalseClass right = new TrueFalseClass(rightValue);

            BinaryExpression expression = Expression.AndAlso(Expression.Constant(left), Expression.Constant(right));
            Func<TrueFalseClass> lambda = Expression.Lambda<Func<TrueFalseClass>>(expression).CompileToTestMethod();
            Assert.Equal(expectedValue, lambda().Value);

            // AndAlso only evaluates the false operator of left
            Assert.Equal(0, left.TrueCallCount);
            Assert.Equal(1, left.FalseCallCount);
            Assert.Equal(0, right.TrueCallCount);
            Assert.Equal(0, right.FalseCallCount);

            // AndAlso only evaluates the operator if left is not false
            Assert.Equal(calledMethod ? 1 : 0, left.OperatorCallCount);
        }

        [Fact]
        public static void AndAlso_UserDefinedOperator_HasMethodNotOperator()
        {
            BinaryExpression expression = Expression.AndAlso(Expression.Constant(new NamedMethods(5)), Expression.Constant(new NamedMethods(3)));
            Func<NamedMethods> lambda = Expression.Lambda<Func<NamedMethods>>(expression).CompileToTestMethod();
            Assert.Equal(1, lambda().Value);
        }

        [Theory]
        [MemberData(nameof(AndAlso_TestData))]
        public static void AndAlso_Method(int leftValue, int rightValue, int expectedValue, bool calledMethod)
        {
            MethodInfo method = typeof(TrueFalseClass).GetMethod(nameof(TrueFalseClass.AndMethod));

            TrueFalseClass left = new TrueFalseClass(leftValue);
            TrueFalseClass right = new TrueFalseClass(rightValue);

            BinaryExpression expression = Expression.AndAlso(Expression.Constant(left), Expression.Constant(right), method);
            Func<TrueFalseClass> lambda = Expression.Lambda<Func<TrueFalseClass>>(expression).CompileToTestMethod();
            Assert.Equal(expectedValue, lambda().Value);

            // AndAlso only evaluates the false operator of left
            Assert.Equal(0, left.TrueCallCount);
            Assert.Equal(1, left.FalseCallCount);
            Assert.Equal(0, right.TrueCallCount);
            Assert.Equal(0, right.FalseCallCount);

            // AndAlso only evaluates the method if left is not false
            Assert.Equal(0, left.OperatorCallCount);
            Assert.Equal(calledMethod ? 1 : 0, left.MethodCallCount);
        }

        public static IEnumerable<object[]> OrElse_TestData()
        {
            yield return new object[] { 5, 3, 5, false };
            yield return new object[] { 0, 3, 3, true };
            yield return new object[] { 5, 0, 5, false };
        }

        [Theory]
        [MemberData(nameof(OrElse_TestData))]
        public static void OrElse_UserDefinedOperator(int leftValue, int rightValue, int expectedValue, bool calledMethod)
        {
            TrueFalseClass left = new TrueFalseClass(leftValue);
            TrueFalseClass right = new TrueFalseClass(rightValue);

            BinaryExpression expression = Expression.OrElse(Expression.Constant(left), Expression.Constant(right));
            Func<TrueFalseClass> lambda = Expression.Lambda<Func<TrueFalseClass>>(expression).CompileToTestMethod();
            Assert.Equal(expectedValue, lambda().Value);

            // OrElse only evaluates the true operator of left
            Assert.Equal(1, left.TrueCallCount);
            Assert.Equal(0, left.FalseCallCount);
            Assert.Equal(0, right.TrueCallCount);
            Assert.Equal(0, right.FalseCallCount);

            // OrElse only evaluates the operator if left is not true
            Assert.Equal(calledMethod ? 1 : 0, left.OperatorCallCount);
        }

        [Fact]
        public static void OrElse_UserDefinedOperator_HasMethodNotOperator()
        {
            BinaryExpression expression = Expression.OrElse(Expression.Constant(new NamedMethods(0)), Expression.Constant(new NamedMethods(3)));
            Func<NamedMethods> lambda = Expression.Lambda<Func<NamedMethods>>(expression).CompileToTestMethod();
            Assert.Equal(3, lambda().Value);
        }

        [Theory]
        [MemberData(nameof(OrElse_TestData))]
        public static void OrElse_Method(int leftValue, int rightValue, int expectedValue, bool calledMethod)
        {
            MethodInfo method = typeof(TrueFalseClass).GetMethod(nameof(TrueFalseClass.OrMethod));

            TrueFalseClass left = new TrueFalseClass(leftValue);
            TrueFalseClass right = new TrueFalseClass(rightValue);

            BinaryExpression expression = Expression.OrElse(Expression.Constant(left), Expression.Constant(right), method);
            Func<TrueFalseClass> lambda = Expression.Lambda<Func<TrueFalseClass>>(expression).CompileToTestMethod();
            Assert.Equal(expectedValue, lambda().Value);

            // OrElse only evaluates the true operator of left
            Assert.Equal(1, left.TrueCallCount);
            Assert.Equal(0, left.FalseCallCount);
            Assert.Equal(0, right.TrueCallCount);
            Assert.Equal(0, right.FalseCallCount);

            // OrElse only evaluates the method if left is not true
            Assert.Equal(0, left.OperatorCallCount);
            Assert.Equal(calledMethod ? 1 : 0, left.MethodCallCount);
        }




        public class NonGenericClass
        {
            public void InstanceMethod() { }
            public static void StaticVoidMethod() { }

            public static int StaticIntMethod0() => 0;
            public static int StaticIntMethod1(int i) => 0;
            public static int StaticIntMethod3(int i1, int i2, int i3) => 0;

            public static int StaticIntMethod2Valid(int i1, int i2) => 0;

            public static int StaticIntMethod2Invalid1(int i1, string i2) => 0;
            public static string StaticIntMethod2Invalid2(int i1, int i2) => "abc";
        }

        public class TrueFalseClass
        {
            public int TrueCallCount { get; set; }
            public int FalseCallCount { get; set; }
            public int OperatorCallCount { get; set; }
            public int MethodCallCount { get; set; }

            public TrueFalseClass(int value) { Value = value; }
            public int Value { get; }

            public static bool operator true(TrueFalseClass c)
            {
                c.TrueCallCount++;
                return c.Value != 0;
            }

            public static bool operator false(TrueFalseClass c)
            {
                c.FalseCallCount++;
                return c.Value == 0;
            }

            public static TrueFalseClass operator &(TrueFalseClass c1, TrueFalseClass c2)
            {
                c1.OperatorCallCount++;
                return new TrueFalseClass(c1.Value & c2.Value);
            }

            public static TrueFalseClass AndMethod(TrueFalseClass c1, TrueFalseClass c2)
            {
                c1.MethodCallCount++;
                return new TrueFalseClass(c1.Value & c2.Value);
            }

            public static TrueFalseClass operator |(TrueFalseClass c1, TrueFalseClass c2)
            {
                c1.OperatorCallCount++;
                return new TrueFalseClass(c1.Value | c2.Value);
            }

            public static TrueFalseClass OrMethod(TrueFalseClass c1, TrueFalseClass c2)
            {
                c1.MethodCallCount++;
                return new TrueFalseClass(c1.Value | c2.Value);
            }
        }

        public class NamedMethods
        {
            public NamedMethods(int value) { Value = value; }
            public int Value { get; }

            public static bool operator true(NamedMethods c) => c.Value != 0;
            public static bool operator false(NamedMethods c) => c.Value == 0;

            public static NamedMethods op_BitwiseAnd(NamedMethods c1, NamedMethods c2) => new NamedMethods(c1.Value & c2.Value);
            public static NamedMethods op_BitwiseOr(NamedMethods c1, NamedMethods c2) => new NamedMethods(c1.Value | c2.Value);
        }

        public class ClassWithImplicitBoolOperator
        {
            public static ClassWithImplicitBoolOperator ConversionMethod(ClassWithImplicitBoolOperator bool1, ClassWithImplicitBoolOperator bool2)
            {
                return bool1;
            }

            public static implicit operator bool(ClassWithImplicitBoolOperator boolClass) => true;
        }
    }
}
