// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class BinaryEqualTests
    {
        public static IEnumerable<object[]> TestData()
        {
            yield return new object[] { new bool[] { true, false } };
            yield return new object[] { new byte[] { 0, 1, byte.MaxValue } };
            yield return new object[] { new char[] { '\0', '\b', 'A', '\uffff' } };
            yield return new object[] { new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue } };
            yield return new object[] { new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN } };
            yield return new object[] { new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN } };
            yield return new object[] { new int[] { 0, 1, -1, int.MinValue, int.MaxValue } };
            yield return new object[] { new long[] { 0, 1, -1, long.MinValue, long.MaxValue } };
            yield return new object[] { new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue } };
            yield return new object[] { new short[] { 0, 1, -1, short.MinValue, short.MaxValue } };
            yield return new object[] { new uint[] { 0, 1, uint.MaxValue } };
            yield return new object[] { new ulong[] { 0, 1, ulong.MaxValue } };
            yield return new object[] { new ushort[] { 0, 1, ushort.MaxValue } };
            yield return new object[] { new TestClass[] { new TestClass(), new TestClass() } };
            yield return new object[] { new TestEnum[] { new TestEnum(), new TestEnum() } };
            yield return new object[] { new E[] {E.A, E.B, (E)int.MinValue} };

            yield return new object[] { new bool?[] { null, true, false } };
            yield return new object[] { new byte?[] { null, 0, 1, byte.MaxValue } };
            yield return new object[] { new char?[] { null, '\0', '\b', 'A', '\uffff' } };
            yield return new object[] { new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue } };
            yield return new object[] { new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN } };
            yield return new object[] { new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN } };
            yield return new object[] { new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue } };
            yield return new object[] { new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue } };
            yield return new object[] { new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue } };
            yield return new object[] { new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue } };
            yield return new object[] { new uint?[] { null, 0, 1, uint.MaxValue } };
            yield return new object[] { new ulong?[] { null, 0, 1, ulong.MaxValue } };
            yield return new object[] { new ushort?[] { null, 0, 1, ushort.MaxValue } };
            yield return new object[] { new E?[] {null, E.A, E.B, (E)int.MaxValue, (E)int.MinValue} };
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public static void Equal(Array array)
        {
            Type type = array.GetType().GetElementType();
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    object a = array.GetValue(i);
                    object b = array.GetValue(j);
                    BinaryExpression equal = Expression.Equal(Expression.Constant(a, type), Expression.Constant(b, type));
                    GeneralBinaryTests.CompileBinaryExpression(equal, GeneralBinaryTests.CustomEquals(a, b));
                }
            }
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public static void NotEqual(Array array)
        {
            Type type = array.GetType().GetElementType();
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    object a = array.GetValue(i);
                    object b = array.GetValue(j);
                    BinaryExpression equal = Expression.NotEqual(Expression.Constant(a, type), Expression.Constant(b, type));
                    GeneralBinaryTests.CompileBinaryExpression(equal, !GeneralBinaryTests.CustomEquals(a, b));
                }
            }
        }

        [Fact]
        public static void Equal_Constant_DefaultString()
        {
            var array = new Expression[] { Expression.Constant("bar", typeof(string)), Expression.Constant(null, typeof(string)), Expression.Default(typeof(string)) };
            var isNull = new bool[] { false, true, true };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    BinaryExpression equal = Expression.Equal(array[i], array[j]);
                    GeneralBinaryTests.CompileBinaryExpression(equal, isNull[i] == isNull[j]);
                }
            }
        }

        [Fact]
        public static void Equal_Constant_DefaultNullable()
        {
            var array = new Expression[] { Expression.Constant(42, typeof(int?)), Expression.Constant(null, typeof(int?)), Expression.Default(typeof(int?)) };
            var isNull = new bool[] { false, true, true };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    BinaryExpression equal = Expression.Equal(array[i], array[j]);
                    GeneralBinaryTests.CompileBinaryExpression(equal, isNull[i] == isNull[j]);
                }
            }
        }


        [Fact]
        public static void CanPerformEqualityOnNullableWithoutOperatorsToConstantNull()
        {
            var nullConst = Expression.Constant(null, typeof(UselessValue?));
            var uvConst = Expression.Constant(new UselessValue(), typeof(UselessValue?));
            var exp = Expression.Lambda<Func<bool>>(Expression.Equal(nullConst, uvConst));
            var func = exp.CompileToTestMethod();
            Assert.False(func());

            exp = Expression.Lambda<Func<bool>>(Expression.Equal(uvConst, nullConst));
            func = exp.CompileToTestMethod();
            Assert.False(func());
        }

        [Fact]
        public static void CanPerformInequalityOnNullableWithoutOperatorsToConstantNull()
        {
            var nullConst = Expression.Constant(null, typeof(UselessValue?));
            var uvConst = Expression.Constant(new UselessValue(), typeof(UselessValue?));
            var exp = Expression.Lambda<Func<bool>>(Expression.NotEqual(nullConst, uvConst));
            var func = exp.CompileToTestMethod();
            Assert.True(func());

            exp = Expression.Lambda<Func<bool>>(Expression.NotEqual(uvConst, nullConst));
            func = exp.CompileToTestMethod();
            Assert.True(func());
        }

        // DBNull having a different type code to other objects could result in bugs surrounding it if
        // that type code got incorrectly used.

        [Fact]
        public static void CanCompareDBNullEqual()
        {
            var x = Expression.Parameter(typeof(DBNull));
            var y = Expression.Parameter(typeof(DBNull));
            var lambda = Expression.Lambda<Func<DBNull, DBNull, bool>>(Expression.Equal(x, y), x, y);
            var func = lambda.CompileToTestMethod();
            foreach(var xVal in new[] { DBNull.Value, null})
                foreach(var yVal in new[] { DBNull.Value, null})
                    Assert.Equal(xVal == yVal, func(xVal, yVal));
        }

        [Fact]
        public static void CanCompareDBNullNotEqual()
        {
            var x = Expression.Parameter(typeof(DBNull));
            var y = Expression.Parameter(typeof(DBNull));
            var lambda = Expression.Lambda<Func<DBNull, DBNull, bool>>(Expression.NotEqual(x, y), x, y);
            var func = lambda.CompileToTestMethod();
            foreach(var xVal in new[] { DBNull.Value, null})
                foreach(var yVal in new[] { DBNull.Value, null})
                    Assert.Equal(xVal != yVal, func(xVal, yVal));
        }

        public struct UselessValue
        {
        }

        public class TestClass { }
        public enum TestEnum { }
    }
}
