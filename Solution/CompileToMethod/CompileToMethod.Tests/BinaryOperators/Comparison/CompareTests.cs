// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class BinaryGreaterThanOrEqualTests
    {
        public static IEnumerable<object[]> TestData()
        {
            yield return new object[] { new byte[] { 0, 1, byte.MaxValue } };
            yield return new object[] { new char[] { '\0', '\b', 'A', '\uffff' } };
            yield return new object[] { new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue } };
            yield return new object[] { new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN } };
            yield return new object[] { new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN } };
            yield return new object[] { new int[] { 0, 1, -1, int.MinValue, int.MaxValue } };
            yield return new object[] { new long[] { 0, 1, -1, long.MinValue, long.MaxValue } };
            yield return new object[] { new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue } };
            yield return new object[] { new short[] { 0, 1, -1, short.MinValue, short.MaxValue } };
            yield return new object[] { new uint[] { 0, 1, uint.MaxValue } };
            yield return new object[] { new ulong[] { 0, 1, ulong.MaxValue } };
            yield return new object[] { new ushort[] { 0, 1, ushort.MaxValue } };

            yield return new object[] { new byte?[] { null, 0, 1, byte.MaxValue } };
            yield return new object[] { new char?[] { null, '\0', '\b', 'A', '\uffff' } };
            yield return new object[] { new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue } };
            yield return new object[] { new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN } };
            yield return new object[] { new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN } };
            yield return new object[] { new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue } };
            yield return new object[] { new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue } };
            yield return new object[] { new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue } };
            yield return new object[] { new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue } };
            yield return new object[] { new uint?[] { null, 0, 1, uint.MaxValue } };
            yield return new object[] { new ulong?[] { null, 0, 1, ulong.MaxValue } };
            yield return new object[] { new ushort?[] { null, 0, 1, ushort.MaxValue } };
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public static void GreaterThanOrEqual(Array array)
        {
            Type type = array.GetType().GetElementType();
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    object a = array.GetValue(i);
                    object b = array.GetValue(j);
                    BinaryExpression equal = Expression.GreaterThanOrEqual(Expression.Constant(a, type), Expression.Constant(b, type));
                    GeneralBinaryTests.CompileBinaryExpression(equal, GeneralBinaryTests.CustomGreaterThanOrEqual(a, b));
                }
            }
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public static void GreaterThan(Array array)
        {
            Type type = array.GetType().GetElementType();
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    object a = array.GetValue(i);
                    object b = array.GetValue(j);
                    BinaryExpression equal = Expression.GreaterThan(Expression.Constant(a, type), Expression.Constant(b, type));
                    GeneralBinaryTests.CompileBinaryExpression(equal, GeneralBinaryTests.CustomGreaterThan(a, b));
                }
            }
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public static void LessThanOrEqual(Array array)
        {
            Type type = array.GetType().GetElementType();
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    object a = array.GetValue(i);
                    object b = array.GetValue(j);
                    BinaryExpression equal = Expression.LessThanOrEqual(Expression.Constant(a, type), Expression.Constant(b, type));
                    GeneralBinaryTests.CompileBinaryExpression(equal, GeneralBinaryTests.CustomLessThanOrEqual(a, b));
                }
            }
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public static void LessThan(Array array)
        {
            Type type = array.GetType().GetElementType();
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    object a = array.GetValue(i);
                    object b = array.GetValue(j);
                    BinaryExpression equal = Expression.LessThan(Expression.Constant(a, type), Expression.Constant(b, type));
                    GeneralBinaryTests.CompileBinaryExpression(equal, GeneralBinaryTests.CustomLessThan(a, b));
                }
            }
        }
    }
}
