// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class ReferenceEqual : ReferenceEqualityTests
    {
        [Theory]
        [MemberData(nameof(ReferenceObjectsData))]
        public void TrueOnSame(object item)
        {
            Expression exp = Expression.ReferenceEqual(
                    Expression.Constant(item, item.GetType()),
                    Expression.Constant(item, item.GetType())
                );
            Assert.True(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ReferenceTypesData))]
        public void TrueOnBothNull(Type type)
        {
            Expression exp = Expression.ReferenceEqual(
                Expression.Constant(null, type),
                Expression.Constant(null, type)
                );
            Assert.True(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ReferenceObjectsData))]
        public void FalseIfLeftNull(object item)
        {
            Expression exp = Expression.ReferenceEqual(
                    Expression.Constant(null, item.GetType()),
                    Expression.Constant(item, item.GetType())
                );
            Assert.False(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ReferenceObjectsData))]
        public void FalseIfRightNull(object item)
        {
            Expression exp = Expression.ReferenceEqual(
                    Expression.Constant(item, item.GetType()),
                    Expression.Constant(null, item.GetType())
                );
            Assert.False(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(DifferentObjects))]
        public void FalseIfDifferentObjectsAsObject(object x, object y)
        {
            Expression exp = Expression.ReferenceEqual(
                    Expression.Constant(x, typeof(object)),
                    Expression.Constant(y, typeof(object))
                );
            Assert.False(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(DifferentObjects))]
        public void FalseIfDifferentObjectsOwnType(object x, object y)
        {
            Expression exp = Expression.ReferenceEqual(
                    Expression.Constant(x),
                    Expression.Constant(y)
                );
            Assert.False(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }


        [Theory]
        [MemberData(nameof(ComparableValuesData))]
        public void TrueOnSameViaInterface(object item)
        {
            Expression exp = Expression.ReferenceEqual(
                Expression.Constant(item, typeof(IComparable)),
                Expression.Constant(item, typeof(IComparable))
            );
            Assert.True(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(DifferentComparableValues))]
        public void FalseOnDifferentViaInterface(object x, object y)
        {
            Expression exp = Expression.ReferenceEqual(
                Expression.Constant(x, typeof(IComparable)),
                Expression.Constant(y, typeof(IComparable))
            );
            Assert.False(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ComparableReferenceTypesData))]
        public void TrueOnSameLeftViaInterface(object item)
        {
            Expression exp = Expression.ReferenceEqual(
                Expression.Convert(Expression.Constant(item), typeof(IComparable)),
                Expression.Constant(item)
            );
            Assert.True(Expression.Lambda<Func<bool>>(exp).CompileToTestMethodSimple()());
        }

        [Theory]
        [MemberData(nameof(ComparableReferenceTypesData))]
        public void TrueOnSameRightViaInterface(object item)
        {
            Expression exp = Expression.ReferenceEqual(
                Expression.Constant(item),
                Expression.Convert(Expression.Constant(item), typeof(IComparable))
            );
            Assert.True(Expression.Lambda<Func<bool>>(exp).CompileToTestMethodSimple()());
        }

    }
}
