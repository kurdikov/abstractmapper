// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class ReferenceNotEqual : ReferenceEqualityTests
    {
        [Theory]
        [MemberData(nameof(ReferenceObjectsData))]
        public void FalseOnSame(object item)
        {
            Expression exp = Expression.ReferenceNotEqual(
                    Expression.Constant(item, item.GetType()),
                    Expression.Constant(item, item.GetType())
                );
            Assert.False(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ReferenceTypesData))]
        public void FalseOnBothNull(Type type)
        {
            Expression exp = Expression.ReferenceNotEqual(
                Expression.Constant(null, type),
                Expression.Constant(null, type)
                );
            Assert.False(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ReferenceObjectsData))]
        public void TrueIfLeftNull(object item)
        {
            Expression exp = Expression.ReferenceNotEqual(
                    Expression.Constant(null, item.GetType()),
                    Expression.Constant(item, item.GetType())
                );
            Assert.True(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ReferenceObjectsData))]
        public void TrueIfRightNull(object item)
        {
            Expression exp = Expression.ReferenceNotEqual(
                    Expression.Constant(item, item.GetType()),
                    Expression.Constant(null, item.GetType())
                );
            Assert.True(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(DifferentObjects))]
        public void TrueIfDifferentObjectsAsObject(object x, object y)
        {
            Expression exp = Expression.ReferenceNotEqual(
                    Expression.Constant(x, typeof(object)),
                    Expression.Constant(y, typeof(object))
                );
            Assert.True(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(DifferentObjects))]
        public void TrueIfDifferentObjectsOwnType(object x, object y)
        {
            Expression exp = Expression.ReferenceNotEqual(
                    Expression.Constant(x),
                    Expression.Constant(y)
                );
            Assert.True(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ComparableValuesData))]
        public void FalseOnSameViaInterface(object item)
        {
            Expression exp = Expression.ReferenceNotEqual(
                Expression.Constant(item, typeof(IComparable)),
                Expression.Constant(item, typeof(IComparable))
            );
            Assert.False(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(DifferentComparableValues))]
        public void TrueOnDifferentViaInterface(object x, object y)
        {
            Expression exp = Expression.ReferenceNotEqual(
                Expression.Constant(x, typeof(IComparable)),
                Expression.Constant(y, typeof(IComparable))
            );
            Assert.True(Expression.Lambda<Func<bool>>(exp).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ComparableReferenceTypesData))]
        public void FalseOnSameLeftViaInterface(object item)
        {
            Expression exp = Expression.ReferenceNotEqual(
                Expression.Convert(Expression.Constant(item), typeof(IComparable)),
                Expression.Constant(item)
            );
            Assert.False(Expression.Lambda<Func<bool>>(exp).CompileToTestMethodSimple()());
        }

        [Theory]
        [MemberData(nameof(ComparableReferenceTypesData))]
        public void FalseOnSameRightViaInterface(object item)
        {
            Expression exp = Expression.ReferenceNotEqual(
                Expression.Constant(item),
                Expression.Convert(Expression.Constant(item), typeof(IComparable))
            );
            Assert.False(Expression.Lambda<Func<bool>>(exp).CompileToTestMethodSimple()());
        }

    }
}
