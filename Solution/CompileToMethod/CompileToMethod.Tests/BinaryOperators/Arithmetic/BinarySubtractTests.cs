// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class BinarySubtractTests
    {
        #region Test methods

        [Fact]
        public static void CheckUShortSubtractTest()
        {
            ushort[] array = new ushort[] { 0, 1, ushort.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyUShortSubtract(array[i], array[j]);
                    VerifyUShortSubtractOvf(array[i], array[j]);
                }
            }
        }

        [Fact]
        public static void CheckShortSubtractTest()
        {
            short[] array = new short[] { 0, 1, -1, short.MinValue, short.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyShortSubtract(array[i], array[j]);
                    VerifyShortSubtractOvf(array[i], array[j]);
                }
            }
        }

        [Fact]
        public static void CheckUIntSubtractTest()
        {
            uint[] array = new uint[] { 0, 1, uint.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyUIntSubtract(array[i], array[j]);
                    VerifyUIntSubtractOvf(array[i], array[j]);
                }
            }
        }

        [Fact]
        public static void CheckIntSubtractTest()
        {
            int[] array = new int[] { 0, 1, -1, int.MinValue, int.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyIntSubtract(array[i], array[j]);
                    VerifyIntSubtractOvf(array[i], array[j]);
                }
            }
        }

        [Fact]
        public static void CheckULongSubtractTest()
        {
            ulong[] array = new ulong[] { 0, 1, ulong.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyULongSubtract(array[i], array[j]);
                    VerifyULongSubtractOvf(array[i], array[j]);
                }
            }
        }

        [Fact]
        public static void CheckLongSubtractTest()
        {
            long[] array = new long[] { 0, 1, -1, long.MinValue, long.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyLongSubtract(array[i], array[j]);
                    VerifyLongSubtractOvf(array[i], array[j]);
                }
            }
        }

        [Fact]
        public static void CheckFloatSubtractTest()
        {
            float[] array = new float[] { 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyFloatSubtract(array[i], array[j]);
                }
            }
        }

        [Fact]
        public static void CheckDoubleSubtractTest()
        {
            double[] array = new double[] { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyDoubleSubtract(array[i], array[j]);
                }
            }
        }

        [Fact]
        public static void CheckDecimalSubtractTest()
        {
            decimal[] array = new decimal[] { decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    VerifyDecimalSubtract(array[i], array[j]);
                }
            }
        }

        #endregion

        #region Test verifiers

        private static void VerifyUShortSubtract(ushort a, ushort b)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(ushort)),
                        Expression.Constant(b, typeof(ushort))),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)(a - b)), f());
        }

        private static void VerifyUShortSubtractOvf(ushort a, ushort b)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    Expression.SubtractChecked(
                        Expression.Constant(a, typeof(ushort)),
                        Expression.Constant(b, typeof(ushort))),
                    Enumerable.Empty<ParameterExpression>());
            Func<ushort> f = e.CompileToTestMethod();

            ushort expected = 0;
            try
            {
                expected = checked((ushort)(a - b));
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyShortSubtract(short a, short b)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(short)),
                        Expression.Constant(b, typeof(short))),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)(a - b)), f());
        }

        private static void VerifyShortSubtractOvf(short a, short b)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    Expression.SubtractChecked(
                        Expression.Constant(a, typeof(short)),
                        Expression.Constant(b, typeof(short))),
                    Enumerable.Empty<ParameterExpression>());
            Func<short> f = e.CompileToTestMethod();

            short expected = 0;
            try
            {
                expected = checked((short)(a - b));
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyUIntSubtract(uint a, uint b)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(uint)),
                        Expression.Constant(b, typeof(uint))),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(unchecked(a - b), f());
        }

        private static void VerifyUIntSubtractOvf(uint a, uint b)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    Expression.SubtractChecked(
                        Expression.Constant(a, typeof(uint)),
                        Expression.Constant(b, typeof(uint))),
                    Enumerable.Empty<ParameterExpression>());
            Func<uint> f = e.CompileToTestMethod();

            uint expected = 0;
            try
            {
                expected = checked(a - b);
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());

        }

        private static void VerifyIntSubtract(int a, int b)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(int)),
                        Expression.Constant(b, typeof(int))),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(unchecked(a - b), f());
        }

        private static void VerifyIntSubtractOvf(int a, int b)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    Expression.SubtractChecked(
                        Expression.Constant(a, typeof(int)),
                        Expression.Constant(b, typeof(int))),
                    Enumerable.Empty<ParameterExpression>());
            Func<int> f = e.CompileToTestMethod();

            int expected = 0;
            try
            {
                expected = checked(a - b);
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyULongSubtract(ulong a, ulong b)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(ulong)),
                        Expression.Constant(b, typeof(ulong))),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(unchecked(a - b), f());
        }

        private static void VerifyULongSubtractOvf(ulong a, ulong b)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    Expression.SubtractChecked(
                        Expression.Constant(a, typeof(ulong)),
                        Expression.Constant(b, typeof(ulong))),
                    Enumerable.Empty<ParameterExpression>());
            Func<ulong> f = e.CompileToTestMethod();

            ulong expected = 0;
            try
            {
                expected = checked(a - b);
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());

        }

        private static void VerifyLongSubtract(long a, long b)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(long)),
                        Expression.Constant(b, typeof(long))),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(unchecked(a - b), f());
        }

        private static void VerifyLongSubtractOvf(long a, long b)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    Expression.SubtractChecked(
                        Expression.Constant(a, typeof(long)),
                        Expression.Constant(b, typeof(long))),
                    Enumerable.Empty<ParameterExpression>());
            Func<long> f = e.CompileToTestMethod();

            long expected = 0;
            try
            {
                expected = checked(a - b);
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifyFloatSubtract(float a, float b)
        {
            Expression<Func<float>> e =
                Expression.Lambda<Func<float>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(float)),
                        Expression.Constant(b, typeof(float))),
                    Enumerable.Empty<ParameterExpression>());
            Func<float> f = e.CompileToTestMethod();

            Assert.Equal(a - b, f());
        }

        private static void VerifyDoubleSubtract(double a, double b)
        {
            Expression<Func<double>> e =
                Expression.Lambda<Func<double>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(double)),
                        Expression.Constant(b, typeof(double))),
                    Enumerable.Empty<ParameterExpression>());
            Func<double> f = e.CompileToTestMethod();

            Assert.Equal(a - b, f());
        }

        private static void VerifyDecimalSubtract(decimal a, decimal b)
        {
            Expression<Func<decimal>> e =
                Expression.Lambda<Func<decimal>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(decimal)),
                        Expression.Constant(b, typeof(decimal))),
                    Enumerable.Empty<ParameterExpression>());
            Func<decimal> f = e.CompileToTestMethod();

            decimal expected = 0;
            try
            {
                expected = a - b;
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        #endregion

#if NETCOREAPP1_1
        [Fact(Skip = "https://github.com/dotnet/corefx/issues/11907")]
#else
        [Fact]
#endif
        public static void Subtract_MultipleOverloads_CorrectlyResolvesOperator1()
        {
            BinaryExpression subtract = Expression.Subtract(Expression.Constant(new DateTime(100)), Expression.Constant(new DateTime(10)));
            Func<TimeSpan> lambda = Expression.Lambda<Func<TimeSpan>>(subtract).CompileToTestMethod();
            Assert.Equal(new TimeSpan(90), lambda());
        }

        [Fact]
        public static void Subtract_MultipleOverloads_CorrectlyResolvesOperator2()
        {
            BinaryExpression subtract = Expression.Subtract(Expression.Constant(new DateTime(100)), Expression.Constant(new TimeSpan(10)));
            Func<DateTime> lambda = Expression.Lambda<Func<DateTime>>(subtract).CompileToTestMethod();
            Assert.Equal(new DateTime(90), lambda());
        }

        public class BaseClass
        {
            public BaseClass(int value) { Value = value; }
            public int Value { get; }

            public static BaseClass operator -(BaseClass i1, BaseClass i2) => new BaseClass(i1.Value - i2.Value);
        }

        public class SubClass : BaseClass
        {
            public SubClass(int value) : base(value) { }
        }

    }
}
