// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class BinaryShiftTests
    {
        #region Test methods

        [Fact]
        public static void CheckByteShiftTest()
        {
            byte[] array = new byte[] { 0, 1, byte.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyByteShift(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableByteShiftTest()
        {
            byte?[] array = { 0, 1, byte.MaxValue, null };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableByteShift(array[i]);
            }
        }

        [Fact]
        public static void CheckSByteShiftTest()
        {
            sbyte[] array = new sbyte[] { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifySByteShift(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableSByteShiftTest()
        {
            sbyte?[] array = { 0, 1, -1, sbyte.MinValue, sbyte.MaxValue, null };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableSByteShift(array[i]);
            }
        }

        [Fact]
        public static void CheckUShortShiftTest()
        {
            ushort[] array = new ushort[] { 0, 1, ushort.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyUShortShift(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableUShortShiftTest()
        {
            ushort?[] array = { 0, 1, ushort.MaxValue, null };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableUShortShift(array[i]);
            }
        }

        [Fact]
        public static void CheckShortShiftTest()
        {
            short[] array = new short[] { 0, 1, -1, short.MinValue, short.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyShortShift(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableShortShiftTest()
        {
            short?[] array = { 0, 1, -1, short.MinValue, short.MaxValue, null };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableShortShift(array[i]);
            }
        }

        [Fact]
        public static void CheckUIntShiftTest()
        {
            uint[] array = new uint[] { 0, 1, uint.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyUIntShift(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableUIntShiftTest()
        {
            uint?[] array = { 0, 1, uint.MaxValue, null };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableUIntShift(array[i]);
            }
        }

        [Fact]
        public static void CheckIntShiftTest()
        {
            int[] array = new int[] { 0, 1, -1, int.MinValue, int.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyIntShift(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableIntShiftTest()
        {
            int?[] array = { 0, 1, -1, int.MinValue, int.MaxValue, null };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableIntShift(array[i]);
            }
        }

        [Fact]
        public static void CheckULongShiftTest()
        {
            ulong[] array = new ulong[] { 0, 1, ulong.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyULongShift(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableULongShiftTest()
        {
            ulong?[] array = { 0, 1, ulong.MaxValue, null };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableULongShift(array[i]);
            }
        }

        [Fact]
        public static void CheckLongShiftTest()
        {
            long[] array = new long[] { 0, 1, -1, long.MinValue, long.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyLongShift(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableLongShiftTest()
        {
            long?[] array = { 0, 1, -1, long.MinValue, long.MaxValue, null };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableLongShift(array[i]);
            }
        }

        #endregion

        #region Test verifiers

        private static int[] s_shifts = new[] { int.MinValue, -1, 0, 1, 2, 31, 32, 63, 64, int.MaxValue };

        private static void VerifyByteShift(byte a)
        {
            foreach (var b in s_shifts)
            {
                VerifyByteShift(a, b, true);
                VerifyByteShift(a, b, false);
            }

            VerifyNullShift(a, true);
            VerifyNullShift(a, false);
        }

        private static void VerifyByteShift(byte a, int b, bool left)
        {
            Expression<Func<byte>> e =
                Expression.Lambda<Func<byte>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(byte)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(byte)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<byte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte)(left ? a << b : a >> b)), f());

            Expression<Func<byte?>> en =
                Expression.Lambda<Func<byte?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(byte)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(byte)),
                        Expression.Constant(b, typeof(int?)))
                    );

            Func<byte?> fn = en.CompileToTestMethod();

            Assert.Equal(unchecked((byte)(left ? a << b : a >> b)), fn());
        }

        private static void VerifyNullableByteShift(byte? a)
        {
            foreach (var b in s_shifts)
            {
                VerifyNullableByteShift(a, b, true);
                VerifyNullableByteShift(a, b, false);
            }

            VerifyNullableNullShift(a, true);
            VerifyNullableNullShift(a, false);
        }

        private static void VerifyNullableByteShift(byte? a, int b, bool left)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(byte?)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(byte?)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)(left ? a << b : a >> b)), f());

            e =
                Expression.Lambda<Func<byte?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(byte?)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(byte?)),
                        Expression.Constant(b, typeof(int?)))
                    );

            f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)(left ? a << b : a >> b)), f());
        }

        private static void VerifySByteShift(sbyte a)
        {
            foreach (var b in s_shifts)
            {
                VerifySByteShift(a, b, true);
                VerifySByteShift(a, b, false);
            }

            VerifyNullShift(a, true);
            VerifyNullShift(a, false);
        }

        private static void VerifySByteShift(sbyte a, int b, bool left)
        {
            Expression<Func<sbyte>> e =
                Expression.Lambda<Func<sbyte>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(sbyte)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(sbyte)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<sbyte> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)(left ? a << b : a >> b)), f());

            Expression<Func<sbyte?>> en =
                Expression.Lambda<Func<sbyte?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(sbyte)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(sbyte)),
                        Expression.Constant(b, typeof(int?)))
                    );

            Func<sbyte?> fn = en.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte)(left ? a << b : a >> b)), fn());
        }

        private static void VerifyNullableSByteShift(sbyte? a)
        {
            foreach (var b in s_shifts)
            {
                VerifyNullableSByteShift(a, b, true);
                VerifyNullableSByteShift(a, b, false);
            }

            VerifyNullableNullShift(a, true);
            VerifyNullableNullShift(a, false);
        }

        private static void VerifyNullableSByteShift(sbyte? a, int b, bool left)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(sbyte?)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(sbyte?)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)(left ? a << b : a >> b)), f());

            e =
                Expression.Lambda<Func<sbyte?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(sbyte?)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(sbyte?)),
                        Expression.Constant(b, typeof(int?)))
                    );

            f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)(left ? a << b : a >> b)), f());
        }

        private static void VerifyUShortShift(ushort a)
        {
            foreach (var b in s_shifts)
            {
                VerifyUShortShift(a, b, true);
                VerifyUShortShift(a, b, false);
            }

            VerifyNullShift(a, true);
            VerifyNullShift(a, false);
        }

        private static void VerifyUShortShift(ushort a, int b, bool left)
        {
            Expression<Func<ushort>> e =
                Expression.Lambda<Func<ushort>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(ushort)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(ushort)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<ushort> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)(left ? a << b : a >> b)), f());

            Expression<Func<ushort?>> en =
                Expression.Lambda<Func<ushort?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(ushort)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(ushort)),
                        Expression.Constant(b, typeof(int?)))
                    );

            Func<ushort?> fn = en.CompileToTestMethod();

            Assert.Equal(unchecked((ushort)(left ? a << b : a >> b)), fn());
        }

        private static void VerifyNullableUShortShift(ushort? a)
        {
            foreach (var b in s_shifts)
            {
                VerifyNullableUShortShift(a, b, true);
                VerifyNullableUShortShift(a, b, false);
            }

            VerifyNullableNullShift(a, true);
            VerifyNullableNullShift(a, false);
        }

        private static void VerifyNullableUShortShift(ushort? a, int b, bool left)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(ushort?)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(ushort?)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)(left ? a << b : a >> b)), f());

            e =
                Expression.Lambda<Func<ushort?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(ushort?)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(ushort?)),
                        Expression.Constant(b, typeof(int?)))
                    );

            f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)(left ? a << b : a >> b)), f());
        }

        private static void VerifyShortShift(short a)
        {
            foreach (var b in s_shifts)
            {
                VerifyShortShift(a, b, true);
                VerifyShortShift(a, b, false);
            }

            VerifyNullShift(a, true);
            VerifyNullShift(a, false);
        }

        private static void VerifyShortShift(short a, int b, bool left)
        {
            Expression<Func<short>> e =
                Expression.Lambda<Func<short>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(short)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(short)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<short> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short)(left ? a << b : a >> b)), f());

            Expression<Func<short?>> en =
                Expression.Lambda<Func<short?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(short)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(short)),
                        Expression.Constant(b, typeof(int?)))
                    );

            Func<short?> fn = en.CompileToTestMethod();

            Assert.Equal(unchecked((short)(left ? a << b : a >> b)), fn());
        }

        private static void VerifyNullableShortShift(short? a)
        {
            foreach (var b in s_shifts)
            {
                VerifyNullableShortShift(a, b, true);
                VerifyNullableShortShift(a, b, false);
            }

            VerifyNullableNullShift(a, true);
            VerifyNullableNullShift(a, false);
        }

        private static void VerifyNullableShortShift(short? a, int b, bool left)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(short?)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(short?)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)(left ? a << b : a >> b)), f());

            e =
                Expression.Lambda<Func<short?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(short?)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(short?)),
                        Expression.Constant(b, typeof(int?)))
                    );

            f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)(left ? a << b : a >> b)), f());
        }

        private static void VerifyUIntShift(uint a)
        {
            foreach (var b in s_shifts)
            {
                VerifyUIntShift(a, b, true);
                VerifyUIntShift(a, b, false);
            }

            VerifyNullShift(a, true);
            VerifyNullShift(a, false);
        }

        private static void VerifyUIntShift(uint a, int b, bool left)
        {
            Expression<Func<uint>> e =
                Expression.Lambda<Func<uint>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(uint)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(uint)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<uint> f = e.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, f());

            Expression<Func<uint?>> en =
                Expression.Lambda<Func<uint?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(uint)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(uint)),
                        Expression.Constant(b, typeof(int?)))
                    );

            Func<uint?> fn = en.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, fn());
        }

        private static void VerifyNullableUIntShift(uint? a)
        {
            foreach (var b in s_shifts)
            {
                VerifyNullableUIntShift(a, b, true);
                VerifyNullableUIntShift(a, b, false);
            }

            VerifyNullableNullShift(a, true);
            VerifyNullableNullShift(a, false);
        }

        private static void VerifyNullableUIntShift(uint? a, int b, bool left)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(uint?)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(uint?)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, f());

            e =
                Expression.Lambda<Func<uint?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(uint?)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(uint?)),
                        Expression.Constant(b, typeof(int?)))
                    );

            f = e.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, f());
        }

        private static void VerifyIntShift(int a)
        {
            foreach (var b in s_shifts)
            {
                VerifyIntShift(a, b, true);
                VerifyIntShift(a, b, false);
            }

            VerifyNullShift(a, true);
            VerifyNullShift(a, false);
        }

        private static void VerifyIntShift(int a, int b, bool left)
        {
            Expression<Func<int>> e =
                Expression.Lambda<Func<int>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(int)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(int)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<int> f = e.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, f());

            Expression<Func<int?>> en =
                Expression.Lambda<Func<int?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(int)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(int)),
                        Expression.Constant(b, typeof(int?)))
                    );

            Func<int?> fn = en.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, fn());
        }

        private static void VerifyNullableIntShift(int? a)
        {
            foreach (var b in s_shifts)
            {
                VerifyNullableIntShift(a, b, true);
                VerifyNullableIntShift(a, b, false);
            }

            VerifyNullableNullShift(a, true);
            VerifyNullableNullShift(a, false);
        }

        private static void VerifyNullableIntShift(int? a, int b, bool left)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(int?)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(int?)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, f());

            e =
                Expression.Lambda<Func<int?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(int?)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(int?)),
                        Expression.Constant(b, typeof(int?)))
                    );

            f = e.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, f());
        }

        private static void VerifyULongShift(ulong a)
        {
            foreach (var b in s_shifts)
            {
                VerifyULongShift(a, b, true);
                VerifyULongShift(a, b, false);
            }

            VerifyNullShift(a, true);
            VerifyNullShift(a, false);
        }

        private static void VerifyULongShift(ulong a, int b, bool left)
        {
            Expression<Func<ulong>> e =
                Expression.Lambda<Func<ulong>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(ulong)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(ulong)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<ulong> f = e.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, f());

            Expression<Func<ulong?>> en =
                Expression.Lambda<Func<ulong?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(ulong)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(ulong)),
                        Expression.Constant(b, typeof(int?)))
                    );

            Func<ulong?> fn = en.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, fn());
        }

        private static void VerifyNullableULongShift(ulong? a)
        {
            foreach (var b in s_shifts)
            {
                VerifyNullableULongShift(a, b, true);
                VerifyNullableULongShift(a, b, false);
            }

            VerifyNullableNullShift(a, true);
            VerifyNullableNullShift(a, false);
        }

        private static void VerifyNullableULongShift(ulong? a, int b, bool left)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(ulong?)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(ulong?)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, f());

            e =
                Expression.Lambda<Func<ulong?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(ulong?)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(ulong?)),
                        Expression.Constant(b, typeof(int?)))
                    );

            f = e.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, f());
        }

        private static void VerifyLongShift(long a)
        {
            foreach (var b in s_shifts)
            {
                VerifyLongShift(a, b, true);
                VerifyLongShift(a, b, false);
            }

            VerifyNullShift(a, true);
            VerifyNullShift(a, false);
        }

        private static void VerifyLongShift(long a, int b, bool left)
        {
            Expression<Func<long>> e =
                Expression.Lambda<Func<long>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(long)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(long)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<long> f = e.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, f());

            Expression<Func<long?>> en =
                Expression.Lambda<Func<long?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(long)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(long)),
                        Expression.Constant(b, typeof(int?)))
                    );

            Func<long?> fn = en.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, fn());
        }

        private static void VerifyNullableLongShift(long? a)
        {
            foreach (var b in s_shifts)
            {
                VerifyNullableLongShift(a, b, true);
                VerifyNullableLongShift(a, b, false);
            }

            VerifyNullableNullShift(a, true);
            VerifyNullableNullShift(a, false);
        }

        private static void VerifyNullableLongShift(long? a, int b, bool left)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(long?)),
                        Expression.Constant(b, typeof(int)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(long?)),
                        Expression.Constant(b, typeof(int)))
                    );

            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, f());

            e =
                Expression.Lambda<Func<long?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(long?)),
                        Expression.Constant(b, typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(long?)),
                        Expression.Constant(b, typeof(int?)))
                    );

            f = e.CompileToTestMethod();

            Assert.Equal(left ? a << b : a >> b, f());
        }

        private static void VerifyNullShift<T>(T a, bool left) where T : struct
        {
            Expression<Func<T?>> e =
                Expression.Lambda<Func<T?>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(T)),
                        Expression.Default(typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(T)),
                        Expression.Default(typeof(int?)))
                    );

            Func<T?> f = e.CompileToTestMethod();

            Assert.Null(f());
        }

        private static void VerifyNullableNullShift<T>(T a, bool left)
        {
            Expression<Func<T>> e =
                Expression.Lambda<Func<T>>(
                    left
                    ?
                    Expression.LeftShift(
                        Expression.Constant(a, typeof(T)),
                        Expression.Default(typeof(int?)))
                    :
                    Expression.RightShift(
                        Expression.Constant(a, typeof(T)),
                        Expression.Default(typeof(int?)))
                    );

            Func<T> f = e.CompileToTestMethod();

            Assert.Null(f());
        }

        #endregion

        [Fact]
        public static void CannotReduceLeft()
        {
            Expression exp = Expression.LeftShift(Expression.Constant(0), Expression.Constant(0));
            Assert.False(exp.CanReduce);
            Assert.Same(exp, exp.Reduce());
            AssertExtensions.Throws<ArgumentException>(null, () => exp.ReduceAndCheck());
        }

        [Fact]
        public static void CannotReduceRight()
        {
            Expression exp = Expression.RightShift(Expression.Constant(0), Expression.Constant(0));
            Assert.False(exp.CanReduce);
            Assert.Same(exp, exp.Reduce());
            AssertExtensions.Throws<ArgumentException>(null, () => exp.ReduceAndCheck());
        }

        [Fact]
        public static void LeftThrowsOnLeftNull()
        {
            AssertExtensions.Throws<ArgumentNullException>("left", () => Expression.LeftShift(null, Expression.Constant("")));
        }

        [Fact]
        public static void LeftThrowsOnRightNull()
        {
            AssertExtensions.Throws<ArgumentNullException>("right", () => Expression.LeftShift(Expression.Constant(""), null));
        }

        [Fact]
        public static void RightThrowsOnLeftNull()
        {
            AssertExtensions.Throws<ArgumentNullException>("left", () => Expression.RightShift(null, Expression.Constant("")));
        }

        [Fact]
        public static void RightThrowsOnRightNull()
        {
            AssertExtensions.Throws<ArgumentNullException>("right", () => Expression.RightShift(Expression.Constant(""), null));
        }

        private static class Unreadable<T>
        {
            public static T WriteOnly
            {
                set { }
            }
        }

        [Fact]
        public static void LeftThrowsOnLeftUnreadable()
        {
            Expression value = Expression.Property(null, typeof(Unreadable<int>), "WriteOnly");
            AssertExtensions.Throws<ArgumentException>("left", () => Expression.LeftShift(value, Expression.Constant(1)));
        }

        [Fact]
        public static void LeftThrowsOnRightUnreadable()
        {
            Expression value = Expression.Property(null, typeof(Unreadable<int>), "WriteOnly");
            AssertExtensions.Throws<ArgumentException>("right", () => Expression.LeftShift(Expression.Constant(1), value));
        }

        [Fact]
        public static void RightThrowsOnLeftUnreadable()
        {
            Expression value = Expression.Property(null, typeof(Unreadable<int>), "WriteOnly");
            AssertExtensions.Throws<ArgumentException>("left", () => Expression.RightShift(value, Expression.Constant(1)));
        }

        [Fact]
        public static void RightThrowsOnRightUnreadable()
        {
            Expression value = Expression.Property(null, typeof(Unreadable<int>), "WriteOnly");
            AssertExtensions.Throws<ArgumentException>("right", () => Expression.RightShift(Expression.Constant(1), value));
        }

        [Fact]
        public static void ToStringTest()
        {
            BinaryExpression e1 = Expression.LeftShift(Expression.Parameter(typeof(int), "a"), Expression.Parameter(typeof(int), "b"));
            Assert.Equal("(a << b)", e1.ToString());

            BinaryExpression e2 = Expression.RightShift(Expression.Parameter(typeof(int), "a"), Expression.Parameter(typeof(int), "b"));
            Assert.Equal("(a >> b)", e2.ToString());
        }

        [Theory, InlineData(typeof(E)), InlineData(typeof(El)), InlineData(typeof(string))]
        public static void IncorrectLHSTypes(Type type)
        {
            DefaultExpression lhs = Expression.Default(type);
            ConstantExpression rhs = Expression.Constant(0);
            Assert.Throws<InvalidOperationException>(() => Expression.LeftShift(lhs, rhs));
            Assert.Throws<InvalidOperationException>(() => Expression.RightShift(lhs, rhs));
        }

        [Theory, InlineData(typeof(E)), InlineData(typeof(El)), InlineData(typeof(string)), InlineData(typeof(long)),
         InlineData(typeof(short)), InlineData(typeof(uint))]
        public static void IncorrectRHSTypes(Type type)
        {
            ConstantExpression lhs = Expression.Constant(0);
            DefaultExpression rhs = Expression.Default(type);
            Assert.Throws<InvalidOperationException>(() => Expression.LeftShift(lhs, rhs));
            Assert.Throws<InvalidOperationException>(() => Expression.RightShift(lhs, rhs));
        }
    }
}
