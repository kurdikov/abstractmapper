// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class Assign
    {
        public class PropertyAndFields
        {
#pragma warning disable 649 // Assigned through expressions.
            public string StringProperty { get; set; }
            public string StringField;
            public readonly string ReadonlyStringField;
            public string ReadonlyStringProperty { get { return ""; } }
            public static string StaticStringProperty { get; set; }
            public static string StaticStringField;
            public static readonly string ReadonlyStaticStringField;
            public static string ReadonlyStaticStringProperty { get { return ""; } }
            public const string ConstantString = "Constant";
            public string WriteOnlyString { set { } }
            public static string WriteOnlyStaticString { set { } }

            public BaseClass BaseClassProperty { get; set; }

            public int Int32Property { get; set; }
            public int Int32Field;
            public readonly int ReadonlyInt32Field;
            public int ReadonlyInt32Property { get { return 42; } }
            public static int StaticInt32Property1 { get; set; }
            public static int StaticInt32Field;
            public static readonly int ReadonlyStaticInt32Field;
            public static int ReadonlyStaticInt32Property { get { return 321; } }
            public const int ConstantInt32 = 12;
            public int WriteOnlyInt32 { set { } }
            public static int WriteOnlyStaticInt32 { set { } }

            private int _underlyingIndexerField1;
            public int this[int i]
            {
                get { return _underlyingIndexerField1; }
                set
                {
                    Assert.Equal(1, i);
                    _underlyingIndexerField1 = value;
                }
            }

            private int _underlyingIndexerField2;
            public int this[int i, int j]
            {
                get { return _underlyingIndexerField2; }
                set
                {
                    Assert.Equal(1, i);
                    Assert.Equal(2, j);
                    _underlyingIndexerField2 = value;
                }
            }

            public static int StaticInt32Property2 { get; set; }

#pragma warning restore 649
        }

        private class ReadOnlyIndexer
        {
            public int this[int i] { get { return 0; } }
        }

        private class WriteOnlyIndexer
        {
            public int this[int i] { set { } }
        }

        public struct StructWithPropertiesAndFields
        {
            private int _underlyingIndexerField;
            public int this[int i]
            {
                get { return _underlyingIndexerField; }
                set
                {
                    Assert.Equal(1, i);
                    _underlyingIndexerField = value;
                }
            }
        }

        private static IEnumerable<object[]> ReadOnlyExpressions()
        {
            Expression obj = Expression.Constant(new PropertyAndFields());
            yield return new object[] { Expression.Field(obj, typeof(PropertyAndFields), nameof(PropertyAndFields.ReadonlyInt32Field)) };
            yield return new object[] { Expression.Property(obj, typeof(PropertyAndFields), nameof(PropertyAndFields.ReadonlyInt32Property)) };
            yield return new object[] { Expression.Field(obj, typeof(PropertyAndFields), nameof(PropertyAndFields.ReadonlyStringField)) };
            yield return new object[] { Expression.Property(obj, typeof(PropertyAndFields), nameof(PropertyAndFields.ReadonlyStringProperty)) };
            yield return new object[] { Expression.Field(null, typeof(PropertyAndFields), nameof(PropertyAndFields.ConstantInt32)) };
            yield return new object[] { Expression.Field(null, typeof(PropertyAndFields), nameof(PropertyAndFields.ConstantString)) };
            yield return new object[] { Expression.Field(null, typeof(PropertyAndFields), nameof(PropertyAndFields.ReadonlyStaticInt32Field)) };
            yield return new object[] { Expression.Property(null, typeof(PropertyAndFields), nameof(PropertyAndFields.ReadonlyStaticInt32Property)) };
            yield return new object[] { Expression.Field(null, typeof(PropertyAndFields), nameof(PropertyAndFields.ReadonlyStaticStringField)) };
            yield return new object[] { Expression.Property(null, typeof(PropertyAndFields), nameof(PropertyAndFields.ReadonlyStaticStringProperty)) };
            yield return new object[] { Expression.Default(typeof(int)) };
            yield return new object[] { Expression.Default(typeof(string)) };
            yield return new object[] { Expression.Default(typeof(DateTime)) };
            yield return new object[] { Expression.Constant(1) };
            yield return new object[] { Expression.Property(Expression.Constant(new ReadOnlyIndexer()), "Item", new Expression[] { Expression.Constant(1) }) };
        }

        private static IEnumerable<object> WriteOnlyExpressions()
        {
            Expression obj = Expression.Constant(new PropertyAndFields());
            yield return new object[] { Expression.Property(obj, typeof(PropertyAndFields), nameof(PropertyAndFields.WriteOnlyInt32)) };
            yield return new object[] { Expression.Property(obj, typeof(PropertyAndFields), nameof(PropertyAndFields.WriteOnlyString)) };
            yield return new object[] { Expression.Property(null, typeof(PropertyAndFields), nameof(PropertyAndFields.WriteOnlyStaticInt32)) };
            yield return new object[] { Expression.Property(null, typeof(PropertyAndFields), nameof(PropertyAndFields.WriteOnlyStaticString)) };
            yield return new object[] { Expression.Property(Expression.Constant(new WriteOnlyIndexer()), "Item", new Expression[] { Expression.Constant(1) }) };
        }

        private static IEnumerable<object> MemberAssignments()
        {
            Expression obj = Expression.Constant(new PropertyAndFields());
            yield return new object[] { Expression.Field(null, typeof(PropertyAndFields), nameof(PropertyAndFields.StaticInt32Field)), 1 };
            yield return new object[] { Expression.Property(null, typeof(PropertyAndFields), nameof(PropertyAndFields.StaticInt32Property1)), 2 };
            yield return new object[] { Expression.Field(obj, typeof(PropertyAndFields), nameof(PropertyAndFields.Int32Field)), 3 };
            yield return new object[] { Expression.Property(obj, typeof(PropertyAndFields), nameof(PropertyAndFields.Int32Property)), 4 };
            yield return new object[] { Expression.Field(null, typeof(PropertyAndFields), nameof(PropertyAndFields.StaticStringField)), "a" };
            yield return new object[] { Expression.Property(null, typeof(PropertyAndFields), nameof(PropertyAndFields.StaticStringProperty)), "b" };
            yield return new object[] { Expression.Field(obj, typeof(PropertyAndFields), nameof(PropertyAndFields.StringField)), "c" };
            yield return new object[] { Expression.Property(obj, typeof(PropertyAndFields), nameof(PropertyAndFields.StringProperty)), "d" };

            yield return new object[] { Expression.Property(obj, typeof(PropertyAndFields), nameof(PropertyAndFields.BaseClassProperty)), new BaseClass() };
            yield return new object[] { Expression.Property(obj, typeof(PropertyAndFields), nameof(PropertyAndFields.BaseClassProperty)), new SubClass() };

            // IndexExpression for indexed property
            PropertyInfo simpleIndexer = typeof(PropertyAndFields).GetProperties().First(prop => prop.GetIndexParameters().Length == 1);
            yield return new object[] { Expression.Property(obj, simpleIndexer, new Expression[] { Expression.Constant(1) }), 5 };

            PropertyInfo advancedIndexer = typeof(PropertyAndFields).GetProperties().First(prop => prop.GetIndexParameters().Length == 2);
            yield return new object[] { Expression.Property(obj, advancedIndexer, new Expression[] { Expression.Constant(1), Expression.Constant(2) }), 5 };

            // IndexExpression for non-indexed property
            yield return new object[] { Expression.Property(null, typeof(PropertyAndFields).GetProperty(nameof(PropertyAndFields.StaticInt32Property2)), new Expression[0]), 6 };
            yield return new object[] { Expression.Property(obj, nameof(PropertyAndFields.Int32Property), new Expression[0]), 7 };
        }

        [Fact]
        public void SimpleAssignment()
        {
            ParameterExpression variable = Expression.Variable(typeof(int));
            LabelTarget target = Expression.Label(typeof(int));
            Expression exp = Expression.Block(
                new ParameterExpression[] { variable },
                Expression.Assign(variable, Expression.Constant(42)),
                Expression.Return(target, variable),
                Expression.Label(target, Expression.Default(typeof(int)))
                );
            Assert.Equal(42, Expression.Lambda<Func<int>>(exp).CompileToTestMethod()());
        }

        [Fact]
        public void AssignmentHasValueItself()
        {
            ParameterExpression variable = Expression.Variable(typeof(int));
            Expression exp = Expression.Block(
                new ParameterExpression[] { variable },
                Expression.Assign(variable, Expression.Constant(42))
                );
            Assert.Equal(42, Expression.Lambda<Func<int>>(exp).CompileToTestMethod()());
        }

        [Theory, MemberData(nameof(MemberAssignments))]
        public void AssignToMember(Expression memberExp, object value)
        {
            Func<bool> func = Expression.Lambda<Func<bool>>(
                Expression.Block(
                    Expression.Assign(memberExp, Expression.Constant(value)),
                    Expression.Equal(memberExp, Expression.Constant(value))
                    )
                ).CompileToTestMethod();
            Assert.True(func());
        }

        [Fact]
        public void ReferenceAssignable()
        {
            ParameterExpression variable = Expression.Variable(typeof(object));
            LabelTarget target = Expression.Label(typeof(object));
            Expression exp = Expression.Block(
                new ParameterExpression[] { variable },
                Expression.Assign(variable, Expression.Constant("Hello")),
                Expression.Return(target, variable),
                Expression.Label(target, Expression.Default(typeof(object)))
                );
            Assert.Equal("Hello", Expression.Lambda<Func<object>>(exp).CompileToTestMethod()());
        }

        [Fact]
        public static void ValueTypeIndexAssign()
        {
            Expression index = Expression.Property(Expression.Constant(new StructWithPropertiesAndFields()), typeof(StructWithPropertiesAndFields).GetProperty("Item"), new Expression[] { Expression.Constant(1) });

            Expression<Func<bool>> func = Expression.Lambda<Func<bool>>(
                Expression.Block(
                    Expression.Assign(index, Expression.Constant(123)),
                    Expression.Equal(index, Expression.Constant(123))
                    )
                );

            Assert.True(func.CompileToTestMethod()());
        }

        [Fact]
        public static void Left_ReferenceTypeContainsChildTryExpression_Compiles()
        {
            Expression tryExpression = Expression.TryFinally(
                Expression.Constant(1),
                Expression.Empty()
            );
            PropertyInfo simpleIndexer = typeof(PropertyAndFields).GetProperties().First(prop => prop.GetIndexParameters().Length == 1);
            Expression index = Expression.Property(Expression.Constant(new PropertyAndFields()), simpleIndexer, new Expression[] { tryExpression });

            Func<bool> func = Expression.Lambda<Func<bool>>(
                Expression.Block(
                    Expression.Assign(index, Expression.Constant(123)),
                    Expression.Equal(index, Expression.Constant(123))
                    )
                ).CompileToTestMethod();
            Assert.True(func());
        }

        public class BaseClass { }
        class SubClass : BaseClass { }
    }
}
