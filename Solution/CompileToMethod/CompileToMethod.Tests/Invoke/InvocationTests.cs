// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class InvocationTests
    {
        public delegate void X(X a);

        [Fact]
        public static void SelfApplication()
        {
            // Expression<X> f = x => {};
            Expression<X> f = Expression.Lambda<X>(Expression.Empty(), Expression.Parameter(typeof(X)));
            LambdaExpression a = Expression.Lambda(Expression.Invoke(f, f));

            a.CompileToTestMethod().DynamicInvoke();

            ParameterExpression it = Expression.Parameter(f.Type);
            LambdaExpression b = Expression.Lambda(Expression.Invoke(Expression.Lambda(Expression.Invoke(it, it), it), f));

            b.CompileToTestMethod().DynamicInvoke();
        }

        [Fact]
        public static void NoWriteBackToInstance()
        {
            new NoThread().DoTest();
        }

        public class NoThread
        {
            public Func<NoThread, int> DoItA = (nt) =>
            {
                nt.DoItA = (nt0) => 1;
                return 0;
            };

            public Action Compile()
            {
                ConstantExpression ind0 = Expression.Constant(this);
                MemberExpression fld = Expression.PropertyOrField(ind0, "DoItA");
                BlockExpression block = Expression.Block(typeof(void), Expression.Invoke(fld, ind0));
                return Expression.Lambda<Action>(block).CompileToTestMethod();
            }

            public void DoTest()
            {
                Action act = Compile();
                act();
                Assert.Equal(1, DoItA(this));
            }
        }

        public class FuncHolder
        {
            public Func<int> Function;

            public FuncHolder()
            {
                Function = () =>
                {
                    Function = () => 1;
                    return 0;
                };
            }
        }

        [Fact]
        public static void InvocationDoesNotChangeFunctionInvoked()
        {
            FuncHolder holder = new FuncHolder();
            MemberExpression fld = Expression.Field(Expression.Constant(holder), "Function");
            InvocationExpression inv = Expression.Invoke(fld);
            Func<int> act = (Func<int>)Expression.Lambda(inv).CompileToTestMethod();
            act();
            Assert.Equal(1, holder.Function());
        }


#if NETCOREAPP1_1
        [Fact(Skip = "Expression.Lambda fails")]
#else
        [Fact]
#endif
        public static void InvokePrivateDelegate()
        {
            AssemblyBuilder assembly = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName("Name"), AssemblyBuilderAccess.Run);
            ModuleBuilder module = assembly.DefineDynamicModule("Name");
            TypeBuilder builder = module.DefineType("Type", TypeAttributes.Class | TypeAttributes.Public | TypeAttributes.Sealed | TypeAttributes.AnsiClass | TypeAttributes.AutoClass, typeof(MulticastDelegate));
            builder.DefineConstructor(MethodAttributes.RTSpecialName | MethodAttributes.HideBySig | MethodAttributes.Public, CallingConventions.Standard, new[] { typeof(object), typeof(IntPtr) }).SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);
            builder.DefineMethod("Invoke", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Virtual, typeof(int), Type.EmptyTypes).SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);
            Type delType = builder.CreateTypeInfo().AsType();
            LambdaExpression lambda = Expression.Lambda(delType, Expression.Constant(42));
            Delegate del = lambda.CompileToTestMethod();
            var invoke = Expression.Invoke(Expression.Constant(del));
            var invLambda = Expression.Lambda<Func<int>>(invoke);
            var invFunc = invLambda.CompileToTestMethod();
            Assert.Equal(42, invFunc());
        }

#if NETCOREAPP1_1
        [Fact(Skip = "Expression.Lambda fails")]
#else
        [Fact]
#endif
        public static void InvokePrivateDelegateTypeLambda()
        {
            AssemblyBuilder assembly = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName("Name"), AssemblyBuilderAccess.Run);
            ModuleBuilder module = assembly.DefineDynamicModule("Name");
            TypeBuilder builder = module.DefineType("Type", TypeAttributes.Class | TypeAttributes.NotPublic | TypeAttributes.Sealed | TypeAttributes.AnsiClass | TypeAttributes.AutoClass, typeof(MulticastDelegate));
            builder.DefineConstructor(MethodAttributes.RTSpecialName | MethodAttributes.HideBySig | MethodAttributes.Public, CallingConventions.Standard, new[] { typeof(object), typeof(IntPtr) }).SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);
            builder.DefineMethod("Invoke", MethodAttributes.Private | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Virtual, typeof(int), Type.EmptyTypes).SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);
            Type delType = builder.CreateTypeInfo().AsType();
            LambdaExpression lambda = Expression.Lambda(delType, Expression.Constant(42));
            var invoke = Expression.Invoke(lambda);
            var invLambda = Expression.Lambda<Func<int>>(invoke);
            var invFunc = invLambda.CompileToTestMethod();
            Assert.Equal(42, invFunc());
        }


    }
}
