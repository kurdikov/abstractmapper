// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class CallTests
    {
        public struct Mutable
        {
            private int x;

            public int X
            {
                get { return x; }
                set { x = value; }
            }

            public int this[int i]
            {
                get { return x; }
                set { x = value; }
            }

            public int Foo()
            {
                return x++;
            }
        }

        public class Wrapper<T>
        {
            public const int Zero = 0;
            public T Field;
#pragma warning disable 649 // For testing purposes
            public readonly T ReadOnlyField;
#pragma warning restore
            public T Property
            {
                get { return Field; }
                set { Field = value; }
            }
        }

        public static class Methods
        {
            public static void ByRef(ref int x) { ++x; }
        }

        [Fact]
        public static void UnboxReturnsReference()
        {
            ParameterExpression p = Expression.Parameter(typeof(object));
            UnaryExpression unbox = Expression.Unbox(p, typeof(Mutable));
            MethodCallExpression call = Expression.Call(unbox, typeof(Mutable).GetMethod("Foo"));
            Func<object, int> lambda = Expression.Lambda<Func<object, int>>(call, p).CompileToTestMethod();

            object boxed = new Mutable();
            Assert.Equal(0, lambda(boxed));
            Assert.Equal(1, lambda(boxed));
            Assert.Equal(2, lambda(boxed));
            Assert.Equal(3, lambda(boxed));
        }

        [Fact]
        public static void ArrayWriteBack()
        {
            ParameterExpression p = Expression.Parameter(typeof(Mutable[]));
            BinaryExpression indexed = Expression.ArrayIndex(p, Expression.Constant(0));
            MethodCallExpression call = Expression.Call(indexed, typeof(Mutable).GetMethod("Foo"));
            Func<Mutable[], int> lambda = Expression.Lambda<Func<Mutable[], int>>(call, p).CompileToTestMethod();

            var array = new Mutable[1];
            Assert.Equal(0, lambda(array));
            Assert.Equal(1, lambda(array));
            Assert.Equal(2, lambda(array));
        }

        [Fact]
        public static void MultiRankArrayWriteBack()
        {
            ParameterExpression p = Expression.Parameter(typeof(Mutable[,]));
            MethodCallExpression indexed = Expression.ArrayIndex(p, Expression.Constant(0), Expression.Constant(0));
            MethodCallExpression call = Expression.Call(indexed, typeof(Mutable).GetMethod("Foo"));
            Func<Mutable[,], int> lambda = Expression.Lambda<Func<Mutable[,], int>>(call, p).CompileToTestMethod();

            var array = new Mutable[1, 1];
            Assert.Equal(0, lambda(array));
            Assert.Equal(1, lambda(array));
            Assert.Equal(2, lambda(array));
        }

        [Fact]
        public static void ArrayAccessWriteBack()
        {
            ParameterExpression p = Expression.Parameter(typeof(Mutable[]));
            IndexExpression indexed = Expression.ArrayAccess(p, Expression.Constant(0));
            MethodCallExpression call = Expression.Call(indexed, typeof(Mutable).GetMethod("Foo"));
            Func<Mutable[], int> lambda = Expression.Lambda<Func<Mutable[], int>>(call, p).CompileToTestMethod();

            var array = new Mutable[1];
            Assert.Equal(0, lambda(array));
            Assert.Equal(1, lambda(array));
            Assert.Equal(2, lambda(array));
        }

        [Fact]
        public static void MultiRankArrayAccessWriteBack()
        {
            ParameterExpression p = Expression.Parameter(typeof(Mutable[,]));
            IndexExpression indexed = Expression.ArrayAccess(p, Expression.Constant(0), Expression.Constant(0));
            MethodCallExpression call = Expression.Call(indexed, typeof(Mutable).GetMethod("Foo"));
            Func<Mutable[,], int> lambda = Expression.Lambda<Func<Mutable[,], int>>(call, p).CompileToTestMethod();

            var array = new Mutable[1, 1];
            Assert.Equal(0, lambda(array));
            Assert.Equal(1, lambda(array));
            Assert.Equal(2, lambda(array));
        }

        [Fact]
        public static void IndexedPropertyAccessNoWriteBack()
        {
            ParameterExpression p = Expression.Parameter(typeof(List<Mutable>));
            IndexExpression indexed = Expression.Property(p, typeof(List<Mutable>).GetProperty("Item"), Expression.Constant(0));
            MethodCallExpression call = Expression.Call(indexed, typeof(Mutable).GetMethod("Foo"));
            Func<List<Mutable>, int> lambda = Expression.Lambda<Func<List<Mutable>, int>>(call, p).CompileToTestMethod();

            var list = new List<Mutable> { new Mutable() };
            Assert.Equal(0, lambda(list));
            Assert.Equal(0, lambda(list));
        }

        [Fact]
        public static void FieldAccessWriteBack()
        {
            ParameterExpression p = Expression.Parameter(typeof(Wrapper<Mutable>));
            MemberExpression member = Expression.Field(p, typeof(Wrapper<Mutable>).GetField("Field"));
            MethodCallExpression call = Expression.Call(member, typeof(Mutable).GetMethod("Foo"));
            Func<Wrapper<Mutable>, int> lambda = Expression.Lambda<Func<Wrapper<Mutable>, int>>(call, p).CompileToTestMethod();

            var wrapper = new Wrapper<Mutable>();
            Assert.Equal(0, lambda(wrapper));
            Assert.Equal(1, lambda(wrapper));
            Assert.Equal(2, lambda(wrapper));
        }

        [Fact]
        public static void PropertyAccessNoWriteBack()
        {
            ParameterExpression p = Expression.Parameter(typeof(Wrapper<Mutable>));
            MemberExpression member = Expression.Property(p, typeof(Wrapper<Mutable>).GetProperty("Property"));
            MethodCallExpression call = Expression.Call(member, typeof(Mutable).GetMethod("Foo"));
            Func<Wrapper<Mutable>, int> lambda = Expression.Lambda<Func<Wrapper<Mutable>, int>>(call, p).CompileToTestMethod();

            var wrapper = new Wrapper<Mutable>();
            Assert.Equal(0, lambda(wrapper));
            Assert.Equal(0, lambda(wrapper));
        }

        [Fact]
        public static void ReadonlyFieldAccessWriteBack()
        {
            ParameterExpression p = Expression.Parameter(typeof(Wrapper<Mutable>));
            MemberExpression member = Expression.Field(p, typeof(Wrapper<Mutable>).GetField("ReadOnlyField"));
            MethodCallExpression call = Expression.Call(member, typeof(Mutable).GetMethod("Foo"));
            Func<Wrapper<Mutable>, int> lambda = Expression.Lambda<Func<Wrapper<Mutable>, int>>(call, p).CompileToTestMethod();

            var wrapper = new Wrapper<Mutable>();
            Assert.Equal(0, lambda(wrapper));
            Assert.Equal(0, lambda(wrapper));
            Assert.Equal(0, lambda(wrapper));
        }

        [Fact]
        public static void ConstFieldAccessWriteBack()
        {
            MemberExpression member = Expression.Field(null, typeof(Wrapper<Mutable>).GetField("Zero"));
            MethodCallExpression call = Expression.Call(member, typeof(int).GetMethod("GetType"));
            Func<Type> lambda = Expression.Lambda<Func<Type>>(call).CompileToTestMethod();

            var wrapper = new Wrapper<Mutable>();
            Assert.Equal(typeof(int), lambda());
        }

        [Fact]
        public static void CallByRefMutableStructPropertyWriteBack()
        {
            ParameterExpression p = Expression.Parameter(typeof(Mutable));
            MemberExpression x = Expression.Property(p, "X");
            MethodCallExpression call = Expression.Call(typeof(Methods).GetMethod("ByRef"), x);
            BlockExpression body = Expression.Block(call, x);
            Func<Mutable, int> lambda = Expression.Lambda<Func<Mutable, int>>(body, p).CompileToTestMethod();

            var m = new Mutable() { X = 41 };
            Assert.Equal(42, lambda(m));
        }

        [Fact]
        public static void CallByRefMutableStructIndexWriteBack()
        {
            ParameterExpression p = Expression.Parameter(typeof(Mutable));
            IndexExpression x = Expression.MakeIndex(p, typeof(Mutable).GetProperty("Item"), new[] { Expression.Constant(0) });
            MethodCallExpression call = Expression.Call(typeof(Methods).GetMethod("ByRef"), x);
            BlockExpression body = Expression.Block(call, x);
            Func<Mutable, int> lambda = Expression.Lambda<Func<Mutable, int>>(body, p).CompileToTestMethod();

            var m = new Mutable() { X = 41 };
            Assert.Equal(42, lambda(m));
        }

        [Fact]
        public static void Call_InstanceNullInside_ThrowsNullReferenceExceptionOnInvocation()
        {
            Expression call = Expression.Call(Expression.Constant(null, typeof(NonGenericClass)), typeof(NonGenericClass).GetMethod(nameof(NonGenericClass.InstanceMethod)));
            Action compiledDelegate = Expression.Lambda<Action>(call).CompileToTestMethod();
            TargetInvocationException exception = Assert.Throws<TargetInvocationException>(() => compiledDelegate.DynamicInvoke());
            Assert.IsType<NullReferenceException>(exception.InnerException);
        }

        public static IEnumerable<object[]> Call_NoParameters_TestData()
        {
            // Basic
            yield return new object[] { Expression.Constant(new ClassWithInterface1()), typeof(ClassWithInterface1).GetMethod(nameof(ClassWithInterface1.Method)), 2 };
            yield return new object[] { Expression.Constant(new StructWithInterface1()), typeof(StructWithInterface1).GetMethod(nameof(StructWithInterface1.Method)), 2 };

            // Object method
            yield return new object[] { Expression.Constant(new ClassWithInterface1()), typeof(object).GetMethod(nameof(object.GetType)), typeof(ClassWithInterface1) };
            yield return new object[] { Expression.Constant(new StructWithInterface1()), typeof(object).GetMethod(nameof(object.GetType)), typeof(StructWithInterface1) };
            yield return new object[] { Expression.Constant(Int32Enum.A), typeof(object).GetMethod(nameof(object.GetType)), typeof(Int32Enum) };

            // ValueType method from struct
            yield return new object[] { Expression.Constant(new StructWithInterface1()), typeof(ValueType).GetMethod(nameof(ValueType.ToString)), new StructWithInterface1().ToString() };

            // Enum method from enum
            yield return new object[] { Expression.Constant(Int32Enum.A), typeof(Enum).GetMethod(nameof(Enum.ToString), new Type[0]), "A" };

            // Interface method
            yield return new object[] { Expression.Constant(new ClassWithInterface1()), typeof(Interface1).GetMethod(nameof(Interface1.InterfaceMethod)), 1 };
            yield return new object[] { Expression.Constant(new StructWithInterface1()), typeof(Interface1).GetMethod(nameof(Interface1.InterfaceMethod)), 1 };
            yield return new object[] { Expression.Constant(new ClassWithCompoundInterface()), typeof(Interface1).GetMethod(nameof(Interface1.InterfaceMethod)), 1 };
            yield return new object[] { Expression.Constant(new StructWithCompoundInterface()), typeof(Interface1).GetMethod(nameof(Interface1.InterfaceMethod)), 1 };

            // Interface method, interface type
            yield return new object[] { Expression.Constant(new ClassWithInterface1(), typeof(Interface1)), typeof(Interface1).GetMethod(nameof(Interface1.InterfaceMethod)), 1 };
            yield return new object[] { Expression.Constant(new StructWithInterface1(), typeof(Interface1)), typeof(Interface1).GetMethod(nameof(Interface1.InterfaceMethod)), 1 };
        }

        [Theory]
        [MemberData(nameof(Call_NoParameters_TestData))]
        public static void Call_NoParameters(Expression instance, MethodInfo method, object expected)
        {
            Expression call = Expression.Call(instance, method);
            Delegate compiledDelegate = Expression.Lambda(call).CompileToTestMethod();
            Assert.Equal(expected, compiledDelegate.DynamicInvoke());
        }

        private static Expression s_valid => Expression.Constant(5);

        private static MethodInfo s_method0 = typeof(NonGenericClass).GetMethod(nameof(NonGenericClass.Method0));
        private static MethodInfo s_method1 = typeof(NonGenericClass).GetMethod(nameof(NonGenericClass.Method1));
        private static MethodInfo s_method2 = typeof(NonGenericClass).GetMethod(nameof(NonGenericClass.Method2));
        private static MethodInfo s_method3 = typeof(NonGenericClass).GetMethod(nameof(NonGenericClass.Method3));
        private static MethodInfo s_method4 = typeof(NonGenericClass).GetMethod(nameof(NonGenericClass.Method4));
        private static MethodInfo s_method5 = typeof(NonGenericClass).GetMethod(nameof(NonGenericClass.Method5));
        private static MethodInfo s_method6 = typeof(NonGenericClass).GetMethod(nameof(NonGenericClass.Method6));
        private static MethodInfo s_method7 = typeof(NonGenericClass).GetMethod(nameof(NonGenericClass.Method7));

        public static IEnumerable<object[]> Method_Invalid_TestData()
        {
            yield return new object[] { null, typeof(ArgumentNullException) };
            yield return new object[] { typeof(GenericClass<>).GetMethod(nameof(GenericClass<string>.NonGenericMethod)), typeof(ArgumentException) };
            yield return new object[] { typeof(NonGenericClass).GetMethod(nameof(NonGenericClass.GenericMethod)), typeof(ArgumentException) };
        }

        public static IEnumerable<object[]> Method_DoesntBelongToInstance_TestData()
        {
            // Different declaring type
            yield return new object[] { Expression.Constant(new ClassWithInterface1()), typeof(OtherClassWithInterface1).GetMethod(nameof(Interface1.InterfaceMethod)) };
            yield return new object[] { Expression.Constant(new StructWithInterface1()), typeof(OtherStructWithInterface1).GetMethod(nameof(Interface1.InterfaceMethod)) };

            // Different interface
            yield return new object[] { Expression.Constant(new ClassWithInterface1()), typeof(Interface2).GetMethod(nameof(Interface2.InterfaceMethod)) };
            yield return new object[] { Expression.Constant(new StructWithInterface1()), typeof(Interface2).GetMethod(nameof(Interface2.InterfaceMethod)) };

            // Custom type
            yield return new object[] { Expression.Constant(new ClassWithInterface1(), typeof(object)), typeof(ClassWithInterface1).GetMethod(nameof(ClassWithInterface1.Method)) };
            yield return new object[] { Expression.Constant(new StructWithInterface1(), typeof(object)), typeof(ClassWithInterface1).GetMethod(nameof(ClassWithInterface1.Method)) };
        }

        public static IEnumerable<object[]> InvalidArg_TestData()
        {
            yield return new object[] { null, typeof(ArgumentNullException) };
            yield return new object[] { Expression.Property(null, typeof(Unreadable<string>), nameof(Unreadable<string>.WriteOnly)), typeof(ArgumentException) };
            yield return new object[] { Expression.Constant("abc"), typeof(ArgumentException) };
        }

        private static void AssertArgumentException(Action action, Type exceptionType, string paramName)
        {
            ArgumentException ex = (ArgumentException)Assert.Throws(exceptionType, action);
            Assert.Equal(paramName, ex.ParamName);
        }
        public static IEnumerable<object[]> InvalidTypeArgs_TestData()
        {
            yield return new object[] { null };
            yield return new object[] { new Type[0] };
            yield return new object[] { new Type[2] };
        }
        public class GenericClass<T>
        {
            public static void NonGenericMethod() { }
        }

        public static class Unreadable<T>
        {
            public static T WriteOnly { set { } }
        }

        public class NonGenericClass
        {
            public static void GenericMethod<T>() { }
            public void InstanceMethod() { }
            public static void StaticMethod() { }

            public static void Method0() { }
            public static void Method1(int i1) { }
            public static void Method2(int i1, int i2) { }
            public static void Method3(int i1, int i2, int i3) { }
            public static void Method4(int i1, int i2, int i3, int i4) { }
            public static void Method5(int i1, int i2, int i3, int i4, int i5) { }
            public static void Method6(int i1, int i2, int i3, int i4, int i5, int i6) { }
            public static void Method7(int i1, int i2, int i3, int i4, int i5, int i6, int i7) { }

            public void staticSameName(uint i1) { }
            public void instanceSameName(int i1) { }

            public static void StaticSameName(uint i1) { }
            public static void staticSameName(int i1) { }

            public void GenericInstanceMethod<T>(T t1) { }
            public static void GenericStaticMethod<T>(T t1) { }

            public void ConstrainedInstanceMethod<T>(T t1) where T : struct { }
            public static void ConstrainedStaticMethod<T>(T t1) where T : struct { }

            public void InstanceMethod0() { }
            public void InstanceMethod1(int i1) { }
            public void InstanceMethod2(int i1, int i2) { }
            public void InstanceMethod3(int i1, int i2, int i3) { }
            public void InstanceMethod4(int i1, int i2, int i3, int i4) { }
            public static void StaticMethod1(int i1) { }
        }

        public interface Interface1
        {
            int InterfaceMethod();
        }

        public interface Interface2
        {
            int InterfaceMethod();
        }

        public interface CompoundInterface : Interface1 { }

        public class ClassWithInterface1 : Interface1
        {
            public int InterfaceMethod() => 1;
            public int Method() => 2;
        }

        public class OtherClassWithInterface1 : Interface1
        {
            public int InterfaceMethod() => 1;
        }

        public struct StructWithInterface1 : Interface1
        {
            public int InterfaceMethod() => 1;
            public int Method() => 2;
        }

        public struct OtherStructWithInterface1 : Interface1
        {
            public int InterfaceMethod() => 1;
        }

        public class ClassWithCompoundInterface : CompoundInterface
        {
            public int InterfaceMethod() => 1;
        }

        public struct StructWithCompoundInterface : CompoundInterface
        {
            public int InterfaceMethod() => 1;
        }
    }

    class SomeMethods
    {
        public static void S0() { }
        public static void S1(int x) { }
        public static void S2(int x, int y) { }

        public void I0() { }
        public void I1(int x) { }
        public void I2(int x, int y) { }
    }

    static class ExtensionMethods
    {
        public static void E0(this int x) { }
        public static void E1(this int x, int y) { }
        public static void E2(this int x, int y, int z) { }
    }
}
