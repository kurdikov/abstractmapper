// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class LabelTargetTests
    {
        // The actual use of label targets when compiling, interpreting or otherwise acting upon an expression
        // that makes use of them is by necessity covered by testing those GotoExpressions that make use of them.
        // These tests focus on the LabelTarget class and the factory methods producing them, with compilation
        // only when some feature of a target itself (viz. a name that is not a valid C# name is still valid)
        // could perhaps have an effect in a regression error.

        private class CustomException : Exception
        {
            public CustomException()
                : base("This is a custom exception that exists just to distinguish throwing it in a test from any other cause of exceptions being thrown.")
            {
            }
        }
        [Fact]
        public void LableNameNeedNotBeValidCSharpLabel()
        {
            LabelTarget target = Expression.Label("1, 2, 3, 4. This is not a valid C♯ label!\"'<>.\uffff");
            Expression.Lambda<Action>(
                Expression.Block(
                    Expression.Goto(target),
                    Expression.Throw(Expression.Constant(new CustomException())),
                    Expression.Label(target)
                    )
                ).CompileToTestMethod()();
        }

        [Fact]
        public void LableNameNeedNotBeValidCSharpLabelWithValue()
        {
            LabelTarget target = Expression.Label(typeof(int), "1, 2, 3, 4. This is not a valid C♯ label!\"'<>.\uffff");
            Func<int> func = Expression.Lambda<Func<int>>(
                Expression.Block(
                    Expression.Return(target, Expression.Constant(42)),
                    Expression.Throw(Expression.Constant(new CustomException())),
                    Expression.Label(target, Expression.Default(typeof(int)))
                    )
                ).CompileToTestMethod();
            Assert.Equal(42, func());
        }
    }
}
