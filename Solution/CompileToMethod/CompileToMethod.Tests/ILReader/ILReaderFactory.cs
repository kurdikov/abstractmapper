// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

// Code adapted from https://blogs.msdn.microsoft.com/haibo_luo/2010/04/19/ilvisualizer-2010-solution

using System.Reflection.Emit;
using System.Reflection;

namespace System.Linq.Expressions.Tests
{
    public class ILReaderFactory
    {
        public static ILReader Create(object obj)
        {
            Type type = obj.GetType();

            if (type == typeof(MethodBuilder))
            {
                var mb = (MethodBuilder)obj;
                return new ILReader(new MethodBuilderILProvider(mb), new ModuleScopeTokenResolver(mb));
            }

            throw new NotSupportedException($"Reading IL from type '{type}' is currently not supported.");
        }

    }
}
