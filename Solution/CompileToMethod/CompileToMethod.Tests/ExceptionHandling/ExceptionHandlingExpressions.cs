// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class ExceptionHandlingExpressions
    {
        // As this class is only used here, it is distinguished from an exception
        // being thrown due to an actual error.
        public class TestException : Exception
        {
            public TestException()
                : base("This is a test exception")
            {
            }
        }

        public class DerivedTestException : TestException
        {
        }

        private static class Unreadable<T>
        {
            public static T WriteOnly
            {
                set { }
            }
        }

        //private static RuntimeWrappedException CreateRuntimeWrappedException(object inner)
        //{
        //    return
        //        (RuntimeWrappedException)
        //            typeof(RuntimeWrappedException).GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.ExactBinding,
        //                                                           null,
        //                                                           new Type[] { typeof(object) },
        //                                                           null).Invoke(new[] {inner});
        //}

        [Fact]
        public void ThrowNullSameAsRethrow()
        {
            UnaryExpression rethrow = Expression.Rethrow();
            UnaryExpression nullThrow = Expression.Throw(null);
            Assert.Equal(rethrow.GetType(), nullThrow.GetType());
            TryExpression rethrowTwice = Expression.TryCatch(
                Expression.TryCatch(
                        Expression.Throw(Expression.Constant(new TestException())),
                        Expression.Catch(typeof(TestException), rethrow)
                    ),
                    Expression.Catch(typeof(TestException), nullThrow)
                );
            Action doRethrowTwice = Expression.Lambda<Action>(rethrowTwice).CompileToTestMethod();
            Assert.Throws<TestException>(doRethrowTwice);
        }

        [Fact]
        public void TypedThrowNullSameAsRethrow()
        {
            UnaryExpression rethrow = Expression.Rethrow(typeof(int));
            UnaryExpression nullThrow = Expression.Throw(null, typeof(int));
            Assert.Equal(rethrow.GetType(), nullThrow.GetType());
            TryExpression rethrowTwice = Expression.TryCatch(
                Expression.TryCatch(
                        Expression.Throw(Expression.Constant(new TestException()), typeof(int)),
                        Expression.Catch(typeof(TestException), rethrow)
                    ),
                    Expression.Catch(typeof(TestException), nullThrow)
                );
            Action doRethrowTwice = Expression.Lambda<Action>(rethrowTwice).CompileToTestMethod();
            Assert.Throws<TestException>(() => doRethrowTwice());
        }

        [Fact]
        public void CannotRethrowOutsideCatch()
        {
            LambdaExpression rethrowNothing = Expression.Lambda<Action>(Expression.Rethrow());
            Assert.Throws<InvalidOperationException>(() => rethrowNothing.CompileToTestMethod());
        }

        [Fact]
        public void CannotRethrowWithinFinallyWithinCatch()
        {
            LambdaExpression rethrowFinally = Expression.Lambda<Action>(
                Expression.TryCatch(
                    Expression.Empty(),
                    Expression.Catch(
                        typeof(Exception),
                        Expression.TryFinally(
                            Expression.Empty(),
                            Expression.Rethrow()
                            )
                        )
                    )
                );
            Assert.Throws<InvalidOperationException>(() => rethrowFinally.CompileToTestMethod());
        }

        [Fact]
        public void CannotRethrowWithinFaultWithinCatch()
        {
            LambdaExpression rethrowFinally = Expression.Lambda<Action>(
                Expression.TryCatch(
                    Expression.Empty(),
                    Expression.Catch(
                        typeof(Exception),
                        Expression.TryFault(
                            Expression.Empty(),
                            Expression.Rethrow()
                            )
                        )
                    )
                );
            Assert.Throws<InvalidOperationException>(() => rethrowFinally.CompileToTestMethod());
        }

        [Fact]
        public void CanCatchAndThrowNonExceptions()
        {
            TryExpression throwCatchString = Expression.TryCatch(
                Expression.Throw(Expression.Constant("Hello")),
                Expression.Catch(typeof(string), Expression.Empty())
                );
            Expression.Lambda<Action>(throwCatchString).CompileToTestMethod()();
        }

        [Fact]
        public void CanCatchAndUseNonExceptions()
        {
            ParameterExpression ex = Expression.Variable(typeof(string));
            TryExpression throwCatchString = Expression.TryCatch(
                Expression.Throw(Expression.Constant("Hello"), typeof(int)),
                Expression.Catch(ex, Expression.Property(ex, "Length"))
                );
            Func<int> func = Expression.Lambda<Func<int>>(throwCatchString).CompileToTestMethod();
            Assert.Equal(5, func());
        }

        [Fact]
        public void ThrownNonExceptionPassesThroughNonMatchingHandlers()
        {
            ParameterExpression ex = Expression.Variable(typeof(object));
            TryExpression nested = Expression.TryCatch(
                Expression.TryCatch(
                    Expression.Throw(Expression.Constant("1234567890"), typeof(int)),
                    Expression.Catch(typeof(InvalidOperationException), Expression.Constant(1)),
                    Expression.Catch(typeof(Exception), Expression.Constant(2))
                    ),
                Expression.Catch(ex, Expression.Property(Expression.Convert(ex, typeof(string)), "Length"))
                );
            Func<int> func = Expression.Lambda<Func<int>>(nested).CompileToTestMethod();
            Assert.Equal(10, func());
        }

        [Fact]
        public void CatchFromExternallyThrownString()
        {
            foreach(bool assemblyWraps in new []{false, true})
            {
                ParameterExpression ex = Expression.Variable(typeof(string));
                TryExpression tryCatch =
                    Expression.TryCatch(
                        Expression.Block(
                            Expression.Call((assemblyWraps ? DynamicTypes.ThrowingWrappingType : DynamicTypes.ThrowingNotWrappingType).GetMethod("WillThrow")), Expression.Constant("Nothing Thrown")),
                        Expression.Catch(ex, ex));
                Func<string> func = Expression.Lambda<Func<string>>(tryCatch).CompileToTestMethod();
                Assert.Equal("An Exceptional Exception!", func());
            }
        }

        [Fact]
        public void ThrowNullThrowsNRE()
        {
            Action throwNull = Expression.Lambda<Action>(
                Expression.Throw(Expression.Constant(null, typeof(Expression)))
                ).CompileToTestMethod();
            Assert.Throws<NullReferenceException>(throwNull);
        }

        [Fact]
        public void ThrowNullThrowsCatchableNRE()
        {
            Func<int> throwCatchNull = Expression.Lambda<Func<int>>(
                Expression.TryCatch(
                    Expression.Throw(Expression.Constant(null, typeof(ArgumentException)), typeof(int)),
                    Expression.Catch(typeof(ArgumentException), Expression.Constant(1)),
                    Expression.Catch(typeof(NullReferenceException), Expression.Constant(2)),
                    Expression.Catch(typeof(Expression), Expression.Constant(3))
                    )
                ).CompileToTestMethod();
            Assert.Equal(2, throwCatchNull());
        }

        [Fact]
        public void CanCatchExceptionAsObject()
        {
            Func<int> throwCatchAsObject = Expression.Lambda<Func<int>>(
                Expression.TryCatch(
                    Expression.Throw(Expression.Constant(new Exception()), typeof(int)),
                    Expression.Catch(typeof(ArgumentException), Expression.Constant(1)),
                    Expression.Catch(typeof(object), Expression.Constant(2))
                    )
                ).CompileToTestMethod();
            Assert.Equal(2, throwCatchAsObject());
        }

        [Fact]
        public void CanCatchExceptionAsObjectObtainingException()
        {
            Exception testException = new Exception();
            ParameterExpression param = Expression.Parameter(typeof(object));
            Func<object> throwCatchAsObject = Expression.Lambda<Func<object>>(
                Expression.TryCatch(
                    Expression.Throw(Expression.Constant(testException), typeof(object)),
                    Expression.Catch(typeof(ArgumentException), Expression.Constant("Will be skipped", typeof(object))),
                    Expression.Catch(param, param)
                    )
                ).CompileToTestMethod();
            Assert.Same(testException, throwCatchAsObject());
        }

        [Fact]
        public void CanAccessExceptionCaught()
        {
            ParameterExpression variable = Expression.Variable(typeof(Exception));
            TryExpression throwCatch = Expression.TryCatch(
                Expression.Throw(Expression.Constant(new TestException()), typeof(string)),
                Expression.Catch(variable, Expression.Property(variable, "Message"))
                );
            Assert.Equal("This is a test exception", Expression.Lambda<Func<string>>(throwCatch).CompileToTestMethod()());
        }

        [Fact]
        public void FromMakeMethods()
        {
            TryExpression tryExp = Expression.MakeTry(
                typeof(int),
                Expression.MakeUnary(ExpressionType.Throw, Expression.Constant(new TestException()), typeof(int)),
                null,
                null,
                new[] { Expression.MakeCatchBlock(typeof(TestException), null, Expression.Constant(3), null) }
                );
            Assert.Equal(3, Expression.Lambda<Func<int>>(tryExp).CompileToTestMethod()());
        }


        [Fact]
        public void FinallyDoesNotDetermineValue()
        {
            TryExpression finally2 = Expression.TryFinally(Expression.Constant(1), Expression.Constant(2));
            Assert.Equal(1, Expression.Lambda<Func<int>>(finally2).CompileToTestMethod()());
        }

        [Fact]
        public void FinallyDoesNotNeedToMatchType()
        {
            TryExpression finally2 = Expression.TryFinally(Expression.Constant(1), Expression.Constant(""));
            Assert.Equal(1, Expression.Lambda<Func<int>>(finally2).CompileToTestMethod()());
        }

        [Fact]
        public void FinallyCanBeVoid()
        {
            TryExpression finally2 = Expression.TryFinally(Expression.Constant(1), Expression.Empty());
            Assert.Equal(1, Expression.Lambda<Func<int>>(finally2).CompileToTestMethod()());
        }

        [Fact]
        public void FinallyCanBeNonVoidWithVoidTry()
        {
            TryExpression finally2 = Expression.TryFinally(Expression.Empty(), Expression.Constant(0));
            Expression.Lambda<Action>(finally2).CompileToTestMethod()();
        }

        [Fact]
        public void FinallyDoesNotDetermineValueNothingCaught()
        {
            TryExpression finally2 = Expression.TryCatchFinally(
                Expression.Constant(1),
                Expression.Constant(2),
                Expression.Catch(typeof(object), Expression.Constant(3))
                );
            Assert.Equal(1, Expression.Lambda<Func<int>>(finally2).CompileToTestMethod()());
        }

        [Fact]
        public void FinallyDoesNotDetermineValueSomethingCaught()
        {
            TryExpression finally2 = Expression.TryCatchFinally(
                Expression.Throw(Expression.Constant(new ArgumentException()), typeof(int)),
                Expression.Constant(2),
                Expression.Catch(typeof(ArgumentException), Expression.Constant(3))
                );
            Assert.Equal(3, Expression.Lambda<Func<int>>(finally2).CompileToTestMethod()());
        }

        [Fact]
        public void FaultNotTriggeredOnNoThrow()
        {
            ParameterExpression variable = Expression.Parameter(typeof(int));
            LabelTarget target = Expression.Label(typeof(int));
            BlockExpression block = Expression.Block(
                new[] { variable },
                Expression.Assign(variable, Expression.Constant(1)),
                Expression.TryFault(
                    Expression.Empty(),
                    Expression.Assign(variable, Expression.Constant(2))
                    ),
                Expression.Return(target, variable),
                Expression.Label(target, Expression.Default(typeof(int)))
                );
            Assert.Equal(1, Expression.Lambda<Func<int>>(block).CompileToTestMethod()());
        }

        [Fact]
        public void FaultNotTriggeredOnNoThrowNonVoid()
        {
            Func<int> func = Expression.Lambda<Func<int>>(
                Expression.TryFault(
                    Expression.Constant(42),
                    Expression.Throw(Expression.Constant(new TestException()))
                    )
                ).CompileToTestMethod();
            Assert.Equal(42, func());
        }

        [Fact]
        public void FaultTriggeredOnThrow()
        {
            ParameterExpression variable = Expression.Parameter(typeof(int));
            LabelTarget target = Expression.Label(typeof(int));
            BlockExpression block = Expression.Block(
                new[] { variable },
                Expression.Assign(variable, Expression.Constant(1)),
                Expression.TryCatch(
                    Expression.TryFault(
                        Expression.Throw(Expression.Constant(new TestException())),
                        Expression.Assign(variable, Expression.Constant(2))
                        ),
                    Expression.Catch(typeof(TestException), Expression.Empty())
                    ),
                Expression.Return(target, variable),
                Expression.Label(target, Expression.Default(typeof(int)))
                );
            Assert.Equal(2, Expression.Lambda<Func<int>>(block).CompileToTestMethod()());
        }

        [Fact]
        public void FinallyAndFaultAfterManyLabels()
        {
            // There is a caching optimisation used below a certain number of faults or
            // finally, so go past that to catch any regressions in the non-optimised
            // path.
            ParameterExpression variable = Expression.Parameter(typeof(int));
            LabelTarget target = Expression.Label(typeof(int));
            BlockExpression block = Expression.Block(
                new[] { variable },
                Expression.Assign(variable, Expression.Constant(1)),
                Expression.TryCatch(
                    Expression.Block(
                        Enumerable.Repeat(Expression.TryFinally(Expression.Empty(), Expression.Empty()), 40).Append(
                            Expression.TryFault(
                                Expression.Throw(Expression.Constant(new TestException())),
                                Expression.Assign(variable, Expression.Constant(2))
                                )
                            )
                        ),
                    Expression.Catch(typeof(TestException), Expression.Empty())
                    ),
                Expression.Return(target, variable),
                Expression.Label(target, Expression.Default(typeof(int)))
                );
            Assert.Equal(2, Expression.Lambda<Func<int>>(block).CompileToTestMethod()());
        }

        [Fact]
        public void FinallyTriggeredOnNoThrow()
        {
            ParameterExpression variable = Expression.Parameter(typeof(int));
            LabelTarget target = Expression.Label(typeof(int));
            BlockExpression block = Expression.Block(
                new[] { variable },
                Expression.Assign(variable, Expression.Constant(1)),
                Expression.TryFinally(
                    Expression.Empty(),
                    Expression.Assign(variable, Expression.Constant(2))
                    ),
                Expression.Return(target, variable),
                Expression.Label(target, Expression.Default(typeof(int)))
                );
            Assert.Equal(2, Expression.Lambda<Func<int>>(block).CompileToTestMethod()());
        }

        [Fact]
        public void ExceptionInFinallyReplacesException()
        {
            Action act = Expression.Lambda<Action>(
                Expression.TryFinally(
                    Expression.Throw(Expression.Constant(new InvalidOperationException())),
                    Expression.Throw(Expression.Constant(new TestException()))
                    )
                ).CompileToTestMethod();
            Assert.Throws<TestException>(act);
        }

        [Fact]
        public void ExceptionInFaultReplacesException()
        {
            Action act = Expression.Lambda<Action>(
                Expression.TryFault(
                    Expression.Throw(Expression.Constant(new InvalidOperationException())),
                    Expression.Throw(Expression.Constant(new TestException()))
                    )
                ).CompileToTestMethod();
            Assert.Throws<TestException>(act);
        }

        [Fact]
        public void FinallyTriggeredOnThrow()
        {
            ParameterExpression variable = Expression.Parameter(typeof(int));
            LabelTarget target = Expression.Label(typeof(int));
            BlockExpression block = Expression.Block(
                new[] { variable },
                Expression.Assign(variable, Expression.Constant(1)),
                Expression.TryCatch(
                    Expression.TryFinally(
                        Expression.Throw(Expression.Constant(new TestException())),
                        Expression.Assign(variable, Expression.Constant(2))
                        ),
                    Expression.Catch(typeof(TestException), Expression.Empty())
                    ),
                Expression.Return(target, variable),
                Expression.Label(target, Expression.Default(typeof(int)))
                );
            Assert.Equal(2, Expression.Lambda<Func<int>>(block).CompileToTestMethod()());
        }

        [Fact]
        public void CatchChaining()
        {
            TryExpression chain = Expression.TryCatch(
                Expression.Throw(Expression.Constant(new DerivedTestException()), typeof(int)),
                Expression.Catch(typeof(InvalidOperationException), Expression.Constant(1)),
                Expression.Catch(typeof(TestException), Expression.Constant(2)),
                Expression.Catch(typeof(DerivedTestException), Expression.Constant(3))
                );
            Assert.Equal(2, Expression.Lambda<Func<int>>(chain).CompileToTestMethod()());
        }

        [Fact]
        public void UserEmulatedFaultThroughCatch()
        {
            ParameterExpression variable = Expression.Parameter(typeof(int));
            Func<int> func = Expression.Lambda<Func<int>>(
                Expression.Block(
                    new[] { variable },
                    Expression.TryCatch(
                        Expression.TryCatch(
                            Expression.Throw(Expression.Constant(new TestException()), typeof(int)),
                            Expression.Catch(typeof(Exception), Expression.Block(Expression.Assign(variable, Expression.Constant(10)), Expression.Rethrow(typeof(int))))
                            ),
                            Expression.Catch(typeof(TestException), Expression.Add(variable, Expression.Constant(2)))
                        )
                    )
                ).CompileToTestMethod();
            Assert.Equal(12, func());
        }

        [Fact]
        public void UserEmulatedFaultThroughCatchVoid()
        {
            StringBuilder output = new StringBuilder();
            ConstantExpression builder = Expression.Constant(output);
            var noTypes = new Type[0];
            Action act = Expression.Lambda<Action>(
                Expression.Block(
                    Expression.TryCatch(
                        Expression.TryCatch(
                            Expression.Throw(Expression.Constant(new TestException())),
                            Expression.Catch(
                                typeof(Exception),
                                Expression.Block(
                                    Expression.Call(builder, "Append", noTypes, Expression.Constant('A')),
                                    Expression.Rethrow()
                                    )
                                )
                            ),
                            Expression.Catch(
                                typeof(TestException),
                                Expression.Block(
                                    Expression.Call(builder, "Append", noTypes, Expression.Constant('B')),
                                    Expression.Empty()
                                    )
                                )
                        )
                    )
                ).CompileToTestMethod();
            act();
            Assert.Equal("AB", output.ToString());
        }

        [Fact]
        public void ExplicitType()
        {
            TryExpression explicitType = Expression.MakeTry(typeof(object), Expression.Constant("hello"), Expression.Empty(), null, null);
            Assert.Equal(typeof(object), explicitType.Type);
            Assert.Equal("hello", Expression.Lambda<Func<object>>(explicitType).CompileToTestMethod()());
        }


        [Fact]
        public void FilterOnCatch()
        {
            TryExpression tryExp = Expression.TryCatch(
                Expression.Throw(Expression.Constant(new TestException()), typeof(int)),
                Expression.Catch(typeof(TestException), Expression.Constant(1), Expression.Constant(false)),
                Expression.Catch(typeof(TestException), Expression.Constant(2), Expression.Constant(true)),
                Expression.Catch(typeof(TestException), Expression.Constant(3))
                );
            Assert.Equal(2, Expression.Lambda<Func<int>>(tryExp).CompileToTestMethod()());
        }

        [Fact]
        public void FilterCanAccessException()
        {
            ParameterExpression exception = Expression.Variable(typeof(TestException));
            TryExpression tryExp = Expression.TryCatch(
                Expression.Throw(Expression.Constant(new TestException()), typeof(int)),
                Expression.Catch(exception, Expression.Constant(1), Expression.Equal(Expression.Constant("This is not a drill."), Expression.Property(exception, "Message"))),
                Expression.Catch(exception, Expression.Constant(2), Expression.Equal(Expression.Constant("This is a test exception"), Expression.Property(exception, "Message"))),
                Expression.Catch(exception, Expression.Constant(3))
                );
            Assert.Equal(2, Expression.Lambda<Func<int>>(tryExp).CompileToTestMethod()());
        }

        [Fact]
        public void FilterOverwiteExceptionVisibleToHandler()
        {
            ParameterExpression exception = Expression.Variable(typeof(TestException));
            TryExpression tryExp = Expression.TryCatch(
                Expression.Throw(Expression.Constant(new TestException()), typeof(bool)),
                Expression.Catch(
                    exception,
                    Expression.ReferenceEqual(Expression.Constant(null), exception),
                    Expression.Block(
                        Expression.Assign(exception, Expression.Constant(null, typeof(TestException))),
                        Expression.Constant(true)
                        )
                    )
                );
            Assert.True(Expression.Lambda<Func<bool>>(tryExp).CompileToTestMethod()());
        }

        [Fact]
        public void FilterOverwriteExceptionNotVisibleToNextFilterOrHandler()
        {
            ParameterExpression exception = Expression.Variable(typeof(TestException));
            TryExpression tryExp = Expression.TryCatch(
                Expression.Throw(Expression.Constant(new TestException()), typeof(int)),
                Expression.Catch(
                    exception,
                    Expression.Constant(-1),
                    Expression.Block(
                        Expression.Assign(exception, Expression.Constant(null, typeof(TestException))),
                        Expression.Constant(false)
                        )
                    ),
                Expression.Catch(
                    exception,
                    Expression.Property(Expression.Property(exception, "Message"), "Length"),
                    Expression.ReferenceNotEqual(exception, Expression.Constant(null))
                )
            );
            Assert.Equal(24, Expression.Lambda<Func<int>>(tryExp).CompileToTestMethod()());
        }

        [Fact]
        public void FilterBeforeInnerFinally()
        {
            StringBuilder sb = new StringBuilder();

            /*
            Comparable to:

            try
            {
                try
                {
                    sb.Append("A");
                    throw new TestException();
                }
                finally
                {
                    sb.Append("B");
                }
            }
            catch (TestException) when (sb.Append("C") != null)
            {
                sb.Append("D");
            }

            The filter should execute on the first pass, so the result should be "ACBD".
            */

            ConstantExpression builder = Expression.Constant(sb);
            Type[] noTypes = Array.Empty<Type>();
            TryExpression tryExp = Expression.TryCatch(
                Expression.TryFinally(
                    Expression.Block(
                        Expression.Call(builder, "Append", noTypes, Expression.Constant('A')),
                        Expression.Throw(Expression.Constant(new TestException()), typeof(StringBuilder))
                        ),
                    Expression.Call(builder, "Append", noTypes, Expression.Constant('B'))
                ),
                Expression.Catch(
                    typeof(TestException),
                    Expression.Call(builder, "Append", noTypes, Expression.Constant('D')),
                    Expression.ReferenceNotEqual(Expression.Call(builder, "Append", noTypes, Expression.Constant('C')), Expression.Constant(null))
                    )
                );
            Func<StringBuilder> func = Expression.Lambda<Func<StringBuilder>>(tryExp).CompileToTestMethod();
            Assert.Equal("ACBD", func().ToString());
        }

        [Fact]
        public void FilterBeforeInnerFault()
        {
            StringBuilder sb = new StringBuilder();
            ConstantExpression builder = Expression.Constant(sb);
            Type[] noTypes = Array.Empty<Type>();
            TryExpression tryExp = Expression.TryCatch(
                Expression.TryFault(
                    Expression.Block(
                        Expression.Call(builder, "Append", noTypes, Expression.Constant('A')),
                        Expression.Throw(Expression.Constant(new TestException()), typeof(StringBuilder))
                        ),
                    Expression.Call(builder, "Append", noTypes, Expression.Constant('B'))
                ),
                Expression.Catch(
                    typeof(TestException),
                    Expression.Call(builder, "Append", noTypes, Expression.Constant('D')),
                    Expression.ReferenceNotEqual(Expression.Call(builder, "Append", noTypes, Expression.Constant('C')), Expression.Constant(null))
                    )
                );
            Func<StringBuilder> func = Expression.Lambda<Func<StringBuilder>>(tryExp).CompileToTestMethod();
            Assert.Equal("ACBD", func().ToString());
        }

        [Fact]
        public void ExceptionThrownInFilter()
        {
            // An exception in a filter should be eaten and the filter fail.

            TryExpression tryExp = Expression.TryCatch(
                Expression.Throw(Expression.Constant(new TestException()), typeof(int)),
                Expression.Catch(
                    typeof(TestException),
                    Expression.Constant(2),
                    Expression.LessThan(
                        Expression.Constant(3),
                        Expression.Throw(Expression.Constant(new InvalidOperationException()), typeof(int))
                        )
                    ),
                Expression.Catch(typeof(TestException), Expression.Constant(9))
                );
            Func<int> func = Expression.Lambda<Func<int>>(tryExp).CompileToTestMethod();
            Assert.Equal(9, func());
        }

        [Fact(Skip = "https://github.com/dotnet/corefx/issues/15719")]
        public void TryFinallyWithinFilter()
        {
            TryExpression tryExp = Expression.TryCatch(
                Expression.Throw(Expression.Constant(new TestException()), typeof(int)),
                Expression.Catch(
                    typeof(TestException),
                    Expression.Constant(1),
                    Expression.TryFinally(Expression.Constant(false), Expression.Empty())
                    ),
                Expression.Catch(
                    typeof(TestException),
                    Expression.Constant(2),
                    Expression.TryFinally(Expression.Constant(true), Expression.Empty())
                    ),
                Expression.Catch(
                    typeof(TestException),
                    Expression.Constant(3)
                    )
                );
            Func<int> func = Expression.Lambda<Func<int>>(tryExp).CompileToTestMethod();
            Assert.Equal(2, func());
        }

        [Fact(Skip = "https://github.com/dotnet/corefx/issues/15719")]
        public void TryFinallyWithinFilterCompiled()
        {
            TryFinallyWithinFilter();
        }

        [Fact(Skip = "https://github.com/dotnet/corefx/issues/15719")]
        public void TryCatchWithinFilter()
        {
            TryExpression tryExp = Expression.TryCatch(
                Expression.Throw(Expression.Constant(new TestException()), typeof(int)),
                Expression.Catch(
                    typeof(TestException),
                    Expression.Constant(1),
                    Expression.TryCatch(Expression.Constant(false), Expression.Catch(typeof(Expression), Expression.Constant(false)))
                    ),
                Expression.Catch(
                    typeof(TestException),
                    Expression.Constant(2),
                    Expression.TryCatch(Expression.Constant(true), Expression.Catch(typeof(Expression), Expression.Constant(true)))
                    ),
                Expression.Catch(
                    typeof(TestException),
                    Expression.Constant(3)
                    )
                );
            Func<int> func = Expression.Lambda<Func<int>>(tryExp).CompileToTestMethod();
            Assert.Equal(2, func());
        }

        [Fact(Skip = "https://github.com/dotnet/corefx/issues/15719")]
        public void TryCatchWithinFilterCompiled()
        {
            TryCatchWithinFilter();
        }

        [Fact(Skip = "https://github.com/dotnet/corefx/issues/15719")]
        public void TryCatchThrowingWithinFilter()
        {
            TryExpression tryExp = Expression.TryCatch(
                Expression.Throw(Expression.Constant(new TestException()), typeof(int)),
                Expression.Catch(
                    typeof(TestException),
                    Expression.Constant(1),
                    Expression.TryCatch(Expression.Throw(Expression.Constant(new TestException()), typeof(bool)), Expression.Catch(typeof(Exception), Expression.Constant(false)))
                    ),
                Expression.Catch(
                    typeof(TestException),
                    Expression.Constant(2),
                    Expression.TryCatch(Expression.Throw(Expression.Constant(new TestException()), typeof(bool)), Expression.Catch(typeof(Exception), Expression.Constant(true)))
                    ),
                Expression.Catch(
                    typeof(TestException),
                    Expression.Constant(3)
                    )
                );
            Func<int> func = Expression.Lambda<Func<int>>(tryExp).CompileToTestMethod();
            Assert.Equal(2, func());
        }

        [Fact(Skip = "https://github.com/dotnet/corefx/issues/15719")]
        public void TryCatchThrowingWithinFilterCompiled()
        {
            TryCatchThrowingWithinFilter();
        }

        public bool MethodWithManyArguments(
            int x, int y, int z,
            int α, int β, int γ, int δ,
            int klaatu, int barada, int nikto,
            int anáil, int nathrach, int ortha, int bháis, int @is, int beatha, int @do, int chéal, int déanaimh,
            bool returnBack)
        {
            return returnBack;
        }

        [Fact]
        public void DeepExceptionFilter()
        {
            // An expression where the deepest use of the stack is within filters.
            MethodCallExpression deepFalse = Expression.Call(
                Expression.Constant(this),
                "MethodWithManyArguments",
                new Type[0],
                Expression.Constant(1),
                Expression.Constant(2),
                Expression.Constant(3),
                Expression.Constant(4),
                Expression.Constant(5),
                Expression.Constant(6),
                Expression.Constant(7),
                Expression.Constant(8),
                Expression.Constant(9),
                Expression.Constant(10),
                Expression.Constant(11),
                Expression.Constant(12),
                Expression.Constant(13),
                Expression.Constant(14),
                Expression.Constant(15),
                Expression.Constant(16),
                Expression.Constant(17),
                Expression.Constant(18),
                Expression.Constant(19),
                Expression.Constant(false)
                );
            MethodCallExpression deepTrue = Expression.Call(
                Expression.Constant(this),
                "MethodWithManyArguments",
                new Type[0],
                Expression.Constant(1),
                Expression.Constant(2),
                Expression.Constant(3),
                Expression.Constant(4),
                Expression.Constant(5),
                Expression.Constant(6),
                Expression.Constant(7),
                Expression.Constant(8),
                Expression.Constant(9),
                Expression.Constant(10),
                Expression.Constant(11),
                Expression.Constant(12),
                Expression.Constant(13),
                Expression.Constant(14),
                Expression.Constant(15),
                Expression.Constant(16),
                Expression.Constant(17),
                Expression.Constant(18),
                Expression.Constant(19),
                Expression.Constant(true)
                );
            TryExpression tryExp = Expression.TryCatch(
                Expression.Throw(Expression.Constant(new TestException()), typeof(int)),
                Expression.Catch(typeof(TestException), Expression.Constant(1), deepFalse),
                Expression.Catch(typeof(TestException), Expression.Constant(2), deepTrue)
                );
            Func<int> func = Expression.Lambda<Func<int>>(tryExp).CompileToTestMethod();
            Assert.Equal(2, func());
        }

        [Fact]
        public void JumpOutOfExceptionFilter()
        {
            LabelTarget target = Expression.Label();
            Expression<Func<int>> tryExp = Expression.Lambda<Func<int>>(
                Expression.TryCatch(
                    Expression.Throw(Expression.Constant(new TestException()), typeof(int)),
                    Expression.Catch(
                        typeof(TestException),
                        Expression.Block(
                            Expression.Label(target),
                            Expression.Constant(0)
                            ),
                        Expression.Block(
                            Expression.Goto(target),
                            Expression.Constant(true)
                            )
                        )
                    )
                );
            Assert.Throws<InvalidOperationException>(() => tryExp.CompileToTestMethod());
        }

        [Fact]
        public void JumpOutOfFinally()
        {
            LabelTarget target = Expression.Label();
            Expression<Func<int>> tryExp = Expression.Lambda<Func<int>>(
                Expression.Block(
                    Expression.Label(target),
                    Expression.TryFinally(
                        Expression.Throw(Expression.Constant(new TestException()), typeof(int)),
                            Expression.Block(
                                Expression.Goto(target),
                                Expression.Constant(true)
                                )
                            )
                        )
                    );
            Assert.Throws<InvalidOperationException>(() => tryExp.CompileToTestMethod());
        }

        [Fact]
        public void JumpIntoTry()
        {
            LabelTarget target = Expression.Label();
            Expression<Action> tryExp = Expression.Lambda<Action>(
                Expression.Block(
                    Expression.Goto(target),
                    Expression.TryFinally(
                        Expression.Label(target),
                        Expression.Empty())));
            Assert.Throws<InvalidOperationException>(() => tryExp.CompileToTestMethod());
        }

        [Fact]
        public void CanReturnAnythingFromExplicitVoid()
        {
            TryExpression tryExp = Expression.MakeTry(
                typeof(void),
                Expression.Constant(1),
                null,
                null,
                new[]
                {
                    Expression.Catch(typeof(InvalidCastException), Expression.Constant("hello")),
                    Expression.Catch(typeof(Exception), Expression.Constant(2.2))
                }
                );
            Expression.Lambda<Action>(tryExp).CompileToTestMethod()();
        }

        [Fact]
        public void CanReturnAnythingFromExplicitVoidVoidBody()
        {
            TryExpression tryExp = Expression.MakeTry(
                typeof(void),
                Expression.Constant(1),
                null,
                null,
                new[]
                {
                    Expression.Catch(typeof(InvalidCastException), Expression.Constant("hello")),
                    Expression.Catch(typeof(Exception), Expression.Constant(2.2))
                }
                );
            Expression.Lambda<Action>(tryExp).CompileToTestMethod()();
        }

        [Fact]
        public void CanReturnAnythingFromExplicitVoidVoidThrowingBody()
        {
            TryExpression tryExp = Expression.MakeTry(
                typeof(void),
                Expression.Throw(Expression.Constant(new InvalidOperationException())),
                null,
                null,
                new[]
                {
                    Expression.Catch(typeof(InvalidCastException), Expression.Constant("hello")),
                    Expression.Catch(typeof(Exception), Expression.Constant(2.2))
                }
                );
            Expression.Lambda<Action>(tryExp).CompileToTestMethod()();
        }

        [Fact]
        public void CanReturnAnythingFromExplicitVoidTypedThrowingBody()
        {
            TryExpression tryExp = Expression.MakeTry(
                typeof(void),
                Expression.Throw(Expression.Constant(new InvalidOperationException()), typeof(int)),
                null,
                null,
                new[]
                {
                    Expression.Catch(typeof(InvalidCastException), Expression.Constant("hello")),
                    Expression.Catch(typeof(Exception), Expression.Constant(2.2))
                }
                );
            Expression.Lambda<Action>(tryExp).CompileToTestMethod()();
        }

        [Fact]
        public void CanReturnAnythingFromExplicitVoidCatch()
        {
            TryExpression tryExp = Expression.MakeTry(
                typeof(void),
                Expression.Throw(Expression.Constant(new InvalidOperationException()), typeof(int)),
                null,
                null,
                new[]
                {
                    Expression.Catch(typeof(InvalidCastException), Expression.Empty()),
                    Expression.Catch(typeof(Exception), Expression.Constant(2.2))
                }
                );
            Expression.Lambda<Action>(tryExp).CompileToTestMethod()();
        }
    }
}
