// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class TypeIs : TypeBinaryTests
    {
        [Fact]
        public void TypePointer()
        {
            Expression exp = Expression.Constant(0);
            Type pointer = typeof(int*);
            var test = Expression.TypeIs(exp, pointer);
            var lambda = Expression.Lambda<Func<bool>>(test);
            var func = lambda.CompileToTestMethod();
            Assert.False(func());
        }


        [Theory]
        [MemberData(nameof(ExpressionAndTypeCombinations))]
        public void ExpressionEvaluationCompiled(Expression expression, Type type)
        {
            bool expected = expression.Type == typeof(void)
                ? type == typeof(void)
                : type.IsInstanceOfType(Expression.Lambda<Func<object>>(Expression.Convert(expression, typeof(object))).CompileToTestMethod()());

            Assert.Equal(expected, Expression.Lambda<Func<bool>>(Expression.TypeIs(expression, type)).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ExpressionAndTypeCombinations))]
        public void ExpressionEvaluationWithParameter(Expression expression, Type type)
        {
            if (expression.Type == typeof(void))
                return; // Can't have void parameter.

            bool expected = expression.Type == typeof(void)
                ? type == typeof(void)
                : type.IsInstanceOfType(Expression.Lambda<Func<object>>(Expression.Convert(expression, typeof(object))).CompileToTestMethod()());

            ParameterExpression param = Expression.Parameter(expression.Type);

            Func<bool> func = Expression.Lambda<Func<bool>>(
                Expression.Block(
                    new[] { param },
                    Expression.Assign(param, expression),
                    Expression.TypeIs(param, type)
                    )
                ).CompileToTestMethod();

            Assert.Equal(expected, func());
        }

    }
}
