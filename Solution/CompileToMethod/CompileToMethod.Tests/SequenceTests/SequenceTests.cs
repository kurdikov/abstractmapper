// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    namespace NWindProxy
    {
        public class Customer
        {
            public string CustomerID;
            public string ContactName;
            public string CompanyName;
            public List<Order> Orders = new List<Order>();
        }
        public class Order
        {
            public string CustomerID;
        }
    }

    public class Compiler_Tests
    {
        public class AndAlso
        {
            public bool value;
            public AndAlso(bool value) { this.value = value; }
            public static AndAlso operator &(AndAlso a1, AndAlso a2) { return new AndAlso(a1.value && a2.value); }
            public static bool operator true(AndAlso a) { return a.value; }
            public static bool operator false(AndAlso a) { return !a.value; }
            public static AndAlso operator !(AndAlso a) { return new AndAlso(false); }
            public override string ToString() { return value.ToString(); }
        }

        [Fact]
        public static void TestAndAlso()
        {
            AndAlso a1 = new AndAlso(true);
            Func<AndAlso> f1 = () => a1 && !a1;
            AndAlso r1 = f1();

            Expression<Func<AndAlso, AndAlso>> e = a => a && !a;
            AndAlso r2 = e.CompileToTestMethod()(a1);

            Assert.Equal(r2.value, r1.value);
        }

        public struct TC1
        {
            public string Name;
            public int data;

            public TC1(string name, int data)
            {
                Name = name;
                this.data = data;
            }
            public static TC1 operator &(TC1 t1, TC1 t2) { return new TC1("And", 01); }
            public static TC1 operator |(TC1 t1, TC1 t2) { return new TC1("Or", 02); }
            public static TC1 Meth1(TC1 t1, TC1 t2) { return new TC1(); }
            public static bool operator true(TC1 a) { return true; }
            public static bool operator false(TC1 a) { return false; }
        }

        [Fact]
        public static void ObjectCallOnValueType()
        {
            object st_local = new TC1();
            MethodInfo mi = typeof(object).GetMethod("ToString");
            Expression<Func<string>> lam = Expression.Lambda<Func<string>>(Expression.Call(Expression.Constant(st_local), mi, null), null);
            Func<string> f = lam.CompileToTestMethod();
        }

        [Fact]
        public static void AndAlsoLift()
        {
            TC1? tc1 = new TC1("lhs", 324589);
            TC1? tc2 = new TC1("rhs", 324589);

            ConstantExpression left = Expression.Constant(tc1, typeof(TC1?));
            ConstantExpression right = Expression.Constant(tc2, typeof(TC1?));
            ParameterExpression p0 = Expression.Parameter(typeof(TC1?), "tc1");
            ParameterExpression p1 = Expression.Parameter(typeof(TC1?), "tc2");

            BinaryExpression result = (BinaryExpression)Expression.AndAlso(left, right);
            Expression<Func<TC1?>> e1 = Expression.Lambda<Func<TC1?>>(
                Expression.Invoke(
                    Expression.Lambda<Func<TC1?, TC1?, TC1?>>((result), new ParameterExpression[] { p0, p1 }),
                    new Expression[] { left, right }),
               Enumerable.Empty<ParameterExpression>());

            Func<TC1?> f1 = e1.CompileToTestMethod();
            Assert.NotNull(f1());
            Assert.Equal(f1().Value.Name, "And");

            BinaryExpression resultOr = (BinaryExpression)Expression.OrElse(left, right);
            Expression<Func<TC1?>> e2 = Expression.Lambda<Func<TC1?>>(
                Expression.Invoke(
                    Expression.Lambda<Func<TC1?, TC1?, TC1?>>((resultOr), new ParameterExpression[] { p0, p1 }),
                    new Expression[] { left, right }),
               Enumerable.Empty<ParameterExpression>());

            Func<TC1?> f2 = e2.CompileToTestMethod();
            Assert.NotNull(f2());
            Assert.Equal(f2().Value.Name, "lhs");

            ConstantExpression constant = Expression.Constant(1.0, typeof(double));
            AssertExtensions.Throws<ArgumentException>(null, () => Expression.Lambda<Func<double?>>(constant, null));
        }

        public static int GetBound()
        {
            return 1;
        }

        public int Bound
        {
            get
            {
                return 3;
            }
        }

        public struct Complex
        {
            public int x;
            public int y;

            public static Complex operator +(Complex c)
            {
                Complex temp = new Complex();
                temp.x = c.x + 1;
                temp.y = c.y + 1;
                return temp;
            }
            public Complex(int x, int y) { this.x = x; this.y = y; }
        }

        public class CustomerWriteBack
        {
            private Customer _cust;
            public string m_x = "ha ha ha";
            public string Func0(ref string x)
            {
                x = "Changed";
                return x;
            }
            public string X
            {
                get { return m_x; }
                set { m_x = value; }
            }
            public static int Funct1(ref int i)
            {
                return i = 5;
            }
            public static int Prop { get { return 7; } set { Assert.Equal(5, value); } }

            public Customer Cust
            {
                get
                {
                    if (_cust == null) _cust = new Customer(98007, "Sree");
                    return _cust;
                }
                set
                {
                    _cust = value;
                }
            }

            public Customer ComputeCust(ref Customer cust)
            {
                cust.zip = 90008;
                cust.name = "SreeCho";
                return cust;
            }
        }

        public class Customer
        {
            public int zip;
            public string name;
            public Customer(int zip, string name) { this.zip = zip; this.name = name; }
        }

        [Fact]
        public static void Writeback()
        {
            CustomerWriteBack a = new CustomerWriteBack();
            Type t = typeof(CustomerWriteBack);
            MethodInfo mi = t.GetMethod("Func0");
            MethodInfo pi = t.GetMethod("get_X");
            MethodInfo piCust = t.GetMethod("get_Cust");
            MethodInfo miCust = t.GetMethod("ComputeCust");

            Expression<Func<int>> e1 =
                    Expression.Lambda<Func<int>>(
                        Expression.Call(typeof(CustomerWriteBack).GetMethod("Funct1"), new[] { Expression.Property(null, typeof(CustomerWriteBack).GetProperty("Prop")) }),
                        null);
            Func<int> f1 = e1.CompileToTestMethod();
            int result = f1();
            Assert.Equal(5, result);

            Expression<Func<string>> e = Expression.Lambda<Func<string>>(
                 Expression.Call(
                     Expression.Constant(a, typeof(CustomerWriteBack)),
                     mi,
                     new Expression[] { Expression.Property(Expression.Constant(a, typeof(CustomerWriteBack)), pi) }
                     ),
                 null);
            Func<string> f = e.CompileToTestMethod();
            string r = f();
            Assert.Equal(a.m_x, "Changed");

            Expression<Func<Customer>> e2 = Expression.Lambda<Func<Customer>>(
                 Expression.Call(
                     Expression.Constant(a, typeof(CustomerWriteBack)),
                     miCust,
                     new Expression[] { Expression.Property(Expression.Constant(a, typeof(CustomerWriteBack)), piCust) }
                     ),
                 null);
            Func<Customer> f2 = e2.CompileToTestMethod();
            Customer r2 = f2();
            Assert.True(a.Cust.zip == 90008 && a.Cust.name == "SreeCho");
        }

        [Fact]
        //[ActiveIssue("https://github.com/dotnet/corefx/issues/20717 - fails on x64", TargetFrameworkMonikers.UapAot)]
        public static void UnaryPlus()
        {
            ConstantExpression ce = Expression.Constant((UInt16)10);

            UnaryExpression result = Expression.UnaryPlus(ce);

            Assert.Throws<InvalidOperationException>(() =>
            {
                //unary Plus Operator
                byte val = 10;
                Expression<Func<byte>> e =
                    Expression.Lambda<Func<byte>>(
                        Expression.UnaryPlus(Expression.Constant(val, typeof(byte))),
                        Enumerable.Empty<ParameterExpression>());
            });

            //User-defined objects
            Complex comp = new Complex(10, 20);
            Expression<Func<Complex>> e1 =
                Expression.Lambda<Func<Complex>>(
                    Expression.UnaryPlus(Expression.Constant(comp, typeof(Complex))),
                    Enumerable.Empty<ParameterExpression>());
            Func<Complex> f1 = e1.CompileToTestMethod();
            Complex comp1 = f1();
            Assert.True((comp1.x == comp.x + 1 && comp1.y == comp.y + 1));

            Expression<Func<Complex, Complex>> testExpr = (x) => +x;
            Assert.Equal(testExpr.ToString(), "x => +x");
            Func<Complex, Complex> v = testExpr.CompileToTestMethod();
        }

        private struct S
        {
        }

        [Fact]
        public static void CompileRelationOveratorswithIsLiftToNullTrue()
        {
            int? x = 10;
            int? y = 2;
            ParameterExpression p1 = Expression.Parameter(typeof(int?), "x");
            ParameterExpression p2 = Expression.Parameter(typeof(int?), "y");

            Expression<Func<int?, int?, bool?>> e = Expression.Lambda<Func<int?, int?, bool?>>(
                Expression.GreaterThan(p1, p2, true, null), new ParameterExpression[] { p1, p2 });
            Func<int?, int?, bool?> f = e.CompileToTestMethod();
            bool? r = f(x, y);
            Assert.True(r.Value);

            Expression<Func<int?, int?, bool?>> e1 = Expression.Lambda<Func<int?, int?, bool?>>(
                Expression.LessThan(p1, p2, true, null), new ParameterExpression[] { p1, p2 });
            f = e1.CompileToTestMethod();
            r = f(x, y);
            Assert.False(r.Value);

            Expression<Func<int?, int?, bool?>> e2 = Expression.Lambda<Func<int?, int?, bool?>>(
                Expression.GreaterThanOrEqual(p1, p2, true, null), new ParameterExpression[] { p1, p2 });
            f = e2.CompileToTestMethod();
            r = f(x, y);
            Assert.True(r.Value);

            Expression<Func<int?, int?, bool?>> e3 = Expression.Lambda<Func<int?, int?, bool?>>(
                Expression.Equal(p1, p2, true, null), new ParameterExpression[] { p1, p2 });
            f = e3.CompileToTestMethod();
            r = f(x, y);
            Assert.False(r.Value);

            Expression<Func<int?, int?, bool?>> e4 = Expression.Lambda<Func<int?, int?, bool?>>(
                Expression.LessThanOrEqual(p1, p2, true, null), new ParameterExpression[] { p1, p2 });
            f = e4.CompileToTestMethod();
            r = f(x, y);
            Assert.False(r.Value);

            Expression<Func<int?, int?, bool?>> e5 = Expression.Lambda<Func<int?, int?, bool?>>(
                Expression.NotEqual(p1, p2, true, null), new ParameterExpression[] { p1, p2 });
            f = e5.CompileToTestMethod();
            r = f(x, y);
            Assert.True(r.Value);

            int? n = 10;
            Expression<Func<bool?>> e6 = Expression.Lambda<Func<bool?>>(
                Expression.NotEqual(
                    Expression.Constant(n, typeof(int?)),
                    Expression.Convert(Expression.Constant(null, typeof(Object)), typeof(int?)),
                    true,
                    null),
                null);
            Func<bool?> f6 = e6.CompileToTestMethod();
            Assert.Null(f6());
        }

        public class TestClass : IEquatable<TestClass>
        {
            private int _val;
            public TestClass(string S, int Val)
            {
                this.S = S;
                _val = Val;
            }

            public string S;
            public int Val { get { return _val; } set { _val = value; } }

            public override bool Equals(object o)
            {
                return (o is TestClass) && Equals((TestClass)o);
            }
            public bool Equals(TestClass other)
            {
                return other.S == S;
            }
            public override int GetHashCode()
            {
                return S.GetHashCode();
            }
        }


        private class AnonHelperClass1
        {
            public Expression<Func<decimal>> mem1;
            public AnonHelperClass1(Expression<Func<decimal>> mem1) { this.mem1 = mem1; }
        }

        [Fact]
        public static void NewExpressionwithMemberAssignInit()
        {
            string s = "Bad Mojo";
            int val = 10;

            ConstructorInfo constructor = typeof(TestClass).GetConstructor(new Type[] { typeof(string), typeof(int) });
            MemberInfo[] members = new MemberInfo[] { typeof(TestClass).GetField("S"), typeof(TestClass).GetProperty("Val") };
            Expression[] expressions = new Expression[] { Expression.Constant(s, typeof(string)), Expression.Constant(val, typeof(int)) };

            Expression<Func<TestClass>> e = Expression.Lambda<Func<TestClass>>(
               Expression.New(constructor, expressions, members),
               Enumerable.Empty<ParameterExpression>());
            Func<TestClass> f = e.CompileToTestMethod();
            Assert.True(object.Equals(f(), new TestClass(s, val)));

            List<MemberInfo> members1 = new List<MemberInfo>();
            members1.Add(typeof(TestClass).GetField("S"));
            members1.Add(typeof(TestClass).GetProperty("Val"));

            Expression<Func<TestClass>> e1 = Expression.Lambda<Func<TestClass>>(
               Expression.New(constructor, expressions, members1),
               Enumerable.Empty<ParameterExpression>());
            Func<TestClass> f1 = e1.CompileToTestMethod();
            Assert.True(object.Equals(f1(), new TestClass(s, val)));
            MemberInfo mem1 = typeof(AnonHelperClass1).GetField("mem1");
            LambdaExpression ce1 = Expression.Lambda(Expression.Constant(45m, typeof(decimal)));
            ConstructorInfo constructor1 = typeof(AnonHelperClass1).GetConstructor(new Type[] { typeof(Expression<Func<decimal>>) });

            Expression[] arguments = new Expression[] { ce1 };
            MemberInfo[] members2 = new MemberInfo[] { mem1 };

            NewExpression result = Expression.New(constructor1, arguments, members2);
            Assert.NotNull(result);
        }

        [Fact]
        public static void TypeAsNullableToObject()
        {
            Expression<Func<object>> e = Expression.Lambda<Func<object>>(Expression.TypeAs(Expression.Constant(0, typeof(int?)), typeof(object)));
            Func<object> f = e.CompileToTestMethod(); // System.ArgumentException: Unhandled unary: TypeAs
            Assert.Equal(0, f());
        }

        [Fact]
        public static void TypesIsConstantValueType()
        {
            Expression<Func<bool>> e = Expression.Lambda<Func<bool>>(Expression.TypeIs(Expression.Constant(5), typeof(object)));
            Func<bool> f = e.CompileToTestMethod();
            Assert.True(f());
        }

        [Fact]
        public static void ConstantEmitsValidIL()
        {
            Expression<Func<byte>> e = Expression.Lambda<Func<byte>>(Expression.Constant((byte)0), Enumerable.Empty<ParameterExpression>());
            Func<byte> f = e.CompileToTestMethod();
            Assert.Equal((byte)0, f());
        }

        public struct MyStruct : IComparable
        {
            int IComparable.CompareTo(object other)
            {
                return 0;
            }
        }

        [Fact]
        public static void Casts()
        {
            // System.ValueType to value type
            Assert.Equal(10, TestCast<System.ValueType, int>(10));

            // System.ValueType to enum type
            Assert.Equal(ExpressionType.Add, TestCast<System.ValueType, ExpressionType>(ExpressionType.Add));

            // System.Enum to enum type
            Assert.Equal(ExpressionType.Add, TestCast<System.Enum, ExpressionType>(ExpressionType.Add));

            // System.ValueType to nullable value type
            Assert.Equal(10, TestCast<System.ValueType, int?>(10));

            // System.ValueType to nullable enum type
            Assert.Equal(ExpressionType.Add, TestCast<System.ValueType, ExpressionType?>(ExpressionType.Add));

            // System.Enum to nullable enum type
            Assert.Equal(ExpressionType.Add, TestCast<System.Enum, ExpressionType?>(ExpressionType.Add));

            // Enum to System.Enum
            Assert.Equal(ExpressionType.Add, TestCast<ExpressionType, System.Enum>(ExpressionType.Add));

            // Enum to System.ValueType
            Assert.Equal(ExpressionType.Add, TestCast<ExpressionType, System.ValueType>(ExpressionType.Add));

            // nullable enum to System.Enum
            Assert.Equal(ExpressionType.Add, TestCast<ExpressionType?, System.Enum>(ExpressionType.Add));

            // nullable enum to System.ValueType
            Assert.Equal(ExpressionType.Add, TestCast<ExpressionType?, System.ValueType>(ExpressionType.Add));

            // nullable to object (box)
            Assert.Equal(10, TestCast<int?, object>(10));

            // object to nullable (unbox)
            Assert.Equal(10, TestCast<object, int?>(10));

            // nullable to interface (box + cast)
            TestCast<int?, IComparable>(10);

            // interface to nullable (unbox)
            TestCast<IComparable, int?>(10);

            // interface to interface
            TestCast<IComparable, IEquatable<int>>(10);
            TestCast<IEquatable<int>, IComparable>(10);

            // value type to object (box)
            Assert.Equal(10, TestCast<int, object>(10));

            // object to value type (unbox)
            Assert.Equal(10, TestCast<object, int>(10));

            // tests with user defined struct
            TestCast<ValueType, MyStruct>(new MyStruct());
            TestCast<MyStruct, ValueType>(new MyStruct());
            TestCast<object, MyStruct>(new MyStruct());
            TestCast<MyStruct, object>(new MyStruct());
            TestCast<IComparable, MyStruct>(new MyStruct());
            TestCast<MyStruct, IComparable>(new MyStruct());
            TestCast<ValueType, MyStruct?>(new MyStruct());
            TestCast<MyStruct?, ValueType>(new MyStruct());
            TestCast<object, MyStruct?>(new MyStruct());
            TestCast<MyStruct?, object>(new MyStruct());
            TestCast<IComparable, MyStruct?>(new MyStruct());
            TestCast<MyStruct?, IComparable>(new MyStruct());
        }

        private static S TestCast<T, S>(T value)
        {
            Func<S> d = Expression.Lambda<Func<S>>(Expression.Convert(Expression.Constant(value, typeof(T)), typeof(S))).CompileToTestMethod();
            return d();
        }

        [Fact]
        public static void Conversions()
        {
            Assert.Equal((byte)10, TestConvert<byte, byte>(10));
            Assert.Equal((byte)10, TestConvert<sbyte, byte>(10));
            Assert.Equal((byte)10, TestConvert<short, byte>(10));
            Assert.Equal((byte)10, TestConvert<ushort, byte>(10));
            Assert.Equal((byte)10, TestConvert<int, byte>(10));
            Assert.Equal((byte)10, TestConvert<uint, byte>(10));
            Assert.Equal((byte)10, TestConvert<long, byte>(10));
            Assert.Equal((byte)10, TestConvert<ulong, byte>(10));
            Assert.Equal((byte)10, TestConvert<float, byte>(10.0f));
            Assert.Equal((byte)10, TestConvert<float, byte>(10.0f));
            Assert.Equal((byte)10, TestConvert<double, byte>(10.0));
            Assert.Equal((byte)10, TestConvert<double, byte>(10.0));
            Assert.Equal((byte)10, TestConvert<decimal, byte>(10m));
            Assert.Equal((byte)10, TestConvert<decimal, byte>(10m));

            Assert.Equal((short)10, TestConvert<byte, short>(10));
            Assert.Equal((short)10, TestConvert<sbyte, short>(10));
            Assert.Equal((short)10, TestConvert<short, short>(10));
            Assert.Equal((short)10, TestConvert<ushort, short>(10));
            Assert.Equal((short)10, TestConvert<int, short>(10));
            Assert.Equal((short)10, TestConvert<uint, short>(10));
            Assert.Equal((short)10, TestConvert<long, short>(10));
            Assert.Equal((short)10, TestConvert<ulong, short>(10));
            Assert.Equal((short)10, TestConvert<float, short>(10.0f));
            Assert.Equal((short)10, TestConvert<double, short>(10.0));
            Assert.Equal((short)10, TestConvert<decimal, short>(10m));

            Assert.Equal((int)10, TestConvert<byte, int>(10));
            Assert.Equal((int)10, TestConvert<sbyte, int>(10));
            Assert.Equal((int)10, TestConvert<short, int>(10));
            Assert.Equal((int)10, TestConvert<ushort, int>(10));
            Assert.Equal((int)10, TestConvert<int, int>(10));
            Assert.Equal((int)10, TestConvert<uint, int>(10));
            Assert.Equal((int)10, TestConvert<long, int>(10));
            Assert.Equal((int)10, TestConvert<ulong, int>(10));
            Assert.Equal((int)10, TestConvert<float, int>(10.0f));
            Assert.Equal((int)10, TestConvert<double, int>(10.0));
            Assert.Equal((int)10, TestConvert<decimal, int>(10m));

            Assert.Equal((long)10, TestConvert<byte, long>(10));
            Assert.Equal((long)10, TestConvert<sbyte, long>(10));
            Assert.Equal((long)10, TestConvert<short, long>(10));
            Assert.Equal((long)10, TestConvert<ushort, long>(10));
            Assert.Equal((long)10, TestConvert<int, long>(10));
            Assert.Equal((long)10, TestConvert<uint, long>(10));
            Assert.Equal((long)10, TestConvert<long, long>(10));
            Assert.Equal((long)10, TestConvert<ulong, long>(10));
            Assert.Equal((long)10, TestConvert<float, long>(10.0f));
            Assert.Equal((long)10, TestConvert<double, long>(10.0));
            Assert.Equal((long)10, TestConvert<decimal, long>(10m));

            Assert.Equal((double)10, TestConvert<byte, double>(10));
            Assert.Equal((double)10, TestConvert<sbyte, double>(10));
            Assert.Equal((double)10, TestConvert<short, double>(10));
            Assert.Equal((double)10, TestConvert<ushort, double>(10));
            Assert.Equal((double)10, TestConvert<int, double>(10));
            Assert.Equal((double)10, TestConvert<uint, double>(10));
            Assert.Equal((double)10, TestConvert<long, double>(10));
            Assert.Equal((double)10, TestConvert<ulong, double>(10));
            Assert.Equal((double)10, TestConvert<float, double>(10.0f));
            Assert.Equal((double)10, TestConvert<double, double>(10.0));
            Assert.Equal((double)10, TestConvert<decimal, double>(10m));

            Assert.Equal((decimal)10, TestConvert<byte, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<sbyte, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<short, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<ushort, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<int, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<uint, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<long, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<ulong, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<float, decimal>(10.0f));
            Assert.Equal((decimal)10, TestConvert<double, decimal>(10.0));
            Assert.Equal((decimal)10, TestConvert<decimal, decimal>(10m));

            // nullable to non-nullable
            Assert.Equal((byte)10, TestConvert<byte?, byte>(10));
            Assert.Equal((byte)10, TestConvert<sbyte?, byte>(10));
            Assert.Equal((byte)10, TestConvert<short?, byte>(10));
            Assert.Equal((byte)10, TestConvert<ushort?, byte>(10));
            Assert.Equal((byte)10, TestConvert<int?, byte>(10));
            Assert.Equal((byte)10, TestConvert<uint?, byte>(10));
            Assert.Equal((byte)10, TestConvert<long?, byte>(10));
            Assert.Equal((byte)10, TestConvert<ulong?, byte>(10));
            Assert.Equal((byte)10, TestConvert<float?, byte>(10.0f));
            Assert.Equal((byte)10, TestConvert<float?, byte>(10.0f));
            Assert.Equal((byte)10, TestConvert<double?, byte>(10.0));
            Assert.Equal((byte)10, TestConvert<double?, byte>(10.0));
            Assert.Equal((byte)10, TestConvert<decimal?, byte>(10m));
            Assert.Equal((byte)10, TestConvert<decimal?, byte>(10m));

            Assert.Equal((short)10, TestConvert<byte?, short>(10));
            Assert.Equal((short)10, TestConvert<sbyte?, short>(10));
            Assert.Equal((short)10, TestConvert<short?, short>(10));
            Assert.Equal((short)10, TestConvert<ushort?, short>(10));
            Assert.Equal((short)10, TestConvert<int?, short>(10));
            Assert.Equal((short)10, TestConvert<uint?, short>(10));
            Assert.Equal((short)10, TestConvert<long?, short>(10));
            Assert.Equal((short)10, TestConvert<ulong?, short>(10));
            Assert.Equal((short)10, TestConvert<float?, short>(10.0f));
            Assert.Equal((short)10, TestConvert<double?, short>(10.0));
            Assert.Equal((short)10, TestConvert<decimal?, short>(10m));

            Assert.Equal((int)10, TestConvert<byte?, int>(10));
            Assert.Equal((int)10, TestConvert<sbyte?, int>(10));
            Assert.Equal((int)10, TestConvert<short?, int>(10));
            Assert.Equal((int)10, TestConvert<ushort?, int>(10));
            Assert.Equal((int)10, TestConvert<int?, int>(10));
            Assert.Equal((int)10, TestConvert<uint?, int>(10));
            Assert.Equal((int)10, TestConvert<long?, int>(10));
            Assert.Equal((int)10, TestConvert<ulong?, int>(10));
            Assert.Equal((int)10, TestConvert<float?, int>(10.0f));
            Assert.Equal((int)10, TestConvert<double?, int>(10.0));
            Assert.Equal((int)10, TestConvert<decimal?, int>(10m));

            Assert.Equal((long)10, TestConvert<byte?, long>(10));
            Assert.Equal((long)10, TestConvert<sbyte?, long>(10));
            Assert.Equal((long)10, TestConvert<short?, long>(10));
            Assert.Equal((long)10, TestConvert<ushort?, long>(10));
            Assert.Equal((long)10, TestConvert<int?, long>(10));
            Assert.Equal((long)10, TestConvert<uint?, long>(10));
            Assert.Equal((long)10, TestConvert<long?, long>(10));
            Assert.Equal((long)10, TestConvert<ulong?, long>(10));
            Assert.Equal((long)10, TestConvert<float?, long>(10.0f));
            Assert.Equal((long)10, TestConvert<double?, long>(10.0));
            Assert.Equal((long)10, TestConvert<decimal?, long>(10m));

            Assert.Equal((double)10, TestConvert<byte?, double>(10));
            Assert.Equal((double)10, TestConvert<sbyte?, double>(10));
            Assert.Equal((double)10, TestConvert<short?, double>(10));
            Assert.Equal((double)10, TestConvert<ushort?, double>(10));
            Assert.Equal((double)10, TestConvert<int?, double>(10));
            Assert.Equal((double)10, TestConvert<uint?, double>(10));
            Assert.Equal((double)10, TestConvert<long?, double>(10));
            Assert.Equal((double)10, TestConvert<ulong?, double>(10));
            Assert.Equal((double)10, TestConvert<float?, double>(10.0f));
            Assert.Equal((double)10, TestConvert<double?, double>(10.0));
            Assert.Equal((double)10, TestConvert<decimal?, double>(10m));

            Assert.Equal((decimal)10, TestConvert<byte?, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<sbyte?, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<short?, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<ushort?, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<int?, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<uint?, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<long?, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<ulong?, decimal>(10));
            Assert.Equal((decimal)10, TestConvert<float?, decimal>(10.0f));
            Assert.Equal((decimal)10, TestConvert<double?, decimal>(10.0));
            Assert.Equal((decimal)10, TestConvert<decimal?, decimal>(10m));

            // non-nullable to nullable
            Assert.Equal((byte?)10, TestConvert<byte, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<sbyte, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<short, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<ushort, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<int, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<uint, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<long, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<ulong, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<float, byte?>(10.0f));
            Assert.Equal((byte?)10, TestConvert<float, byte?>(10.0f));
            Assert.Equal((byte?)10, TestConvert<double, byte?>(10.0));
            Assert.Equal((byte?)10, TestConvert<double, byte?>(10.0));
            Assert.Equal((byte?)10, TestConvert<decimal, byte?>(10m));
            Assert.Equal((byte?)10, TestConvert<decimal, byte?>(10m));

            Assert.Equal((short?)10, TestConvert<byte, short?>(10));
            Assert.Equal((short?)10, TestConvert<sbyte, short?>(10));
            Assert.Equal((short?)10, TestConvert<short, short?>(10));
            Assert.Equal((short?)10, TestConvert<ushort, short?>(10));
            Assert.Equal((short?)10, TestConvert<int, short?>(10));
            Assert.Equal((short?)10, TestConvert<uint, short?>(10));
            Assert.Equal((short?)10, TestConvert<long, short?>(10));
            Assert.Equal((short?)10, TestConvert<ulong, short?>(10));
            Assert.Equal((short?)10, TestConvert<float, short?>(10.0f));
            Assert.Equal((short?)10, TestConvert<double, short?>(10.0));
            Assert.Equal((short?)10, TestConvert<decimal, short?>(10m));

            Assert.Equal((int?)10, TestConvert<byte, int?>(10));
            Assert.Equal((int?)10, TestConvert<sbyte, int?>(10));
            Assert.Equal((int?)10, TestConvert<short, int?>(10));
            Assert.Equal((int?)10, TestConvert<ushort, int?>(10));
            Assert.Equal((int?)10, TestConvert<int, int?>(10));
            Assert.Equal((int?)10, TestConvert<uint, int?>(10));
            Assert.Equal((int?)10, TestConvert<long, int?>(10));
            Assert.Equal((int?)10, TestConvert<ulong, int?>(10));
            Assert.Equal((int?)10, TestConvert<float, int?>(10.0f));
            Assert.Equal((int?)10, TestConvert<double, int?>(10.0));
            Assert.Equal((int?)10, TestConvert<decimal, int?>(10m));

            Assert.Equal((long?)10, TestConvert<byte, long?>(10));
            Assert.Equal((long?)10, TestConvert<sbyte, long?>(10));
            Assert.Equal((long?)10, TestConvert<short, long?>(10));
            Assert.Equal((long?)10, TestConvert<ushort, long?>(10));
            Assert.Equal((long?)10, TestConvert<int, long?>(10));
            Assert.Equal((long?)10, TestConvert<uint, long?>(10));
            Assert.Equal((long?)10, TestConvert<long, long?>(10));
            Assert.Equal((long?)10, TestConvert<ulong, long?>(10));
            Assert.Equal((long?)10, TestConvert<float, long?>(10.0f));
            Assert.Equal((long?)10, TestConvert<double, long?>(10.0));
            Assert.Equal((long?)10, TestConvert<decimal, long?>(10m));

            Assert.Equal((double?)10, TestConvert<byte, double?>(10));
            Assert.Equal((double?)10, TestConvert<sbyte, double?>(10));
            Assert.Equal((double?)10, TestConvert<short, double?>(10));
            Assert.Equal((double?)10, TestConvert<ushort, double?>(10));
            Assert.Equal((double?)10, TestConvert<int, double?>(10));
            Assert.Equal((double?)10, TestConvert<uint, double?>(10));
            Assert.Equal((double?)10, TestConvert<long, double?>(10));
            Assert.Equal((double?)10, TestConvert<ulong, double?>(10));
            Assert.Equal((double?)10, TestConvert<float, double?>(10.0f));
            Assert.Equal((double?)10, TestConvert<double, double?>(10.0));
            Assert.Equal((double?)10, TestConvert<decimal, double?>(10m));

            Assert.Equal((decimal?)10, TestConvert<byte, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<sbyte, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<short, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<ushort, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<int, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<uint, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<long, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<ulong, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<float, decimal?>(10.0f));
            Assert.Equal((decimal?)10, TestConvert<double, decimal?>(10.0));
            Assert.Equal((decimal?)10, TestConvert<decimal, decimal?>(10m));

            // nullable to nullable
            Assert.Equal((byte?)10, TestConvert<byte?, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<sbyte?, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<short?, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<ushort?, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<int?, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<uint?, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<long?, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<ulong?, byte?>(10));
            Assert.Equal((byte?)10, TestConvert<float?, byte?>(10.0f));
            Assert.Equal((byte?)10, TestConvert<float?, byte?>(10.0f));
            Assert.Equal((byte?)10, TestConvert<double?, byte?>(10.0));
            Assert.Equal((byte?)10, TestConvert<double?, byte?>(10.0));
            Assert.Equal((byte?)10, TestConvert<decimal?, byte?>(10m));
            Assert.Equal((byte?)10, TestConvert<decimal?, byte?>(10m));

            Assert.Equal((short?)10, TestConvert<byte?, short?>(10));
            Assert.Equal((short?)10, TestConvert<sbyte?, short?>(10));
            Assert.Equal((short?)10, TestConvert<short?, short?>(10));
            Assert.Equal((short?)10, TestConvert<ushort?, short?>(10));
            Assert.Equal((short?)10, TestConvert<int?, short?>(10));
            Assert.Equal((short?)10, TestConvert<uint?, short?>(10));
            Assert.Equal((short?)10, TestConvert<long?, short?>(10));
            Assert.Equal((short?)10, TestConvert<ulong?, short?>(10));
            Assert.Equal((short?)10, TestConvert<float?, short?>(10.0f));
            Assert.Equal((short?)10, TestConvert<double?, short?>(10.0));
            Assert.Equal((short?)10, TestConvert<decimal?, short?>(10m));

            Assert.Equal((int?)10, TestConvert<byte?, int?>(10));
            Assert.Equal((int?)10, TestConvert<sbyte?, int?>(10));
            Assert.Equal((int?)10, TestConvert<short?, int?>(10));
            Assert.Equal((int?)10, TestConvert<ushort?, int?>(10));
            Assert.Equal((int?)10, TestConvert<int?, int?>(10));
            Assert.Equal((int?)10, TestConvert<uint?, int?>(10));
            Assert.Equal((int?)10, TestConvert<long?, int?>(10));
            Assert.Equal((int?)10, TestConvert<ulong?, int?>(10));
            Assert.Equal((int?)10, TestConvert<float?, int?>(10.0f));
            Assert.Equal((int?)10, TestConvert<double?, int?>(10.0));
            Assert.Equal((int?)10, TestConvert<decimal?, int?>(10m));

            Assert.Equal((long?)10, TestConvert<byte?, long?>(10));
            Assert.Equal((long?)10, TestConvert<sbyte?, long?>(10));
            Assert.Equal((long?)10, TestConvert<short?, long?>(10));
            Assert.Equal((long?)10, TestConvert<ushort?, long?>(10));
            Assert.Equal((long?)10, TestConvert<int?, long?>(10));
            Assert.Equal((long?)10, TestConvert<uint?, long?>(10));
            Assert.Equal((long?)10, TestConvert<long?, long?>(10));
            Assert.Equal((long?)10, TestConvert<ulong?, long?>(10));
            Assert.Equal((long?)10, TestConvert<float?, long?>(10.0f));
            Assert.Equal((long?)10, TestConvert<double?, long?>(10.0));
            Assert.Equal((long?)10, TestConvert<decimal?, long?>(10m));

            Assert.Equal((double?)10, TestConvert<byte?, double?>(10));
            Assert.Equal((double?)10, TestConvert<sbyte?, double?>(10));
            Assert.Equal((double?)10, TestConvert<short?, double?>(10));
            Assert.Equal((double?)10, TestConvert<ushort?, double?>(10));
            Assert.Equal((double?)10, TestConvert<int?, double?>(10));
            Assert.Equal((double?)10, TestConvert<uint?, double?>(10));
            Assert.Equal((double?)10, TestConvert<long?, double?>(10));
            Assert.Equal((double?)10, TestConvert<ulong?, double?>(10));
            Assert.Equal((double?)10, TestConvert<float?, double?>(10.0f));
            Assert.Equal((double?)10, TestConvert<double?, double?>(10.0));
            Assert.Equal((double?)10, TestConvert<decimal?, double?>(10m));

            Assert.Equal((decimal?)10, TestConvert<byte?, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<sbyte?, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<short?, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<ushort?, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<int?, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<uint?, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<long?, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<ulong?, decimal?>(10));
            Assert.Equal((decimal?)10, TestConvert<float?, decimal?>(10.0f));
            Assert.Equal((decimal?)10, TestConvert<double?, decimal?>(10.0));
            Assert.Equal((decimal?)10, TestConvert<decimal?, decimal?>(10m));
        }

        [Fact]
        public static void ConvertMinMax()
        {
            unchecked
            {
                Assert.Equal((float)uint.MaxValue, TestConvert<uint, float>(uint.MaxValue));
                Assert.Equal((double)uint.MaxValue, TestConvert<uint, double>(uint.MaxValue));
                Assert.Equal((float?)uint.MaxValue, TestConvert<uint, float?>(uint.MaxValue));
                Assert.Equal((double?)uint.MaxValue, TestConvert<uint, double?>(uint.MaxValue));

                Assert.Equal((float)ulong.MaxValue, TestConvert<ulong, float>(ulong.MaxValue));
                Assert.Equal((double)ulong.MaxValue, TestConvert<ulong, double>(ulong.MaxValue));
                Assert.Equal((float?)ulong.MaxValue, TestConvert<ulong, float?>(ulong.MaxValue));
                Assert.Equal((double?)ulong.MaxValue, TestConvert<ulong, double?>(ulong.MaxValue));

                /*
                 * needs more thought about what should happen.. these have undefined runtime behavior.
                 * results depend on whether values are in registers or locals, debug or retail etc.
                 *
                float fmin = float.MinValue;
                float fmax = float.MaxValue;
                double dmin = double.MinValue;
                double dmax = double.MaxValue;

                Assert.AreEqual((uint)fmin, TestConvert<float, uint>(fmin));
                Assert.AreEqual((ulong)fmax, TestConvert<float, ulong>(fmax));
                Assert.AreEqual((uint?)fmin, TestConvert<float, uint?>(fmin));
                Assert.AreEqual((ulong?)fmax, TestConvert<float, ulong?>(fmax));

                Assert.AreEqual((uint)dmin, TestConvert<double, uint>(dmin));
                Assert.AreEqual((ulong)dmax, TestConvert<double, ulong>(dmax));
                Assert.AreEqual((uint?)dmin, TestConvert<double, uint?>(dmin));
                Assert.AreEqual((ulong?)dmax, TestConvert<double, ulong?>(dmax));
                 */

                Assert.Equal((float)(uint?)uint.MaxValue, TestConvert<uint?, float>(uint.MaxValue));
                Assert.Equal((double)(uint?)uint.MaxValue, TestConvert<uint?, double>(uint.MaxValue));
                Assert.Equal((float?)(uint?)uint.MaxValue, TestConvert<uint?, float?>(uint.MaxValue));
                Assert.Equal((double?)(uint?)uint.MaxValue, TestConvert<uint?, double?>(uint.MaxValue));

                Assert.Equal((float)(ulong?)ulong.MaxValue, TestConvert<ulong?, float>(ulong.MaxValue));
                Assert.Equal((double)(ulong?)ulong.MaxValue, TestConvert<ulong?, double>(ulong.MaxValue));
                Assert.Equal((float?)(ulong?)ulong.MaxValue, TestConvert<ulong?, float?>(ulong.MaxValue));
                Assert.Equal((double?)(ulong?)ulong.MaxValue, TestConvert<ulong?, double?>(ulong.MaxValue));
            }
        }

        private static S TestConvert<T, S>(T value)
        {
            Func<S> d = Expression.Lambda<Func<S>>(Expression.Convert(Expression.Constant(value, typeof(T)), typeof(S))).CompileToTestMethod();
            return d();
        }

        private static S TestConvertChecked<T, S>(T value)
        {
            Func<S> d = Expression.Lambda<Func<S>>(Expression.ConvertChecked(Expression.Constant(value, typeof(T)), typeof(S))).CompileToTestMethod();
            return d();
        }

        [Fact]
        public static void ConvertNullToInt()
        {
            Assert.Throws<NullReferenceException>(() =>
            {
                Expression<Func<ValueType, int>> e = v => (int)v;
                Func<ValueType, int> f = e.CompileToTestMethod();
                f(null);
            });
        }

        [Fact]
        public static void ShiftWithMismatchedNulls()
        {
            Expression<Func<byte?, int, int?>> e = (byte? b, int i) => (byte?)(b << i);
            Func<byte?, int, int?> f = e.CompileToTestMethod();
            Assert.Equal(20, f(5, 2));
        }

        [Fact]
        public static void CoalesceChars()
        {
            ParameterExpression x = Expression.Parameter(typeof(char?), "x");
            ParameterExpression y = Expression.Parameter(typeof(char?), "y");
            Expression<Func<char?, char?, char?>> e =
                Expression.Lambda<Func<char?, char?, char?>>(
                    Expression.Coalesce(x, y),
                    new ParameterExpression[] { x, y });
            Func<char?, char?, char?> f = e.CompileToTestMethod();
        }

        [Fact]
        public static void ConvertToChar()
        {
            Func<char> f = Expression.Lambda<Func<Char>>(Expression.Convert(Expression.Constant((byte)65), typeof(char))).CompileToTestMethod();
            Assert.Equal('A', f());

            Func<char> f2 = Expression.Lambda<Func<Char>>(Expression.Convert(Expression.Constant(65), typeof(char))).CompileToTestMethod();
            Assert.Equal('A', f2());

            Func<char> f3 = Expression.Lambda<Func<Char>>(Expression.Convert(Expression.Constant(-1), typeof(char))).CompileToTestMethod();
            char c3 = f3();
            Func<int> f4 = Expression.Lambda<Func<int>>(Expression.Convert(Expression.Constant(c3), typeof(int))).CompileToTestMethod();
            Assert.Equal(UInt16.MaxValue, f4());
        }

        [Fact]
        public static void MixedTypeNullableOps()
        {
            Expression<Func<decimal, int?, decimal?>> e = (d, i) => d + i;
            Func<decimal, int?, decimal?> f = e.CompileToTestMethod();
            decimal? result = f(1.0m, 4);
            Debug.WriteLine(result);
        }

        [Fact]
        public static void NullGuidConstant()
        {
            Expression<Func<Guid?, bool>> f2 = g2 => g2 != null;
            Func<Guid?, bool> d2 = f2.CompileToTestMethod();
            Assert.True(d2(Guid.NewGuid()));
            Assert.False(d2(null));
        }

        [Fact]
        public static void AddNullConstants()
        {
            Expression<Func<int?>> f = Expression.Lambda<Func<int?>>(
                Expression.Add(
                    Expression.Constant(null, typeof(int?)),
                    Expression.Constant(1, typeof(int?))
                    ));

            int? result = f.CompileToTestMethod()();
            Assert.False(result.HasValue);
        }

        [Fact]
        public static void CallWithRefParam()
        {
            Expression<Func<int, int>> f = x => x + MethodWithRefParam(ref x) + x;
            Func<int, int> d = f.CompileToTestMethod();
            Assert.Equal(113, d(10));
        }

        public static int MethodWithRefParam(ref int x)
        {
            x = 3;
            return 100;
        }

        [Fact]
        public static void CallWithOutParam()
        {
            Expression<Func<int, int>> f = x => x + MethodWithOutParam(out x) + x;
            Func<int, int> d = f.CompileToTestMethod();
            Assert.Equal(113, d(10));
        }

        public static int MethodWithOutParam(out int x)
        {
            x = 3;
            return 100;
        }

        [Fact]
        public static void NewArrayInvoke()
        {
            Expression<Func<int, string[]>> linq1 = (a => new string[a]);
            InvocationExpression linq1a = Expression.Invoke(linq1, new Expression[] { Expression.Constant(3) });
            Expression<Func<string[]>> linq1b = Expression.Lambda<Func<string[]>>(linq1a, new ParameterExpression[] { });
            Func<string[]> f = linq1b.CompileToTestMethod();
        }

        [Fact]
        public static void LiftedAddDateTimeTimeSpan()
        {
            Expression<Func<DateTime?, TimeSpan, DateTime?>> f = (x, y) => x + y;
            Assert.Equal(ExpressionType.Add, f.Body.NodeType);
            Debug.WriteLine(f);
            Func<DateTime?, TimeSpan, DateTime?> d = f.CompileToTestMethod();
            DateTime? dt = DateTime.Now;
            TimeSpan ts = new TimeSpan(3, 2, 1);
            DateTime? dt2 = dt + ts;
            Assert.Equal(dt2, d(dt, ts));
            Assert.Null(d(null, ts));
        }

        [Fact]
        public static void LiftedAddDateTimeTimeSpan2()
        {
            Expression<Func<DateTime?, TimeSpan?, DateTime?>> f = (x, y) => x + y;
            Assert.Equal(ExpressionType.Add, f.Body.NodeType);
            Debug.WriteLine(f);
            Func<DateTime?, TimeSpan?, DateTime?> d = f.CompileToTestMethod();
            DateTime? dt = DateTime.Now;
            TimeSpan? ts = new TimeSpan(3, 2, 1);
            DateTime? dt2 = dt + ts;
            Assert.Equal(dt2, d(dt, ts));
            Assert.Null(d(null, ts));
            Assert.Null(d(dt, null));
            Assert.Null(d(null, null));
        }

        [Fact]
        public static void LiftedSubDateTime()
        {
            Expression<Func<DateTime?, DateTime?, TimeSpan?>> f = (x, y) => x - y;
            Assert.Equal(ExpressionType.Subtract, f.Body.NodeType);
            Debug.WriteLine(f);
            Func<DateTime?, DateTime?, TimeSpan?> d = f.CompileToTestMethod();
            DateTime? dt1 = DateTime.Now;
            DateTime? dt2 = new DateTime(2006, 5, 1);
            TimeSpan? ts = dt1 - dt2;
            Assert.Equal(ts, d(dt1, dt2));
            Assert.Null(d(null, dt2));
            Assert.Null(d(dt1, null));
            Assert.Null(d(null, null));
        }

        [Fact]
        public static void LiftedEqualDateTime()
        {
            Expression<Func<DateTime?, DateTime?, bool>> f = (x, y) => x == y;
            Assert.Equal(ExpressionType.Equal, f.Body.NodeType);
            Debug.WriteLine(f);
            Func<DateTime?, DateTime?, bool> d = f.CompileToTestMethod();
            DateTime? dt1 = DateTime.Now;
            DateTime? dt2 = new DateTime(2006, 5, 1);
            Assert.True(d(dt1, dt1));
            Assert.False(d(dt1, dt2));
            Assert.False(d(null, dt2));
            Assert.False(d(dt1, null));
            Assert.True(d(null, null));
        }

        [Fact]
        public static void LiftedNotEqualDateTime()
        {
            Expression<Func<DateTime?, DateTime?, bool>> f = (x, y) => x != y;
            Assert.Equal(ExpressionType.NotEqual, f.Body.NodeType);
            Debug.WriteLine(f);
            Func<DateTime?, DateTime?, bool> d = f.CompileToTestMethod();
            DateTime? dt1 = DateTime.Now;
            DateTime? dt2 = new DateTime(2006, 5, 1);
            Assert.False(d(dt1, dt1));
            Assert.True(d(dt1, dt2));
            Assert.True(d(null, dt2));
            Assert.True(d(dt1, null));
            Assert.False(d(null, null));
        }

        [Fact]
        public static void LiftedLessThanDateTime()
        {
            Expression<Func<DateTime?, DateTime?, bool>> f = (x, y) => x < y;
            Assert.Equal(ExpressionType.LessThan, f.Body.NodeType);
            Debug.WriteLine(f);
            Func<DateTime?, DateTime?, bool> d = f.CompileToTestMethod();
            DateTime? dt1 = DateTime.Now;
            DateTime? dt2 = new DateTime(2006, 5, 1);
            Assert.False(d(dt1, dt1));
            Assert.True(d(dt2, dt1));
            Assert.False(d(null, dt2));
            Assert.False(d(dt1, null));
            Assert.False(d(null, null));
        }

        [Fact]
        public static void LessThanDateTime()
        {
            Expression<Func<DateTime, DateTime, bool>> f = (x, y) => x < y;
            Assert.Equal(ExpressionType.LessThan, f.Body.NodeType);
            Debug.WriteLine(f);
            Func<DateTime, DateTime, bool> d = f.CompileToTestMethod();
            DateTime dt1 = DateTime.Now;
            DateTime dt2 = new DateTime(2006, 5, 1);
            Assert.False(d(dt1, dt1));
            Assert.True(d(dt2, dt1));
        }

        [Fact]
        public static void InvokeLambda()
        {
            Expression<Func<int, int>> f = x => x + 1;
            InvocationExpression ie = Expression.Invoke(f, Expression.Constant(5));
            Expression<Func<int>> lambda = Expression.Lambda<Func<int>>(ie);
            Func<int> d = lambda.CompileToTestMethod();
            Assert.Equal(6, d());
        }

        [Fact]
        public static void CallCompiledLambda()
        {
            Expression<Func<int, int>> f = x => x + 1;
            Func<int, int> compiled = f.CompileToTestMethod();
            Expression<Func<Func<int, int>, int>> lambda = c => c(5);
            Func<Func<int, int>, int> d = lambda.CompileToTestMethod();
            Assert.Equal(6, d(compiled));
        }

        [Fact]
        public static void CallCompiledLambdaWithTypeMissing()
        {
            Expression<Func<object, bool>> f = x => x == Type.Missing;
            Func<object, bool> compiled = f.CompileToTestMethod();
            Expression<Func<Func<object, bool>, object, bool>> lambda = (c, x) => c(x);
            Func<Func<object, bool>, object, bool> d = lambda.CompileToTestMethod();
            Assert.Equal(true, d(compiled, Type.Missing));
        }

        [Fact]
        public static void InvokeQuotedLambda()
        {
            Expression<Func<int, int>> f = x => x + 1;
            InvocationExpression ie = Expression.Invoke(Expression.Quote(f), Expression.Constant(5));
            Expression<Func<int>> lambda = Expression.Lambda<Func<int>>(ie);
            Func<int> d = lambda.CompileToTestMethod();
            Assert.Equal(6, d());
        }

        [Fact]
        public static void InvokeComputedDelegate()
        {
            ParameterExpression x = Expression.Parameter(typeof(int), "x");
            ParameterExpression y = Expression.Parameter(typeof(int), "y");
            Expression call = Expression.Call(null, typeof(Compiler_Tests).GetMethod("ComputeDelegate", BindingFlags.Static | BindingFlags.Public), new Expression[] { y });
            InvocationExpression ie = Expression.Invoke(call, x);
            Expression<Func<int, int, int>> lambda = Expression.Lambda<Func<int, int, int>>(ie, x, y);

            Func<int, int, int> d = lambda.CompileToTestMethod();
            Assert.Equal(14, d(5, 9));
            Assert.Equal(40, d(5, 8));
        }

        public static Func<int, int> ComputeDelegate(int y)
        {
            if ((y & 1) != 0)
                return x => x + y;
            else
                return x => x * y;
        }

        public static LambdaExpression ComputeDynamicLambda()
        {
            return null;
        }

        public static Delegate ComputeDynamicDelegate()
        {
            return null;
        }

        [Fact(Skip = "can't imagine how this would work with methodbuilders")]
        public static void NestedQuotedLambdas()
        {
            Expression<Func<int, Expression<Func<int, int>>>> f = a => b => a + b;
            Func<int, Expression<Func<int, int>>> d = f.CompileToTestMethod();
            Expression<Func<int, int>> f2 = d(3);
            Func<int, int> d2 = f2.CompileToTestMethod();
            int v = d2(4);
            Assert.Equal(7, v);
        }

        [Fact]
        public static void Hmm()
        {
            Expression<Func<int, Func<int, int>>> f = a => b => a + b;
            Func<int, Func<int, int>> d = f.CompileToTestMethod();
            Func<int, int> d2 = d(3);
            int v = d2(4);
            Assert.Equal(7, v);
        }

        [Fact]
        public static void StaticMethodCall()
        {
            Expression<Func<int, int, int>> f = (a, b) => Math.Max(a, b);
            Func<int, int, int> d = f.CompileToTestMethod();
            Assert.Equal(4, d(3, 4));
        }

        [Fact]
        public static void VirtualCall()
        {
            Foo bar = new Bar();
            Expression<Func<Foo, string>> f = foo => foo.Virt();
            Func<Foo, string> d = f.CompileToTestMethod();
            Assert.Equal("Bar", d(bar));
        }

        [Fact]
        public static void NestedLambda()
        {
            Expression<Func<int, int>> f = (a) => M1(a, (b) => b * b);
            Func<int, int> d = f.CompileToTestMethod();
            Assert.Equal(100, d(10));
        }

        [Fact]
        public static void NestedLambdaWithOuterArg()
        {
            Expression<Func<int, int>> f = (a) => M1(a + a, (b) => b * a);
            Func<int, int> d = f.CompileToTestMethod();
            Assert.Equal(200, d(10));
        }

        [Fact]
        public static void NestedExpressionLambda()
        {
            Expression<Func<int, int>> f = (a) => M2(a, (b) => b * b);
            Func<int, int> d = f.CompileToTestMethod();
            Assert.Equal(10, d(10));
        }

        [Fact]
        public static void NestedExpressionLambdaWithOuterArg()
        {
            Expression<Func<int, int>> f = (a) => M2(a, (b) => b * a);
            Func<int, int> d = f.CompileToTestMethod();
            Assert.Equal(99, d(99));
        }

        [Fact]
        public static void ArrayInitializedWithLiterals()
        {
            Expression<Func<int[]>> f = () => new int[] { 1, 2, 3, 4, 5 };
            Func<int[]> d = f.CompileToTestMethod();
            int[] v = d();
            Assert.Equal(5, v.Length);
        }

        [Fact]
        public static void NullableAddition()
        {
            Expression<Func<double?, double?>> f = (v) => v + v;
            Func<double?, double?> d = f.CompileToTestMethod();
            Assert.Equal(20.0, d(10.0));
        }

        [Fact]
        public static void NullableComparedToLiteral()
        {
            Expression<Func<int?, bool>> f = (v) => v > 10;
            Func<int?, bool> d = f.CompileToTestMethod();
            Assert.True(d(12));
            Assert.False(d(5));
            Assert.True(d(int.MaxValue));
            Assert.False(d(int.MinValue));
            Assert.False(d(null));
        }

        [Fact]
        public static void NullableModuloLiteral()
        {
            Expression<Func<double?, double?>> f = (v) => v % 10;
            Func<double?, double?> d = f.CompileToTestMethod();
            Assert.Equal(5.0, d(15.0));
        }

        [Fact]
        public static void ArrayIndexer()
        {
            Expression<Func<int[], int, int>> f = (v, i) => v[i];
            Func<int[], int, int> d = f.CompileToTestMethod();
            int[] ints = new[] { 1, 2, 3 };
            Assert.Equal(3, d(ints, 2));
        }

        [Fact]
        public static void ConvertToNullableDouble()
        {
            Expression<Func<int?, double?>> f = (v) => (double?)v;
            Func<int?, double?> d = f.CompileToTestMethod();
            Assert.Equal(10.0, d(10));
        }

        [Fact]
        public static void UnboxToInt()
        {
            Expression<Func<object, int>> f = (a) => (int)a;
            Func<object, int> d = f.CompileToTestMethod();
            Assert.Equal(5, d(5));
        }

        [Fact]
        public static void TypeIs()
        {
            Expression<Func<Foo, bool>> f = x => x is Foo;
            Func<Foo, bool> d = f.CompileToTestMethod();
            Assert.True(d(new Foo()));
        }

        [Fact]
        public static void TypeAs()
        {
            Expression<Func<Foo, Bar>> f = x => x as Bar;
            Func<Foo, Bar> d = f.CompileToTestMethod();
            Assert.Null(d(new Foo()));
            Assert.NotNull(d(new Bar()));
        }

        [Fact]
        public static void Coalesce()
        {
            Expression<Func<int?, int>> f = x => x ?? 5;
            Func<int?, int> d = f.CompileToTestMethod();
            Assert.Equal(5, d(null));
            Assert.Equal(2, d(2));
        }

        [Fact]
        public static void CoalesceRefTypes()
        {
            Expression<Func<string, string>> f = x => x ?? "nil";
            Func<string, string> d = f.CompileToTestMethod();
            Assert.Equal("nil", d(null));
            Assert.Equal("Not Nil", d("Not Nil"));
        }

        [Fact]
        public static void MultiDimensionalArrayAccess()
        {
            Expression<Func<int, int, int[,], int>> f = (x, y, a) => a[x, y];
            Func<int, int, int[,], int> d = f.CompileToTestMethod();
            int[,] array = new int[2, 2] { { 0, 1 }, { 2, 3 } };
            Assert.Equal(3, d(1, 1, array));
        }

        [Fact]
        public static void NewClassWithMemberInitializer()
        {
            Expression<Func<int, ClassX>> f = v => new ClassX { A = v };
            Func<int, ClassX> d = f.CompileToTestMethod();
            Assert.Equal(5, d(5).A);
        }

        [Fact]
        public static void NewStructWithArgs()
        {
            Expression<Func<int, StructZ>> f = v => new StructZ(v);
            Func<int, StructZ> d = f.CompileToTestMethod();
            Assert.Equal(5, d(5).A);
        }

        [Fact]
        public static void NewStructWithArgsAndMemberInitializer()
        {
            Expression<Func<int, StructZ>> f = v => new StructZ(v) { A = v + 1 };
            Func<int, StructZ> d = f.CompileToTestMethod();
            Assert.Equal(6, d(5).A);
        }

        [Fact]
        public static void NewClassWithMemberInitializers()
        {
            Expression<Func<int, ClassX>> f = v => new ClassX { A = v, B = v };
            Func<int, ClassX> d = f.CompileToTestMethod();
            Assert.Equal(5, d(5).A);
            Assert.Equal(7, d(7).B);
        }

        [Fact]
        public static void NewStructWithMemberInitializer()
        {
            Expression<Func<int, StructX>> f = v => new StructX { A = v };
            Func<int, StructX> d = f.CompileToTestMethod();
            Assert.Equal(5, d(5).A);
        }

        [Fact]
        public static void NewStructWithMemberInitializers()
        {
            Expression<Func<int, StructX>> f = v => new StructX { A = v, B = v };
            Func<int, StructX> d = f.CompileToTestMethod();
            Assert.Equal(5, d(5).A);
            Assert.Equal(7, d(7).B);
        }

        [Fact]
        public static void ListInitializer()
        {
            Expression<Func<int, List<ClassY>>> f = x => new List<ClassY> { new ClassY { B = x } };
            Func<int, List<ClassY>> d = f.CompileToTestMethod();
            List<ClassY> list = d(5);
            Assert.Equal(1, list.Count);
            Assert.Equal(5, list[0].B);
        }

        [Fact]
        public static void ListInitializerLong()
        {
            Expression<Func<int, List<ClassY>>> f = x => new List<ClassY> { new ClassY { B = x }, new ClassY { B = x + 1 }, new ClassY { B = x + 2 } };
            Func<int, List<ClassY>> d = f.CompileToTestMethod();
            List<ClassY> list = d(5);
            Assert.Equal(3, list.Count);
            Assert.Equal(5, list[0].B);
            Assert.Equal(6, list[1].B);
            Assert.Equal(7, list[2].B);
        }

        [Fact]
        public static void ListInitializerInferred()
        {
            Expression<Func<int, List<ClassY>>> f = x => new List<ClassY> { new ClassY { B = x }, new ClassY { B = x + 1 }, new ClassY { B = x + 2 } };
            Func<int, List<ClassY>> d = f.CompileToTestMethod();
            List<ClassY> list = d(5);
            Assert.Equal(3, list.Count);
            Assert.Equal(5, list[0].B);
            Assert.Equal(6, list[1].B);
            Assert.Equal(7, list[2].B);
        }

        [Fact]
        public void NewClassWithMemberListInitializer()
        {
            Expression<Func<int, ClassX>> f =
                v => new ClassX { A = v, B = v + 1, Ys = { new ClassY { B = v + 2 } } };
            Func<int, ClassX> d = f.CompileToTestMethod();
            ClassX x = d(5);
            Assert.Equal(5, x.A);
            Assert.Equal(6, x.B);
            Assert.Equal(1, x.Ys.Count);
            Assert.Equal(7, x.Ys[0].B);
        }

        [Fact]
        public void NewClassWithMemberListOfStructInitializer()
        {
            Expression<Func<int, ClassX>> f =
                v => new ClassX { A = v, B = v + 1, SYs = { new StructY { B = v + 2 } } };
            Func<int, ClassX> d = f.CompileToTestMethod();
            ClassX x = d(5);
            Assert.Equal(5, x.A);
            Assert.Equal(6, x.B);
            Assert.Equal(1, x.SYs.Count);
            Assert.Equal(7, x.SYs[0].B);
        }

        [Fact]
        public static void NewClassWithMemberMemberInitializer()
        {
            Expression<Func<int, ClassX>> f =
                v => new ClassX { A = v, B = v + 1, Y = { B = v + 2 } };
            Func<int, ClassX> d = f.CompileToTestMethod();
            ClassX x = d(5);
            Assert.Equal(5, x.A);
            Assert.Equal(6, x.B);
            Assert.Equal(7, x.Y.B);
        }

        [Fact]
        public void NewStructWithMemberListInitializer()
        {
            Expression<Func<int, StructX>> f =
                v => new StructX { A = v, B = v + 1, Ys = { new ClassY { B = v + 2 } } };
            Func<int, StructX> d = f.CompileToTestMethod();
            StructX x = d(5);
            Assert.Equal(5, x.A);
            Assert.Equal(6, x.B);
            Assert.Equal(1, x.Ys.Count);
            Assert.Equal(7, x.Ys[0].B);
        }

        [Fact]
        public void NewStructWithStructMemberMemberInitializer()
        {
            Expression<Func<int, StructX>> f =
                v => new StructX { A = v, B = v + 1, SY = new StructY { B = v + 2 } };
            Func<int, StructX> d = f.CompileToTestMethod();
            StructX x = d(5);
            Assert.Equal(5, x.A);
            Assert.Equal(6, x.B);
            Assert.Equal(7, x.SY.B);
        }

        [Fact]
        public static void StructStructMemberInitializationThroughPropertyThrowsException()
        {
            Expression<Func<int, StructX>> f = GetExpressionTreeForMemberInitializationThroughProperty<StructX>();
            Assert.Throws<InvalidOperationException>(() => f.CompileToTestMethod());
        }

        [Fact]
        public static void ClassStructMemberInitializationThroughPropertyThrowsException()
        {
            Expression<Func<int, ClassX>> f = GetExpressionTreeForMemberInitializationThroughProperty<ClassX>();
            Assert.Throws<InvalidOperationException>(() => f.CompileToTestMethod());
        }

        private static Expression<Func<int, T>> GetExpressionTreeForMemberInitializationThroughProperty<T>()
        {
            // Generate the expression:
            //   v => new T { A = v, B = v + 1, SYP = { B = v + 2 } };
            ParameterExpression parameterV = Expression.Parameter(typeof(int), "v");
            return
                Expression.Lambda<Func<int, T>>(
                    // new T { A = v, B= v + 1, SYP = { B = v + 2 } }
                    Expression.MemberInit(
                        // new T
                        Expression.New(typeof(T).GetConstructor(new Type[0])),

                        // { A = v, B= v + 1, SYP = { B = v + 2 } };
                        new MemberBinding[] {
                            // A = v
                            Expression.Bind(typeof(T).GetField("A"), parameterV),

                            // B = v + 1
                            Expression.Bind(typeof(T).GetField("B"), Expression.Add(parameterV, Expression.Constant(1, typeof(int)))),

                            // SYP = { B = v + 2 }
                            Expression.MemberBind(
                                typeof(T).GetMethod("get_SYP"),
                                new MemberBinding[] {
                                    Expression.Bind(
                                        typeof(StructY).GetField("B"),
                                        Expression.Add(
                                            parameterV,
                                            Expression.Constant(2, typeof(int))
                                        )
                                    )
                                }
                            )
                        }
                    ),

                    // v =>
                    new ParameterExpression[] { parameterV }
                );
        }



        [Fact]
        public static void UnaryOperators()
        {
            // Not
            Assert.False(TestUnary<bool, bool>(ExpressionType.Not, true));
            Assert.True(TestUnary<bool, bool>(ExpressionType.Not, false));
            Assert.False((bool)TestUnary<bool?, bool?>(ExpressionType.Not, true));
            Assert.True((bool)TestUnary<bool?, bool?>(ExpressionType.Not, false));
            Assert.Null(TestUnary<bool?, bool?>(ExpressionType.Not, null));
            Assert.Equal(~1, TestUnary<int, int>(ExpressionType.Not, 1));
            Assert.Equal(~1, TestUnary<int?, int?>(ExpressionType.Not, 1));
            Assert.Null(TestUnary<int?, int?>(ExpressionType.Not, null));

            // Negate
            Assert.Equal(-1, TestUnary<int, int>(ExpressionType.Negate, 1));
            Assert.Equal(-1, TestUnary<int?, int?>(ExpressionType.Negate, 1));
            Assert.Null(TestUnary<int?, int?>(ExpressionType.Negate, null));
            Assert.Equal(-1, TestUnary<int, int>(ExpressionType.NegateChecked, 1));
            Assert.Equal(-1, TestUnary<int?, int?>(ExpressionType.NegateChecked, 1));
            Assert.Null(TestUnary<int?, int?>(ExpressionType.NegateChecked, null));

            Assert.Equal(-1, TestUnary<decimal, decimal>(ExpressionType.Negate, 1));
            Assert.Equal(-1, TestUnary<decimal?, decimal?>(ExpressionType.Negate, 1));
            Assert.Null(TestUnary<decimal?, decimal?>(ExpressionType.Negate, null));
            Assert.Equal(-1, TestUnary<decimal, decimal>(ExpressionType.NegateChecked, 1));
            Assert.Equal(-1, TestUnary<decimal?, decimal?>(ExpressionType.NegateChecked, 1));
            Assert.Null(TestUnary<decimal?, decimal?>(ExpressionType.NegateChecked, null));
        }

        private static R TestUnary<T, R>(Expression<Func<T, R>> f, T v)
        {
            Func<T, R> d = f.CompileToTestMethod();
            R rv = d(v);
            return rv;
        }

        private static R TestUnary<T, R>(ExpressionType op, T v)
        {
            ParameterExpression p = Expression.Parameter(typeof(T), "v");
            Expression<Func<T, R>> f = Expression.Lambda<Func<T, R>>(Expression.MakeUnary(op, p, null), p);
            return TestUnary(f, v);
        }

        [Fact]
        public static void ShiftULong()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                Expression<Func<ulong>> e =
                  Expression.Lambda<Func<ulong>>(
                    Expression.RightShift(
                        Expression.Constant((ulong)5, typeof(ulong)),
                        Expression.Constant((ulong)1, typeof(ulong))),
                    Enumerable.Empty<ParameterExpression>());
                Func<ulong> f = e.CompileToTestMethod();
                f();
            });
        }

        [Fact]
        public static void MultiplyMinInt()
        {
            Assert.Throws<OverflowException>(() =>
            {
                Func<long> f = Expression.Lambda<Func<long>>(
                  Expression.MultiplyChecked(
                    Expression.Constant((long)-1, typeof(long)),
                    Expression.Constant(long.MinValue, typeof(long))),
                    Enumerable.Empty<ParameterExpression>()
                    ).CompileToTestMethod();
                f();
            });
        }

        [Fact]
        public static void MultiplyMinInt2()
        {
            Assert.Throws<OverflowException>(() =>
            {
                Func<long> f = Expression.Lambda<Func<long>>(
                  Expression.MultiplyChecked(
                    Expression.Constant(long.MinValue, typeof(long)),
                    Expression.Constant((long)-1, typeof(long))),
                  Enumerable.Empty<ParameterExpression>()).CompileToTestMethod();
                f();
            });
        }

        [Fact]
        public static void ConvertSignedToUnsigned()
        {
            Func<ulong> f = Expression.Lambda<Func<ulong>>(Expression.Convert(Expression.Constant((sbyte)-1), typeof(ulong))).CompileToTestMethod();
            Assert.Equal(UInt64.MaxValue, f());
        }

        [Fact]
        public static void ConvertUnsignedToSigned()
        {
            Func<sbyte> f = Expression.Lambda<Func<sbyte>>(Expression.Convert(Expression.Constant(UInt64.MaxValue), typeof(sbyte))).CompileToTestMethod();
            Assert.Equal((sbyte)-1, f());
        }

        [Fact]
        public static void ConvertCheckedSignedToUnsigned()
        {
            Func<ulong> f = Expression.Lambda<Func<ulong>>(Expression.ConvertChecked(Expression.Constant((sbyte)-1), typeof(ulong))).CompileToTestMethod();
            Assert.Throws<OverflowException>(() => f());
        }

        [Fact]
        public static void ConvertCheckedUnsignedToSigned()
        {
            Func<sbyte> f = Expression.Lambda<Func<sbyte>>(Expression.ConvertChecked(Expression.Constant(UInt64.MaxValue), typeof(sbyte))).CompileToTestMethod();
            Assert.Throws<OverflowException>(() => f());
        }

        static class System_Linq_Expressions_Expression_TDelegate__1
        {
            public static T Default<T>() { return default(T); }
            public static void UseSystem_Linq_Expressions_Expression_TDelegate__1(bool call) // call this passing false
            {
                if (call)
                {
                    Default<System.Linq.Expressions.Expression<System.Object>>().CompileToTestMethod();
                    Default<System.Linq.Expressions.Expression<System.Object>>().Update(
                Default<System.Linq.Expressions.Expression>(),
                Default<System.Collections.Generic.IEnumerable<System.Linq.Expressions.ParameterExpression>>());
                }
            }
        }

        [Fact]
        public static void ExprT_Update()
        {
            System_Linq_Expressions_Expression_TDelegate__1.UseSystem_Linq_Expressions_Expression_TDelegate__1(false);
        }

        public enum MyEnum
        {
            Value
        }

        public class EnumOutLambdaClass
        {
            public static void Bar(out MyEnum o)
            {
                o = MyEnum.Value;
            }

            public static void BarRef(ref MyEnum o)
            {
                o = MyEnum.Value;
            }
        }

        [Fact]
        public static void UninitializedEnumOut()
        {
            ParameterExpression x = Expression.Variable(typeof(MyEnum), "x");

            Expression<Action> expression = Expression.Lambda<Action>(
                            Expression.Block(
                            new[] { x },
                            Expression.Call(null, typeof(EnumOutLambdaClass).GetMethod("Bar"), x)));

            expression.CompileToTestMethod()();
        }

        [Fact]
        public static void BinaryOperators()
        {
            // AndAlso
            Assert.True(TestBinary<bool, bool>(ExpressionType.AndAlso, true, true));
            Assert.False(TestBinary<bool, bool>(ExpressionType.AndAlso, false, true));
            Assert.False(TestBinary<bool, bool>(ExpressionType.AndAlso, true, false));
            Assert.False(TestBinary<bool, bool>(ExpressionType.AndAlso, false, false));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.AndAlso, true, true));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.AndAlso, true, false));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.AndAlso, false, true));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.AndAlso, false, false));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.AndAlso, true, null));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.AndAlso, null, true));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.AndAlso, false, null));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.AndAlso, null, false));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.AndAlso, null, null));

            // OrElse
            Assert.True(TestBinary<bool, bool>(ExpressionType.OrElse, true, true));
            Assert.True(TestBinary<bool, bool>(ExpressionType.OrElse, false, true));
            Assert.True(TestBinary<bool, bool>(ExpressionType.OrElse, true, false));
            Assert.False(TestBinary<bool, bool>(ExpressionType.OrElse, false, false));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.OrElse, true, true));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.OrElse, true, false));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.OrElse, false, true));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.OrElse, false, false));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.OrElse, true, null));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.OrElse, null, true));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.OrElse, false, null));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.OrElse, null, false));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.OrElse, null, null));

            // And
            Assert.True(TestBinary<bool, bool>(ExpressionType.And, true, true));
            Assert.False(TestBinary<bool, bool>(ExpressionType.And, false, true));
            Assert.False(TestBinary<bool, bool>(ExpressionType.And, true, false));
            Assert.False(TestBinary<bool, bool>(ExpressionType.And, false, false));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.And, true, true));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.And, true, false));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.And, false, true));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.And, false, false));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.And, true, null));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.And, null, true));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.And, false, null));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.And, null, false));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.And, null, null));
            Assert.Equal(2, TestBinary<int, int>(ExpressionType.And, 2, 3));
            Assert.Equal(2, TestBinary<int?, int?>(ExpressionType.And, 2, 3));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.And, null, 3));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.And, 2, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.And, null, null));

            // Or
            Assert.True(TestBinary<bool, bool>(ExpressionType.Or, true, true));
            Assert.True(TestBinary<bool, bool>(ExpressionType.Or, false, true));
            Assert.True(TestBinary<bool, bool>(ExpressionType.Or, true, false));
            Assert.False(TestBinary<bool, bool>(ExpressionType.Or, false, false));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.Or, true, true));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.Or, true, false));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.Or, false, true));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.Or, false, false));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.Or, true, null));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.Or, null, true));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.Or, false, null));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.Or, null, false));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.Or, null, null));
            Assert.Equal(3, TestBinary<int, int>(ExpressionType.Or, 2, 1));
            Assert.Equal(3, TestBinary<int?, int?>(ExpressionType.Or, 2, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Or, null, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Or, 2, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Or, null, 1));

            // ExclusiveOr
            Assert.False(TestBinary<bool, bool>(ExpressionType.ExclusiveOr, true, true));
            Assert.True(TestBinary<bool, bool>(ExpressionType.ExclusiveOr, true, false));
            Assert.True(TestBinary<bool, bool>(ExpressionType.ExclusiveOr, false, true));
            Assert.False(TestBinary<bool, bool>(ExpressionType.ExclusiveOr, false, false));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.ExclusiveOr, true, true));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.ExclusiveOr, true, false));
            Assert.True((bool)TestBinary<bool?, bool?>(ExpressionType.ExclusiveOr, false, true));
            Assert.False((bool)TestBinary<bool?, bool?>(ExpressionType.ExclusiveOr, false, false));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.ExclusiveOr, true, null));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.ExclusiveOr, null, true));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.ExclusiveOr, false, null));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.ExclusiveOr, null, false));
            Assert.Null(TestBinary<bool?, bool?>(ExpressionType.ExclusiveOr, null, null));
            Assert.Equal(4, TestBinary<int, int>(ExpressionType.ExclusiveOr, 5, 1));
            Assert.Equal(4, TestBinary<int?, int?>(ExpressionType.ExclusiveOr, 5, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.ExclusiveOr, null, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.ExclusiveOr, 5, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.ExclusiveOr, null, null));

            // Equal
            Assert.False(TestBinary<int, bool>(ExpressionType.Equal, 1, 2));
            Assert.True(TestBinary<int, bool>(ExpressionType.Equal, 1, 1));
            Assert.False(TestBinary<int?, bool>(ExpressionType.Equal, 1, 2));
            Assert.True(TestBinary<int?, bool>(ExpressionType.Equal, 1, 1));
            Assert.False(TestBinary<int?, bool>(ExpressionType.Equal, null, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.Equal, 1, null));
            Assert.True(TestBinary<int?, bool>(ExpressionType.Equal, null, null));

            Assert.False(TestBinary<decimal, bool>(ExpressionType.Equal, 1, 2));
            Assert.True(TestBinary<decimal, bool>(ExpressionType.Equal, 1, 1));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.Equal, 1, 2));
            Assert.True(TestBinary<decimal?, bool>(ExpressionType.Equal, 1, 1));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.Equal, null, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.Equal, 1, null));
            Assert.True(TestBinary<decimal?, bool>(ExpressionType.Equal, null, null));

            // NotEqual
            Assert.True(TestBinary<int, bool>(ExpressionType.NotEqual, 1, 2));
            Assert.False(TestBinary<int, bool>(ExpressionType.NotEqual, 1, 1));
            Assert.True(TestBinary<int?, bool>(ExpressionType.NotEqual, 1, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.NotEqual, 1, 1));
            Assert.True(TestBinary<int?, bool>(ExpressionType.NotEqual, null, 2));
            Assert.True(TestBinary<int?, bool>(ExpressionType.NotEqual, 1, null));
            Assert.False(TestBinary<int?, bool>(ExpressionType.NotEqual, null, null));

            Assert.True(TestBinary<decimal, bool>(ExpressionType.NotEqual, 1, 2));
            Assert.False(TestBinary<decimal, bool>(ExpressionType.NotEqual, 1, 1));
            Assert.True(TestBinary<decimal?, bool>(ExpressionType.NotEqual, 1, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.NotEqual, 1, 1));
            Assert.True(TestBinary<decimal?, bool>(ExpressionType.NotEqual, null, 2));
            Assert.True(TestBinary<decimal?, bool>(ExpressionType.NotEqual, 1, null));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.NotEqual, null, null));

            // LessThan
            Assert.True(TestBinary<int, bool>(ExpressionType.LessThan, 1, 2));
            Assert.False(TestBinary<int, bool>(ExpressionType.LessThan, 2, 1));
            Assert.False(TestBinary<int, bool>(ExpressionType.LessThan, 2, 2));
            Assert.True(TestBinary<int?, bool>(ExpressionType.LessThan, 1, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.LessThan, 2, 1));
            Assert.False(TestBinary<int?, bool>(ExpressionType.LessThan, 2, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.LessThan, null, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.LessThan, 2, null));
            Assert.False(TestBinary<int?, bool>(ExpressionType.LessThan, null, null));

            Assert.True(TestBinary<decimal, bool>(ExpressionType.LessThan, 1, 2));
            Assert.False(TestBinary<decimal, bool>(ExpressionType.LessThan, 2, 1));
            Assert.False(TestBinary<decimal, bool>(ExpressionType.LessThan, 2, 2));
            Assert.True(TestBinary<decimal?, bool>(ExpressionType.LessThan, 1, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.LessThan, 2, 1));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.LessThan, 2, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.LessThan, null, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.LessThan, 2, null));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.LessThan, null, null));

            // LessThanOrEqual
            Assert.True(TestBinary<int, bool>(ExpressionType.LessThanOrEqual, 1, 2));
            Assert.False(TestBinary<int, bool>(ExpressionType.LessThanOrEqual, 2, 1));
            Assert.True(TestBinary<int, bool>(ExpressionType.LessThanOrEqual, 2, 2));
            Assert.True(TestBinary<int?, bool>(ExpressionType.LessThanOrEqual, 1, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.LessThanOrEqual, 2, 1));
            Assert.True(TestBinary<int?, bool>(ExpressionType.LessThanOrEqual, 2, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.LessThanOrEqual, null, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.LessThanOrEqual, 2, null));
            Assert.False(TestBinary<int?, bool>(ExpressionType.LessThanOrEqual, null, null));

            Assert.True(TestBinary<decimal, bool>(ExpressionType.LessThanOrEqual, 1, 2));
            Assert.False(TestBinary<decimal, bool>(ExpressionType.LessThanOrEqual, 2, 1));
            Assert.True(TestBinary<decimal, bool>(ExpressionType.LessThanOrEqual, 2, 2));
            Assert.True(TestBinary<decimal?, bool>(ExpressionType.LessThanOrEqual, 1, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.LessThanOrEqual, 2, 1));
            Assert.True(TestBinary<decimal?, bool>(ExpressionType.LessThanOrEqual, 2, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.LessThanOrEqual, null, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.LessThanOrEqual, 2, null));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.LessThanOrEqual, null, null));

            // GreaterThan
            Assert.False(TestBinary<int, bool>(ExpressionType.GreaterThan, 1, 2));
            Assert.True(TestBinary<int, bool>(ExpressionType.GreaterThan, 2, 1));
            Assert.False(TestBinary<int, bool>(ExpressionType.GreaterThan, 2, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.GreaterThan, 1, 2));
            Assert.True(TestBinary<int?, bool>(ExpressionType.GreaterThan, 2, 1));
            Assert.False(TestBinary<int?, bool>(ExpressionType.GreaterThan, 2, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.GreaterThan, null, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.GreaterThan, 2, null));
            Assert.False(TestBinary<int?, bool>(ExpressionType.GreaterThan, null, null));

            Assert.False(TestBinary<decimal, bool>(ExpressionType.GreaterThan, 1, 2));
            Assert.True(TestBinary<decimal, bool>(ExpressionType.GreaterThan, 2, 1));
            Assert.False(TestBinary<decimal, bool>(ExpressionType.GreaterThan, 2, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.GreaterThan, 1, 2));
            Assert.True(TestBinary<decimal?, bool>(ExpressionType.GreaterThan, 2, 1));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.GreaterThan, 2, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.GreaterThan, null, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.GreaterThan, 2, null));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.GreaterThan, null, null));

            // GreaterThanOrEqual
            Assert.False(TestBinary<int, bool>(ExpressionType.GreaterThanOrEqual, 1, 2));
            Assert.True(TestBinary<int, bool>(ExpressionType.GreaterThanOrEqual, 2, 1));
            Assert.True(TestBinary<int, bool>(ExpressionType.GreaterThanOrEqual, 2, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.GreaterThanOrEqual, 1, 2));
            Assert.True(TestBinary<int?, bool>(ExpressionType.GreaterThanOrEqual, 2, 1));
            Assert.True(TestBinary<int?, bool>(ExpressionType.GreaterThanOrEqual, 2, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.GreaterThanOrEqual, null, 2));
            Assert.False(TestBinary<int?, bool>(ExpressionType.GreaterThanOrEqual, 2, null));
            Assert.False(TestBinary<int?, bool>(ExpressionType.GreaterThanOrEqual, null, null));

            Assert.False(TestBinary<decimal, bool>(ExpressionType.GreaterThanOrEqual, 1, 2));
            Assert.True(TestBinary<decimal, bool>(ExpressionType.GreaterThanOrEqual, 2, 1));
            Assert.True(TestBinary<decimal, bool>(ExpressionType.GreaterThanOrEqual, 2, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.GreaterThanOrEqual, 1, 2));
            Assert.True(TestBinary<decimal?, bool>(ExpressionType.GreaterThanOrEqual, 2, 1));
            Assert.True(TestBinary<decimal?, bool>(ExpressionType.GreaterThanOrEqual, 2, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.GreaterThanOrEqual, null, 2));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.GreaterThanOrEqual, 2, null));
            Assert.False(TestBinary<decimal?, bool>(ExpressionType.GreaterThanOrEqual, null, null));

            // Add
            Assert.Equal(3, TestBinary<int, int>(ExpressionType.Add, 1, 2));
            Assert.Equal(3, TestBinary<int?, int?>(ExpressionType.Add, 1, 2));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Add, null, 2));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Add, 1, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Add, null, null));

            Assert.Equal(3, TestBinary<decimal, decimal>(ExpressionType.Add, 1, 2));
            Assert.Equal(3, TestBinary<decimal?, decimal?>(ExpressionType.Add, 1, 2));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Add, null, 2));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Add, 1, null));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Add, null, null));

            // AddChecked
            Assert.Equal(3, TestBinary<int, int>(ExpressionType.AddChecked, 1, 2));
            Assert.Equal(3, TestBinary<int?, int?>(ExpressionType.AddChecked, 1, 2));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.AddChecked, null, 2));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.AddChecked, 1, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.AddChecked, null, null));

            // Subtract
            Assert.Equal(1, TestBinary<int, int>(ExpressionType.Subtract, 2, 1));
            Assert.Equal(1, TestBinary<int?, int?>(ExpressionType.Subtract, 2, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Subtract, null, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Subtract, 2, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Subtract, null, null));

            Assert.Equal(1, TestBinary<decimal, decimal>(ExpressionType.Subtract, 2, 1));
            Assert.Equal(1, TestBinary<decimal?, decimal?>(ExpressionType.Subtract, 2, 1));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Subtract, null, 1));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Subtract, 2, null));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Subtract, null, null));

            // SubtractChecked
            Assert.Equal(1, TestBinary<int, int>(ExpressionType.SubtractChecked, 2, 1));
            Assert.Equal(1, TestBinary<int?, int?>(ExpressionType.SubtractChecked, 2, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.SubtractChecked, null, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.SubtractChecked, 2, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.SubtractChecked, null, null));

            // Multiply
            Assert.Equal(2, TestBinary<int, int>(ExpressionType.Multiply, 2, 1));
            Assert.Equal(2, TestBinary<int?, int?>(ExpressionType.Multiply, 2, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Multiply, null, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Multiply, 2, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Multiply, null, null));

            Assert.Equal(2, TestBinary<decimal, decimal>(ExpressionType.Multiply, 2, 1));
            Assert.Equal(2, TestBinary<decimal?, decimal?>(ExpressionType.Multiply, 2, 1));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Multiply, null, 1));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Multiply, 2, null));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Multiply, null, null));

            // MultiplyChecked
            Assert.Equal(2, TestBinary<int, int>(ExpressionType.MultiplyChecked, 2, 1));
            Assert.Equal(2, TestBinary<int?, int?>(ExpressionType.MultiplyChecked, 2, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.MultiplyChecked, null, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.MultiplyChecked, 2, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.MultiplyChecked, null, null));

            // Divide
            Assert.Equal(2, TestBinary<int, int>(ExpressionType.Divide, 5, 2));
            Assert.Equal(2, TestBinary<int?, int?>(ExpressionType.Divide, 5, 2));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Divide, null, 2));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Divide, 5, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Divide, null, null));

            Assert.Equal(2.5m, TestBinary<decimal, decimal>(ExpressionType.Divide, 5, 2));
            Assert.Equal(2.5m, TestBinary<decimal?, decimal?>(ExpressionType.Divide, 5, 2));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Divide, null, 2));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Divide, 5, null));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Divide, null, null));

            // Modulo
            Assert.Equal(3, TestBinary<int, int>(ExpressionType.Modulo, 7, 4));
            Assert.Equal(3, TestBinary<int?, int?>(ExpressionType.Modulo, 7, 4));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Modulo, null, 4));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Modulo, 7, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.Modulo, null, null));

            Assert.Equal(3, TestBinary<decimal, decimal>(ExpressionType.Modulo, 7, 4));
            Assert.Equal(3, TestBinary<decimal?, decimal?>(ExpressionType.Modulo, 7, 4));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Modulo, null, 4));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Modulo, 7, null));
            Assert.Null(TestBinary<decimal?, decimal?>(ExpressionType.Modulo, null, null));

            // Power
            Assert.Equal(16, TestBinary<double, double>(ExpressionType.Power, 2, 4));
            Assert.Equal(16, TestBinary<double?, double?>(ExpressionType.Power, 2, 4));
            Assert.Null(TestBinary<double?, double?>(ExpressionType.Power, null, 4));
            Assert.Null(TestBinary<double?, double?>(ExpressionType.Power, 2, null));
            Assert.Null(TestBinary<double?, double?>(ExpressionType.Power, null, null));

            // LeftShift
            Assert.Equal(10, TestBinary<int, int>(ExpressionType.LeftShift, 5, 1));
            Assert.Equal(10, TestBinary<int?, int?>(ExpressionType.LeftShift, 5, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.LeftShift, null, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.LeftShift, 5, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.LeftShift, null, null));

            // RightShift
            Assert.Equal(2, TestBinary<int, int>(ExpressionType.RightShift, 4, 1));
            Assert.Equal(2, TestBinary<int?, int?>(ExpressionType.RightShift, 4, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.RightShift, null, 1));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.RightShift, 4, null));
            Assert.Null(TestBinary<int?, int?>(ExpressionType.RightShift, null, null));
        }

        private static R TestBinary<T, R>(Expression<Func<T, T, R>> f, T v1, T v2)
        {
            Func<T, T, R> d = f.CompileToTestMethod();
            R rv = d(v1, v2);
            return rv;
        }

        private static R TestBinary<T, R>(ExpressionType op, T v1, T v2)
        {
            ParameterExpression p1 = Expression.Parameter(typeof(T), "v1");
            ParameterExpression p2 = Expression.Parameter(typeof(T), "v2");
            Expression<Func<T, T, R>> f = Expression.Lambda<Func<T, T, R>>(Expression.MakeBinary(op, p1, p2), p1, p2);
            return TestBinary(f, v1, v2);
        }

        [Fact]
        public static void TestConvertToNullable()
        {
            // Using an unchecked cast to ensure that a Convert expression is used (and not ConvertChecked)
            Expression<Func<int, int?>> f = x => unchecked((int?)x);
            Assert.Equal(f.Body.NodeType, ExpressionType.Convert);
            Func<int, int?> d = f.CompileToTestMethod();
            Assert.Equal(2, d(2));
        }

        [Fact]
        public static void TestNullableMethods()
        {
            TestNullableCall(new ArraySegment<int>(), (v) => v.HasValue, (v) => v.HasValue);
            TestNullableCall(5.1, (v) => v.GetHashCode(), (v) => v.GetHashCode());
            TestNullableCall(5L, (v) => v.ToString(), (v) => v.ToString());
            TestNullableCall(5, (v) => v.GetValueOrDefault(7), (v) => v.GetValueOrDefault(7));
            TestNullableCall(42, (v) => v.Equals(42), (v) => v.Equals(42));
            TestNullableCall(42, (v) => v.Equals(0), (v) => v.Equals(0));
            TestNullableCall(5, (v) => v.GetValueOrDefault(), (v) => v.GetValueOrDefault());

            Expression<Func<int?, int>> f = x => x.Value;
            Func<int?, int> d = f.CompileToTestMethod();
            Assert.Equal(2, d(2));
            Assert.Throws<InvalidOperationException>(() => d(null));
        }

        private static void TestNullableCall<T, U>(T arg, Func<T?, U> f, Expression<Func<T?, U>> e)
            where T : struct
        {
            Func<T?, U> d = e.CompileToTestMethod();
            Assert.Equal(f(arg), d(arg));
            Assert.Equal(f(null), d(null));
        }

        public static int BadJuju(int v)
        {
            throw new Exception("Bad Juju");
        }

        public static int M1(int v, Func<int, int> f)
        {
            return f(v);
        }

        public static int M2(int v, Expression<Func<int, int>> f)
        {
            return v;
        }

        public class Foo
        {
            public Foo()
            {
            }
            public int Zip(int y)
            {
                return y * y;
            }
            public virtual string Virt()
            {
                return "Foo";
            }
        }

        public class Bar : Foo
        {
            public Bar()
            {
            }
            public override string Virt()
            {
                return "Bar";
            }
        }

        public class ClassX
        {
            public int A;
            public int B;
            public int C;
            public ClassY Y = new ClassY();
            public ClassY YP
            {
                get { return this.Y; }
            }
            public List<ClassY> Ys = new List<ClassY>();
            public List<StructY> SYs = new List<StructY>();
            public StructY SY;
            public StructY SYP
            {
                get { return this.SY; }
            }
        }

        public class ClassY
        {
            public int B;
            public int PB
            {
                get { return this.B; }
                set { this.B = value; }
            }
        }

        public class StructX
        {
            public int A;
            public int B;
            public int C;
            public ClassY Y = new ClassY();
            public ClassY YP
            {
                get { return this.Y; }
            }
            public List<ClassY> Ys = new List<ClassY>();
            public StructY SY;
            public StructY SYP
            {
                get { return this.SY; }
            }
        }

        public struct StructY
        {
            public int B;
            public int PB
            {
                get { return this.B; }
                set { this.B = value; }
            }
        }

        public struct StructZ
        {
            public int A;
            public StructZ(int a)
            {
                this.A = a;
            }
        }

        [Fact]
        public static void PropertyAccess()
        {
            NWindProxy.Customer cust = new NWindProxy.Customer { CustomerID = "BUBBA", ContactName = "Bubba Gump" };
            ParameterExpression c = Expression.Parameter(typeof(NWindProxy.Customer), "c");
            ParameterExpression c2 = Expression.Parameter(typeof(NWindProxy.Customer), "c2");

            Assert.Equal(cust, Expression.Lambda(c, c).CompileToTestMethod().DynamicInvoke(cust));
            Assert.Equal(cust.ContactName, Expression.Lambda(Expression.PropertyOrField(c, "ContactName"), c).CompileToTestMethod().DynamicInvoke(cust));
            Assert.Equal(cust.Orders, Expression.Lambda(Expression.PropertyOrField(c, "Orders"), c).CompileToTestMethod().DynamicInvoke(cust));
            Assert.Equal(cust.CustomerID, Expression.Lambda(Expression.PropertyOrField(c, "CustomerId"), c).CompileToTestMethod().DynamicInvoke(cust));
            Assert.True((bool)Expression.Lambda(Expression.Equal(Expression.PropertyOrField(c, "CustomerId"), Expression.PropertyOrField(c, "CUSTOMERID")), c).CompileToTestMethod().DynamicInvoke(cust));
            Assert.True((bool)
                Expression.Lambda(
                    Expression.And(
                        Expression.Equal(Expression.PropertyOrField(c, "CustomerId"), Expression.PropertyOrField(c2, "CustomerId")),
                        Expression.Equal(Expression.PropertyOrField(c, "ContactName"), Expression.PropertyOrField(c2, "ContactName"))
                        ),
                    c, c2)
                .CompileToTestMethod().DynamicInvoke(cust, cust));
        }

        private static void ArimeticOperatorTests(Type type, object value, bool testUnSigned)
        {
            ParameterExpression p = Expression.Parameter(type, "x");
            if (testUnSigned)
                Expression.Lambda(Expression.Negate(p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.Add(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.Subtract(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.Multiply(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.Divide(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
        }

        private static void RelationalOperatorTests(Type type, object value, bool testModulo)
        {
            ParameterExpression p = Expression.Parameter(type, "x");
            if (testModulo)
                Expression.Lambda(Expression.Modulo(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.Equal(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.NotEqual(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.LessThan(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.LessThanOrEqual(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.GreaterThan(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.GreaterThanOrEqual(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
        }

        private static void NumericOperatorTests(Type type, object value, bool testModulo, bool testUnSigned)
        {
            ArimeticOperatorTests(type, value, testUnSigned);
            RelationalOperatorTests(type, value, testModulo);
        }

        private static void NumericOperatorTests(Type type, object value)
        {
            NumericOperatorTests(type, value, true, true);
        }

        private static void IntegerOperatorTests(Type type, object value, bool testModulo, bool testUnsigned)
        {
            NumericOperatorTests(type, value, testModulo, testUnsigned);
            LogicalOperatorTests(type, value);
        }

        private static void LogicalOperatorTests(Type type, object value)
        {
            ParameterExpression p = Expression.Parameter(type, "x");
            Expression.Lambda(Expression.Not(p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.Or(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.And(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
            Expression.Lambda(Expression.ExclusiveOr(p, p), p).CompileToTestMethod().DynamicInvoke(new object[] { value });
        }

        private static void IntegerOperatorTests(Type type, object value)
        {
            IntegerOperatorTests(type, value, true, true);
        }

        [Fact]
        public static void NumericOperators()
        {
            RelationalOperatorTests(typeof(sbyte), (sbyte)1, false);
            LogicalOperatorTests(typeof(sbyte), (sbyte)1);
            RelationalOperatorTests(typeof(short), (short)1, false);
            LogicalOperatorTests(typeof(sbyte), (sbyte)1);
            IntegerOperatorTests(typeof(int), 1);
            IntegerOperatorTests(typeof(long), (long)1);
            RelationalOperatorTests(typeof(byte), (byte)1, false);
            LogicalOperatorTests(typeof(byte), (byte)1);
            RelationalOperatorTests(typeof(ushort), (ushort)1, false);
            LogicalOperatorTests(typeof(ushort), (ushort)1);
            IntegerOperatorTests(typeof(uint), (uint)1, true, false);
            IntegerOperatorTests(typeof(ulong), (ulong)1, true, false);
            NumericOperatorTests(typeof(float), (float)1);
            NumericOperatorTests(typeof(double), (double)1);
            NumericOperatorTests(typeof(decimal), (decimal)1);

            RelationalOperatorTests(typeof(sbyte?), (sbyte?)1, false);
            LogicalOperatorTests(typeof(sbyte?), (sbyte?)1);
            RelationalOperatorTests(typeof(short?), (short?)1, false);
            LogicalOperatorTests(typeof(short?), (short?)1);
            IntegerOperatorTests(typeof(int?), (int?)1);
            IntegerOperatorTests(typeof(long?), (long?)1);
            RelationalOperatorTests(typeof(byte?), (byte?)1, false);
            LogicalOperatorTests(typeof(byte?), (byte?)1);
            RelationalOperatorTests(typeof(ushort?), (ushort?)1, false);
            LogicalOperatorTests(typeof(ushort?), (ushort?)1);
            IntegerOperatorTests(typeof(uint?), (uint?)1, true, false);
            IntegerOperatorTests(typeof(ulong?), (ulong?)1, true, false);

            NumericOperatorTests(typeof(float?), (float?)1);
            NumericOperatorTests(typeof(double?), (double?)1);
            NumericOperatorTests(typeof(decimal?), (decimal?)1);
        }

        private static void TrueBooleanOperatorTests(Type type, object arg1, object arg2)
        {
            ParameterExpression x = Expression.Parameter(type, "x");
            ParameterExpression y = Expression.Parameter(type, "y");
            Expression.Lambda(Expression.AndAlso(x, y), x, y).CompileToTestMethod().DynamicInvoke(new object[] { arg1, arg2 });
            Expression.Lambda(Expression.OrElse(x, y), x, y).CompileToTestMethod().DynamicInvoke(new object[] { arg1, arg2 });
            GeneralBooleanOperatorTests(type, arg1, arg2);
        }

        private static void GeneralBooleanOperatorTests(Type type, object arg1, object arg2)
        {
            ParameterExpression x = Expression.Parameter(type, "x");
            ParameterExpression y = Expression.Parameter(type, "y");
            Expression.Lambda(Expression.And(x, y), x, y).CompileToTestMethod().DynamicInvoke(new object[] { arg1, arg2 });
            Expression.Lambda(Expression.Or(x, y), x, y).CompileToTestMethod().DynamicInvoke(new object[] { arg1, arg2 });
            Expression.Lambda(Expression.Not(x), x).CompileToTestMethod().DynamicInvoke(new object[] { arg1 });
            Expression.Lambda(Expression.Equal(x, y), x, y).CompileToTestMethod().DynamicInvoke(new object[] { arg1, arg2 });
            Expression.Lambda(Expression.NotEqual(x, y), x, y).CompileToTestMethod().DynamicInvoke(new object[] { arg1, arg2 });
        }

        [Fact]
        public static void BooleanOperators()
        {
            TrueBooleanOperatorTests(typeof(bool), true, false);
            TrueBooleanOperatorTests(typeof(bool?), true, false);
        }
    }

    public struct U
    {
        public static U operator +(U x, U y) { throw new NotImplementedException(); }
        public static U operator -(U x, U y) { throw new NotImplementedException(); }
        public static U operator *(U x, U y) { throw new NotImplementedException(); }
        public static U operator /(U x, U y) { throw new NotImplementedException(); }
        public static U operator <(U x, U y) { throw new NotImplementedException(); }
        public static U operator <=(U x, U y) { throw new NotImplementedException(); }
        public static U operator >(U x, U y) { throw new NotImplementedException(); }
        public static U operator >=(U x, U y) { throw new NotImplementedException(); }
        public static U operator ==(U x, U y) { throw new NotImplementedException(); }
        public static U operator !=(U x, U y) { throw new NotImplementedException(); }
        public static U operator &(U x, U y) { throw new NotImplementedException(); }
        public static U operator |(U x, U y) { throw new NotImplementedException(); }
        public static U operator ^(U x, U y) { throw new NotImplementedException(); }
        public static U operator -(U x) { throw new NotImplementedException(); }
        public static U operator ~(U x) { throw new NotImplementedException(); }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public struct B
    {
        public static B operator <(B x, B y) { throw new NotImplementedException(); }
        public static B operator <=(B x, B y) { throw new NotImplementedException(); }
        public static B operator >(B x, B y) { throw new NotImplementedException(); }
        public static B operator >=(B x, B y) { throw new NotImplementedException(); }
        public static B operator ==(B x, B y) { throw new NotImplementedException(); }
        public static B operator !=(B x, B y) { throw new NotImplementedException(); }
        public static B operator &(B x, B y) { throw new NotImplementedException(); }
        public static B operator |(B x, B y) { throw new NotImplementedException(); }
        public static B operator ^(B x, B y) { throw new NotImplementedException(); }
        public static B operator !(B x) { throw new NotImplementedException(); }
        public static B operator -(B x) { throw new NotImplementedException(); }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public struct M
    {
        public static M operator +(M m, N n) { throw new NotImplementedException(); }
        public static M operator -(M m, N n) { throw new NotImplementedException(); }
        public static M operator -(M m) { throw new NotImplementedException(); }
        public static M operator ~(M m) { throw new NotImplementedException(); }
        public static explicit operator M(N n) { throw new NotImplementedException(); }
        public static N Foo(M m) { throw new NotImplementedException(); }
    }

    public struct N
    {
        public static M operator *(M m, N n) { throw new NotImplementedException(); }
        public static M operator /(M m, N n) { throw new NotImplementedException(); }
        public static N operator -(N n) { throw new NotImplementedException(); }
        public static implicit operator N(M m) { throw new NotImplementedException(); }
        public static M Bar(N n) { throw new NotImplementedException(); }
    }

    public class DerivedClass : BaseClass
    {
    }

    internal class AssertionException : Exception
    {
        public AssertionException(string msg)
            : base(msg)
        {
        }
    }
}
