// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class IndexExpressionTests
    {
        [Fact]
        public void CallWithLambdaIndex()
        {
            // An exception to the rule against unassignable indices, lamdba expressions
            // can be automatically quoted.
            PropertyInfo prop = typeof(Dictionary<Expression<Func<int>>, int>).GetProperty("Item");
            Expression<Func<int>> index = () => 2;
            ConstantExpression dict = Expression.Constant(new Dictionary<Expression<Func<int>>, int>{{index, 9}});
            Func<int> f = Expression.Lambda<Func<int>>(Expression.Property(dict, prop, Expression.Constant(index))).CompileToTestMethod();
            Assert.Equal(9, f());
        }

        [Fact]
        public void CallWithIntAndLambdaIndex()
        {
            PropertyInfo prop = typeof(IntAndExpressionIndexed).GetProperty("Item");
            ConstantExpression instance = Expression.Constant(new IntAndExpressionIndexed());
            Expression<Action> index = Expression.Lambda<Action>(Expression.Empty());
            ConstantExpression intIdx = Expression.Constant(0);
            Func<bool> f = Expression.Lambda<Func<bool>>(Expression.Property(instance, prop, intIdx, Expression.Constant(index))).CompileToTestMethod();
            Assert.True(f());
        }

        [Fact]
        public void OverloadedIndexer()
        {
            ConstantExpression instance = Expression.Constant(new OverloadedIndexers());
            ConstantExpression index = Expression.Constant("");
            Expression<Func<int>> exp = Expression.Lambda<Func<int>>(Expression.Property(instance, "Item", index));
            Func<int> f = exp.CompileToTestMethod();
            Assert.Equal(2, f());
        }

        [Fact]
        public void NonIndexedPropertyExplicitlyNoIndices()
        {
            ConstantExpression instance = Expression.Constant("123");
            IndexExpression prop = Expression.Property(instance, "Length", null);
            Expression<Func<int>> exp = Expression.Lambda<Func<int>>(prop);
            Func<int> func = exp.CompileToTestMethod();
            Assert.Equal(3, func());
        }


        [Fact]
        public static void ConstrainedVirtualCall()
        {
            // Virtual call via base declaration to valuetype.
            ConstantExpression instance = Expression.Constant(new InterfaceIndexableValueType());
            PropertyInfo prop = typeof(IIndexable).GetProperty("Item");
            IndexExpression index = Expression.Property(instance, prop, Expression.Constant(4));
            Expression<Func<int>> lambda = Expression.Lambda<Func<int>>(
                index
            );
            Func<int> func = lambda.CompileToTestMethod();
            Assert.Equal(8, func());
        }


        public interface IIndexable
        {
            int this[int index] { get; }
        }

        public struct InterfaceIndexableValueType : IIndexable
        {
            public int this[int index] => index * 2;
        }

        public class IntAndExpressionIndexed
        {
            public bool this[int x, Expression<Action> y] => true;
        }

        public class OverloadedIndexers
        {
            public int this[int index] => 0;

            public int this[int x, int y] => 1;

            public int this[string index] => 2;
        }

        public class OverloadedIndexersBothMatchString
        {
            public int this[IComparable index] => 0;
        }

        public class Vector1
        {
            public int this[int x]
            {
                get { return 0; }
            }
        }

        public class Vector2
        {
            public int this[int x, int y]
            {
                get { return 0; }
            }
        }
    }
}
