// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class ParameterBlockTests : SharedBlockTests
    {
        private static IEnumerable<ParameterExpression> SingleParameter
        {
            get { return Enumerable.Repeat(Expression.Variable(typeof(int)), 1); }
        }

        [Theory]
        [MemberData(nameof(ConstantValueData))]
        public void SingleElementBlock(object value)
        {
            Type type = value.GetType();
            ConstantExpression constant = Expression.Constant(value, type);
            BlockExpression block = Expression.Block(
                SingleParameter,
                constant
               );

            Assert.Equal(type, block.Type);

            Expression equal = Expression.Equal(constant, block);
            Assert.True(Expression.Lambda<Func<bool>>(equal).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ConstantValueData))]
        public void DoubleElementBlock(object value)
        {
            Type type = value.GetType();
            ConstantExpression constant = Expression.Constant(value, type);
            BlockExpression block = Expression.Block(
                SingleParameter,
                Expression.Empty(),
                constant
               );

            Assert.Equal(type, block.Type);

            Expression equal = Expression.Equal(constant, block);
            Assert.True(Expression.Lambda<Func<bool>>(equal).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ObjectAssignableConstantValuesAndSizes))]
        public void BlockExplicitType(object value, int blockSize)
        {
            ConstantExpression constant = Expression.Constant(value, value.GetType());
            BlockExpression block = Expression.Block(typeof(object), SingleParameter, PadBlock(blockSize - 1, constant));

            Assert.Equal(typeof(object), block.Type);

            Expression equal = Expression.Equal(constant, block);
            Assert.True(Expression.Lambda<Func<bool>>(equal).CompileToTestMethod()());
        }

        [Theory]
        [MemberData(nameof(ConstantValuesAndSizes))]
        public void BlockFromEmptyParametersSameAsFromParams(object value, int blockSize)
        {
            ConstantExpression constant = Expression.Constant(value, value.GetType());
            IEnumerable<Expression> expressions = PadBlock(blockSize - 1, constant);

            BlockExpression fromParamsBlock = Expression.Block(SingleParameter, expressions.ToArray());
            BlockExpression fromEnumBlock = Expression.Block(SingleParameter, expressions);

            Assert.Equal(fromParamsBlock.GetType(), fromEnumBlock.GetType());

            Assert.True(Expression.Lambda<Func<bool>>(Expression.Equal(constant, fromParamsBlock)).CompileToTestMethod()());
            Assert.True(Expression.Lambda<Func<bool>>(Expression.Equal(constant, fromEnumBlock)).CompileToTestMethod()());
        }

    }
}
