// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class BlockTests
    {
        #region Test methods

        [Fact]
        public static void CheckBlockClosureVariableInitializationTest()
        {
            foreach (var kv in BlockClosureVariableInitialization())
            {
                VerifyBlockClosureVariableInitialization(kv.Key, kv.Value);
            }
        }

        private static IEnumerable<KeyValuePair<Expression, object>> BlockClosureVariableInitialization()
        {
            {
                ParameterExpression p = Expression.Parameter(typeof(int));
                ParameterExpression q = Expression.Parameter(typeof(Func<int>));
                Expression<Func<int>> l = Expression.Lambda<Func<int>>(p);
                yield return new KeyValuePair<Expression, object>(Expression.Block(new[] { p, q }, Expression.Assign(q, l), p), default(int));
            }

            {
                ParameterExpression p = Expression.Parameter(typeof(int));
                ParameterExpression q = Expression.Parameter(typeof(Action<int>));
                ParameterExpression x = Expression.Parameter(typeof(int));
                Expression<Action<int>> l = Expression.Lambda<Action<int>>(Expression.Assign(p, x), x);
                yield return new KeyValuePair<Expression, object>(Expression.Block(new[] { p, q }, Expression.Assign(q, l), p), default(int));
            }

            {
                ParameterExpression p = Expression.Parameter(typeof(TimeSpan));
                ParameterExpression q = Expression.Parameter(typeof(Func<TimeSpan>));
                Expression<Func<TimeSpan>> l = Expression.Lambda<Func<TimeSpan>>(p);
                yield return new KeyValuePair<Expression, object>(Expression.Block(new[] { p, q }, Expression.Assign(q, l), p), default(TimeSpan));
            }

            {
                ParameterExpression p = Expression.Parameter(typeof(TimeSpan));
                ParameterExpression q = Expression.Parameter(typeof(Action<TimeSpan>));
                ParameterExpression x = Expression.Parameter(typeof(TimeSpan));
                Expression<Action<TimeSpan>> l = Expression.Lambda<Action<TimeSpan>>(Expression.Assign(p, x), x);
                yield return new KeyValuePair<Expression, object>(Expression.Block(new[] { p, q }, Expression.Assign(q, l), p), default(TimeSpan));
            }

            {
                ParameterExpression p = Expression.Parameter(typeof(string));
                ParameterExpression q = Expression.Parameter(typeof(Func<string>));
                Expression<Func<string>> l = Expression.Lambda<Func<string>>(p);
                yield return new KeyValuePair<Expression, object>(Expression.Block(new[] { p, q }, Expression.Assign(q, l), p), default(string));
            }

            {
                ParameterExpression p = Expression.Parameter(typeof(string));
                ParameterExpression q = Expression.Parameter(typeof(Action<string>));
                ParameterExpression x = Expression.Parameter(typeof(string));
                Expression<Action<string>> l = Expression.Lambda<Action<string>>(Expression.Assign(p, x), x);
                yield return new KeyValuePair<Expression, object>(Expression.Block(new[] { p, q }, Expression.Assign(q, l), p), default(string));
            }

            {
                ParameterExpression p = Expression.Parameter(typeof(int?));
                ParameterExpression q = Expression.Parameter(typeof(Func<int?>));
                Expression<Func<int?>> l = Expression.Lambda<Func<int?>>(p);
                yield return new KeyValuePair<Expression, object>(Expression.Block(new[] { p, q }, Expression.Assign(q, l), p), default(int?));
            }

            {
                ParameterExpression p = Expression.Parameter(typeof(int?));
                ParameterExpression q = Expression.Parameter(typeof(Action<int?>));
                ParameterExpression x = Expression.Parameter(typeof(int?));
                Expression<Action<int?>> l = Expression.Lambda<Action<int?>>(Expression.Assign(p, x), x);
                yield return new KeyValuePair<Expression, object>(Expression.Block(new[] { p, q }, Expression.Assign(q, l), p), default(int?));
            }

            {
                ParameterExpression p = Expression.Parameter(typeof(TimeSpan?));
                ParameterExpression q = Expression.Parameter(typeof(Func<TimeSpan?>));
                Expression<Func<TimeSpan?>> l = Expression.Lambda<Func<TimeSpan?>>(p);
                yield return new KeyValuePair<Expression, object>(Expression.Block(new[] { p, q }, Expression.Assign(q, l), p), default(TimeSpan?));
            }

            {
                ParameterExpression p = Expression.Parameter(typeof(TimeSpan?));
                ParameterExpression q = Expression.Parameter(typeof(Action<TimeSpan?>));
                ParameterExpression x = Expression.Parameter(typeof(TimeSpan?));
                Expression<Action<TimeSpan?>> l = Expression.Lambda<Action<TimeSpan?>>(Expression.Assign(p, x), x);
                yield return new KeyValuePair<Expression, object>(Expression.Block(new[] { p, q }, Expression.Assign(q, l), p), default(TimeSpan?));
            }
        }

        #endregion

        #region Test verifiers

        private static void VerifyBlockClosureVariableInitialization(Expression e, object o)
        {
            Expression<Func<object>> f =
                Expression.Lambda<Func<object>>(
                    Expression.Convert(e, typeof(object)));

            Func<object> c = f.CompileToTestMethod();
            Assert.Equal(o, c());
        }

        #endregion

        private class ParameterChangingVisitor : ExpressionVisitor
        {
            protected override Expression VisitParameter(ParameterExpression node)
            {
                return Expression.Parameter(node.IsByRef ? node.Type.MakeByRefType() : node.Type, node.Name);
            }
        }

        [Fact]
        public static void EmptyBlock()
        {
            BlockExpression block = Expression.Block();
            Assert.Equal(typeof(void), block.Type);
            AssertExtensions.Throws<ArgumentOutOfRangeException>("index", () => block.Result);
            Action nop = Expression.Lambda<Action>(block).CompileToTestMethod();
            nop();
        }

        [Fact]
        public static void EmptyBlockExplicitType()
        {
            BlockExpression block = Expression.Block(typeof(void));
            Assert.Equal(typeof(void), block.Type);
            AssertExtensions.Throws<ArgumentOutOfRangeException>("index", () => block.Result);
            Action nop = Expression.Lambda<Action>(block).CompileToTestMethod();
            nop();
        }

        [Fact]
        public static void EmptyScope()
        {
            BlockExpression scope = Expression.Block(new[] { Expression.Parameter(typeof(int), "x") }, new Expression[0]);
            Assert.Equal(typeof(void), scope.Type);
            AssertExtensions.Throws<ArgumentOutOfRangeException>("index", () => scope.Result);
            Action nop = Expression.Lambda<Action>(scope).CompileToTestMethod();
            nop();
        }

        [Fact]
        public static void EmptyScopeExplicitType()
        {
            BlockExpression scope = Expression.Block(typeof(void), new[] { Expression.Parameter(typeof(int), "x") }, new Expression[0]);
            Assert.Equal(typeof(void), scope.Type);
            AssertExtensions.Throws<ArgumentOutOfRangeException>("index", () => scope.Result);
            Action nop = Expression.Lambda<Action>(scope).CompileToTestMethod();
            nop();
        }
    }
}
