// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class DefaultTests
    {
        public enum MyEnum
        {
            Value
        }

        public class EnumOutLambdaClass
        {
            public static void BarRef(out MyEnum o)
            {
                o = MyEnum.Value;
            }
        }

        [Fact]
        public void DefaultEnumRef()
        {
            ParameterExpression x = Expression.Variable(typeof(MyEnum), "x");

            Expression<Action> expression = Expression.Lambda<Action>(
                            Expression.Block(
                            new[] { x },
                            Expression.Assign(x, Expression.Default(typeof(MyEnum))),
                            Expression.Call(null, typeof(EnumOutLambdaClass).GetMethod(nameof(EnumOutLambdaClass.BarRef)), x)));

            expression.CompileToTestMethod()();
        }

        [Fact]
        public void DbNull()
        {
            Expression<Func<DBNull>> lambda = Expression.Lambda<Func<DBNull>>(Expression.Default(typeof(DBNull)));
            Func<DBNull> func = lambda.CompileToTestMethod();
            Assert.Null(func());
        }

        [Fact]
        public void StructType()
        {
            Expression<Func<Sp>> lambda = Expression.Lambda<Func<Sp>>(Expression.Default(typeof(Sp)));
            Func<Sp> func = lambda.CompileToTestMethod();
            Sp defaultValue = func();
            Assert.Equal(0, defaultValue.I);
            Assert.Equal(0.0, defaultValue.D);
        }

        [Fact]
        public void PrivateStructType()
        {
            Expression<Func<StPri>> lambda = Expression.Lambda<Func<StPri>>(Expression.Default(typeof(StPri)));
            Func<StPri> func = lambda.CompileToTestMethod();
            StPri defaultValue = func();
            Assert.Equal(0, defaultValue.IntProperty);
        }

        private struct StPri
        {
            public int IntProperty { get; set; }
        }
    }
}
