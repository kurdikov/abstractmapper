// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class AsNullableTests
    {
        #region Test methods

        [Fact]
        public static void CheckNullableEnumAsEnumTypeTest()
        {
            E?[] array = new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableEnumAsEnumType(array[i]);
            }
        }

        [Fact]
        public static void CheckEnumAsNullableEnumTypeTest()
        {
            E[] array = { 0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyEnumAsNullableEnumType(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableEnumAsNullableEnumTypeTest()
        {
            E?[] array = { null, 0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableEnumAsNullableEnumType(array[i]);
            }
        }

        [Fact]
        public static void CheckLongAsNullableEnumTypeTest()
        {
            long[] array = { 0, 1, long.MinValue, long.MaxValue };
            foreach (long value in array)
            {
                VerifyLongAsNullableEnumType(value);
            }
        }

        [Fact]
        public static void CheckNullableLongAsNullableEnumTypeTest()
        {
            long?[] array = { null, 0, 1, long.MinValue, long.MaxValue };
            foreach (long? value in array)
            {
                VerifyNullableLongAsNullableEnumType(value);
            }
        }

        [Fact]
        public static void CheckNullableEnumAsObjectTest()
        {
            E?[] array = new E?[] { null, (E)0, E.A, E.B, (E)int.MaxValue, (E)int.MinValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableEnumAsObject(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableIntAsObjectTest()
        {
            int?[] array = new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableIntAsObject(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableIntAsValueTypeTest()
        {
            int?[] array = new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableIntAsValueType(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableStructAsIEquatableOfStructTest()
        {
            S?[] array = new S?[] { null, default(S), new S() };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableStructAsIEquatableOfStruct(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableStructAsObjectTest()
        {
            S?[] array = new S?[] { null, default(S), new S() };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableStructAsObject(array[i]);
            }
        }

        [Fact]
        public static void CheckNullableStructAsValueTypeTest()
        {
            S?[] array = new S?[] { null, default(S), new S() };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyNullableStructAsValueType(array[i]);
            }
        }

        [Fact]
        public static void ConvertGenericWithStructRestrictionCastObjectAsEnum()
        {
            CheckGenericWithStructRestrictionAsObjectHelper<E>();
        }

        [Fact]
        public static void ConvertGenericWithStructRestrictionCastObjectAsStruct()
        {
            CheckGenericWithStructRestrictionAsObjectHelper<S>();
        }

        [Fact]
        public static void ConvertGenericWithStructRestrictionCastObjectAsStructWithStringAndField()
        {
            CheckGenericWithStructRestrictionAsObjectHelper<Scs>();
        }

        [Fact]
        public static void ConvertGenericWithStructRestrictionCastValueTypeAsEnum()
        {
            CheckGenericWithStructRestrictionAsValueTypeHelper<E>();
        }

        [Fact]
        public static void ConvertGenericWithStructRestrictionCastValueTypeAsStruct()
        {
            CheckGenericWithStructRestrictionAsValueTypeHelper<S>();
        }

        [Fact]
        public static void ConvertGenericWithStructRestrictionCastValueTypeAsStructWithStringAndField()
        {
            CheckGenericWithStructRestrictionAsValueTypeHelper<Scs>();
        }

        #endregion

        #region Generic helpers

        private static void CheckGenericWithStructRestrictionAsObjectHelper<Ts>() where Ts : struct
        {
            Ts[] array = new Ts[] { default(Ts), new Ts() };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyGenericWithStructRestrictionAsObject<Ts>(array[i]);
            }
        }

        private static void CheckGenericWithStructRestrictionAsValueTypeHelper<Ts>() where Ts : struct
        {
            Ts[] array = new Ts[] { default(Ts), new Ts() };
            for (int i = 0; i < array.Length; i++)
            {
                VerifyGenericWithStructRestrictionAsValueType<Ts>(array[i]);
            }
        }

        #endregion

        #region Test verifiers

        private static void VerifyNullableEnumAsEnumType(E? value)
        {
            Expression<Func<Enum>> e =
                Expression.Lambda<Func<Enum>>(
                    Expression.TypeAs(Expression.Constant(value, typeof(E?)), typeof(Enum)),
                    Enumerable.Empty<ParameterExpression>());
            Func<Enum> f = e.CompileToTestMethod();

            Assert.Equal(value as ValueType, f());
        }

        private static void VerifyEnumAsNullableEnumType(E value)
        {
            Expression<Func<E?>> e = Expression.Lambda<Func<E?>>(
                Expression.TypeAs(Expression.Constant(value), typeof(E?)));
            Func<E?> f = e.CompileToTestMethod();
            Assert.Equal(value, f());
        }

        private static void VerifyNullableEnumAsNullableEnumType(E? value)
        {
            Expression<Func<E?>> e = Expression.Lambda<Func<E?>>(
                Expression.TypeAs(Expression.Constant(value, typeof(E?)), typeof(E?)));
            Func<E?> f = e.CompileToTestMethod();
            Assert.Equal(value, f());
        }

        private static void VerifyLongAsNullableEnumType(long value)
        {
            Expression<Func<E?>> e = Expression.Lambda<Func<E?>>(
                Expression.TypeAs(Expression.Constant(value), typeof(E?)));
            Func<E?> f = e.CompileToTestMethod();
            Assert.False(f().HasValue);
        }

        private static void VerifyNullableLongAsNullableEnumType(long? value)
        {
            Expression<Func<E?>> e = Expression.Lambda<Func<E?>>(
                Expression.TypeAs(Expression.Constant(value, typeof(long?)), typeof(E?)));
            Func<E?> f = e.CompileToTestMethod();
            Assert.False(f().HasValue);
        }

        private static void VerifyNullableEnumAsObject(E? value)
        {
            Expression<Func<object>> e =
                Expression.Lambda<Func<object>>(
                    Expression.TypeAs(Expression.Constant(value, typeof(E?)), typeof(object)),
                    Enumerable.Empty<ParameterExpression>());
            Func<object> f = e.CompileToTestMethod();

            Assert.Equal(value as object, f());
        }

        private static void VerifyNullableIntAsObject(int? value)
        {
            Expression<Func<object>> e =
                Expression.Lambda<Func<object>>(
                    Expression.TypeAs(Expression.Constant(value, typeof(int?)), typeof(object)),
                    Enumerable.Empty<ParameterExpression>());
            Func<object> f = e.CompileToTestMethod();

            Assert.Equal(value as object, f());
        }

        private static void VerifyNullableIntAsValueType(int? value)
        {
            Expression<Func<ValueType>> e =
                Expression.Lambda<Func<ValueType>>(
                    Expression.TypeAs(Expression.Constant(value, typeof(int?)), typeof(ValueType)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ValueType> f = e.CompileToTestMethod();

            Assert.Equal(value as ValueType, f());
        }

        private static void VerifyNullableStructAsIEquatableOfStruct(S? value)
        {
            Expression<Func<IEquatable<S>>> e =
                Expression.Lambda<Func<IEquatable<S>>>(
                    Expression.TypeAs(Expression.Constant(value, typeof(S?)), typeof(IEquatable<S>)),
                    Enumerable.Empty<ParameterExpression>());
            Func<IEquatable<S>> f = e.CompileToTestMethod();

            Assert.Equal(value as IEquatable<S>, f());
        }

        private static void VerifyNullableStructAsObject(S? value)
        {
            Expression<Func<object>> e =
                Expression.Lambda<Func<object>>(
                    Expression.TypeAs(Expression.Constant(value, typeof(S?)), typeof(object)),
                    Enumerable.Empty<ParameterExpression>());
            Func<object> f = e.CompileToTestMethod();

            Assert.Equal(value as object, f());
        }

        private static void VerifyNullableStructAsValueType(S? value)
        {
            Expression<Func<ValueType>> e =
                Expression.Lambda<Func<ValueType>>(
                    Expression.TypeAs(Expression.Constant(value, typeof(S?)), typeof(ValueType)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ValueType> f = e.CompileToTestMethod();

            Assert.Equal(value as ValueType, f());
        }

        private static void VerifyGenericWithStructRestrictionAsObject<Ts>(Ts value) where Ts : struct
        {
            Expression<Func<object>> e =
                Expression.Lambda<Func<object>>(
                    Expression.TypeAs(Expression.Constant(value, typeof(Ts)), typeof(object)),
                    Enumerable.Empty<ParameterExpression>());
            Func<object> f = e.CompileToTestMethod();

            Assert.Equal(value as object, f());
        }

        private static void VerifyGenericWithStructRestrictionAsValueType<Ts>(Ts value) where Ts : struct
        {
            Expression<Func<ValueType>> e =
                Expression.Lambda<Func<ValueType>>(
                    Expression.TypeAs(Expression.Constant(value, typeof(Ts)), typeof(ValueType)),
                    Enumerable.Empty<ParameterExpression>());
            Func<ValueType> f = e.CompileToTestMethod();

            Assert.Equal(value as ValueType, f());
        }

        #endregion
    }
}
