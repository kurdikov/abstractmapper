// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class LiftedSubtractNullableTests
    {
        #region Test methods

        [Fact]
        public static void CheckLiftedSubtractNullableByteTest()
        {
            byte?[] values = new byte?[] { null, 0, 1, byte.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableByte(values[i], values[j]);
                }
            }
        }

        [Fact]
        public static void CheckLiftedSubtractNullableCharTest()
        {
            char?[] values = new char?[] { null, '\0', '\b', 'A', '\uffff' };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableChar(values[i], values[j]);
                }
            }
        }

        [Fact]
        public static void CheckLiftedSubtractNullableDecimalTest()
        {
            decimal?[] values = new decimal?[] { null, decimal.Zero, decimal.One, decimal.MinusOne, decimal.MinValue, decimal.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableDecimal(values[i], values[j]);
                }
            }
        }

        [Fact]
        public static void CheckLiftedSubtractNullableDoubleTest()
        {
            double?[] values = new double?[] { null, 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableDouble(values[i], values[j]);
                }
            }
        }

        [Fact]
        public static void CheckLiftedSubtractNullableFloatTest()
        {
            float?[] values = new float?[] { null, 0, 1, -1, float.MinValue, float.MaxValue, float.Epsilon, float.NegativeInfinity, float.PositiveInfinity, float.NaN };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableFloat(values[i], values[j]);
                }
            }
        }

        [Fact]
        public static void CheckLiftedSubtractNullableIntTest()
        {
            int?[] values = new int?[] { null, 0, 1, -1, int.MinValue, int.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableInt(values[i], values[j]);
                }
            }
        }

        [Fact]
        public static void CheckLiftedSubtractNullableLongTest()
        {
            long?[] values = new long?[] { null, 0, 1, -1, long.MinValue, long.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableLong(values[i], values[j]);
                }
            }
        }

        [Fact]
        public static void CheckLiftedSubtractNullableSByteTest()
        {
            sbyte?[] values = new sbyte?[] { null, 0, 1, -1, sbyte.MinValue, sbyte.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableSByte(values[i], values[j]);
                }
            }
        }

        [Fact]
        public static void CheckLiftedSubtractNullableShortTest()
        {
            short?[] values = new short?[] { null, 0, 1, -1, short.MinValue, short.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableShort(values[i], values[j]);
                }
            }
        }

        [Fact]
        public static void CheckLiftedSubtractNullableUIntTest()
        {
            uint?[] values = new uint?[] { null, 0, 1, uint.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableUInt(values[i], values[j]);
                }
            }
        }

        [Fact]
        public static void CheckLiftedSubtractNullableULongTest()
        {
            ulong?[] values = new ulong?[] { null, 0, 1, ulong.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableULong(values[i], values[j]);
                }
            }
        }

        [Fact]
        public static void CheckLiftedSubtractNullableUShortTest()
        {
            ushort?[] values = new ushort?[] { null, 0, 1, ushort.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableUShort(values[i], values[j]);
                }
            }
        }

        [Fact]
        public static void CheckLiftedSubtractNullableNumberTest()
        {
            Number?[] values = new Number?[] { null, new Number(0), new Number(1), Number.MaxValue };
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = 0; j < values.Length; j++)
                {
                    VerifySubtractNullableNumber(values[i], values[j]);
                }
            }
        }

        #endregion

        #region Helpers

        public static byte SubtractNullableByte(byte a, byte b)
        {
            return unchecked((byte)(a - b));
        }

        public static char SubtractNullableChar(char a, char b)
        {
            return unchecked((char)(a - b));
        }

        public static decimal SubtractNullableDecimal(decimal a, decimal b)
        {
            return (decimal)(a - b);
        }

        public static double SubtractNullableDouble(double a, double b)
        {
            return (double)(a - b);
        }

        public static float SubtractNullableFloat(float a, float b)
        {
            return (float)(a - b);
        }

        public static int SubtractNullableInt(int a, int b)
        {
            return unchecked((int)(a - b));
        }

        public static long SubtractNullableLong(long a, long b)
        {
            return unchecked((long)(a - b));
        }

        public static sbyte SubtractNullableSByte(sbyte a, sbyte b)
        {
            return unchecked((sbyte)(a - b));
        }

        public static short SubtractNullableShort(short a, short b)
        {
            return unchecked((short)(a - b));
        }

        public static uint SubtractNullableUInt(uint a, uint b)
        {
            return unchecked((uint)(a - b));
        }

        public static ulong SubtractNullableULong(ulong a, ulong b)
        {
            return unchecked((ulong)(a - b));
        }

        public static ushort SubtractNullableUShort(ushort a, ushort b)
        {
            return unchecked((ushort)(a - b));
        }

        #endregion

        #region Test verifiers

        private static void VerifySubtractNullableByte(byte? a, byte? b)
        {
            Expression<Func<byte?>> e =
                Expression.Lambda<Func<byte?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(byte?)),
                        Expression.Constant(b, typeof(byte?)),
                        typeof(LiftedSubtractNullableTests).GetTypeInfo().GetDeclaredMethod("SubtractNullableByte")));
            Func<byte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((byte?)(a - b)), f());
        }

        private static void VerifySubtractNullableChar(char? a, char? b)
        {
            Expression<Func<char?>> e =
                Expression.Lambda<Func<char?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(char?)),
                        Expression.Constant(b, typeof(char?)),
                        typeof(LiftedSubtractNullableTests).GetTypeInfo().GetDeclaredMethod("SubtractNullableChar")));
            Func<char?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((char?)(a - b)), f());
        }

        private static void VerifySubtractNullableDecimal(decimal? a, decimal? b)
        {
            Expression<Func<decimal?>> e =
                Expression.Lambda<Func<decimal?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(decimal?)),
                        Expression.Constant(b, typeof(decimal?)),
                        typeof(LiftedSubtractNullableTests).GetTypeInfo().GetDeclaredMethod("SubtractNullableDecimal")));
            Func<decimal?> f = e.CompileToTestMethod();

            decimal? expected = default(decimal);
            try
            {
                expected = checked(a - b);
            }
            catch (OverflowException)
            {
                Assert.Throws<OverflowException>(() => f());
                return;
            }

            Assert.Equal(expected, f());
        }

        private static void VerifySubtractNullableDouble(double? a, double? b)
        {
            Expression<Func<double?>> e =
                Expression.Lambda<Func<double?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(double?)),
                        Expression.Constant(b, typeof(double?)),
                        typeof(LiftedSubtractNullableTests).GetTypeInfo().GetDeclaredMethod("SubtractNullableDouble")));
            Func<double?> f = e.CompileToTestMethod();

            Assert.Equal(a - b, f());
        }

        private static void VerifySubtractNullableFloat(float? a, float? b)
        {
            Expression<Func<float?>> e =
                Expression.Lambda<Func<float?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(float?)),
                        Expression.Constant(b, typeof(float?)),
                        typeof(LiftedSubtractNullableTests).GetTypeInfo().GetDeclaredMethod("SubtractNullableFloat")));
            Func<float?> f = e.CompileToTestMethod();

            Assert.Equal(a - b, f());
        }

        private static void VerifySubtractNullableInt(int? a, int? b)
        {
            Expression<Func<int?>> e =
                Expression.Lambda<Func<int?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(int?)),
                        Expression.Constant(b, typeof(int?)),
                        typeof(LiftedSubtractNullableTests).GetTypeInfo().GetDeclaredMethod("SubtractNullableInt")));
            Func<int?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked(a - b), f());
        }

        private static void VerifySubtractNullableLong(long? a, long? b)
        {
            Expression<Func<long?>> e =
                Expression.Lambda<Func<long?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(long?)),
                        Expression.Constant(b, typeof(long?)),
                        typeof(LiftedSubtractNullableTests).GetTypeInfo().GetDeclaredMethod("SubtractNullableLong")));
            Func<long?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked(a - b), f());
        }

        private static void VerifySubtractNullableSByte(sbyte? a, sbyte? b)
        {
            Expression<Func<sbyte?>> e =
                Expression.Lambda<Func<sbyte?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(sbyte?)),
                        Expression.Constant(b, typeof(sbyte?)),
                        typeof(LiftedSubtractNullableTests).GetTypeInfo().GetDeclaredMethod("SubtractNullableSByte")));
            Func<sbyte?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((sbyte?)(a - b)), f());
        }

        private static void VerifySubtractNullableShort(short? a, short? b)
        {
            Expression<Func<short?>> e =
                Expression.Lambda<Func<short?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(short?)),
                        Expression.Constant(b, typeof(short?)),
                        typeof(LiftedSubtractNullableTests).GetTypeInfo().GetDeclaredMethod("SubtractNullableShort")));
            Func<short?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((short?)(a - b)), f());
        }

        private static void VerifySubtractNullableUInt(uint? a, uint? b)
        {
            Expression<Func<uint?>> e =
                Expression.Lambda<Func<uint?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(uint?)),
                        Expression.Constant(b, typeof(uint?)),
                        typeof(LiftedSubtractNullableTests).GetTypeInfo().GetDeclaredMethod("SubtractNullableUInt")));
            Func<uint?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked(a - b), f());
        }

        private static void VerifySubtractNullableULong(ulong? a, ulong? b)
        {
            Expression<Func<ulong?>> e =
                Expression.Lambda<Func<ulong?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(ulong?)),
                        Expression.Constant(b, typeof(ulong?)),
                        typeof(LiftedSubtractNullableTests).GetTypeInfo().GetDeclaredMethod("SubtractNullableULong")));
            Func<ulong?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked(a - b), f());
        }

        private static void VerifySubtractNullableUShort(ushort? a, ushort? b)
        {
            Expression<Func<ushort?>> e =
                Expression.Lambda<Func<ushort?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(ushort?)),
                        Expression.Constant(b, typeof(ushort?)),
                        typeof(LiftedSubtractNullableTests).GetTypeInfo().GetDeclaredMethod("SubtractNullableUShort")));
            Func<ushort?> f = e.CompileToTestMethod();

            Assert.Equal(unchecked((ushort?)(a - b)), f());
        }

        private static void VerifySubtractNullableNumber(Number? a, Number? b)
        {
            Expression<Func<Number?>> e =
                Expression.Lambda<Func<Number?>>(
                    Expression.Subtract(
                        Expression.Constant(a, typeof(Number?)),
                        Expression.Constant(b, typeof(Number?))));
            Assert.Equal(typeof(Number?), e.Body.Type);
            Func<Number?> f = e.CompileToTestMethod();

            Number? expected = a - b;
            Assert.Equal(expected, f());
        }

        #endregion
    }
}
