// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class ListInitExpressionTests
    {
        private class NonEnumerableAddable
        {
            public readonly List<int> Store = new List<int>();

            public void Add(int value)
            {
                Store.Add(value);
            }
        }

        private class EnumerableStaticAdd : IEnumerable<string>
        {
            public IEnumerator<string> GetEnumerator()
            {
                yield break;
            }

            IEnumerator IEnumerable.GetEnumerator()  => GetEnumerator();

            public static void Add(string value)
            {
            }
        }

        public class AnyTypeList : IEnumerable<object>
        {
            private readonly List<object> _inner = new List<object>();

            public void Add<T>(T item) => _inner.Add(item);

            public void AddIntRegardless<T>(int item) => _inner.Add(item);

            public IEnumerator<object> GetEnumerator() => _inner.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        public struct ListValueType : IEnumerable<int>
        {
            private List<int> _store;

            private List<int> EnsureStore() => _store ?? (_store = new List<int>());

            public int Add(int value)
            {
                EnsureStore().Add(value);
                return value;
            }

            public IEnumerator<int> GetEnumerator() => EnsureStore().GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }


        private static IEnumerable<object[]> ZeroInitializerInits()
        {
            NewExpression validNew = Expression.New(typeof(List<int>));
            yield return new object[] {Expression.ListInit(validNew, new Expression[0])};
            yield return new object[] {Expression.ListInit(validNew, Enumerable.Empty<Expression>())};
            yield return new object[] {Expression.ListInit(validNew, new ElementInit[0])};
            yield return new object[] {Expression.ListInit(validNew, Enumerable.Empty<ElementInit>())};

            MethodInfo validMethod = typeof(List<int>).GetMethod("Add");
            yield return new object[] {Expression.ListInit(validNew, validMethod)};
            yield return new object[] {Expression.ListInit(validNew, validMethod, Enumerable.Empty<Expression>())};

            yield return new object[] {Expression.ListInit(validNew, null, new Expression[0])};
            yield return new object[] {Expression.ListInit(validNew, null, Enumerable.Empty<Expression>())};
        }

        [Theory, MemberData(nameof(ZeroInitializerInits))]
        public void ZeroInitializers(Expression init)
        {
            Expression<Func<List<int>>> exp = Expression.Lambda<Func<List<int>>>(init);
            Func<List<int>> func = exp.CompileToTestMethod();
            Assert.Empty(func());
        }


        [Fact]
        public void GenericAddMethod()
        {
            NewExpression newExp = Expression.New(typeof(AnyTypeList));
            MethodInfo adder = typeof(AnyTypeList).GetMethod(nameof(AnyTypeList.Add)).MakeGenericMethod(typeof(int));
            Expression<Func<AnyTypeList>> lambda =
                Expression.Lambda<Func<AnyTypeList>>(
                    Expression.ListInit(
                        newExp, adder, Expression.Constant(3), Expression.Constant(2), Expression.Constant(1)));
            Func<AnyTypeList> func = lambda.CompileToTestMethod();
            Assert.Equal(new object[] {3, 2, 1}, func());
        }

        [Fact]
        public void InitializeVoidAdd()
        {
            Expression<Func<List<int>>> listInit = () => new List<int> { 1, 2, 4, 16, 42 };
            Func<List<int>> func = listInit.CompileToTestMethod();
            Assert.Equal(new[] { 1, 2, 4, 16, 42 }, func());
        }

        [Fact]
        public void InitializeNonVoidAdd()
        {
            Expression<Func<HashSet<int>>> hashInit = () => new HashSet<int> { 1, 2, 4, 16, 42 };
            Func<HashSet<int>> func = hashInit.CompileToTestMethod();
            Assert.Equal(new[] { 1, 2, 4, 16, 42 }, func().OrderBy(i => i));
        }

        [Fact]
        public void InitializeTwoParameterAdd()
        {
            Expression<Func<Dictionary<string, int>>> dictInit = () => new Dictionary<string, int>
            {
                { "a", 1 }, {"b", 2 }, {"c", 3 }
            };
            Func<Dictionary<string, int>> func = dictInit.CompileToTestMethod();
            var expected = new Dictionary<string, int>
            {
                { "a", 1 }, {"b", 2 }, {"c", 3 }
            };
            Assert.Equal(expected.OrderBy(kvp => kvp.Key), func().OrderBy(kvp => kvp.Key));
        }


        [Fact]
        public void ValueTypeList()
        {
            Expression<Func<ListValueType>> lambda = Expression.Lambda<Func<ListValueType>>(
                Expression.ListInit(
                    Expression.New(typeof(ListValueType)),
                    Expression.Constant(5),
                    Expression.Constant(6),
                    Expression.Constant(7),
                    Expression.Constant(8)
                )
            );
            Func<ListValueType> func = lambda.CompileToTestMethod();
            Assert.Equal(new[] { 5, 6, 7, 8 }, func());
        }

        [Fact]
        public void EmptyValueTypeList()
        {
            Expression<Func<ListValueType>> lambda = Expression.Lambda<Func<ListValueType>>(
                Expression.ListInit(
                    Expression.New(typeof(ListValueType)),
                    Array.Empty<Expression>()
                )
            );
            Func<ListValueType> func = lambda.CompileToTestMethod();
            Assert.Empty(func());
        }
    }
}
