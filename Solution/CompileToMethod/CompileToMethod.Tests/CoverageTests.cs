﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using Xunit;

namespace CompileToMethod.Tests
{
    public static class CoverageTests
    {
        [Theory]
        [InlineData(4)]
        [InlineData(511)]
        public static void HugeRuntimeVariablesList_TriggersLargeArrayBuilder(int count)
        {
            var vars = Enumerable.Range(0, count).Select(i => Expression.Parameter(typeof(object), "p" + i)).ToList();
            var expr = Expression.Lambda<Func<object>>(
                Expression.Block(vars,
                    Expression.RuntimeVariables(vars)));
            var func = expr.CompileToTestMethod();
            dynamic result = func();
            Assert.Equal(count, result.Count);
        }

        [Fact]
        public static void CompileToMethodPreconditions_TriggersContractUtils_TriggersErrorTypeBuilder()
        {
            Expression<Func<int>> lambda = () => 0;

            Assert.Throws<ArgumentNullException>("method", () => lambda.CompileToMethod(null));

            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName("Coverage"), AssemblyBuilderAccess.RunAndCollect);
            var moduleBuilder = assemblyBuilder.DefineDynamicModule("Coverage");
            var typeBuilder = moduleBuilder.DefineType("Coverage");
            var methodBuilder = typeBuilder.DefineMethod("Method", MethodAttributes.Public);
            Assert.Throws<ArgumentException>("method", () => lambda.CompileToMethod(methodBuilder));

            var moduleMethodBuilder = moduleBuilder.DefineGlobalMethod("Coverage", MethodAttributes.Public | MethodAttributes.Static, typeof(int), Type.EmptyTypes);
            Assert.Throws<ArgumentException>(() => lambda.CompileToMethod(moduleMethodBuilder));
        }

        // LocalBoxStorage.FreeLocal
        // LabelInfo.ValidateJump Try/catch across
        // LambdaCompiler.EmitExpressionAddress
        // LambdaCompiler checked byte/sbyte convert
        // LambdaCompiler jumps through switches
        // EmitConstantArray

        [Fact]
        public static void InvocationWithExpressionReturningExpression_TriggersTypeUtilsFindGenericType()
        {
            var method = typeof(CoverageTests).GetMethod(nameof(SubtractingExpression), BindingFlags.Public | BindingFlags.Static);
            var expr = Expression.Lambda<Func<int>>(
                Expression.Invoke(
                    Expression.Call(method),
                    Expression.TryFinally(
                        Expression.Constant(1),
                        Expression.Empty()),
                    Expression.Constant(2)));
            var func = expr.CompileToTestMethod();
            var result = func();
            Assert.Equal(-1, result);
        }

        public static Expression<Func<int, int, int>> SubtractingExpression() => (o1, o2) => o1 - o2;

        [Fact]
        public static void LambdaWithTailCall_TriggersTypeUtilsIsByRefParameter()
        {
            var method = typeof(CoverageTests).GetMethod(nameof(TailCall), BindingFlags.Public | BindingFlags.Static);
            var param = Expression.Parameter(typeof(object));
            var expr = Expression.Lambda<Func<object, object>>(
                Expression.Call(method, param),
                true,
                param);
            var func = expr.CompileToTestMethod();
            var obj = new object();
            var result = func(obj);
            Assert.Same(obj, result);
        }

        [Fact]
        public static void LambdaWithTailCallRef_TriggersTypeUtilsIsByRefParameter()
        {
            var method = typeof(CoverageTests).GetMethod(nameof(TailCallRef), BindingFlags.Public | BindingFlags.Static);
            var param = Expression.Parameter(typeof(object));
            var expr = Expression.Lambda<Func<object, object>>(
                Expression.Call(method, param),
                true,
                param);
            var func = expr.CompileToTestMethod();
            var obj = new object();
            var result = func(obj);
            Assert.Same(obj, result);
        }

        [Fact]
        public static void LambdaWithTailCallOut_TriggersTypeUtilsIsByRefParameter()
        {
            var method = typeof(CoverageTests).GetMethod(nameof(TailCallOut), BindingFlags.Public | BindingFlags.Static);
            var param = Expression.Parameter(typeof(object));
            var expr = Expression.Lambda<Func<object, object>>(
                Expression.Call(method, param),
                true,
                param);
            var func = expr.CompileToTestMethod();
            var obj = new object();
            var result = func(obj);
            Assert.Same(obj, result);
        }

        public static object TailCall(object param) => param;
        public static object TailCallRef(ref object param) => param;
        public static object TailCallOut([Out]object param) => param;

        [Fact]
        public static void LambdaWithTailSwitch_TriggersEmitSwitchCasesTail()
        {
            var param = Expression.Parameter(typeof(int));
            var expr = Expression.Lambda<Func<int, int>>(
                Expression.Switch(
                    param,
                    Expression.Constant(0),
                    Expression.SwitchCase(
                        Expression.Constant(1),
                        Expression.Constant(1))),
                true,
                param);
            var func = expr.CompileToTestMethod();
            Assert.Equal(0, func(0));
            Assert.Equal(1, func(1));
            Assert.Equal(0, func(2));
        }

        [Fact]
        public static void LambdaWithTailBlockWithoutGoto_TriggersEmitBlockEmitAsMiddle()
        {
            var expr = Expression.Lambda<Func<int>>(
                Expression.Block(
                    Expression.Constant(0),
                    Expression.Constant(1)),
                true);
            var func = expr.CompileToTestMethod();
            Assert.Equal(1, func());
        }

        [Fact]
        public static void LambdaWithTailBlockWithGoto_TriggersEmitBlockEmitAsMiddle()
        {
            var label = Expression.Label(typeof(int), "ret");
            var expr = Expression.Lambda<Func<int>>(
                Expression.Block(
                   Expression.Constant(0),
                   Expression.Return(label, Expression.Constant(1)),
                   Expression.Label(label, Expression.Constant(2))),
                true);
            var func = expr.CompileToTestMethod();
            Assert.Equal(1, func());
        }

        [Fact]
        public static void LambdaWithTailBlockWithGoto_TriggersEmitBlockEmitAsTail()
        {
            LambdaWithTailBlockWithGoto_TriggersEmitBlockEmitAsTail_Executed = false;
            var field = typeof(CoverageTests).GetField(nameof(LambdaWithTailBlockWithGoto_TriggersEmitBlockEmitAsTail_Executed),
                BindingFlags.Public | BindingFlags.Static);
            var label = Expression.Label(typeof(void), "ret");
            var expr = Expression.Lambda<Action>(
                Expression.Block(
                   Expression.Assign(Expression.Field(null, field), Expression.Constant(true)),
                   Expression.Goto(label),
                   Expression.Assign(Expression.Field(null, field), Expression.Constant(false)),
                   Expression.Goto(label, Expression.Block()),
                   Expression.Label(label)),
                true);
            var func = expr.CompileToTestMethod();
            func();
            Assert.True(LambdaWithTailBlockWithGoto_TriggersEmitBlockEmitAsTail_Executed);
        }

        public static bool LambdaWithTailBlockWithGoto_TriggersEmitBlockEmitAsTail_Executed;

        [Fact]
        public static void LambdaWithUndefinedVariable()
        {
            var undefined = Expression.Parameter(typeof(object), "undefined");
            var expr = Expression.Lambda<Func<object>>(undefined);
            Assert.Throws<InvalidOperationException>(() => expr.CompileToTestMethod());
        }

        [Fact]
        public static void MethodConstant_TriggersILGenEmit()
        {
            var method = typeof(CoverageTests).GetMethod(nameof(Method), BindingFlags.Public | BindingFlags.Static);
            var expr = Expression.Lambda<Func<object>>(
                Expression.Constant(method, typeof(MethodInfo)));
            var func = expr.CompileToTestMethodSimple();
            var result = func();
            Assert.Same(method, result);
        }

        [Fact]
        public static void MethodBaseConstant_TriggersILGenEmit()
        {
            var method = typeof(CoverageTests).GetMethod(nameof(Method), BindingFlags.Public | BindingFlags.Static);
            var expr = Expression.Lambda<Func<object>>(
                Expression.Constant(method, typeof(MethodBase)));
            var func = expr.CompileToTestMethodSimple();
            var result = func();
            Assert.Same(method, result);
        }

        public static int Method() => 42;

        [Fact]
        public static void ModuleMethodConstant_TriggersILGenEmit()
        {
            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName("ModuleMethod"), AssemblyBuilderAccess.RunAndCollect);
            var moduleBuilder = assemblyBuilder.DefineDynamicModule("ModuleMethod");
            var moduleMethodBuilder = moduleBuilder.DefineGlobalMethod("ModuleMethod", MethodAttributes.Public | MethodAttributes.Static,
                typeof(int), Type.EmptyTypes);
            var ilgen = moduleMethodBuilder.GetILGenerator();
            ilgen.Emit(OpCodes.Ldc_I4_1);
            ilgen.Emit(OpCodes.Ret);
            moduleBuilder.CreateGlobalFunctions();
            var expr = Expression.Lambda<Func<object>>(
                Expression.Constant(moduleMethodBuilder, typeof(MethodInfo)));
            var typeBuilder = moduleBuilder.DefineType("Type");
            var methodBuilder = typeBuilder.DefineMethod("Method", MethodAttributes.Static | MethodAttributes.Public);
            expr.CompileToMethod(methodBuilder);
            var type = typeBuilder.CreateTypeInfo();
            var result = type.GetMethod("Method");
            Assert.Same(moduleBuilder.GetMethod("ModuleMethod"), result.Invoke(null, new object[0]));
        }

        [Fact]
        public static void GenericTypeMethodConstant_TriggersILGenEmit()
        {
            var method = typeof(Generic<object>).GetMethod(nameof(Generic<object>.Method), BindingFlags.Public | BindingFlags.Instance);
            var expr = Expression.Lambda<Func<object>>(
                Expression.Constant(method, typeof(MethodInfo)));
            var func = expr.CompileToTestMethodSimple();
            var result = func();
            Assert.Same(method, result);
        }

        public class Generic<T>
        {
            public void Method() { }
        }

        [Fact]
        public static void ConstructorConstant_TriggersILGenEmit()
        {
            var ctor = typeof(Constructable).GetConstructor(Type.EmptyTypes);
            var expr = Expression.Lambda<Func<object>>(
                Expression.Constant(ctor, typeof(ConstructorInfo)));
            var func = expr.CompileToTestMethodSimple();
            var result = func();
            Assert.Same(ctor, result);
        }

        public class Constructable
        {
        }

        [Fact]
        public static void TypeInfoConstant_TriggersILGenEmitCastToType()
        {
            var typeInfo = typeof(CoverageTests).GetTypeInfo();
            var expr = Expression.Lambda<Func<object>>(
                Expression.Constant(typeInfo, typeof(TypeInfo)));
            var func = expr.CompileToTestMethodSimple();
            var result = func();
            Assert.Same(typeInfo, result);
        }

        [Fact]
        public static void BadMethodConstant_TriggersILGenEmit()
        {
            var method = typeof(Invisible).GetMethod(nameof(Invisible.Method), BindingFlags.Public | BindingFlags.Instance);
            var expr = Expression.Lambda<Func<object>>(
                Expression.Constant(method, typeof(MethodInfo)));
            Assert.Throws<InvalidOperationException>(() => expr.CompileToTestMethodSimple());
        }

        [Fact]
        public static void BadTypeConstant_TriggersVariableBinderCanEmit()
        {
            var type = typeof(Invisible);
            var expr = Expression.Lambda<Func<object>>(
                Expression.Constant(type, typeof(Type)));
            Assert.Throws<InvalidOperationException>(() => expr.CompileToTestMethodSimple());
        }

        private class Invisible
        {
            public void Method() { }
        }

        [Fact]
        public static void ManyArguments_TriggerILGenEmit_LoadArg256_LoadArgAddress256_StoreArg256()
        {
            var parameters = Enumerable.Range(0, 257).Select(i => Expression.Parameter(typeof(bool?), "p" + i)).ToList();
            var expr = Expression.Lambda(
                Expression.Block(
                    Expression.Assign(parameters[256], parameters[255]),
                    Expression.Or(
                        Expression.Equal(parameters[256], Expression.Constant(null)),
                        Expression.Equal(parameters[256], Expression.Constant(true, typeof(bool?))))),
                parameters);
            var func = expr.CompileToTestMethod();
            var parameterValues = new object[257];

            parameterValues[255] = false;
            var result = (bool)func.DynamicInvoke(parameterValues);
            Assert.False(result);

            parameterValues[255] = true;
            result = (bool)func.DynamicInvoke(parameterValues);
            Assert.True(result);

            parameterValues[255] = null;
            result = (bool)func.DynamicInvoke(parameterValues);
            Assert.True(result);
        }

        [Fact]
        public static void StaticFieldNullComparison_TriggersILGenEmitFieldAddress()
        {
            var field = typeof(CoverageTests).GetField(nameof(Field), BindingFlags.Public | BindingFlags.Static);
            var expr = Expression.Lambda<Func<bool>>(
                Expression.Equal(
                    Expression.Field(null, field),
                    Expression.Constant(null)));
            var func = expr.CompileToTestMethodSimple();
            Field = null;
            var result = func();
            Assert.True(result);
            Field = true;
            result = func();
            Assert.False(result);
            Field = false;
            result = func();
            Assert.False(result);
        }

        public static bool? Field;

        [Fact]
        public static void AmbiguousJump_TriggersLabelInfoValidateJump()
        {
            var label = Expression.Label("label");
            var expr = Expression.Lambda<Action>(
                Expression.Block(
                    Expression.Block(
                        Expression.Label(label)
                    ),
                    Expression.Block(
                        Expression.Label(label)
                    ),
                    Expression.Goto(label)
                ));
            Assert.Throws<InvalidOperationException>(() => expr.CompileToTestMethodSimple());
        }

        [Fact]
        public static void JumpFromTry_TriggersLabelInfoValidateJump_Leave()
        {
            var label = Expression.Label("label");
            var expr = Expression.Lambda<Func<int>>(
                Expression.Block(
                    Expression.TryCatchFinally(
                        Expression.Goto(label),
                        Expression.Empty(),
                        Expression.Catch(
                            typeof(Exception),
                            Expression.Goto(label)
                        )
                    ),
                    Expression.Block(
                        Expression.Label(label)
                    ),
                    Expression.Constant(1)
                ));
            var func = expr.CompileToTestMethodSimple();
            var result = func();
            Assert.Equal(1, result);
        }

        [Fact]
        public static void LabelInBlockInsideSwitch_TriggersLabelScopeInfoTryGetWithNullDictionary()
        {
            var param = Expression.Parameter(typeof(int));
            var label = Expression.Label("label");
            var expr = Expression.Lambda<Func<int, int>>(
                Expression.Switch(
                    param,
                    Expression.Block(
                        Expression.Constant(3)),
                    Expression.SwitchCase(
                        Expression.Block(
                            Expression.Label(label, Expression.Constant(1)),
                            Expression.Constant(1)
                        ),
                        Expression.Constant(1)
                    )
                ),
                param
            );
            var func = expr.CompileToTestMethodSimple();
            var result = func(1);
            Assert.Equal(1, result);
            result = func(2);
            Assert.Equal(3, result);
        }

        [Fact]
        public static void Quote_CompilationError()
        {
            var expr = Expression.Lambda<Func<Expression<Action>>>(
                Expression.Quote(
                    Expression.Lambda<Action>(
                        Expression.Empty()
                    )
                )
            );
            Assert.Throws<InvalidOperationException>(() => expr.CompileToTestMethodSimple());
        }

        [Fact]
        public static void TryInsideFilter_CompilationError()
        {
            var expr = Expression.Lambda<Action>(
                Expression.TryCatch(
                    Expression.Empty(),
                    Expression.Catch(
                        typeof(Exception),
                        Expression.Empty(),
                        Expression.TryFinally(
                            Expression.Constant(true),
                            Expression.Empty()))));
            Assert.Throws<InvalidOperationException>(() => expr.CompileToTestMethodSimple());
        }

        [Fact]
        public static void Switch_TriggersLambdaCompilerEmitSwitchBucketsRecursion()
        {
            var param = Expression.Parameter(typeof(int));
            var cases = Enumerable.Range(0, 4).Select(i =>
                Expression.SwitchCase(
                    Expression.Constant(i + 1),
                    Expression.Constant(3 * i))).ToArray();
            var expr = Expression.Lambda<Func<int, int>>(
                Expression.Switch(
                    param,
                    Expression.Constant(0),
                    cases),
                param);
            var func = expr.CompileToTestMethodSimple();
            Assert.Equal(1, func(0));
            Assert.Equal(2, func(3));
            Assert.Equal(3, func(6));
            Assert.Equal(4, func(9));
            Assert.Equal(0, func(1));
        }

        [Fact]
        public static void Switch_Unsigned_TriggersLambdaCompilerEmitSwitchBucketsRecursion()
        {
            var param = Expression.Parameter(typeof(uint));
            var cases = Enumerable.Range(0, 4).Select(i =>
                Expression.SwitchCase(
                    Expression.Constant(i + 1),
                    Expression.Constant((uint)(3 * i), typeof(uint)))).ToArray();
            var expr = Expression.Lambda<Func<uint, int>>(
                Expression.Switch(
                    param,
                    Expression.Constant(0),
                    cases),
                param);
            var func = expr.CompileToTestMethodSimple();
            Assert.Equal(1, func(0u));
            Assert.Equal(2, func(3u));
            Assert.Equal(3, func(6u));
            Assert.Equal(4, func(9u));
            Assert.Equal(0, func(1u));
        }

        [Fact]
        public static void Switch_DefaultBodyNull_TriggersLambdaCompilerEmitSwitchCases()
        {
            var param = Expression.Parameter(typeof(int));
            var expr = Expression.Lambda<Func<int, int>>(
                Expression.Block(
                    Expression.Switch(
                        typeof(void),
                        param,
                        null,
                        null,
                        Expression.SwitchCase(
                            Expression.Assign(param, Expression.Constant(1)),
                            Expression.Constant(0))),
                    param),
                param);
            var func = expr.CompileToTestMethodSimple();
            Assert.Equal(1, func(0));
            Assert.Equal(1, func(1));
            Assert.Equal(2, func(2));
        }

        [Fact]
        public static void SwitchSparseValues_TriggersLambdaCompilerMergeBuckets()
        {
            var param = Expression.Parameter(typeof(int));
            var cases = new[] { 0, 3, 6, 7 }.Select(i =>
                Expression.SwitchCase(
                    Expression.Constant(i + 1),
                    Expression.Constant(i))).ToArray();
            var expr = Expression.Lambda<Func<int, int>>(
                Expression.Switch(
                    param,
                    Expression.Constant(0),
                    cases),
                param);
            var func = expr.CompileToTestMethodSimple();
            Assert.Equal(1, func(0));
            Assert.Equal(0, func(1));
            Assert.Equal(0, func(2));
            Assert.Equal(4, func(3));
            Assert.Equal(0, func(4));
            Assert.Equal(0, func(5));
            Assert.Equal(7, func(6));
            Assert.Equal(8, func(7));
            Assert.Equal(0, func(8));
        }
    }
}
