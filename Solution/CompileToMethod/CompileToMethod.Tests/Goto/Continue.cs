// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class Continue : GotoExpressionTests
    {
        [Fact]
        public void ContinueVoidNoValue()
        {
            LabelTarget target = Expression.Label();
            Expression block = Expression.Block(
                Expression.Continue(target),
                Expression.Throw(Expression.Constant(new InvalidOperationException())),
                Expression.Label(target)
                );
            Expression.Lambda<Action>(block).CompileToTestMethod()();
        }

        [Fact]
        public void ContinueExplicitVoidNoValue()
        {
            LabelTarget target = Expression.Label();
            Expression block = Expression.Block(
                Expression.Continue(target, typeof(void)),
                Expression.Throw(Expression.Constant(new InvalidOperationException())),
                Expression.Label(target)
                );
            Expression.Lambda<Action>(block).CompileToTestMethod()();
        }

        [Fact]
        public void UndefinedLabel()
        {
            Expression<Action> continueNowhere = Expression.Lambda<Action>(Expression.Continue(Expression.Label()));
            Assert.Throws<InvalidOperationException>(() => continueNowhere.CompileToTestMethod());
        }

        [Fact]
        public void AmbiguousJump()
        {
            LabelTarget target = Expression.Label();
            Expression<Action> exp = Expression.Lambda<Action>(
                Expression.Block(
                    Expression.Continue(target),
                    Expression.Block(Expression.Label(target)),
                    Expression.Block(Expression.Label(target))
                    )
                );
            Assert.Throws<InvalidOperationException>(() => exp.CompileToTestMethod());
        }

        [Fact]
        public void MultipleDefinitionsInSeparateBlocks()
        {
            LabelTarget target = Expression.Label();
            Func<int> add = Expression.Lambda<Func<int>>(
                Expression.Add(
                    Expression.Block(
                        Expression.Continue(target),
                        Expression.Throw(Expression.Constant(new Exception())),
                        Expression.Label(target),
                        Expression.Constant(10)
                    ),
                    Expression.Block(
                        Expression.Continue(target),
                        Expression.Throw(Expression.Constant(new Exception())),
                        Expression.Label(target),
                        Expression.Constant(5)
                    )
                )
            ).CompileToTestMethod();
            Assert.Equal(15, add());
        }

        [Fact]
        public void JumpIntoExpression()
        {
            LabelTarget target = Expression.Label();
            Expression<Func<bool>> isInt = Expression.Lambda<Func<bool>>(
                Expression.Block(
                    Expression.Continue(target),
                    Expression.TypeIs(Expression.Label(target), typeof(int))
                )
            );
            Assert.Throws<InvalidOperationException>(() => isInt.CompileToTestMethod());
        }
    }
}
