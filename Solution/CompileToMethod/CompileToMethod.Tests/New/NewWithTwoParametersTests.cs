// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public static class NewWithTwoParametersTests
    {
        #region Test methods

        [Fact]
        public static void CheckNewWithTwoParametersStructWithValueTest()
        {
            int[] array1 = { 0, 1, -1, int.MinValue, int.MaxValue };
            double[] array2 = { 0, 1, -1, double.MinValue, double.MaxValue, double.Epsilon, double.NegativeInfinity, double.PositiveInfinity, double.NaN };
            for (int i = 0; i < array1.Length; i++)
            {
                for (int j = 0; j < array2.Length; j++)
                {
                    VerifyNewWithTwoParametersStructWithValue(array1[i], array2[j]);
                }
            }
        }

        [Fact]
        public static void CheckNewWithTwoParametersCustom2Test()
        {
            int[] array1 = { 0, 1, -1, int.MinValue, int.MaxValue };
            string[] array2 = { null, "", "a", "foo" }; ;
            for (int i = 0; i < array1.Length; i++)
            {
                for (int j = 0; j < array2.Length; j++)
                {
                    VerifyNewWithTwoParametersCustom2(array1[i], array2[j]);
                }
            }
        }

        [Fact]
        public static void CheckNewWithTwoParametersStructWithStringAndValueTest()
        {
            string[] array1 = { null, "", "a", "foo" };
            S[] array2 = { default(S), new S() };
            for (int i = 0; i < array1.Length; i++)
            {
                for (int j = 0; j < array2.Length; j++)
                {
                    VerifyNewWithTwoParametersStructWithStringAndValue(array1[i], array2[j]);
                }
            }
        }

        #endregion

        #region Verifier methods

        private static void VerifyNewWithTwoParametersStructWithValue(int a, double b)
        {
            ConstructorInfo constructor = typeof(Sp).GetConstructor(new[] { typeof(int), typeof(double) });
            Expression[] exprArgs = { Expression.Constant(a, typeof(int)), Expression.Constant(b, typeof(double)) };
            Expression<Func<Sp>> e =
                Expression.Lambda<Func<Sp>>(
                    Expression.New(constructor, exprArgs),
                    Enumerable.Empty<ParameterExpression>());
            Func<Sp> f = e.CompileToTestMethod();

            Assert.Equal(new Sp(a, b), f());
        }

        private static void VerifyNewWithTwoParametersCustom2(int a, string b)
        {
            ConstructorInfo constructor = typeof(D).GetConstructor(new[] { typeof(int), typeof(string) });
            Expression[] exprArgs = { Expression.Constant(a, typeof(int)), Expression.Constant(b, typeof(string)) };
            Expression<Func<D>> e =
                Expression.Lambda<Func<D>>(
                    Expression.New(constructor, exprArgs),
                    Enumerable.Empty<ParameterExpression>());
            Func<D> f = e.CompileToTestMethod();

            Assert.Equal(new D(a, b), f());
        }

        private static void VerifyNewWithTwoParametersStructWithStringAndValue(string a, S b)
        {
            ConstructorInfo constructor = typeof(Scs).GetConstructor(new[] { typeof(string), typeof(S) });
            Expression[] exprArgs = { Expression.Constant(a, typeof(string)), Expression.Constant(b, typeof(S)) };
            Expression<Func<Scs>> e =
                Expression.Lambda<Func<Scs>>(
                    Expression.New(constructor, exprArgs),
                    Enumerable.Empty<ParameterExpression>());
            Func<Scs> f = e.CompileToTestMethod();

            Assert.Equal(new Scs(a, b), f());
        }

        #endregion

        public class TestClass
        {
            public TestClass(string s, int val)
            {
                S = s;
                Val = val;
            }

            public string S;

            public int Val { get; set; }
        }
    }
}
