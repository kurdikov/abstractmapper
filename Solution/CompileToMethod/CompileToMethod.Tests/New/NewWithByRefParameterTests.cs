// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Reflection;
using Xunit;

namespace System.Linq.Expressions.Tests
{
    public class NewWithByRefParameterTests
    {
        public readonly int Always2 = 2;

        public class ByRefNewType
        {
            public ByRefNewType(ref int x, ref int y)
            {
                if (x < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(x));
                }

                ++x;
                y *= x;
            }
        }

        public class OutNewType
        {
            public OutNewType(out int x)
            {
                x = 42;
            }
        }

        private delegate ByRefNewType ByRefNewFactory2(ref int x, ref int y);

        private delegate ByRefNewType ByRefNewFactory1(ref int x);

        private delegate OutNewType OutNewTypeFactory(out int x);

        [Fact]
        public void CreateByRef()
        {
            ParameterExpression pX = Expression.Parameter(typeof(int).MakeByRefType());
            ParameterExpression pY = Expression.Parameter(typeof(int).MakeByRefType());
            ByRefNewFactory2 del =
                Expression.Lambda<ByRefNewFactory2>(
                    Expression.New(typeof(ByRefNewType).GetConstructors()[0], pX, pY), pX, pY).CompileToTestMethod();
            int x = 3;
            int y = 4;
            Assert.NotNull(del(ref x, ref y));
            Assert.Equal(4, x);
            Assert.Equal(16, y);
        }

        private void CreateByRefAliasing()
        {
            ParameterExpression pX = Expression.Parameter(typeof(int).MakeByRefType());
            ParameterExpression pY = Expression.Parameter(typeof(int).MakeByRefType());
            ByRefNewFactory2 del =
                Expression.Lambda<ByRefNewFactory2>(
                    Expression.New(typeof(ByRefNewType).GetConstructors()[0], pX, pY), pX, pY).CompileToTestMethod();
            int x = 3;
            Assert.NotNull(del(ref x, ref x));
            Assert.Equal(16, x);
        }

        [Fact]
        public void CreateByRefAliasingCompiled()
        {
            CreateByRefAliasing();
        }

        [Fact]
        public void CreateByRefReferencingReadonly()
        {
            ParameterExpression p = Expression.Parameter(typeof(int).MakeByRefType());
            ByRefNewFactory1 del =
                Expression.Lambda<ByRefNewFactory1>(
                    Expression.New(
                        typeof(ByRefNewType).GetConstructors()[0],
                        Expression.Field(Expression.Constant(this), "Always2"), p), p).CompileToTestMethod();
            int x = 19;
            Assert.NotNull(del(ref x));
            Assert.Equal(2, Always2);
            Assert.Equal(57, x);
        }

        [Fact]
        public void CreateByRefReferencingOnlyReadonly()
        {
            Func<ByRefNewType> del =
                Expression.Lambda<Func<ByRefNewType>>(
                    Expression.New(
                        typeof(ByRefNewType).GetConstructors()[0],
                        Expression.Field(Expression.Constant(this), "Always2"),
                        Expression.Field(Expression.Constant(this), "Always2"))).CompileToTestMethod();
            Assert.NotNull(del());
            Assert.Equal(2, Always2);
        }

        [Fact]
        public void CreateByRefThrowing()
        {
            ParameterExpression pX = Expression.Parameter(typeof(int).MakeByRefType());
            ParameterExpression pY = Expression.Parameter(typeof(int).MakeByRefType());
            ByRefNewFactory2 del =
                Expression.Lambda<ByRefNewFactory2>(
                    Expression.New(typeof(ByRefNewType).GetConstructors()[0], pX, pY), pX, pY).CompileToTestMethod();
            int x = -9;
            int y = 4;
            AssertExtensions.Throws<ArgumentOutOfRangeException>("x", () => del(ref x, ref y));
        }

        [Fact]
        public void CreateOut()
        {
            ParameterExpression p = Expression.Parameter(typeof(int).MakeByRefType());
            OutNewTypeFactory del =
                Expression.Lambda<OutNewTypeFactory>(Expression.New(typeof(OutNewType).GetConstructors()[0], p), p)
                    .CompileToTestMethod();
            int x;
            Assert.NotNull(del(out x));
            Assert.Equal(42, x);
        }
    }
}
