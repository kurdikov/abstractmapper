﻿using System.Dynamic.Utils;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
    public static class ExpressionExtensions
    {
        public static void CompileToMethod(this LambdaExpression expression, MethodBuilder method)
        {
            ContractUtils.RequiresNotNull(method, nameof(method));
            ContractUtils.Requires(method.IsStatic, nameof(method));
            var type = method.DeclaringType?.GetTypeInfo() as TypeBuilder;
            if (type == null) throw Error.MethodBuilderDoesNotHaveTypeBuilder();

            Compiler.LambdaCompiler.Compile(expression, method);
        }
    }
}
