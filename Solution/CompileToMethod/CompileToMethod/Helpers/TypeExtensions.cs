﻿namespace System.Linq.Expressions
{
    static class TypeExtensions
    {
        public static bool IsSZArray(this Type type)
        {
            if (!type.IsArray || type.GetArrayRank() != 1)
            {
                return false;
            }
            return !type.Name.Contains("*");
        }
    }
}
