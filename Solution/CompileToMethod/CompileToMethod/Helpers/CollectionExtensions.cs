﻿using System.Collections.Generic;

namespace System.Linq.Expressions
{
    static class CollectionExtensions
    {
        public static bool TryPop<T>(this Stack<T> stack, out T value)
        {
            if (stack.Count > 0)
            {
                value = stack.Pop();
                return true;
            }
            value = default(T);
            return false;
        }
    }
}
