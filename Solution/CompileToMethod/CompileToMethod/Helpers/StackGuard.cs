// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.Linq.Expressions
{
    internal sealed class StackGuard
    {
        private const int MaxExecutionStackCount = 1024;

        private int _executionStackCount;

#if NETSTANDARD1_1
        private static Func<bool> _check;

        static StackGuard()
        {
            var method = typeof(RuntimeHelpers).GetTypeInfo().GetMethod("EnsureSufficientExecutionStack", BindingFlags.Public | BindingFlags.Static);
            var exceptionType = typeof(RuntimeHelpers).GetTypeInfo().Assembly.GetType("System.InsufficientExecutionStackException");
            if (method == null || exceptionType == null)
            {
                _check = () => true;
            }
            else
            {
                var expression = Expression.Lambda<Func<bool>>(
                    Expression.TryCatch(
                        Expression.Block(
                            Expression.Call(method),
                            Expression.Constant(true)),
                        Expression.Catch(exceptionType, Expression.Constant(false))
                    ));
                _check = expression.Compile();
            }
        }
#endif

        public bool TryEnterOnCurrentStack()
        {
#if NETSTANDARD1_1
            if (_check())
            {
                return true;
            }
            if (_executionStackCount < MaxExecutionStackCount)
            {
                return false;
            }

            throw new Exception("Insufficient stack to continue executing the program safely. This can happen from having too many functions on the call stack or function on the stack using too much stack space.");
#else
            try
            {
                RuntimeHelpers.EnsureSufficientExecutionStack();
                return true;
            }
            catch (InsufficientExecutionStackException)
            {
            }

            if (_executionStackCount < MaxExecutionStackCount)
            {
                return false;
            }

            throw new InsufficientExecutionStackException();
#endif
        }

        public void RunOnEmptyStack<T1, T2, T3>(Action<T1, T2, T3> action, T1 arg1, T2 arg2, T3 arg3)
        {
            RunOnEmptyStackCore(s =>
            {
                var t = (Tuple<Action<T1, T2, T3>, T1, T2, T3>)s;
                t.Item1(t.Item2, t.Item3, t.Item4);
                return default(object);
            }, Tuple.Create(action, arg1, arg2, arg3));
        }

        public R RunOnEmptyStack<T1, T2, R>(Func<T1, T2, R> action, T1 arg1, T2 arg2)
        {
            return RunOnEmptyStackCore(s =>
            {
                var t = (Tuple<Func<T1, T2, R>, T1, T2>)s;
                return t.Item1(t.Item2, t.Item3);
            }, Tuple.Create(action, arg1, arg2));
        }

        public R RunOnEmptyStack<T1, T2, T3, R>(Func<T1, T2, T3, R> action, T1 arg1, T2 arg2, T3 arg3)
        {
            return RunOnEmptyStackCore(s =>
            {
                var t = (Tuple<Func<T1, T2, T3, R>, T1, T2, T3>)s;
                return t.Item1(t.Item2, t.Item3, t.Item4);
            }, Tuple.Create(action, arg1, arg2, arg3));
        }

        private R RunOnEmptyStackCore<R>(Func<object, R> action, object state)
        {
            _executionStackCount++;

            try
            {
                // Using default scheduler rather than picking up the current scheduler.
                Task<R> task = Task.Factory.StartNew(action, state, CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);

                TaskAwaiter<R> awaiter = task.GetAwaiter();

                // Avoid AsyncWaitHandle lazy allocation of ManualResetEvent in the rare case we finish quickly.
                if (!awaiter.IsCompleted)
                {
                    // Task.Wait has the potential of inlining the task's execution on the current thread; avoid this.
                    ((IAsyncResult)task).AsyncWaitHandle.WaitOne();
                }

                // Using awaiter here to unwrap AggregateException.
                return awaiter.GetResult();
            }
            finally
            {
                _executionStackCount--;
            }
        }
    }
}
