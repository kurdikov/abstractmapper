﻿using System.Diagnostics;
using System.Dynamic.Utils;
using System.Reflection;
using System.Runtime.CompilerServices;
using static System.Linq.Expressions.CachedReflectionInfo;

namespace System.Linq.Expressions
{
    internal static class TypeBinaryExpressionExtensions
    {
        internal static Expression ReduceTypeEqual(this TypeBinaryExpression expression)
        {
            Type cType = expression.Expression.Type;

            if (cType.GetTypeInfo().IsValueType || expression.TypeOperand.IsPointer)
            {
                if (cType.IsNullableType())
                {
                    // If the expression type is a nullable type, it will match if
                    // the value is not null and the type operand
                    // either matches or is its type argument (T to its T?).
                    if (cType.GetNonNullableType() != expression.TypeOperand.GetNonNullableType())
                    {
                        return Expression.Block(expression.Expression, Utils.Constant(value: false));
                    }
                    else
                    {
                        return Expression.NotEqual(expression.Expression, Expression.Constant(null, expression.Expression.Type));
                    }
                }
                else
                {
                    // For other value types (including Void), we can
                    // determine the result now
                    return Expression.Block(expression.Expression, Utils.Constant(cType == expression.TypeOperand.GetNonNullableType()));
                }
            }

            Debug.Assert(TypeUtils.AreReferenceAssignable(typeof(object), expression.Expression.Type), "Expecting reference types only after this point.");

            // Can check the value right now for constants.
            if (expression.Expression.NodeType == ExpressionType.Constant)
            {
                return expression.ReduceConstantTypeEqual();
            }

            // expression is a ByVal parameter. Can safely reevaluate.
            var parameter = expression.Expression as ParameterExpression;
            if (parameter != null && !parameter.IsByRef)
            {
                return expression.ByValParameterTypeEqual(parameter);
            }

            // Create a temp so we only evaluate the left side once
            parameter = Expression.Parameter(typeof(object));

            return Expression.Block(
                new TrueReadOnlyCollection<ParameterExpression>(parameter),
                new TrueReadOnlyCollection<Expression>(
                    Expression.Assign(parameter, expression.Expression),
                    expression.ByValParameterTypeEqual(parameter)
                )
            );
        }

        // Helper that is used when re-eval of LHS is safe.
        private static Expression ByValParameterTypeEqual(this TypeBinaryExpression expression, ParameterExpression value)
        {
            Expression getType = Expression.Call(value, Object_GetType);

            // In remoting scenarios, obj.GetType() can return an interface.
            // But JIT32's optimized "obj.GetType() == typeof(ISomething)" codegen,
            // causing it to always return false.
            // We workaround this optimization by generating different, less optimal IL
            // if TypeOperand is an interface.
            if (expression.TypeOperand.GetTypeInfo().IsInterface)
            {
                ParameterExpression temp = Expression.Parameter(typeof(Type));
                getType = Expression.Block(
                    new TrueReadOnlyCollection<ParameterExpression>(temp),
                    new TrueReadOnlyCollection<Expression>(
                        Expression.Assign(temp, getType),
                        temp
                    )
                );
            }

            // We use reference equality when comparing to null for correctness
            // (don't invoke a user defined operator), and reference equality
            // on types for performance (so the JIT can optimize the IL).
            return Expression.AndAlso(
                Expression.ReferenceNotEqual(value, Utils.Null),
                Expression.ReferenceEqual(
                    getType,
                    Expression.Constant(expression.TypeOperand.GetNonNullableType(), typeof(Type))
                )
            );
        }

        private static Expression ReduceConstantTypeEqual(this TypeBinaryExpression expression)
        {
            ConstantExpression ce = expression.Expression as ConstantExpression;
            //TypeEqual(null, T) always returns false.
            if (ce.Value == null)
            {
                return Utils.Constant(value: false);
            }
            else
            {
                return Utils.Constant(expression.TypeOperand.GetNonNullableType() == ce.Value.GetType());
            }
        }
    }
}
