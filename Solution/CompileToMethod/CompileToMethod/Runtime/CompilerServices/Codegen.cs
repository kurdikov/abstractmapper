﻿#if !NETSTANDARD2_0
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Runtime.CompilerServices
{
    internal static class Codegen
    {
        public static MethodInfo CreateEmptyRuntimeVariables { get; private set; }
        public static MethodInfo CreateRuntimeVariables { get; private set; }

        static Codegen()
        {
            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(
                new AssemblyName("CompileToMethod.RuntimeVariables"),
                AssemblyBuilderAccess.Run);
            var moduleBuilder = assemblyBuilder.DefineDynamicModule("CompileToMethod.RuntimeVariables");

            var @interface = Expression.RuntimeVariables().Type;
            if (!@interface.GetTypeInfo().IsInterface)
            {
                throw new InvalidOperationException($"{@interface} is not an interface");
            }
            if (!@interface.GetTypeInfo().IsPublic)
            {
                throw new InvalidOperationException($"{@interface} is not public");
            }

            var emptyRuntimeVariablesTypeBuilder = moduleBuilder.DefineType(
                nameof(CTM_RuntimeOps.EmptyRuntimeVariables),
                TypeAttributes.Public | TypeAttributes.Sealed,
                typeof(CTM_RuntimeOps.EmptyRuntimeVariables));
            emptyRuntimeVariablesTypeBuilder.AddInterfaceImplementation(@interface);
            var emptyRuntimeVariablesCtor = emptyRuntimeVariablesTypeBuilder.DefineDefaultConstructor(MethodAttributes.Public);
            var createEmptyRuntimeVariables = emptyRuntimeVariablesTypeBuilder.DefineMethod(
                "Create",
                MethodAttributes.Public | MethodAttributes.Static | MethodAttributes.HideBySig,
                emptyRuntimeVariablesTypeBuilder.AsType(),
                ArrayHelper.Empty<Type>());
            var createEmptyRuntimeVariablesIlgen = createEmptyRuntimeVariables.GetILGenerator();
            createEmptyRuntimeVariablesIlgen.Emit(OpCodes.Newobj, emptyRuntimeVariablesCtor);
            createEmptyRuntimeVariablesIlgen.Emit(OpCodes.Ret);
            var emptyRuntimeVariablesType = emptyRuntimeVariablesTypeBuilder.CreateTypeInfo();
            CreateEmptyRuntimeVariables = emptyRuntimeVariablesType.GetMethod(
                "Create",
                BindingFlags.Public | BindingFlags.Static);

            var runtimeVariableListTypeBuilder = moduleBuilder.DefineType(
                nameof(CTM_RuntimeOps.RuntimeVariableList),
                TypeAttributes.Public | TypeAttributes.Sealed,
                typeof(CTM_RuntimeOps.RuntimeVariableList));
            runtimeVariableListTypeBuilder.AddInterfaceImplementation(@interface);
            var ctorParamTypes = new[] { typeof(object[]), typeof(long[]) };
            var runtimeVariableListCtor = runtimeVariableListTypeBuilder.DefineConstructor(
                MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName | MethodAttributes.HideBySig,
                CallingConventions.Standard,
                ctorParamTypes);
            var parentCtor = typeof(CTM_RuntimeOps.RuntimeVariableList).GetTypeInfo().GetConstructor(ctorParamTypes);
            var runtimeVariableListCtorIlgen = runtimeVariableListCtor.GetILGenerator();
            runtimeVariableListCtorIlgen.Emit(OpCodes.Ldarg_0);
            runtimeVariableListCtorIlgen.Emit(OpCodes.Ldarg_1);
            runtimeVariableListCtorIlgen.Emit(OpCodes.Ldarg_2);
            runtimeVariableListCtorIlgen.Emit(OpCodes.Call, parentCtor);
            runtimeVariableListCtorIlgen.Emit(OpCodes.Ret);
            var createRuntimeVariableList = runtimeVariableListTypeBuilder.DefineMethod(
                "Create",
                MethodAttributes.Public | MethodAttributes.Static,
                runtimeVariableListTypeBuilder.AsType(),
                ctorParamTypes);
            var createRuntimeVariableListIlgen = createRuntimeVariableList.GetILGenerator();
            createRuntimeVariableListIlgen.Emit(OpCodes.Ldarg_0);
            createRuntimeVariableListIlgen.Emit(OpCodes.Ldarg_1);
            createRuntimeVariableListIlgen.Emit(OpCodes.Newobj, runtimeVariableListCtor);
            createRuntimeVariableListIlgen.Emit(OpCodes.Ret);
            var runtimeVariableListType = runtimeVariableListTypeBuilder.CreateTypeInfo();
            CreateRuntimeVariables = runtimeVariableListType.GetMethod(
                "Create",
                BindingFlags.Public | BindingFlags.Static);
        }
    }
}
#endif
