﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;

namespace System.Linq.Expressions.Compiler
{
    internal partial class StackSpiller
    {
        /// <summary>
        /// Creates a special block that is marked as not allowing jumps in.
        /// This should not be used for rewriting BlockExpression itself, or
        /// anything else that supports jumping.
        /// </summary>
        private static Expression MakeBlock(params Expression[] expressions)
        {
            return new SpilledExpressionBlock(expressions);
        }

        /// <summary>
        /// Creates a special block that is marked as not allowing jumps in.
        /// This should not be used for rewriting BlockExpression itself, or
        /// anything else that supports jumping.
        /// </summary>
        private static Expression MakeBlock(IReadOnlyList<Expression> expressions)
        {
            return new SpilledExpressionBlock(expressions);
        }
    }

    internal static class BlockHelper
    {
        public static BlockExpression GetBlock(Expression expression)
        {
            if (expression is SpilledExpressionBlock spilledBlock)
            {
                return spilledBlock.SpilledBlock;
            }
            return (BlockExpression)expression;
        }
    }

    /// <summary>
    /// A special subtype of BlockExpression that indicates to the compiler
    /// that this block is a spilled expression and should not allow jumps in.
    /// </summary>
    internal sealed class SpilledExpressionBlock : Expression
    {
        private readonly BlockExpression _block;

        internal SpilledExpressionBlock(IReadOnlyList<Expression> expressions)
        {
            _block = Expression.Block(expressions);
        }

        public BlockExpression SpilledBlock => _block;

        protected override Expression Accept(ExpressionVisitor visitor)
        {
            return visitor.Visit(_block);
        }

        public override Type Type => _block.Type;

        public override ExpressionType NodeType => ExpressionType.Block;
    }
}
