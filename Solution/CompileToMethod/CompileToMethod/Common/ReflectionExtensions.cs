﻿#if NETSTANDARD1_1 || NETSTANDARD1_3
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;

#if NETSTANDARD1_1
namespace System
{
    internal enum TypeCode
    {
        Boolean = 3,
        Byte = 6,
        Char = 4,
        DateTime = 16,
        Decimal = 15,
        Double = 14,
        Empty = 0,
        Int16 = 7,
        Int32 = 9,
        Int64 = 11,
        Object = 1,
        SByte = 5,
        Single = 13,
        String = 18,
        UInt16 = 8,
        UInt32 = 10,
        UInt64 = 12,
    }
}

namespace System.Linq.Expressions
{
    internal class IArgumentProvider
    {
        private readonly ReadOnlyCollection<Expression> _arguments;

        public int ArgumentCount => _arguments.Count;

        public Expression GetArgument(int i) => _arguments[i];

        protected IArgumentProvider(ReadOnlyCollection<Expression> arguments)
        {
            _arguments = arguments ?? new ReadOnlyCollection<Expression>(new Expression[0]);
        }

        public static implicit operator IArgumentProvider(IndexExpression expression)
        {
            return new IArgumentProvider(expression.Arguments);
        }

        public static implicit operator IArgumentProvider(MethodCallExpression expression)
        {
            return new IArgumentProvider(expression.Arguments);
        }

        public static implicit operator IArgumentProvider(InvocationExpression expression)
        {
            return new IArgumentProvider(expression.Arguments);
        }

        public static implicit operator IArgumentProvider(NewExpression expression)
        {
            return new IArgumentProvider(expression.Arguments);
        }

        public static implicit operator IArgumentProvider(ElementInit expression)
        {
            return new IArgumentProvider(expression.Arguments);
        }
    }
}
#endif

namespace System.Reflection
{
    [Flags]
    internal enum BindingFlags
    {
        DeclaredOnly = 2,
        Instance = 4,
        NonPublic = 32,
        Public = 16,
        Static = 8,
    }

    internal static class ReflectionExtensions
    {
        public static TypeCode GetTypeCode(this Type type)
        {
            if (type == null)
                return TypeCode.Empty;
            else if (type == typeof(bool))
                return TypeCode.Boolean;
            else if (type == typeof(char))
                return TypeCode.Char;
            else if (type == typeof(sbyte))
                return TypeCode.SByte;
            else if (type == typeof(byte))
                return TypeCode.Byte;
            else if (type == typeof(short))
                return TypeCode.Int16;
            else if (type == typeof(ushort))
                return TypeCode.UInt16;
            else if (type == typeof(int))
                return TypeCode.Int32;
            else if (type == typeof(uint))
                return TypeCode.UInt32;
            else if (type == typeof(long))
                return TypeCode.Int64;
            else if (type == typeof(ulong))
                return TypeCode.UInt64;
            else if (type == typeof(float))
                return TypeCode.Single;
            else if (type == typeof(double))
                return TypeCode.Double;
            else if (type == typeof(decimal))
                return TypeCode.Decimal;
            else if (type == typeof(System.DateTime))
                return TypeCode.DateTime;
            else if (type == typeof(string))
                return TypeCode.String;
            else if (type.GetTypeInfo().IsEnum)
                return GetTypeCode(Enum.GetUnderlyingType(type));
            else
                return TypeCode.Object;
        }

        public static bool IsAssignableFrom(this TypeInfo type, Type from)
        {
            return type.IsAssignableFrom(from.GetTypeInfo());
        }

        public static bool IsEquivalentTo(this TypeInfo type, Type to)
        {
            return type.AsType() == to;
        }

        private static bool Match(ParameterInfo[] parameters, Type[] paramTypes)
        {
            if (parameters.Length != paramTypes.Length)
            {
                return false;
            }
            for (int i = 0; i < parameters.Length; ++i)
            {
                if (parameters[i].ParameterType != paramTypes[i])
                {
                    return false;
                }
            }
            return true;
        }

        public static ConstructorInfo GetConstructor(this TypeInfo type, Type[] paramTypes)
        {
            foreach (var ctor in type.DeclaredConstructors)
            {
                var parameters = ctor.GetParameters();
                if (Match(parameters, paramTypes))
                {
                    return ctor;
                }
            }
            return null;
        }

        public static FieldInfo GetField(this TypeInfo type, string name)
        {
            foreach (var field in type.DeclaredFields)
            {
                if (field.Name == name)
                {
                    return field;
                }
            }
            return null;
        }

        public static MethodInfo GetMethod(this TypeInfo type, string name)
        {
            foreach (var method in type.DeclaredMethods)
            {
                if (method.Name == name)
                {
                    return method;
                }
            }
            return null;
        }

        public static MethodInfo GetMethod(this TypeInfo type, string name, Type[] paramTypes)
        {
            foreach (var method in type.DeclaredMethods)
            {
                if (method.Name == name && Match(method.GetParameters(), paramTypes))
                {
                    return method;
                }
            }
            return null;
        }

        private static bool HasNot(this BindingFlags flags, BindingFlags flag) => (flags & flag) == 0;

        private static bool Match(MethodInfo method, BindingFlags bindingFlags)
        {
            if (!method.IsStatic && bindingFlags.HasNot(BindingFlags.Instance))
            {
                return false;
            }
            if (method.IsStatic && bindingFlags.HasNot(BindingFlags.Static))
            {
                return false;
            }
            if (method.IsPublic && bindingFlags.HasNot(BindingFlags.Public))
            {
                return false;
            }
            if (!method.IsPublic && bindingFlags.HasNot(BindingFlags.NonPublic))
            {
                return false;
            }
            return true;
        }

        public static MethodInfo GetMethod(this TypeInfo type, string name, BindingFlags bindingFlags)
        {
            MethodInfo result = null;
            foreach (var method in type.DeclaredMethods)
            {
                if (method.Name == name && Match(method, bindingFlags))
                {
                    if (result != null)
                    {
                        throw new AmbiguousMatchException($"Found multiple methods for {type}.GetMethod({name}, {bindingFlags})");
                    }
                    result = method;
                }
            }
            if (bindingFlags.HasNot(BindingFlags.DeclaredOnly))
            {
                while (type.BaseType != null)
                {
                    type = type.BaseType.GetTypeInfo();
                    foreach (var method in type.DeclaredMethods)
                    {
                        if (method.Name == name && Match(method, bindingFlags))
                        {
                            if (result != null)
                            {
                                throw new AmbiguousMatchException($"Found multiple methods for {type}.GetMethod({name}, {bindingFlags})");
                            }
                            result = method;
                        }
                    }
                }
            }
            return result;
        }

        public static MemberInfo[] GetMember(this TypeInfo type, string name)
        {
            var builder = new List<MemberInfo>();
            foreach (var member in type.DeclaredMembers)
            {
                if (member.Name == name)
                {
                    builder.Add(member);
                }
            }
            return builder.ToArray();
        }

        public static MethodInfo[] GetMethods(this TypeInfo type, BindingFlags bindingFlags)
        {
            var result = new List<MethodInfo>();
            foreach (var method in type.DeclaredMethods)
            {
                if (Match(method, bindingFlags))
                {
                    result.Add(method);
                }
            }
            if (bindingFlags.HasNot(BindingFlags.DeclaredOnly))
            {
                while (type.BaseType != null)
                {
                    type = type.BaseType.GetTypeInfo();
                    foreach (var method in type.DeclaredMethods)
                    {
                        if (Match(method, bindingFlags))
                        {
                            result.Add(method);
                        }
                    }
                }
            }
            return result.ToArray();
        }

        public static Type[] GetGenericArguments(this TypeInfo type)
        {
            // https://stackoverflow.com/a/19504096/6729916
            if (type.IsGenericTypeDefinition)
            {
                return type.GenericTypeParameters;
            }
            return type.GenericTypeArguments;
        }

        public static MethodInfo GetGetMethod(this PropertyInfo property, bool nonPublic)
        {
            //https://stackoverflow.com/a/20241132/6729916
            Debug.Assert(nonPublic);
            return property.GetMethod;
        }

        public static MethodInfo GetSetMethod(this PropertyInfo property, bool nonPublic)
        {
            //https://stackoverflow.com/a/20241132/6729916
            Debug.Assert(nonPublic);
            return property.SetMethod;
        }

        public static object GetRawConstantValue(this FieldInfo field)
        {
            return field.GetValue(null);
        }
    }
}
#endif
