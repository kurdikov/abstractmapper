﻿namespace System.Collections.Generic
{
    internal static class ArrayHelper
    {
        public static T[] Empty<T>()
        {
#if !NETSTANDARD1_1
            return Array.Empty<T>();
#else
            return new T[0];
#endif
        }

        public static Type[] EmptyTypes
        {
            get
            {
#if !NETSTANDARD1_1
                return Type.EmptyTypes;
#else
                return Empty<Type>();
#endif
            }
        }
    }
}
