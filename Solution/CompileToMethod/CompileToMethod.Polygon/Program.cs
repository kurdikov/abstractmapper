﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;

namespace CompileToMethod.Polygon
{
    public class Program
    {
        static void Main(string[] args)
        {
            var assemblyName = new AssemblyName("Polygon");
            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            var moduleBuilder = assemblyBuilder.DefineDynamicModule("Polygon");
            var typeBuilder = moduleBuilder.DefineType("PolygonType");
            typeBuilder.DefineDefaultConstructor(MethodAttributes.Public);
            var methodBuilder = typeBuilder.DefineMethod("PolygonMethod", MethodAttributes.Public | MethodAttributes.Static);

            Expression<Func<int>> expr = () => Woo();
            expr.CompileToMethod(methodBuilder);

            var typeInfo = typeBuilder.CreateTypeInfo();
            var type = typeInfo.AsType();
            var method = type.GetMethod("PolygonMethod");
            var result = method.Invoke(null, new object[0]);
        }

        public static int Woo() => 42;
    }
}
