﻿namespace TsSoft.Expressions.Helpers.Entity.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using BaseTestClasses.Database;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers.Entity.Attributes;
    using TsSoft.Expressions.Models;

    [TestClass]
    public class EntityDependencyHelperTests
    {
        [NotNull]
        private EntityDependencyHelper _helper = new EntityDependencyHelper(EntityStaticHelpers.EntityTypesHelper);

        [TestMethod]
        public void NoHardDependenciesWithSoft()
        {
            var deps = _helper.GetHardDependencies(typeof(First)).ToList();
            Assert.AreEqual(0, deps.Count);
        }

        [TestMethod]
        public void OneHardDependencyWithSameSoft()
        {
            var deps = _helper.GetHardDependencies(typeof(Second)).ToList();
            Assert.AreEqual(1, deps.Count);
            Assert.IsTrue(deps.Contains(typeof(First)));
        }

        [TestMethod]
        public void TwoHardDependenciesWithNoSoft()
        {
            var deps = _helper.GetHardDependencies(typeof(SecondWithFkPk)).ToList();
            Assert.AreEqual(2, deps.Count);
            Assert.IsTrue(deps.Contains(typeof(First)));
            Assert.IsTrue(deps.Contains(typeof(ThirdWithPkPk)));
        }

        [TestMethod]
        public void OneHardDependencyWithDifferentSoft()
        {
            var deps = _helper.GetHardDependencies(typeof(DiamondOne)).ToList();
            Assert.AreEqual(1, deps.Count);
            Assert.IsTrue(deps.Contains(typeof(First)));
        }

        [NotNull]
        private ValueHoldingMember Member<T>(Expression<Func<T, object>> expr)
        {
            return EntityStaticHelpers.MemberInfo.GetValueHoldingMember(expr);
        }

        [TestMethod]
        public void TestIsNavigationalToChildrenProperty()
        {
            Assert.IsTrue(_helper.IsNavigationalToChildrenProperty(Member<First>(f => f.Seconds), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(First))));
            Assert.IsTrue(_helper.IsNavigationalToChildrenProperty(Member<First>(f => f.SecondWithFkPks), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(First))));
            Assert.IsFalse(_helper.IsNavigationalToChildrenProperty(Member<First>(f => f.Zero), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(First))));
            Assert.IsFalse(_helper.IsNavigationalToChildrenProperty(Member<WeirdView>(f => f.ViewEntryParents), new List<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>>()));
            Assert.IsFalse(_helper.IsNavigationalToChildrenProperty(Member<SecondWithFkPk>(f => f.Third), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(SecondWithFkPk))));
            Assert.IsFalse(_helper.IsNavigationalToChildrenProperty(Member<ThirdWithPkPk>(f => f.Second), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(ThirdWithPkPk))));
        }

        [TestMethod]
        public void TestIsNavigationalToSingleChildProperty()
        {
            Assert.IsFalse(_helper.IsNavigationalToSingleChildProperty(Member<First>(f => f.Seconds), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(First))));
            Assert.IsFalse(_helper.IsNavigationalToSingleChildProperty(Member<First>(f => f.SecondWithFkPks), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(First))));
            Assert.IsFalse(_helper.IsNavigationalToSingleChildProperty(Member<First>(f => f.Zero), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(First))));
            Assert.IsFalse(_helper.IsNavigationalToSingleChildProperty(Member<WeirdView>(f => f.ViewEntryParents), new List<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>>()));
            Assert.IsFalse(_helper.IsNavigationalToSingleChildProperty(Member<SecondWithFkPk>(f => f.Third), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(SecondWithFkPk))));
            Assert.IsTrue(_helper.IsNavigationalToSingleChildProperty(Member<ThirdWithPkPk>(f => f.Second), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(ThirdWithPkPk))));
        }

        [TestMethod]
        public void TestIsNavigationalToParentProperty()
        {
            Assert.IsFalse(_helper.IsNavigationalToParentProperty(Member<First>(f => f.Seconds), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(First))));
            Assert.IsFalse(_helper.IsNavigationalToParentProperty(Member<First>(f => f.SecondWithFkPks), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(First))));
            Assert.IsTrue(_helper.IsNavigationalToParentProperty(Member<First>(f => f.Zero), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(First))));
            Assert.IsTrue(_helper.IsNavigationalToParentProperty(Member<WeirdView>(f => f.ViewEntryParents), new List<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>>()));
            Assert.IsTrue(_helper.IsNavigationalToParentProperty(Member<SecondWithFkPk>(f => f.Third), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(SecondWithFkPk))));
            Assert.IsFalse(_helper.IsNavigationalToParentProperty(Member<ThirdWithPkPk>(f => f.Second), EntityStaticHelpers.EntityTypesHelper.GetForeignKeys(typeof(ThirdWithPkPk))));
        }
        class WeirdView
        {
            [NavigationToParent]
            public ICollection<First> ViewEntryParents { get; set; }
        }
    }
}
