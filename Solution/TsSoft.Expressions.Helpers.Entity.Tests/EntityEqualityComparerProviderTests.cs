﻿namespace TsSoft.Expressions.Helpers.Entity.Tests
{
    using System;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.EntityRepository.Interfaces;

    [TestClass]
    public class EntityEqualityComparerProviderTests
    {
        [NotNull]
        private EntityEqualityComparerProvider _provider = new EntityEqualityComparerProvider();

        [TestMethod]
        public void TestGetComparerForEntityIntId()
        {
            var comparer = _provider.GetEqualityComparer<EntityIntId>();
            Assert.IsNotNull(comparer);
            Assert.IsInstanceOfType(comparer, typeof(EntityComparer<EntityIntId, int>));
        }

        [TestMethod]
        public void TestGetComparerForEntityGuidId()
        {
            var comparer = _provider.GetEqualityComparer<EntityGuidId>();
            Assert.IsNotNull(comparer);
            Assert.IsInstanceOfType(comparer, typeof(EntityComparer<EntityGuidId, Guid>));
        }

        [TestMethod]
        public void TestGetComparerForEntityStringId()
        {
            var comparer = _provider.GetEqualityComparer<EntityStringId>();
            Assert.IsNotNull(comparer);
            Assert.IsInstanceOfType(comparer, typeof(EntityComparer<EntityStringId, string>));
        }

        [TestMethod]
        public void TestGetComparerForNonEntity()
        {
            var comparer = _provider.GetEqualityComparer<NonEntity>();
            Assert.IsNull(comparer);
        }

        public class EntityIntId : IEntityWithId<int>
        {
            public int Id { get; set; }
        }
        public class EntityGuidId : IEntityWithId<Guid>
        {
            public Guid Id { get; set; }
        }
        public class EntityStringId : IEntityWithId<string>
        {
            public string Id { get; set; }
        }
        public class NonEntity
        {
            public int Id { get; set; }
        }
    }
}
