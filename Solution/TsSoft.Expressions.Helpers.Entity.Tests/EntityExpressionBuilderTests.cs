﻿namespace TsSoft.Expressions.Helpers.Entity.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using BaseTestClasses;
    using BaseTestClasses.Database;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    [TestClass]
    public class EntityExpressionBuilderTests
    {
        [NotNull]
        private EntityExpressionBuilder _builder = new EntityExpressionBuilder(
            EntityStaticHelpers.EntityTypesHelper, new CollectionReflectionHelper(EntityStaticHelpers.MemberInfo));

        [TestMethod]
        public void TestGetGetByIdLambdaGenericSimplePk()
        {
            Expression<Func<First, bool>> expected = f => f.Id == 1;

            LambdaExpression actual = _builder.GetGetByIdLambda<First>(new object[] { 1 });
            ExprAssert.AreEqual(expected, actual);

            actual = _builder.GetGetByIdLambda(typeof(First), new object[] { 1 });
            ExprAssert.AreEqual(expected, actual);

            actual = _builder.GetGetByIdLambda(
                typeof(First), ValueHoldingMember.GetValueHoldingMember(typeof(First), "Id"), 1);
            ExprAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetGetByIdLambdaGenericCompositePk()
        {
            var id1 = Guid.Parse("11111111-1111-1111-1111-111111111111");
            var id2 = Guid.Parse("22222222-2222-2222-2222-222222222222");
            Expression<Func<CompositePk, bool>> expected = f => f.Part1 == id1 && f.Part2 == id2;

            LambdaExpression actual = _builder.GetGetByIdLambda<CompositePk>(new object[] { id1, id2 });
            ExprAssert.AreEqualWithConstants(expected, actual);
        
            actual = _builder.GetGetByIdLambda(typeof(CompositePk), new object[] { id1, id2 });
            ExprAssert.AreEqualWithConstants(expected, actual);

            actual = _builder.GetGetByIdLambda(
                typeof(CompositePk),
                new[]
                {
                    ValueHoldingMember.GetValueHoldingMember(typeof(CompositePk), "Part1"),
                    ValueHoldingMember.GetValueHoldingMember(typeof(CompositePk), "Part2")
                }, new object[] {id1, id2});
            ExprAssert.AreEqualWithConstants(expected, actual);
        }

        [TestMethod]
        public void TestGetIncludeLambda()
        {
            var actual = _builder.GetIncludeLambda(
                typeof(First), EntityStaticHelpers.MemberInfo.GetValueHoldingMember((First f) => f.Zero));
            Expression<Func<First, object>> expected = f => f.Zero;
            ExprAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetIncludeLambdas()
        {
            var actual = _builder.GetIncludeLambdas(
                typeof(First), new[]
                {
                    EntityStaticHelpers.MemberInfo.GetValueHoldingMember((First f) => f.Zero),
                    EntityStaticHelpers.MemberInfo.GetValueHoldingMember((First f) => f.Seconds),
                }) as IReadOnlyCollection<Expression<Func<First, object>>>;
            Assert.IsNotNull(actual);
            Assert.AreEqual(2, actual.Count);
            IncludeAssert.Contains(f => f.Zero, actual);
            IncludeAssert.Contains(f => f.Seconds, actual);
        }

        [TestMethod]
        public void TestGetAllIncludeLambdas()
        {
            var actual = _builder.GetAllIncludes(typeof(DiamondBottom)) as IReadOnlyCollection<Expression<Func<DiamondBottom, object>>>;
            Assert.IsNotNull(actual);
            Assert.AreEqual(3, actual.Count);
            IncludeAssert.Contains(f => f.First, actual);
            IncludeAssert.Contains(f => f.DiamondOnes, actual);
            IncludeAssert.Contains(f => f.DiamondTwos, actual);
        }
    }
}
