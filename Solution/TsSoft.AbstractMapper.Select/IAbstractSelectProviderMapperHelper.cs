﻿namespace TsSoft.AbstractMapper
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Хелпер для маппера, отдающего селект
    /// </summary>
    public interface IAbstractSelectProviderMapperHelper : IAbstractMapperHelper
    {
        /// <summary>
        /// Построить селект по использованным путям
        /// </summary>
        /// <typeparam name="TFrom">Тип, из которого осуществляется преобразование</typeparam>
        /// <param name="paths">Описание маппинга</param>
        /// <param name="additionalSelect">Дополнительный селект</param>
        /// <param name="context">Контекст генерации</param>
        /// <param name="settings">Настройки генерации селекта</param>
        SelectExpression<TFrom> GetSelectFromPaths<TFrom>(
            [NotNull]IEnumerable<MappedPathDescription> paths, 
            IReadOnlySelectExpression<TFrom> additionalSelect = null, 
            GeneratorContext context = null,
            SelectSettings settings = null);
    }
}
