﻿namespace TsSoft.AbstractMapper
{
    using System.Collections.Generic;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Маппер, отдающий необходимые ему селект
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    /// <typeparam name="TModel">Тип модели</typeparam>
    public interface ISelectProviderMapper<TEntity, out TModel> : IMapper<TEntity, TModel>, ISelectProvider<TEntity>
    {
    }

    /// <summary>
    /// Маппер, отдающий необходимые ему селект
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    /// <typeparam name="TModel">Тип модели</typeparam>
    public interface ISelectProviderCollectionMapper<TEntity, out TModel> : IMapper<IEnumerable<TEntity>, TModel>,
                                                                            ISelectProvider<TEntity>
    {
    }
}