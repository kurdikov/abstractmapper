﻿namespace TsSoft.AbstractMapper
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Маппер, отдающий необходимый ему селект
    /// </summary>
    /// <typeparam name="TFrom">Тип, из которого осуществляется преобразование</typeparam>
    /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
    public class SelectProviderMapper<TFrom, TTo> : Mapper<TFrom, TTo>, ISelectProviderMapper<TFrom, TTo>, ICyclePreventingSelectGenerator<TFrom> 
        where TFrom : class
    {
        [NotNull]private readonly Lazy<SelectExpression<TFrom>> _select;

        /// <summary>
        /// Маппер, отдающий необходимый ему селект
        /// </summary>
        public SelectProviderMapper([NotNull]IAbstractSelectProviderMapperHelper mapperHelper)
            : base(mapperHelper)
        {
            _select = new Lazy<SelectExpression<TFrom>>(() => GenerateSelect(new GeneratorContext(GetType(), MaxNestingLevel), SelectSettings));
        }

        /// <summary>
        /// Минимальный селект, необходимый для обработки сущности
        /// </summary>
        public virtual IReadOnlySelectExpression<TFrom> Select { get { return GetLazyValueAndClearCacheIfProcessed(_select); } }

        /// <summary>
        /// Добавленные в обход MapRules селекты
        /// </summary>
        protected virtual IReadOnlySelectExpression<TFrom> AdditionalSelect { get { return null; }}

        /// <summary>
        /// Вся ли необходимая информация получена из построенного выражения
        /// </summary>
        protected override bool IsExpressionProcessed
        {
            get { return base.IsExpressionProcessed && _select.IsValueCreated; }
        }

        /// <summary>
        /// Сгенерировать селект в заданном контексте
        /// </summary>
        private SelectExpression<TFrom> GenerateSelect(GeneratorContext context, SelectSettings settings)
        {
            try
            {
                return _select.IsValueCreated// || (context == null || !context.RemainingNestingLevel.ContainsKey(GetType()))
                    ? _select.Value
                    : ((IAbstractSelectProviderMapperHelper) MapperHelper).GetSelectFromPaths(
                        MapperExpression.MappedPaths, AdditionalSelect, context, settings);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    string.Format("Unable to build select expression for {0}", GetType()),
                    ex);
            }
        }

        /// <summary>
        /// Сгенерировать селект в заданном контексте
        /// </summary>
        public SelectExpression<TFrom> GenerateSelect(GeneratorContext context)
        {
            return GenerateSelect(context, null);
        }

        /// <summary>
        /// Максимальное количество вложений генератора селекта в себя
        /// </summary>
        public virtual int MaxNestingLevel { get { return 0; } }

        /// <summary>
        /// Настройки генерации селекта
        /// </summary>
        [NotNull]
        protected virtual SelectSettings SelectSettings {get {return new SelectSettings();}}
    }
}
