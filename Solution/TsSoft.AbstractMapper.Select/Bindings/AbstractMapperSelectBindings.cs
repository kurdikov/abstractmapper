﻿namespace TsSoft.Bindings
{
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Engine;
    using TsSoft.BindingsDescription;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.AbstractMapper.Select
    /// </summary>
    public class AbstractMapperSelectBindings : BindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке TsSoft.AbstractMapper.Select
        /// </summary>
        public AbstractMapperSelectBindings()
        {
            Bind<IAbstractSelectProviderMapperHelper, AbstractSelectProviderMapperHelper>();
            Bind<ISelectExpressionCreator, SelectExpressionCreator>();
            Bind<IMapperSelectTreeExtractor, MapperSelectTreeExtractor>();
        }
    }
}
