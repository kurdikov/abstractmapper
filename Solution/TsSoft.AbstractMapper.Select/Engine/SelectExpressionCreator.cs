﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Helpers;
    using TsSoft.Expressions.SelectBuilder.Models;

    internal class SelectExpressionCreator : ISelectExpressionCreator
    {
        [NotNull]private readonly IParsedPathsToSelectTreeConverter _pathConverter;
        [NotNull]private readonly ISelectTreeExtractor _selectedTreeExtractor;
        [NotNull]private readonly IMapperSelectTreeExtractor _mapperSelectTreeExtractor;
        [NotNull]private readonly ISelectTreeToSelectConverter _selectTreeConverter;

        public SelectExpressionCreator(
            [NotNull] IParsedPathsToSelectTreeConverter pathConverter,
            [NotNull] ISelectTreeExtractor selectedTreeExtractor,
            [NotNull] IMapperSelectTreeExtractor mapperSelectTreeExtractor,
            [NotNull] ISelectTreeToSelectConverter selectTreeConverter)
        {
            if (pathConverter == null) throw new ArgumentNullException("pathConverter");
            if (selectedTreeExtractor == null) throw new ArgumentNullException("selectedTreeExtractor");
            if (mapperSelectTreeExtractor == null) throw new ArgumentNullException("mapperSelectTreeExtractor");
            if (selectTreeConverter == null) throw new ArgumentNullException("selectTreeConverter");
            _pathConverter = pathConverter;
            _selectedTreeExtractor = selectedTreeExtractor;
            _mapperSelectTreeExtractor = mapperSelectTreeExtractor;
            _selectTreeConverter = selectTreeConverter;
        }

        private KeyValuePair<ParsedPath, SelectTree> ExtractTrees([NotNull]MappedPathDescription path, [NotNull]GeneratorContext context)
        {
            SelectTree tree = null;
            if (path.MapperDescription != null)
            {
                tree = _mapperSelectTreeExtractor.GetSelectedTree(path.MapperDescription, context);
            }
            else if (path.ProcessorDescription != null)
            {
                tree = _selectedTreeExtractor.GetSelectTree(path.ProcessorDescription, context);
            }
            return new KeyValuePair<ParsedPath, SelectTree>(path.Path, tree);
        }

        public SelectExpression<TEntity> Create<TEntity>(IEnumerable<MappedPathDescription> paths, GeneratorContext context, SelectSettings settings = null)
        {
            var rawPaths = paths.Where(p => p != null).Select(p => ExtractTrees(p, context));
            var rawTree = _pathConverter.Convert(typeof(TEntity), rawPaths);
            return _selectTreeConverter.TransformAndConvert<TEntity>(rawTree, settings ?? new SelectSettings());
        }
    }
}
