namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Helpers;

    class MapperSelectTreeExtractor : IMapperSelectTreeExtractor
    {
        [NotNull] private readonly IMapperResolver _mapperResolver;
        [NotNull] private readonly ISelectTreeExtractor _extractor;

        [NotNull] private readonly MethodInfo _tryGetSelectTree;

        public MapperSelectTreeExtractor(
            [NotNull] IMapperResolver mapperResolver,
            [NotNull] ISelectTreeExtractor extractor,
            [NotNull] IMemberInfoHelper memberInfo)
        {
            if (mapperResolver == null) throw new ArgumentNullException("mapperResolver");
            if (extractor == null) throw new ArgumentNullException("extractor");
            if (memberInfo == null) throw new ArgumentNullException("memberInfo");
            _mapperResolver = mapperResolver;
            _extractor = extractor;

            _tryGetSelectTree = memberInfo.GetGenericDefinitionMethodInfo(() => TryGetSelectTree<object, object, object>(null));
        }

        private SelectTree TryGetSelectTree<TInnerFrom, TInnerFromSingle, TInnerTo>(GeneratorContext context)
        {
            var mapper = _mapperResolver.TryGet<TInnerFrom, TInnerTo>();
            return _extractor.GetSelectTree(typeof(TInnerFromSingle), mapper, context);
        }

        private SelectTree TryGetSelectTree([NotNull]MapperDescription description, GeneratorContext context)
        {
            var method = _tryGetSelectTree.MakeGenericMethod(
                description.MapperFromType,
                description.MapperFromSingleType,
                description.MapperToType);
            var @delegate = DelegateCreator.Create<Func<GeneratorContext, SelectTree>>(this, method);
            return @delegate(context);
        }

        public SelectTree GetSelectedTree(MapperDescription description, GeneratorContext context)
        {
            if (description == null)
            {
                return null;
            }
            return description.MapperObject == null
                ? TryGetSelectTree(description, context)
                : _extractor.GetSelectTree(description.MapperFromSingleType, description.MapperObject, context);
        }
    }
}
