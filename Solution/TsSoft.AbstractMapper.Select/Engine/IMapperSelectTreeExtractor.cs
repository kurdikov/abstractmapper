﻿namespace TsSoft.AbstractMapper.Engine
{
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Получает по описанию маппера дерево выбранных свойств
    /// </summary>
    public interface IMapperSelectTreeExtractor
    {
        /// <summary>
        /// Получить дерево выбранных свойств для маппера
        /// </summary>
        /// <param name="description">Описание маппера</param>
        /// <param name="context">Контекст генерации</param>
        [CanBeNull]
        SelectTree GetSelectedTree(MapperDescription description, GeneratorContext context);
    }
}
