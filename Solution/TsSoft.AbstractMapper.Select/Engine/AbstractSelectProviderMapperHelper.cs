﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules.Factories;
    using TsSoft.Expressions.Helpers.Closures;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.SelectBuilder;
    using TsSoft.Expressions.SelectBuilder.Models;

    internal class AbstractSelectProviderMapperHelper : AbstractMapperHelper, IAbstractSelectProviderMapperHelper
    {
        [NotNull] private readonly ISelectExpressionCreator _selectCreator;
        [NotNull] private readonly ISelectHelper _selectHelper;

        public AbstractSelectProviderMapperHelper(
            [NotNull] IMapExpressionCreator expressionCreator,
            [NotNull] IIgnoreRulesFactory ignoreRulesFactory,
            [NotNull] ILambdaCompiler lambdaCompiler,
            [NotNull] IFullMapRulesCreator fullMapRulesCreator,
            [NotNull] ISelectExpressionCreator selectCreator,
            [NotNull] ISelectHelper selectHelper)
            : base(expressionCreator, ignoreRulesFactory, lambdaCompiler, fullMapRulesCreator)
        {
            if (selectCreator == null) throw new ArgumentNullException("selectCreator");
            if (selectHelper == null) throw new ArgumentNullException("selectHelper");
            _selectCreator = selectCreator;
            _selectHelper = selectHelper;
        }

        public SelectExpression<TFrom> GetSelectFromPaths<TFrom>(
            IEnumerable<MappedPathDescription> paths, 
            IReadOnlySelectExpression<TFrom> additionalSelect = null,
            GeneratorContext context = null,
            SelectSettings settings = null)
        {
            var result = _selectCreator.Create<TFrom>(paths, context ?? new GeneratorContext(), settings);
            return additionalSelect != null ? _selectHelper.Merge(result, additionalSelect) : result;
        }
    }
}
