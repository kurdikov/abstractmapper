﻿namespace TsSoft.AbstractMapper.Engine
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Создатель select-выражения для маппера
    /// </summary>
    public interface ISelectExpressionCreator
    {
        /// <summary>
        /// Создать select-выражение по использованным преобразователем путям
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности БД</typeparam>
        /// <param name="paths">Описания использованных путей</param>
        /// <param name="context">Контекст генерации</param>
        /// <param name="settings">Настройки генерации селекта</param>
        [NotNull]
        SelectExpression<TEntity> Create<TEntity>([NotNull]IEnumerable<MappedPathDescription> paths, [NotNull]GeneratorContext context, SelectSettings settings);
    }
}
