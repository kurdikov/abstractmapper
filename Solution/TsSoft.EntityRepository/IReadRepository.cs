﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    ///     Отвечает за чтение сущностей из хранилища
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IReadRepository<TEntity>
    {
        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        IReadOnlyCollection<TEntity> Get(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        IReadOnlyCollection<TEntity> GetFromSpecifiedContext(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        IReadOnlyCollection<TEntity> Get(
            Expression<Func<TEntity, bool>> where,
            [NotNull]IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null);

        /// <summary>
        ///     Получить количество сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Количество сущностей по предикату</returns>
        int Count(Expression<Func<TEntity, bool>> where);

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Сущность</returns>
        [CanBeNull]
        TEntity GetSingle(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <returns>Сущность</returns>
        [CanBeNull]
        TEntity GetSingle(
            Expression<Func<TEntity, bool>> where,
            [NotNull]IReadOnlySelectExpression<TEntity> select);

        /// <summary>
        ///     Существует ли сущность в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        bool Exists(Expression<Func<TEntity, bool>> where);

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        IPage<TEntity> GetPaged(
            Expression<Func<TEntity, bool>> where,
            int pageNumber,
            int pageSize,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            IEnumerable<IOrderByClause<TEntity>> orderBy,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        IPage<TEntity> GetPaged(
            Expression<Func<TEntity, bool>> where,
            int pageNumber,
            int pageSize,
            [NotNull]IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy);

        /// <summary>
        ///     Получить отслеживаемые EF сущности
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        [NotNull]
        IReadOnlyCollection<TEntity> GetTracked(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Максимальное значение поля или null</returns>
        T? Max<T>(
            [NotNull]Expression<Func<TEntity, T>> selector,
            Expression<Func<TEntity, bool>> where) where T : struct;

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Минимальное значение поля или null</returns>
        T? Min<T>(
            [NotNull]Expression<Func<TEntity, T>> selector,
            Expression<Func<TEntity, bool>> where) where T : struct;


        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        [ItemNotNull]
        Task<IReadOnlyCollection<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        [ItemNotNull]
        Task<IReadOnlyCollection<TEntity>> GetFromSpecifiedContextAsync(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        [ItemNotNull]
        Task<IReadOnlyCollection<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> where,
            [NotNull] IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null);

        /// <summary>
        ///     Получить количество сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - количество сущностей по предикату</returns>
        [NotNull]
        Task<int> CountAsync(Expression<Func<TEntity, bool>> where);

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Задача, результат которой - сущность</returns>
        [NotNull]
        [ItemCanBeNull]
        Task<TEntity> GetSingleAsync(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <returns>Задача, результат которой - сущность</returns>
        [NotNull]
        [ItemCanBeNull]
        Task<TEntity> GetSingleAsync(
            Expression<Func<TEntity, bool>> where,
            [NotNull]IReadOnlySelectExpression<TEntity> select);

        /// <summary>
        ///     Существует ли сущность в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        [NotNull]
        Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> where);

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        [ItemNotNull]
        Task<IPage<TEntity>> GetPagedAsync(
            Expression<Func<TEntity, bool>> where,
            int pageNumber,
            int pageSize,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            IEnumerable<IOrderByClause<TEntity>> orderBy,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        [ItemNotNull]
        Task<IPage<TEntity>> GetPagedAsync(
            Expression<Func<TEntity, bool>> where,
            int pageNumber,
            int pageSize,
            IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy);

        /// <summary>
        ///     Получить отслеживаемые EF сущности
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        [NotNull]
        [ItemNotNull]
        Task<IReadOnlyCollection<TEntity>> GetTrackedAsync(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - максимальное значение поля или null</returns>
        [NotNull]
        Task<T?> MaxAsync<T>(
            [NotNull]Expression<Func<TEntity, T>> selector,
            Expression<Func<TEntity, bool>> where) where T : struct;

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - минимальное значение поля или null</returns>
        [NotNull]
        Task<T?> MinAsync<T>(
            [NotNull]Expression<Func<TEntity, T>> selector,
            Expression<Func<TEntity, bool>> where) where T : struct;
    }
}
