﻿namespace TsSoft.EntityRepository
{
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    ///     Репозиторий удаляемых сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IDeletableEntityRepository<TEntity> : 
        IDeletableEntityDeleteRepository<TEntity>,
        IDeletableEntityReadRepository<TEntity>,
        IBaseRepository<TEntity> where TEntity : class, IDeletable
    {
        
    }
}