﻿namespace TsSoft.EntityRepository.Factory
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository;
    using TsSoft.EntityRepository.External;
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    /// Фабрика репозиториев
    /// </summary>
    public interface IRepositoryFactory
    {
        /// <summary>
        /// Получить репозиторий
        /// </summary>
        /// <param name="type">Тип сущности</param>
        [NotNull]
        object GetBaseRepository(Type type);

        /// <summary>
        /// Получить репозиторий сложных сущностей
        /// </summary>
        /// <param name="type">Тип сущности</param>
        [NotNull]
        object GetComplexEntityRepository(Type type);

        /// <summary>
        /// Получить репозиторий удаляемых сущностей
        /// </summary>
        /// <param name="type">Тип сущности</param>
        [NotNull]
        object GetDeletableEntityRepository(Type type);

        /// <summary>
        /// Получить репозиторий внешних сущностей
        /// </summary>
        /// <param name="type">Тип сущности</param>
        [NotNull]
        object GetExternalRepository(Type type);

        /// <summary>
        /// Получить репозиторий
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        [NotNull]
        IBaseRepository<TEntity> GetBaseRepository<TEntity>() where TEntity: class;

        /// <summary>
        /// Получить репозиторий сложных сущностей
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        [NotNull]
        IComplexEntityRepository<TEntity> GetComplexEntityRepository<TEntity>() where TEntity : class, IComplexEntity;

        /// <summary>
        /// Получить репозиторий сложных сущностей
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        [NotNull]
        IDeletableEntityRepository<TEntity> GetDeletableEntityRepository<TEntity>() where TEntity : class, IDeletable;

        /// <summary>
        /// Получить репозиторий внешних сущностей
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <typeparam name="TId">Тип идентификатора</typeparam>
        [NotNull]
        IExternalRepository<TEntity, TId> GetExternalRepository<TEntity, TId>() where TEntity : class, IExternalEntity<TId>;
    }
}
