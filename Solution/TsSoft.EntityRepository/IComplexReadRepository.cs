﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    ///     Репозиторий получения сложных сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IComplexReadRepository<TEntity> : IReadRepository<TEntity>
            where TEntity : class, IComplexEntity
    {
        /// <summary>
        ///     Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="include">Инклюды</param>
        /// <param name="includeConditions">Кондишены</param>
        TEntity GetSingleWithStubs(Expression<Func<TEntity, bool>> where,
                                   IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                   IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="select">Селект</param>
        TEntity GetSingleWithStubs(
                Expression<Func<TEntity, bool>> where,
                IReadOnlySelectExpression<TEntity> select);

        /// <summary>
        ///     Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="include">Инклюды</param>
        /// <param name="includeConditions">Кондишены</param>
        [NotNull]
        [ItemCanBeNull]
        Task<TEntity> GetSingleWithStubsAsync(Expression<Func<TEntity, bool>> where,
                                              IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                              IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="select">Селект</param>
        [NotNull]
        [ItemCanBeNull]
        Task<TEntity> GetSingleWithStubsAsync(
                Expression<Func<TEntity, bool>> where,
                IReadOnlySelectExpression<TEntity> select);

        /// <summary>
        ///     Получить коллекцию сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        IReadOnlyCollection<TEntity> GetWithStubs(Expression<Func<TEntity, bool>> where,
                                                  IReadOnlySelectExpression<TEntity> select,
                                                  IEnumerable<IOrderByClause<TEntity>> orderBy = null);

        /// <summary>
        ///     Получить коллекцию сущностей (включая несохраненные)
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        IReadOnlyCollection<TEntity> GetWithStubs(Expression<Func<TEntity, bool>> where,
                                                  IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                  IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                  IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);


        /// <summary>
        ///     Получить коллекцию сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        Task<IReadOnlyCollection<TEntity>> GetWithStubsAsync(Expression<Func<TEntity, bool>> where,
                                                             IReadOnlySelectExpression<TEntity> select,
                                                             IEnumerable<IOrderByClause<TEntity>> orderBy = null);

        /// <summary>
        ///     Получить количество сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Количество сущностей по предикату</returns>
        int CountWithStubs(Expression<Func<TEntity, bool>> where);

        /// <summary>
        ///     Существует ли сущность в хранилище, возможно, несохраненная
        /// </summary>
        /// <param name="where">Предикат</param>
        bool ExistsWithStubs(Expression<Func<TEntity, bool>> where);


        /// <summary>
        ///     Получить пагинированную коллекцию сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        IPage<TEntity> GetPagedWithStubs(
                Expression<Func<TEntity, bool>> where,
                int pageNumber,
                int pageSize,
                IReadOnlySelectExpression<TEntity> select,
                IEnumerable<IOrderByClause<TEntity>> orderBy);

        /// <summary>
        ///     Получить отслеживаемые EF сущности с несохраненными
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какий подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        [NotNull]
        IReadOnlyCollection<TEntity> GetTrackedWithStubs(
                Expression<Func<TEntity, bool>> where,
                IEnumerable<Expression<Func<TEntity, object>>> include = null,
                IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище (включая несохраненные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Максимальное значение поля или null</returns>
        T? MaxWithStubs<T>(
                Expression<Func<TEntity, T>> selector,
                Expression<Func<TEntity, bool>> where) where T : struct;

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище (включая несохраненные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Минимальное значение поля или null</returns>
        T? MinWithStubs<T>(
                Expression<Func<TEntity, T>> selector,
                Expression<Func<TEntity, bool>> where) where T : struct;

        /// <summary>
        ///     Получить количество сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - количество сущностей по предикату</returns>
        [NotNull]
        Task<int> CountWithStubsAsync(Expression<Func<TEntity, bool>> where);

        /// <summary>
        ///     Существует ли сущность в хранилище, возможно, несохраненная
        /// </summary>
        /// <param name="where">Предикат</param>
        [NotNull]
        Task<bool> ExistsWithStubsAsync(Expression<Func<TEntity, bool>> where);

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        Task<IPage<TEntity>> GetPagedWithStubsAsync(
                Expression<Func<TEntity, bool>> where,
                int pageNumber,
                int pageSize,
                IReadOnlySelectExpression<TEntity> select,
                IEnumerable<IOrderByClause<TEntity>> orderBy);

        /// <summary>
        ///     Получить отслеживаемые EF сущности с несохраненными
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        [NotNull]
        Task<IReadOnlyCollection<TEntity>> GetTrackedWithStubsAsync(
                Expression<Func<TEntity, bool>> where,
                IEnumerable<Expression<Func<TEntity, object>>> include = null,
                IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище (включая несохраненные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - максимальное значение поля или null</returns>
        [NotNull]
        Task<T?> MaxWithStubsAsync<T>(
                [NotNull]Expression<Func<TEntity, T>> selector,
                Expression<Func<TEntity, bool>> where) where T : struct;

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище (включая несохраненные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - минимальное значение поля или null</returns>
        [NotNull]
        Task<T?> MinWithStubsAsync<T>(
                [NotNull]Expression<Func<TEntity, T>> selector,
                Expression<Func<TEntity, bool>> where) where T : struct;

        /// <summary>
        ///     Получить коллекцию сущностей (включая несохраненные)
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        Task<IReadOnlyCollection<TEntity>> GetWithStubsAsync(
                Expression<Func<TEntity, bool>> where,
                IEnumerable<Expression<Func<TEntity, object>>> include = null,
                IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);
    }
}