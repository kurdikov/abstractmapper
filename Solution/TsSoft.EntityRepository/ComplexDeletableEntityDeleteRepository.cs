﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.EntityRepository.IncludeProcessors;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Async;

    /// <summary>
    ///     Репозиторий удаления сложных удаляемых сущностей
    /// </summary>
    public class ComplexDeletableEntityDeleteRepository<TEntity> : DeleteRepository<TEntity>, IComplexDeletableEntityDeleteRepository<TEntity>
        where TEntity : class, IComplexEntity, IDeletable
    {
        [NotNull] private readonly IRepositoryHelper _repositoryHelper;
        [NotNull] private readonly IUpdateRepository<TEntity> _updateRepository;

        /// <summary>
        ///     Репозиторий удаления сложных удаляемых сущностей
        /// </summary>
        public ComplexDeletableEntityDeleteRepository(
            [NotNull] IItemWrapper<DbContext> contextWrapper,
            [NotNull] IUniversalIncludeProcessor universalIncludeProcessor,
            [NotNull] IDatabaseCommitter databaseCommitter,
            [NotNull] ITransactor transactor,
            [NotNull] IDetacher detacher,
            [NotNull] IRepositoryHelper repositoryHelper,
            [NotNull] IUpdateRepository<TEntity> updateRepository)
            : base(contextWrapper, universalIncludeProcessor, databaseCommitter, transactor, detacher)
        {
            if (repositoryHelper == null) throw new ArgumentNullException("repositoryHelper");
            if (updateRepository == null) throw new ArgumentNullException("updateRepository");
            _repositoryHelper = repositoryHelper;
            _updateRepository = updateRepository;
        }

        /// <summary>
        ///     Удалить все несохранённые сущности
        /// </summary>
        public IReadOnlyCollection<TEntity> DeleteStubs()
        {
            return AsyncHelper.RunSync(DeleteStubsAsync);
        }

        /// <summary>
        ///     Удалить сущности
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        public override IReadOnlyCollection<TEntity> Delete(Expression<Func<TEntity, bool>> where,
                                                                       bool commit = true)
        {
            return _updateRepository.Update(entity => entity.IsDeleted = true, _repositoryHelper.WhereToNonStubWhere(where),
                                                 commit: commit);
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Задача, результат которой - удалённая сущность</returns>
        public override TEntity DeleteSingle(Expression<Func<TEntity, bool>> where, bool commit = true)
        {
            return _updateRepository.UpdateSingle(entity => entity.IsDeleted = true, _repositoryHelper.WhereToNonStubWhere(where),
                                                       commit: commit);
        }

        /// <summary>
        ///     Удалить сущности
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        public override Task<IReadOnlyCollection<TEntity>> DeleteAsync(Expression<Func<TEntity, bool>> where,
                                                                       bool commit = true)
        {
            return _updateRepository.UpdateAsync(entity => entity.IsDeleted = true, _repositoryHelper.WhereToNonStubWhere(where),
                                                 commit: commit);
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Задача, результат которой - удалённая сущность</returns>
        public override Task<TEntity> DeleteSingleAsync(Expression<Func<TEntity, bool>> where, bool commit = true)
        {
            return _updateRepository.UpdateSingleAsync(entity => entity.IsDeleted = true, _repositoryHelper.WhereToNonStubWhere(where),
                                                       commit: commit);
        }

        /// <summary>
        ///     Отменить удаление сущностей
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Изменённые сущности</returns>
        public IReadOnlyCollection<TEntity> Undelete(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return AsyncHelper.RunSync(() => UndeleteAsync(where, commit));
        }

        /// <summary>
        ///     Отменить удаление сущности
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Изменённая сущность</returns>
        public TEntity UndeleteSingle(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return AsyncHelper.RunSync(() => UndeleteSingleAsync(where, commit));
        }

        /// <summary>
        ///     Отменить удаление сущностей
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Задача, результат которой - изменённые сущности</returns>
        public Task<IReadOnlyCollection<TEntity>> UndeleteAsync(Expression<Func<TEntity, bool>> where, bool commit = true)
        {
            return _updateRepository.UpdateAsync(entity => entity.IsDeleted = false,
                                                 _repositoryHelper.WhereToNonStubWhere(where).And(e => e.IsDeleted), commit: commit);
        }

        /// <summary>
        ///     Отменить удаление сущности
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Задача, результат которой - изменённая сущность</returns>
        public Task<TEntity> UndeleteSingleAsync(Expression<Func<TEntity, bool>> where, bool commit = true)
        {
            return _updateRepository.UpdateSingleAsync(entity => entity.IsDeleted = false,
                                                       _repositoryHelper.WhereToNonStubWhere(where).And(e => e.IsDeleted),
                                                       commit: commit);
        }

        /// <summary>
        ///     Удалить все несохранённые сущности
        /// </summary>
        public Task<IReadOnlyCollection<TEntity>> DeleteStubsAsync()
        {
            return base.DeleteAsync(e => !e.IsSaved);
        }
    }
}