﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    ///     Отвечает за обновление сущностей в хранилище
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IUpdateRepository<TEntity> : IDatabaseCommitter, IContextedTransactor
    {
        /// <summary>
        ///     Обновить сущности
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на каждой из сущностей, которые попадут под предикат
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Коллекция обновлённых сущностей</returns>
        IReadOnlyCollection<TEntity> Update(
            Action<TEntity> updater,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            bool commit = true);

        /// <summary>
        ///     Обновить одну сущность
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на сущности
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Обновлённая сущность</returns>
        TEntity UpdateSingle(
            Action<TEntity> updater,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            bool commit = true);

        /// <summary>
        ///     Обновить одну сущность другой сущностью
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на каждой из сущностей, которые попадут под предикат
        ///     где первый параметр - updateSource, а второй - сущность в хранилище
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="updateSource">Чем обновлять</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Обновлённая сущность</returns>
        TEntity UpdateSingleFrom(
            Action<TEntity, TEntity> updater,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            TEntity updateSource,
            bool commit = true);


        /// <summary>
        ///     Обновить сущности
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на каждой из сущностей, которые попадут под предикат
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Задача, результат которой - коллекция обновлённых сущностей</returns>
        [NotNull]
        Task<IReadOnlyCollection<TEntity>> UpdateAsync(
            Action<TEntity> updater,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            bool commit = true);

        /// <summary>
        ///     Обновить одну сущность
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на сущности
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Задача, результат которой - обновлённая сущность</returns>
        [NotNull]
        Task<TEntity> UpdateSingleAsync(
            Action<TEntity> updater,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            bool commit = true);

        /// <summary>
        ///     Обновить одну сущность другой сущностью
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на каждой из сущностей, которые попадут под предикат
        ///     где первый параметр - updateSource, а второй - сущность в хранилище
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="updateSource">Чем обновлять</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Задача, результат которой - обновлённая сущность</returns>
        [NotNull]
        Task<TEntity> UpdateSingleFromAsync(
            Action<TEntity, TEntity> updater,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            TEntity updateSource,
            bool commit = true);

        /// <summary>
        ///     Обновить одну сущность другой сущностью
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на каждой из сущностей, которые попадут под предикат
        ///     где первый параметр - updateSource, а второй - сущность в хранилище
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="updateSource">Чем обновлять</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Задача, результат которой - обновлённая сущность</returns>
        [NotNull]
        Task<TEntity> UpdateSingleFromAsync(
            Func<TEntity, TEntity, IEnumerable<Func<Task>>> updater,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            TEntity updateSource,
            bool commit = true);
    }
}
