﻿namespace TsSoft.EntityRepository
{
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    ///     Осуществляет прямые sql-запросы на чтение
    /// </summary>
    public interface IRawSqlReadRepository
    {
        /// <summary>
        ///     Сделать запрос, возвращающий данные
        /// </summary>
        /// <typeparam name="T">Тип объекта, в который мапятся возвращаемые данные</typeparam>
        /// <param name="sql">SQL-запрос</param>
        /// <param name="parameters">Параметры запроса</param>
        IReadOnlyCollection<T> Query<T>([NotNull] string sql, [NotNull] IEnumerable<IDataParameter> parameters);

        /// <summary>
        ///     Сделать запрос, возвращающий данные
        /// </summary>
        /// <typeparam name="T">Тип объекта, в который мапятся возвращаемые данные</typeparam>
        /// <param name="sql">SQL-запрос</param>
        /// <param name="parameters">Параметры запроса</param>
        [NotNull]
        Task<IReadOnlyCollection<T>> QueryAsync<T>([NotNull] string sql, [NotNull] IEnumerable<IDataParameter> parameters);
    }
}