﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Data;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore.Storage;
#endif
    using JetBrains.Annotations;

    /// <summary>
    ///     Транзакция, реализующая проброс транзакции Ef в <see cref="ITsSoftDatabaseTransaction" />
    /// </summary>
    public class TsSoftDbContextTransaction : ITsSoftDatabaseTransaction
    {
#if NET45
        [NotNull] private readonly DbContextTransaction _efTransaction;
#elif NETSTANDARD15
        [NotNull] private readonly IDbContextTransaction _efTransaction;
#endif
        private readonly IsolationLevel _level;

        /// <summary>
        ///     Транзакция, реализующая проброс транзакции Ef в <see cref="ITsSoftDatabaseTransaction" />
        /// </summary>
        public TsSoftDbContextTransaction(
#if NET45
            [NotNull] DbContextTransaction efTransaction,
#elif NETSTANDARD15
            [NotNull] IDbContextTransaction efTransaction,
#endif
            IsolationLevel level)
        {
            if (efTransaction == null) throw new ArgumentNullException("efTransaction");
            _efTransaction = efTransaction;
            _level = level;
        }

        private IDbTransaction DbTransaction
        {
#if NET45
            get { return _efTransaction.UnderlyingTransaction; }
#elif NETSTANDARD15
            get { return _efTransaction.GetDbTransaction(); }
#endif
        }

        /// <summary>
        ///     Выполняет определяемые приложением задачи, связанные с высвобождением или сбросом неуправляемых ресурсов.
        /// </summary>
        public void Dispose()
        {
            _efTransaction.Dispose();
        }

        /// <summary>
        ///     Фиксирует транзакцию базы данных.
        /// </summary>
        /// <exception cref="T:System.Exception">При попытке фиксации транзакции возникла ошибка.</exception>
        /// <exception cref="T:System.InvalidOperationException">
        ///     Транзакция уже зафиксирована или произошел ее откат. -или-
        ///     Подключение разорвано.
        /// </exception>
        public void Commit()
        {
            _efTransaction.Commit();
        }

        /// <summary>
        ///     Откатывает транзакцию, которая находится в состоянии ожидания.
        /// </summary>
        /// <exception cref="T:System.Exception">При попытке фиксации транзакции возникла ошибка.</exception>
        /// <exception cref="T:System.InvalidOperationException">
        ///     Транзакция уже зафиксирована или произошел ее откат. -или-
        ///     Подключение разорвано.
        /// </exception>
        public void Rollback()
        {
            _efTransaction.Rollback();
        }

        /// <summary>
        ///     Задает объект Connection, который должен быть связан с транзакцией.
        /// </summary>
        /// <returns>
        ///     Объект Connection, который должен быть связан с транзакцией.
        /// </returns>
        public IDbConnection Connection
        {
            get
            {
                var t = DbTransaction;
                return t == null ? null : t.Connection;
            }
        }

        /// <summary>
        ///     Задает объект <see cref="T:System.Data.IsolationLevel" /> для этой транзакции.
        /// </summary>
        /// <returns>
        ///     Объект <see cref="T:System.Data.IsolationLevel" /> для этой транзакции. Значение по умолчанию: ReadCommitted.
        /// </returns>
        public IsolationLevel IsolationLevel
        {
            get
            {
                var dbTransaction = DbTransaction;
                return dbTransaction == null ? _level : dbTransaction.IsolationLevel;
            }
        }
    }
}