﻿namespace TsSoft.EntityRepository
{
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using JetBrains.Annotations;

    /// <summary>
    ///     Инкапсулирует CRUD для сущности и сохранение изменений в бд
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IBaseRepository<TEntity> :
        ICreateRepository<TEntity>,
        IReadRepository<TEntity>,
        IUpdateRepository<TEntity>,
        IDeleteRepository<TEntity>,
        IRawSqlRepository
    {
        /// <summary>
        /// EF-контекст
        /// </summary>
        [NotNull]
        DbContext Context { get; }
    }
}
