﻿namespace TsSoft.EntityRepository
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    ///     Репозиторий удаления сложных сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IComplexDeleteRepository<TEntity> : IDeleteRepository<TEntity>
        where TEntity : class, IComplexEntity
    {
        /// <summary>
        ///     Удалить все несохранённые сущности
        /// </summary>
        IReadOnlyCollection<TEntity> DeleteStubs();


        /// <summary>
        ///     Удалить все несохранённые сущности
        /// </summary>
        Task<IReadOnlyCollection<TEntity>> DeleteStubsAsync();
    }
}