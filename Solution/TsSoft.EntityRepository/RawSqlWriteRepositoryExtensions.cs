﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace TsSoft.EntityRepository
{
    /// <summary>
    /// Расширения репозитория
    /// </summary>
    public static class RawSqlWriteRepositoryExtensions
    {
        /// <summary>
        ///     Выполнить запрос
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        /// <param name="sql">Запрос</param>
        /// <param name="parameters">Параметры запроса</param>
        /// <returns>Количество обработанных запросом строк</returns>
        public static int Execute(
            this IRawSqlWriteRepository repository,
            [NotNull] string sql,
            [NotNull] IEnumerable<IDataParameter> parameters)
        {
            return repository.Execute(sql, parameters, IsolationLevel.ReadUncommitted);
        }

        /// <summary>
        ///     Выполнить запрос
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        /// <param name="sql">Запрос</param>
        /// <param name="parameters">Параметры запроса</param>
        /// <returns>Задача, результат которой - количество обработанных запросом строк</returns>
        [NotNull]
        public static Task<int> ExecuteAsync(
            this IRawSqlWriteRepository repository,
            [NotNull] string sql,
            [NotNull] IEnumerable<IDataParameter> parameters)
        {
            return repository.ExecuteAsync(sql, parameters, IsolationLevel.ReadUncommitted);
        }
    }
}
