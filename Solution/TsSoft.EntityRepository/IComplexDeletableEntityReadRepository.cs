﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    ///     Репозиторий чтения сложных удаляемых сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IComplexDeletableEntityReadRepository<TEntity> : IComplexReadRepository<TEntity>, IDeletableEntityReadRepository<TEntity>
        where TEntity : class, IComplexEntity, IDeletable
    {
        /// <summary>
        ///     Получить коллекцию сущностей с удаленными и несохраненными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        IReadOnlyCollection<TEntity> GetWithStubsAndDeleted(Expression<Func<TEntity, bool>> where,
                                                    IReadOnlySelectExpression<TEntity> select,
                                                    IEnumerable<IOrderByClause<TEntity>> orderBy = null);

        /// <summary>
        /// Получить сущность, возможно, несохранённую и удаленную
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="select">Селект</param>
        TEntity GetSingleWithStubsAndDeleted(
            Expression<Func<TEntity, bool>> where,
            IReadOnlySelectExpression<TEntity> select);

        /// <summary>
        /// Получить сущность, возможно, несохранённую и удаленную
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="select">Селект</param>
        [NotNull]
        Task<TEntity> GetSingleWithStubsAndDeletedAsync(
            Expression<Func<TEntity, bool>> where,
            IReadOnlySelectExpression<TEntity> select);

        /// <summary>
        ///     Получить коллекцию сущностей с удаленными и несохраненными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        Task<IReadOnlyCollection<TEntity>> GetWithStubsAndDeletedAsync(Expression<Func<TEntity, bool>> where,
                                                               IReadOnlySelectExpression<TEntity> select,
                                                               IEnumerable<IOrderByClause<TEntity>> orderBy = null);

    }
}