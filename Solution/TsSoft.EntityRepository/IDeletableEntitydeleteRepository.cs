﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    ///     Репозиторий удаления удаляемых сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IDeletableEntityDeleteRepository<TEntity> : IDeleteRepository<TEntity> where TEntity : class, IDeletable
    {
        /// <summary>
        ///     Отменить удаление сущностей
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Изменённые сущности</returns>
        [NotNull]
        [ItemNotNull]
        IReadOnlyCollection<TEntity> Undelete(Expression<Func<TEntity, bool>> where, bool commit = true);

        /// <summary>
        ///     Отменить удаление сущности
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Изменённая сущность</returns>
        [NotNull]
        [ItemCanBeNull]
        TEntity UndeleteSingle(Expression<Func<TEntity, bool>> where, bool commit = true);

        /// <summary>
        ///     Отменить удаление сущностей
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Задача, результат которой - изменённые сущности</returns>
        [NotNull]
        [ItemNotNull]
        Task<IReadOnlyCollection<TEntity>> UndeleteAsync(Expression<Func<TEntity, bool>> where, bool commit = true);

        /// <summary>
        ///     Отменить удаление сущности
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Задача, результат которой - изменённая сущность</returns>
        [NotNull]
        [ItemCanBeNull]
        Task<TEntity> UndeleteSingleAsync(Expression<Func<TEntity, bool>> where, bool commit = true);
    }
}
