﻿namespace TsSoft.EntityRepository.Helpers
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Models;

    internal static class IqueryableExtensions
    {
        private static FlatPathParser IncludeParser = new FlatPathParser();

        [NotNull]
        public static IQueryable<T> Include<T>(this IQueryable<T> source, IEnumerable<IncludeDescription<T>> paths)
            where T: class
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (paths == null)
            {
                return source;
            }
            var result = source;
            foreach (var path in paths)
            {
                if (path != null && path.DbInclude != null)
                {
#if NET45
                    result = result.Include(path.DbInclude).ThrowIfNull("Include");
#elif NETSTANDARD15
                    result = Include(result, path.DbIncludeProperties);
#endif
                }
            }
            return result;
        }

        private static IQueryable<T> Include<T>(IQueryable<T> queryable, IReadOnlyList<ValueHoldingMember> properties)
            where T : class
        {
            if (properties.Count <= 0)
            {
                return queryable;
            }
            return queryable.Include(string.Join(".", properties.Select(p => p.Name)));
        }

        [NotNull]
        public static IQueryable<T> Include<T>(this IQueryable<T> source, IEnumerable<Expression<Func<T, object>>> paths)
            where T : class
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (paths == null)
            {
                return source;
            }
            var result = source;
            foreach (var path in paths)
            {
                if (path != null)
                {
#if NET45
                    result = result.Include(path).ThrowIfNull("Include");
#elif NETSTANDARD15
                    result = Include(result, IncludeParser.Parse(path));
#endif
                }
            }
            return result;
        }

        [NotNull]
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, IEnumerable<IOrderByClause<T>> paths)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (paths == null)
            {
                return source;
            }
            using (var enumerator = paths.GetEnumerator())
            {
                if (!enumerator.MoveNext())
                {
                    return source;
                }
                if (enumerator.Current == null)
                {
                    throw new ArgumentException("The first ordering clause is null");
                }
                var orderedSource = enumerator.Current.ApplyOrderBy(source);
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current == null)
                    {
                        continue;
                    }
                    orderedSource = enumerator.Current.ApplyThenBy(orderedSource);
                }
                return orderedSource;
            }
        }
    }
}
