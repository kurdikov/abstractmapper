﻿namespace TsSoft.EntityRepository.Helpers.Clone
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    /// Описание внешнего ключа таблицы на себя
    /// </summary>
    /// <typeparam name="TEntity">Тип внешней сущности</typeparam>
    /// <typeparam name="TKey">Тип ключа</typeparam>
    /// <typeparam name="TInnerTree">Тип вложенной сущности с ключом на себя</typeparam>
    public class InnerTreeForeignKeyDescription<TEntity, TInnerTree, TKey> : ICyclicForeignKey<TEntity>
        where TEntity: class 
        where TInnerTree: class, ITreeElement<TInnerTree, TKey>
        where TKey: struct
    {
        /// <summary>
        /// Путь от внешней сущности до сущности с ключом на себя
        /// </summary>
        [NotNull]
        public Func<TEntity, IEnumerable<TInnerTree>> TreeSelector { get; set; }

        /// <summary>
        /// Переставить ключ согласно текущему состоянию сущности
        /// </summary>
        public void SetFks(IEnumerable<TEntity> entities)
        {
            if (entities == null)
            {
                return;
            }
            var treeItems = entities.SelectMany(TreeSelector).ToDictionary(e => e.Id);
            foreach (var item in treeItems)
            {
                if (item.Value != null && item.Value.ParentId.HasValue)
                {
                    TInnerTree parentClone;
                    treeItems.TryGetValue(item.Value.ParentId.Value, out parentClone);
                    item.Value.Parent = parentClone;
                }
            }
        }
    }
}