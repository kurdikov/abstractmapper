﻿namespace TsSoft.EntityRepository.Helpers.Clone
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.Expressions.Helpers.Extensions;

    /// <summary>
    /// Описание внешнего ключа в циклической структуре
    /// </summary>
    /// <typeparam name="TEntity">Тип внешней сущности</typeparam>
    /// <typeparam name="TReferenceContainer">Тип сущности, содержащей внешний ключ</typeparam>
    /// <typeparam name="TReferencedEntity">Тип сущности, на которую указывает внешний ключ</typeparam>
    /// <typeparam name="TReferencedEntityKey">Тип внешнего ключа</typeparam>
    public class CyclicForeignKeyDescription<TEntity, TReferenceContainer, TReferencedEntity, TReferencedEntityKey> : ICyclicForeignKey<TEntity>
        where TEntity: class
        where TReferenceContainer: class
        where TReferencedEntity: class, IEntityWithId<TReferencedEntityKey>
        where TReferencedEntityKey : struct, IEquatable<TReferencedEntityKey>
    {
        /// <summary>
        /// Путь от главной сущности до сущности, на которую указывает внешний ключ
        /// </summary>
        [NotNull]
        public Func<TEntity, IEnumerable<TReferencedEntity>> ReferencedEntitySelector { get; set; }

        /// <summary>
        /// Путь от главной сущности до сущности, содержащей внешний ключ
        /// </summary>
        [NotNull]
        public Func<TEntity, IEnumerable<TReferenceContainer>> ContainerSelector { get; set; }

        /// <summary>
        /// Как получить внешний ключ из содержащей его сущности
        /// </summary>
        [NotNull]
        public Func<TReferenceContainer, TReferencedEntityKey?> FkSelector { get; set; }

        /// <summary>
        /// Как выставить навигационное свойство для внешнего ключа
        /// </summary>
        [NotNull]
        public Action<TReferenceContainer, TReferencedEntity> FkNavigationSetter { get; set; }

        /// <summary>
        /// Переставить ключ согласно текущему состоянию сущности
        /// </summary>
        public void SetFks(IEnumerable<TEntity> entities)
        {
            if (entities == null)
            {
                return;
            }
            foreach (var entity in entities)
            {
                var referencedLookup = ReferencedEntitySelector(entity)
                    .NotNull()
                    .Where(re => re != null)
                    .Distinct(EntityComparer<TReferencedEntity, TReferencedEntityKey>.Instance)
                    .ToDictionary(re => re.Id);
                foreach (var container in ContainerSelector(entity).NotNull().Where(c => c != null))
                {
                    var fk = FkSelector(container);
                    if (fk.HasValue)
                    {
                        TReferencedEntity referenced;
                        if (referencedLookup.TryGetValue(fk.Value, out referenced))
                        {
                            FkNavigationSetter(container, referenced);
                        }
                    }
                }
            }
        }
    }
}
