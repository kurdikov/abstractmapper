﻿namespace TsSoft.EntityRepository.Helpers.Clone
{
    using System.Collections.Generic;

    /// <summary>
    /// Описание внешнего ключа, достижимого из TEntity, умеющее переставить ключ согласно текущему состоянию сущности
    /// </summary>
    public interface ICyclicForeignKey<in TEntity>
    {
        /// <summary>
        /// Переставить ключ согласно текущему состоянию сущности
        /// </summary>
        void SetFks(IEnumerable<TEntity> entities);
    }
}
