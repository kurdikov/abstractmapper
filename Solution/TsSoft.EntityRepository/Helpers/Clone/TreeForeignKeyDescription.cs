﻿namespace TsSoft.EntityRepository.Helpers.Clone
{
    using System.Collections.Generic;
    using System.Linq;
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    /// Описание внешнего ключа таблицы на себя
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    /// <typeparam name="TKey">Тип ключа</typeparam>
    public class TreeForeignKeyDescription<TEntity, TKey> : ICyclicForeignKey<TEntity>
        where TEntity: class, ITreeElement<TEntity, TKey>
        where TKey: struct
    {
        /// <summary>
        /// Обновить внешние ключи
        /// </summary>
        /// <param name="entities">Сущности</param>
        public void SetFks(IEnumerable<TEntity> entities)
        {
            if (entities == null)
            {
                return;
            }
            var lookup = entities.ToDictionary(e => e.Id);
            foreach (var entity in lookup.Values.Where(e => e != null))
            {
                if (entity.ParentId.HasValue)
                {
                    TEntity parent;
                    lookup.TryGetValue(entity.ParentId.Value, out parent);
                    entity.Parent = parent;
                }
            }
        }
    }
}
