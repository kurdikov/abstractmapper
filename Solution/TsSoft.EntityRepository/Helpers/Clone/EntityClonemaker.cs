﻿namespace TsSoft.EntityRepository.Helpers.Clone
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Reflection;

    internal class EntityClonemaker<TEntity> : IEntityClonemaker<TEntity> where TEntity : class, new()
    {
        [NotNull]private readonly IBaseRepository<TEntity> _repository;
        [NotNull]private readonly IEntityTypesHelper _entityHelper;
        [NotNull]private readonly IFlatPathParser _expressionMapper;
        [NotNull]private readonly IObjectClonemaker _objectClonemaker;

        [NotNull]private readonly ClonedPropertyManager _entityClonedPropertyManager;

        [NotNull]private readonly MethodInfo _clearOneMethod;
        [NotNull]private readonly MethodInfo _clearManyMethod;

        public EntityClonemaker(
            [NotNull]IBaseRepository<TEntity> repository, 
            [NotNull]IEntityTypesHelper entityHelper,
            [NotNull]IFlatPathParser expressionMapper,
            [NotNull]IMemberInfoHelper memberInfoHelper, 
            [NotNull]IObjectClonemaker objectClonemaker)
        {
            if (repository == null) throw new ArgumentNullException("repository");
            if (entityHelper == null) throw new ArgumentNullException("entityHelper");
            if (expressionMapper == null) throw new ArgumentNullException("expressionMapper");
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (objectClonemaker == null) throw new ArgumentNullException("objectClonemaker");
            _repository = repository;
            _entityHelper = entityHelper;
            _expressionMapper = expressionMapper;
            _objectClonemaker = objectClonemaker;

            _entityClonedPropertyManager = new ClonedPropertyManager(
                copy: _entityHelper.IsColumnProperty,
                singleClone: _entityHelper.IsNavigationalSingleProperty,
                collectionClone: _entityHelper.IsNavigationalCollectionProperty);

            // ReSharper disable RedundantTypeArgumentsOfMethod
            _clearOneMethod = memberInfoHelper.GetGenericDefinitionMethodInfo(() => ClearPks<object>(new object(), null, 0));
            _clearManyMethod = memberInfoHelper.GetGenericDefinitionMethodInfo(() => ClearPks<object>(new object[0], null, 0));
            // ReSharper restore RedundantTypeArgumentsOfMethod
        }

        private void ClearPksOnPaths<T>(T clone, [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, int index)
        {
            var pathStarts = paths.Where(p => p != null && p.Count > index).GroupBy(p => p[index]);
            foreach (var pathStart in pathStarts)
            {
                if (pathStart == null || pathStart.Key == null)
                {
                    continue;
                }
                var current = pathStart.Key;
                var value = current.GetValue(clone);
                if (value == null)
                {
                    continue;
                }
                if (_entityClonedPropertyManager.SingleClone(current))
                {
                    _clearOneMethod.MakeGenericMethod(current.ValueType)
                        .Invoke(this, new[] { value, pathStart, index + 1 });
                }
                else if (_entityClonedPropertyManager.CollectionClone(current))
                {
                    _clearManyMethod.MakeGenericMethod(current.ValueType.GetGenericEnumerableArgument())
                        .Invoke(this, new[] { value, pathStart, index + 1 });
                }
            }
        }

        private void ClearPks<T>([NotNull]T clone, [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, int index)
        {
            var primaryKey = _entityHelper.GetPrimaryKey(typeof(T));
            foreach (var pkProp in primaryKey)
            {
                if (pkProp == null)
                {
                    continue;
                }
                if (pkProp.ValueType == typeof(Guid))
                {
                    pkProp.SetValue(clone, Guid.NewGuid());
                }
                else if (pkProp.ValueType == typeof(int))
                {
                    pkProp.SetValue(clone, 0);
                }
            }
            ClearPksOnPaths(clone, paths, index);
        }

        private void ClearPks<T>(IEnumerable<T> clones, [NotNull]IEnumerable<IReadOnlyList<ValueHoldingMember>> paths, int index)
        {
            var pathList = paths.ToList();
            foreach (var clone in clones ?? Enumerable.Empty<T>())
            {
// ReSharper disable once CompareNonConstrainedGenericWithNull
                if (clone == null)
                {
                    continue;
                }
                ClearPks(clone, pathList, index);
            }
        }

        [NotNull]
        private IReadOnlyCollection<Expression<Func<TEntity, object>>> GetIncludes(
            IEnumerable<Expression<Func<TEntity, object>>> withEntities)
        {
            return (withEntities ?? Enumerable.Empty<Expression<Func<TEntity, object>>>()).ToList();
        }

        [NotNull]
        private IEnumerable<TEntity> CloneAndProcess(
            [NotNull]IEnumerable<TEntity> entities,
            [NotNull]IEnumerable<Expression<Func<TEntity, object>>> includes,
            Action<TEntity> updater,
            IEnumerable<ICyclicForeignKey<TEntity>> cyclicForeignKeys = null)
        {
            var paths = includes.Select(_expressionMapper.Parse).ToList();
            var clones = _objectClonemaker.Clone(entities, paths, _entityClonedPropertyManager).ToList();
            if (updater != null)
            {
                foreach (var clone in clones)
                {
                    updater(clone);
                }
            }
            foreach (var cyclicForeignKey in cyclicForeignKeys ?? Enumerable.Empty<ICyclicForeignKey<TEntity>>())
            {
                if (cyclicForeignKey == null)
                {
                    continue;
                }
                cyclicForeignKey.SetFks(clones);
            }
            foreach (var clone in clones)
            {
                if (clone == null)
                {
                    continue;
                }
                ClearPks(clone, paths, 0);
                _repository.Create(clone, false);
            }
            return clones;
        }

        public async Task<IEnumerable<TEntity>> CloneAsync(
            Expression<Func<TEntity, bool>> where, 
            IEnumerable<Expression<Func<TEntity, object>>> withEntities,
            Action<TEntity> updater,
            IEnumerable<ICyclicForeignKey<TEntity>> cyclicForeignKeys = null, 
            bool commit = true)
        {
            var includes = GetIncludes(withEntities);
            var entities = (await _repository.GetAsync(where, includes).ConfigureAwait(false)).ThrowIfNull("GetAsync");
            var clones = CloneAndProcess(entities, includes, updater, cyclicForeignKeys);
            if (commit)
            {
                await _repository.SaveChangesAsync().ConfigureAwait(false);
            }
            return clones;
        }

        public IEnumerable<TEntity> Clone(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> withEntities,
            Action<TEntity> updater,
            IEnumerable<ICyclicForeignKey<TEntity>> cyclicForeignKeys = null,
            bool commit = true)
        {
            var includes = GetIncludes(withEntities);
            var entities = _repository.Get(where, includes);
            var clones = CloneAndProcess(entities, includes, updater, cyclicForeignKeys);
            if (commit)
            {
                _repository.SaveChanges();
            }
            return clones;
        }
    }
}
