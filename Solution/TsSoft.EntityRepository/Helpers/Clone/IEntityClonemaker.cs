﻿namespace TsSoft.EntityRepository.Helpers.Clone
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// Копировальщик сущностей в БД
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IEntityClonemaker<TEntity>
    {
        /// <summary>
        /// Копировать сущности в БД (если какая-то зависимая сущность придёт по двум путям, то получится две разные копии)
        /// </summary>
        /// <param name="where">Предикат для выборки сущностей</param>
        /// <param name="withEntities">Копируемые зависимые сущности</param>
        /// <param name="updater">Применяемая к копиям операция обновления</param>
        /// <param name="cyclicForeignKeys">Описания переустанавливаемых циклических внешних ключей</param>
        /// <param name="commit">Сохранить изменения в БД</param>
        /// <returns>Построенные копии</returns>
        [NotNull]
        IEnumerable<TEntity> Clone(
            Expression<Func<TEntity, bool>> where, 
            IEnumerable<Expression<Func<TEntity, object>>> withEntities,
            Action<TEntity> updater,
            IEnumerable<ICyclicForeignKey<TEntity>> cyclicForeignKeys = null,
            bool commit = true);

        /// <summary>
        /// Копировать сущности в БД (если какая-то зависимая сущность придёт по двум путям, то получится две разные копии)
        /// </summary>
        /// <param name="where">Предикат для выборки сущностей</param>
        /// <param name="withEntities">Копируемые зависимые сущности</param>
        /// <param name="updater">Применяемая к копиям операция обновления</param>
        /// <param name="cyclicForeignKeys">Описания переустанавливаемых циклических внешних ключей</param>
        /// <param name="commit">Сохранить изменения в БД</param>
        /// <returns>Построенные копии</returns>
        [NotNull]
        Task<IEnumerable<TEntity>> CloneAsync(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> withEntities,
            Action<TEntity> updater,
            IEnumerable<ICyclicForeignKey<TEntity>> cyclicForeignKeys = null,
            bool commit = true);
    }
}
