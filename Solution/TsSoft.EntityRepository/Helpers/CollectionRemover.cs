﻿namespace TsSoft.EntityRepository.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Factory;

    class CollectionRemover : ICollectionRemover
    {
        [NotNull]private readonly IRepositoryFactory _repositoryFactory;

        public CollectionRemover([NotNull]IRepositoryFactory repositoryFactory)
        {
            if (repositoryFactory == null) throw new ArgumentNullException("repositoryFactory");
            _repositoryFactory = repositoryFactory;
        }

        public void RemoveEntities<T, TKey>(ICollection<T> toDelete, ICollection<T> preserve, Func<T, TKey> keyExtractor)
            where T: class
        {
            if (toDelete == null || !toDelete.Any())
            {
                return;
            }
            var repository = _repositoryFactory.GetBaseRepository<T>();
            var preservedKeys = new HashSet<TKey>((preserve ?? Enumerable.Empty<T>()).Select(keyExtractor));
            foreach (var deleting in toDelete.ToList()) // to prevent InvalidOperationException due to EF modifying collections
            {
                var key = keyExtractor(deleting);
                if (key == null)
                {
                    throw new InvalidOperationException("missing key");
                }
                if (!preservedKeys.Contains(key))
                {
                    repository.DeleteSingle(deleting, false);
                }
            }
        }

        public void RemoveEntities<T>(Expression<Func<T, bool>> where)
            where T: class
        {
            var repository = _repositoryFactory.GetBaseRepository<T>();
            repository.Delete(where, false);
        }

        public Task RemoveEntitiesAsync<T, TKey>(ICollection<T> toDelete, ICollection<T> preserve, Func<T, TKey> keyExtractor) where T : class
        {
            RemoveEntities(toDelete, preserve, keyExtractor);
            return Task.FromResult(0);
        }

        public Task RemoveEntitiesAsync<T>(Expression<Func<T, bool>> where) where T : class
        {
            var repository = _repositoryFactory.GetBaseRepository<T>();
            return repository.DeleteAsync(where, false);
        }
    }
}
