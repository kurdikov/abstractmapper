namespace TsSoft.EntityRepository.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    /// ������ ��� ������������
    /// </summary>
    public interface IRepositoryHelper
    {
        /// <summary>
        /// ������������� ����������� ������� � �������, ����������� ������� ��������
        /// </summary>
        /// <typeparam name="TEntity">��� ��������</typeparam>
        /// <param name="updater">����������� �������</param>
        [NotNull]
        Action<TEntity> UpdaterToStubUpdater<TEntity>(
            Action<TEntity> updater)
            where TEntity : class, IComplexEntity;

        /// <summary>
        /// ������������� ����������� ������� � �������, ����������� ������� ��������
        /// </summary>
        /// <typeparam name="TEntity">��� ��������</typeparam>
        /// <param name="updater">����������� �������</param>
        [NotNull]
        Action<TEntity, TEntity> UpdaterToStubUpdater<TEntity>(
            Action<TEntity, TEntity> updater)
            where TEntity : class, IComplexEntity;

        /// <summary>
        /// ������������� ����������� ������� � �������, ����������� ������� ��������
        /// </summary>
        /// <typeparam name="TEntity">��� ��������</typeparam>
        /// <param name="updater">����������� �������</param>
        [NotNull]
        Func<TEntity, TEntity, IEnumerable<Func<Task>>> UpdaterToStubUpdater<TEntity>(
            Func<TEntity, TEntity, IEnumerable<Func<Task>>> updater)
            where TEntity : class, IComplexEntity;

        /// <summary>
        /// ������������� ������������� ��������
        /// </summary>
        /// <typeparam name="TEntity">��� ��������</typeparam>
        /// <param name="where">�������� �������</param>
        [NotNull]
        Expression<Func<TEntity, bool>> WhereToNonStubWhere<TEntity>(
            Expression<Func<TEntity, bool>> where)
            where TEntity : class, IComplexEntity;

        /// <summary>
        /// ������������� �������� ��������
        /// </summary>
        /// <typeparam name="TEntity">��� ��������</typeparam>
        /// <param name="where">�������� �������</param>
        [NotNull]
        Expression<Func<TEntity, bool>> WhereToNotDeletedWhere<TEntity>(
            Expression<Func<TEntity, bool>> where)
            where TEntity : class, IDeletable;

        /// <summary>
        /// ������������� �������� � ������������� ��������
        /// </summary>
        /// <typeparam name="TEntity">��� ��������</typeparam>
        /// <param name="where">�������� �������</param>
        [NotNull]
        Expression<Func<TEntity, bool>> WhereToNonStubNotDeletedWhere<TEntity>(
            Expression<Func<TEntity, bool>> where)
            where TEntity : class, IComplexEntity, IDeletable;
    }
}