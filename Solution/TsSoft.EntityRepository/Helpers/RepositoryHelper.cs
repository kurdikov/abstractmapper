﻿namespace TsSoft.EntityRepository.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.Expressions.Helpers;

    internal class RepositoryHelper : IRepositoryHelper
    {
        public Action<TEntity> UpdaterToStubUpdater<TEntity>(
            [NotNull]Action<TEntity> updater)
            where TEntity : class, IComplexEntity
        {
            if (updater == null)
            {
                throw new ArgumentNullException("updater");
            }
            Action<TEntity> stubUpdater = e =>
                {
                    e.IsSaved = true;
                    updater(e);
                };
            return stubUpdater;
        }

        public Action<TEntity, TEntity> UpdaterToStubUpdater<TEntity>(
            Action<TEntity, TEntity> updater)
            where TEntity : class, IComplexEntity
        {
            if (updater == null)
            {
                throw new ArgumentNullException("updater");
            }
            Action<TEntity, TEntity> stubUpdater = (source, target) =>
                {
                    target.IsSaved = true;
                    updater(source, target);
                };
            return stubUpdater;
        }

        public Func<TEntity, TEntity, IEnumerable<Func<Task>>> UpdaterToStubUpdater<TEntity>(
            Func<TEntity, TEntity, IEnumerable<Func<Task>>> updater)
            where TEntity : class, IComplexEntity
        {
            if (updater == null)
            {
                throw new ArgumentNullException("updater");
            }
            Func<TEntity, TEntity, IEnumerable<Func<Task>>> stubUpdater = (source, target) =>
            {
                target.IsSaved = true;
                return updater(source, target);
            };
            return stubUpdater;
        }

        public Expression<Func<TEntity, bool>> WhereToNonStubWhere<TEntity>(
            Expression<Func<TEntity, bool>> where)
            where TEntity : class, IComplexEntity
        {
            return WhereOrTrue(where)
                .And(e => e.IsSaved);
        }

        public Expression<Func<TEntity, bool>> WhereToNotDeletedWhere<TEntity>(
            Expression<Func<TEntity, bool>> where)
            where TEntity : class, IDeletable
        {
            return WhereOrTrue(where)
                .And(e => !e.IsDeleted);
        }

        public Expression<Func<TEntity, bool>> WhereToNonStubNotDeletedWhere<TEntity>(
            Expression<Func<TEntity, bool>> where)
            where TEntity : class, IComplexEntity, IDeletable
        {
            return WhereOrTrue(where)
                .And(e => e.IsSaved && !e.IsDeleted);
        }

        [NotNull]
        private static Expression<Func<TEntity, bool>> WhereOrTrue<TEntity>(Expression<Func<TEntity, bool>> where)
        {
            return where ?? (e => true);
        }
    }
}