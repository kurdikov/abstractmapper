﻿namespace TsSoft.EntityRepository.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// Удаляет сущности из БД
    /// </summary>
    public interface ICollectionRemover
    {
        /// <summary>
        /// Удаляет сущности из БД
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <typeparam name="TKey">Тип ключа сущности</typeparam>
        /// <param name="toDelete">Сущности для удаления</param>
        /// <param name="preserve">Сущности, которые следует сохранить, даже если они входят в список для удаления</param>
        /// <param name="keyExtractor">Функция для получения первичного ключа</param>
        void RemoveEntities<T, TKey>(ICollection<T> toDelete, ICollection<T> preserve, [NotNull]Func<T, TKey> keyExtractor)
            where T: class;

        /// <summary>
        /// Удаляет сущности из БД
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <typeparam name="TKey">Тип ключа сущности</typeparam>
        /// <param name="toDelete">Сущности для удаления</param>
        /// <param name="preserve">Сущности, которые следует сохранить, даже если они входят в список для удаления</param>
        /// <param name="keyExtractor">Функция для получения первичного ключа</param>
        Task RemoveEntitiesAsync<T, TKey>(ICollection<T> toDelete, ICollection<T> preserve, [NotNull]Func<T, TKey> keyExtractor)
            where T : class;
        
        /// <summary>
        /// Удаляет сущности из БД
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="where">Предикат</param>
        void RemoveEntities<T>(Expression<Func<T, bool>> where)
            where T: class;

        /// <summary>
        /// Удаляет сущности из БД
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="where">Предикат</param>
        Task RemoveEntitiesAsync<T>(Expression<Func<T, bool>> where)
            where T : class;
    }
}
