﻿namespace TsSoft.EntityRepository.Helpers.Remove
{
    /// <summary>
    /// Превращает идентификатор в строку для использования в sql
    /// </summary>
    public class IdStringifier : IIdStringifier
    {
        /// <summary>
        /// Получить строковое представление для идентификатора
        /// </summary>
        /// <typeparam name="TId">Тип идентификатора</typeparam>
        /// <param name="id">Идентификатор</param>
        /// <returns>Строковое представление идентификатора</returns>
        public string Stringify<TId>(TId id)
        {
            return "'" + id.ToString() + "'";
        }
    }
}
