﻿namespace TsSoft.EntityRepository.Helpers.Remove
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// Удаляет сущности и то, что от них зависит
    /// </summary>
    public interface IEntityRemover
    {
        /// <summary>
        /// Удаляет сущность со всеми зависимостями
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <param name="where">Предикат для выборки сущностей</param>
        /// <param name="transactionIsolationLevel">Уровень изоляции транзакции, в которой производится удаление</param>
        /// <param name="maxTypeOccurrence">Максимальное число повторных вхождений типов сущностей на путях от удаляемой сущности до зависимых сущностей</param>
        void Remove<TEntity>(
            [NotNull]Expression<Func<TEntity, bool>> where,
            IsolationLevel? transactionIsolationLevel,
            IDictionary<Type, int> maxTypeOccurrence = null)
            where TEntity : class;

        /// <summary>
        /// Удаляет сущность со всеми зависимостями
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <param name="where">Предикат для выборки сущностей</param>
        /// <param name="transactionIsolationLevel">Уровень изоляции транзакции, в которой производится удаление</param>
        /// <param name="maxTypeOccurrence">Максимальное число повторных вхождений типов сущностей на путях от удаляемой сущности до зависимых сущностей</param>
        [NotNull]
        Task RemoveAsync<TEntity>(
            [NotNull]Expression<Func<TEntity, bool>> where,
            IsolationLevel? transactionIsolationLevel,
            IDictionary<Type, int> maxTypeOccurrence = null)
            where TEntity : class;
    }
}
