﻿namespace TsSoft.EntityRepository.Helpers.Remove
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.SelectBuilder;
    using TsSoft.Expressions.SelectBuilder.Models;

    class EntityRemoverSelectCreator : IEntityRemoverSelectCreator
    {
        [NotNull] private readonly ISelectHelper _selectHelper;
        [NotNull] private readonly IExpressionBuilder _expressionBuilder;
        [NotNull] private readonly IEntityDependencyHelper _entityDependencyHelper;

        [NotNull]
        private readonly ConcurrentDictionary<KeyValuePair<Type, IDictionary<Type, int>>, object> _selectCache = 
            new ConcurrentDictionary<KeyValuePair<Type, IDictionary<Type, int>>, object>();

        [NotNull] private readonly MethodInfo _addSingleChildSelect;
        [NotNull] private readonly MethodInfo _addChildrenSelect;

        public EntityRemoverSelectCreator(
            [NotNull]IMemberInfoHelper memberInfoHelper,
            [NotNull]ISelectHelper selectHelper,
            [NotNull]IExpressionBuilder expressionBuilder,
            [NotNull]IEntityDependencyHelper entityDependencyHelper)
        {
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (selectHelper == null) throw new ArgumentNullException("selectHelper");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (entityDependencyHelper == null) throw new ArgumentNullException("entityDependencyHelper");

            _selectHelper = selectHelper;
            _expressionBuilder = expressionBuilder;
            _entityDependencyHelper = entityDependencyHelper;

            _addSingleChildSelect = memberInfoHelper.GetGenericDefinitionMethodInfo(() => AddSingleChildSelect<object, object>(null, null, null));
            _addChildrenSelect = memberInfoHelper.GetGenericDefinitionMethodInfo(() => AddChildrenSelect<object, object>(null, null, null));
        }

        public IReadOnlySelectExpression<TEntity> GetSelect<TEntity>(IDictionary<Type, int> maxTypeOccurrence)
        {
            var select = _selectCache.GetOrAdd(
                new KeyValuePair<Type, IDictionary<Type, int>>(typeof(TEntity), maxTypeOccurrence), 
                t => MakeSelect<TEntity>(t.Value))
                as IReadOnlySelectExpression<TEntity>;
            if (select == null)
            {
                throw new InvalidOperationException(string.Format("Cache contains null includes for {0}", typeof(TEntity)));
            }
            return select;
        }

        [NotNull]
        private IReadOnlySelectExpression<T> MakeSelect<T>(IDictionary<Type, int> maxOccurrence)
        {
            var result = _selectHelper.CreateByPrimaryKey<T>();
            var children = _entityDependencyHelper.GetChildMembers(typeof(T));
            foreach (var property in children.Collections)
            {
                if (property == null)
                {
                    continue;
                }
                var param = Expression.Parameter(typeof(T), "p");
                var elementType = property.ValueType.GetGenericEnumerableArgument();
                var propertyExpr = Expression.Lambda(
                    typeof(Func<,>).MakeGenericType(typeof(T), typeof(IEnumerable<>).MakeGenericType(elementType)),
                    Expression.MakeMemberAccess(param, property.Member),
                    param);
                DelegateCreator.Create<Action<SelectExpression<T>, object, IDictionary<Type, int>>>
                    (this, _addChildrenSelect.MakeGenericMethod(typeof(T), elementType))
                    (result, propertyExpr, maxOccurrence);
            }
            foreach (var property in children.Single)
            {
                if (property == null)
                {
                    continue;
                }
                DelegateCreator.Create<Action<SelectExpression<T>, object, IDictionary<Type, int>>>
                    (this, _addSingleChildSelect.MakeGenericMethod(typeof(T), property.ValueType))
                    (result, _expressionBuilder.BuildMemberAccessExpression(property), maxOccurrence);
            }
            return result;
        }

        
        private void AddSingleChildSelect<T, TChild>(
            [NotNull]SelectExpression<T> select, 
            [NotNull]object path,
            [NotNull]IDictionary<Type, int> maxOccurrence)
        {
            var typedPath = (Expression<Func<T, TChild>>)path; 
            bool add;
            var newMaxOccurrence = ProcessOccurrence(maxOccurrence, typeof(TChild), out add);
            if (add)
            {
                _selectHelper.AddSelect(select, MakeSelect<TChild>(newMaxOccurrence), typedPath);
            }
        }

        private void AddChildrenSelect<T, TChild>([NotNull]SelectExpression<T> select, [NotNull]object path, [NotNull]IDictionary<Type, int> maxOccurrence)
        {
            var typedPath = (Expression<Func<T, IEnumerable<TChild>>>) path;
            bool add;
            var newMaxOccurrence = ProcessOccurrence(maxOccurrence, typeof(TChild), out add);
            if (add)
            {
                _selectHelper.AddSelect(select, MakeSelect<TChild>(newMaxOccurrence), typedPath);
            }
        }
        
        private IDictionary<Type, int> ProcessOccurrence(
            IDictionary<Type, int> maxOccurrence, 
            [NotNull]Type occurredType,
            out bool add)
        {
            var result = maxOccurrence != null ? new Dictionary<Type, int>(maxOccurrence) : new Dictionary<Type, int>();
            int existingThreshold;
            result.TryGetValue(occurredType, out existingThreshold);
            add = existingThreshold >= 0;
            result[occurredType] = existingThreshold - 1;
            return result;
        }
    }
}
