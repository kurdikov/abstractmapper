namespace TsSoft.EntityRepository.Helpers.Remove
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Tuples;

    class EntityRemoverSqlCreator : IEntityRemoverSqlCreator
    {
        [NotNull]private readonly IMemberInfoLibrary _library;
        [NotNull]private readonly IEntityTypesHelper _entityTypesHelper;
        [NotNull]private readonly IIdStringifier _idStringifier;

        [NotNull]private readonly ConcurrentDictionary<Type, Func<DeletedEntitiesDescription, string>> _constructSqlFuncCache =
            new ConcurrentDictionary<Type, Func<DeletedEntitiesDescription, string>>();
        [NotNull]private readonly ConcurrentDictionary<Type, MulticastDelegate> _whereCompositeKeyEqualsCache =
            new ConcurrentDictionary<Type, MulticastDelegate>();

        [NotNull]private readonly MethodInfo _makeSimpleKeyDeleteSql;
        [NotNull]private readonly MethodInfo _stringifyId;
        [NotNull]private readonly Expression _this;

        public EntityRemoverSqlCreator(
            [NotNull]IMemberInfoHelper memberInfoHelper,
            [NotNull]IMemberInfoLibrary library,
            [NotNull]IEntityTypesHelper entityTypesHelper,
            [NotNull]IIdStringifier idStringifier)
        {
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (library == null) throw new ArgumentNullException("library");
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            if (idStringifier == null) throw new ArgumentNullException("idStringifier");

            _library = library;
            _entityTypesHelper = entityTypesHelper;
            _idStringifier = idStringifier;

            _makeSimpleKeyDeleteSql = memberInfoHelper.GetGenericDefinitionMethodInfo(() => MakeDeleteByIdsSql<object>(null, null));
            _stringifyId = memberInfoHelper.GetGenericDefinitionMethodInfo(() => _idStringifier.Stringify<object>(null));
            _this = Expression.Constant(this);
        }

        public string GenerateDeleteSql(Type entityType, DeletedEntitiesDescription description)
        {
            return GetConstructSqlFunc(entityType, _entityTypesHelper.GetPrimaryKey(entityType))(description);
        }

        [NotNull]
        private Func<DeletedEntitiesDescription, string> GetConstructSqlFunc([NotNull]Type deletedEntityType, [NotNull]IReadOnlyList<ValueHoldingMember> pk)
        {
            var action = _constructSqlFuncCache.GetOrAdd(deletedEntityType, t => MakeConstructSqlFunc(deletedEntityType, pk));
            if (action == null)
            {
                throw new InvalidOperationException(string.Format("Delete action cache contains null for {0}", deletedEntityType));
            }
            return action;
        }

        private Func<DeletedEntitiesDescription, string> MakeConstructSqlFunc([NotNull]Type entityType, [NotNull]IReadOnlyList<ValueHoldingMember> pk)
        {
            var param = Expression.Parameter(typeof(DeletedEntitiesDescription), "desc");
            if (pk[0] == null)
            {
                throw new NullReferenceException("pk[0]");
            }
            var idContainerType = pk.Count == 1 ? pk[0].ValueType : entityType;
            var lambda = Expression.Lambda<Func<DeletedEntitiesDescription, string>>(
                Expression.Call(
                    _this,
                    _makeSimpleKeyDeleteSql.MakeGenericMethod(idContainerType),
                    new Expression[] { param, Expression.Constant(entityType, typeof(Type)) }),
                param);
            return lambda.Compile();
        }


        private string MakeDeleteByIdsSql<TId>([NotNull] DeletedEntitiesDescription description, [NotNull]Type entityType)
        {
            var ids = description.Ids as IEnumerable<TId>;
            if (ids == null)
            {
                throw new InvalidOperationException(string.Format("Invalid id collection type for {0}: expected IEnumerable[{1}], got {2}",
                    description.TableName, typeof(TId), description.Ids != null ? description.Ids.GetType() : typeof(void)));
            }
            if (description.PrimaryKey == null)
            {
                throw new InvalidOperationException("missing key");
            }
            if (description.PrimaryKey.Count == 1 && !typeof(IEquatableTuple).GetTypeInfo().IsAssignableFrom(typeof(TId)))
            {
                if (description.PrimaryKey[0] == null)
                {
                    throw new NullReferenceException("description.PrimaryKey[0]");
                }
                var builder = new StringBuilder()
                    .Append("delete from ")
                    .Append(description.TableName)
                    .Append(" where ")
                    .Append(_entityTypesHelper.GetColumnName(description.PrimaryKey[0]))
                    .Append(" in (");
                bool hasIds = false;
                foreach (var id in ids)
                {
                    hasIds = true;
                    builder.Append(_idStringifier.Stringify(id)).Append(',');
                }
                if (hasIds)
                {
                    builder.Length -= 1;
                    return builder.Append(");").ToString();
                }
                return string.Empty;
            }
            var whereClause = string.Join(
                " or ", ids.Select(GetWhereCompositeKeyEqualsFunc<TId>(entityType, description.PrimaryKey)));
            return !string.IsNullOrEmpty(whereClause)
                ? string.Format("delete from {0} where {1};", description.TableName, whereClause)
                : string.Empty;
        }

        [NotNull]
        private Func<TId, string> GetWhereCompositeKeyEqualsFunc<TId>([NotNull]Type entityType, [NotNull]IReadOnlyList<ValueHoldingMember> primaryKey)
        {
            var result = _whereCompositeKeyEqualsCache.GetOrAdd(entityType, t => MakeWhereCompositeKeyEqualsFunc<TId>(primaryKey)) as Func<TId, string>;
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("WhereCompositeKeyEquals func cache contains null for {0}", typeof(TId)));
            }
            return result;
        }

        private Func<TId, string> MakeWhereCompositeKeyEqualsFunc<TId>([NotNull]IReadOnlyList<ValueHoldingMember> primaryKey)
        {
            if (primaryKey.Count == 1 && !typeof(IEquatableTuple).GetTypeInfo().IsAssignableFrom(typeof(TId)))
            {
                return id => _idStringifier.Stringify<TId>(id);
            }
            //string.Format("({0})", string.Join(" or ", props.Select(string.Format("{0} = '{1}'", propName, propValue)))

            var param = Expression.Parameter(typeof(TId), "id");
            var propNames = new string[primaryKey.Count];
            for (int i = 0; i < primaryKey.Count; ++i)
            {
                if (primaryKey[i] == null)
                {
                    throw new NullReferenceException(string.Format("primaryKey[{0}]", i));
                }
                propNames[i] = _entityTypesHelper.GetColumnName(primaryKey[i]);
            }
            var expressions = new List<Expression>();
            var propArray = Expression.Variable(typeof(string[]), "props");
            var pattern = Expression.Constant("{0} = {1}", typeof(string));
            var joinedStrings = Enumerable.Range(0, primaryKey.Count).Select(
                i => Expression.Call(
                    _library.StringFormatTwo(),
                    pattern,
                    Expression.Constant(propNames[i], typeof(string)),
                    Expression.Call(
                        Expression.Constant(_idStringifier),
                        _stringifyId.MakeGenericMethod(primaryKey[i].ValueType),
                        Expression.MakeMemberAccess(param, primaryKey[i].Member))));
            expressions.Add(
                Expression.Assign(
                    propArray,
                    Expression.NewArrayInit(
                        typeof(string),
                        joinedStrings)));

            expressions.Add(Expression.Call(
                _library.StringFormatOne(),
                Expression.Constant("({0})", typeof(string)),
                Expression.Call(
                    _library.StringJoinArrayStrings(),
                    Expression.Constant(" and ", typeof(string)),
                    propArray)));
            var block = Expression.Block(new[] { propArray }, expressions);
            var lambda = Expression.Lambda<Func<TId, string>>(block, param);
            return lambda.Compile();
        }
    }
}
