namespace TsSoft.EntityRepository.Helpers.Remove
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// ���������� ������-��������� ��� ��������� ��������������� �������� � ���� � ������������
    /// </summary>
    public interface IEntityRemoverSelectCreator
    {
        /// <summary>
        /// �������� ������-��������� ��� ��������� ��������������� �������� � ���� � ������������
        /// </summary>
        /// <typeparam name="TEntity">��� ��������</typeparam>
        /// <param name="maxTypeOccurrence">������������ ���������� ���������� �����</param>
        [NotNull]
        IReadOnlySelectExpression<TEntity> GetSelect<TEntity>(IDictionary<Type, int> maxTypeOccurrence);
    }
}
