﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace TsSoft.EntityRepository.Helpers.Remove
{
    /// <summary>
    /// Расширения удалятеля сущностей
    /// </summary>
    public static class EntityRemoverExtensions
    {
        /// <summary>
        /// Удаляет сущность со всеми зависимостями
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <param name="remover">Удалятель сущностей</param>
        /// <param name="where">Предикат для выборки сущностей</param>
        /// <param name="maxTypeOccurrence">Максимальное число повторных вхождений типов сущностей на путях от удаляемой сущности до зависимых сущностей</param>
        public static void Remove<TEntity>(
            [NotNull]this IEntityRemover remover,
            [NotNull]Expression<Func<TEntity, bool>> where,
            IDictionary<Type, int> maxTypeOccurrence = null)
            where TEntity : class
        {
            remover.Remove(where, transactionIsolationLevel: IsolationLevel.ReadUncommitted, maxTypeOccurrence: maxTypeOccurrence);
        }


        /// <summary>
        /// Удаляет сущность со всеми зависимостями
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <param name="remover">Удалятель сущностей</param>
        /// <param name="where">Предикат для выборки сущностей</param>
        /// <param name="maxTypeOccurrence">Максимальное число повторных вхождений типов сущностей на путях от удаляемой сущности до зависимых сущностей</param>
        [NotNull]
        public static Task RemoveAsync<TEntity>(
            [NotNull]this IEntityRemover remover,
            [NotNull]Expression<Func<TEntity, bool>> where,
            IDictionary<Type, int> maxTypeOccurrence = null)
            where TEntity : class
        {
            return remover.RemoveAsync(where, transactionIsolationLevel: IsolationLevel.ReadUncommitted, maxTypeOccurrence: maxTypeOccurrence);
        }
    }
}
