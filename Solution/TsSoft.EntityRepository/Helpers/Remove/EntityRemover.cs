﻿namespace TsSoft.EntityRepository.Helpers.Remove
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Factory;

    class EntityRemover : IEntityRemover
    {
        [NotNull] private readonly IEntityRemoverSelectCreator _selectCreator;
        [NotNull] private readonly IRepositoryFactory _repositoryFactory;
        [NotNull] private readonly IDeletedEntitiesDescriptionCreator _descriptionCreator;
        [NotNull] private readonly IEntityRemoverSqlCreator _sqlCreator;

        public EntityRemover(
            [NotNull] IEntityRemoverSelectCreator selectCreator,
            [NotNull] IRepositoryFactory repositoryFactory,
            [NotNull] IDeletedEntitiesDescriptionCreator descriptionCreator,
            [NotNull] IEntityRemoverSqlCreator sqlCreator)
        {
            if (selectCreator == null) throw new ArgumentNullException("selectCreator");
            if (repositoryFactory == null) throw new ArgumentNullException("repositoryFactory");
            if (descriptionCreator == null) throw new ArgumentNullException("descriptionCreator");
            if (sqlCreator == null) throw new ArgumentNullException("sqlCreator");
            _selectCreator = selectCreator;
            _repositoryFactory = repositoryFactory;
            _descriptionCreator = descriptionCreator;
            _sqlCreator = sqlCreator;
        }

        public async Task RemoveAsync<TEntity>(
            Expression<Func<TEntity, bool>> @where, 
            IsolationLevel? transactionIsolationLevel = IsolationLevel.ReadUncommitted,
            IDictionary<Type, int> maxTypeOccurrence = null)
            where TEntity : class
        {
            if (where == null)
            {
                throw new ArgumentNullException("where");
            }
            var select = _selectCreator.GetSelect<TEntity>(maxTypeOccurrence);
            var repository = _repositoryFactory.GetBaseRepository<TEntity>();
            var entities = await repository.GetAsync(where, select).ConfigureAwait(false);
            var sql = GetDeleteSql(entities);
            await repository.ExecuteAsync(sql, Enumerable.Empty<IDataParameter>(), transactionIsolationLevel).ConfigureAwait(false);
        }

        public void Remove<TEntity>(
            Expression<Func<TEntity, bool>> @where,
            IsolationLevel? transactionIsolationLevel = IsolationLevel.ReadUncommitted,
            IDictionary<Type, int> maxTypeOccurrence = null)
            where TEntity : class
        {
            if (where == null)
            {
                throw new ArgumentNullException("where");
            }
            var select = _selectCreator.GetSelect<TEntity>(maxTypeOccurrence);
            var repository = _repositoryFactory.GetBaseRepository<TEntity>();
            var entities = repository.Get(where, select);
            var sql = GetDeleteSql(entities);
            repository.Execute(sql, Enumerable.Empty<IDataParameter>(), transactionIsolationLevel);
        }

        [NotNull]
        private string GetDeleteSql<T>(IEnumerable<T> entities)
            where T: class
        {
            var deleteDescription = _descriptionCreator.GetDeletedEntitiesDescription(entities);
            var sqlIterations = new List<string>();
            for (var i = deleteDescription.Count - 1; i >= 0; --i)
            {
                if (deleteDescription[i] == null)
                {
                    throw new InvalidOperationException(string.Format("Deletion descriptions contain null at index {0}", i));
                }
                sqlIterations.Add(DeleteByDescription(deleteDescription[i]));
            }
            return string.Join("\n", sqlIterations);
        }

        private string DeleteByDescription(
            [NotNull]IEnumerable<KeyValuePair<Type, DeletedEntitiesDescription>> deletedEntitiesDescriptions)
        {
            var sqlBuilder = new StringBuilder();
            foreach (var deleted in deletedEntitiesDescriptions)
            {
                if (deleted.Key == null)
                {
                    continue;
                }
                if (deleted.Value == null)
                {
                    throw new InvalidOperationException(string.Format("DeletedEntitiesDescription is null for {0}", deleted.Key));
                }
                if (deleted.Value.PrimaryKey == null)
                {
                    throw new InvalidOperationException(string.Format("missing key : {0}", deleted.Key));
                }
                var sqlElement = _sqlCreator.GenerateDeleteSql(deleted.Key, deleted.Value);
                sqlBuilder.AppendLine(sqlElement);
            }
            var sql = sqlBuilder.ToString();
            return sql;
        }
    }
}
