﻿namespace TsSoft.EntityRepository.Helpers.Remove
{
    using System.Collections;
    using System.Collections.Generic;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Описание удаляемых сущностей
    /// </summary>
    public class DeletedEntitiesDescription
    {
        /// <summary>
        /// Экранированное имя таблицы для использования в SQL
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Первичный ключ
        /// </summary>
        public IReadOnlyList<ValueHoldingMember> PrimaryKey { get; set; }

        /// <summary>
        /// Идентификаторы сущностей с простым первичным ключом или сущности с композитным первичным ключом
        /// </summary>
        public IEnumerable Ids { get; set; }
    }
}
