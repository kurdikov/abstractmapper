﻿namespace TsSoft.EntityRepository.Helpers.Remove
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Создаёт описание удалённых сущностей
    /// </summary>
    public interface IDeletedEntitiesDescriptionCreator
    {
        /// <summary>
        /// Получить описание удаляемых сущностей
        /// </summary>
        /// <typeparam name="TEntity">Тип основной удаляемой сущности</typeparam>
        /// <param name="entities">Коллекция корневых удаляемых сущностей, полученная из базы</param>
        /// <returns>Список для последовательного удаления сущностей, от корней к листьям</returns>
        [NotNull]
        IReadOnlyList<IDictionary<Type, DeletedEntitiesDescription>> GetDeletedEntitiesDescription<TEntity>(
            IEnumerable<TEntity> entities)
            where TEntity: class;
    }
}
