﻿namespace TsSoft.EntityRepository.Helpers.Remove
{
    /// <summary>
    /// Превращает идентификатор в строку для использования в sql
    /// </summary>
    public interface IIdStringifier
    {
        /// <summary>
        /// Получить строковое представление для идентификатора
        /// </summary>
        /// <typeparam name="TId">Тип идентификатора</typeparam>
        /// <param name="id">Идентификатор</param>
        /// <returns>Строковое представление идентификатора</returns>
        string Stringify<TId>(TId id);
    }
}
