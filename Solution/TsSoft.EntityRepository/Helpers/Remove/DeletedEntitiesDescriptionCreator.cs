namespace TsSoft.EntityRepository.Helpers.Remove
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;

    class DeletedEntitiesDescriptionCreator : IDeletedEntitiesDescriptionCreator
    {
        [NotNull] private readonly IEntityTypesHelper _entityTypesHelper;
        [NotNull] private readonly IEntityDependencyHelper _entityDependencyHelper;
        [NotNull] private readonly IExpressionBuilder _expressionBuilder;


        [NotNull]
        private readonly ConcurrentDictionary<Type, MulticastDelegate> _constructDeleteDescriptionActionCache =
            new ConcurrentDictionary<Type, MulticastDelegate>();

        [NotNull]private readonly MethodInfo _addId;
        [NotNull]private readonly MethodInfo _constructDeleteDescription;

        [NotNull] private readonly Expression _this;

        public DeletedEntitiesDescriptionCreator(
            [NotNull] IEntityTypesHelper entityTypesHelper,
            [NotNull] IEntityDependencyHelper entityDependencyHelper,
            [NotNull] IExpressionBuilder expressionBuilder,
            [NotNull] IMemberInfoHelper memberInfoHelper)
        {
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            if (entityDependencyHelper == null) throw new ArgumentNullException("entityDependencyHelper");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            _entityTypesHelper = entityTypesHelper;
            _entityDependencyHelper = entityDependencyHelper;
            _expressionBuilder = expressionBuilder;

            _this = Expression.Constant(this);

            _addId = memberInfoHelper.GetGenericDefinitionMethodInfo(() => AddId<object>(null, null));
            _constructDeleteDescription = memberInfoHelper.GetGenericDefinitionMethodInfo(() => ConstructDeleteDescription<object>(null, null, 0));
        }

        public IReadOnlyList<IDictionary<Type, DeletedEntitiesDescription>> GetDeletedEntitiesDescription<TEntity>(
            IEnumerable<TEntity> entities)
            where TEntity: class
        {
            var result = new List<IDictionary<Type, DeletedEntitiesDescription>>();
            foreach (var entity in entities ?? Enumerable.Empty<TEntity>())
            {
                ConstructDeleteDescription(entity, result, 0);
            }
            return result;
        }

        private void ConstructDeleteDescription<T>(T entity, [NotNull]IList<IDictionary<Type, DeletedEntitiesDescription>> deleteDescription, int i)
            where T : class
        {
            if (entity == null)
            {
                return;
            }
            while (deleteDescription.Count <= i)
            {
                deleteDescription.Add(new Dictionary<Type, DeletedEntitiesDescription>());
            }
            DeletedEntitiesDescription currentDescription;
            if (deleteDescription[i] == null)
            {
                throw new InvalidOperationException(string.Format("Stack of delete operations contains null for index {0}", i));
            }
            if (!deleteDescription[i].TryGetValue(typeof(T), out currentDescription) || currentDescription == null)
            {
                currentDescription = deleteDescription[i][typeof(T)] = new DeletedEntitiesDescription();
                currentDescription.TableName = _entityTypesHelper.GetTableName(typeof(T));
                currentDescription.PrimaryKey = _entityTypesHelper.GetPrimaryKey(typeof(T));
            }
            if (currentDescription.PrimaryKey == null)
            {
                throw new InvalidOperationException("missing key: " + typeof(T).ToString());
            }
            var action = _constructDeleteDescriptionActionCache.GetOrAdd(
                typeof(T),
                t => MakeConstructDeleteDescriptionAction<T>(param => currentDescription.PrimaryKey.Count > 1
                    ? (Expression)param
                    : Expression.MakeMemberAccess(param, currentDescription.PrimaryKey[0].Member)))
                as Action<T, IList<IDictionary<Type, DeletedEntitiesDescription>>, DeletedEntitiesDescription, int>;
            if (action == null)
            {
                throw new InvalidOperationException(string.Format("Cache contains null action for {0}", typeof(T)));
            }
            action(entity, deleteDescription, currentDescription, i + 1);
        }

        private Action<TEntity, IList<IDictionary<Type, DeletedEntitiesDescription>>, DeletedEntitiesDescription, int>
            MakeConstructDeleteDescriptionAction<TEntity>([NotNull]Func<ParameterExpression, Expression> pkExtractor)
        {
            var entity = Expression.Parameter(typeof(TEntity), "e");
            var description = Expression.Parameter(typeof(IList<IDictionary<Type, DeletedEntitiesDescription>>), "desc");
            var currentDesc = Expression.Parameter(typeof(DeletedEntitiesDescription), "current");
            var nextStep = Expression.Parameter(typeof(int), "nextStep");
            var block = new List<Expression>();
            var extractPk = pkExtractor(entity);
            if (extractPk == null)
            {
                throw new NullReferenceException("extractPk");
            }
            block.Add(Expression.Call(
                _this,
                _addId.MakeGenericMethod(extractPk.Type),
                currentDesc,
                extractPk));
            var children = _entityDependencyHelper.GetChildMembers(typeof(TEntity));
            foreach (var collProp in children.Collections)
            {
                if (collProp == null)
                {
                    continue;
                }
                var elementType = collProp.ValueType.GetGenericEnumerableArgument();
                var child = Expression.Variable(elementType, "child");
                var collection = Expression.MakeMemberAccess(entity, collProp.Member);
                var foreachExpr = _expressionBuilder.Foreach(
                    collection,
                    child,
                    Expression.Call(
                        _this,
                        _constructDeleteDescription.MakeGenericMethod(elementType),
                        new Expression[] { child, description, nextStep }),
                    elementType);
                block.Add(Expression.IfThen(
                    Expression.NotEqual(collection, Expression.Constant(null, collProp.ValueType)),
                    foreachExpr));
            }
            foreach (var singleProp in children.Single)
            {
                if (singleProp == null)
                {
                    continue;
                }
                var elementType = singleProp.ValueType;
                block.Add(Expression.Call(
                    _this,
                    _constructDeleteDescription.MakeGenericMethod(elementType),
                    new Expression[] { Expression.MakeMemberAccess(entity, singleProp.Member), description, nextStep }));
            }
            var lambda = Expression.Lambda<Action<TEntity, IList<IDictionary<Type, DeletedEntitiesDescription>>, DeletedEntitiesDescription, int>>(
                Expression.Block(block),
                entity, description, currentDesc, nextStep);
            return lambda.Compile();
        }

        private void AddId<TId>([NotNull]DeletedEntitiesDescription description, TId id)
        {
            description.Ids = description.Ids ?? new List<TId>();
            var list = description.Ids as ICollection<TId>;
            if (list == null)
            {
                throw new InvalidOperationException(string.Format(
                    "Id collection in deletion description for {0} has type {1} as is not convertible to ICollection[{2}]",
                    description.TableName,
                    description.Ids != null ? description.Ids.GetType() : typeof(void),
                    typeof(TId)));
            }
            list.Add(id);
        }

    }
}
