﻿namespace TsSoft.EntityRepository.Helpers.Remove
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Генерирует SQL для удаления сущностей с зависимостями
    /// </summary>
    public interface IEntityRemoverSqlCreator
    {
        /// <summary>
        /// Генерировать SQL для удаления сущностей
        /// </summary>
        /// <param name="entityType">Тип удаляемых сущностей</param>
        /// <param name="description">Описание удаляемых сущностей</param>
        /// <returns>SQL</returns>
        string GenerateDeleteSql([NotNull]Type entityType, [NotNull]DeletedEntitiesDescription description);
    }
}
