﻿namespace TsSoft.EntityRepository.Helpers
{
    /// <summary>
    /// Хелпер для определения параметра Skip
    /// </summary>
    public static class SkipHelper
    {
        /// <summary>
        /// Получить skip по номеру и размеру страницы
        /// </summary>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Размер страницы</param>
        /// <returns>Сколько записей необходимо пропустить</returns>
        public static int GetSkip(int pageNumber, int pageSize)
        {
            var result = (pageNumber - 1) * pageSize;
            return result >= 0 ? result : 0;
        }
    }
}
