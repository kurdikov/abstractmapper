﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Расширения репозитория
    /// </summary>
    public static class BaseRepositoryExtensions
    {
        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <param name="repo">Репозиторий</param>
        /// <returns>Сущность</returns>
        [CanBeNull]
        public static TEntity GetSingle<TEntity, TId>(
            [NotNull] this IReadRepository<TEntity> repo,
            TId id,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.GetSingle(e => e.Id.Equals(id), include, includeConditions);
        }

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="select">Проекция</param>
        /// <param name="repo">Репозиторий</param>
        /// <returns>Сущность</returns>
        [CanBeNull]
        public static TEntity GetSingle<TEntity, TId>(
            [NotNull] this IReadRepository<TEntity> repo,
            TId id,
            [NotNull]IReadOnlySelectExpression<TEntity> select)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.GetSingle(e => e.Id.Equals(id), select);
        }

        /// <summary>
        ///     Обновить одну сущность
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на сущности
        /// </param>
        /// <param name="repo">Репозиторий</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Обновлённая сущность</returns>
        [CanBeNull]
        public static TEntity UpdateSingle<TEntity, TId>(
            [NotNull]this IUpdateRepository<TEntity> repo,
            Action<TEntity> updater,
            TId id,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            bool commit = true)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.UpdateSingle(updater, e => e.Id.Equals(id), include, commit);
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Удалённая сущность</returns>
        [CanBeNull]
        public static TEntity DeleteSingle<TEntity, TId>(
            [NotNull]this IDeleteRepository<TEntity> repo,
            TId id,
            bool commit = true)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.DeleteSingle(e => e.Id.Equals(id), commit);
        }

        /// <summary>
        /// Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="include">Инклюды</param>
        [CanBeNull]
        public static TEntity GetSingleWithStubs<TEntity, TId>(
            [NotNull]this IComplexEntityRepository<TEntity> repo,
            TId id,
            IEnumerable<Expression<Func<TEntity, object>>> include = null)
            where TEntity : class, IComplexEntity, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.GetSingleWithStubs(e => e.Id.Equals(id), include);
        }

        /// <summary>
        /// Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="select">Проекция</param>
        [CanBeNull]
        public static TEntity GetSingleWithStubs<TEntity, TId>(
            [NotNull]this IComplexEntityRepository<TEntity> repo,
            TId id,
            IReadOnlySelectExpression<TEntity> select)
            where TEntity : class, IComplexEntity, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.GetSingleWithStubs(e => e.Id.Equals(id), select);
        }

        /// <summary>
        /// Отменить удаление сущности
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Изменённая сущность</returns>
        [CanBeNull]
        public static TEntity UndeleteSingle<TEntity, TId>(
            [NotNull]this IDeletableEntityRepository<TEntity> repo,
            TId id,
            bool commit = true)
            where TEntity: class, IDeletable, IEntityWithId<TId>
            where TId: IEquatable<TId>
        {
            return repo.UndeleteSingle(e => e.Id.Equals(id), commit);
        }

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <param name="repo">Репозиторий</param>
        /// <returns>Задача, результат которой - сущность</returns>
        [NotNull]
        [ItemCanBeNull]
        public static Task<TEntity> GetSingleAsync<TEntity, TId>(
            [NotNull]this IReadRepository<TEntity> repo,
            TId id,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.GetSingleAsync(e => e.Id.Equals(id), include, includeConditions);
        }

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="select">Проекция</param>
        /// <param name="repo">Репозиторий</param>
        /// <returns>Сущность</returns>
        [NotNull]
        [ItemCanBeNull]
        public static Task<TEntity> GetSingleAsync<TEntity, TId>(
            [NotNull] this IReadRepository<TEntity> repo,
            TId id,
            [NotNull]IReadOnlySelectExpression<TEntity> select)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.GetSingleAsync(e => e.Id.Equals(id), select);
        }
        /// <summary>
        ///     Обновить одну сущность
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на сущности
        /// </param>
        /// <param name="repo">Репозиторий</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Задача, результат которой - обновлённая сущность</returns>
        [NotNull]
        [ItemCanBeNull]
        public static Task<TEntity> UpdateSingleAsync<TEntity, TId>(
            [NotNull]this IUpdateRepository<TEntity> repo,
            Action<TEntity> updater,
            TId id,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            bool commit = true)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.UpdateSingleAsync(updater, e => e.Id.Equals(id), include, commit);
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Задача, результат которой - удалённая сущность</returns>
        [NotNull]
        [ItemCanBeNull]
        public static Task<TEntity> DeleteSingleAsync<TEntity, TId>(
            [NotNull]this IDeleteRepository<TEntity> repo,
            TId id,
            bool commit = true)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.DeleteSingleAsync(e => e.Id.Equals(id), commit);
        }
        
        /// <summary>
        /// Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="include">Инклюды</param>
        [NotNull]
        [ItemCanBeNull]
        public static Task<TEntity> GetSingleWithStubsAsync<TEntity, TId>(
            [NotNull]this IComplexEntityRepository<TEntity> repo,
            TId id,
            IEnumerable<Expression<Func<TEntity, object>>> include = null)
            where TEntity : class, IComplexEntity, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.GetSingleWithStubsAsync(e => e.Id.Equals(id), include);
        }

        /// <summary>
        /// Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="select">Проекция</param>
        [NotNull]
        [ItemCanBeNull]
        public static Task<TEntity> GetSingleWithStubsAsync<TEntity, TId>(
            [NotNull]this IComplexEntityRepository<TEntity> repo,
            TId id,
            IReadOnlySelectExpression<TEntity> select)
            where TEntity : class, IComplexEntity, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.GetSingleWithStubsAsync(e => e.Id.Equals(id), select);
        }
        /// <summary>
        /// Отменить удаление сущности
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Задача, результат которой - изменённая сущность</returns>
        [NotNull]
        [ItemCanBeNull]
        public static Task<TEntity> UndeleteSingleAsync<TEntity, TId>(
            [NotNull]this IDeletableEntityRepository<TEntity> repo,
            TId id,
            bool commit = true)
            where TEntity : class, IDeletable, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return repo.UndeleteSingleAsync(e => e.Id.Equals(id), commit);
        }


        /// <summary>
        ///     Немедленно записать сущность в хранилище и удалить её из контекста
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="entity">Сущность</param>
        /// <param name="paths">Пути к добавляемым вложенным сущностям</param>
        /// <returns>Созданная сущность</returns>
        [NotNull]
        public static TEntity CreateDetached<TEntity>(
            [NotNull]this ICreateRepository<TEntity> repo,
            [NotNull]TEntity entity,
            params Func<TEntity, object>[] paths)
        {
            return repo.CreateDetached(entity, paths);
        }

        /// <summary>
        ///     Немедленно записать сущность в хранилище и удалить её из контекста
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="entity">Сущность</param>
        /// <param name="paths">Пути к добавляемым вложенным сущностям</param>
        /// <returns>Задача, результат которой - созданная сущность</returns>
        [NotNull]
        [ItemNotNull]
        public static Task<TEntity> CreateDetachedAsync<TEntity>(
            [NotNull]this ICreateRepository<TEntity> repo,
            [NotNull]TEntity entity,
            params Func<TEntity, object>[] paths)
        {
            return repo.CreateDetachedAsync(entity, paths);
        }


        /// <summary>
        ///     Получить все сущности
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        public static IReadOnlyCollection<TEntity> GetAll<TEntity>(
            [NotNull] this IReadRepository<TEntity> repo,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return repo.Get(null, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить все сущности
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        public static IReadOnlyCollection<TEntity> GetAll<TEntity>(
            [NotNull] this IReadRepository<TEntity> repo,
            [NotNull] IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return repo.Get(null, select, orderBy);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей (без предиката)
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        public static IPage<TEntity> GetAllPaged<TEntity>(
            [NotNull] this IReadRepository<TEntity> repo,
            int pageNumber,
            int pageSize,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            IEnumerable<IOrderByClause<TEntity>> orderBy,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return repo.GetPaged(null, pageNumber, pageSize, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей (без предиката)
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        public static IPage<TEntity> GetAllPaged<TEntity>(
            [NotNull] this IReadRepository<TEntity> repo,
            int pageNumber,
            int pageSize,
            [NotNull] IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            return repo.GetPaged(null, pageNumber, pageSize, select, orderBy);
        }

        /// <summary>
        ///     Получить все сущности
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        [ItemNotNull]
        public static Task<IReadOnlyCollection<TEntity>> GetAllAsync<TEntity>(
            [NotNull] this IReadRepository<TEntity> repo,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return repo.GetAsync(null, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить все сущности
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     Задача, результат которой -  <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        [ItemNotNull]
        public static Task<IReadOnlyCollection<TEntity>> GetAllAsync<TEntity>(
            [NotNull] this IReadRepository<TEntity> repo,
            [NotNull] IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return repo.GetAsync(null, select, orderBy);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей (без предиката)
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        [ItemNotNull]
        public static Task<IPage<TEntity>> GetAllPagedAsync<TEntity>(
            [NotNull] this IReadRepository<TEntity> repo,
            int pageNumber,
            int pageSize,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            IEnumerable<IOrderByClause<TEntity>> orderBy,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return repo.GetPagedAsync(null, pageNumber, pageSize, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей (без предиката)
        /// </summary>
        /// <param name="repo">Репозиторий</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        [ItemNotNull]
        public static Task<IPage<TEntity>> GetAllPagedAsync<TEntity>(
            [NotNull] this IReadRepository<TEntity> repo,
            int pageNumber,
            int pageSize,
            IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            return repo.GetPagedAsync(null, pageNumber, pageSize, select, orderBy);
        }
    }
}
