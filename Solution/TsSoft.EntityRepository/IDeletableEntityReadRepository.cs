﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    ///     Репозиторий получения удаляемых сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IDeletableEntityReadRepository<TEntity> : IReadRepository<TEntity> where TEntity : class, IDeletable
    {
        /// <summary>
        /// Получить сущность, возможно, удаленную
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="include">Инклюды</param>
        /// <param name="includeConditions">Кондишены</param>
        TEntity GetSingleWithDeleted(Expression<Func<TEntity, bool>> where,
                                   IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        /// Получить сущность, возможно, удаленную
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="select">Селект</param>
        TEntity GetSingleWithDeleted(
            Expression<Func<TEntity, bool>> where,
            IReadOnlySelectExpression<TEntity> select);

        /// <summary>
        /// Получить сущность, возможно, удаленную
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="include">Инклюды</param>
        /// <param name="includeConditions">Кондишены</param>
        Task<TEntity> GetSingleWithDeletedAsync(Expression<Func<TEntity, bool>> where,
                                              IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        /// Получить сущность, возможно, удаленную
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="select">Селект</param>
        Task<TEntity> GetSingleWithDeletedAsync(
            Expression<Func<TEntity, bool>> where,
            IReadOnlySelectExpression<TEntity> select);

        /// <summary>
        ///     Получить коллекцию сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        IReadOnlyCollection<TEntity> GetWithDeleted(Expression<Func<TEntity, bool>> where,
                                                    IReadOnlySelectExpression<TEntity> select,
                                                    IEnumerable<IOrderByClause<TEntity>> orderBy = null);

        /// <summary>
        ///     Получить коллекцию сущностей (включая удаленные)
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        IReadOnlyCollection<TEntity> GetWithDeleted(Expression<Func<TEntity, bool>> where,
                IEnumerable<Expression<Func<TEntity, object>>> include = null,
                IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);


        /// <summary>
        ///     Получить коллекцию сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        Task<IReadOnlyCollection<TEntity>> GetWithDeletedAsync(Expression<Func<TEntity, bool>> where,
                                                               IReadOnlySelectExpression<TEntity> select,
                                                               IEnumerable<IOrderByClause<TEntity>> orderBy = null);

        /// <summary>
        ///     Получить количество сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Количество сущностей по предикату</returns>
        int CountWithDeleted(Expression<Func<TEntity, bool>> where);

        /// <summary>
        ///     Существует ли сущность в хранилище, возможно, удаленная
        /// </summary>
        /// <param name="where">Предикат</param>
        bool ExistsWithDeleted(Expression<Func<TEntity, bool>> where);


        /// <summary>
        ///     Получить пагинированную коллекцию сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        IPage<TEntity> GetPagedWithDeleted(
            Expression<Func<TEntity, bool>> where,
            int pageNumber,
            int pageSize,
            IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy);

        /// <summary>
        ///     Получить отслеживаемые EF сущности с удаленными
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какий подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        [NotNull]
        IReadOnlyCollection<TEntity> GetTrackedWithDeleted(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище (включая удаленные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Максимальное значение поля или null</returns>
        T? MaxWithDeleted<T>(
            Expression<Func<TEntity, T>> selector,
            Expression<Func<TEntity, bool>> where) where T : struct;

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище (включая удаленные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Минимальное значение поля или null</returns>
        T? MinWithDeleted<T>(
            Expression<Func<TEntity, T>> selector,
            Expression<Func<TEntity, bool>> where) where T : struct;

        /// <summary>
        ///     Получить количество сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - количество сущностей по предикату</returns>
        [NotNull]
        Task<int> CountWithDeletedAsync(Expression<Func<TEntity, bool>> where);

        /// <summary>
        ///     Существует ли сущность в хранилище, возможно, удаленная
        /// </summary>
        /// <param name="where">Предикат</param>
        [NotNull]
        Task<bool> ExistsWithDeletedAsync(Expression<Func<TEntity, bool>> where);

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        Task<IPage<TEntity>> GetPagedWithDeletedAsync(
            Expression<Func<TEntity, bool>> where,
            int pageNumber,
            int pageSize,
            IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy);

        /// <summary>
        ///     Получить отслеживаемые EF сущности с удаленными
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        [NotNull]
        Task<IReadOnlyCollection<TEntity>> GetTrackedWithDeletedAsync(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище (включая удаленные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - максимальное значение поля или null</returns>
        [NotNull]
        Task<T?> MaxWithDeletedAsync<T>(
            [NotNull]Expression<Func<TEntity, T>> selector,
            Expression<Func<TEntity, bool>> where) where T : struct;

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище (включая удаленные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - минимальное значение поля или null</returns>
        [NotNull]
        Task<T?> MinWithDeletedAsync<T>(
            [NotNull]Expression<Func<TEntity, T>> selector,
            Expression<Func<TEntity, bool>> where) where T : struct;

        /// <summary>
        ///     Получить коллекцию сущностей (включая удаленные)
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        Task<IReadOnlyCollection<TEntity>> GetWithDeletedAsync(
                Expression<Func<TEntity, bool>> where,
                IEnumerable<Expression<Func<TEntity, object>>> include = null,
                IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                IEnumerable<IIncludeCondition<TEntity>> includeConditions = null);
    }
}