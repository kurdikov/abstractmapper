﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    ///     Репозиторий сложных сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public class ComplexEntityRepository<TEntity> : BaseRepository<TEntity>, IComplexEntityRepository<TEntity>
            where TEntity : class, IComplexEntity
    {
        [NotNull] private readonly IComplexDeleteRepository<TEntity> _deleteRepository;
        [NotNull] private readonly IComplexReadRepository<TEntity> _readRepository;

        /// <summary>
        ///     Репозиторий сложных сущностей
        /// </summary>
        public ComplexEntityRepository([NotNull] IItemWrapper<DbContext> contextWrapper, [NotNull] ICreateRepository<TEntity> createRepository,
                                       [NotNull] IComplexUpdateRepository<TEntity> updateRepository,
                                       [NotNull] IComplexDeleteRepository<TEntity> deleteRepository,
                                       [NotNull] IComplexReadRepository<TEntity> readRepository, [NotNull] ITransactor transactor,
                                       [NotNull] IDatabaseCommitter databaseCommitter)
                : base(contextWrapper, createRepository, updateRepository, deleteRepository, readRepository, transactor, databaseCommitter)
        {
            if (deleteRepository == null) throw new ArgumentNullException("deleteRepository");
            if (readRepository == null) throw new ArgumentNullException("readRepository");
            _deleteRepository = deleteRepository;
            _readRepository = readRepository;
        }

        /// <summary>
        ///     Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="include">Инклюды</param>
        /// <param name="includeConditions"></param>
        public TEntity GetSingleWithStubs(Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                          IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetSingleWithStubs(@where, include, includeConditions);
        }

        /// <summary>
        ///     Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="select">Селект</param>
        public TEntity GetSingleWithStubs(Expression<Func<TEntity, bool>> @where, IReadOnlySelectExpression<TEntity> @select)
        {
            return _readRepository.GetSingleWithStubs(@where, @select);
        }

        /// <summary>
        ///     Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="include">Инклюды</param>
        /// <param name="includeConditions"></param>
        public Task<TEntity> GetSingleWithStubsAsync(Expression<Func<TEntity, bool>> @where,
                                                     IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                     IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetSingleWithStubsAsync(@where, include, includeConditions);
        }

        /// <summary>
        ///     Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="select">Селект</param>
        public Task<TEntity> GetSingleWithStubsAsync(Expression<Func<TEntity, bool>> @where, IReadOnlySelectExpression<TEntity> @select)
        {
            return _readRepository.GetSingleWithStubsAsync(@where, @select);
        }


        /// <summary>
        ///     Получить коллекцию сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetWithStubs(Expression<Func<TEntity, bool>> @where, IReadOnlySelectExpression<TEntity> @select,
                                                         IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return _readRepository.GetWithStubs(@where, @select, orderBy);
        }

        /// <summary>
        ///     Получить коллекцию сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetWithStubsAsync(Expression<Func<TEntity, bool>> @where, IReadOnlySelectExpression<TEntity> @select,
                                                                    IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return _readRepository.GetWithStubsAsync(@where, @select, orderBy);
        }

        /// <summary>
        ///     Удалить все несохранённые сущности
        /// </summary>
        public IReadOnlyCollection<TEntity> DeleteStubs()
        {
            return _deleteRepository.DeleteStubs();
        }

        /// <summary>
        ///     Удалить все несохранённые сущности
        /// </summary>
        public Task<IReadOnlyCollection<TEntity>> DeleteStubsAsync()
        {
            return _deleteRepository.DeleteStubsAsync();
        }

        /// <summary>
        ///     Получить количество сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Количество сущностей по предикату</returns>
        public int CountWithStubs(Expression<Func<TEntity, bool>> @where)
        {
            return _readRepository.CountWithStubs(@where);
        }

        /// <summary>
        ///     Получить количество сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - количество сущностей по предикату</returns>
        public Task<int> CountWithStubsAsync(Expression<Func<TEntity, bool>> @where)
        {
            return _readRepository.CountWithStubsAsync(@where);
        }

        /// <summary>
        ///     Существует ли сущность в хранилище, возможно, несохраненная
        /// </summary>
        /// <param name="where">Предикат</param>
        public bool ExistsWithStubs(Expression<Func<TEntity, bool>> @where)
        {
            return _readRepository.ExistsWithStubs(@where);
        }

        /// <summary>
        ///     Существует ли сущность в хранилище, возможно, несохраненная
        /// </summary>
        /// <param name="where">Предикат</param>
        public Task<bool> ExistsWithStubsAsync(Expression<Func<TEntity, bool>> @where)
        {
            return _readRepository.ExistsWithStubsAsync(@where);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public IPage<TEntity> GetPagedWithStubs(Expression<Func<TEntity, bool>> @where, int pageNumber, int pageSize,
                                                           IReadOnlySelectExpression<TEntity> @select, IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            return _readRepository.GetPagedWithStubs(@where, pageNumber, pageSize, @select, orderBy);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public Task<IPage<TEntity>> GetPagedWithStubsAsync(Expression<Func<TEntity, bool>> @where, int pageNumber, int pageSize,
                                                                      IReadOnlySelectExpression<TEntity> @select,
                                                                      IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            return _readRepository.GetPagedWithStubsAsync(@where, pageNumber, pageSize, @select, orderBy);
        }


        /// <summary>
        ///     Получить отслеживаемые EF сущности с несохраненными
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какий подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetTrackedWithStubs(Expression<Func<TEntity, bool>> @where,
                                                                IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                                IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                                IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetTrackedWithStubs(@where, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности с несохраненными
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetTrackedWithStubsAsync(Expression<Func<TEntity, bool>> @where,
                                                                           IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                                           IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                                           IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetTrackedWithStubsAsync(@where, include, orderBy, includeConditions);
        }


        /// <summary>
        ///     Получить коллекцию сущностей (включая несохраненные)
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetWithStubs(Expression<Func<TEntity, bool>> @where,
                                                         IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                         IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                         IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetWithStubs(@where, include, orderBy, includeConditions);
        }


        /// <summary>
        ///     Получить коллекцию сущностей (включая несохраненные)
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetWithStubsAsync(Expression<Func<TEntity, bool>> @where,
                                                                    IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                                    IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                                    IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetWithStubsAsync(@where, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище (включая несохраненные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Максимальное значение поля или null</returns>
        public T? MaxWithStubs<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return _readRepository.MaxWithStubs(selector, @where);
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище (включая несохраненные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - максимальное значение поля или null</returns>
        public Task<T?> MaxWithStubsAsync<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return _readRepository.MaxWithStubsAsync(selector, @where);
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище (включая несохраненные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Минимальное значение поля или null</returns>
        public T? MinWithStubs<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return _readRepository.MinWithStubs(selector, @where);
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище (включая несохраненные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - минимальное значение поля или null</returns>
        public Task<T?> MinWithStubsAsync<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return _readRepository.MinWithStubsAsync(selector, @where);
        }
    }
}