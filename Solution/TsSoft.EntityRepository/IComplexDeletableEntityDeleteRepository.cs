﻿namespace TsSoft.EntityRepository
{
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    ///     Репозиторий удаления сложных удаляемых сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IComplexDeletableEntityDeleteRepository<TEntity> : IComplexDeleteRepository<TEntity>, IDeletableEntityDeleteRepository<TEntity>
        where TEntity : class, IComplexEntity, IDeletable
    {
    }
}