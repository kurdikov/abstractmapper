﻿namespace TsSoft.EntityRepository
{
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    ///     Осуществляет прямые sql-запросы на ззапись
    /// </summary>
    public interface IRawSqlWriteRepository
    {
        /// <summary>
        ///     Выполнить запрос
        /// </summary>
        /// <param name="sql">Запрос</param>
        /// <param name="parameters">Параметры запроса</param>
        /// <param name="transactionIsolationLevel">Уровень транзакции (null в случае выполнения без транзакции)</param>
        /// <returns>Количество обработанных запросом строк</returns>
        int Execute(
            [NotNull] string sql,
            [NotNull] IEnumerable<IDataParameter> parameters,
            IsolationLevel? transactionIsolationLevel);

        /// <summary>
        ///     Выполнить запрос
        /// </summary>
        /// <param name="sql">Запрос</param>
        /// <param name="parameters">Параметры запроса</param>
        /// <param name="transactionIsolationLevel">Уровень транзакции (null в случае выполнения без транзакции)</param>
        /// <returns>Задача, результат которой - количество обработанных запросом строк</returns>
        [NotNull]
        Task<int> ExecuteAsync(
            [NotNull] string sql,
            [NotNull] IEnumerable<IDataParameter> parameters,
            IsolationLevel? transactionIsolationLevel);
    }
}
