﻿namespace TsSoft.EntityRepository
{
    using System;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.IncludeProcessors;

    /// <summary>
    ///     Репозиторий
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public abstract class InternalBaseRepository<TEntity> : CommitterBase where TEntity : class
    {
        /// <summary>
        ///     Обработчик сложных инклюдов
        /// </summary>
        [NotNull] protected readonly IUniversalIncludeProcessor UniversalIncludeProcessor;


        /// <summary>
        ///     Репозиторий
        /// </summary>
        protected InternalBaseRepository(
            [NotNull] IItemWrapper<DbContext> contextWrapper,
            [NotNull] IUniversalIncludeProcessor universalIncludeProcessor) : base(contextWrapper)
        {
            if (universalIncludeProcessor == null) throw new ArgumentNullException("universalIncludeProcessor");
            UniversalIncludeProcessor = universalIncludeProcessor;
        }

        /// <summary>
        ///     Таблица, обслуживаемая репозиторием
        /// </summary>
        [NotNull]
        protected DbSet<TEntity> Table([NotNull]DbContext context)
        {
            var set = context.Set<TEntity>();
            if (set == null)
            {
                throw new InvalidOperationException(string.Format("Context {0} does not contain DbSet<{1}>", context.GetType(), typeof(TEntity)));
            }
            return set;
        }

        /// <summary>
        ///     Создать запрос к таблице без трекинга
        /// </summary>
        [NotNull]
        protected IQueryable<TEntity> NoTracking([NotNull]DbContext context)
        {
            var table = Table(context).AsNoTracking();
            if (table == null)
            {
                throw new InvalidOperationException("Table.AsNoTracking() returned null");
            }
            return table;
        }
    }
}
