﻿namespace TsSoft.EntityRepository.IncludeProcessors
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.External;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.IncludeBuilder;
    using TsSoft.Expressions.IncludeBuilder.Models;

    class UniversalIncludeProcessor : IUniversalIncludeProcessor
    {
        [NotNull]private readonly IIncludeDescriptionHelper _helper;
        [NotNull]private readonly IExternalIncludeProcessor _externalIncludeProcessor;
        [NotNull]private readonly IConditionalIncludeProcessor _conditionalIncludeProcessor;
        [NotNull]private readonly IEntityTypesRetriever _entityTypesRetriever;

        public UniversalIncludeProcessor(
            [NotNull]IIncludeDescriptionHelper helper, 
            [NotNull]IExternalIncludeProcessor externalIncludeProcessor, 
            [NotNull]IConditionalIncludeProcessor conditionalIncludeProcessor,
            [NotNull]IEntityTypesRetriever entityTypesRetriever)
        {
            if (helper == null) throw new ArgumentNullException("helper");
            if (externalIncludeProcessor == null) throw new ArgumentNullException("externalIncludeProcessor");
            if (conditionalIncludeProcessor == null) throw new ArgumentNullException("conditionalIncludeProcessor");
            if (entityTypesRetriever == null) throw new ArgumentNullException("entityTypesRetriever");
            _helper = helper;
            _externalIncludeProcessor = externalIncludeProcessor;
            _conditionalIncludeProcessor = conditionalIncludeProcessor;
            _entityTypesRetriever = entityTypesRetriever;
        }

        [NotNull]
        private IEnumerable<IGrouping<Type, IncludeDescription<T>>> GetExternalIncludeGroups<T>([NotNull]IEnumerable<IncludeDescription<T>> includes)
        {
            var externalTypes = _entityTypesRetriever.GetExternalEntityTypes() ?? Enumerable.Empty<Type>();
            var includeTypes = includes
                .ToLookup(ei => ei.ThrowIfNull("ei").ExternalType)
                .Where(l => l != null && externalTypes.Contains(l.Key));
            return includeTypes;
        }
        
        [NotNull]
        private IEnumerable<IGrouping<IIncludeCondition<T>, IncludeDescription<T>>> GetConditionalIncludeGroups<T>(
            [NotNull]IEnumerable<IncludeDescription<T>> includes)
        {
            return includes.Where(i => i != null && i.Condition != null).GroupBy(i => i.Condition);
        }

        public void ProcessComplexIncludes<T>(
            DbContext context,
            IReadOnlyCollection<T> entities,
            IReadOnlyCollection<IncludeDescription<T>> includes,
            IReadOnlyCollection<IIncludeCondition<T>> conditions)
            where T: class
        {
            var includeTypes = GetExternalIncludeGroups(includes);
            foreach (var includeType in includeTypes)
            {
                if (includeType == null || includeType.Key == null)
                {
                    continue;
                }
                _externalIncludeProcessor.ProcessExternalIncludes(
                    includeType.Key, 
                    entities, 
                    includeType.Select(si => _helper.IncludeToApplicationDescription(si.ThrowIfNull("si"), entities)));
            }

            var conditionGroupings = GetConditionalIncludeGroups(includes);
            foreach (var includesCutByCondition in conditionGroupings)
            {
                if (includesCutByCondition == null || includesCutByCondition.Key == null)
                {
                    continue;
                }
                _conditionalIncludeProcessor.ProcessCondition(
                    context,
                    entities, 
                    includesCutByCondition.Key, 
                    includesCutByCondition, 
                    conditions);
            }
        }


        public async Task ProcessComplexIncludesAsync<T>(
            DbContext context,
            IReadOnlyCollection<T> entities,
            IReadOnlyCollection<IncludeDescription<T>> includes, 
            IReadOnlyCollection<IIncludeCondition<T>> conditions) where T : class
        {
            var includeTypes = GetExternalIncludeGroups(includes);
            foreach (var includeType in includeTypes)
            {
                if (includeType == null || includeType.Key == null)
                {
                    continue;
                }
                await _externalIncludeProcessor.ProcessExternalIncludesAsync(
                    includeType.Key,
                    entities,
                    includeType.Select(si => _helper.IncludeToApplicationDescription(si.ThrowIfNull("si"), entities))).ConfigureAwait(false);
            }

            var conditionGroupings = GetConditionalIncludeGroups(includes);
            foreach (var includesCutByCondition in conditionGroupings)
            {
                if (includesCutByCondition == null || includesCutByCondition.Key == null)
                {
                    continue;
                }
                await _conditionalIncludeProcessor.ProcessConditionAsync(
                    context,
                    entities,
                    includesCutByCondition.Key,
                    includesCutByCondition,
                    conditions).ConfigureAwait(false);
            }
        }
    }
}
