﻿namespace TsSoft.EntityRepository.IncludeProcessors
{
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.Expressions.IncludeBuilder.Models;

    /// <summary>
    /// Обработчик условных инклюдов
    /// </summary>
    public interface IConditionalIncludeProcessor
    {
        /// <summary>
        /// Обработать условие на инклюды
        /// </summary>
        /// <typeparam name="T">Тип сущности основного запроса</typeparam>
        /// <param name="context">Контекст</param>
        /// <param name="entities">Выбранные из БД сущности</param>
        /// <param name="condition">Обрабатываемое условие</param>
        /// <param name="originalCutIncludeDescriptions">Обрезанные обрабатываемым условием инклюды</param>
        /// <param name="nestedConditions">Вложенные условия</param>
        void ProcessCondition<T>(
            [NotNull]DbContext context,
            [NotNull]IEnumerable<T> entities,
            [NotNull]IIncludeCondition<T> condition,
            [NotNull]IEnumerable<IncludeDescription<T>> originalCutIncludeDescriptions,
            [NotNull]IEnumerable<IIncludeCondition<T>> nestedConditions) 
            where T: class;

        /// <summary>
        /// Обработать условие на инклюды
        /// </summary>
        /// <typeparam name="T">Тип сущности основного запроса</typeparam>
        /// <param name="context">Контекст</param>
        /// <param name="entities">Выбранные из БД сущности</param>
        /// <param name="condition">Обрабатываемое условие</param>
        /// <param name="originalCutIncludeDescriptions">Обрезанные обрабатываемым условием инклюды</param>
        /// <param name="nestedConditions">Вложенные условия</param>
        [NotNull]
        Task ProcessConditionAsync<T>(
            [NotNull]DbContext context,
            [NotNull]IEnumerable<T> entities,
            [NotNull]IIncludeCondition<T> condition,
            [NotNull]IEnumerable<IncludeDescription<T>> originalCutIncludeDescriptions,
            [NotNull]IEnumerable<IIncludeCondition<T>> nestedConditions)
            where T : class;
    }
}
