﻿namespace TsSoft.EntityRepository.IncludeProcessors
{
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Threading.Tasks;
    using Expressions.IncludeBuilder.Models;
    using JetBrains.Annotations;

    /// <summary>
    /// Обработчик сложных инклюдов
    /// </summary>
    public interface IUniversalIncludeProcessor
    {
        /// <summary>
        /// Обработать сложные инклюды
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="context">Контекст</param>
        /// <param name="entities">Извлечённые из БД сущности</param>
        /// <param name="externalIncludes">Внешние инклюды</param>
        /// <param name="conditions">Условия</param>
        void ProcessComplexIncludes<T>(
            [NotNull]DbContext context,
            [NotNull]IReadOnlyCollection<T> entities,
            [NotNull]IReadOnlyCollection<IncludeDescription<T>> externalIncludes,
            [NotNull]IReadOnlyCollection<IIncludeCondition<T>> conditions)
            where T : class;

        /// <summary>
        /// Обработать сложные инклюды
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="context">Контекст</param>
        /// <param name="entities">Извлечённые из БД сущности</param>
        /// <param name="externalIncludes">Внешние инклюды</param>
        /// <param name="conditions">Условия</param>
        [NotNull]
        Task ProcessComplexIncludesAsync<T>(
            [NotNull]DbContext context,
            [NotNull]IReadOnlyCollection<T> entities,
            [NotNull]IReadOnlyCollection<IncludeDescription<T>> externalIncludes,
            [NotNull]IReadOnlyCollection<IIncludeCondition<T>> conditions)
            where T : class;
    }
}
