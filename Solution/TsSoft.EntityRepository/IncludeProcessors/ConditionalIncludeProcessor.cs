﻿namespace TsSoft.EntityRepository.IncludeProcessors
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
#if NET45
    using System.Data;
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Factory;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.OrderBy;

    internal class ConditionalIncludeProcessor : IConditionalIncludeProcessor
    {
        [NotNull]private readonly IKeyExpressionBuilder _expressionBuilder;
        [NotNull]private readonly IRepositoryFactory _repositoryFactory;
        [NotNull]private readonly IIncludeDescriptionHelper _descriptionHelper;
        [NotNull]private readonly IConditionalIncludeCreator _conditionalIncludeCreator;
        
        [NotNull]private readonly ConcurrentDictionary<ValueHoldingMember, object> _functionalDescriptionCache =
            new ConcurrentDictionary<ValueHoldingMember, object>(MemberInfoComparer.Instance);
        [NotNull]private readonly ConcurrentDictionary<ValueHoldingMember, RetrieveDelegate> _retrieveDelegateCache =
            new ConcurrentDictionary<ValueHoldingMember, RetrieveDelegate>(MemberInfoComparer.Instance);
        [NotNull]
        private readonly ConcurrentDictionary<ValueHoldingMember, RetrieveAsyncDelegate> _retrieveAsyncDelegateCache =
            new ConcurrentDictionary<ValueHoldingMember, RetrieveAsyncDelegate>(MemberInfoComparer.Instance);

        [NotNull]private readonly MethodInfo _retrieve;
        [NotNull]private readonly MethodInfo _retrieveAsync;

        private delegate void RetrieveDelegate(
            [NotNull]DbContext context,
            [NotNull]ConditionalIncludeApplicationDescription description,
            [NotNull]object where,
            IEnumerable<LambdaExpression> includes,
            object orderBy,
            [NotNull]object condition,
            [NotNull]object nestedConditions);

        private delegate Task RetrieveAsyncDelegate(
            [NotNull]DbContext context,
            [NotNull]ConditionalIncludeApplicationDescription description,
            [NotNull]object where,
            IEnumerable<LambdaExpression> includes,
            object orderBy,
            [NotNull]object condition,
            [NotNull]object nestedConditions);

        public ConditionalIncludeProcessor(
            [NotNull]IKeyExpressionBuilder expressionBuilder, 
            [NotNull]IRepositoryFactory repositoryFactory, 
            [NotNull]IIncludeDescriptionHelper descriptionHelper,
            [NotNull]IMemberInfoHelper memberInfoHelper,
            [NotNull]IConditionalIncludeCreator conditionalIncludeCreator)
        {
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (repositoryFactory == null) throw new ArgumentNullException("repositoryFactory");
            if (descriptionHelper == null) throw new ArgumentNullException("descriptionHelper");
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (conditionalIncludeCreator == null) throw new ArgumentNullException("conditionalIncludeCreator");
            _expressionBuilder = expressionBuilder;
            _repositoryFactory = repositoryFactory;
            _descriptionHelper = descriptionHelper;
            _conditionalIncludeCreator = conditionalIncludeCreator;

            _retrieve = memberInfoHelper.GetGenericDefinitionMethodInfo(
                () => RetrieveFromDbAndSet<object, object, object, object>(null, null, null, null, null, null, null));
#pragma warning disable 4014
            _retrieveAsync = memberInfoHelper.GetGenericDefinitionMethodInfo(
                () => RetrieveFromDbAndSetAsync<object, object, object, object>(null, null, null, null, null, null, null));
#pragma warning restore 4014
        }

        private void Set<TDb, TFk>(
            [NotNull]ConditionalIncludeApplicationDescription description,
            [NotNull]IConditionalIncludeFunctionalDescription<TDb, TFk> funcDescription,
            [NotNull]IEnumerable<TDb> containedEntities)
        {
            var containedLookup = containedEntities.ToLookup(funcDescription.GetFk);
            foreach (var container in description.Containers)
            {
                if (container == null)
                {
                    continue;
                }
                var id = funcDescription.GetPk(container);
// ReSharper disable once CompareNonConstrainedGenericWithNull
                if (id == null)
                {
                    throw new InvalidOperationException("missing key");
                }
                if (containedLookup.Contains(id))
                {
                    var collection = funcDescription.GetCollection(container);
                    if (collection != null)
                    {
                        foreach (var contained in containedLookup[id])
                        {
                            collection.Add(contained);
                        }
                    }
                }
            }
        }

        [NotNull]
        private Expression<Func<TDb, bool>> GetWhere<TDb, TFk>(
            [NotNull]ConditionalIncludeApplicationDescription description,
            [NotNull]IConditionalIncludeFunctionalDescription<TDb, TFk> funcDescription,
            [NotNull]Expression<Func<TDb, bool>> where)
        {
            var fkWhere = _expressionBuilder.BuildWhereIn<TDb, TFk>(
                description.ContainedFk,
                description.Containers.Where(c => c != null).Select(funcDescription.GetPk));
            return where.And(fkWhere);
        }

        [NotNull]
        private IEnumerable<Expression<Func<TDb, object>>> GetIncludes<TDb>(IEnumerable<LambdaExpression> includes)
        {
            return includes.NotNull().Cast<Expression<Func<TDb, object>>>();
        }

        [NotNull]
        private ConditionalIncludeFunctionalDescription<TContainer, TDb, TFk> GetFunctionalDescription<TContainer, TDb, TFk>(
            [NotNull] ConditionalIncludeApplicationDescription description)
        {
            if (description.Collection == null)
            {
                throw new NullReferenceException("description.Collection");
            }
            var result = _functionalDescriptionCache.GetOrAdd(
                description.Collection,
                pi => MakeFunctionalDescription<TContainer, TDb, TFk>(description))
                as ConditionalIncludeFunctionalDescription<TContainer, TDb, TFk>;
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Functional description cache contains null for {0}", description.Collection));
            }
            return result;
        }

        private ConditionalIncludeFunctionalDescription<TContainer, TDb, TFk> MakeFunctionalDescription<TContainer, TDb, TFk>([NotNull]ConditionalIncludeApplicationDescription description)
        {
            if (description.Collection == null || description.ContainedFk == null || description.ContainerPk == null)
            {
                throw new InvalidOperationException(string.Format("Invalid include description: collection '{0}' pk '{1}' fk '{2}'", description.Collection, description.ContainerPk, description.ContainedFk));
            }
            var containerType = description.Collection.DeclaringType;
            var containerParam = Expression.Parameter(containerType, "container");
            var includedParam = Expression.Parameter(typeof(TDb), "included");
            var collectionLambda = Expression.Lambda<Func<TContainer, ICollection<TDb>>>(
                Expression.MakeMemberAccess(containerParam, description.Collection.Member),
                containerParam);
            var pkLambda = Expression.Lambda<Func<TContainer, TFk>>(
                _expressionBuilder.CreateExtractKeyExpression(containerParam, description.ContainerPk),
                containerParam);
            var fkLambda = Expression.Lambda<Func<TDb, TFk>>(
                _expressionBuilder.CreateExtractKeyExpression(includedParam, description.ContainedFk),
                includedParam);
            return new ConditionalIncludeFunctionalDescription<TContainer, TDb, TFk>(
                collectionLambda, pkLambda, fkLambda);
        }

        private void RetrieveFromDbAndSet<TContainer, TDb, TFk, TOriginal>(
            [NotNull]DbContext context,
            [NotNull]ConditionalIncludeApplicationDescription description,
            [NotNull]object where, 
            IEnumerable<LambdaExpression> includes, 
            object orderBy,
            [NotNull]object condition,
            [NotNull]object nestedConditions)
            where TDb : class
        {
            var typedWhere = (Expression<Func<TDb, bool>>) where;
            var typedOrderBy = orderBy as IEnumerable<IOrderByClause<TDb>>;
            var typedCondition = (IIncludeCondition<TOriginal>)condition;
            var typedNestedConditions = (IEnumerable<IIncludeCondition<TOriginal>>)nestedConditions;
            var repository = _repositoryFactory.GetBaseRepository<TDb>();
            var includeConditions = _conditionalIncludeCreator.GetNested<TOriginal, TDb>(typedCondition, typedNestedConditions);
            var funcDescription = GetFunctionalDescription<TContainer, TDb, TFk>(description);
            typedWhere = GetWhere(description, funcDescription, typedWhere);
            var contained = repository.GetFromSpecifiedContext(
                context,
                typedWhere,
                GetIncludes<TDb>(includes),
                typedOrderBy,
                includeConditions);
            Set(description, funcDescription, contained);
        }

        private async Task RetrieveFromDbAndSetAsync<TContainer, TDb, TFk, TOriginal>(
            [NotNull]DbContext context,
            [NotNull]ConditionalIncludeApplicationDescription description,
            [NotNull]object where,
            IEnumerable<LambdaExpression> includes,
            object orderBy,
            [NotNull]object condition,
            [NotNull]object nestedConditions)
            where TDb : class
        {
            var typedWhere = (Expression<Func<TDb, bool>>)where;
            var typedOrderBy = orderBy as IEnumerable<IOrderByClause<TDb>>;
            var typedCondition = (IIncludeCondition<TOriginal>)condition;
            var typedNestedConditions = (IEnumerable<IIncludeCondition<TOriginal>>)nestedConditions;
            var repository = _repositoryFactory.GetBaseRepository<TDb>();
            var includeConditions = _conditionalIncludeCreator.GetNested<TOriginal, TDb>(typedCondition, typedNestedConditions);
            var funcDescription = GetFunctionalDescription<TContainer, TDb, TFk>(description);
            typedWhere = GetWhere(description, funcDescription, typedWhere);
            var contained = await repository.GetFromSpecifiedContextAsync(
                context,
                typedWhere,
                GetIncludes<TDb>(includes),
                typedOrderBy,
                includeConditions).ConfigureAwait(false);
            Set(description, funcDescription, contained.ThrowIfNull("contained"));
        }


        public void ProcessCondition<T>(
            DbContext context,
            IEnumerable<T> entities,
            IIncludeCondition<T> condition,
            IEnumerable<IncludeDescription<T>> originalCutIncludeDescriptions,
            IEnumerable<IIncludeCondition<T>> nestedConditions)
            where T : class
        {
            var description = _descriptionHelper.IncludeToConditionalDescription(entities, condition);
            var originalCutIncludes = originalCutIncludeDescriptions.Select(id => id.ThrowIfNull("id").IncludeSuffix);

            var @delegate = _retrieveDelegateCache.GetOrAdd(
                description.Collection,
                coll =>
                {
                    var keyType = _expressionBuilder.GetKeyType(description.ContainerPk);
                    var method = _retrieve.MakeGenericMethod(
                        description.Collection.DeclaringType,
                        description.ContainedType,
                        keyType,
                        typeof(T));
                    return DelegateCreator.Create<RetrieveDelegate>(
                        this, method);
                }).ThrowIfNull();

            @delegate(
                context,
                description, 
                condition.WhereExpression, 
                originalCutIncludes,
                condition.OrderByObjects,
                condition,
                nestedConditions);
        }

        public Task ProcessConditionAsync<T>(
            DbContext context,
            IEnumerable<T> entities,
            IIncludeCondition<T> condition,
            IEnumerable<IncludeDescription<T>> originalCutIncludeDescriptions,
            IEnumerable<IIncludeCondition<T>> nestedConditions)
            where T : class
        {
            var description = _descriptionHelper.IncludeToConditionalDescription(entities, condition);
            var originalCutIncludes = originalCutIncludeDescriptions.Select(id => id.ThrowIfNull("id").IncludeSuffix);

            var @delegate = _retrieveAsyncDelegateCache.GetOrAdd(
                description.Collection,
                coll =>
                {
                    var keyType = _expressionBuilder.GetKeyType(description.ContainerPk);
                    var method = _retrieveAsync.MakeGenericMethod(
                        description.Collection.DeclaringType,
                        description.ContainedType,
                        keyType,
                        typeof(T));
                    return DelegateCreator.Create<RetrieveAsyncDelegate>(
                        this, method);
                }).ThrowIfNull();

            return @delegate(
                context,
                description,
                condition.WhereExpression,
                originalCutIncludes,
                condition.OrderByObjects,
                condition,
                nestedConditions).ThrowIfNull();
        }
    }
}
