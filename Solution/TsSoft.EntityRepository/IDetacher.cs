﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using JetBrains.Annotations;

    /// <summary>
    /// Удаляет сущности из контекста в памяти
    /// </summary>
    public interface IDetacher
    {
        /// <summary>
        /// Удалить сущность из контекста (при этом портится содержимое навигационных свойств)
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <param name="context">Контекст</param>
        /// <param name="entity">Сущность</param>
        /// <param name="paths">Пути к подсущностям, начиная с путей к более глубоко вложенным</param>
        void Detach<TEntity>([NotNull]DbContext context, TEntity entity, IEnumerable<Func<TEntity, object>> paths)
            where TEntity: class;
    }
}
