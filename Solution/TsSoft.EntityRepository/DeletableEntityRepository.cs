﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    ///     Репозиторий удаляемых сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public class DeletableEntityRepository<TEntity> : BaseRepository<TEntity>, IDeletableEntityRepository<TEntity>
            where TEntity : class, IDeletable
    {
        [NotNull] private readonly IDeletableEntityDeleteRepository<TEntity> _deleteRepository;
        [NotNull] private readonly IDeletableEntityReadRepository<TEntity> _readRepository;

        /// <summary>
        ///     Репозиторий удаляемых сущностей
        /// </summary>
        public DeletableEntityRepository([NotNull] IItemWrapper<DbContext> contextWrapper, [NotNull] ICreateRepository<TEntity> createRepository,
                                         [NotNull] IUpdateRepository<TEntity> updateRepository,
                                         [NotNull] IDeletableEntityDeleteRepository<TEntity> deleteRepository,
                                         [NotNull] IDeletableEntityReadRepository<TEntity> readRepository, [NotNull] ITransactor transactor,
                                         [NotNull] IDatabaseCommitter databaseCommitter)
                : base(contextWrapper, createRepository, updateRepository, deleteRepository, readRepository, transactor, databaseCommitter)
        {
            if (deleteRepository == null) throw new ArgumentNullException("deleteRepository");
            _deleteRepository = deleteRepository;
            _readRepository = readRepository;
        }

        /// <summary>
        ///     Отменить удаление сущностей
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Изменённые сущности</returns>
        public IReadOnlyCollection<TEntity> Undelete(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return _deleteRepository.Undelete(@where, commit);
        }

        /// <summary>
        ///     Отменить удаление сущности
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Изменённая сущность</returns>
        public TEntity UndeleteSingle(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return _deleteRepository.UndeleteSingle(@where, commit);
        }

        /// <summary>
        ///     Отменить удаление сущностей
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Задача, результат которой - изменённые сущности</returns>
        public Task<IReadOnlyCollection<TEntity>> UndeleteAsync(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return _deleteRepository.UndeleteAsync(@where, commit);
        }

        /// <summary>
        ///     Отменить удаление сущности
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Задача, результат которой - изменённая сущность</returns>
        public Task<TEntity> UndeleteSingleAsync(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return _deleteRepository.UndeleteSingleAsync(@where, commit);
        }

        /// <summary>
        ///     Получить количество сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Количество сущностей по предикату</returns>
        public int CountWithDeleted(Expression<Func<TEntity, bool>> @where)
        {
            return _readRepository.CountWithDeleted(@where);
        }

        /// <summary>
        ///     Получить количество сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - количество сущностей по предикату</returns>
        public Task<int> CountWithDeletedAsync(Expression<Func<TEntity, bool>> @where)
        {
            return _readRepository.CountWithDeletedAsync(@where);
        }

        /// <summary>
        ///     Существует ли сущность в хранилище, возможно, удаленная
        /// </summary>
        /// <param name="where">Предикат</param>
        public bool ExistsWithDeleted(Expression<Func<TEntity, bool>> @where)
        {
            return _readRepository.ExistsWithDeleted(@where);
        }

        /// <summary>
        ///     Существует ли сущность в хранилище, возможно, удаленная
        /// </summary>
        /// <param name="where">Предикат</param>
        public Task<bool> ExistsWithDeletedAsync(Expression<Func<TEntity, bool>> @where)
        {
            return _readRepository.ExistsWithDeletedAsync(@where);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public IPage<TEntity> GetPagedWithDeleted(Expression<Func<TEntity, bool>> @where, int pageNumber, int pageSize, IReadOnlySelectExpression<TEntity> @select, IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            return _readRepository.GetPagedWithDeleted(@where, pageNumber, pageSize, @select, orderBy);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public Task<IPage<TEntity>> GetPagedWithDeletedAsync(Expression<Func<TEntity, bool>> @where, int pageNumber, int pageSize, IReadOnlySelectExpression<TEntity> @select, IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            return _readRepository.GetPagedWithDeletedAsync(@where, pageNumber, pageSize, @select, orderBy);
        }

        /// <summary>
        /// Получить сущность, возможно, удаленную
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="include">Инклюды</param>
        /// <param name="includeConditions">Кондишены</param>
        public TEntity GetSingleWithDeleted(Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include = null, IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetSingleWithDeleted(@where, include, includeConditions);
        }

        /// <summary>
        /// Получить сущность, возможно, удаленную
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="select">Селект</param>
        public TEntity GetSingleWithDeleted(Expression<Func<TEntity, bool>> @where, IReadOnlySelectExpression<TEntity> @select)
        {
            return _readRepository.GetSingleWithDeleted(@where, @select);
        }

        /// <summary>
        /// Получить сущность, возможно, удаленную
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="include">Инклюды</param>
        /// <param name="includeConditions">Кондишены</param>
        public Task<TEntity> GetSingleWithDeletedAsync(Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include = null, IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetSingleWithDeletedAsync(@where, include, includeConditions);
        }

        /// <summary>
        /// Получить сущность, возможно, удаленную
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="select">Селект</param>
        public Task<TEntity> GetSingleWithDeletedAsync(Expression<Func<TEntity, bool>> @where, IReadOnlySelectExpression<TEntity> @select)
        {
            return _readRepository.GetSingleWithDeletedAsync(@where, @select);
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности с удаленными
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какий подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetTrackedWithDeleted(Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include = null, IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                         IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetTrackedWithDeleted(@where, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности с удаленными
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetTrackedWithDeletedAsync(Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include = null, IEnumerable<IOrderByClause<TEntity>> orderBy = null, IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetTrackedWithDeletedAsync(@where, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить коллекцию сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetWithDeleted(Expression<Func<TEntity, bool>> @where, IReadOnlySelectExpression<TEntity> @select, IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return _readRepository.GetWithDeleted(@where, @select, orderBy);
        }

        /// <summary>
        ///     Получить коллекцию сущностей (включая удаленные)
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetWithDeleted(Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include = null, IEnumerable<IOrderByClause<TEntity>> orderBy = null, IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetWithDeleted(@where, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить коллекцию сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetWithDeletedAsync(Expression<Func<TEntity, bool>> @where, IReadOnlySelectExpression<TEntity> @select, IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return _readRepository.GetWithDeletedAsync(@where, @select, orderBy);
        }

        /// <summary>
        ///     Получить коллекцию сущностей (включая удаленные)
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetWithDeletedAsync(Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include = null, IEnumerable<IOrderByClause<TEntity>> orderBy = null, IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetWithDeletedAsync(@where, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище (включая удаленные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Максимальное значение поля или null</returns>
        public T? MaxWithDeleted<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return _readRepository.MaxWithDeleted(selector, @where);
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище (включая удаленные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - максимальное значение поля или null</returns>
        public Task<T?> MaxWithDeletedAsync<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return _readRepository.MaxWithDeletedAsync(selector, @where);
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище (включая удаленные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Минимальное значение поля или null</returns>
        public T? MinWithDeleted<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return _readRepository.MinWithDeleted(selector, @where);
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище (включая удаленные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - минимальное значение поля или null</returns>
        public Task<T?> MinWithDeletedAsync<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return _readRepository.MinWithDeletedAsync(selector, @where);
        }
    }
}
