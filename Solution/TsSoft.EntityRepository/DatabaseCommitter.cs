namespace TsSoft.EntityRepository
{
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;

    /// <summary>
    /// ��������� ��������� � ��
    /// </summary>
    public class DatabaseCommitter : CommitterBase, IDatabaseCommitter
    {
        /// <summary>
        /// ��������� ��������� � ��
        /// </summary>
        public DatabaseCommitter([NotNull] IItemWrapper<DbContext> contextWrapper)
            : base(contextWrapper)
        {
        }



        /// <summary>
        ///     ���������� ��������� ��������� � ��
        /// </summary>
        /// <returns>
        ///     <see cref="Task" /> ���������� ���������
        /// </returns>
        public async Task<int> SaveChangesAsync()
        {
            var ctx = Context;
            return await ConditionalCommitAsync(ctx, true).ConfigureAwait(false);
        }


        /// <summary>
        ///     ��������� ��������� � ��
        /// </summary>
        public int SaveChanges()
        {
            var ctx = Context;
            return ConditionalCommit(ctx, true);
        }
    }
}
