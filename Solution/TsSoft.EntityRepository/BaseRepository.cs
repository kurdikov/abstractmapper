﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    ///     Репозиторий
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public class BaseRepository<TEntity> : DatabaseWrapper, IBaseRepository<TEntity>
        where TEntity : class
    {
        [NotNull] private readonly ICreateRepository<TEntity> _createRepository;
        [NotNull] private readonly IDatabaseCommitter _databaseCommitter;
        [NotNull] private readonly IDeleteRepository<TEntity> _deleteRepository;
        [NotNull] private readonly IReadRepository<TEntity> _readRepository;
        [NotNull] private readonly ITransactor _transactor;
        [NotNull] private readonly IUpdateRepository<TEntity> _updateRepository;

        /// <summary>
        ///     Репозиторий
        /// </summary>
        public BaseRepository(
            [NotNull] IItemWrapper<DbContext> contextWrapper,
            [NotNull] ICreateRepository<TEntity> createRepository,
            [NotNull] IUpdateRepository<TEntity> updateRepository,
            [NotNull] IDeleteRepository<TEntity> deleteRepository,
            [NotNull] IReadRepository<TEntity> readRepository,
            [NotNull] ITransactor transactor,
            [NotNull] IDatabaseCommitter databaseCommitter)
            : base(contextWrapper)
        {
            if (createRepository == null) throw new ArgumentNullException("createRepository");
            if (updateRepository == null) throw new ArgumentNullException("updateRepository");
            if (deleteRepository == null) throw new ArgumentNullException("deleteRepository");
            if (readRepository == null) throw new ArgumentNullException("readRepository");
            if (transactor == null) throw new ArgumentNullException("transactor");
            if (databaseCommitter == null) throw new ArgumentNullException("databaseCommitter");
            _createRepository = createRepository;
            _updateRepository = updateRepository;
            _deleteRepository = deleteRepository;
            _readRepository = readRepository;
            _transactor = transactor;
            _databaseCommitter = databaseCommitter;
        }

        /// <summary>
        ///     Выполнить запрос
        /// </summary>
        /// <param name="sql">Запрос</param>
        /// <param name="parameters">Параметры запроса</param>
        /// <param name="transactionIsolationLevel">Уровень транзакции (null в случае выполнения без транзакции)</param>
        /// <returns>Количество обработанных запросом строк</returns>
        public int Execute(string sql, IEnumerable<IDataParameter> parameters,
                           IsolationLevel? transactionIsolationLevel = IsolationLevel.ReadUncommitted)
        {
            if (sql == null) throw new ArgumentNullException("sql");
            if (parameters == null) throw new ArgumentNullException("parameters");
            if (sql == string.Empty)
            {
                return 0;
            }
            var ctx = Context;
            var db = Database(ctx);
            if (transactionIsolationLevel.HasValue)
            {
                using (var transaction = _transactor.Begin(ctx, transactionIsolationLevel.Value))
                {
                    var result = db.ExecuteSqlCommand(sql, MakeEfParameters(parameters));
                    transaction.Commit();
                    return result;
                }
            }
            return db.ExecuteSqlCommand(sql, MakeEfParameters(parameters));
        }

        /// <summary>
        ///     Сделать запрос, возвращающий данные
        /// </summary>
        /// <typeparam name="T">Тип объекта, в который мапятся возвращаемые данные</typeparam>
        /// <param name="sql">SQL-запрос</param>
        /// <param name="parameters">Параметры запроса</param>
        public IReadOnlyCollection<T> Query<T>(string sql, IEnumerable<IDataParameter> parameters)
        {
            if (sql == null) throw new ArgumentNullException("sql");
            if (parameters == null) throw new ArgumentNullException("parameters");
#if NET45
            var ctx = Context;
            return Database(ctx).SqlQuery<T>(sql, MakeEfParameters(parameters)).ThrowIfNull("Database.SqlQuery").ToList();
#else
            throw new NotSupportedException();
#endif
        }

        /// <summary>
        ///     Сделать запрос, возвращающий данные
        /// </summary>
        /// <typeparam name="T">Тип объекта, в который мапятся возвращаемые данные</typeparam>
        /// <param name="sql">SQL-запрос</param>
        /// <param name="parameters">Параметры запроса</param>
        public async Task<IReadOnlyCollection<T>> QueryAsync<T>(string sql, IEnumerable<IDataParameter> parameters)
        {
            if (sql == null) throw new ArgumentNullException("sql");
            if (parameters == null) throw new ArgumentNullException("parameters");
#if NET45
            var ctx = Context;
            return await Database(ctx)
                .SqlQuery<T>(sql, MakeEfParameters(parameters)).ThrowIfNull("Database.SqlQuery").ToListAsync()
                .ThrowIfNull("ToListAsync")
                .ConfigureAwait(false);
#else
            await Task.Delay(0);
            throw new NotSupportedException();
#endif
        }

        /// <summary>
        ///     Выполнить запрос
        /// </summary>
        /// <param name="sql">Запрос</param>
        /// <param name="parameters">Параметры запроса</param>
        /// <param name="transactionIsolationLevel">Уровень транзакции (null в случае выполнения без транзакции)</param>
        /// <returns>Задача, результат которой - количество обработанных запросом строк</returns>
        public async Task<int> ExecuteAsync(string sql, IEnumerable<IDataParameter> parameters,
                                            IsolationLevel? transactionIsolationLevel = IsolationLevel.ReadUncommitted)
        {
            if (sql == null) throw new ArgumentNullException("sql");
            if (parameters == null) throw new ArgumentNullException("parameters");
            if (sql == string.Empty)
            {
                return 0;
            }
            var ctx = Context;
            var db = Database(ctx);
            if (transactionIsolationLevel.HasValue)
            {
                using (var transaction = _transactor.Begin(ctx, transactionIsolationLevel.Value))
                {
                    var result = await db.ExecuteSqlCommandAsync(sql, parameters: MakeEfParameters(parameters)).ThrowIfNull("ExecuteSqlCommandAsync").ConfigureAwait(false);
                    transaction.Commit();
                    return result;
                }
            }
            return await db.ExecuteSqlCommandAsync(sql, parameters: MakeEfParameters(parameters)).ThrowIfNull("ExecuteSqlCommandAsync").ConfigureAwait(false);
        }

        /// <summary>
        ///     Обновить сущности
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на каждой из сущностей, которые попадут под предикат
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Коллекция обновлённых сущностей</returns>
        public IReadOnlyCollection<TEntity> Update(Action<TEntity> updater, Expression<Func<TEntity, bool>> @where,
                                                   IEnumerable<Expression<Func<TEntity, object>>> include = null, bool commit = true)
        {
            return _updateRepository.Update(updater, @where, include, commit);
        }

        /// <summary>
        ///     Обновить одну сущность
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на сущности
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Обновлённая сущность</returns>
        public TEntity UpdateSingle(Action<TEntity> updater, Expression<Func<TEntity, bool>> @where,
                                    IEnumerable<Expression<Func<TEntity, object>>> include = null, bool commit = true)
        {
            return _updateRepository.UpdateSingle(updater, @where, include, commit);
        }

        /// <summary>
        ///     Обновить одну сущность другой сущностью
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на каждой из сущностей, которые попадут под предикат
        ///     где первый параметр - updateSource, а второй - сущность в хранилище
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="updateSource">Чем обновлять</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Обновлённая сущность</returns>
        public TEntity UpdateSingleFrom(Action<TEntity, TEntity> updater, Expression<Func<TEntity, bool>> @where,
                                        IEnumerable<Expression<Func<TEntity, object>>> include, TEntity updateSource,
                                        bool commit = true)
        {
            return _updateRepository.UpdateSingleFrom(updater, @where, include, updateSource, commit);
        }

        /// <summary>
        ///     Обновить сущности
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на каждой из сущностей, которые попадут под предикат
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Задача, результат которой - коллекция обновлённых сущностей</returns>
        public Task<IReadOnlyCollection<TEntity>> UpdateAsync(Action<TEntity> updater,
                                                              Expression<Func<TEntity, bool>> @where,
                                                              IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                              bool commit = true)
        {
            return _updateRepository.UpdateAsync(updater, @where, include, commit);
        }

        /// <summary>
        ///     Обновить одну сущность
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на сущности
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Задача, результат которой - обновлённая сущность</returns>
        public Task<TEntity> UpdateSingleAsync(Action<TEntity> updater, Expression<Func<TEntity, bool>> @where,
                                               IEnumerable<Expression<Func<TEntity, object>>> include = null, bool commit = true)
        {
            return _updateRepository.UpdateSingleAsync(updater, @where, include, commit);
        }

        /// <summary>
        ///     Обновить одну сущность другой сущностью
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на каждой из сущностей, которые попадут под предикат
        ///     где первый параметр - updateSource, а второй - сущность в хранилище
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="updateSource">Чем обновлять</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Задача, результат которой - обновлённая сущность</returns>
        public Task<TEntity> UpdateSingleFromAsync(Action<TEntity, TEntity> updater,
                                                   Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include,
                                                   TEntity updateSource,
                                                   bool commit = true)
        {
            return _updateRepository.UpdateSingleFromAsync(updater, @where, include, updateSource, commit);
        }

        /// <summary>
        ///     Обновить одну сущность другой сущностью
        /// </summary>
        /// <param name="updater">
        ///     Обновлятор - вызовется на каждой из сущностей, которые попадут под предикат
        ///     где первый параметр - updateSource, а второй - сущность в хранилище
        /// </param>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности вытащить вместе с сущностями</param>
        /// <param name="updateSource">Чем обновлять</param>
        /// <param name="commit">Записать ли изменения в хранилище</param>
        /// <returns>Задача, результат которой - обновлённая сущность</returns>
        public Task<TEntity> UpdateSingleFromAsync(
            Func<TEntity, TEntity,
                IEnumerable<Func<Task>>> updater,
            Expression<Func<TEntity, bool>> @where,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            TEntity updateSource,
            bool commit = true)
        {
            return _updateRepository.UpdateSingleFromAsync(updater, @where, include, updateSource, commit);
        }

        /// <summary>
        ///     Записать сущность в хранилище
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="commit">
        ///     Сразу ли произвести запись
        ///     (если нет, то изменения не будут записаны после вызова SaveChanges или любого метода
        ///     с commit = true
        ///     )
        /// </param>
        /// <returns>Созданная сущность</returns>
        public TEntity Create(TEntity entity, bool commit = true)
        {
            return _createRepository.Create(entity, commit);
        }

        /// <summary>
        ///     Записать сущность в хранилище
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="commit">
        ///     Сразу ли произвести запись
        ///     (если нет, то изменения не будут записаны после вызова SaveChanges или любого метода
        ///     с commit = true
        ///     )
        /// </param>
        /// <returns>Задача, результат которой - созданная сущность</returns>
        public Task<TEntity> CreateAsync(TEntity entity, bool commit = true)
        {
            return _createRepository.CreateAsync(entity, commit);
        }

        /// <summary>
        ///     Немедленно записать сущность в хранилище и удалить её из контекста
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="paths">Пути к добавляемым вложенным сущностям, начиная с путей к более глубоко вложенным</param>
        /// <returns>Созданная сущность</returns>
        public TEntity CreateDetached(TEntity entity, IEnumerable<Func<TEntity, object>> paths = null)
        {
            return _createRepository.CreateDetached(entity, paths);
        }

        /// <summary>
        ///     Немедленно записать сущность в хранилище и удалить её из контекста
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="paths">Пути к добавляемым вложенным сущностям, начиная с путей к более глубоко вложенным</param>
        /// <returns>Задача, результат которой - созданная сущность</returns>
        public Task<TEntity> CreateDetachedAsync(TEntity entity, IEnumerable<Func<TEntity, object>> paths = null)
        {
            return _createRepository.CreateDetachedAsync(entity, paths);
        }

        /// <summary>
        ///     Удалить сущности
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Коллекция удалённых сущностей</returns>
        public IReadOnlyCollection<TEntity> Delete(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return _deleteRepository.Delete(@where, commit);
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Удалённая сущность</returns>
        public TEntity DeleteSingle(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return _deleteRepository.DeleteSingle(@where, commit);
        }

        /// <summary>
        ///     Удалить сущности
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Задача, результат которой - коллекция удалённых сущностей</returns>
        public virtual Task<IReadOnlyCollection<TEntity>> DeleteAsync(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return _deleteRepository.DeleteAsync(@where, commit);
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Задача, результат которой - удалённая сущность</returns>
        public virtual Task<TEntity> DeleteSingleAsync(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return _deleteRepository.DeleteSingleAsync(@where, commit);
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> Get(Expression<Func<TEntity, bool>> @where,
                                                IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.Get(@where, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> Get(Expression<Func<TEntity, bool>> @where,
                                                IReadOnlySelectExpression<TEntity> @select, IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return _readRepository.Get(@where, @select, orderBy);
        }

        /// <summary>
        ///     Получить количество сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Количество сущностей по предикату</returns>
        public int Count(Expression<Func<TEntity, bool>> @where)
        {
            return _readRepository.Count(@where);
        }

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Сущность</returns>
        public TEntity GetSingle(Expression<Func<TEntity, bool>> @where,
                                 IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                 IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetSingle(@where, include, includeConditions);
        }

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <returns>Сущность</returns>
        public TEntity GetSingle(Expression<Func<TEntity, bool>> @where, IReadOnlySelectExpression<TEntity> @select)
        {
            return _readRepository.GetSingle(@where, @select);
        }

        /// <summary>
        ///     Существует ли сущность в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        public bool Exists(Expression<Func<TEntity, bool>> @where)
        {
            return _readRepository.Exists(@where);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="include">Какий подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public IPage<TEntity> GetPaged(Expression<Func<TEntity, bool>> @where, int pageNumber, int pageSize,
                                                  IEnumerable<Expression<Func<TEntity, object>>> include, IEnumerable<IOrderByClause<TEntity>> orderBy,
                                                  IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetPaged(@where, pageNumber, pageSize, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public IPage<TEntity> GetPaged(Expression<Func<TEntity, bool>> @where, int pageNumber, int pageSize,
                                                  IReadOnlySelectExpression<TEntity> @select,
                                                  IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            return _readRepository.GetPaged(@where, pageNumber, pageSize, @select, orderBy);
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какий подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetTracked(Expression<Func<TEntity, bool>> @where,
                                                       IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                       IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                       IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetTracked(@where, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Максимальное значение поля или null</returns>
        public T? Max<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return _readRepository.Max(selector, @where);
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Минимальное значение поля или null</returns>
        public T? Min<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return _readRepository.Min(selector, @where);
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какий подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetAsync(Expression<Func<TEntity, bool>> @where,
                                                           IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                           IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                           IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetAsync(@where, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetAsync(Expression<Func<TEntity, bool>> @where,
                                                           IReadOnlySelectExpression<TEntity> @select,
                                                           IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return _readRepository.GetAsync(@where, @select, orderBy);
        }

        /// <summary>
        ///     Получить количество сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - количество сущностей по предикату</returns>
        public Task<int> CountAsync(Expression<Func<TEntity, bool>> @where)
        {
            return _readRepository.CountAsync(@where);
        }

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Задача, результат которой - сущность</returns>
        public Task<TEntity> GetSingleAsync(Expression<Func<TEntity, bool>> @where,
                                            IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetSingleAsync(@where, include, includeConditions);
        }

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <returns>Задача, результат которой - сущность</returns>
        public Task<TEntity> GetSingleAsync(Expression<Func<TEntity, bool>> @where,
                                            IReadOnlySelectExpression<TEntity> @select)
        {
            return _readRepository.GetSingleAsync(@where, @select);
        }

        /// <summary>
        ///     Существует ли сущность в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        public Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> @where)
        {
            return _readRepository.ExistsAsync(@where);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="include">Какий подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public Task<IPage<TEntity>> GetPagedAsync(Expression<Func<TEntity, bool>> @where, int pageNumber,
                                                             int pageSize, IEnumerable<Expression<Func<TEntity, object>>> include,
                                                             IEnumerable<IOrderByClause<TEntity>> orderBy,
                                                             IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetPagedAsync(@where, pageNumber, pageSize, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public Task<IPage<TEntity>> GetPagedAsync(Expression<Func<TEntity, bool>> @where, int pageNumber,
                                                             int pageSize, IReadOnlySelectExpression<TEntity> @select,
                                                             IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            return _readRepository.GetPagedAsync(@where, pageNumber, pageSize, @select, orderBy);
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какий подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetTrackedAsync(Expression<Func<TEntity, bool>> @where,
                                                                  IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                                  IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                                  IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetTrackedAsync(@where, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - максимальное значение поля или null</returns>
        public Task<T?> MaxAsync<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where)
            where T : struct
        {
            return _readRepository.MaxAsync(selector, @where);
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - минимальное значение поля или null</returns>
        public Task<T?> MinAsync<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where)
            where T : struct
        {
            return _readRepository.MinAsync(selector, @where);
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="entity">Сущность (из контекста или с заполненными ключевыми свойствами)</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Удалённая сущность</returns>
        public TEntity DeleteSingle(TEntity entity, bool commit = true)
        {
            return _deleteRepository.DeleteSingle(entity, commit);
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="entity">Сущность (из контекста или с заполненными ключевыми свойствами)</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Удалённая сущность</returns>
        public Task<TEntity> DeleteSingleAsync(TEntity entity, bool commit = true)
        {
            return _deleteRepository.DeleteSingleAsync(entity, commit);
        }

        /// <summary>
        ///     Сохранить изменения в бд
        /// </summary>
        /// <returns>Количество объектов, записанных в бд</returns>
        public int SaveChanges()
        {
            return _databaseCommitter.SaveChanges();
        }

        /// <summary>
        ///     Асинхронно сохранить изменения в бд
        /// </summary>
        /// <returns>
        ///     <see cref="Task" /> количества объектов, записанных в бд
        /// </returns>
        public Task<int> SaveChangesAsync()
        {
            return _databaseCommitter.SaveChangesAsync();
        }

        /// <summary>
        /// Начать транзакцию
        /// </summary>
        /// <param name="level"><see cref="IsolationLevel"/> транзакции</param>
        /// <returns>Транзакция, реализующая <see cref="ITsSoftDatabaseTransaction"/></returns>
        public IDbTransaction Begin(IsolationLevel level)
        {
            var ctx = Context;
            return _transactor.Begin(ctx, level);
        }

        /// <summary>
        ///     Привести параметры к виду, потребляемому Entity Framework
        /// </summary>
        protected object[] MakeEfParameters([NotNull] IEnumerable<IDataParameter> parameters)
        {
            return parameters.Cast<object>().ToArray();
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetFromSpecifiedContext(DbContext context, Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null, IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetFromSpecifiedContext(context, where, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetFromSpecifiedContextAsync(DbContext context, Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null, IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return _readRepository.GetFromSpecifiedContextAsync(context, where, include, orderBy, includeConditions);
        }
    }
}
