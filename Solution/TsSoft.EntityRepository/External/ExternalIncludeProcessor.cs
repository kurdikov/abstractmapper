﻿namespace TsSoft.EntityRepository.External
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Factory;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;

    class ExternalIncludeProcessor : IExternalIncludeProcessor
    {
        [NotNull]private readonly IRepositoryFactory _repositoryFactory;

        [NotNull]private readonly ConcurrentDictionary<ValueHoldingMember, object> _functionalDescriptionCache = 
            new ConcurrentDictionary<ValueHoldingMember, object>(MemberInfoComparer.Instance);
        [NotNull]private readonly ConcurrentDictionary<Type, Type> _idTypeCache =
            new ConcurrentDictionary<Type, Type>();
        [NotNull]private readonly ConcurrentDictionary<Type, Action<IEnumerable<IncludeApplicationDescription>>> _externalRetrieveDelegateCache =
            new ConcurrentDictionary<Type, Action<IEnumerable<IncludeApplicationDescription>>>();
        [NotNull]private readonly ConcurrentDictionary<Type, Func<IEnumerable<IncludeApplicationDescription>, Task>> _externalRetrieveAsyncDelegateCache =
            new ConcurrentDictionary<Type, Func<IEnumerable<IncludeApplicationDescription>, Task>>();

        [NotNull]private readonly MethodInfo _externalRetrieveMethod;
        [NotNull]private readonly MethodInfo _externalRetrieveAsyncMethod;

        public ExternalIncludeProcessor([NotNull]IRepositoryFactory repositoryFactory, [NotNull]IMemberInfoHelper memberInfoHelper)
        {
            if (repositoryFactory == null) throw new ArgumentNullException("repositoryFactory");
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            _repositoryFactory = repositoryFactory;
        
            _externalRetrieveMethod = memberInfoHelper.GetGenericDefinitionMethodInfo(
                () => RetrieveAndSet<IExternalEntity, Guid>(null));
#pragma warning disable 4014
            _externalRetrieveAsyncMethod = memberInfoHelper.GetGenericDefinitionMethodInfo(
                () => RetrieveAndSetAsync<IExternalEntity, Guid>(null));
#pragma warning restore 4014
        }

        [NotNull]
        private ExternalIncludeDescriptionCollection<TExt, TId>
            GetFunctionalDescriptions<TExt, TId>(
            [NotNull]IEnumerable<IncludeApplicationDescription> descriptions)
        {
            return new ExternalIncludeDescriptionCollection<TExt, TId>(descriptions
                .Where(ad => ad != null)
                .Select(ad => new KeyValuePair<IncludeApplicationDescription, IExternalIncludeFunctionalDescription<TExt, TId>>(
                    ad,
                    _functionalDescriptionCache.GetOrAdd(ad.ContainedEntity, pi => MakeFunctionalDescription(ad)) as IExternalIncludeFunctionalDescription<TExt, TId>))
                .ToList());
        }

        [NotNull]
        private IEnumerable<TId> ExtractIds<TExt, TId>(
            [NotNull]ExternalIncludeDescriptionCollection<TExt, TId> descriptions)
        {
            return descriptions.Descriptions
                .Where(eid => eid.Key != null && eid.Key.DbEntities != null && eid.Value != null)
                .SelectMany(eid => eid.Key.DbEntities.Select(e => eid.Value.GetId(e)).Where(e => e.Key).Select(e => e.Value))
                .Distinct();
        }

        private void Set<TExt, TId>(
            [NotNull] ExternalIncludeDescriptionCollection<TExt, TId> descriptions,
            [NotNull] IEnumerable<TExt> entities)
            where TExt: class, IExternalEntity<TId>
        {
            var lookup = entities.Where(e => e != null).ToDictionary(e => e.Id);
            foreach (var description in descriptions.Descriptions)
            {
                if (description.Key == null || description.Key.DbEntities == null || description.Value == null)
                {
                    continue;
                }
                foreach (var entity in description.Key.DbEntities)
                {
                    var id = description.Value.GetId(entity);
                    if (id.Key && id.Value != null && lookup.ContainsKey(id.Value))
                    {
                        description.Value.Set(entity, lookup[id.Value]);
                    }
                }
            }
        }
        
        private void RetrieveAndSet<TExt, TId>(
            [NotNull]IEnumerable<IncludeApplicationDescription> appDescriptions)
            where TExt : class, IExternalEntity<TId>
        {
            var descriptions = GetFunctionalDescriptions<TExt, TId>(appDescriptions);
            var ids = ExtractIds(descriptions); 
            var repository = _repositoryFactory.GetExternalRepository<TExt, TId>();
            var entities = repository.Get(ids);
            Set(descriptions, entities);
        }

        private async Task RetrieveAndSetAsync<TExt, TId>(
            [NotNull] IEnumerable<IncludeApplicationDescription> appDescriptions)
            where TExt : class, IExternalEntity<TId>
        {
            var descriptions = GetFunctionalDescriptions<TExt, TId>(appDescriptions);
            var ids = ExtractIds(descriptions);
            var repository = _repositoryFactory.GetExternalRepository<TExt, TId>();
            var asyncRepository = repository as IAsyncExternalRepository<TExt, TId>;
            var entities = asyncRepository == null
                ? repository.Get(ids)
                : await asyncRepository.GetAsync(ids).ConfigureAwait(false);
            Set(descriptions, entities.ThrowIfNull("entities"));
        }

        public void ProcessExternalIncludes<T>(
            Type externalType, 
            IEnumerable<T> entities,
            IEnumerable<IncludeApplicationDescription> applicationDescriptions)
            where T : class
        {
            var @delegate = _externalRetrieveDelegateCache.GetOrAdd(
                externalType,
                t =>
                {
                    var externalIdType = _idTypeCache.GetOrAdd(externalType, GetExternalIdType);
                    var method = _externalRetrieveMethod.MakeGenericMethod(externalType, externalIdType);
                    return DelegateCreator.Create<Action<IEnumerable<IncludeApplicationDescription>>>(this, method);
                }).ThrowIfNull();
            @delegate(applicationDescriptions);
        }

        public Task ProcessExternalIncludesAsync<T>(
            Type externalType,
            IEnumerable<T> entities,
            IEnumerable<IncludeApplicationDescription> applicationDescriptions)
            where T : class
        {
            var @delegate = _externalRetrieveAsyncDelegateCache.GetOrAdd(
                externalType,
                t =>
                {
                    var externalIdType = _idTypeCache.GetOrAdd(externalType, GetExternalIdType);
                    var method = _externalRetrieveAsyncMethod.MakeGenericMethod(externalType, externalIdType);
                    return DelegateCreator.Create<Func<IEnumerable<IncludeApplicationDescription>, Task>>(this, method);
                }).ThrowIfNull();
            return @delegate(applicationDescriptions);
        }

        private Type GetExternalIdType([NotNull]Type arg)
        {
            var interfaces = arg.GetTypeInfo().GetInterfaces().Where(i => i != null && i.GetTypeInfo().IsGenericType && i.GetGenericTypeDefinition() == typeof(IExternalEntity<>)).ToList();
            if (interfaces.Count != 1 || interfaces[0] == null)
            {
                throw new InvalidOperationException(string.Format("Type {0} can not be external because it implements the IExternalEntity interface {1} times", arg, interfaces.Count));
            }
            return interfaces[0].GetTypeInfo().GetGenericArguments()[0];
        }

        [NotNull]
        private object MakeFunctionalDescription([NotNull]IncludeApplicationDescription description)
        {
            var entityType = description.IdSelector.DeclaringType;
            var externalType = description.ContainedEntity.ValueType;
            var isSelectorNullable = description.IdSelector.ValueType.IsNullableStruct();
            var externalIdType = isSelectorNullable
                ? description.IdSelector.ValueType.NullableStructUnderlyingType()
                : description.IdSelector.ValueType;
            var descriptionType = typeof(ExternalIncludeFunctionalDescription<,,>).MakeGenericType(
                entityType, externalType, externalIdType);
            var param = Expression.Parameter(entityType);
            var extParam = Expression.Parameter(externalType);
            var kvGenericArgs = new[] {typeof(bool), externalIdType};
            var kvType = typeof(KeyValuePair<,>).MakeGenericType(kvGenericArgs);
            var constructor = kvType.GetTypeInfo().GetConstructor(kvGenericArgs);
            if (constructor == null)
            {
                throw new InvalidOperationException(string.Format("{0} constructor with bool and {1} arguments not found", kvType, externalIdType));
            }
            var result = Activator.CreateInstance(descriptionType, new object[]
                {
                    Expression.Lambda(
                        typeof(Func<,>).MakeGenericType(entityType, kvType),
                        Expression.New(
                            constructor, 
                            isSelectorNullable 
                                ? Expression.NotEqual(Expression.MakeMemberAccess(param, description.IdSelector.Member), Expression.Constant(null))
                                : (Expression)Expression.Constant(true),
                            isSelectorNullable 
                                ? Expression.Coalesce(Expression.MakeMemberAccess(param, description.IdSelector.Member), Expression.Default(externalIdType))
                                : (Expression)Expression.MakeMemberAccess(param, description.IdSelector.Member)),
                        param),
                    Expression.Lambda(
                        typeof(Action<,>).MakeGenericType(entityType, externalType),
                        Expression.Assign(
                            Expression.MakeMemberAccess(param, description.ContainedEntity.Member),
                            extParam),
                        param, extParam)
                });
            return result;
        }
    }
}
