﻿namespace TsSoft.EntityRepository.External
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    /// Репозиторий внешних сущностей, поддерживающий асинхронные операции
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    /// <typeparam name="TId">Тип идентификатора</typeparam>
    public interface IAsyncExternalRepository<TEntity, in TId> : IExternalRepository<TEntity, TId>
        where TEntity : IExternalEntity<TId>
    {
        /// <summary>
        /// Получить сущности по идентификаторам
        /// </summary>
        [NotNull]
        Task<IEnumerable<TEntity>> GetAsync(IEnumerable<TId> ids);
    }
}