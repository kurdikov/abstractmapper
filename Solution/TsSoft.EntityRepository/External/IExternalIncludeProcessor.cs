﻿namespace TsSoft.EntityRepository.External
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.Expressions.IncludeBuilder.Models;

    /// <summary>
    /// Обработчик внешних инклюдов
    /// </summary>
    public interface IExternalIncludeProcessor
    {
        /// <summary>
        /// Обработать внешние инклюды
        /// </summary>
        /// <typeparam name="T">Тип БД-сущности</typeparam>
        /// <param name="externalType">Тип внешней сущности</param>
        /// <param name="entities">Извлечённые из БД сущности</param>
        /// <param name="descriptions">Описания применения инклюдов</param>
        void ProcessExternalIncludes<T>(
            [NotNull]Type externalType, 
            [NotNull]IEnumerable<T> entities, 
            [NotNull]IEnumerable<IncludeApplicationDescription> descriptions)
            where T : class;
        
        /// <summary>
        /// Обработать внешние инклюды
        /// </summary>
        /// <typeparam name="T">Тип БД-сущности</typeparam>
        /// <param name="externalType">Тип внешней сущности</param>
        /// <param name="entities">Извлечённые из БД сущности</param>
        /// <param name="descriptions">Описания применения инклюдов</param>
        [NotNull]
        Task ProcessExternalIncludesAsync<T>(
            [NotNull]Type externalType,
            [NotNull]IEnumerable<T> entities,
            [NotNull]IEnumerable<IncludeApplicationDescription> descriptions)
            where T : class;
    }
}
