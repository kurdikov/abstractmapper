﻿namespace TsSoft.EntityRepository.External
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    /// Репозиторий внешних сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    /// <typeparam name="TId">Тип идентификатора</typeparam>
    public interface IExternalRepository<out TEntity, in TId>
        where TEntity: IExternalEntity<TId>
    {
        /// <summary>
        /// Получить сущности по идентификаторам
        /// </summary>
        [NotNull]
        IEnumerable<TEntity> Get(IEnumerable<TId> ids);
    }

    /// <summary>
    /// Репозиторий внешних сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IExternalRepository<out TEntity> : IExternalRepository<TEntity, Guid>
        where TEntity : IExternalEntity<Guid>
    {
    }
}
