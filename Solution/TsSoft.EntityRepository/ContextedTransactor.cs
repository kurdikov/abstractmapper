﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Data;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;

    /// <summary>
    /// Позволяет получать транзакцию
    /// </summary>
    public class ContextedTransactor : Transactor, IContextedTransactor
    {
        [NotNull] private readonly IItemWrapper<DbContext> _dbWrapper;

        /// <summary>
        /// Позволяет получать транзакцию
        /// </summary>
        public ContextedTransactor([NotNull] IItemWrapper<DbContext> dbWrapper)
        {
            if (dbWrapper == null) throw new ArgumentNullException("dbWrapper");
            _dbWrapper = dbWrapper;
        }

        /// <summary>
        /// Начать транзакцию
        /// </summary>
        /// <param name="level"><see cref="IsolationLevel"/> транзакции</param>
        /// <returns>Транзакция, реализующая <see cref="ITsSoftDatabaseTransaction"/></returns>
        public IDbTransaction Begin(IsolationLevel level)
        {
            return Begin(_dbWrapper.Current, level);
        }
    }
}
