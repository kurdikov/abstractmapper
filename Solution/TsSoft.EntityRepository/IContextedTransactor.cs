﻿namespace TsSoft.EntityRepository
{
    using System.Data;
    using JetBrains.Annotations;

    /// <summary>
    /// Позволяет получать транзакцию
    /// </summary>
    public interface IContextedTransactor
    {
        /// <summary>
        /// Начать транзакцию
        /// </summary>
        /// <param name="level"><see cref="IsolationLevel"/> транзакции</param>
        /// <returns>Транзакция, реализующая <see cref="ITsSoftDatabaseTransaction"/></returns>
        [NotNull]
        IDbTransaction Begin(IsolationLevel level);
    }
}
