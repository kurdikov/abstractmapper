namespace TsSoft.EntityRepository
{
    using System.Data;

    /// <summary>
    /// ����������, ����������� <see cref="IDbTransaction"/>
    /// � ������ �������������� TsSoft
    /// </summary>
    public interface ITsSoftDatabaseTransaction : IDbTransaction
    {
    }
}