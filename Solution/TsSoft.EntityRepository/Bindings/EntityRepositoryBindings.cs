﻿namespace TsSoft.Bindings
{
    using TsSoft.BindingsDescription;
    using TsSoft.EntityRepository;
    using TsSoft.EntityRepository.External;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.EntityRepository.Helpers.Clone;
    using TsSoft.EntityRepository.Helpers.Remove;
    using TsSoft.EntityRepository.IncludeProcessors;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.EntityRepository
    /// </summary>
    public class EntityRepositoryBindings : BindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке TsSoft.EntityRepository
        /// </summary>
        public EntityRepositoryBindings()
        {
            Bindings.Add(typeof(ICreateRepository<>), typeof(CreateRepository<>));
            Bindings.Add(typeof(IReadRepository<>), typeof(ReadRepository<>));
            Bindings.Add(typeof(IUpdateRepository<>), typeof(UpdateRepository<>));
            Bindings.Add(typeof(IDeleteRepository<>), typeof(DeleteRepository<>));
            Bindings.Add(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            Bind<IDatabaseCommitter, DatabaseCommitter>();
            Bind<ITransactor, Transactor>();
            Bind<IContextedTransactor, ContextedTransactor>();

            Bindings.Add(typeof(IComplexReadRepository<>), typeof(ComplexEntityReadRepository<>));
            Bindings.Add(typeof(IComplexDeleteRepository<>), typeof(ComplexDeleteRepository<>));
            Bindings.Add(typeof(IComplexUpdateRepository<>), typeof(ComplexUpdateRepository<>));
            Bindings.Add(typeof(IComplexEntityRepository<>), typeof(ComplexEntityRepository<>));


            Bindings.Add(typeof(IDeletableEntityDeleteRepository<>), typeof(DeletableEntityDeleteRepository<>));
            Bindings.Add(typeof(IDeletableEntityReadRepository<>), typeof(DeletableEntityReadRepository<>));
            Bindings.Add(typeof(IDeletableEntityRepository<>), typeof(DeletableEntityRepository<>));


            Bindings.Add(typeof(IComplexDeletableEntityDeleteRepository<>), typeof(ComplexDeletableEntityDeleteRepository<>));
            Bindings.Add(typeof(IComplexDeletableEntityUpdateRepository<>), typeof(ComplexDeletableEntityUpdateRepository<>));
            Bindings.Add(typeof(IComplexDeletableEntityReadRepository<>), typeof(ComplexDeletableEntityReadRepository<>));
            Bindings.Add(typeof(IComplexDeletableEntityRepository<>), typeof(ComplexDeletableEntityRepository<>));

            Bind<IExternalIncludeProcessor, ExternalIncludeProcessor>();
            Bind<IRepositoryHelper, RepositoryHelper>();
            Bindings.Add(typeof(IEntityClonemaker<>), typeof(EntityClonemaker<>));
            Bind<IConditionalIncludeProcessor, ConditionalIncludeProcessor>();
            Bind<IUniversalIncludeProcessor, UniversalIncludeProcessor>();
            Bind<IEntityRemover, EntityRemover>();
            Bind<IDetacher, Detacher>();

            Bind<ICollectionRemover, CollectionRemover>();
            Bind<IEntityRemoverSelectCreator, EntityRemoverSelectCreator>();
            Bind<IEntityRemoverSqlCreator, EntityRemoverSqlCreator>();
            Bind<IDeletedEntitiesDescriptionCreator, DeletedEntitiesDescriptionCreator>();
            Bind<IIdStringifier, IdStringifier>();
        }
    }
}
