﻿namespace TsSoft.EntityRepository
{
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    ///     Сохраняет изменения в бд
    /// </summary>
    public interface IDatabaseCommitter
    {
        /// <summary>
        ///     Сохранить изменения в бд
        /// </summary>
        /// <returns>Количество объектов, записанных в бд</returns>
        int SaveChanges();

        /// <summary>
        ///     Асинхронно сохранить изменения в бд
        /// </summary>
        /// <returns>
        ///     <see cref="Task" /> количества объектов, записанных в бд
        /// </returns>
        [NotNull]
        Task<int> SaveChangesAsync();
    }
}