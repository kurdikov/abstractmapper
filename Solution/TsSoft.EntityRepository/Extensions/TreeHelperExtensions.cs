﻿// ReSharper disable CheckNamespace
namespace TsSoft.Expressions.Helpers.Collections
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    /// Расширения хелпера для древесных структур
    /// </summary>
    public static class TreeHelperExtensions
    {
        /// <summary>
        ///     Заполняет навигационные поля сущности с древесной структурой
        /// </summary>
        /// <typeparam name="T">Сущность</typeparam>
        /// <typeparam name="TId">Тип идентификатора сущности (уникального для сущности)</typeparam>
        /// <param name="treeHelper">Хелпер для древесных структур</param>
        /// <param name="treeElements">Выбранные сущности</param>
        /// <param name="parentIdSelector">Функция, возвращающая идентификатор родителя сущности</param>
        /// <param name="parentSetter">Функция, заполняющая поле родителя сущности ( (child, parent) => child.Parent = parent )</param>
        /// <param name="childrenCollectionSelector">Функция, возвращающая коллекцию детей сущности</param>
        /// <returns>Сущности с заполненными навигационными полями</returns>
        public static IEnumerable<T> ConstructTreeStructure<T, TId>(
            [NotNull] this ITreeHelper treeHelper,
            [NotNull]IEnumerable<T> treeElements,
            [NotNull]Func<T, TId?> parentIdSelector,
            [NotNull]Action<T, T> parentSetter,
            [NotNull]Func<T, ICollection<T>> childrenCollectionSelector)
            where T : class, IEntityWithId<TId>
            where TId : struct
        {
            return treeHelper.ConstructTreeStructure(
                treeElements,
                arg => arg.Id,
                parentIdSelector,
                parentSetter,
                childrenCollectionSelector);
        }

        /// <summary>
        ///     Заполняет навигационные поля сущности с древесной структурой
        /// </summary>
        /// <typeparam name="T">Сущность</typeparam>
        /// <typeparam name="TId">Тип идентификатора сущности (уникального для сущности)</typeparam>
        /// <param name="treeHelper">Хелпер для древесных структур</param>
        /// <param name="treeElements">Выбранные сущности</param>
        /// <param name="idSelector">Функция, возвращающая идентификатор сущности</param>
        /// <param name="parentIdSelector">Функция, возвращающая идентификатор родителя сущности</param>
        /// <param name="parentSetter">Функция, заполняющая поле родителя сущности ( (child, parent) => child.Parent = parent )</param>
        /// <returns>Сущности с заполненными навигационными полями</returns>
        public static IEnumerable<T> ConstructTreeStructure<T, TId>(
            [NotNull] this ITreeHelper treeHelper,
            [NotNull]IEnumerable<T> treeElements,
            [NotNull]Func<T, TId> idSelector,
            [NotNull]Func<T, TId?> parentIdSelector,
            [NotNull]Action<T, T> parentSetter)
            where T : class, IWithChildren<T>
            where TId : struct
        {
            return treeHelper.ConstructTreeStructure(
                treeElements,
                idSelector,
                parentIdSelector,
                parentSetter,
                arg => arg.Children);
        }

        /// <summary>
        ///     Заполняет навигационные поля сущности с древесной структурой
        /// </summary>
        /// <typeparam name="T">Сущность</typeparam>
        /// <typeparam name="TId">Тип идентификатора сущности (уникального для сущности)</typeparam>
        /// <param name="treeHelper">Хелпер для древесных структур</param>
        /// <param name="treeElements">Выбранные сущности</param>
        /// <param name="idSelector">Функция, возвращающая идентификатор сущности</param>
        /// <param name="parentIdSelector">Функция, возвращающая идентификатор родителя сущности</param>
        /// <param name="childrenCollectionSelector">Функция, возвращающая коллекцию детей сущности</param>
        /// <returns>Сущности с заполненными навигационными полями</returns>
        public static IEnumerable<T> ConstructTreeStructure<T, TId>([NotNull] this ITreeHelper treeHelper,
                                                                    [NotNull]IEnumerable<T> treeElements,
                                                                    [NotNull]Func<T, TId> idSelector,
                                                                    [NotNull]Func<T, TId?> parentIdSelector,
                                                                    [NotNull]Func<T, ICollection<T>> childrenCollectionSelector)
            where T : class, IHasParent<T>
            where TId : struct
        {
            return treeHelper.ConstructTreeStructure(
                treeElements,
                idSelector, 
                parentIdSelector,
                (child, parent) => child.Parent = parent,
                childrenCollectionSelector);
        }

        /// <summary>
        ///     Заполняет навигационные поля сущности с древесной структурой
        /// </summary>
        /// <typeparam name="T">Сущность</typeparam>
        /// <typeparam name="TId">Тип идентификатора сущности (уникального для сущности)</typeparam>
        /// <param name="treeHelper">Хелпер для древесных структур</param>
        /// <param name="treeElements">Выбранные сущности</param>
        /// <param name="parentIdSelector">Функция, возвращающая идентификатор родителя сущности</param>
        /// <returns>Сущности с заполненными навигационными полями</returns>
        public static IEnumerable<T> ConstructTreeStructure<T, TId>(
            [NotNull] this ITreeHelper treeHelper,
            [NotNull]IEnumerable<T> treeElements,
            [NotNull]Func<T, TId?> parentIdSelector)
            where T : class, IHasParent<T>, IWithChildren<T>, IEntityWithId<TId>
            where TId : struct
        {
            return treeHelper.ConstructTreeStructure(
                treeElements,
                x => x.Id,
                parentIdSelector,
                (child, parent) => child.Parent = parent,
                arg => arg.Children);
        }

        /// <summary>
        ///     Заполняет навигационные поля сущности с древесной структурой
        /// </summary>
        /// <typeparam name="T">Сущность</typeparam>
        /// <typeparam name="TId">Тип идентификатора сущности (уникального для сущности)</typeparam>
        /// <param name="treeHelper">Хелпер для древесных структур</param>
        /// <param name="treeElements">Выбранные сущности</param>
        /// <returns>Сущности с заполненными навигационными полями</returns>
        public static IEnumerable<T> ConstructTreeStructure<T, TId>(
            [NotNull] this ITreeHelper treeHelper,
            [NotNull]IEnumerable<T> treeElements)
            where T : class, ITreeElement<T, TId>
            where TId : struct
        {
            return treeHelper.ConstructTreeStructure(
                treeElements,
                x => x.Id,
                x => x.ParentId,
                (child, parent) => child.Parent = parent,
                arg => arg.Children);
        }

        /// <summary>
        /// Сортирует элементы древесной структуры в виде плоского дерева (1, 2, 3, 1.1, 1.1.2,2.1) -> (1, 1.1, 1.1.2, 2, 2.1, 3)
        /// </summary>
        /// <typeparam name="T">Сущность</typeparam>
        /// <typeparam name="TId">Идентификатор сущности</typeparam>
        /// <typeparam name="TKey">Тип ключа сущности</typeparam>
        /// <param name="treeHelper">Хелпер для древесных структур</param>
        /// <param name="treeElements"></param>
        /// <param name="compareFieldSelector">Селектор поля, по которому идет сортировка</param>
        /// <param name="postprocessor">Постпроцессор отсортированных сущностей</param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static IEnumerable<T> OrderByHierarchy<T, TKey, TId>(
            [NotNull]this ITreeHelper treeHelper,
            [NotNull]IEnumerable<T> treeElements,
            [NotNull]Func<T, TKey> compareFieldSelector,
            Action<T> postprocessor,
            IComparer<TKey> comparer) 
            where T : class, ITreeElement<T, TId>
            where TId : struct
        {
            return treeHelper.OrderByHierarchy(treeElements, i => i.Id, arg => arg.ParentId, arg => arg.Children,
                                    (child, parent) => child.Parent = parent, compareFieldSelector, postprocessor,
                                    comparer);
        }
    }
}
