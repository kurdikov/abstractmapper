﻿namespace TsSoft.EntityRepository
{
    using Interfaces;

    /// <summary>
    /// Репозиторий сложных удаляемых сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IComplexDeletableEntityRepository<TEntity>
        : IComplexEntityRepository<TEntity>, 
        IDeletableEntityRepository<TEntity>,
        IComplexDeletableEntityReadRepository<TEntity>
        where TEntity : class, IComplexEntity, IDeletable
    {
    }
}