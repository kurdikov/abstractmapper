namespace TsSoft.EntityRepository
{
    using System;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Infrastructure;
#endif
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;

    /// <summary>
    ///     �������� ��
    /// </summary>
    public abstract class DatabaseWrapper
    {
        /// <summary>
        /// EF-��������
        /// </summary>
        [NotNull] protected readonly IItemWrapper<DbContext> Wrapper;

        /// <summary>
        ///     �������� ��
        /// </summary>
        protected DatabaseWrapper([NotNull] IItemWrapper<DbContext> contextWrapper)
        {
            if (contextWrapper == null) throw new ArgumentNullException("contextWrapper");
            Wrapper = contextWrapper;
        }

        /// <summary>
        ///     ���������� ��������
        /// </summary>
        [NotNull]
        public DbContext Context
        {
            get { return Wrapper.Current; }
        }

        /// <summary>
        ///     ���������� <see cref="Database" />, ������������� �������� ���������
        /// </summary>
        [NotNull]
#if NET45
        protected Database Database([NotNull]DbContext context)
#elif NETSTANDARD15
        protected DatabaseFacade Database([NotNull]DbContext context)
#endif
        {
            var database = context.Database;
            if (database == null)
            {
                throw new InvalidOperationException("Context.Database is null");
            }
            return database;
        }
    }
}
