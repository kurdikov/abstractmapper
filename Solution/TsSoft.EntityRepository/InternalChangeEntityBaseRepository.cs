﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Data;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.IncludeProcessors;

    /// <summary>
    ///     Репозиторий, умеющий сохранять сущности
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public abstract class InternalChangeEntityBaseRepository<TEntity> : InternalBaseRepository<TEntity>, IDatabaseCommitter, IContextedTransactor
        where TEntity : class
    {
        /// <summary>
        /// Удаляет сущности из контекста в памяти
        /// </summary>
        [NotNull] protected readonly IDetacher Detacher;
        [NotNull] private readonly IDatabaseCommitter _databaseCommitter;
        [NotNull] private readonly ITransactor _transactor;

        /// <summary>
        ///     Репозиторий, умеющий сохранять сущности
        /// </summary>
        protected InternalChangeEntityBaseRepository(
            [NotNull] IItemWrapper<DbContext> contextWrapper,
            [NotNull] IUniversalIncludeProcessor universalIncludeProcessor,
            [NotNull] IDatabaseCommitter databaseCommitter,
            [NotNull] ITransactor transactor,
            [NotNull] IDetacher detacher)
            : base(contextWrapper, universalIncludeProcessor)
        {
            if (databaseCommitter == null) throw new ArgumentNullException("databaseCommitter");
            if (transactor == null) throw new ArgumentNullException("transactor");
            if (detacher == null) throw new ArgumentNullException("detacher");
            _databaseCommitter = databaseCommitter;
            _transactor = transactor;
            Detacher = detacher;
        }

        /// <summary>
        ///     Сохранить изменения в бд
        /// </summary>
        /// <returns>Количество объектов, записанных в бд</returns>
        public int SaveChanges()
        {
            return _databaseCommitter.SaveChanges();
        }

        /// <summary>
        ///     Асинхронно сохранить изменения в бд
        /// </summary>
        /// <returns>
        ///     <see cref="Task" /> количества объектов, записанных в бд
        /// </returns>
        public Task<int> SaveChangesAsync()
        {
            return _databaseCommitter.SaveChangesAsync();
        }

        /// <summary>
        ///     Начать транзакцию
        /// </summary>
        /// <param name="level">
        ///     <see cref="IsolationLevel" /> транзакции
        /// </param>
        /// <returns>
        ///     Транзакция, реализующая <see cref="ITsSoftDatabaseTransaction" />
        /// </returns>
        public IDbTransaction Begin(IsolationLevel level)
        {
            var ctx = Context;
            return _transactor.Begin(ctx, level);
        }
    }
}
