﻿namespace TsSoft.EntityRepository
{
    using System.Data;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using JetBrains.Annotations;
    
    /// <summary>
    /// Позволяет получать транзакцию
    /// </summary>
    public interface ITransactor
    {
        /// <summary>
        /// Начать транзакцию
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="level"><see cref="IsolationLevel"/> транзакции</param>
        /// <returns>Транзакция, реализующая <see cref="ITsSoftDatabaseTransaction"/></returns>
        [NotNull]
        IDbTransaction Begin([NotNull]DbContext context, IsolationLevel level);
    }
}
