﻿namespace TsSoft.EntityRepository
{
    using Interfaces;

    /// <summary>
    /// Репозиторий сложных сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IComplexEntityRepository<TEntity> : 
        IComplexReadRepository<TEntity>,
        IComplexDeleteRepository<TEntity>,
        IComplexUpdateRepository<TEntity>,
        IBaseRepository<TEntity> 
        where TEntity : class, IComplexEntity
    {
        
    }


}