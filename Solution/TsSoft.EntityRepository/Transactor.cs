﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Data;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif

    /// <summary>
    /// Позволяет получать транзакцию
    /// </summary>
    public class Transactor : ITransactor
    {
        /// <summary>
        /// Начать транзакцию
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="level"><see cref="IsolationLevel"/> транзакции</param>
        /// <returns>Транзакция, реализующая <see cref="ITsSoftDatabaseTransaction"/></returns>
        public IDbTransaction Begin(DbContext context, IsolationLevel level)
        {
            var database = context.Database;
            if (database == null)
            {
                throw new InvalidOperationException("Database in DbContext is null");
            }
            var dbContextTransaction = database.BeginTransaction(level);
            if (dbContextTransaction == null)
            {
                throw new InvalidOperationException("BeginTransaction returned null");
            }
            return new TsSoftDbContextTransaction(dbContextTransaction, level);
        }
    }
}
