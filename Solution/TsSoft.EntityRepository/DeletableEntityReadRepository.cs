﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Mappers;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.EntityRepository.IncludeProcessors;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.Helpers.Async;
    using TsSoft.Expressions.IncludeBuilder;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    ///     Репозиторий получения удаляемых сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public class DeletableEntityReadRepository<TEntity> : ReadRepository<TEntity>, IDeletableEntityReadRepository<TEntity>
            where TEntity : class, IDeletable
    {
        [NotNull] private readonly IRepositoryHelper _repositoryHelper;


        /// <summary>
        ///     Репозиторий получения удаляемых сущностей
        /// </summary>
        public DeletableEntityReadRepository([NotNull] IItemWrapper<DbContext> contextWrapper,
                                             [NotNull] IUniversalIncludeProcessor universalIncludeProcessor,
                                             [NotNull] IDynamicEntityToEntityMapper<TEntity> dynamicMapper,
                                             [NotNull] IIncludeDescriptionHelper includeDescriptionHelper,
                                             [NotNull] IRepositoryHelper repositoryHelper)
                : base(contextWrapper, universalIncludeProcessor, dynamicMapper, includeDescriptionHelper)
        {
            if (repositoryHelper == null) throw new ArgumentNullException("repositoryHelper");
            _repositoryHelper = repositoryHelper;
        }

        /// <summary>
        ///     Получить коллекцию сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetWithDeleted(Expression<Func<TEntity, bool>> @where,
                                                           [NotNull] IReadOnlySelectExpression<TEntity> @select,
                                                           IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return AsyncHelper.RunSync(() => GetWithDeletedAsync(@where, select, orderBy));
        }

        /// <summary>
        ///     Получить коллекцию сущностей (включая удаленные)
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetWithDeleted(Expression<Func<TEntity, bool>> @where,
                                                           IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                           IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                           IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return AsyncHelper.RunSync(() => GetWithDeletedAsync(where, include, orderBy, includeConditions));
        }

        /// <summary>
        ///     Получить сущность, возможно, удаленную
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="include">Инклюды</param>
        /// <param name="includeConditions"></param>
        public TEntity GetSingleWithDeleted(Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return AsyncHelper.RunSync(() => GetSingleWithDeletedAsync(where, include, includeConditions));
        }

        /// <summary>
        ///     Получить одну сущность с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <returns>Сущность</returns>
        public TEntity GetSingleWithDeleted(Expression<Func<TEntity, bool>> @where, [NotNull] IReadOnlySelectExpression<TEntity> @select)
        {
            return AsyncHelper.RunSync(() => GetSingleWithDeletedAsync(@where, select));
        }

        /// <summary>
        ///     Получить сущность, возможно, удаленную
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="include">Инклюды</param>
        /// <param name="includeConditions"></param>
        public Task<TEntity> GetSingleWithDeletedAsync(Expression<Func<TEntity, bool>> @where,
                                                       IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                       IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return GetSingleAsync(ctx, where, include, includeConditions, false);
        }

        /// <summary>
        ///     Получить коллекцию сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetWithDeletedAsync(Expression<Func<TEntity, bool>> @where,
                                                                      [NotNull] IReadOnlySelectExpression<TEntity> @select,
                                                                      IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            if (@select == null) throw new ArgumentNullException("select");
            var ctx = Context;
            return GetAsync(ctx, where, select, orderBy, false);
        }

        /// <summary>
        ///     Получить количество сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Количество сущностей по предикату</returns>
        public int CountWithDeleted(Expression<Func<TEntity, bool>> @where)
        {
            return AsyncHelper.RunSync(() => CountWithDeletedAsync(where));
        }

        /// <summary>
        ///     Существует ли сущность в хранилище, возможно, удаленная
        /// </summary>
        /// <param name="where">Предикат</param>
        public bool ExistsWithDeleted(Expression<Func<TEntity, bool>> @where)
        {
            return AsyncHelper.RunSync(() => ExistsWithDeletedAsync(where));
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public IPage<TEntity> GetPagedWithDeleted(Expression<Func<TEntity, bool>> @where, int pageNumber, int pageSize,
                                                             IReadOnlySelectExpression<TEntity> @select, IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            return AsyncHelper.RunSync(() => GetPagedWithDeletedAsync(where, pageNumber, pageSize, select, orderBy));
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности с удаленными
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какий подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetTrackedWithDeleted(Expression<Func<TEntity, bool>> @where,
                                                                  IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                                  IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                                  IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return AsyncHelper.RunSync(() => GetTrackedWithDeletedAsync(where, include, orderBy, includeConditions));
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище (включая удаленные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Максимальное значение поля или null</returns>
        public T? MaxWithDeleted<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return AsyncHelper.RunSync(() => MaxWithDeletedAsync(selector, where));
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище (включая удаленные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Минимальное значение поля или null</returns>
        public T? MinWithDeleted<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return AsyncHelper.RunSync(() => MinWithDeletedAsync(selector, where));
        }

        /// <summary>
        ///     Получить количество сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - количество сущностей по предикату</returns>
        public Task<int> CountWithDeletedAsync(Expression<Func<TEntity, bool>> @where)
        {
            var ctx = Context;
            return CountAsync(ctx, where, false);
        }

        /// <summary>
        ///     Существует ли сущность в хранилище, возможно, удаленная
        /// </summary>
        /// <param name="where">Предикат</param>
        public Task<bool> ExistsWithDeletedAsync(Expression<Func<TEntity, bool>> @where)
        {
            var ctx = Context;
            return ExistsAsync(ctx, where, false);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public Task<IPage<TEntity>> GetPagedWithDeletedAsync(Expression<Func<TEntity, bool>> @where, int pageNumber, int pageSize,
                                                                        IReadOnlySelectExpression<TEntity> @select,
                                                                        IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            var ctx = Context;
            return GetPagedAsync(ctx, where, pageNumber, pageSize, select, orderBy, false);
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности с удаленными
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetTrackedWithDeletedAsync(Expression<Func<TEntity, bool>> @where,
                                                                             IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                                             IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                                             IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return GetTrackedAsync(ctx, where, false, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище (включая удаленные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - максимальное значение поля или null</returns>
        public Task<T?> MaxWithDeletedAsync<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            var ctx = Context;
            return MaxAsync(ctx, selector, where, false);
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище (включая удаленные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - минимальное значение поля или null</returns>
        public Task<T?> MinWithDeletedAsync<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            var ctx = Context;
            return MinAsync(ctx, selector, where, false);
        }

        /// <summary>
        ///     Получить коллекцию сущностей (включая удаленные)
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetWithDeletedAsync(Expression<Func<TEntity, bool>> @where,
                                                                      IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                                      IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                                      IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return GetAsync(ctx, where, false, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить одну сущность с удаленными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <returns>Задача, результат которой - сущность</returns>
        public Task<TEntity> GetSingleWithDeletedAsync(Expression<Func<TEntity, bool>> @where, [NotNull] IReadOnlySelectExpression<TEntity> @select)
        {
            if (@select == null) throw new ArgumentNullException("select");
            var ctx = Context;
            return GetSingleAsync(ctx, where, select, false);
        }


        /// <summary>
        ///     Преобразовать условие выборки согласно логике репозитория
        /// </summary>
        /// <param name="where">Исходное условие</param>
        /// <returns>Преобразованное условие</returns>
        protected override Expression<Func<TEntity, bool>> GetWhereTransformer(Expression<Func<TEntity, bool>> where)
        {
            return _repositoryHelper.WhereToNotDeletedWhere(where);
        }
    }
}