﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Mappers;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.EntityRepository.IncludeProcessors;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.Helpers.Async;
    using TsSoft.Expressions.IncludeBuilder;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    ///     Репозиторий получения сложных сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public class ComplexEntityReadRepository<TEntity> : ReadRepository<TEntity>, IComplexReadRepository<TEntity>
            where TEntity : class, IComplexEntity
    {
        [NotNull] private readonly IRepositoryHelper _repositoryHelper;

        /// <summary>
        ///     Репозиторий получения сложных сущностей
        /// </summary>
        public ComplexEntityReadRepository([NotNull] IItemWrapper<DbContext> contextWrapper,
                                           [NotNull] IUniversalIncludeProcessor universalIncludeProcessor,
                                           [NotNull] IDynamicEntityToEntityMapper<TEntity> dynamicMapper,
                                           [NotNull] IIncludeDescriptionHelper includeDescriptionHelper, [NotNull] IRepositoryHelper repositoryHelper)
                : base(contextWrapper, universalIncludeProcessor, dynamicMapper, includeDescriptionHelper)
        {
            if (repositoryHelper == null) throw new ArgumentNullException("repositoryHelper");
            _repositoryHelper = repositoryHelper;
        }

        /// <summary>
        ///     Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="include">Инклюды</param>
        /// <param name="includeConditions"></param>
        public TEntity GetSingleWithStubs(Expression<Func<TEntity, bool>> @where,
                                          IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                          IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return AsyncHelper.RunSync(() => GetSingleWithStubsAsync(where, include, includeConditions));
        }

        /// <summary>
        ///     Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="select">Селект</param>
        public TEntity GetSingleWithStubs(Expression<Func<TEntity, bool>> @where, [NotNull] IReadOnlySelectExpression<TEntity> @select)
        {
            return AsyncHelper.RunSync(() => GetSingleWithStubsAsync(where, select));
        }

        /// <summary>
        ///     Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="include">Инклюды</param>
        /// <param name="includeConditions"></param>
        public async Task<TEntity> GetSingleWithStubsAsync(Expression<Func<TEntity, bool>> where,
                                                           IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                           IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return await GetSingleAsync(ctx, where, include, includeConditions, false).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить сущность, возможно, несохранённую
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="select">Селект</param>
        public async Task<TEntity> GetSingleWithStubsAsync(Expression<Func<TEntity, bool>> where, [NotNull] IReadOnlySelectExpression<TEntity> select)
        {
            var ctx = Context;
            return await GetSingleAsync(ctx, where, select, false).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить коллекцию сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetWithStubs(Expression<Func<TEntity, bool>> @where, [NotNull] IReadOnlySelectExpression<TEntity> @select,
                                                         IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            if (@select == null) throw new ArgumentNullException("select");
            return AsyncHelper.RunSync(() => GetWithStubsAsync(where, select, orderBy));
        }

        /// <summary>
        ///     Получить коллекцию сущностей (включая несохраненные)
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetWithStubs(Expression<Func<TEntity, bool>> @where,
                                                         IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                         IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                         IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return AsyncHelper.RunSync(() => GetWithStubsAsync(where, include, orderBy, includeConditions));
        }

        /// <summary>
        ///     Получить коллекцию сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetWithStubsAsync(Expression<Func<TEntity, bool>> @where,
                                                                    [NotNull] IReadOnlySelectExpression<TEntity> @select,
                                                                    IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            if (@select == null) throw new ArgumentNullException("select");
            var ctx = Context;
            return GetAsync(ctx, where, select, orderBy, false);
        }

        /// <summary>
        ///     Получить количество сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Количество сущностей по предикату</returns>
        public int CountWithStubs(Expression<Func<TEntity, bool>> @where)
        {
            return AsyncHelper.RunSync(() => CountWithStubsAsync(where));
        }

        /// <summary>
        ///     Существует ли сущность в хранилище, возможно, несохраненная
        /// </summary>
        /// <param name="where">Предикат</param>
        public bool ExistsWithStubs(Expression<Func<TEntity, bool>> @where)
        {
            return AsyncHelper.RunSync(() => ExistsWithStubsAsync(where));
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public IPage<TEntity> GetPagedWithStubs(Expression<Func<TEntity, bool>> @where, int pageNumber, int pageSize,
                                                           IReadOnlySelectExpression<TEntity> @select, IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            return AsyncHelper.RunSync(() => GetPagedWithStubsAsync(where, pageNumber, pageSize, select, orderBy));
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности с несохраненными
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какий подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetTrackedWithStubs(Expression<Func<TEntity, bool>> @where,
                                                                IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                                IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                                IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return AsyncHelper.RunSync(() => GetTrackedWithStubsAsync(where, include, orderBy, includeConditions));
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище (включая несохраненные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Максимальное значение поля или null</returns>
        public T? MaxWithStubs<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return AsyncHelper.RunSync(() => MaxWithStubsAsync(selector, where));
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище (включая несохраненные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Минимальное значение поля или null</returns>
        public T? MinWithStubs<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            return AsyncHelper.RunSync(() => MinWithStubsAsync(selector, where));
        }

        /// <summary>
        ///     Получить количество сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - количество сущностей по предикату</returns>
        public Task<int> CountWithStubsAsync(Expression<Func<TEntity, bool>> @where)
        {
            var ctx = Context;
            return CountAsync(ctx, where, false);
        }

        /// <summary>
        ///     Существует ли сущность в хранилище, возможно, несохраненная
        /// </summary>
        /// <param name="where">Предикат</param>
        public Task<bool> ExistsWithStubsAsync(Expression<Func<TEntity, bool>> @where)
        {
            var ctx = Context;
            return ExistsAsync(ctx, where, false);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей с несохраненными
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public Task<IPage<TEntity>> GetPagedWithStubsAsync(Expression<Func<TEntity, bool>> @where, int pageNumber, int pageSize,
                                                                      IReadOnlySelectExpression<TEntity> @select,
                                                                      IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            var ctx = Context;
            return GetPagedAsync(ctx, where, pageNumber, pageSize, select, orderBy, false);
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности с несохраненными
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetTrackedWithStubsAsync(Expression<Func<TEntity, bool>> @where,
                                                                           IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                                           IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                                           IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return GetTrackedAsync(ctx, where, false, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище (включая несохраненные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - максимальное значение поля или null</returns>
        public Task<T?> MaxWithStubsAsync<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            var ctx = Context;
            return MaxAsync(ctx, selector, where, false);
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище (включая несохраненные)
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - минимальное значение поля или null</returns>
        public Task<T?> MinWithStubsAsync<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            var ctx = Context;
            return MinAsync(ctx, selector, where, false);
        }

        /// <summary>
        ///     Получить коллекцию сущностей (включая несохраненные)
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetWithStubsAsync(Expression<Func<TEntity, bool>> @where,
                                                                    IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                                    IEnumerable<IOrderByClause<TEntity>> orderBy = null,
                                                                    IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return GetAsync(ctx, where, false, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Преобразовать условие выборки согласно логике репозитория
        /// </summary>
        /// <param name="where">Исходное условие</param>
        /// <returns>Преобразованное условие</returns>
        protected override Expression<Func<TEntity, bool>> GetWhereTransformer(Expression<Func<TEntity, bool>> where)
        {
            return _repositoryHelper.WhereToNonStubWhere(where);
        }
    }
}