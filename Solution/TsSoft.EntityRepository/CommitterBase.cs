namespace TsSoft.EntityRepository
{
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;

    /// <summary>
    ///     �������� �� � ������������ ����������
    /// </summary>
    public abstract class CommitterBase : DatabaseWrapper
    {
        /// <summary>
        ///     �������� �� � ������������ ����������
        /// </summary>
        protected CommitterBase([NotNull] IItemWrapper<DbContext> contextWrapper)
            : base(contextWrapper)
        {
        }

        /// <summary>
        /// ��������� ��������� ��� ���������� �������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="commit">��������� �� �������</param>
        [NotNull]
        protected async Task<int> ConditionalCommitAsync([NotNull]DbContext context, bool commit)
        {
            if (!commit)
            {
                return -1;
            }
            var saveChangesTask = context.SaveChangesAsync();
            if (saveChangesTask != null)
            {
                return await saveChangesTask.ConfigureAwait(false);
            }
            return -1;
        }

        /// <summary>
        /// ��������� ��������� ��� ���������� �������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="commit">��������� �� �������</param>
        protected int ConditionalCommit([NotNull]DbContext context, bool commit)
        {
            if (!commit)
            {
                return -1;
            }
            return context.SaveChanges();
        }
    }
}
