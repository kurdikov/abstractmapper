﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.EntityRepository.IncludeProcessors;
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    ///     Репозиторий обновления сложных удаляемых сущностей
    /// </summary>
    public class ComplexDeletableEntityUpdateRepository<TEntity> : UpdateRepository<TEntity>, IComplexDeletableEntityUpdateRepository<TEntity>
        where TEntity : class, IComplexEntity, IDeletable
    {
        [NotNull] private readonly IRepositoryHelper _repositoryHelper;

        /// <summary>
        ///     Репозиторий обновления сложных удаляемых сущностей
        /// </summary>
        public ComplexDeletableEntityUpdateRepository([NotNull] IItemWrapper<DbContext> contextWrapper,
                                                      [NotNull] IUniversalIncludeProcessor universalIncludeProcessor,
                                                      [NotNull] IDatabaseCommitter databaseCommitter, [NotNull] ITransactor transactor,
                                                      [NotNull] IDetacher detacher, [NotNull] IRepositoryHelper repositoryHelper)
            : base(contextWrapper, universalIncludeProcessor, databaseCommitter, transactor, detacher)
        {
            if (repositoryHelper == null) throw new ArgumentNullException("repositoryHelper");
            _repositoryHelper = repositoryHelper;
        }

        /// <summary>
        ///     Преобразовать функцию обновления согласно логике репозитория
        /// </summary>
        /// <param name="updater">Исходная функция обновления</param>
        /// <returns>Преобразованная функция</returns>
        protected override Action<TEntity> UpdaterTransformer(Action<TEntity> updater)
        {
            return _repositoryHelper.UpdaterToStubUpdater(updater);
        }

        /// <summary>
        ///     Преобразовать функцию обновления согласно логике репозитория
        /// </summary>
        /// <param name="updater">Исходная функция обновления</param>
        /// <returns>Преобразованная функция</returns>
        protected override Action<TEntity, TEntity> UpdaterTransformer(Action<TEntity, TEntity> updater)
        {
            return _repositoryHelper.UpdaterToStubUpdater(updater);
        }


        /// <summary>
        ///     Преобразовать функцию обновления согласно логике репозитория
        /// </summary>
        /// <param name="updater">Исходная функция обновления</param>
        /// <returns>Преобразованная функция</returns>
        protected override Func<TEntity, TEntity, IEnumerable<Func<Task>>> UpdaterTransformer(Func<TEntity, TEntity, IEnumerable<Func<Task>>> updater)
        {
            return _repositoryHelper.UpdaterToStubUpdater(updater);
        }
    }
}