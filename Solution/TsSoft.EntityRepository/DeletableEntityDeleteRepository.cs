﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.IncludeProcessors;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Async;

    /// <summary>
    ///     Репозиторий удаления удаляемых сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public class DeletableEntityDeleteRepository<TEntity> : DeleteRepository<TEntity>, IDeletableEntityDeleteRepository<TEntity>
        where TEntity : class, IDeletable
    {
        [NotNull] private readonly IUpdateRepository<TEntity> _updateRepository;

        /// <summary>
        ///     Репозиторий удаления удаляемых сущностей
        /// </summary>
        public DeletableEntityDeleteRepository([NotNull] IItemWrapper<DbContext> contextWrapper,
                                               [NotNull] IUniversalIncludeProcessor universalIncludeProcessor,
                                               [NotNull] IDatabaseCommitter databaseCommitter, [NotNull] ITransactor transactor,
                                               [NotNull] IDetacher detacher, [NotNull] IUpdateRepository<TEntity> updateRepository)
            : base(contextWrapper, universalIncludeProcessor, databaseCommitter, transactor, detacher)
        {
            if (updateRepository == null) throw new ArgumentNullException("updateRepository");
            _updateRepository = updateRepository;
        }

        /// <summary>
        ///     Удалить сущности
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        public override IReadOnlyCollection<TEntity> Delete(
            Expression<Func<TEntity, bool>> where,
            bool commit = true)
        {
            return _updateRepository.Update(entity => entity.IsDeleted = true, where, commit: commit);
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        public override TEntity DeleteSingle(Expression<Func<TEntity, bool>> where, bool commit = true)
        {
            return _updateRepository.UpdateSingle(entity => entity.IsDeleted = true, where, commit: commit);
        }

        /// <summary>
        ///     Удалить сущности
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Задача, результат которой - коллекция удалённых сущностей</returns>
        public override Task<IReadOnlyCollection<TEntity>> DeleteAsync(Expression<Func<TEntity, bool>> where,
                                                                       bool commit = true)
        {
            return _updateRepository.UpdateAsync(entity => entity.IsDeleted = true, where, commit: commit);
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Задача, результат которой - удалённая сущность</returns>
        public override Task<TEntity> DeleteSingleAsync(Expression<Func<TEntity, bool>> where, bool commit = true)
        {
            return _updateRepository.UpdateSingleAsync(entity => entity.IsDeleted = true, where, commit: commit);
        }

        /// <summary>
        ///     Отменить удаление сущностей
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Изменённые сущности</returns>
        public IReadOnlyCollection<TEntity> Undelete(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return AsyncHelper.RunSync(() => UndeleteAsync(where, commit));
        }

        /// <summary>
        ///     Отменить удаление сущности
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Изменённая сущность</returns>
        public TEntity UndeleteSingle(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return AsyncHelper.RunSync(() => UndeleteSingleAsync(where, commit));
        }

        /// <summary>
        ///     Отменить удаление сущностей
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Задача, результат которой - изменённые сущности</returns>
        public Task<IReadOnlyCollection<TEntity>> UndeleteAsync(Expression<Func<TEntity, bool>> where, bool commit = true)
        {
            return _updateRepository.UpdateAsync(entity => entity.IsDeleted = false, where.And(e => e.IsDeleted), commit: commit);
        }

        /// <summary>
        ///     Отменить удаление сущности
        /// </summary>
        /// <param name="where">Условие</param>
        /// <param name="commit">Сохранить ли изменения немедленно</param>
        /// <returns>Задача, результат которой - изменённая сущность</returns>
        public Task<TEntity> UndeleteSingleAsync(Expression<Func<TEntity, bool>> where, bool commit = true)
        {
            return _updateRepository.UpdateSingleAsync(entity => entity.IsDeleted = false, where.And(e => e.IsDeleted), commit: commit);
        }
    }
}