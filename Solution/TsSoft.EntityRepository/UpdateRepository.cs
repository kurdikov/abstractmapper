namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.EntityRepository.IncludeProcessors;

    /// <summary>
    ///     �����������, ���������� �� ���������� ���������
    /// </summary>
    /// <typeparam name="TEntity">��� ��������</typeparam>
    public class UpdateRepository<TEntity> : InternalChangeEntityBaseRepository<TEntity>, IUpdateRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        ///     �����������, ���������� �� ���������� ���������
        /// </summary>
        public UpdateRepository(
            [NotNull] IItemWrapper<DbContext> contextWrapper,
            [NotNull] IUniversalIncludeProcessor universalIncludeProcessor,
            [NotNull] IDatabaseCommitter databaseCommitter,
            [NotNull] ITransactor transactor,
            [NotNull] IDetacher detacher)
            : base(contextWrapper, universalIncludeProcessor, databaseCommitter, transactor, detacher)
        {
        }

        /// <summary>
        ///     �������� ��������
        /// </summary>
        /// <param name="updater">
        ///     ���������� - ��������� �� ������ �� ���������, ������� ������� ��� ��������
        /// </param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� �������� ������ � ����������</param>
        /// <param name="commit">�������� �� ��������� � ���������</param>
        /// <returns>��������� ���������� ���������</returns>
        public IReadOnlyCollection<TEntity> Update(Action<TEntity> updater, Expression<Func<TEntity, bool>> @where,
                                                   IEnumerable<Expression<Func<TEntity, object>>> include = null,
                                                   bool commit = true)
        {
            var ctx = Context;
            return InternalUpdate(ctx, UpdaterTransformer(updater), UpdateWhereTransformer(where), include, commit);
        }

        /// <summary>
        ///     �������� ���� ��������
        /// </summary>
        /// <param name="updater">
        ///     ���������� - ��������� �� ��������
        /// </param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� �������� ������ � ����������</param>
        /// <param name="commit">�������� �� ��������� � ���������</param>
        /// <returns>���������� ��������</returns>
        public TEntity UpdateSingle(Action<TEntity> updater, Expression<Func<TEntity, bool>> @where,
                                    IEnumerable<Expression<Func<TEntity, object>>> include = null, bool commit = true)
        {
            var ctx = Context;
            return InternalUpdateSingle(ctx,
                UpdaterTransformer(updater), UpdateWhereTransformer(where), include, commit);
        }

        /// <summary>
        ///     �������� ���� �������� ������ ���������
        /// </summary>
        /// <param name="updater">
        ///     ���������� - ��������� �� ������ �� ���������, ������� ������� ��� ��������
        ///     ��� ������ �������� - updateSource, � ������ - �������� � ���������
        /// </param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� �������� ������ � ����������</param>
        /// <param name="updateSource">��� ���������</param>
        /// <param name="commit">�������� �� ��������� � ���������</param>
        /// <returns>���������� ��������</returns>
        public TEntity UpdateSingleFrom(Action<TEntity, TEntity> updater, Expression<Func<TEntity, bool>> @where,
                                        IEnumerable<Expression<Func<TEntity, object>>> include, TEntity updateSource,
                                        bool commit = true)
        {
            var ctx = Context;
            return InternalUpdateSingleFrom(ctx,
                UpdaterTransformer(updater), UpdateWhereTransformer(where), include, updateSource, commit);
        }

        /// <summary>
        ///     �������� ��������
        /// </summary>
        /// <param name="updater">
        ///     ���������� - ��������� �� ������ �� ���������, ������� ������� ��� ��������
        /// </param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� �������� ������ � ����������</param>
        /// <param name="commit">�������� �� ��������� � ���������</param>
        /// <returns>������, ��������� ������� - ��������� ���������� ���������</returns>
        public Task<IReadOnlyCollection<TEntity>> UpdateAsync(
            Action<TEntity> updater,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            bool commit = true)
        {
            var ctx = Context;
            return InternalUpdateAsync(ctx, UpdaterTransformer(updater), UpdateWhereTransformer(where), include, commit);
        }

        /// <summary>
        ///     �������� ���� ��������
        /// </summary>
        /// <param name="updater">
        ///     ���������� - ��������� �� ��������
        /// </param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� �������� ������ � ����������</param>
        /// <param name="commit">�������� �� ��������� � ���������</param>
        /// <returns>������, ��������� ������� - ���������� ��������</returns>
        public Task<TEntity> UpdateSingleAsync(
            Action<TEntity> updater,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            bool commit = true)
        {
            var ctx = Context;
            return InternalUpdateSingleAsync(ctx,
                UpdaterTransformer(updater), UpdateWhereTransformer(where), include, commit);
        }

        /// <summary>
        ///     �������� ���� �������� ������ ���������
        /// </summary>
        /// <param name="updater">
        ///     ���������� - ��������� �� ������ �� ���������, ������� ������� ��� ��������
        ///     ��� ������ �������� - updateSource, � ������ - �������� � ���������
        /// </param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� �������� ������ � ����������</param>
        /// <param name="updateSource">��� ���������</param>
        /// <param name="commit">�������� �� ��������� � ���������</param>
        /// <returns>������, ��������� ������� - ���������� ��������</returns>
        public Task<TEntity> UpdateSingleFromAsync(
            Action<TEntity, TEntity> updater,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            TEntity updateSource,
            bool commit = true)
        {
            var ctx = Context;
            return InternalUpdateSingleFromAsync(ctx,
                UpdaterTransformer(updater), UpdateWhereTransformer(where), include, updateSource, commit);
        }

        /// <summary>
        ///     �������� ���� �������� ������ ���������
        /// </summary>
        /// <param name="updater">
        ///     ���������� - ��������� �� ������ �� ���������, ������� ������� ��� ��������
        ///     ��� ������ �������� - updateSource, � ������ - �������� � ���������
        /// </param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� �������� ������ � ����������</param>
        /// <param name="updateSource">��� ���������</param>
        /// <param name="commit">�������� �� ��������� � ���������</param>
        /// <returns>������, ��������� ������� - ���������� ��������</returns>
        public Task<TEntity> UpdateSingleFromAsync(
            Func<TEntity, TEntity, IEnumerable<Func<Task>>> updater,
            Expression<Func<TEntity, bool>> @where,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            TEntity updateSource,
            bool commit = true)
        {
            var ctx = Context;
            return InternalUpdateSingleFromAsync(ctx,
                UpdaterTransformer(updater), UpdateWhereTransformer(where), include, updateSource, commit);
        }

        /// <summary>
        ///     �������� �������� ��� �������������� ����������� � ���������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="updater">����������</param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� ��������</param>
        /// <param name="commit">�������� �� ��������� �����</param>
        /// <returns>���������� ��������</returns>
        protected async Task<IReadOnlyCollection<TEntity>> InternalUpdateAsync(
            [NotNull] DbContext context,
            [NotNull] Action<TEntity> updater,
            [NotNull] Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            bool commit = true)
        {
            var listTask = Table(context).Where(@where).Include(include).ToListAsync();
            if (listTask == null)
            {
                throw new InvalidOperationException("ToListAsync returned null");
            }
            var entities = await listTask.ConfigureAwait(false);
            if (entities == null)
            {
                throw new InvalidOperationException("Entities is null");
            }
            foreach (var entity in entities)
            {
                updater(entity);
            }
            await ConditionalCommitAsync(context, commit).ConfigureAwait(false);
            return entities;
        }

        /// <summary>
        ///     �������� �������� ��� �������������� ����������� � ���������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="updater">����������</param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� ��������</param>
        /// <param name="commit">�������� �� ��������� �����</param>
        /// <returns>���������� ��������</returns>
        protected IReadOnlyCollection<TEntity> InternalUpdate(
            [NotNull]DbContext context,
            [NotNull] Action<TEntity> updater,
            [NotNull] Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            bool commit = true)
        {
            var entities = Table(context).Where(@where).Include(include).ToList();
            foreach (var entity in entities)
            {
                updater(entity);
            }
            ConditionalCommit(context, commit);
            return entities;
        }

        /// <summary>
        ///     �������� �������� ��� �������������� ����������� � ���������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="updater">����������</param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� ��������</param>
        /// <param name="commit">�������� �� ��������� �����</param>
        /// <returns>���������� ��������</returns>
        protected async Task<TEntity> InternalUpdateSingleAsync(
            [NotNull] DbContext context,
            [NotNull] Action<TEntity> updater,
            [NotNull] Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            bool commit = true)
        {
            var entity = await GetSingleForUpdateAsync(context, where, include).ConfigureAwait(false);
            if (entity == null)
            {
                return null;
            }
            updater(entity);
            await ConditionalCommitAsync(context, commit).ConfigureAwait(false);
            return entity;
        }

        /// <summary>
        ///     �������� �������� ��� �������������� ����������� � ���������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="updater">����������</param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� ��������</param>
        /// <param name="commit">�������� �� ��������� �����</param>
        /// <returns>���������� ��������</returns>
        protected TEntity InternalUpdateSingle(
            [NotNull] DbContext context,
            [NotNull] Action<TEntity> updater,
            [NotNull] Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            bool commit = true)
        {
            var entity = GetSingleForUpdate(context, where, include);
            if (entity == null)
            {
                return null;
            }
            updater(entity);
            ConditionalCommit(context, commit);
            return entity;
        }

        /// <summary>
        ///     �������� ���� �������� ��� ����������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� ��������</param>
        protected TEntity GetSingleForUpdate(
            [NotNull] DbContext context,
            [NotNull] Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include)
        {
            var entity = Table(context).Include(include).SingleOrDefault(@where);
            return entity;
        }

        /// <summary>
        ///     �������� ���� �������� ��� ����������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� ��������</param>
        protected async Task<TEntity> GetSingleForUpdateAsync(
            [NotNull]DbContext context,
            [NotNull] Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include)
        {
            var singleOrDefaultTask = Table(context).Include(include).SingleOrDefaultAsync(@where);
            if (singleOrDefaultTask == null)
            {
                throw new InvalidOperationException("SingleOrDefaultAsync returned null");
            }
            var entity = await singleOrDefaultTask.ConfigureAwait(false);
            return entity;
        }

        /// <summary>
        ///     �������� ���� �������� ������ ��������� ��� �������������� ����������� � ���������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="updater">
        ///     ���������� - ��������� �� ������ �� ���������, ������� ������� ��� ��������
        ///     ��� ������ �������� - updateSource, � ������ - �������� � ���������
        /// </param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� �������� ������ � ����������</param>
        /// <param name="updateSource">��� ���������</param>
        /// <param name="commit">�������� �� ��������� � ���������</param>
        /// <returns>������, ��������� ������� - ���������� ��������</returns>
        protected async Task<TEntity> InternalUpdateSingleFromAsync(
            [NotNull]DbContext context,
            [NotNull] Action<TEntity, TEntity> updater,
            [NotNull] Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            TEntity updateSource,
            bool commit = true)
        {
            var entity = await GetSingleForUpdateAsync(context, where, include).ConfigureAwait(false);
            if (entity == null)
            {
                return null;
            }
            updater(updateSource, entity);
            await ConditionalCommitAsync(context, commit).ConfigureAwait(false);
            return entity;
        }

        /// <summary>
        ///     �������� ���� �������� ������ ��������� ��� �������������� ����������� � ���������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="updater">
        ///     ���������� - ��������� �� ������ �� ���������, ������� ������� ��� ��������
        ///     ��� ������ �������� - updateSource, � ������ - �������� � ���������, ������ ����� �����, ������� ����� ��������� ����� �����������
        /// </param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� �������� ������ � ����������</param>
        /// <param name="updateSource">��� ���������</param>
        /// <param name="commit">�������� �� ��������� � ���������</param>
        /// <returns>������, ��������� ������� - ���������� ��������</returns>
        protected async Task<TEntity> InternalUpdateSingleFromAsync(
            [NotNull]DbContext context,
            [NotNull] Func<TEntity, TEntity, IEnumerable<Func<Task>>> updater,
            [NotNull] Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            TEntity updateSource,
            bool commit = true)
        {
            var entity = await GetSingleForUpdateAsync(context, where, include).ConfigureAwait(false);
            if (entity == null)
            {
                return null;
            }
            var tasks = updater(updateSource, entity) ?? Enumerable.Empty<Func<Task>>();
            foreach (var task in tasks)
            {
                if (task == null)
                {
                    continue;
                }
                var runningTask = task();
                if (runningTask != null)
                {
                    await runningTask.ConfigureAwait(false);
                }
            }
            await ConditionalCommitAsync(context, commit).ConfigureAwait(false);
            return entity;
        }

        /// <summary>
        ///     �������� ���� �������� ������ ��������� ��� �������������� ����������� � ���������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="updater">
        ///     ���������� - ��������� �� ������ �� ���������, ������� ������� ��� ��������
        ///     ��� ������ �������� - updateSource, � ������ - �������� � ���������
        /// </param>
        /// <param name="where">��������</param>
        /// <param name="include">����� ����������� �������� ������ � ����������</param>
        /// <param name="updateSource">��� ���������</param>
        /// <param name="commit">�������� �� ��������� � ���������</param>
        /// <returns>���������� ��������</returns>
        protected TEntity InternalUpdateSingleFrom(
            [NotNull] DbContext context,
            [NotNull] Action<TEntity, TEntity> updater,
            [NotNull] Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            TEntity updateSource,
            bool commit = true)
        {
            var entity = GetSingleForUpdate(context, where, include);
            if (entity == null)
            {
                return null;
            }
            updater(updateSource, entity);
            ConditionalCommit(context, commit);
            return entity;
        }

        /// <summary>
        ///     ������������� ������� ���������� �������� ������ �����������
        /// </summary>
        /// <param name="where">�������� �������</param>
        /// <returns>��������������� �������</returns>
        [NotNull]
        protected virtual Expression<Func<TEntity, bool>> UpdateWhereTransformer(Expression<Func<TEntity, bool>> where)
        {
            return where ?? (e => true);
        }

        /// <summary>
        ///     ������������� ������� ���������� �������� ������ �����������
        /// </summary>
        /// <param name="updater">�������� ������� ����������</param>
        /// <returns>��������������� �������</returns>
        [NotNull]
        protected virtual Action<TEntity> UpdaterTransformer(Action<TEntity> updater)
        {
            if (updater == null)
            {
                return entity => { };
            }
            return updater;
        }

        /// <summary>
        ///     ������������� ������� ���������� �������� ������ �����������
        /// </summary>
        /// <param name="updater">�������� ������� ����������</param>
        /// <returns>��������������� �������</returns>
        [NotNull]
        protected virtual Action<TEntity, TEntity> UpdaterTransformer(Action<TEntity, TEntity> updater)
        {
            if (updater == null)
            {
                return (entity, entity1) => { };
            }
            return updater;
        }

        /// <summary>
        ///     ������������� ������� ���������� �������� ������ �����������
        /// </summary>
        /// <param name="updater">�������� ������� ����������</param>
        /// <returns>��������������� �������</returns>
        [NotNull]
        protected virtual Func<TEntity, TEntity, IEnumerable<Func<Task>>> UpdaterTransformer(Func<TEntity, TEntity, IEnumerable<Func<Task>>> updater)
        {
            if (updater == null)
            {
                return (entity, entity1) => Enumerable.Empty<Func<Task>>();
            }
            return updater;
        }
    }
}