﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Mappers;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.IncludeProcessors;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.IncludeBuilder;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    ///     Отвечает за чтение сущностей из хранилища
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public partial class ReadRepository<TEntity> :
        InternalBaseRepository<TEntity>,
        IReadRepository<TEntity>
        where TEntity : class
    {
        [NotNull] private readonly IDynamicEntityToEntityMapper<TEntity> _dynamicMapper;
        [NotNull] private readonly IIncludeDescriptionHelper _includeDescriptionHelper;

        /// <summary>
        ///     Отвечает за чтение сущностей из хранилища
        /// </summary>
        public ReadRepository(
            [NotNull] IItemWrapper<DbContext> contextWrapper,
            [NotNull] IUniversalIncludeProcessor universalIncludeProcessor,
            [NotNull] IDynamicEntityToEntityMapper<TEntity> dynamicMapper,
            [NotNull] IIncludeDescriptionHelper includeDescriptionHelper)
            : base(contextWrapper, universalIncludeProcessor)
        {
            if (dynamicMapper == null) throw new ArgumentNullException("dynamicMapper");
            if (includeDescriptionHelper == null) throw new ArgumentNullException("includeDescriptionHelper");
            _dynamicMapper = dynamicMapper;
            _includeDescriptionHelper = includeDescriptionHelper;
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> Get(
            Expression<Func<TEntity, bool>> @where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return Get(ctx, where, true, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> Get(
            Expression<Func<TEntity, bool>> @where,
            IReadOnlySelectExpression<TEntity> @select,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            var ctx = Context;
            return Get(ctx, where, select, orderBy, true);
        }

        /// <summary>
        ///     Получить количество сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Количество сущностей по предикату</returns>
        public int Count(Expression<Func<TEntity, bool>> @where)
        {
            var ctx = Context;
            return Count(ctx, where, true);
        }

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Сущность</returns>
        public TEntity GetSingle(
            Expression<Func<TEntity, bool>> @where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return GetSingle(ctx, where, include, includeConditions, true);
        }

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <returns>Сущность</returns>
        public TEntity GetSingle(Expression<Func<TEntity, bool>> @where, IReadOnlySelectExpression<TEntity> @select)
        {
            var ctx = Context;
            return GetSingle(ctx, where, select, true);
        }

        /// <summary>
        ///     Существует ли сущность в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        public bool Exists(Expression<Func<TEntity, bool>> @where)
        {
            var ctx = Context;
            return Exists(ctx, where, true);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public IPage<TEntity> GetPaged(
            Expression<Func<TEntity, bool>> @where,
            int pageNumber,
            int pageSize,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            IEnumerable<IOrderByClause<TEntity>> orderBy,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return GetPaged(ctx, where, pageNumber, pageSize, true, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public IPage<TEntity> GetPaged(
            Expression<Func<TEntity, bool>> @where,
            int pageNumber,
            int pageSize,
            IReadOnlySelectExpression<TEntity> @select,
            IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            var ctx = Context;
            return GetPaged(ctx, where, pageNumber, pageSize, select, orderBy, true);
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetTracked(
            Expression<Func<TEntity, bool>> @where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return GetTracked(ctx, where, true, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Максимальное значение поля или null</returns>
        public T? Max<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            var ctx = Context;
            return Max(ctx, selector, where, true);
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Минимальное значение поля или null</returns>
        public T? Min<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> @where) where T : struct
        {
            var ctx = Context;
            return Min(ctx, selector, where, true);
        }

        /// <summary>
        ///     Получить количество сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - количество сущностей по предикату</returns>
        public async Task<int> CountAsync(Expression<Func<TEntity, bool>> @where)
        {
            var ctx = Context;
            return await CountAsync(ctx, where, true).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public async Task<IReadOnlyCollection<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> where,
            IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            var ctx = Context;
            return await GetAsync(ctx, where, select, orderBy, true).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public async Task<IReadOnlyCollection<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return await GetAsync(ctx, where, true, include, orderBy, includeConditions).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        public async Task<IReadOnlyCollection<TEntity>> GetTrackedAsync(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return await GetTrackedAsync(ctx, where, true, include, orderBy, includeConditions).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Задача, результат которой - сущность</returns>
        public async Task<TEntity> GetSingleAsync(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return await GetSingleAsync(ctx, where, include, includeConditions, true).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <returns>Задача, результат которой - сущность</returns>
        public async Task<TEntity> GetSingleAsync(
            Expression<Func<TEntity, bool>> where,
            IReadOnlySelectExpression<TEntity> select)
        {
            var ctx = Context;
            return await GetSingleAsync(ctx, where, select, true).ConfigureAwait(false);
        }

        /// <summary>
        ///     Существует ли сущность в хранилище
        /// </summary>
        /// <param name="where">Предикат</param>
        public async Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> where)
        {
            var ctx = Context;
            return await ExistsAsync(ctx, where, true).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public async Task<IPage<TEntity>> GetPagedAsync(
            Expression<Func<TEntity, bool>> @where,
            int pageNumber,
            int pageSize,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var ctx = Context;
            return await GetPagedAsync(ctx, where, pageNumber, pageSize, true, include, orderBy, includeConditions).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Количество сущностей на странице</param>
        /// <param name="select">Получить только эти поля сущности и подсущностей</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        public async Task<IPage<TEntity>> GetPagedAsync(
            Expression<Func<TEntity, bool>> where,
            int pageNumber,
            int pageSize,
            IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy)
        {
            var ctx = Context;
            return await GetPagedAsync(ctx, where, pageNumber, pageSize, select, orderBy, true).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - максимальное значение поля или null</returns>
        public async Task<T?> MaxAsync<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> where)
            where T : struct
        {
            var ctx = Context;
            return await MaxAsync(ctx, selector, where, true).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="selector">Доступ к полю</param>
        /// <param name="where">Предикат</param>
        /// <returns>Задача, результат которой - минимальное значение поля или null</returns>
        public async Task<T?> MinAsync<T>(Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> where)
            where T : struct
        {
            var ctx = Context;
            return await MinAsync(ctx, selector, where, true).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности включить (join)</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public IReadOnlyCollection<TEntity> GetFromSpecifiedContext(DbContext context, Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null, IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return Get(context, where, true, include, orderBy, includeConditions);
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="include">Какие подсущности выбрать</param>
        /// <param name="orderBy">Сортировка для сущностей</param>
        /// <param name="includeConditions">Ограничения на включенные подсущности</param>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        public Task<IReadOnlyCollection<TEntity>> GetFromSpecifiedContextAsync(DbContext context, Expression<Func<TEntity, bool>> @where, IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null, IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return GetAsync(context, where, true, include, orderBy, includeConditions);
        }
    }
}
