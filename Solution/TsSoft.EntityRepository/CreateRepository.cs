namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.IncludeProcessors;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

    /// <summary>
    ///     ����������� �������� ���������
    /// </summary>
    /// <typeparam name="TEntity">��� ��������</typeparam>
    public class CreateRepository<TEntity> : InternalChangeEntityBaseRepository<TEntity>, ICreateRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        ///     ����������� �������� ���������
        /// </summary>
        public CreateRepository([NotNull] IItemWrapper<DbContext> contextWrapper, [NotNull] IUniversalIncludeProcessor universalIncludeProcessor,
                                [NotNull] IDatabaseCommitter databaseCommitter, [NotNull] ITransactor transactor, [NotNull] IDetacher detacher)
            : base(contextWrapper, universalIncludeProcessor, databaseCommitter, transactor, detacher)
        {
        }

        /// <summary>
        ///     �������� �������� � ���������
        /// </summary>
        /// <param name="entity">��������</param>
        /// <param name="commit">
        ///     ����� �� ���������� ������
        ///     (���� ���, �� ��������� �� ����� �������� ����� ������ SaveChanges ��� ������ ������
        ///     � commit = true
        ///     )
        /// </param>
        /// <returns>��������� ��������</returns>
        public TEntity Create(TEntity entity, bool commit = true)
        {
            var ctx = Context;
            return CreateInternal(ctx, entity, commit);
        }


        /// <summary>
        ///     �������� �������� � ���������
        /// </summary>
        /// <param name="entity">��������</param>
        /// <param name="commit">
        ///     ����� �� ���������� ������
        ///     (���� ���, �� ��������� �� ����� �������� ����� ������ SaveChanges ��� ������ ������
        ///     � commit = true
        ///     )
        /// </param>
        /// <returns>������, ��������� ������� - ��������� ��������</returns>
        public virtual Task<TEntity> CreateAsync(TEntity entity, bool commit = true)
        {
            var ctx = Context;
            return CreateInternalAsync(ctx, entity, commit);
        }

        /// <summary>
        ///     �������� �������� � ���������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="entity">��������</param>
        /// <param name="commit">
        ///     ����� �� ���������� ������
        ///     (���� ���, �� ��������� �� ����� �������� ����� ������ SaveChanges ��� ������ ������
        ///     � commit = true
        ///     )
        /// </param>
        /// <returns>��������� ��������</returns>
        [NotNull]
        protected TEntity CreateInternal([NotNull]DbContext context, [NotNull]TEntity entity, bool commit)
        {
            Table(context).Add(entity);
            ConditionalCommit(context, commit);
            return entity;
        }

        /// <summary>
        ///     �������� �������� � ���������
        /// </summary>
        /// <param name="context">��������</param>
        /// <param name="entity">��������</param>
        /// <param name="commit">
        ///     ����� �� ���������� ������
        ///     (���� ���, �� ��������� �� ����� �������� ����� ������ SaveChanges ��� ������ ������
        ///     � commit = true
        ///     )
        /// </param>
        /// <returns>������, ��������� ������� - ��������� ��������</returns>
        [NotNull]
        protected async Task<TEntity> CreateInternalAsync([NotNull]DbContext context, [NotNull]TEntity entity, bool commit)
        {
            Table(context).Add(entity);
            await ConditionalCommitAsync(context, commit).ConfigureAwait(false);
            return entity;
        }

        /// <summary>
        ///     ���������� �������� �������� � ��������� � ������� � �� ���������
        /// </summary>
        /// <param name="entity">��������</param>
        /// <param name="paths">���� � ����������� ��������� ���������, �� ����� ��������� � ����� ���������</param>
        /// <returns>��������� �������� ��� ������������</returns>
        public TEntity CreateDetached(TEntity entity, IEnumerable<Func<TEntity, object>> paths = null)
        {
            var ctx = Context;
            var result = CreateInternal(ctx, entity, true);
            Detacher.Detach(ctx, result, paths);
            return result;
        }

        /// <summary>
        ///     ���������� �������� �������� � ��������� � ������� � �� ���������
        /// </summary>
        /// <param name="entity">��������</param>
        /// <param name="paths">���� � ����������� ��������� ���������, �� ����� ��������� � ����� ���������</param>
        /// <returns>������, ��������� ������� - ��������� �������� ��� ������������</returns>
        public async Task<TEntity> CreateDetachedAsync(TEntity entity, IEnumerable<Func<TEntity, object>> paths = null)
        {
            var ctx = Context;
            var result = await CreateInternalAsync(ctx, entity, true).ConfigureAwait(false);
            Detacher.Detach(ctx, result, paths);
            return result;
        }
    }
}
