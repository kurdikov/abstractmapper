﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    ///     Отвечает за удаление сущностей из хранилища
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IDeleteRepository<TEntity> : IDatabaseCommitter, IContextedTransactor
    {
        /// <summary>
        ///     Удалить сущности
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Коллекция удалённых сущностей</returns>
        [NotNull]
        IReadOnlyCollection<TEntity> Delete(Expression<Func<TEntity, bool>> where, bool commit = true);

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Удалённая сущность</returns>
        TEntity DeleteSingle(Expression<Func<TEntity, bool>> where, bool commit = true);

        /// <summary>
        /// Удалить одну сущность
        /// </summary>
        /// <param name="entity">Сущность (из контекста или с заполненными ключевыми свойствами)</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Удалённая сущность</returns>
        TEntity DeleteSingle(TEntity entity, bool commit = true);

        /// <summary>
        ///     Удалить сущности
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Задача, результат которой - коллекция удалённых сущностей</returns>
        [NotNull]
        Task<IReadOnlyCollection<TEntity>> DeleteAsync(Expression<Func<TEntity, bool>> where, bool commit = true);

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Задача, результат которой - удалённая сущность</returns>
        [NotNull]
        Task<TEntity> DeleteSingleAsync(Expression<Func<TEntity, bool>> where, bool commit = true);

        /// <summary>
        /// Удалить одну сущность
        /// </summary>
        /// <param name="entity">Сущность (из контекста или с заполненными ключевыми свойствами)</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Удалённая сущность</returns>
        [NotNull]
        Task<TEntity> DeleteSingleAsync(TEntity entity, bool commit = true);
    }
}