﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.IncludeProcessors;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

    /// <summary>
    ///     Репозиторий, отвечающий за удаление сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public class DeleteRepository<TEntity> : InternalChangeEntityBaseRepository<TEntity>, IDeleteRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        ///     Репозиторий, отвечающий за удаление сущностей
        /// </summary>
        public DeleteRepository(
            [NotNull] IItemWrapper<DbContext> contextWrapper, 
            [NotNull] IUniversalIncludeProcessor universalIncludeProcessor,
            [NotNull] IDatabaseCommitter databaseCommitter,
            [NotNull] ITransactor transactor,
            [NotNull] IDetacher detacher)
            : base(contextWrapper, universalIncludeProcessor, databaseCommitter, transactor, detacher)
        {
        }

        /// <summary>
        ///     Удалить сущности
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Коллекция удалённых сущностей</returns>
        public virtual IReadOnlyCollection<TEntity> Delete(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            var ctx = Context;
            var entities = Table(ctx).Where(@where ?? (e => true)).ToList();
            foreach (var entity in entities)
            {
                Table(ctx).Remove(entity);
            }
            ConditionalCommit(ctx, commit);
            return entities;
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Удалённая сущность</returns>
        public virtual TEntity DeleteSingle(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            var ctx = Context;
            var entity = Table(ctx).Where(@where ?? (e => true)).SingleOrDefault();
            if (entity == null)
            {
                return null;
            }
            Table(ctx).Remove(entity);
            ConditionalCommit(ctx, commit);
            return entity;
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="entity">Сущность (из контекста или с заполненными ключевыми свойствами)</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Удалённая сущность</returns>
        public TEntity DeleteSingle(TEntity entity, bool commit = true)
        {
            var ctx = Context;
            if (ctx.Entry(entity).ThrowIfNull("Context.Entry").State == EntityState.Detached)
            {
                Table(ctx).Attach(entity);
            }
            Table(ctx).Remove(entity);
            ConditionalCommit(ctx, commit);
            return entity;
        }


        /// <summary>
        ///     Удалить сущности
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Задача, результат которой - коллекция удалённых сущностей</returns>
        public virtual async Task<IReadOnlyCollection<TEntity>> DeleteAsync(
            Expression<Func<TEntity, bool>> where,
            bool commit = true)
        {
            var ctx = Context;
            var listAsync = Table(ctx).Where(@where ?? (e => true)).ToListAsync();
            if (listAsync == null)
            {
                throw new InvalidOperationException("ToListAsync returned null");
            }
            var entities = await listAsync.ConfigureAwait(false);
            if (entities == null)
            {
                throw new InvalidOperationException("Entities is null");
            }
            foreach (var entity in entities)
            {
                Table(ctx).Remove(entity);
            }
            await ConditionalCommitAsync(ctx, commit).ConfigureAwait(false);
            return entities;
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Задача, результат которой - удалённая сущность</returns>
        public virtual async Task<TEntity> DeleteSingleAsync(Expression<Func<TEntity, bool>> where, bool commit = true)
        {
            var ctx = Context;
            var singleOrDefaultTask = Table(ctx).Where(@where ?? (e => true)).SingleOrDefaultAsync();
            if (singleOrDefaultTask == null)
            {
                throw new InvalidOperationException("SingleOrDefaultAsync returned null");
            }
            var entity = await singleOrDefaultTask.ConfigureAwait(false);
            if (entity == null)
            {
                return null;
            }
            Table(ctx).Remove(entity);
            await ConditionalCommitAsync(ctx, commit).ConfigureAwait(false);
            return entity;
        }

        /// <summary>
        ///     Удалить одну сущность
        /// </summary>
        /// <param name="entity">Сущность (из контекста или с заполненными ключевыми свойствами)</param>
        /// <param name="commit">Записать ли изменения сразу</param>
        /// <returns>Удалённая сущность</returns>
        public async Task<TEntity> DeleteSingleAsync(TEntity entity, bool commit = true)
        {
            var ctx = Context;
            if (ctx.Entry(entity).ThrowIfNull("Context.Entry").State == EntityState.Detached)
            {
                Table(ctx).Attach(entity);
            }
            Table(ctx).Remove(entity);
            await ConditionalCommitAsync(ctx, commit).ConfigureAwait(false);
            return entity;
        }
    }
}
