﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    ///     Отвечает за создание сущностей в хранилище
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface ICreateRepository<TEntity> : IDatabaseCommitter, IContextedTransactor
    {
        /// <summary>
        ///     Записать сущность в хранилище
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="commit">
        ///     Сразу ли произвести запись
        ///     (если нет, то изменения не будут записаны после вызова SaveChanges или любого метода
        ///     с commit = true
        ///     )
        /// </param>
        /// <returns>Созданная сущность</returns>
        TEntity Create([NotNull]TEntity entity, bool commit = true);

        /// <summary>
        ///     Записать сущность в хранилище
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="commit">
        ///     Сразу ли произвести запись
        ///     (если нет, то изменения не будут записаны после вызова SaveChanges или любого метода
        ///     с commit = true
        ///     )
        /// </param>
        /// <returns>Задача, результат которой - созданная сущность</returns>
        [NotNull]
        Task<TEntity> CreateAsync([NotNull]TEntity entity, bool commit = true);

        /// <summary>
        ///     Немедленно записать сущность в хранилище и удалить её из контекста
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="paths">Пути к добавляемым вложенным сущностям, начиная с путей к более глубоко вложенным</param>
        /// <returns>Созданная сущность без подсущностей</returns>
        [NotNull]
        TEntity CreateDetached([NotNull]TEntity entity, IEnumerable<Func<TEntity, object>> paths = null);

        /// <summary>
        ///     Немедленно записать сущность в хранилище и удалить её из контекста
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="paths">Пути к добавляемым вложенным сущностям, начиная с путей к более глубоко вложенным</param>
        /// <returns>Задача, результат которой - созданная сущность без подсущностей</returns>
        [NotNull]
        Task<TEntity> CreateDetachedAsync([NotNull]TEntity entity, IEnumerable<Func<TEntity, object>> paths = null);
    }
}
