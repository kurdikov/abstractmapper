﻿namespace TsSoft.EntityRepository
{
    /// <summary>
    ///     Осуществляет прямые sql-запросы
    /// </summary>
    public interface IRawSqlRepository : IRawSqlReadRepository, IRawSqlWriteRepository
    {
    }
}