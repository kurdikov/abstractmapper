﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq;

    /// <summary>
    /// Удаляет сущности из контекста в памяти
    /// </summary>
    public class Detacher : IDetacher
    {
        /// <summary>
        /// Удалить сущность из контекста (при этом портится содержимое навигационных свойств)
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <param name="context">Контекст</param>
        /// <param name="entity">Сущность</param>
        /// <param name="paths">Пути к подсущностям, начиная с путей к более глубоко вложенным</param>
        public void Detach<TEntity>(DbContext context, TEntity entity, IEnumerable<Func<TEntity, object>> paths)
            where TEntity: class
        {
            foreach (var path in paths ?? Enumerable.Empty<Func<TEntity, object>>())
            {
                if (path == null)
                {
                    continue;
                }
                var pathEnd = path(entity);
                var pathEndCollection = pathEnd as IEnumerable<object>;
                if (pathEndCollection != null)
                {
                    foreach (var subentity in pathEndCollection.Where(e => e != null).ToList())
                    {
                        context.Entry(subentity).State = EntityState.Detached;
                    }
                }
                else if (pathEnd != null)
                {
                    context.Entry(pathEnd).State = EntityState.Detached;
                }
            }
            context.Entry(entity).State = EntityState.Detached;
        }
    }
}
