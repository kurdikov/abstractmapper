﻿namespace TsSoft.EntityRepository
{
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository.IncludeProcessors;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.Expressions.Helpers.Async;

    /// <summary>
    ///     Репозиторий удаления сложных сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public class ComplexDeleteRepository<TEntity> : DeleteRepository<TEntity>, IComplexDeleteRepository<TEntity> where TEntity : class, IComplexEntity
    {
        /// <summary>
        ///     Репозиторий удаления сложных сущностей
        /// </summary>
        public ComplexDeleteRepository([NotNull] IItemWrapper<DbContext> contextWrapper,
                                       [NotNull] IUniversalIncludeProcessor universalIncludeProcessor,
                                       [NotNull] IDatabaseCommitter databaseCommitter,
                                       [NotNull] ITransactor transactor, [NotNull] IDetacher detacher)
            : base(contextWrapper, universalIncludeProcessor, databaseCommitter, transactor, detacher)
        {
        }

        /// <summary>
        ///     Удалить все несохранённые сущности
        /// </summary>
        public IReadOnlyCollection<TEntity> DeleteStubs()
        {
            return AsyncHelper.RunSync(DeleteStubsAsync);
        }


        /// <summary>
        ///     Удалить все несохранённые сущности
        /// </summary>
        public async Task<IReadOnlyCollection<TEntity>> DeleteStubsAsync()
        {
            return await base.DeleteAsync(e => !e.IsSaved).ConfigureAwait(false);
        }
    }
}
