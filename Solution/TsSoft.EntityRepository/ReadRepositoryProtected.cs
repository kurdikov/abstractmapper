﻿namespace TsSoft.EntityRepository
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.OrderBy;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    ///     Отвечает за чтение сущностей из хранилища
    /// </summary>
    public partial class ReadRepository<TEntity>
    {
        /// <summary>
        ///     Превратить запрос в пагинируемый
        /// </summary>
        /// <typeparam name="T">Тип вытаскиваемого объекта</typeparam>
        /// <param name="queryable">Запрос</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Размер страницы</param>
        /// <returns>Пагинированный запрос</returns>
        [NotNull]
        protected IQueryable<T> ApplySkipTake<T>([NotNull] IQueryable<T> queryable, int pageNumber, int pageSize)
        {
            var skip = SkipHelper.GetSkip(pageNumber, pageSize);
            return queryable
#if NET45
                .Skip(() => skip)
                .Take(() => pageSize)
#elif NETSTANDARD15
                .Skip(skip)
                .Take(pageSize)
#endif
                .ThrowIfNull("result");
        }

        /// <summary>
        ///     Преобразовать условие выборки согласно логике репозитория
        /// </summary>
        /// <param name="where">Исходное условие</param>
        /// <returns>Преобразованное условие</returns>
        [NotNull]
        protected virtual Expression<Func<TEntity, bool>> GetWhereTransformer(Expression<Func<TEntity, bool>> where)
        {
            return where ?? (e => true);
        }

        /// <summary>
        ///     Создаёт запрос с инклюдами, без трекинга
        /// </summary>
        [NotNull]
        protected IQueryable<TEntity> InternalGetQueryable(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<IncludeDescription<TEntity>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            bool transformWhere = true)
        {
            return NoTracking(context)
                .Where(transformWhere ? GetWhereTransformer(where) : (where ?? (e => true)))
                .Include(include)
                .OrderBy(orderBy);
        }

        /// <summary>
        ///     Создаёт запрос с селектом, без трекинга
        /// </summary>
        [NotNull]
        protected IQueryable<object> InternalGetQueryableWithSelect(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            [NotNull] Expression<Func<TEntity, object>> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy,
            bool transformWhere = true)
        {
            return NoTracking(context)
                .Where(transformWhere ? GetWhereTransformer(where) : (where ?? (e => true)))
                .OrderBy(orderBy)
                .Select(select);
        }

        /// <summary>
        ///     Создаёт запрос без трекинга
        /// </summary>
        protected IQueryable<TEntity> GetQueryable(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            var descriptions = IncludeToDescriptions(include);
            return InternalGetQueryable(context, where, descriptions, orderBy);
        }

        /// <summary>
        ///     Для получения без переопределения where
        /// </summary>
        protected async Task<TEntity> GetSingleAsync(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions,
            bool transformWhere)
        {
            return (await InternalGetAsync(context, async includes =>
            {
                var result = await InternalGetQueryable(
                    context,
                    where,
                    includes,
                    transformWhere: transformWhere)
                    .SingleOrDefaultAsync()
                    .ThrowIfNull("SingleOrDefaultAsync")
                    .ConfigureAwait(false);
                return result != null ? new[] { result } : new TEntity[0];
            }, include, includeConditions).ConfigureAwait(false)).ThrowIfNull("InternalGetAsync").SingleOrDefault();
        }

        /// <summary>
        ///     Для получения без переопределения where
        /// </summary>
        protected TEntity GetSingle(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<Expression<Func<TEntity, object>>> include,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions,
            bool transformWhere)
        {
            return (InternalGet(context,
                includes =>
                {
                    var result = InternalGetQueryable(
                        context,
                        where,
                        includes,
                        transformWhere: transformWhere)
                        .SingleOrDefault();
                    return result != null ? new[] { result } : new TEntity[0];
                }, include, includeConditions)).SingleOrDefault();
        }

        /// <summary>
        ///     Асинхронный метод получения без переопределения where
        /// </summary>
        protected async Task<TEntity> GetSingleAsync(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            [NotNull] IReadOnlySelectExpression<TEntity> select,
            bool transformWhere)
        {
            if (@select == null || select.Select == null) throw new ArgumentNullException("select");
            var collection = (await InternalGetQueryableWithSelect(context, where, select.Select, null, transformWhere)
                    .SingleOrDefaultAsync()
                    .ThrowIfNull("SingleOrDefaultAsync")
                    .ConfigureAwait(false))
                .ToEnumerableNotNull();
            var selected = await InternalGetWithSelectAsync(context, collection, select).ConfigureAwait(false);
            return selected.ThrowIfNull("selected").SingleOrDefault();
        }

        /// <summary>
        ///     Для получения без переопределения where
        /// </summary>
        protected TEntity GetSingle(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            [NotNull] IReadOnlySelectExpression<TEntity> select,
            bool transformWhere)
        {
            if (@select == null || select.Select == null) throw new ArgumentNullException("select");
            var collection = (InternalGetQueryableWithSelect(context, where, select.Select, null, transformWhere)
                    .SingleOrDefault())
                .ToEnumerableNotNull();
            var selected = InternalGetWithSelect(context, collection, select);
            return selected.SingleOrDefault();
        }

        /// <summary>
        ///     Асинхронный метод получения без переопределения where
        /// </summary>
        protected async Task<IReadOnlyCollection<TEntity>> GetAsync(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            [NotNull] IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy,
            bool transformWhere)
        {
            if (select.Select == null) throw new ArgumentNullException("select");
            var collection = await InternalGetQueryableWithSelect(
                context,
                where,
                select.Select,
                orderBy,
                transformWhere).ToListAsync().ThrowIfNull("ToListAsync").ConfigureAwait(false);
            return await InternalGetWithSelectAsync(context, collection.ThrowIfNull("collection"), select).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить количество сущностей без переопределения where
        /// </summary>
        /// <returns>Задача, результат которой - количество сущностей по предикату</returns>
        protected async Task<int> CountAsync([NotNull]DbContext context, Expression<Func<TEntity, bool>> @where, bool transformWhere)
        {
            return await InternalGetQueryable(
                    context,
                    where,
                    null, null, transformWhere)
                .CountAsync()
                .ThrowIfNull("CountAsync")
                .ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить количество сущностей без переопределения where
        /// </summary>
        protected int Count([NotNull]DbContext context, Expression<Func<TEntity, bool>> @where, bool transformWhere)
        {
            return InternalGetQueryable(
                    context,
                    where,
                    null, null, transformWhere)
                .Count();
        }


        /// <summary>
        ///     Для получения без переопределения where
        /// </summary>
        [NotNull]
        protected IReadOnlyCollection<TEntity> Get(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            [NotNull] IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy,
            bool transformWhere)
        {
            if (select.Select == null) throw new ArgumentNullException("select");
            var collection = InternalGetQueryableWithSelect(
                context,
                where,
                select.Select,
                orderBy,
                transformWhere).ToList();
            return InternalGetWithSelect(context, collection, select);
        }

        /// <summary>
        ///     Создаёт запрос с трекингом
        /// </summary>
        [NotNull]
        protected IQueryable<TEntity> InternalGetQueryableTracked(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            IEnumerable<IncludeDescription<TEntity>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            bool transformWhere = true)
        {
            where = transformWhere ? GetWhereTransformer(where) : where ?? (entity => true) ;
            return Table(context)
                .Where(where)
                .Include(include)
                .OrderBy(orderBy);
        }

        /// <summary>
        ///     Обрабатывает внешние инклюды для запроса с селектом
        /// </summary>
        [NotNull]
        protected async Task<IReadOnlyCollection<TEntity>> InternalGetWithSelectAsync(
            [NotNull] DbContext context,
            [NotNull] IEnumerable<object> collection,
            [NotNull] IReadOnlySelectExpression<TEntity> select)
        {
            var result = collection.Select(_dynamicMapper.Map).ToList();
            if (select.ExternalIncludes != null && select.ExternalIncludes.Any())
            {
                await UniversalIncludeProcessor.ProcessComplexIncludesAsync(
                    context,
                    result,
                    IncludeToDescriptions(select.ExternalIncludes),
                    new IIncludeCondition<TEntity>[0]).ConfigureAwait(false);
            }
            return result;
        }

        /// <summary>
        ///     Обрабатывает внешние инклюды для запроса с селектом
        /// </summary>
        [NotNull]
        protected IReadOnlyCollection<TEntity> InternalGetWithSelect(
            [NotNull] DbContext context,
            [NotNull] IEnumerable<object> collection,
            [NotNull] IReadOnlySelectExpression<TEntity> select)
        {
            var result = collection.Select(_dynamicMapper.Map).ToList();
            if (select.ExternalIncludes != null && select.ExternalIncludes.Any())
            {
                UniversalIncludeProcessor.ProcessComplexIncludes(
                    context,
                    result,
                    IncludeToDescriptions(select.ExternalIncludes),
                    new IIncludeCondition<TEntity>[0]);
            }
            return result;
        }

        /// <summary>
        ///     Преобразует инклюды в описания
        /// </summary>
        [NotNull]
        protected IReadOnlyCollection<IncludeDescription<TEntity>> IncludeToDescriptions(
            IEnumerable<Expression<Func<TEntity, object>>> include,
            ICollection<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return (include ?? Enumerable.Empty<Expression<Func<TEntity, object>>>())
                .Where(i => i != null)
                .Select(inc => _includeDescriptionHelper.IncludeToDescription(inc, includeConditions))
                .ToList();
        }


        /// <summary>
        ///     Обрабатывает внешние инклюды для запроса с инклюдами
        /// </summary>
        [NotNull]
        protected IReadOnlyCollection<TEntity> InternalGet(
            [NotNull]DbContext context,
            Func<IEnumerable<IncludeDescription<TEntity>>, IReadOnlyCollection<TEntity>> getFromDbWithIncludes,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var sortedConditions = (includeConditions ?? Enumerable.Empty<IIncludeCondition<TEntity>>())
                .OrderBy(ic => ic.ThrowIfNull("ic").PathProperties.Count)
                .ToList();
            var includeDescriptions = IncludeToDescriptions(include, sortedConditions);
            var result = getFromDbWithIncludes(includeDescriptions).ThrowIfNull("result");
            UniversalIncludeProcessor.ProcessComplexIncludes(context, result, includeDescriptions, sortedConditions);
            return result;
        }

        /// <summary>
        ///     Обрабатывает внешние инклюды для запроса с инклюдами
        /// </summary>
        [NotNull]
        protected async Task<IReadOnlyCollection<TEntity>> InternalGetAsync(
            [NotNull]DbContext context,
            Func<IEnumerable<IncludeDescription<TEntity>>, Task<IReadOnlyCollection<TEntity>>> getFromDbWithIncludes,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var sortedConditions = (includeConditions ?? Enumerable.Empty<IIncludeCondition<TEntity>>())
                .OrderBy(ic => ic.ThrowIfNull("ic").PathProperties.Count)
                .ToList();
            var includeDescriptions = IncludeToDescriptions(include, sortedConditions);
            var result = await getFromDbWithIncludes(includeDescriptions).ThrowIfNull("getFromDbWithIncludes").ConfigureAwait(false);
            await UniversalIncludeProcessor.ProcessComplexIncludesAsync(context, result.ThrowIfNull("result"), includeDescriptions, sortedConditions).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <returns>Задача, результат которой - максимальное значение поля или null</returns>
        [NotNull]
        protected async Task<T?> MaxAsync<T>([NotNull]DbContext context, [NotNull]Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> where, bool transformWhere)
                where T : struct
        {
            var query = InternalGetQueryable(context, where, null, null, transformWhere);
            return await query.AnyAsync().ThrowIfNull("AnyAsync").ConfigureAwait(false) ? await query.MaxAsync(selector).ThrowIfNull("MaxAsync").ConfigureAwait(false) : (T?)null;
        }

        /// <summary>
        ///     Получить максимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        protected T? Max<T>([NotNull]DbContext context, [NotNull]Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> where, bool transformWhere)
                where T : struct
        {
            var query = InternalGetQueryable(context, where, null, null, transformWhere);
            return query.Any() ? query.Max(selector) : (T?)null;
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <returns>Задача, результат которой - минимальное значение поля или null</returns>
        [NotNull]
        protected async Task<T?> MinAsync<T>([NotNull]DbContext context, [NotNull]Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> where, bool transformWhere)
                where T : struct
        {
            var query = InternalGetQueryable(context, where, null, null, transformWhere);
            return await query.AnyAsync().ThrowIfNull("AnyAsync").ConfigureAwait(false) ? await query.MinAsync(selector).ThrowIfNull("MinAsync").ConfigureAwait(false) : (T?)null;
        }

        /// <summary>
        ///     Получить минимальное значение поля для сущности в хранилище
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        protected T? Min<T>([NotNull]DbContext context, [NotNull]Expression<Func<TEntity, T>> selector, Expression<Func<TEntity, bool>> where, bool transformWhere)
                where T : struct
        {
            var query = InternalGetQueryable(context, where, null, null, transformWhere);
            return query.Any() ? query.Min(selector) : (T?)null;
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        protected async Task<IPage<TEntity>> GetPagedAsync(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            int pageNumber,
            int pageSize,
            IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy,
            bool transformWhere)
        {
            if (select == null || select.Select == null)
            {
                return await GetPagedAsync(context, where, pageNumber, pageSize, transformWhere, null, orderBy).ConfigureAwait(false);
            }
            var totalRecords = await InternalGetQueryable(context, where, null, null, transformWhere)
                .CountAsync()
                .ThrowIfNull("CountAsync")
                .ConfigureAwait(false);
            var page = await ApplySkipTake(
                InternalGetQueryableWithSelect(context, where, select.Select, orderBy, transformWhere),
                pageNumber,
                pageSize).ToListAsync().ThrowIfNull("ToListAsync").ConfigureAwait(false);
            var result = await InternalGetWithSelectAsync(
                context,
                page.ThrowIfNull("page"),
                select).ConfigureAwait(false);
            return new Page<TEntity>(totalRecords, result.ThrowIfNull("result"));
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        [NotNull]
        protected IPage<TEntity> GetPaged(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            int pageNumber,
            int pageSize,
            IReadOnlySelectExpression<TEntity> select,
            IEnumerable<IOrderByClause<TEntity>> orderBy,
            bool transformWhere)
        {
            if (select == null || select.Select == null)
            {
                return GetPaged(context, where, pageNumber, pageSize, transformWhere, null, orderBy);
            }
            var totalRecords = InternalGetQueryable(context, where, null, null, transformWhere)
                .Count();
            var page = ApplySkipTake(
                InternalGetQueryableWithSelect(context, where, select.Select, orderBy, transformWhere),
                pageNumber,
                pageSize).ToList();
            var result = InternalGetWithSelect(
                context,
                page.ThrowIfNull("page"),
                select);
            return new Page<TEntity>(totalRecords, result);
        }
        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        /// <returns>Задача, результат которой - пагинированный результат (сущности на странице и общее количество сущностей)</returns>
        [NotNull]
        protected async Task<IPage<TEntity>> GetPagedAsync(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> @where,
            int pageNumber,
            int pageSize,
            bool transformWhere,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var totalRecords = await InternalGetQueryable(
                    context,
                    where,
                    null,
                    null,
                    transformWhere)
                .CountAsync()
                .ThrowIfNull("CountAsync")
                .ConfigureAwait(false);
            var result = await InternalGetAsync(context,
                async includes => await ApplySkipTake(
                    InternalGetQueryable(context, where, includes, orderBy, transformWhere),
                    pageNumber,
                    pageSize).ToListAsync().ThrowIfNull("ToListAsync").ConfigureAwait(false),
                include,
                includeConditions).ConfigureAwait(false);
            return new Page<TEntity>(totalRecords, result.ThrowIfNull("result"));
        }

        /// <summary>
        ///     Получить пагинированную коллекцию сущностей
        /// </summary>
        [NotNull]
        protected IPage<TEntity> GetPaged(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> @where,
            int pageNumber,
            int pageSize,
            bool transformWhere,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            var totalRecords = InternalGetQueryable(
                    context,
                    where,
                    null,
                    null,
                    transformWhere)
                .Count();
            var result = InternalGet(context,
                includes => ApplySkipTake(
                    InternalGetQueryable(context, where, includes, orderBy, transformWhere),
                    pageNumber,
                    pageSize).ToList(),
                include,
                includeConditions);
            return new Page<TEntity>(totalRecords, result);
        }
        /// <summary>
        ///     Существует ли сущность в хранилище
        /// </summary>
        [NotNull]
        protected async Task<bool> ExistsAsync([NotNull]DbContext context, Expression<Func<TEntity, bool>> where, bool transformWhere)
        {
            return await InternalGetQueryable(
                context,
                where,
                null,
                null,
                transformWhere).AnyAsync().ThrowIfNull("AnyAsync").ConfigureAwait(false);
        }

        /// <summary>
        ///     Существует ли сущность в хранилище
        /// </summary>
        protected bool Exists([NotNull]DbContext context, Expression<Func<TEntity, bool>> where, bool transformWhere)
        {
            return InternalGetQueryable(
                context,
                where,
                null,
                null,
                transformWhere).Any();
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция отслеживаемых сущностей
        /// </returns>
        [NotNull]
        protected async Task<IReadOnlyCollection<TEntity>> GetTrackedAsync(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where, 
            bool transformWhere,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return await InternalGetAsync(context,
                async includes => await InternalGetQueryableTracked(
                    context,
                    where,
                    includes,
                    orderBy,
                    transformWhere).ToListAsync().ThrowIfNull("ToListAsync").ConfigureAwait(false),
                include,
                includeConditions).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить отслеживаемые EF сущности
        ///     При изменении этих сущностей, SaveChanges сохранит изменения в хранилище
        /// </summary>
        [NotNull]
        protected IReadOnlyCollection<TEntity> GetTracked(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            bool transformWhere,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return InternalGet(context,
                includes => InternalGetQueryableTracked(
                    context,
                    where,
                    includes,
                    orderBy,
                    transformWhere).ToList(),
                include,
                includeConditions);
        }
        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        /// <returns>
        ///     Задача, результат которой - <see cref="IReadOnlyCollection{TEntity}" /> - коллекция сущностей
        /// </returns>
        [NotNull]
        protected async Task<IReadOnlyCollection<TEntity>> GetAsync(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            bool transformWhere,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return await InternalGetAsync(context,
                async includes => await InternalGetQueryable(
                    context,
                    where,
                    includes,
                    orderBy,
                    transformWhere).ToListAsync().ThrowIfNull("ToListAsync").ConfigureAwait(false),
                include,
                includeConditions).ConfigureAwait(false);
        }

        /// <summary>
        ///     Получить коллекцию сущностей
        /// </summary>
        [NotNull]
        protected IReadOnlyCollection<TEntity> Get(
            [NotNull]DbContext context,
            Expression<Func<TEntity, bool>> where,
            bool transformWhere,
            IEnumerable<Expression<Func<TEntity, object>>> include = null,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null,
            IEnumerable<IIncludeCondition<TEntity>> includeConditions = null)
        {
            return InternalGet(context,
                includes => InternalGetQueryable(
                    context,
                    where,
                    includes,
                    orderBy,
                    transformWhere).ToList(),
                include,
                includeConditions);
        }
    }
}
