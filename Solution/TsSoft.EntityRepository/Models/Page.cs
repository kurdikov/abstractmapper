﻿namespace TsSoft.EntityRepository.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;

    /// <summary>
    /// Страница результата
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public class Page<TEntity> : IPage<TEntity>
    {
        /// <summary>
        /// Страница результата
        /// </summary>
        public Page(int totalRecords, [NotNull] IReadOnlyCollection<TEntity> result)
        {
            if (result == null)
            {
                throw new ArgumentNullException("result");
            }
            Result = result;
            TotalRecords = totalRecords;
        }

        /// <summary>
        /// Страница результата
        /// </summary>
        public Page(int totalRecords, [NotNull] IEnumerable<TEntity> result)
        {
            if (result == null)
            {
                throw new ArgumentNullException("result");
            }
            Result = result.ToList();
            TotalRecords = totalRecords;
        }

        /// <summary>
        /// Страница результата
        /// </summary>
        public Page()
        {
            Result = Enumerable.Empty<TEntity>().ToReadOnlyCollectionIfNeeded();
            TotalRecords = 0;
        }

        /// <summary>
        /// Пустая страница
        /// </summary>
        public static Page<TEntity> Empty
        {
            get { return new Page<TEntity>(); }
        }

        /// <summary>
        /// Общее количество записей
        /// </summary>
        public virtual int TotalRecords { get; private set; }

        /// <summary>
        /// Страница
        /// </summary>
        public virtual IReadOnlyCollection<TEntity> Result { get; private set; }
    }
}
