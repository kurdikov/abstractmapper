﻿namespace TsSoft.EntityRepository.Models
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Страница результата
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IPage<out TEntity>
    {
        /// <summary>
        /// Всего записей
        /// </summary>
        int TotalRecords { get; }

        /// <summary>
        /// Страница
        /// </summary>
        [NotNull]
        IReadOnlyCollection<TEntity> Result { get; }
    }
}
