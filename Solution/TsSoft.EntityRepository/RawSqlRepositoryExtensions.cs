namespace TsSoft.EntityRepository
{
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// ���������� ��� ����������� � ������� sql-���������
    /// </summary>
    public static class RawSqlRepositoryExtensions
    {
        /// <summary>
        /// ������� ������, ������������ ������
        /// </summary>
        /// <typeparam name="T">��� �������, � ������� ������� ������������ ������</typeparam>
        /// <param name="repository">�����������</param>
        /// <param name="sql">SQL-������</param>
        /// <param name="parameters">��������� �������</param>
        public static IReadOnlyCollection<T> Query<T>([NotNull]this IRawSqlRepository repository, [NotNull]string sql, 
            [NotNull]params IDataParameter[] parameters)
        {
            return repository.Query<T>(sql, parameters);
        }

        /// <summary>
        /// ������� ������, ������������ ������
        /// </summary>
        /// <typeparam name="T">��� �������, � ������� ������� ������������ ������</typeparam>
        /// <param name="repository">�����������</param>
        /// <param name="sql">SQL-������</param>
        /// <param name="parameters">��������� �������</param>
        public static Task<IReadOnlyCollection<T>> QueryAsync<T>([NotNull]this IRawSqlRepository repository, [NotNull]string sql,
            [NotNull]params IDataParameter[] parameters)
        {
            return repository.QueryAsync<T>(sql, parameters);
        }

        /// <summary>
        /// ��������� ������
        /// </summary>
        /// <param name="repository">�����������</param>
        /// <param name="sql">������</param>
        /// <param name="parameters">��������� �������</param>
        /// <returns>���������� ������������ �������� �����</returns>
        public static int Execute([NotNull]this IRawSqlRepository repository, [NotNull]string sql, [NotNull]params IDataParameter[] parameters)
        {
            return repository.Execute(sql, parameters);
        }

        /// <summary>
        /// ��������� ������
        /// </summary>
        /// <param name="repository">�����������</param>
        /// <param name="sql">������</param>
        /// <param name="parameters">��������� �������</param>
        /// <returns>������, ��������� ������� - ���������� ������������ �������� �����</returns>
        public static Task<int> ExecuteAsync([NotNull]this IRawSqlRepository repository, [NotNull]string sql,
            [NotNull]params IDataParameter[] parameters)
        {
            return repository.ExecuteAsync(sql, parameters);
        }
    }
}