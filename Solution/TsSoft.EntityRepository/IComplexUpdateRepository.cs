﻿namespace TsSoft.EntityRepository
{
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    ///     Репозиторий обновления сложных сущностей
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IComplexUpdateRepository<TEntity> : IUpdateRepository<TEntity>
        where TEntity : class, IComplexEntity
    {
    }
}