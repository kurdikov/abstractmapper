﻿namespace TsSoft.ContextWrapper.Wcf
{
    using System.Collections;
    using System.ServiceModel;
    using JetBrains.Annotations;

    /// <summary>
    /// HttpContext-подобная обёртка над OperationContext
    /// </summary>
    public class WcfContext : IExtension<InstanceContext>
    {
        [NotNull]
        private readonly IDictionary _items;

        private WcfContext()
        {
            _items = new Hashtable();
        }

        /// <summary>
        /// Элементы контекста
        /// </summary>
        [NotNull]
        public IDictionary Items
        {
            get { return _items; }
        }

        /// <summary>
        /// Текущий контекст
        /// </summary>
        public static WcfContext Current
        {
            get
            {
                if (OperationContext.Current == null || OperationContext.Current.InstanceContext == null)
                {
                    return null;
                }
                var context = OperationContext.Current.InstanceContext.Extensions.Find<WcfContext>();
                if (context == null)
                {
                    context = new WcfContext();
                    OperationContext.Current.InstanceContext.Extensions.Add(context);
                }
                return context;
            }
        }

        /// <summary>
        /// Вызывается при включении этого объекта в OperationContext
        /// </summary>
        public void Attach(InstanceContext owner) { }

        /// <summary>
        /// Вызывается при исключении этого объекта из OperationContext
        /// </summary>
        public void Detach(InstanceContext owner) { }
    }
}
