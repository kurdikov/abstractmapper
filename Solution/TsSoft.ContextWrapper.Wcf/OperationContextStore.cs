﻿namespace TsSoft.ContextWrapper.Wcf
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Хранилище объектов с использованием OperationContext и CallContext
    /// </summary>
    public class OperationContextStore : CallContextStore
    {
        private const string StoreName = "OperationContextStore";

        /// <summary>
        /// Получить объект по ключу
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="key">Ключ</param>
        public override T Get<T>(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            return WcfContext.Current != null
                ? GetFromContext(key, GetFromOperationContext<ILazy<T>>, StoreName)
                : base.Get<T>(key);
        }

        /// <summary>
        /// Подготовить хранилище для нового контекста
        /// </summary>
        public override void NewContext()
        {
            base.NewContext();
        }

        /// <summary>
        /// Инициализировать хранилище для объекта
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        /// <param name="contextElement">Элемент контекста с производящей объект функцией</param>
        protected override void Initialize(string key, IDisposableLazy<object> contextElement)
        {
            if (WcfContext.Current != null)
            {
                PutIntoContext(key, contextElement, PutIntoOperationContext, StoreName, Keys);
            }
            base.Initialize(key, contextElement);
        }

        /// <summary>
        /// Вызвать Dispose для объекта из хранилища
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public override void DisposeIfCreated(string key)
        {
            if (WcfContext.Current != null)
            {
                DisposeByKey(key, GetFromOperationContext<IDisposable>, StoreName);
            }
            base.DisposeIfCreated(key);
        }

        /// <summary>
        /// Вызвать Dispose для всех объектов из хранилища
        /// </summary>
        public override void DisposeAll()
        {
            Dispose();
        }

        /// <summary>
        /// Удалить все элементы из хранилища
        /// </summary>
        public override void Clear()
        {
            ClearContext(Keys, DeleteFromOperationContext);
            base.Clear();
        }

        private static T GetFromOperationContext<T>([NotNull]string key)
            where T : class
        {
            if (WcfContext.Current == null)
            {
                throw new NullReferenceException("WcfContext.Current is null");
            }
            return WcfContext.Current.Items[key] as T;
        }

        private static void PutIntoOperationContext<T>([NotNull]string key, [NotNull]T item)
        {
            if (WcfContext.Current == null)
            {
                throw new NullReferenceException("WcfContext.Current is null");
            }
            WcfContext.Current.Items[key] = item;
        }

        private static void DeleteFromOperationContext([NotNull] string key)
        {
            if (WcfContext.Current == null)
            {
                throw new NullReferenceException("WcfContext.Current is null");
            }
            WcfContext.Current.Items.Remove(key);
        }

        /// <summary>
        /// Вызвать Dispose для всех объектов хранилища
        /// </summary>
        public new static void Dispose()
        {
            if (WcfContext.Current != null)
            {
                Dispose(Keys, GetFromOperationContext<IDisposable>, StoreName);
            }
            CallContextStore.Dispose();
        }

        /// <summary>
        /// Инициализировано ли хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public override bool IsInitialized<T>(string key)
        {
            return IsInitialized(key, GetFromOperationContext<ILazy<T>>);
        }


        /// <summary>
        /// Создано ли значение в хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public override bool IsValueCreated<T>(string key)
        {
            if (WcfContext.Current != null)
            {
                return IsValueCreated(key, GetFromOperationContext<ILazy<T>>);
            }
            return base.IsValueCreated<T>(key);
        }
    }
}
