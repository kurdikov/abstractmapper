﻿namespace BaseTestClasses
{
    using System;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

    public static class ExceptAssert
    {
        public static void Throws<T>([NotNull]Action action)
            where T: Exception
        {
            try
            {
                action();
                throw new AssertFailedException(string.Format("Exception {0} expected but was not thrown", typeof(T)));
            }
            catch (T)
            {
            }
        }

        public static async Task ThrowsAsync<T>([NotNull]Func<Task> action)
            where T : Exception
        {
            try
            {
                await action().ThrowIfNull("action");
                throw new AssertFailedException(string.Format("Exception {0} expected but was not thrown", typeof(T)));
            }
            catch (T)
            {
            }
        }

        [StringFormatMethod("message")]
        public static void Throws<T>([NotNull]Action action, [NotNull]string message, [NotNull]params object[] formatParams)
            where T : Exception
        {
            try
            {
                action();
                throw new AssertFailedException(string.Format("Exception {0} expected but was not thrown. ", typeof(T)) + string.Format(message, formatParams));
            }
            catch (T)
            {
            }
        }
        public static void Throws<T>([NotNull]Action action, [NotNull]Func<T, bool> where)
            where T : Exception
        {
            try
            {
                action();
                throw new AssertFailedException(string.Format("Exception {0} expected but was not thrown", typeof(T)));
            }
            catch (T ex)
            {
                if (!where(ex))
                {
                    throw new AssertFailedException(string.Format("Thrown exception {0} did not have desired properties", typeof(T)));
                }
            }
        }

        public static void Throws<T>([NotNull]Action action, [NotNull]Func<T, bool> where, [NotNull]Func<T, string> message)
            where T : Exception
        {
            try
            {
                action();
                throw new AssertFailedException(string.Format("Exception {0} expected but was not thrown. ", typeof(T)) + message(null));
            }
            catch (T ex)
            {
                if (!where(ex))
                {
                    throw new AssertFailedException(string.Format("Thrown exception {0} did not have desired properties", typeof(T)) + message(ex));
                }
            }
        }
    }
}
