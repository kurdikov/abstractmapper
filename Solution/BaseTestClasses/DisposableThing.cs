﻿namespace BaseTestClasses
{
    using System;

    public class DisposableThing : IDisposable
    {
        private static int Count = 0;

        public static void Reset()
        {
            Count = 0;
        }

        public DisposableThing()
        {
            Id = ++Count;
        }

        public bool IsDisposed;
        public int Id;

        public void Dispose()
        {
            IsDisposed = true;
        }
    }
}
