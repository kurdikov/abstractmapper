﻿namespace BaseTestClasses.Database
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq;
    using JetBrains.Annotations;
    using Moq;
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.AbstractMapper.FuncCreators;
    using TsSoft.AbstractMapper.Helpers;
    using TsSoft.AbstractMapper.Mappers;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Collections;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder;
    using TsSoft.Expressions.SelectBuilder;
    using System.Reflection;
    using TsSoft.Bindings;

    public static class EntityStaticHelpers
    {
        [NotNull]public static readonly HashSet<Type> EntityTypes;

        [NotNull]public static readonly HashSet<Type> ExternalTypes;

        [NotNull]public static readonly IEntityTypesRetriever EntityTypesRetriever;
        [NotNull]public static readonly IEntityTypesHelper EntityTypesHelper;
        [NotNull] public static readonly IEntityDependencyHelper EntityDependencyHelper;

        [NotNull]public static IMemberInfoHelper MemberInfo = new MemberInfoHelper();
        [NotNull]public static IMemberInfoLibrary Library = new MemberInfoLibrary(MemberInfo);
        [NotNull]public static IMapperMethodHelper MapperMethod = new MapperMethodHelper(MemberInfo);

        [NotNull]public static IExpressionBuilder ExpressionBuilder = new ExpressionBuilder(MemberInfo, Library);

        [NotNull]public static IFlatPathParser FlatPathParser = new FlatPathParser();

        [NotNull]public static ICollectionHelper CollectionHelper = new CollectionHelper();

        [NotNull]public static IFlatPathApplicator FlatPathApplicator = new FlatPathApplicator();

        [NotNull] public static IIncludeDescriptionHelper IncludeDescriptionHelper;

        [NotNull] public static IConditionalIncludeCreator ConditionalIncludeCreator;

        [NotNull] public static IMapperResolver MapperResolver;

        [NotNull] public static IKeyExpressionBuilder KeyExpressionBuilder = new KeyExpressionBuilder(MemberInfo, Library);

        [NotNull] public static ISelectHelper SelectHelper;

        static EntityStaticHelpers()
        {
            EntityTypes = new HashSet<Type>(
                typeof(TestDbContext).GetTypeInfo().GetProperties()
                .Where(p => p.PropertyType.GetTypeInfo().IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(DbSet<>))
                .Select(p => p.PropertyType.GetGenericEnumerableArgument()));

            ExternalTypes = new HashSet<Type> {typeof(TestExternalEntity)};

            var mockRetriever = new Mock<IEntityTypesRetriever>(MockBehavior.Strict);
            mockRetriever.Setup(r => r.GetEntityTypes()).Returns(EntityTypes);
            mockRetriever.Setup(r => r.GetExternalEntityTypes()).Returns(ExternalTypes);
            mockRetriever.Setup(r => r.IsEntityType(It.IsAny<Type>())).Returns((Type t) => EntityTypes.Contains(t));
            mockRetriever.Setup(r => r.IsEntityCollectionType(It.IsAny<Type>()))
                .Returns((Type t) => t.IsGenericEnumerable() && EntityTypes.Contains(t.GetGenericEnumerableArgument()));
            mockRetriever.Setup(r => r.IsExternalEntityType(It.IsAny<Type>())).Returns((Type t) => ExternalTypes.Contains(t));
            if (mockRetriever.Object == null)
            {
                throw new NullReferenceException("mockRetriever.Object");
            }
            EntityTypesRetriever = mockRetriever.Object;

            EntityTypesHelper = new EntityTypesHelper(EntityTypesRetriever);
            EntityDependencyHelper = new EntityDependencyHelper(EntityTypesHelper);

            ConditionalIncludeCreator = Activator.CreateInstance(
                new ExpressionsIncludeBuilderBindings().GetBindings()[typeof(IConditionalIncludeCreator)],
                FlatPathParser,
                CollectionHelper,
                MemberInfo,
                EntityTypesHelper) as IConditionalIncludeCreator;

            IncludeDescriptionHelper = new IncludeDescriptionHelper(
                FlatPathParser,
                CollectionHelper,
                ExpressionBuilder,
                EntityTypesHelper);

            MapperResolver = new TestMapperResolver();

            SelectHelper = AbstractMapperDependenciesFactory.GetDependency<ISelectHelper>(
                EntityTypesRetriever, MapperResolver);
        }

        private class TestMapperResolver : IMapperResolver
        {
            private readonly IDynamicEntityMapFuncCreator _funcCreator;
            private readonly IDynamicEntityMapFuncCache _funcCache;

            public TestMapperResolver()
            {
                _funcCreator = new DynamicEntityMapFuncCreator(this, MemberInfo, MapperMethod);
                _funcCache = new DynamicEntityMapFuncCache(_funcCreator);
            }

            public IMapper<TFrom, TTo> TryGet<TFrom, TTo>()
            {
                if (typeof (TFrom) == typeof (object))
                {
                    var type = typeof (DynamicEntityToEntityMapper<>).MakeGenericType(typeof (TTo));
                    return Activator.CreateInstance(type, _funcCache) as IMapper<TFrom, TTo>;
                }
                throw new InvalidOperationException("No such mapper");
            }

            public LooseMapper<TFrom, TTo> GetDefault<TFrom, TTo>()
                where TFrom : class
                where TTo : class, new()
            {
                return AbstractMapperFactory.Create<LooseMapper<TFrom, TTo>>(EntityTypesRetriever, this);
            }

            public DynamicMapper<TFrom, TTo> GetDynamic<TFrom, TTo>()
                where TFrom : class
                where TTo : class, new()
            {
                return AbstractMapperFactory.Create<DynamicMapper<TFrom, TTo>>(EntityTypesRetriever, this);
            }

            public IDynamicEntityToEntityMapper<TTo> GetTypeDeterminingMapper<TTo>() where TTo : class, new()
            {
                return AbstractMapperFactory.Create<DynamicEntityToEntityMapper<TTo>>(EntityTypesRetriever, this);
            }
        }
    }
}
