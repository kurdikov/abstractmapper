﻿namespace BaseTestClasses.Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
#if NET45
    using System.Data.Common;
    using System.Data.Entity;
#elif NETSTANDARD15
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Diagnostics;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.Expressions.Helpers.Entity.Attributes;

    public class TestDbContext : DbContext
    {
        public DbSet<First> Firsts { get; set; }
        public DbSet<Second> Seconds { get; set; }
        public DbSet<Third> Thirds { get; set; }
        public DbSet<Fourth> Fourths { get; set; }
        public DbSet<Fifth> Fifths { get; set; }
        public DbSet<NotInterestingSecond> NotInterestingSeconds { get; set; }
        public DbSet<SecondWithFkPk> SecondWithFkPks { get; set; }
        public DbSet<ThirdWithPkPk> ThirdWithPkPks { get; set; }
        public DbSet<DiamondOne> DiamondOnes { get; set; }
        public DbSet<DiamondTwo> DiamondTwos { get; set; }
        public DbSet<DiamondBottom> DiamondBottoms { get; set; }
        public DbSet<Zero> Zeros { get; set; }
        public DbSet<Tree> Trees { get; set; }
        public DbSet<CompositePk> CompositePks { get; set; }
        public DbSet<CompositeFk> CompositeFks { get; set; }
        public DbSet<CompositeRoot> CompositeRoots { get; set; }
        public DbSet<ComplexEntity> ComplexEntityEntities { get; set; }

#if NET45
        public TestDbContext(string connectionString) : base(connectionString)
        {
            Database.SetInitializer<TestDbContext>(null);
            Database.Log = s => Debug.Print(s);
        }

        public TestDbContext(DbConnection connection, bool owns = true) : base(connection, owns)
        {
            Database.SetInitializer<TestDbContext>(null);
            Database.Log = s => Debug.Print(s);
            //Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
#elif NETSTANDARD15
        public TestDbContext(DbContextOptions<TestDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompositePk>().HasKey(pk => new { pk.Part1, pk.Part2 });
        }
#endif
    }

    public enum FirstEnum
    {
        Val1,
        Val2,
    }

    [Table("Firsts")]
    public class First : IDeletable, IEntityWithId<int>, IBadFirst, IComplexEntity
    {
        public First()
        {
            Seconds = new HashSet<Second>();
            NotInterestingSeconds = new HashSet<NotInterestingSecond>();
            SecondWithFkPks = new HashSet<SecondWithFkPk>();
        }

        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime ImportantMoment { get; set; }
        public int? ZeroId { get; set; }
        public bool? NullableBool { get; set; }
        public Guid? ExternalEntityNullableId { get; set; }
        public Guid ExternalEntityId { get; set; }

        [NotMapped]
        public TestExternalEntity ExternalEntityNullable { get; set; }
        [NotMapped]
        public TestExternalEntity ExternalEntity { get; set; }

        [NotMapped]
        [DependentOnColumn("NullableBool")]
        public bool NullableBoolIsTrue {get { return NullableBool == true; }}

        [NotMapped]
        public FirstEnum FirstEnum { get; set; }

        [ForeignKey("ZeroId")]
        public Zero Zero { get; set; }

        [InverseProperty("First")]
        public ICollection<Second> Seconds { get; set; }

        [InverseProperty("First")]
        public ICollection<NotInterestingSecond> NotInterestingSeconds { get; set; }

        [InverseProperty("First")]
        public ICollection<SecondWithFkPk> SecondWithFkPks { get; set; }

        [InverseProperty("First")]
        public ICollection<DiamondOne> DiamondOnes { get; set; }
        [InverseProperty("First")]
        public ICollection<DiamondTwo> DiamondTwos { get; set; }
        [InverseProperty("First")]
        public ICollection<DiamondBottom> DiamondBottoms { get; set; }

        [InverseProperty("First")]
        public ICollection<Tree> Trees { get; set; }
        
        [NotMapped]
        [NavigationWrapper("Seconds")]
        public IEnumerable<ISecond> SecondsWrapper {get { return Seconds; }} 
        [NotMapped]
        IEnumerable<IBadSecond> IBadFirst.Seconds { get { return Seconds; }}

        public bool IsSaved { get; set; }
    }

    [Table("ComplexEntities")]
    public class ComplexEntity : IComplexEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public bool IsSaved { get; set; }
    }
   
    public interface IFirst
    {
        int Id { get; }
        bool NullableBoolIsTrue { get; }
        string Name { get; set; }
        IEnumerable<ISecond> SecondsWrapper { get; }
    }

    public interface IBadFirst : IFirst
    {
        IEnumerable<IBadSecond> Seconds { get; } 
    }
    
    public interface ISecond
    {
        int Id { get; set; }
        string Type { get; }
    }

    public interface IBadSecond : ISecond
    {
        IFirst First { get; }
    }

    [Table("NonStandardPkNames")]
    public class NonStandardPkName
    {
        public int NonStandardPkNameId { get; set; }
    }

    [Table("Zeroes")]
    public class Zero
    {
        public Zero()
        {
        }

        public int Id { get; set; }
        public Guid Guid { get; set; }
        public DateTime Date { get; set; }
        public string String { get; set; }

        [InverseProperty("Zero")]
        public virtual ICollection<First> Firsts { get; set; }
    }

    [Table("Seconds")]
    public class Second : IBadSecond, IDeletable
    {
        public Second()
        {
            Thirds = new HashSet<Third>();
        }

        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Type { get; set; }
        public int FirstId { get; set; }
        public int? NullableFirstId { get; set; }
        public bool Bool { get; set; }

        [ForeignKey("FirstId")]
        public First First { get; set; }

        [ForeignKey("NullableFirstId")]
        public First NullableFirst { get; set; }

        [InverseProperty("Second")]
        public ICollection<Third> Thirds { get; set; } 

        IFirst IBadSecond.First {get { return First; }}
        public bool IsDeleted { get; set; }
    }

    [Table("NotInterestingSeconds")]
    public class NotInterestingSecond
    {
        public NotInterestingSecond()
        {
        }

        public int Id { get; set; }
        public int ClientId { get; set; }
        public int FirstId { get; set; }
        public Guid Guid { get; set; }

        [ForeignKey("FirstId")]
        public First First { get; set; }
    }

    [Table("SecondWithFkPks")]
    public class SecondWithFkPk
    {
        public SecondWithFkPk()
        {
        }

        [Key, ForeignKey("Third")]
        public Guid Id { get; set; }
        public string Type { get; set; }

        [ForeignKey("First")]
        public int FirstId { get; set; }

        public First First { get; set; }

        [ForeignKey("Id")]
        public ThirdWithPkPk Third { get; set; }
    }

    [Table("DiamondOnes")]
    public class DiamondOne : IEntityWithId<int>
    {
        public DiamondOne()
        {
        }

        public int Id { get; set; }
        public int FirstId { get; set; }
        public int? DiamondBottomId { get; set; }
        public int Data { get; set; }

        [ForeignKey("FirstId")]
        public First First { get; set; }
        [ForeignKey("DiamondBottomId")]
        public DiamondBottom DiamondBottom { get; set; }
    }

    [Table("DiamondTwos")]
    public class DiamondTwo : IEntityWithId<int>
    {
        public DiamondTwo()
        {
        }

        public int Id { get; set; }
        public int FirstId { get; set; }
        public int? DiamondBottomId { get; set; }
        public int Data { get; set; }

        [ForeignKey("FirstId")]
        public First First { get; set; }
        [ForeignKey("DiamondBottomId")]
        public DiamondBottom DiamondBottom { get; set; }
    }

    [Table("DiamondBottoms")]
    public class DiamondBottom : IEntityWithId<int>
    {
        public DiamondBottom()
        {
            DiamondOnes = new HashSet<DiamondOne>();
            DiamondTwos = new HashSet<DiamondTwo>();
        }

        public int Id { get; set; }
        public int FirstId { get; set; }
        public int Data { get; set; }

        [ForeignKey("FirstId")]
        public First First { get; set; }

        [InverseProperty("DiamondBottom")]
        public ICollection<DiamondOne> DiamondOnes { get; set; }
        [InverseProperty("DiamondBottom")]
        public ICollection<DiamondTwo> DiamondTwos { get; set; } 
    }

    [Table("ThirdWithPkPks")]
    public class ThirdWithPkPk
    {
        public ThirdWithPkPk()
        {
        }

        public Guid Id { get; set; }
        public int Data { get; set; }

        [InverseProperty("Third")]
        public SecondWithFkPk Second { get; set; }
    }

    [Table("Thirds")]
    public class Third
    {
        public Third()
        {
        }

        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Type { get; set; }
        public int SecondId { get; set; }
        public Guid? FourthId { get; set; }
        public int? NullableSecondId { get; set; }

        [ForeignKey("SecondId")]
        public Second Second { get; set; }

        [ForeignKey("NullableSecondId")]
        public Second NullableSecond { get; set; }

        [ForeignKey("FourthId")]
        public Fourth Fourth { get; set; } 
    }

    [Table("Fourths")]
    public class Fourth
    {
        public Fourth()
        {
            Thirds = new HashSet<Third>();
            Fifths = new HashSet<Fifth>();
        }

        public Guid Id { get; set; }
        public int ClientId { get; set; }

        [InverseProperty("Fourth")]
        public ICollection<Third> Thirds { get; set; }

        [InverseProperty("Fourth")]
        public ICollection<Fifth> Fifths { get; set; } 
    }

    [Table("Fifths")]
    public class Fifth
    {
        public Fifth()
        {
        }

        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Type { get; set; }
        public Guid FourthId { get; set; }

        [ForeignKey("FourthId")]
        public Fourth Fourth { get; set; }
    }

    [Table("Trees")]
    public class Tree : ITreeElement<Tree, int>
    {
        public Tree()
        {
            Children = new HashSet<Tree>();
        }

        public int? ParentId { get; set; }

        public int Id { get; set; }

        public int Number { get; set; }

        public string Data { get; set; }

        public int AdditionalData { get; set; }

        public string NotEnum { get; set; }
        
        [InverseProperty("Parent")]
        public ICollection<Tree> Children { get; set; }

        [ForeignKey("ParentId")]
        public Tree Parent { get; set; }

        [ForeignKey("FirstId")]
        public First First { get; set; }

        public int FirstId { get; set; }
    }

    [Table("CompositeRoots")]
    public class CompositeRoot
    {
        public CompositeRoot()
        {
            CompositePks = new HashSet<CompositePk>();
        }

        public int Id { get; set; }
        public string Data { get; set; }
        [InverseProperty("CompositeRoot")]
        public ICollection<CompositePk> CompositePks { get; set; } 
    }

    [Table("CompositePks")]
    public class CompositePk
    {
        public CompositePk()
        {
            CompositeFks = new HashSet<CompositeFk>();
        }

        [Key]
        [Column(Order = 0)]
        public Guid Part1 { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid Part2 { get; set; }

        public string Data { get; set; }

        public int CompositeRootId { get; set; }

        [ForeignKey("CompositeRootId")]
        public CompositeRoot CompositeRoot { get; set; }

        [InverseProperty("CompositePk")]
        public ICollection<CompositeFk> CompositeFks { get; set; } 
    }

    [Table("CompositeFks")]
    public class CompositeFk
    {
        public CompositeFk()
        {
        }

        public int Id { get; set; }

        [Column(Order = 0)]
        public Guid Part1Id { get; set; }

        [Column(Order = 1)]
        public Guid Part2Id { get; set; }

        public string Data { get; set; }

        [ForeignKey("Part1Id, Part2Id")]
        public CompositePk CompositePk { get; set; }
    }

    public class TestExternalEntity : IExternalEntity<Guid>
    {
        public Guid Id { get; set; }
    }

    public class DynEntityInt
    {
        public int Id { get; set; }
        public IEnumerable<object> Seconds { get; set; }
        public IEnumerable<object> NotInterestingSeconds { get; set; }
        public IEnumerable<object> SecondWithFkPks { get; set; }
        public IEnumerable<object> DiamondOnes { get; set; }
        public IEnumerable<object> DiamondTwos { get; set; }
        public IEnumerable<object> DiamondBottoms { get; set; }
        public IEnumerable<object> Thirds { get; set; }
    }
    public class DynEntityGuid
    {
        public Guid Id { get; set; }
    }
}
