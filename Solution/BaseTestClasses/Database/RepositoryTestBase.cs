﻿namespace BaseTestClasses.Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Common;
#if NET45
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure.Pluralization;
    using System.Data.SQLite;
#elif NETSTANDARD15
    using Microsoft.Data.Sqlite;
    using Microsoft.EntityFrameworkCore;
#endif
    using System.IO;
    using System.Linq;
    using System.Text;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Mappers;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository;
    using TsSoft.EntityRepository.External;
    using TsSoft.EntityRepository.Factory;
    using TsSoft.EntityRepository.IncludeProcessors;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using System.Reflection;

#if NET45
    [DeploymentItem("x86/SQLite.Interop.dll", "x86")]
    [DeploymentItem("x64/SQLite.Interop.dll", "x64")]
    [DeploymentItem("System.Data.SQLite.dll")]
    [DeploymentItem("System.Data.SQLite.EF6.dll")]
    [DeploymentItem("System.Data.SQLite.Linq.dll")]
#endif
    [TestClass]
    public abstract class RepositoryTestBase
    {
        [NotNull]protected DbConnection Connection { get; set; }
        [NotNull]protected TestDbContext Context { get; set; }
        [NotNull]Mock<IItemWrapper<DbContext>> MockWrapper { get; set; }
        [NotNull]protected IItemWrapper<DbContext> ContextWrapper { get; set; }
        [NotNull]protected IUniversalIncludeProcessor IncludeProcessor { get; set; }
        [NotNull]protected Mock<IRepositoryFactory> MockRepositoryFactory { get; set; }
        [NotNull]protected IRepositoryFactory RepositoryFactory { get; set; }
        [NotNull]protected IMapperResolver MapperResolver { get; set; }

        private class EnglishPluralizationService
        {
            internal string Pluralize(string name)
            {
                return name + "s";
            }
        }

        private DbConnection CreateInMemorySqliteConnection()
        {
#if NET45
            var connection = new SQLiteConnection(@"Data Source=:memory:;Version=3;New=True;BinaryGuid=False;");
#elif NETSTANDARD15
            var connection = new SqliteConnection(@"Data Source=:memory:");
#endif
            connection.Open();
            return connection;
        }

        [TestInitialize]
        public void TestInit()
        {
            Connection = CreateInMemorySqliteConnection();
            Context = CreateContext(Connection);

            var pluralizer = new EnglishPluralizationService();
            var clrToSqlite = new Dictionary<Type, string>
                {
                    {typeof(int), "INTEGER"},
                    {typeof(long), "INTEGER"},
                    {typeof(double), "REAL"},
                    {typeof(float), "REAL"},
                    {typeof(decimal), "NUMERIC"},
                    {typeof(bool), "BOOLEAN"},
                    {typeof(DateTime), "TIMESTAMP"},
                    {typeof(string), "NVARCHAR(1000)"},
                    {typeof(Guid), "BLOB"},
                };
            var autoincrementPks = new HashSet<Type> { typeof(int), typeof(long) };
            foreach (var type in EntityStaticHelpers.EntityTypes)
            {
                var tableAttribute = type.GetTypeInfo().GetCustomAttribute(typeof(TableAttribute)) as TableAttribute;
                var tableName = tableAttribute != null ? tableAttribute.Name : pluralizer.Pluralize(type.Name);
                var pk = EntityStaticHelpers.EntityTypesHelper.GetPrimaryKey(type);
                var nonPkProps = ValueHoldingMember.GetValueHoldingMembers(type)
                    .Where(p => !pk.Contains(p, MemberInfoComparer.Instance) && EntityStaticHelpers.EntityTypesHelper.IsColumnProperty(p))
                    .ToList();
                var commandBuilder = new StringBuilder();
                commandBuilder.AppendFormat("CREATE TABLE [{0}] (", tableName);
                var firstPk = true;
                var hasAutoIncrement = false;
                foreach (var pkProp in pk)
                {
                    var pkGeneration = pkProp.GetCustomAttribute<DatabaseGeneratedAttribute>();
                    if (!firstPk)
                    {
                        commandBuilder.Append(',');
                    }
                    var autoIncrement = autoincrementPks.Contains(pkProp.ValueType)
                        && (pkGeneration == null || pkGeneration.DatabaseGeneratedOption != DatabaseGeneratedOption.None);
                    if (hasAutoIncrement && autoIncrement)
                    {
                        throw new InvalidDataException(string.Format("Autoincrement in composite keys is not supported : {0} in {1}", pkProp, type));
                    }
                    hasAutoIncrement = hasAutoIncrement || autoIncrement;
                    var propType = pkProp.ValueType.NullableStructUnderlyingType();
                    commandBuilder.AppendFormat("\n[{0}] {1} NOT NULL {2}",
                        pkProp.Name,
                        clrToSqlite[propType],
                        autoIncrement
                            ? "PRIMARY KEY AUTOINCREMENT"
                            : "");
                    firstPk = false;
                }

                foreach (var prop in nonPkProps)
                {
                    if (prop == null)
                    {
                        throw new NullReferenceException("prop");
                    }
                    var propType = prop.ValueType.NullableStructUnderlyingType();
                    commandBuilder.AppendFormat(",\n[{0}] {1} {2}",
                        prop.Name,
                        clrToSqlite[propType],
                        prop.ValueType.IsNullableStruct() || prop.ValueType == typeof(string) ? "NULL" : "NOT NULL");
                }

                if (!hasAutoIncrement)
                {
                    commandBuilder.AppendFormat(",\nPRIMARY KEY ({0})", string.Join(",", pk.Select(pi => pi.Name)));
                }
                commandBuilder.Append("\n);");

                var command = commandBuilder.ToString();

                Context.Database.ExecuteSqlCommand(command);
            }

            MockWrapper = new Mock<IItemWrapper<DbContext>>(MockBehavior.Strict);
            MockWrapper.Setup(w => w.Current).Returns(Context);
            ContextWrapper = MockWrapper.Object;

            MockRepositoryFactory = new Mock<IRepositoryFactory>(MockBehavior.Strict);
            RepositoryFactory = MockRepositoryFactory.Object;

            IncludeProcessor = new UniversalIncludeProcessor(
                EntityStaticHelpers.IncludeDescriptionHelper,
                new ExternalIncludeProcessor(RepositoryFactory, EntityStaticHelpers.MemberInfo),
                new ConditionalIncludeProcessor(
                    EntityStaticHelpers.KeyExpressionBuilder,
                    RepositoryFactory,
                    EntityStaticHelpers.IncludeDescriptionHelper,
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.ConditionalIncludeCreator),
                EntityStaticHelpers.EntityTypesRetriever);

            var setupRepository = EntityStaticHelpers.MemberInfo.GetGenericDefinitionMethodInfo(
                () => SetupRepository<object>(null));
            foreach (var type in EntityStaticHelpers.EntityTypes)
            {
                setupRepository.MakeGenericMethod(type).Invoke(this, new object[] { MockRepositoryFactory });
            }
            var setupExternal = EntityStaticHelpers.MemberInfo.GetGenericDefinitionMethodInfo(
                () => SetupExternalRepository<IExternalEntity>(null));
            foreach (var type in EntityStaticHelpers.ExternalTypes)
            {
                setupExternal.MakeGenericMethod(type).Invoke(this, new object[] { MockRepositoryFactory });
            }
        }

        protected TestDbContext CreateContext(DbConnection connection)
        {
#if NET45
            return new TestDbContext(connection, false);
#elif NETSTANDARD15
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseSqlite(Connection)
                .Options;
            return new TestDbContext(options);
#endif
        }

        protected void Seed<T>(IEnumerable<T> data) where T : class
        {
            using (var seedContext = CreateContext(Connection))
            {
                var table = seedContext.Set<T>();
                table.AddRange(data);
                seedContext.SaveChanges();
            }
        }

        protected void ClearContext()
        {
            foreach (var entry in Context.ChangeTracker.Entries())
            {
                if (entry.Entity != null)
                {
                    entry.State = EntityState.Detached;
                }
            }
        }

        protected void CheckContextRetrievals(int count)
        {
            MockWrapper.VerifyGet(mw => mw.Current, Times.Exactly(count));
        }

        [TestCleanup]
        public void Cleanup()
        {
            Connection?.Close();
        }

        private void SetupRepository<T>(Mock<IRepositoryFactory> mock)
            where T : class
        {
            var transactor = new Transactor();
            var committer = new DatabaseCommitter(ContextWrapper);
            var detacher = new Detacher();
            var includeDescriptionHelper = EntityStaticHelpers.IncludeDescriptionHelper;
            var dynamicEntityToEntityMapper = EntityStaticHelpers.MapperResolver.TryGet<object, T>() as IDynamicEntityToEntityMapper<T>;

            var createRepository = new CreateRepository<T>(ContextWrapper, IncludeProcessor, committer, transactor, detacher);

            var updateRepository = new UpdateRepository<T>(ContextWrapper, IncludeProcessor, committer,
                                                           transactor, detacher);
            var readRepository = new ReadRepository<T>(ContextWrapper, IncludeProcessor, dynamicEntityToEntityMapper, includeDescriptionHelper);
            var deleteRepository = new DeleteRepository<T>(ContextWrapper, IncludeProcessor, committer, transactor, detacher);

            var repo = new BaseRepository<T>(
                ContextWrapper,
                createRepository,
                updateRepository,
                deleteRepository,
                readRepository,
                transactor,
                committer
                );
            mock.Setup(rf => rf.GetBaseRepository<T>()).Returns(repo);
        }

        protected void SetupExternalRepository<T>(Mock<IRepositoryFactory> mock)
            where T : class, IExternalEntity<Guid>
        {
        }
    }
}
