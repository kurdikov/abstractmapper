﻿namespace BaseTestClasses
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    public interface ITestEntity
    {
        [Key]
        int Prop1 { get; set; }
        string Prop2 { get; set; }
    }

    public interface IFullTestEntity
    {
        [Key]
        int Prop1 { get; set; }

        string Prop2 { get; set; }

        int? Prop3 { get; set; }

        IFullTestEntity Parent { get; set; }

        ICollection<IFullTestEntity> Children { get; set; }

        int ExternalId { get; set; }

        [NotMapped]
        ExternalEntity External { get; set; }
    }

    public interface ITestEntityWithEntityProp
    {
        [Key]
        int Prop1 { get; set; }

        TestEntity Parent { get; set; }
    }

    public class TestEntity : ITestEntity, ITestEntityWithEntityProp
    {
        [Key]
        public int Prop1 { get; set; }

        public string Prop2 { get; set; }

        public int? Prop3 { get; set; }

        public TestEntity Parent { get; set; }

        public ICollection<TestEntity> Children { get; set; }

        public int ExternalId { get; set; }

        [NotMapped]
        public ExternalEntity External { get; set; }

        public static PropertyInfo GetProp([NotNull]string name)
        {
            return typeof(TestEntity).GetProperty(name);
        }
    }

    public class TestEntityWithNullableBool : TestEntity
    {
        public bool? Prop4 { get; set; }

        public ICollection<TestEntityWithNullableBool> NChildren { get; set; }

        public new static PropertyInfo GetProp([NotNull]string name)
        {
            return typeof(TestEntityWithNullableBool).GetProperty(name);
        }
    }

    public static class TestEntityProp
    {
        public static PropertyInfo Prop1 = TestEntity.GetProp("Prop1");
        public static PropertyInfo Prop2 = TestEntity.GetProp("Prop2");
        public static PropertyInfo Prop3 = TestEntity.GetProp("Prop3");
        public static PropertyInfo Prop4 = TestEntityWithNullableBool.GetProp("Prop4");
        public static PropertyInfo Parent = TestEntity.GetProp("Parent");
        public static PropertyInfo Children = TestEntity.GetProp("Children");
    }

    public static class TestEntityVh
    {
        public static ValueHoldingMember Prop1 = new ValueHoldingMember(TestEntity.GetProp("Prop1"));
        public static ValueHoldingMember Prop2 = new ValueHoldingMember(TestEntity.GetProp("Prop2"));
        public static ValueHoldingMember Prop3 = new ValueHoldingMember(TestEntity.GetProp("Prop3"));
        public static ValueHoldingMember Prop4 = new ValueHoldingMember(TestEntityWithNullableBool.GetProp("Prop4"));
        public static ValueHoldingMember Parent = new ValueHoldingMember(TestEntity.GetProp("Parent"));
        public static ValueHoldingMember Children = new ValueHoldingMember(TestEntity.GetProp("Children"));
    }

    public class DynamicTestEntity
    {
        public int Prop1 { get; set; }

        public string Prop2 { get; set; }

        public int? Prop3 { get; set; }

        public object Parent { get; set; }

        public IEnumerable<object> Children { get; set; }

        public int ExternalId { get; set; }
    }

    public class ExternalEntity
    {
    }

    public class MappedTestEntity1
    {
        public int Prop1 { get; set; }

        public string Prop2 { get; set; }

        public int? Prop3 { get; set; }

        public int Child1 { get; set; }

        public int Child2 { get; set; }
    }

    public class MappedTestEntity2 : IMappedEntity
    {
        public int Int { get; set; }

        public string String { get; set; }
    }

    public class MappedTestEntity3
    {
        public int? Result { get; set; }
    }

    public interface IMappedEntity
    {
        int Int { get; set; }
        string String { get; set; }
    }
}
