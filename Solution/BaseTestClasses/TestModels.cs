﻿namespace BaseTestClasses
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.Expressions.Helpers.Entity.Attributes;

    public interface IFrom
    {
        int Id { get; }
        string Name { get; }
    }

    public interface IInheritedFrom : IFrom
    {
        Guid Guid { get; }
    }

    public interface IComplexFrom
    {
        int Id { get; }
        IFrom Parent { get; }
        IEnumerable<IFrom> InterfaceChildren { get; } 
    }

    public interface ITwoFroms
    {
        IEnumerable<IFrom> InterfaceChildren { get; }
        IEnumerable<IFrom> AllChildren { get; }
    }

    public interface IFromWithEntity
    {
        int Id { get; }
        From Parent { get; }
        ICollection<From> Children { get; }
    }

    public interface IExplicit
    {
        int Name { get; set; }
    }

    public class From : IFrom, IInheritedFrom, IComplexFrom, IEntityWithId<int>, ITwoFroms, IFromWithEntity, IExplicit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Guid Guid { get; set; }
        public From Parent { get; set; }
        public AEnum AEnum1 { get; set; }
        public AEnum? NullableEnum { get; set; }
        public string AEnumStr { get; set; }
        public int AEnumInt { get; set; }
        public int? NullProperty { get; set; }
        public ICollection<From> Children { get; set; }
        public ICollection<From> Bastards { get; set; }
        public bool Bool { get; set; }
        public bool? NullableBool { get; set; }

        [DependentOnColumn("Bool")]
        [DependentOnColumn("NullableBool")]
        [NotMapped]
        public string Dependent { get; set; }

        public string Field;

        IFrom IComplexFrom.Parent
        {
            get { return Parent; }
        }

        [NavigationWrapper("Children")]
        public IEnumerable<IFrom> InterfaceChildren {get { return Children; }} 

        [NavigationWrapper("Children")]
        [NavigationWrapper("Bastards")]
        public IEnumerable<IFrom> AllChildren {get { return (Children ?? Enumerable.Empty<IFrom>()).Concat(Bastards ?? Enumerable.Empty<IFrom>()); }}

        [DependentOnColumn("Id")]
        int IExplicit.Name {get { return Id; } set { Id = value; }}
    }

    public class FromDerived : From
    {
        public int Derived { get; set; }
    }
    
    public class DynamicFrom
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Derived { get; set; }
        public object Parent { get; set; }
        public IEnumerable<object> Children { get; set; }
        public IEnumerable<object> Bastards { get; set; }
        public bool Bool { get; set; }
        public bool? NullableBool { get; set; }
        public string AEnumStr { get; set; }
        public Guid Guid { get; set; }
        public AEnum AEnum1 { get; set; }
        public int AEnumInt { get; set; }
    }

    public class SimpleTo : ISimpleTo
    {
        public int Id { get; set; }
    }

    public class SimpleEnumerableTo
    {
        public IEnumerable<int> Ids { get; set; }
    }
    public class SimpleListTo
    {
        public List<int> Ids { get; set; }
    }
    public interface ISimpleTo
    {
        int Id { get; }
    }

    public class NotSoSimpleTo
    {
        public int Id { get; set; }
        public int NotSimple { get; set; }
    }

    public class ThreePropertiesTo
    {
        public int Prop1 { get; set; }
        public int Prop2 { get; set; }
        public int Prop3 { get; set; }
    }

    public class SimpleToWithCollection
    {
        public ICollection<SimpleTo> Children { get; set; }
        public ICollection<SimpleTo> Children2 { get; set; }
    }

    public class SimpleToWithCollectionWrapper
    {
        public SimpleToWithCollection SimpleToWithCollection { get; set; }
    }

    public class SimpleToWithCollectionCollectionWrapper
    {
        public ICollection<SimpleToWithCollection> SimpleToWithCollectionCollection { get; set; }
    }

    public class ToWithNotMappedProperty
    {
        public int Id { get; set; }
        public int NotInFrom { get; set; }
    }

    public enum AEnum
    {
        A,
        B
    }

    public enum BEnum : byte
    {
        A,
        B
    }

    public class ToBase
    {
        public int Id { get; set; }
        public int? NullableId { get; set; }
        public string StringId { get; set; }
        public string Id1 { get; set; }
        public int Id2 { get; set; }
        public long Id3 { get; set; }
        public string Name { get; set; }
        public Guid Guid { get; set; }

        public string AEnum1 { get; set; }
        public string AutoAEnum1String { get; set; }
        public int AutoAEnum1Int { get; set; }
        public long AutoAEnum1Long { get; set; }
        public AEnum AEnumStr { get; set; }
        public AEnum AEnumInt { get; set; }

        public int? NullableEnum { get; set; }

        public string Field { get; set; }
    }
    public class ToWithProcessorProps : ToBase
    {
        public int MappedByParentProcessor { get; set; }
        public int MappedByChildrenProcessor { get; set; }
        public int MappedBySelfProcessor { get; set; }

        public int MappedByParentSelectProcessor { get; set; }
        public int MappedByChildrenSelectProcessor { get; set; }
        public int MappedBySelfSelectProcessor { get; set; }

        public int MappedByParentInterfaceProcessor { get; set; }
        public int MappedBySelfInterfaceProcessor { get; set; }

        public int MappedByParentSelectInterfaceProcessor { get; set; }
        public int MappedBySelfSelectInterfaceProcessor { get; set; }
    }
    public class To : ToWithProcessorProps
    {
        public int ComplexPathRule { get; set; }
        public IList<int> ComplexPathRuleWithCollection { get; set; }
        public int NullProperty { get; set; }
        public To Parent { get; set; }
        public IEnumerable<ToBase> Children { get; set; }
        public ICollection<ToBase> ChildrenInterface { get; set; }
        public List<ToBase> ChildrenList { get; set; }
        public HashSet<ToBase> ChildrenSet { get; set; }
        public ToBase[] ChildrenArray { get; set; }
        public Collection<ToBase> ChildrenCollection { get; set; }
    }

    public class SimpleNestedTo
    {
        public int Id { get; set; }
        public SimpleNestedTo Parent { get; set; }
        public IEnumerable<SimpleNestedTo> Children { get; set; } 
    }

    public class ExceptionFrom
    {
        public string Name
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }

    public class ExceptionTo
    {
        public string Name { get; set; }
    }

    public class ToWithoutParent : ToBase
    {
        public IEnumerable<ToBase> Children { get; set; }
        public ICollection<ToBase> ChildrenInterface { get; set; }
        public List<ToBase> ChildrenList { get; set; }
        public HashSet<ToBase> ChildrenSet { get; set; }
        public ToBase[] ChildrenArray { get; set; }
        public Collection<ToBase> ChildrenCollection { get; set; }
    }
    public class ToWithOneParent : ToBase
    {
        public ToWithoutParent Parent { get; set; }
    }
    public class ToWithTwoParents : ToWithProcessorProps
    {
        public ToWithOneParent Parent { get; set; }
    }

    public class DateContainer
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DateTimeOffset DateOffset { get; set; }
        public TimeSpan Time { get; set; }

        public ICollection<DateContainer> Children { get; set; }
    }

    public class CollectionContainer<T>
    {
        public IEnumerable<T> EnumerableOne { get; set; }
        public IEnumerable<T> EnumerableTwo { get; set; }
        public IReadOnlyCollection<T> ReadOnlyCollection { get; set; }
        public ICollection<T> Collection { get; set; }
        public List<T> List { get; set; }
        public T[] Array { get; set; }
    }

    public class CollectionContainerMapped<T>
    {
        public IEnumerable<T> EnumerableOne { get; set; }
        public IReadOnlyCollection<T> EnumerableOneRoc { get; set; }
        public ICollection<T> EnumerableOneC { get; set; }
        public List<T> EnumerableOneList { get; set; }
        public T[] EnumerableOneArray { get; set; }

        public IEnumerable<T> EnumerableTwo { get; set; }
        public IReadOnlyCollection<T> EnumerableTwoRoc { get; set; }
        public ICollection<T> EnumerableTwoC { get; set; }
        public List<T> EnumerableTwoList { get; set; }
        public T[] EnumerableTwoArray { get; set; }

        public IEnumerable<T> ReadOnlyCollection { get; set; }
        public IReadOnlyCollection<T> ReadOnlyCollectionRoc { get; set; }
        public ICollection<T> ReadOnlyCollectionC { get; set; }
        public List<T> ReadOnlyCollectionList { get; set; }
        public T[] ReadOnlyCollectionArray { get; set; }

        public IEnumerable<T> Collection { get; set; }
        public IReadOnlyCollection<T> CollectionRoc { get; set; }
        public ICollection<T> CollectionC { get; set; }
        public List<T> CollectionList { get; set; }
        public T[] CollectionArray { get; set; }

        public IEnumerable<T> List { get; set; }
        public IReadOnlyCollection<T> ListRoc { get; set; }
        public ICollection<T> ListC { get; set; }
        public List<T> ListList { get; set; }
        public T[] ListArray { get; set; }

        public IEnumerable<T> Array { get; set; }
        public IReadOnlyCollection<T> ArrayRoc { get; set; }
        public ICollection<T> ArrayC { get; set; }
        public List<T> ArrayList { get; set; }
        public T[] ArrayArray { get; set; }
    }

}
