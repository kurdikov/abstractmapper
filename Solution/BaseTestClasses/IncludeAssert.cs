﻿namespace BaseTestClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Extensions;

    public static class IncludeAssert
    {
        public static void AreEqual<T>(Expression<Func<T, object>> expected, Expression<Func<T, object>> actual)
        {
            if (!IncludeEqualityComparer<T>.Instance.Equals(expected, actual))
            {
                throw new AssertFailedException(string.Format("IncludeAssert.AreEqual failed. Expected: {0}. Actual: {1}.", expected, actual));
            }
        }

        public static void Contains<T>(Expression<Func<T, object>> expected,
            IEnumerable<Expression<Func<T, object>>> actualCollection)
        {
            if (actualCollection == null)
            {
                throw new AssertFailedException(string.Format("IncludeAssert.Contains failed. Expected: {0}. Actual: null.", expected));
            }
            var coll = actualCollection.ToReadOnlyCollectionIfNeeded();
            if (coll.All(i => !IncludeEqualityComparer<T>.Instance.Equals(expected, i)))
            {
                throw new AssertFailedException(string.Format("IncludeAssert.Contains failed. Expected: {0}. Actual: {1}.", expected, string.Join(", ", coll)));
            }
        }
    }
}