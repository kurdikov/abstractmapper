﻿namespace BaseTestClasses
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.SelectBuilder.Models;

    public static class SelectAssert
    {
        public static void AreEqual<T>(Expression<T> expected, Expression<T> actual)
        {
            ExpressionDifference difference;
            if (!SelectComparer.Instance.AreEqual(
                expected, actual,
                new Dictionary<ParameterExpression, ParameterExpression>(), new Dictionary<LabelTarget, LabelTarget>(),
                out difference))
            {
                throw new AssertFailedException(string.Format("SelectAssert.AreEqual failed. Expected: {0}. Actual: {1}. Difference: {2}.", expected, actual, difference));
            }
        }

        public static void NoReusedTypes<T>(IReadOnlySelectExpression<T> actual)
        {
            Assert.IsNotNull(actual);
            var reusedType = NewExpressionTypeUniquenessChecker.Check(actual.Select);
            if (reusedType != null)
            {
                throw new AssertFailedException(string.Format("SelectAssert.NoReusedTypes failed. Type {0} is reused in {1}", reusedType, actual.Select));
            }
        }
    }
}
