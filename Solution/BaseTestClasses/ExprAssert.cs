namespace BaseTestClasses
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.Expressions.Helpers;

    public static class ExprAssert
    {
        public static void NoInnerLambdas(Expression expression)
        {
            LambdaCheckerVisitor.Instance.Visit(expression);
        }

        private class LambdaCheckerVisitor : ExpressionVisitor
        {
            [NotNull]
            public static readonly LambdaCheckerVisitor Instance = new LambdaCheckerVisitor();

            protected override Expression VisitLambda<T>(Expression<T> node)
            {
                throw new AssertFailedException(string.Format("Expression contains a lambda: {0}", node));
            }
        }

        public static void AreEqual<T>(Expression<T> expected, Expression<T> actual)
        {
            ExpressionDifference difference;
            if (!TestExpressionComparer.Instance.AreEqual(expected, actual, out difference))
            {
                throw new AssertFailedException(string.Format("ExprAssert.AreEqual failed. Expected: {0}. Actual: {1}. Difference: {2}", expected, actual, difference));
            }
        }
        public static void AreNotEqual<T>(Expression<T> expected, Expression<T> actual)
        {
            if (TestExpressionComparer.Instance.Equals(expected, actual))
            {
                throw new AssertFailedException(string.Format("ExprAssert.AreNotEqual failed. Expected: {0}. Actual: {1}.", expected, actual));
            }
        }
        public static void AreEqual(Expression expected, Expression actual)
        {
            ExpressionDifference difference;
            if (!TestExpressionComparer.Instance.AreEqual(expected, actual, out difference))
            {
                throw new AssertFailedException(string.Format("ExprAssert.AreEqual failed. Expected: {0}. Actual: {1}. Difference: {2}", expected, actual, difference));
            }
        }
        [StringFormatMethod("format")]
        public static void AreEqual<T>(Expression<T> expected, Expression<T> actual, string format, params object[] param)
        {
            if (!TestExpressionComparer.Instance.Equals(expected, actual))
            {
                throw new AssertFailedException(string.Format(format, param));
            }
        }
        [StringFormatMethod("format")]
        public static void AreEqual(Expression expected, Expression actual, string format, params object[] param)
        {
            if (!TestExpressionComparer.Instance.Equals(expected, actual))
            {
                throw new AssertFailedException(string.Format(format, param));
            }
        }
        public static void AreEqual<T>(Expression<T> expected, Expression<T> actual, [NotNull]Dictionary<ParameterExpression, ParameterExpression> parameterMap)
        {
            ExpressionDifference diff;
            if (!TestExpressionComparer.Instance.AreEqual(expected, actual, parameterMap, new Dictionary<LabelTarget, LabelTarget>(), out diff))
            {
                throw new AssertFailedException(string.Format("ExprAssert.AreEqual failed. Expected: {0}. Actual: {1}. Difference: {2}.", expected, actual, diff));
            }
        }
        public static void AreEqual(Expression expected, Expression actual, [NotNull]Dictionary<ParameterExpression, ParameterExpression> parameterMap)
        {
            ExpressionDifference diff;
            if (!TestExpressionComparer.Instance.AreEqual(expected, actual, parameterMap, new Dictionary<LabelTarget, LabelTarget>(), out diff))
            {
                throw new AssertFailedException(string.Format("ExprAssert.AreEqual failed. Expected: {0}. Actual: {1}. Difference: {2}.", expected, actual, diff));
            }
        }

        public static void AreEqualWithConstants(Expression expected, Expression actual)
        {
            ExpressionDifference diff;
            if (!TestExpressionComparer.ConstantComputingInstance.AreEqual(expected, actual, out diff))
            {
                throw new AssertFailedException(string.Format("ExprAssert.AreEqualWithConstants failed. Expected: {0}. Actual: {1}. Difference: {2}.", expected, actual, diff));
            }
        }

        public static void AreEqualWithConstants<T>(Expression<T> expected, Expression<T> actual)
        {
            ExpressionDifference diff;
            if (!TestExpressionComparer.ConstantComputingInstance.AreEqual(expected, actual, out diff))
            {
                throw new AssertFailedException(string.Format("ExprAssert.AreEqualWithConstants failed. Expected: {0}. Actual: {1}. Difference: {2}.", expected, actual, diff));
            }
        }

        public static void IsLambdaBody<T>(Expression<T> expectedLambda, Expression actual)
        {
            ExpressionDifference diff;
            var parameters = ParameterExtractor.GetParameters(actual);
            var parameterMap = parameters.ToDictionary(param => param, param => expectedLambda.Parameters[0]);
            if (!TestExpressionComparer.Instance.AreEqual(actual, expectedLambda.Body, parameterMap, new Dictionary<LabelTarget, LabelTarget>(), out diff))
            {
                throw new AssertFailedException(string.Format("ExprAssert.IsLambdaBody failed. Expected: {0}. Actual: {1}. Difference: {2}.", expectedLambda.Body, actual, diff));
            }
        }

        private class ParameterExtractor : ExpressionVisitor
        {
            [NotNull]
            private readonly HashSet<ParameterExpression> _parameters = new HashSet<ParameterExpression>();

            public static IEnumerable<ParameterExpression> GetParameters(Expression expr)
            {
                var extr = new ParameterExtractor();
                extr.Visit(expr);
                return extr._parameters;
            }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                _parameters.Add(node);
                return base.VisitParameter(node);
            }
        }
    }
}
