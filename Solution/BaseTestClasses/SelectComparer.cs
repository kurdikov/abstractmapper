namespace BaseTestClasses
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers;

    public class SelectComparer : TestExpressionComparer
    {
        [NotNull]
        public new static SelectComparer Instance = new SelectComparer();

        public SelectComparer()
            : base(
                compareTypes: false,
                computeClosures: false,
                computeConstantMembers: false,
                orderMemberNames: true)
        {
        }

        private Expression RemoveToList(Expression x)
        {
            var xMethod = x as MethodCallExpression;
            if (xMethod != null && StaticHelpers.Library.IsEnumerableToList(xMethod.Method))
            {
                x = xMethod.Arguments[0];
            }
            return x;
        }

        protected override bool AreEqual(MemberInfo x, MemberInfo y, Expression xParent, Expression yParent, out ExpressionDifference diff)
        {
            diff = null;
            return IsShadow(x, y)
                || IsShadow(y, x)
                || base.AreEqual(x, y, xParent, yParent, out diff);
        }

        private bool IsShadow(MemberInfo x, MemberInfo y)
        {
            var xProp = x as PropertyInfo;
            var yProp = y as PropertyInfo;
            return xProp != null && yProp != null
                && xProp.Name.StartsWith("Shadow_")
                && yProp.Name == StaticHelpers.MemberNamingHelper.WriteableShadow(xProp.Name.Substring("Shadow_".Length));
        }

        public override bool AreEqual(Expression x, Expression y,
            Context context,
            out ExpressionDifference difference)
        {
            return base.AreEqual(RemoveToList(x), RemoveToList(y), context, out difference);
        }
    }
}
