namespace BaseTestClasses
{
    using System.Diagnostics;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;

    public class TestExpressionComparer : ExpressionEqualityComparer
    {
        [NotNull]
        public new static TestExpressionComparer Instance = new TestExpressionComparer();
        [NotNull]
        public new static TestExpressionComparer ConstantComputingInstance = new TestExpressionComparer(false, true, true);

        public TestExpressionComparer()
            : base(false)
        {
        }

        public TestExpressionComparer(bool computeClosures, bool compareTypes = true, bool computeConstantMembers = false, bool orderMemberNames = false) : base(computeClosures, compareTypes, computeConstantMembers, orderMemberNames)
        {
        }

        public override bool AreEqual(Expression x, Expression y,
            Context context,
            out ExpressionDifference diff)
        {
            Debug.WriteLine("Comparing {0} and {1}", x, y);
            var result = base.AreEqual(x, y, context, out diff);
            Debug.WriteLine("Compared {0} and {1}.\n Result: {2}", x, y, result);
            return result;
        }
    }
}
