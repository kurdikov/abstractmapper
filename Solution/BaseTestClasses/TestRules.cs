﻿namespace BaseTestClasses
{
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules;

    public static class ToBaseRules<T> where T : ToBase
    {
        [NotNull]
        public static MapRules<T, From> MapRules = new MapRules<T, From>
        {
            {to => to.Id, from => from.Id},
            {to => to.Id1, from => from.Id, i => i.ToString("D2")},
            {to => to.Id2, from => from.Id},
            {to => to.Id3, from => from.Id, i => i},
            {to => to.NullableId, from => from.Id},
            {to => to.AEnum1,(AEnum e) => e.ToString()},
            {to => to.StringId, from => from.Id},
            {to => to.AutoAEnum1String, from => from.AEnum1},
            {to => to.AutoAEnum1Int, from => from.AEnum1},
            {to => to.AutoAEnum1Long, from => from.AEnum1},
        };
    }
    public static class ToProcessedPropsRules<T> where T : ToWithProcessorProps
    {
        [NotNull]
        public static MapRules<T, From> MapRules = new EntityMapRules<T, From>
        {
            {to => to.MappedBySelfProcessor, from => from, IncludeProviderRule.Rule, TestEntityProcessor.Processor, (self, proc) => proc.Process(self)},
            {to => to.MappedByParentProcessor, from => from.Parent, IncludeProviderRule.Rule, TestEntityProcessor.Processor, (pere, proc) => proc.Process(pere)},
            {to => to.MappedByChildrenProcessor, from => from.Children, IncludeCollectionProviderRule.Rule, TestEntityProcessor.Processor, (chld, proc) => 
                proc.ProcessMany(chld)},
            {to => to.MappedBySelfSelectProcessor, from => from, SelectProviderRule.Rule, TestEntitySelectProcessor.Proc, (self, proc) => proc.Process(self)},
            {to => to.MappedByParentSelectProcessor, from => from.Parent, SelectProviderRule.Rule, TestEntitySelectProcessor.Proc, (pere, proc) => proc.Process(pere)},
            {to => to.MappedByChildrenSelectProcessor, from => from.Children, SelectCollectionProviderRule.Rule, TestEntitySelectProcessor.Proc, (chld, proc) => 
                proc.ProcessMany(chld)},
            {to => to.MappedBySelfInterfaceProcessor, from => (IFrom)from, IncludeProviderRule.Rule, TestEntityInterfaceProcessor.Proc, (self, proc) => proc.Process(self)},
            {to => to.MappedByParentInterfaceProcessor, from => (IFrom)from.Parent, IncludeProviderRule.Rule, TestEntityInterfaceProcessor.Proc, (pere, proc) => proc.Process(pere)},
            {to => to.MappedBySelfSelectInterfaceProcessor, from => (IFrom)from, SelectProviderRule.Rule, TestEntityInterfaceSelectProcessor.Proc, (self, proc) => proc.Process(self)},
            {to => to.MappedByParentSelectInterfaceProcessor, from => (IFrom)from.Parent, SelectProviderRule.Rule, TestEntityInterfaceSelectProcessor.Proc, (pere, proc) => proc.Process(pere)},
        };
    }
}
