﻿namespace BaseTestClasses
{
    using System.Collections.Generic;

    public static class CollectionComparer
    {
        public static bool HaveSameElements<T>(IEnumerable<T> first, IEnumerable<T> second)
        {
            if (first == null || second == null)
            {
                return first == null && second == null;
            }
            var result = new HashSet<T>(first).SetEquals(second);
            return result;
        }
    }
}
