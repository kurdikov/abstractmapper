﻿namespace BaseTestClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.SelectBuilder.Models;

    public class TestEntityProcessor : IIncludeProvider<From>
    {
        public readonly static TestEntityProcessor Processor = new TestEntityProcessor();

        public int Process(From from)
        {
            return from != null ? from.Id * 2 : 987;
        }

        public int ProcessMany(IEnumerable<From> from)
        {
            return from != null ? from.Aggregate(0, (current, f) => current + f.Id) : 654;
        }

        public IReadOnlyIncludes<From> Includes
        {
            get
            {
                return new Includes<From>
                    {
                        f => f.Children,
                    };
            }
        }
    }

    public class TestEntityInterfaceProcessor : IIncludeProvider<IFrom>
    {
        public readonly static TestEntityInterfaceProcessor Proc = new TestEntityInterfaceProcessor();

        public int Process(IFrom from)
        {
            return from != null ? from.Id * 17 : 19;
        }

        public IReadOnlyIncludes<IFrom> Includes
        {
            get { return new Includes<IFrom>(); }
        }
    }

    public class TestEntitySelectProcessor : ISelectProvider<From>, ISelectProvider<IFrom>
    {
        public readonly static TestEntitySelectProcessor Proc = new TestEntitySelectProcessor();

        public int Process(From from)
        {
            return from != null ? from.Id * 3 : 789;
        }

        public int ProcessMany(IEnumerable<From> from)
        {
            return from != null ? from.Aggregate(0, (current, f) => current + f.Id * 2) : 456;
        }

        public IReadOnlySelectExpression<From> Select
        {
            get
            {
                return new SelectExpression<From>
                {
                    Select = f => new From { Id = f.Id },
                    ExternalIncludes = new List<Expression<Func<From, object>>>(),
                };
            }
        }

        IReadOnlySelectExpression<IFrom> ISelectProvider<IFrom>.Select
        {
            get
            {
                return new SelectExpression<IFrom>
                {
                    Select = f => new From { Id = f.Id, Name = f.Name },
                    ExternalIncludes = new List<Expression<Func<IFrom, object>>>(),
                };
            }
        }
    }

    public class TestEntityInterfaceSelectProcessor : ISelectProvider<IFrom>
    {
        public readonly static TestEntityInterfaceSelectProcessor Proc = new TestEntityInterfaceSelectProcessor();

        public int Process(IFrom from)
        {
            return from != null ? from.Id * 31 : 37;
        }

        public IReadOnlySelectExpression<IFrom> Select
        {
            get
            {
                return new SelectExpression<IFrom>
                {
                    Select = f => new { f.Id },
                };
            }
        }
    }

}
