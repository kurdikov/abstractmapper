namespace BaseTestClasses
{
    using System.Reflection;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    public static class MemberAssert
    {
        public static void AreEqual(MemberInfo expected, MemberInfo actual)
        {
            if (!MemberInfoComparer.Instance.Equals(expected, actual))
            {
                throw new AssertFailedException(string.Format("MemberAssert.AreEqual failed. Expected: {0}. Actual: {1}.", expected, actual));
            }
        }

        public static void AreEqual(ValueHoldingMember expected, ValueHoldingMember actual)
        {
            if (!MemberInfoComparer.Instance.Equals(expected, actual))
            {
                throw new AssertFailedException(string.Format("MemberAssert.AreEqual failed. Expected: {0}. Actual: {1}.", expected, actual));
            }
        }

        public static void AreEqual(MemberInfo expected, ValueHoldingMember actual)
        {
            if (ReferenceEquals(actual, null) && expected == null || !ReferenceEquals(actual, null) && !MemberInfoComparer.Instance.Equals(expected, actual.Member))
            {
                throw new AssertFailedException(string.Format("MemberAssert.AreEqual failed. Expected: {0}. Actual: {1}.", expected, actual));
            }
        }

        public static void IsWhere(MethodInfo actual)
        {
            if (!StaticHelpers.Library.IsEnumerableWhere(actual))
            {
                throw new AssertFailedException(string.Format("MemberAssert.IsWhere failed. Actual: {0}.", actual));
            }
        }
    }
}
