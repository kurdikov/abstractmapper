﻿namespace TsSoft.NinjectBindings
{
    using System;
    using JetBrains.Annotations;
    using Ninject;

    /// <summary>
    /// Расширения ядра Ninject
    /// </summary>
    public static class KernelExtensions
    {
        /// <summary>
        /// Создать связыватель интерфейсов со стандартными реализациями для ядра
        /// </summary>
        /// <param name="kernel">Ядро</param>
        /// <returns>Связыватель</returns>
        public static NinjectBinder<IKernel> CreateBinder([NotNull]this IKernel kernel)
        {
            if (kernel == null)
            {
                throw new ArgumentNullException("kernel");
            }
            return new NinjectBinder<IKernel>(kernel);
        }
    }
}
