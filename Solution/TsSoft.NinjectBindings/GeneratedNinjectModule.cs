namespace TsSoft.NinjectBindings
{
    using Ninject.Modules;
    using TsSoft.BindingsDescription;

    /// <summary>
    /// Ninject-������, ������������ �� �������� ����������, ����������� ������ ��� ���������
    /// </summary>
    public class GeneratedNinjectModule<T> : NinjectModule
        where T: IBindingsDescription
    {
        /// <summary>
        /// ��������� ������ ��� ���������
        /// </summary>
        public override void Load()
        {
            NinjectBinder.Create(this).Add<T>().InSingletonScope();
        }
    }
}
