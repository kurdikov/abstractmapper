﻿namespace TsSoft.NinjectBindings
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using JetBrains.Annotations;
    using Ninject;
    using Ninject.Syntax;
    using TsSoft.BindingsDescription;

    /// <summary>
    /// Добавляет в ядро связи интерфейсов с реализациями
    /// </summary>
    public class NinjectBinder<T> : BinderBase
        where T: IBindingRoot
    {
        [NotNull]
        private readonly T _root;

        /// <summary>
        /// Добавляет в ядро связи интерфейсов с реализациями
        /// </summary>
        internal NinjectBinder([NotNull]T root, IBindingsRetriever retriever = null, IAssemblyRetriever assemblyRetriever = null)
            : base(retriever, assemblyRetriever)
        {
            _root = root;
        }

        /// <summary>
        /// Применить связывание
        /// </summary>
        /// <returns>Ядро</returns>
        public T InSingletonScope()
        {
            // ReSharper disable  PossibleNullReferenceException
            InScope(binding => binding.InSingletonScope());
            // ReSharper restore PossibleNullReferenceException
            return _root;
        }

        /// <summary>
        /// Связать интерфейсы с реализациями
        /// </summary>
        /// <param name="scopeDescription">Описание выбора scope</param>
        public T InScope([NotNull]BindInScope scopeDescription)
        {
            if (scopeDescription == null)
            {
                throw new ArgumentNullException("scopeDescription");
            }
            // ReSharper disable  PossibleNullReferenceException
            // ReSharper disable AssignNullToNotNullAttribute
            Bind(binding => scopeDescription(_root
                .Bind(binding.Key)
                .To(binding.Value)));
            // ReSharper restore PossibleNullReferenceException
            // ReSharper restore AssignNullToNotNullAttribute
            return _root;
        }

        /// <summary>
        /// Связать интерфейсы с реализациями
        /// </summary>
        /// <param name="scopeDescription">Описание выбора scope</param>
        public T InScope([NotNull]BindInScopeDiscriminating scopeDescription)
        {
            if (scopeDescription == null)
            {
                throw new ArgumentNullException("scopeDescription");
            }
            // ReSharper disable  PossibleNullReferenceException
            // ReSharper disable AssignNullToNotNullAttribute
            Bind(binding => scopeDescription(binding.Key, binding.Value, _root
                .Bind(binding.Key)
                .To(binding.Value)));
            // ReSharper restore PossibleNullReferenceException
            // ReSharper restore AssignNullToNotNullAttribute
            return _root;
        }
    }

    /// <summary>
    /// Добавляет в Ninject-ядро связи интерфейсов с их стандартными реализациями из указанных сборок
    /// </summary>
    public static class NinjectBinder
    {
        /// <summary>
        /// Создать ninject-связыватель
        /// </summary>
        /// <typeparam name="T">Тип корня связывания</typeparam>
        /// <param name="root">Корень связывания</param>
        /// <param name="bindingsRetriever">Получатель связей из сборки</param>
        /// <param name="assemblyRetriever">Получатель сборок-зависимостей</param>
        [NotNull]
        public static NinjectBinder<T> Create<T>([NotNull]T root, IBindingsRetriever bindingsRetriever = null, IAssemblyRetriever assemblyRetriever = null) where T : IBindingRoot
        {
            if (root == null) throw new ArgumentNullException("root");
            return new NinjectBinder<T>(root, bindingsRetriever, assemblyRetriever);
        }

        /// <summary>
        /// Добавить в Ninject-ядро связи интерфейсов с их стандартными реализациями
        /// </summary>
        /// <param name="kernel">Ядро</param>
        /// <param name="usedLibraryTypes">Непосредственно используемые библиотечные типы</param>
        /// <param name="bindingsRetriever">Получатель связей из сборки</param>
        /// <param name="assemblyRetriever">Получатель сборок-зависимостей</param>
        /// <param name="shouldLoad">Предикат необходимости загрузки сборки</param>
        public static void BindAllInSingletonScope(
            [NotNull]IKernel kernel, 
            IEnumerable<Type> usedLibraryTypes, 
            IBindingsRetriever bindingsRetriever = null,
            IAssemblyRetriever assemblyRetriever = null,
            Func<AssemblyName, bool> shouldLoad = null)
        {
            if (kernel == null) throw new ArgumentNullException("kernel");
            var binder = new NinjectBinder<IKernel>(kernel, bindingsRetriever, assemblyRetriever);
            binder.AddByUsedTypes(usedLibraryTypes, shouldLoad);
            binder.InSingletonScope();
        }
        
        /// <summary>
        /// Занести связки в ядро как синглтоны
        /// </summary>
        public static IKernel InSingletonScope(this BinderBase binder)
        {
            return ToNinjectKernelBinder(binder).InSingletonScope();
        }

        /// <summary>
        /// Получить ninject-связыватель
        /// </summary>
        [NotNull]
        public static NinjectBinder<T> ToNinjectBinder<T>(this BinderBase binder)
            where T: IBindingRoot
        {
            var result = binder as NinjectBinder<T>;
            if (result == null)
            {
                throw new ArgumentException(string.Format("{0} is not a {1}", binder, typeof(NinjectBinder<T>)));
            }
            return result;
        }

        /// <summary>
        /// Получить ninject-связыватель
        /// </summary>
        [NotNull]
        public static NinjectBinder<IKernel> ToNinjectKernelBinder(this BinderBase binder)
        {
            return ToNinjectBinder<IKernel>(binder);
        }
    }

    /// <summary>
    /// Функция выбора ninject scope
    /// </summary>
    public delegate IBindingNamedWithOrOnSyntax<object> BindInScope([NotNull]IBindingWhenInNamedWithOrOnSyntax<object> ninjectBinding);
    /// <summary>
    /// Функция выбора ninject scope
    /// </summary>
    public delegate IBindingNamedWithOrOnSyntax<object> BindInScopeDiscriminating([NotNull]Type inter, [NotNull]Type impl, [NotNull]IBindingWhenInNamedWithOrOnSyntax<object> ninjectBinding);
}
