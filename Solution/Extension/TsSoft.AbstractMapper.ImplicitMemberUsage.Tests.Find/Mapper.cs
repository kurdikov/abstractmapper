﻿using TsSoft.AbstractMapper;

namespace Tests
{
    class Mapper : Mapper<From, To>
    {
        public Mapper(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }
    }
}
