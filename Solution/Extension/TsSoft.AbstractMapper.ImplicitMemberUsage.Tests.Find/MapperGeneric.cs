﻿using TsSoft.AbstractMapper;

namespace Tests
{
    class MapperGeneric<T> : Mapper<T, To>
        where T : From
    {
        public MapperGeneric(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }
    }
}
