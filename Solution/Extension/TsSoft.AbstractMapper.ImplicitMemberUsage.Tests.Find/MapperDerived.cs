﻿using TsSoft.AbstractMapper;

namespace Tests
{
    class MapperDerived : Mapper<FromDerived, To>
    {
        public MapperDerived(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }
    }
}
