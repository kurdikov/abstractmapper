﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using EnvDTE80;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.ComponentModelHost;
using Microsoft.VisualStudio.LanguageServices;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage.Vsix
{
    [PackageRegistration(UseManagedResourcesOnly = true)]
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)] // Info on this package for Help/About
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [Guid(PackageGuidString)]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
    public sealed class ImplicitMemberUsagePackage : Package
    {
        public const string PackageGuidString = "f0ce9a6e-6953-49ac-994e-4d6541e0a06e";

        public static VisualStudioWorkspace Workspace { get; private set; }

        public static IVsOutputWindow OutputWindow { get; private set; }

        public static DTE2 Ide => (DTE2)Instance.GetService(typeof(SDTE));

        public static Guid ImplicitMemberUsageOutput = Guid.Parse("6AAAC449-37B7-43D0-97C5-92495AA6B3DE");

        public static IImplicitMemberUsageFinder ImplicitMemberUsageFinder { get; private set; }

        private static ImplicitMemberUsagePackage Instance;

        public ImplicitMemberUsagePackage()
        {
            Instance = this;
        }

        protected override void Initialize()
        {
            var componentModel = (IComponentModel)GetGlobalService(typeof(SComponentModel));
            Workspace = componentModel.GetService<VisualStudioWorkspace>();
            OutputWindow = (IVsOutputWindow)GetGlobalService(typeof(SVsOutputWindow));
            ImplicitMemberUsageFinder = new ImplicitMemberUsageFinder(
                new MapperFinder(),
                new MemberRetriever(),
                new MapperAnalyzer(),
                new NodeFinder());

            FindImplicitUsagesCommand.Initialize(this);
            base.Initialize();
        }

        internal static IVsOutputWindowPane GetUsageOutputPane()
        {
            var outputWindowGuid = ImplicitMemberUsageOutput;
            IVsOutputWindowPane pane;
            OutputWindow.GetPane(ref outputWindowGuid, out pane);
            if (pane == null)
            {
                OutputWindow.CreatePane(ref outputWindowGuid, "Find implicit usages", 1, 0);
                OutputWindow.GetPane(ref outputWindowGuid, out pane);
            }
            return pane;
        }

        internal static IServiceProvider GetServiceProvider()
        {
            return (IServiceProvider)Ide;
        }
    }
}
