﻿using System;
using System.ComponentModel.Design;
using System.Globalization;
using System.IO;
using System.Linq;
using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage.Vsix
{
    internal sealed class FindImplicitUsagesCommand
    {
        public const int CommandId = 0x0100;

        public static readonly Guid CommandSet = new Guid("66326bba-7ed2-4dc1-b43f-dbc3b56b72e0");

        private readonly ImplicitMemberUsagePackage _package;

        private FindImplicitUsagesCommand(ImplicitMemberUsagePackage package)
        {
            if (package == null)
            {
                throw new ArgumentNullException("package");
            }

            _package = package;

            OleMenuCommandService commandService = this.ServiceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (commandService != null)
            {
                var menuCommandID = new CommandID(CommandSet, CommandId);
                var menuItem = new MenuCommand(this.MenuItemCallback, menuCommandID);
                commandService.AddCommand(menuItem);
            }
        }

        public static FindImplicitUsagesCommand Instance { get; private set; }

        private IServiceProvider ServiceProvider => _package;

        public static void Initialize(ImplicitMemberUsagePackage package)
        {
            Instance = new FindImplicitUsagesCommand(package);
        }

        private CodePosition GetCursorPosition()
        {
            var activeDocument = ImplicitMemberUsagePackage.Ide.ActiveDocument;
            var project = activeDocument.ProjectItem.ContainingProject;
            var projectPath = project.FileName;
            var documentPath = Path.Combine(activeDocument.Path, activeDocument.Name);
            var selection = (TextSelection)activeDocument.Selection;
            var selectionPoint = selection.ActivePoint;
            var line = selectionPoint.Line - 1; // roslyn is 0-based, this shit is not
            var column = selectionPoint.DisplayColumn - 1;
            return new CodePosition(projectPath, documentPath, line, column);
        }

        private async System.Threading.Tasks.Task Run()
        {
            var position = GetCursorPosition();
            var result = await ImplicitMemberUsagePackage.ImplicitMemberUsageFinder.GetMappersWithImplicitUsage(
                ImplicitMemberUsagePackage.Workspace,
                position);
            if (result == null)
            {
                return;
            }

            var pane = ImplicitMemberUsagePackage.GetUsageOutputPane();
            pane.Activate();
            if (result.Type == null)
            {
                pane.OutputString($"\nUnable to perform search: not found the type containing {result.Member}\n");
                return;
            }
            pane.OutputString($"\nFind implicit usages of '{result.Member}' from type {result.Type.Name}:\n");
            foreach (var mapper in result.Mappers)
            {
                var mapperDecl = mapper.Mapper.DeclaringSyntaxReferences.FirstOrDefault();
                if (mapperDecl != null)
                {
                    var mapperLine = mapperDecl.SyntaxTree.GetLineSpan(mapperDecl.Span);
                    pane.OutputString($"{mapperDecl.SyntaxTree.FilePath}({mapperLine.StartLinePosition.Line + 1}): {mapper.Mapper.Name}\n");
                }
                else
                {
                    pane.OutputString($"(unknown): {mapper.Mapper.Name}\n");
                }
            }
            pane.OutputString($"{result.Mappers.Count} implicit usages found\n");
        }

        private async void MenuItemCallback(object sender, EventArgs e)
        {
            try
            {
                await Run();
            }
            catch (Exception ex)
            {
                VsShellUtilities.ShowMessageBox(
                    ServiceProvider,
                    ex.Message,
                    "Error",
                    OLEMSGICON.OLEMSGICON_CRITICAL,
                    OLEMSGBUTTON.OLEMSGBUTTON_OK,
                    OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
            }
        }
    }
}
