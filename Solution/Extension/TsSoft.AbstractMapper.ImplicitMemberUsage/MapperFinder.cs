﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    /// <summary>
    /// Находит абстрактные мапперы по from-типу
    /// </summary>
    public class MapperFinder : IMapperFinder
    {
        public IReadOnlyCollection<MapperDescription> GetMappersFrom(
            Compilation compilation,
            ITypeSymbol fromType)
        {
            var mapperType = compilation.GetTypeByMetadataName("TsSoft.AbstractMapper.Mapper`2")?.ConstructUnboundGenericType();
            var result = new List<MapperDescription>();
            if (mapperType == null)
            {
                return result;
            }
            foreach (var tree in compilation.SyntaxTrees)
            {
                var declarations = tree.GetRoot().DescendantNodes().OfType<ClassDeclarationSyntax>();
                var semantic = compilation.GetSemanticModel(tree);
                foreach (var node in declarations)
                {
                    var type = semantic.GetDeclaredSymbol(node);
                    var mapperDescription = GetMapperDescription(type, mapperType);
                    if (mapperDescription != null && IsFirstSubclassOfSecond(mapperDescription.From, fromType))
                    {
                        result.Add(mapperDescription);
                    }
                }
            }
            return result;
        }

        private bool IsFirstSubclassOfSecond(ITypeSymbol first, ITypeSymbol second)
        {
            if (first.TypeKind == TypeKind.TypeParameter)
            {
                var typeParam = (ITypeParameterSymbol)first;
                var constraints = typeParam.ConstraintTypes;
                foreach (var constraint in constraints)
                {
                    if (IsFirstSubclassOfSecond(constraint, second))
                    {
                        return true;
                    }
                }
            }
            else
            {
                var current = first;
                while (current != null)
                {
                    if (current.Equals(second))
                    {
                        return true;
                    }
                    current = current.BaseType;
                }
            }
            return false;
        }

        private MapperDescription GetMapperDescription(INamedTypeSymbol type, ITypeSymbol mapperType)
        {
            var current = type;
            while (current != null)
            {
                if (current.IsGenericType && current.ConstructUnboundGenericType().Equals(mapperType))
                {
                    return new MapperDescription(type, current.TypeArguments[0], current.TypeArguments[1]);
                }
                current = current.BaseType;
            }
            return null;
        }
    }
}
