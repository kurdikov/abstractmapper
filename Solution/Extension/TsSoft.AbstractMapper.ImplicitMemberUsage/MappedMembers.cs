﻿using System;
using System.Collections.Generic;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    /// <summary>
    /// Разобранное содержимое правил маппера
    /// </summary>
    public class MappedMembers
    {
        public IReadOnlyCollection<string> ExplicitlyMapped { get; }
        public IReadOnlyCollection<string> Ignored { get; }

        public MappedMembers(
            IReadOnlyCollection<string> explicitlyMapped,
            IReadOnlyCollection<string> ignored)
        {
            if (explicitlyMapped == null) throw new ArgumentNullException(nameof(explicitlyMapped));
            if (ignored == null) throw new ArgumentNullException(nameof(ignored));
            ExplicitlyMapped = explicitlyMapped;
            Ignored = ignored;
        }
    }
}
