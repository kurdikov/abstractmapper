﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    public class MemberRetriever : IMemberRetriever
    {
        public IEnumerable<IPropertySymbol> GetDeclaredProperties(ITypeSymbol type)
        {
            return type.GetMembers().Where(p => p.Kind == SymbolKind.Property)
                .Cast<IPropertySymbol>();
        }

        public IReadOnlyCollection<IPropertySymbol> GetProperties(ITypeSymbol type)
        {
            var result = GetDeclaredProperties(type).ToList();
            while (type.BaseType != null)
            {
                type = type.BaseType;
                result.AddRange(GetDeclaredProperties(type));
            }
            if (IsInterface(type))
            {
                foreach (var inter in type.Interfaces)
                {
                    result.AddRange(GetDeclaredProperties(inter));
                }
            }
            return result;
        }

        private bool IsInterface(ITypeSymbol type)
        {
            return type.TypeKind == TypeKind.Interface;
        }

        public IEnumerable<IFieldSymbol> GetDeclaredFields(ITypeSymbol type)
        {
            return type.GetMembers().Where(p => p.Kind == SymbolKind.Field)
                .Cast<IFieldSymbol>();
        }

        public IReadOnlyCollection<IFieldSymbol> GetFields(ITypeSymbol type)
        {
            var result = GetDeclaredFields(type).ToList();
            while (type.BaseType != null)
            {
                type = type.BaseType;
                result.AddRange(GetDeclaredFields(type));
            }
            return result;
        }
    }
}
