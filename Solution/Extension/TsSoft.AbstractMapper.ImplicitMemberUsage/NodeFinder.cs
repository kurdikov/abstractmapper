﻿using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    public class NodeFinder : INodeFinder
    {
        public MemberDeclarationSyntax Find(SyntaxTree tree, int line, int column)
        {
            if (tree == null)
            {
                return null;
            }
            var root = tree.GetRoot();
            if (root == null)
            {
                return null;
            }
            var text = tree.GetText();
            if (text == null)
            {
                return null;
            }
            if (line >= text.Lines.Count || line < 0)
            {
                return null;
            }
            var lineSpan = tree.GetText().Lines[line].Span;
            if (column < 0 || column >= lineSpan.Length)
            {
                return null;
            }
            var symbolSpan = TextSpan.FromBounds(lineSpan.Start + column, lineSpan.Start + column + 1);
            return (MemberDeclarationSyntax)root
                .DescendantNodes(symbolSpan)
                .FirstOrDefault(n => n.Kind() == SyntaxKind.PropertyDeclaration || n.Kind() == SyntaxKind.FieldDeclaration);
        }
    }
}
