﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    public interface IImplicitMemberUsageFinder
    {
        IReadOnlyCollection<MapperDescription> GetMappersWithImplicitUsage(
            Compilation compilation,
            ITypeSymbol fromType,
            string propertyName);

        Task<Usages> GetMappersWithImplicitUsage(
            Workspace workspace,
            CodePosition codePosition);
    }
}
