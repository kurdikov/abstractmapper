﻿using Microsoft.CodeAnalysis;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    public interface IMapperAnalyzer
    {
        MappedMembers GetMappedMembers(
            Compilation compilation,
            ITypeSymbol mapper);
    }
}
