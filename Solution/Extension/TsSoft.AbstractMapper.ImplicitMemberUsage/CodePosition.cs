﻿using System;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    public class CodePosition
    {
        public string ProjectFilePath { get; }
        public string FilePath { get; }
        /// <summary>
        /// 0-based
        /// </summary>
        public int Line { get; }
        /// <summary>
        /// 0-based
        /// </summary>
        public int Column { get; }

        public CodePosition(
            string projectFilePath,
            string filePath,
            int line,
            int column)
        {
            if (projectFilePath == null) throw new ArgumentNullException(nameof(projectFilePath));
            if (filePath == null) throw new ArgumentNullException(nameof(filePath));
            ProjectFilePath = projectFilePath;
            FilePath = filePath;
            Line = line;
            Column = column;
        }

        public override string ToString()
        {
            return $"{System.IO.Path.GetFileName(FilePath)}({Line}, {Column})";
        }
    }
}
