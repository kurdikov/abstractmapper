﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    public interface IMemberRetriever
    {
        IEnumerable<IPropertySymbol> GetDeclaredProperties(ITypeSymbol type);

        IReadOnlyCollection<IPropertySymbol> GetProperties(ITypeSymbol type);
    }
}
