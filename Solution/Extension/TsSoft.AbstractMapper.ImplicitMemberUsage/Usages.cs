﻿using System;
using System.Collections.Generic;
using Microsoft.CodeAnalysis;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    public class Usages
    {
        public INamedTypeSymbol Type { get; }
        public string Member { get; }
        public IReadOnlyCollection<MapperDescription> Mappers { get; }

        public Usages(
            INamedTypeSymbol type,
            string member,
            IReadOnlyCollection<MapperDescription> mappers)
        {
            if (mappers == null) throw new ArgumentNullException(nameof(mappers));
            Type = type;
            Member = member;
            Mappers = mappers;
        }
    }
}
