﻿using Microsoft.CodeAnalysis;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    /// <summary>
    /// Описание абстрактного маппера
    /// </summary>
    public class MapperDescription
    {
        public INamedTypeSymbol Mapper { get; }
        public ITypeSymbol From { get; }
        public ITypeSymbol To { get; }

        public MapperDescription(
            INamedTypeSymbol mapper,
            ITypeSymbol from,
            ITypeSymbol to)
        {
            Mapper = mapper;
            From = from;
            To = to;
        }
    }
}
