﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.FindSymbols;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    public class ImplicitMemberUsageFinder : IImplicitMemberUsageFinder
    {
        private readonly IMapperFinder _mapperFinder;
        private readonly IMemberRetriever _memberRetriever;
        private readonly IMapperAnalyzer _mapperAnalyzer;
        private readonly INodeFinder _nodeFinder;

        public ImplicitMemberUsageFinder(
            IMapperFinder mapperFinder,
            IMemberRetriever memberRetriever,
            IMapperAnalyzer mapperAnalyzer,
            INodeFinder nodeFinder)
        {
            if (mapperFinder == null) throw new ArgumentNullException(nameof(mapperFinder));
            if (memberRetriever == null) throw new ArgumentNullException(nameof(memberRetriever));
            if (mapperAnalyzer == null) throw new ArgumentNullException(nameof(mapperAnalyzer));
            if (nodeFinder == null) throw new ArgumentNullException(nameof(nodeFinder));
            _mapperFinder = mapperFinder;
            _memberRetriever = memberRetriever;
            _mapperAnalyzer = mapperAnalyzer;
            _nodeFinder = nodeFinder;
        }

        public IReadOnlyCollection<MapperDescription> GetMappersWithImplicitUsage(
            Compilation compilation,
            ITypeSymbol fromType,
            string propertyName)
        {
            var result = new List<MapperDescription>();
            var mappers = _mapperFinder.GetMappersFrom(compilation, fromType);
            foreach (var mapper in mappers)
            {
                var toProps = _memberRetriever.GetProperties(mapper.To);
                // TODO fields
                // TODO To may be a generic parameter; 
                // in this case we'll need to look at the constraints and find all classes satisfying them
                if (toProps.Any(p => p.Name == propertyName && !p.IsReadOnly))
                {
                    var mapperProps = _mapperAnalyzer.GetMappedMembers(compilation, mapper.Mapper);
                    if (!mapperProps.ExplicitlyMapped.Contains(propertyName)
                        && !mapperProps.Ignored.Contains(propertyName))
                    {
                        result.Add(mapper);
                    }
                }
            }
            return result;
        }

        private readonly MapperDescription[] NoMappers = new MapperDescription[0];

        public async Task<Usages> GetMappersWithImplicitUsage(
            Workspace workspace,
            CodePosition codePosition)
        {
            if (workspace == null) throw new ArgumentNullException(nameof(workspace));
            if (codePosition == null) throw new ArgumentNullException(nameof(codePosition));
            var solution = workspace.CurrentSolution;
            var projectWithFrom = solution.Projects.SingleOrDefault(
                p => p.FilePath.Equals(codePosition.ProjectFilePath, StringComparison.OrdinalIgnoreCase));
            if (projectWithFrom == null)
            {
                return null;
            }
            var fromFile = projectWithFrom.Documents.SingleOrDefault(
                d => d.FilePath.Equals(codePosition.FilePath, StringComparison.OrdinalIgnoreCase));
            if (fromFile == null)
            {
                return null;
            }
            var compilation = await projectWithFrom.GetCompilationAsync();
            var tree = await fromFile.GetSyntaxTreeAsync();
            var model = compilation.GetSemanticModel(tree);
            var node = _nodeFinder.Find(tree, codePosition.Line, codePosition.Column) as PropertyDeclarationSyntax;
            var memberName = node.Identifier.ValueText;
            if (node == null)
            {
                return null;
            }
            var typeNode = node.Ancestors().OfType<TypeDeclarationSyntax>().FirstOrDefault();
            if (typeNode == null)
            {
                return new Usages(null, memberName, NoMappers);
            }
            var typeSymbol = model.GetDeclaredSymbol(typeNode) as INamedTypeSymbol;
            if (typeSymbol == null)
            {
                return new Usages(null, memberName, NoMappers);
            }
            var mappers = new List<MapperDescription>();
            foreach (var project in solution.Projects)
            {
                if (project == projectWithFrom)
                {
                    var found = GetMappersWithImplicitUsage(compilation, typeSymbol, memberName);
                    mappers.AddRange(found);
                }
                else if (project.AllProjectReferences.Any(pr => pr.ProjectId == projectWithFrom.Id))
                {
                    var otherCompilation = await project.GetCompilationAsync();
                    var otherTypeSymbol = SymbolFinder.FindSimilarSymbols(typeSymbol, otherCompilation).FirstOrDefault();
                    var found = GetMappersWithImplicitUsage(otherCompilation, otherTypeSymbol, memberName);
                    mappers.AddRange(found);
                }
            }
            return new Usages(typeSymbol, memberName, mappers);
        }
    }
}
