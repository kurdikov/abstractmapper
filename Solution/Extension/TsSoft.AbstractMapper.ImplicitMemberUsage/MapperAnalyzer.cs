﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    public class MapperAnalyzer : IMapperAnalyzer
    {
        private class ClassAnalyzer
        {
            public Compilation Compilation { get; }
            public ITypeSymbol MapperType { get; }

            public ClassAnalyzer(
                Compilation compilation,
                ITypeSymbol mapperType)
            {
                if (compilation == null) throw new ArgumentNullException(nameof(compilation));
                if (mapperType == null) throw new ArgumentNullException(nameof(mapperType));
                Compilation = compilation;
                MapperType = mapperType;
            }

            public IReadOnlyCollection<string> GetExplicitlyMappedMembers()
            {
                var node = GetVirtualPropertyNode(MapperType, "MapRules");
                return Analyze(node);
            }

            public IReadOnlyCollection<string> GetIgnoredMembers()
            {
                var node = GetVirtualPropertyNode(MapperType, "IgnoreRules");
                return Analyze(node);
            }

            private IReadOnlyCollection<string> Analyze(PropertyDeclarationSyntax node)
            {
                if (node == null)
                {
                    return NoMembers;
                }
                var analyzer = new PropertyAnalyzer(this, node);

                var result = new MemberCollectionBuilder();
                analyzer.AnalyzeProperty(result);
                return result.Build();
            }

            private PropertyDeclarationSyntax GetVirtualPropertyNode(ITypeSymbol @class, string propertyName)
            {
                var current = @class;
                while (current != null && current.ContainingAssembly.Equals(@class.ContainingAssembly)) // TODO
                {
                    var prop = GetOwnPropertyNode(current, propertyName);
                    if (prop != null)
                    {
                        return prop;
                    }
                    current = current.BaseType;
                }
                return null;
            }

            private PropertyDeclarationSyntax GetOwnPropertyNode(ITypeSymbol @class, string propertyName)
            {
                var propSymbol = (IPropertySymbol)@class
                    .GetMembers()
                    .FirstOrDefault(m => m.Kind == SymbolKind.Property && m.Name == propertyName);
                if (propSymbol == null || propSymbol.DeclaringSyntaxReferences.Length <= 0)
                {
                    return null;
                }
                if (propSymbol.DeclaringSyntaxReferences.Length > 1)
                {
                    return null;    // TODO possible?
                }
                var syntax = propSymbol?.DeclaringSyntaxReferences[0].GetSyntax();
                return (PropertyDeclarationSyntax)syntax;
            }

        }

        private class PropertyAnalyzer
        {
            public ClassAnalyzer ClassAnalyzer { get; }
            public PropertyDeclarationSyntax PropertyNode { get; }
            public SemanticModel SemanticModel { get; }

            public PropertyAnalyzer(
                ClassAnalyzer classAnalyzer,
                PropertyDeclarationSyntax propertyNode)
            {
                if (classAnalyzer == null) throw new ArgumentNullException(nameof(classAnalyzer));
                if (propertyNode == null) throw new ArgumentNullException(nameof(propertyNode));
                ClassAnalyzer = classAnalyzer;
                PropertyNode = propertyNode;
                SemanticModel = ClassAnalyzer.Compilation.GetSemanticModel(PropertyNode.SyntaxTree);
            }

            public void AnalyzeProperty(MemberCollectionBuilder builder)
            {
                if (PropertyNode.ExpressionBody != null)
                {
                    AnalyzeValue(PropertyNode.ExpressionBody.Expression, builder);
                }
                else
                {
                    var returnStatement = PropertyNode.DescendantNodes().OfType<ReturnStatementSyntax>().ToList();
                    if (returnStatement.Count == 1)
                    {
                        AnalyzeValue(returnStatement[0].Expression, builder);
                    }
                    else
                    {
                        builder.AddNote($"Multiple return statements in property {PropertyNode}");
                    }
                }
            }

            private void AnalyzeValue(
                ExpressionSyntax returnedValue,
                MemberCollectionBuilder builder)
            {
                switch (returnedValue.Kind())
                {
                    case SyntaxKind.ObjectCreationExpression:
                        var value = (ObjectCreationExpressionSyntax)returnedValue;
                        if (value.Initializer != null)
                        {
                            var initializers = value.Initializer.Expressions;
                            foreach (var initializer in initializers)
                            {
                                AnalyzeInitializer(initializer, builder);
                            }
                        }
                        break;
                    case SyntaxKind.IdentifierName:
                        AnalyzeIdentifier((IdentifierNameSyntax)returnedValue, builder);
                        break;
                    default:
                        builder.AddNote($"Unknown node type {returnedValue.Kind()} in returned value tree");
                        break;
                }
            }

            private void AnalyzeIdentifier(
                IdentifierNameSyntax returnedValue,
                MemberCollectionBuilder builder)
            {
                var symbol = SemanticModel.GetSymbolInfo(returnedValue).Symbol;
                if (symbol == null)
                {
                    builder.AddNote($"Could not resolve symbol {returnedValue}");
                }
                switch (symbol.Kind)
                {
                    case SymbolKind.Local:
                        var varDecl = symbol.DeclaringSyntaxReferences;
                        if (varDecl.Length != 1)
                        {
                            builder.AddNote($"Multiple declarations for variable {symbol} in rules");
                            break;
                        }
                        var varNode = varDecl[0].GetSyntax() as VariableDeclaratorSyntax;
                        if (varNode == null)
                        {
                            builder.AddNote($"Unknown node type {varDecl[0].GetSyntax().Kind()} for variable declaration of {symbol}; VariableDeclarator expected");
                            break;
                        }
                        var assignment = FindLastAssignment((ILocalSymbol)symbol);
                        if (assignment == null)
                        {
                            AnalyzeValue(varNode.Initializer.Value, builder);
                        }
                        else
                        {
                            AnalyzeValue(assignment.Right, builder);
                        }
                        break;
                    case SymbolKind.Property:
                        var propertySymbol = (IPropertySymbol)symbol;
                        var propDecl = symbol.DeclaringSyntaxReferences;
                        if (propDecl.Length != 1)
                        {
                            builder.AddNote($"Multiple declarations for property {symbol}");
                            break;
                        }
                        var propNode = propDecl[0].GetSyntax() as PropertyDeclarationSyntax;
                        if (!propertySymbol.ContainingType.Equals(ClassAnalyzer.MapperType))
                        {
                            // TODO this property belongs to another class, requiring new class analyzer
                            //var analyzer = new PropertyAnalyzer(

                            //    new ClassAnalyzer(propertySymbol.ContainingType),
                            //    propNode);
                            //analyzer.AnalyzeProperty(builder);
                            builder.AddNote($"Property {propertySymbol} assigned to rules of {ClassAnalyzer.MapperType} is outside of mapper type");
                            break;
                        }
                        var analyzer = new PropertyAnalyzer(ClassAnalyzer, propNode);
                        analyzer.AnalyzeProperty(builder);
                        break;
                    case SymbolKind.Field:
                        break;
                }
                // TODO
            }

            private AssignmentExpressionSyntax FindLastAssignment(ILocalSymbol symbol)
            {
                AssignmentExpressionSyntax lastAssignment = null;
                foreach (var assignment in PropertyNode.DescendantNodes().OfType<AssignmentExpressionSyntax>())
                {
                    var assignedSymbol = SemanticModel.GetSymbolInfo(assignment.Left);
                    if (assignedSymbol.Symbol == null || !assignedSymbol.Symbol.Equals(symbol))
                    {
                        continue;
                    }
                    lastAssignment = assignment;
                }
                return lastAssignment;
            }

            private void AnalyzeInitializer(
                ExpressionSyntax expr,
                MemberCollectionBuilder builder)
            {
                switch (expr.Kind())
                {
                    case SyntaxKind.ComplexElementInitializerExpression:
                        var initializer = (InitializerExpressionSyntax)expr;
                        var firstExpr = initializer.Expressions[0];
                        if (firstExpr.Kind() == SyntaxKind.SimpleLambdaExpression)
                        {
                            TryAddPropertyName((SimpleLambdaExpressionSyntax)firstExpr, builder);
                        }
                        else if (firstExpr.Kind() == SyntaxKind.ParenthesizedLambdaExpression)
                        {
                            TryAddPropertyName((ParenthesizedLambdaExpressionSyntax)firstExpr, builder);
                        }
                        else if (firstExpr.Kind() == SyntaxKind.SimpleMemberAccessExpression
                            && initializer.Expressions.Count == 1)
                        {
                            AnalyzeMember((MemberAccessExpressionSyntax)firstExpr, builder);
                        }
                        else
                        {
                            builder.AddNote($"Unknown node type {firstExpr.Kind()} in bracketed rule target {expr}");
                        }
                        break;
                    case SyntaxKind.SimpleLambdaExpression:
                        TryAddPropertyName((SimpleLambdaExpressionSyntax)expr, builder);
                        break;
                    case SyntaxKind.ParenthesizedLambdaExpression:
                        TryAddPropertyName((ParenthesizedLambdaExpressionSyntax)expr, builder);
                        break;
                    case SyntaxKind.SimpleMemberAccessExpression:
                        AnalyzeMember((MemberAccessExpressionSyntax)expr, builder);
                        break;
                    default:
                        builder.AddNote($"Unknown node type {expr.Kind()} in rule target {expr}");
                        break;
                }
            }

            private void TryAddPropertyName( SimpleLambdaExpressionSyntax toLambda, MemberCollectionBuilder builder)
            {
                var param = toLambda.Parameter;
                TryAddPropertyName(param, toLambda.Body, builder);
            }

            private void TryAddPropertyName(ParenthesizedLambdaExpressionSyntax toLambda, MemberCollectionBuilder builder)
            {
                if (toLambda.ParameterList.Parameters.Count != 1)
                {
                    builder.AddNote($"Lambda with multiple parameters in rule target {toLambda}");
                }
                else
                {
                    TryAddPropertyName(toLambda.ParameterList.Parameters[0], toLambda.Body, builder);
                }
            }

            private void TryAddPropertyName(ParameterSyntax param, CSharpSyntaxNode body, MemberCollectionBuilder builder)
            {
                if (body.Kind() != SyntaxKind.SimpleMemberAccessExpression)
                {
                    builder.AddNote($"Invalid lambda body type {body.Kind()} in rule target");
                    return;
                }
                var toAccess = (MemberAccessExpressionSyntax)body;
                var expression = toAccess.Expression;
                if (expression.Kind() != SyntaxKind.IdentifierName)
                {
                    builder.AddNote($"Invalid object in lambda body member access '{expression}' in rule target: invalid type {expression.Kind()}");
                    return;
                }
                var idSemantic = SemanticModel.GetSymbolInfo(expression);
                var paramSemantic = SemanticModel.GetDeclaredSymbol(param);
                if (!idSemantic.Symbol.Equals(paramSemantic))
                {
                    builder.AddNote($"Invalid object in lambda body member access '{expression}' in rule target: lambda parameter expected");
                    return;
                }
                builder.AddMember(toAccess.Name.Identifier.ValueText);
            }

            private void AnalyzeMember(
                MemberAccessExpressionSyntax memberAccess,
                MemberCollectionBuilder builder)
            {
                var obj = memberAccess.Expression;
                var name = memberAccess.Name;
                if (obj.Kind() == SyntaxKind.BaseExpression
                    && name.Identifier.ValueText == "MapRules")
                {
                    var analyzer = new ClassAnalyzer(ClassAnalyzer.Compilation, ClassAnalyzer.MapperType.BaseType);
                    builder.AddMembers(analyzer.GetExplicitlyMappedMembers());
                }
                else if (obj.Kind() == SyntaxKind.BaseExpression
                    && name.Identifier.ValueText == "IgnoreRules")
                {
                    var analyzer = new ClassAnalyzer(ClassAnalyzer.Compilation, ClassAnalyzer.MapperType.BaseType);
                    builder.AddMembers(analyzer.GetIgnoredMembers());
                }
            }
        }

        public MappedMembers GetMappedMembers(
            Compilation compilation,
            ITypeSymbol mapper)
        {
            var analyzer = new ClassAnalyzer(compilation, mapper);
            return new MappedMembers(
                analyzer.GetExplicitlyMappedMembers(),
                analyzer.GetIgnoredMembers());
        }

        private static IReadOnlyCollection<string> NoMembers = new string[0];

        private class MemberCollectionBuilder
        {
            private readonly HashSet<string> Members = new HashSet<string>();
            private readonly List<string> Notes = new List<string>();

            public void AddMember(string memberName)
            {
                Members.Add(memberName);
            }

            public void AddMembers(IEnumerable<string> memberNames)
            {
                Members.UnionWith(memberNames);
            }

            public void AddNote(string note)
            {
                Notes.Add(note);
            }

            public IReadOnlyCollection<string> Build()
            {
                return Members;
            }
        }
    }
}
