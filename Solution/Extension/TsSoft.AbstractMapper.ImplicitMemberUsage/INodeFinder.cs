﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    /// <summary>
    /// Находит свойство или поле по номеру строки (начинается с 0) и столбца
    /// </summary>
    public interface INodeFinder
    {
        MemberDeclarationSyntax Find(SyntaxTree tree, int line, int column);
    }
}
