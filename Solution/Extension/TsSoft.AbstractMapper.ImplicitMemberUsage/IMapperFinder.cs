﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    public interface IMapperFinder
    {
        IReadOnlyCollection<MapperDescription> GetMappersFrom(
            Compilation compilation,
            ITypeSymbol fromType);
    }
}
