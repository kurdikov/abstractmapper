﻿using TsSoft.AbstractMapper;
using TsSoft.AbstractMapper.Rules;

namespace Tests
{
    class From
    {
        public int First { get; set; }
        public int Second { get; set; }
        public int Third { get; set; }
        public int Fourth { get; set; }
    }

    class To
    {
        public int First { get; set; }
        public int Second { get; set; }
        public int Third { get; set; }
        public int Fourth { get; set; }
    }

    class MapperWithoutRules : Mapper<From, To>
    {
        public MapperWithoutRules(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }
    }

    class MapperWithEmptyRules : Mapper<From, To>
    {
        public MapperWithEmptyRules(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override IMapRules<To, From> MapRules
        {
            get
            {
                return new MapRules<To, From>();
            }
        }

        protected override IIgnoreRules<To> IgnoreRules
        {
            get
            {
                return new IgnoreRules<To>();
            }
        }
    }

    class MapperWithReturnNewRules : Mapper<From, To>
    {
        public MapperWithReturnNewRules(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override IMapRules<To, From> MapRules
        {
            get
            {
                return new MapRules<To, From>
                {
                    { t => t.First, f => f.First },
                    { t => t.Second, f => f.Third },
                };
            }
        }

        protected override IIgnoreRules<To> IgnoreRules
        {
            get
            {
                return new IgnoreRules<To>
                {
                    { t => t.Third },
                    t => t.Fourth,
                };
            }
        }
    }

    class MapperWithArrowRules : Mapper<From, To>
    {
        public MapperWithArrowRules(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override IMapRules<To, From> MapRules => new MapRules<To, From>
        {
            { t => t.First, f => f.First },
            { t => t.Second, f => f.Third },
        };

        protected override IIgnoreRules<To> IgnoreRules => new IgnoreRules<To>
        {
            { t => t.Third },
            t => t.Fourth,
        };
    }

    class BaseMapper : Mapper<From, To>
    {
        public BaseMapper(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override IMapRules<To, From> MapRules
        {
            get
            {
                return new MapRules<To, From>
                {
                    { t => t.First, f => f.First },
                };
            }
        }

        protected override IIgnoreRules<To> IgnoreRules
        {
            get
            {
                return new IgnoreRules<To>
                {
                    t => t.Third,
                };
            }
        }
    }

    class DerivedMapperWithoutOverriding : BaseMapper
    {
        public DerivedMapperWithoutOverriding(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }
    }

    class DerivedMapperUsingBase : BaseMapper
    {
        public DerivedMapperUsingBase(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override IMapRules<To, From> MapRules
        {
            get
            {
                return new MapRules<To, From>
                {
                    base.MapRules,
                    { t => t.Second, f => f.Second },
                };
            }
        }

        protected override IIgnoreRules<To> IgnoreRules
        {
            get
            {
                return new IgnoreRules<To>
                {
                    base.IgnoreRules,
                    t => t.Fourth,
                };
            }
        }
    }

    class DerivedMapperNotUsingBase : BaseMapper
    {
        public DerivedMapperNotUsingBase(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override IMapRules<To, From> MapRules
        {
            get
            {
                return new MapRules<To, From>
                {
                    { t => t.Second, f => f.Second },
                };
            }
        }

        protected override IIgnoreRules<To> IgnoreRules
        {
            get
            {
                return new IgnoreRules<To>
                {
                    t => t.Fourth,
                };
            }
        }
    }

    class DerivedMapperNotUsingBaseMap : BaseMapper
    {
        public DerivedMapperNotUsingBaseMap(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override IMapRules<To, From> MapRules
        {
            get
            {
                return new MapRules<To, From>
                {
                    { t => t.Second, f => f.Second },
                };
            }
        }

        protected override IIgnoreRules<To> IgnoreRules
        {
            get
            {
                return new IgnoreRules<To>
                {
                    base.IgnoreRules,
                    t => t.Fourth,
                };
            }
        }
    }

    class DerivedMapperNotUsingBaseIgnore : BaseMapper
    {
        public DerivedMapperNotUsingBaseIgnore(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override IMapRules<To, From> MapRules
        {
            get
            {
                return new MapRules<To, From>
                {
                    base.MapRules,
                    { t => t.Second, f => f.Second },
                };
            }
        }

        protected override IIgnoreRules<To> IgnoreRules
        {
            get
            {
                return new IgnoreRules<To>
                {
                    t => t.Fourth,
                };
            }
        }
    }

    class MapperWithLocalVariableAsRules : Mapper<From, To>
    {
        public MapperWithLocalVariableAsRules(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override IMapRules<To, From> MapRules
        {
            get
            {
                var result = new MapRules<To, From>
                {
                    { t => t.First, f => f.First },
                    { t => t.Second, f => f.Second },
                };
                return result;
            }
        }
    }

    class MapperWithAssignedLocalVariableAsRules : Mapper<From, To>
    {
        public MapperWithAssignedLocalVariableAsRules(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
        {
        }

        protected override IMapRules<To, From> MapRules
        {
            get
            {
                IMapRules<To, From> result;
                result = new MapRules<To, From>
                {
                    { t => t.First, f => f.First },
                    { t => t.Second, f => f.Second },
                };
                return result;
            }
        }
    }
}
