﻿using System.Diagnostics;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.MSBuild;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    [TestClass]
    public class MapperAnalyzerTests
    {
        private static Compilation Compilation;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            Debug.WriteLine($"current dir: {System.IO.Directory.GetCurrentDirectory()}");
            var workspace = MSBuildWorkspace.Create();
            var project = workspace
                .OpenProjectAsync(
                    @"..\..\..\TsSoft.AbstractMapper.ImplicitMemberUsage.Tests.Analyze\TsSoft.AbstractMapper.ImplicitMemberUsage.Tests.Analyze.csproj")
                .Result;
            Compilation = project.GetCompilationAsync().Result;
        }

        [TestMethod]
        public void MapperWithNoRules()
        {
            var analyzer = new MapperAnalyzer();
            var type = Compilation.GetTypeByMetadataName("Tests.MapperWithoutRules");
            var info = analyzer.GetMappedMembers(Compilation, type);
            Assert.AreEqual(0, info.ExplicitlyMapped.Count);
            Assert.AreEqual(0, info.Ignored.Count);
        }

        [TestMethod]
        public void MapperWithEmptyRules()
        {
            var analyzer = new MapperAnalyzer();
            var type = Compilation.GetTypeByMetadataName("Tests.MapperWithEmptyRules");
            var info = analyzer.GetMappedMembers(Compilation, type);
            Assert.AreEqual(0, info.ExplicitlyMapped.Count);
            Assert.AreEqual(0, info.Ignored.Count);
        }

        [TestMethod]
        public void MapperWithReturnNewRules()
        {
            var analyzer = new MapperAnalyzer();
            var type = Compilation.GetTypeByMetadataName("Tests.MapperWithReturnNewRules");
            var info = analyzer.GetMappedMembers(Compilation, type);
            Assert.AreEqual(2, info.ExplicitlyMapped.Count);
            Assert.IsTrue(info.ExplicitlyMapped.Contains("First"));
            Assert.IsTrue(info.ExplicitlyMapped.Contains("Second"));
            Assert.AreEqual(2, info.Ignored.Count);
            Assert.IsTrue(info.Ignored.Contains("Third"));
            Assert.IsTrue(info.Ignored.Contains("Fourth"));
        }

        [TestMethod]
        public void MapperWithArrowRules()
        {
            var analyzer = new MapperAnalyzer();
            var type = Compilation.GetTypeByMetadataName("Tests.MapperWithArrowRules");
            var info = analyzer.GetMappedMembers(Compilation, type);
            Assert.AreEqual(2, info.ExplicitlyMapped.Count);
            Assert.IsTrue(info.ExplicitlyMapped.Contains("First"));
            Assert.IsTrue(info.ExplicitlyMapped.Contains("Second"));
            Assert.AreEqual(2, info.Ignored.Count);
            Assert.IsTrue(info.Ignored.Contains("Third"));
            Assert.IsTrue(info.Ignored.Contains("Fourth"));
        }

        [TestMethod]
        public void DerivedMapperUsingBaseRules()
        {
            var analyzer = new MapperAnalyzer();
            var type = Compilation.GetTypeByMetadataName("Tests.DerivedMapperUsingBase");
            var info = analyzer.GetMappedMembers(Compilation, type);
            Assert.AreEqual(2, info.ExplicitlyMapped.Count);
            Assert.IsTrue(info.ExplicitlyMapped.Contains("First"));
            Assert.IsTrue(info.ExplicitlyMapped.Contains("Second"));
            Assert.AreEqual(2, info.Ignored.Count);
            Assert.IsTrue(info.Ignored.Contains("Third"));
            Assert.IsTrue(info.Ignored.Contains("Fourth"));
        }

        [TestMethod]
        public void DerivedMapperNotUsingBaseRules()
        {
            var analyzer = new MapperAnalyzer();
            var type = Compilation.GetTypeByMetadataName("Tests.DerivedMapperNotUsingBase");
            var info = analyzer.GetMappedMembers(Compilation, type);
            Assert.AreEqual(1, info.ExplicitlyMapped.Count);
            Assert.IsTrue(info.ExplicitlyMapped.Contains("Second"));
            Assert.AreEqual(1, info.Ignored.Count);
            Assert.IsTrue(info.Ignored.Contains("Fourth"));
        }

        [TestMethod]
        public void DerivedMapperNotUsingBaseMapRules()
        {
            var analyzer = new MapperAnalyzer();
            var type = Compilation.GetTypeByMetadataName("Tests.DerivedMapperNotUsingBaseMap");
            var info = analyzer.GetMappedMembers(Compilation, type);
            Assert.AreEqual(1, info.ExplicitlyMapped.Count);
            Assert.IsTrue(info.ExplicitlyMapped.Contains("Second"));
            Assert.AreEqual(2, info.Ignored.Count);
            Assert.IsTrue(info.Ignored.Contains("Third"));
            Assert.IsTrue(info.Ignored.Contains("Fourth"));
        }

        [TestMethod]
        public void DerivedMapperNotUsingBaseIgnoreRules()
        {
            var analyzer = new MapperAnalyzer();
            var type = Compilation.GetTypeByMetadataName("Tests.DerivedMapperNotUsingBaseIgnore");
            var info = analyzer.GetMappedMembers(Compilation, type);
            Assert.AreEqual(2, info.ExplicitlyMapped.Count);
            Assert.IsTrue(info.ExplicitlyMapped.Contains("First"));
            Assert.IsTrue(info.ExplicitlyMapped.Contains("Second"));
            Assert.AreEqual(1, info.Ignored.Count);
            Assert.IsTrue(info.Ignored.Contains("Fourth"));
        }

        [TestMethod]
        public void DerivedMapperWithoutOverriding()
        {
            var analyzer = new MapperAnalyzer();
            var type = Compilation.GetTypeByMetadataName("Tests.DerivedMapperWithoutOverriding");
            var info = analyzer.GetMappedMembers(Compilation, type);
            Assert.AreEqual(1, info.ExplicitlyMapped.Count);
            Assert.IsTrue(info.ExplicitlyMapped.Contains("First"));
            Assert.AreEqual(1, info.Ignored.Count);
            Assert.IsTrue(info.Ignored.Contains("Third"));
        }

        [TestMethod]
        public void MapperWithLocalVariableInRules()
        {
            var analyzer = new MapperAnalyzer();
            var type = Compilation.GetTypeByMetadataName("Tests.MapperWithLocalVariableAsRules");
            var info = analyzer.GetMappedMembers(Compilation, type);
            Assert.AreEqual(2, info.ExplicitlyMapped.Count);
            Assert.IsTrue(info.ExplicitlyMapped.Contains("First"));
            Assert.IsTrue(info.ExplicitlyMapped.Contains("Second"));
            Assert.AreEqual(0, info.Ignored.Count);
        }

        [TestMethod]
        public void MapperWithLocalVariableAssignedInRules()
        {
            var analyzer = new MapperAnalyzer();
            var type = Compilation.GetTypeByMetadataName("Tests.MapperWithAssignedLocalVariableAsRules");
            var info = analyzer.GetMappedMembers(Compilation, type);
            Assert.AreEqual(2, info.ExplicitlyMapped.Count);
            Assert.IsTrue(info.ExplicitlyMapped.Contains("First"));
            Assert.IsTrue(info.ExplicitlyMapped.Contains("Second"));
            Assert.AreEqual(0, info.Ignored.Count);
        }
    }
}
