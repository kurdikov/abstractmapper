﻿using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    [TestClass]
    public class NodeFinderTests
    {
        [TestMethod]
        public void FindFirst()
        {
            var syntax = CSharpSyntaxTree.ParseText(@"
namespace Tests                            //1
{                                          //2
    public class None                      //3
    {                                      //4
        public int Property {get;set;}     //5
    }                                      //6
}                                          //7
");
            var finder = new NodeFinder();
            var result = (PropertyDeclarationSyntax)finder.Find(syntax, 5, 1);
            Assert.IsNotNull(result);
            Assert.AreEqual("Property", result.Identifier.ValueText);
        }

        [TestMethod]
        public void FindSecond()
        {
            var syntax = CSharpSyntaxTree.ParseText(@"
namespace Tests                             //1
{                                           //2
    public class None                       //3
    {                                       //4
        public int Property {get;set;}      //5
        public int Property2 {get;set;}     //6
    }                                       //7
}                                           //8
");
            var finder = new NodeFinder();
            var result = (PropertyDeclarationSyntax)finder.Find(syntax, 6, 1);
            Assert.IsNotNull(result);
            Assert.AreEqual("Property2", result.Identifier.ValueText);
        }
    }
}
