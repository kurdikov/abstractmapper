﻿using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.MSBuild;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    public class AssemblyInit
    {
        public static Compilation Compilation { get; private set; }

        [AssemblyInitialize]
        public static void Init(TestContext context)
        {
            //var workspace = MSBuildWorkspace.Create();
            //var sln = workspace.OpenSolutionAsync(@"").Result;
            //var project = sln.Projects.Single();
            //Compilation = project.GetCompilationAsync().Result;
        }

        [AssemblyCleanup]
        public static void Cleanup()
        {
        }
    }
}
