﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.MSBuild;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    [TestClass]
    public class MapperFinderTests
    {
        [TestMethod]
        public async Task Find()
        {
            var workspace = MSBuildWorkspace.Create();
            var project = await workspace
                .OpenProjectAsync(
                    @"..\..\..\TsSoft.AbstractMapper.ImplicitMemberUsage.Tests.Find\TsSoft.AbstractMapper.ImplicitMemberUsage.Tests.Find.csproj");
            var compilation = await project.GetCompilationAsync();

            var finder = new MapperFinder();
            var from = compilation.GetTypeByMetadataName("Tests.From");
            var mappers = finder.GetMappersFrom(compilation, from);
            Assert.AreEqual(3, mappers.Count);
            var typeNames = mappers.Select(m => m.Mapper.MetadataName).ToList();
            Assert.IsTrue(typeNames.Contains("Mapper"));
            Assert.IsTrue(typeNames.Contains("MapperDerived"));
            Assert.IsTrue(typeNames.Contains("MapperGeneric`1"));
        }
    }
}
