﻿using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TsSoft.AbstractMapper.ImplicitMemberUsage
{
    [TestClass]
    public class MemberRetrieverTests
    {
        [TestMethod]
        public void WithoutProperties()
        {
            var syntax = CSharpSyntaxTree.ParseText(@"
namespace Tests
{
    public class None
    {
    }
}
");
            var compilation = CSharpCompilation.Create("tests", new[] { syntax });
            var type = compilation.GetTypeByMetadataName("Tests.None");
            var retriever = new MemberRetriever();
            var result = retriever.GetProperties(type);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void WithTwoProperties()
        {
            var syntax = CSharpSyntaxTree.ParseText(@"
namespace Tests
{
    public class Props
    {
        public int Int {get;set;}
        public string String {get;set;}
    }
}
");
            var compilation = CSharpCompilation.Create("tests", new[] { syntax });
            var type = compilation.GetTypeByMetadataName("Tests.Props");
            var retriever = new MemberRetriever();
            var result = retriever.GetProperties(type);
            Assert.AreEqual(2, result.Count);
            var names = result.Select(r => r.Name).ToList();
            Assert.IsTrue(names.Contains("Int"));
            Assert.IsTrue(names.Contains("String"));
        }

        [TestMethod]
        public void WithInheritedProperties()
        {
            var syntax = CSharpSyntaxTree.ParseText(@"
namespace Tests
{
    public class Props : PropsBase
    {
        public int Int {get;set;}
        public string String {get;set;}
    }

    public class PropsBase
    {
        public int Inherited {get;set;}
    }
}
");
            var compilation = CSharpCompilation.Create("tests", new[] { syntax });
            var type = compilation.GetTypeByMetadataName("Tests.Props");
            var retriever = new MemberRetriever();
            var result = retriever.GetProperties(type);
            Assert.AreEqual(3, result.Count);
            var names = result.Select(r => r.Name).ToList();
            Assert.IsTrue(names.Contains("Int"));
            Assert.IsTrue(names.Contains("String"));
            Assert.IsTrue(names.Contains("Inherited"));
        }

        [TestMethod]
        public void WithTransitivelyInheritedProperties()
        {
            var syntax = CSharpSyntaxTree.ParseText(@"
namespace Tests
{
    public class Props : PropsBase
    {
        public int Int {get;set;}
        public string String {get;set;}
    }

    public class PropsBase : PropsBaseBase
    {
        public int Inherited {get;set;}
    }

    public class PropsBaseBase
    {
        public int InheritedBase {get;set;}
    }
}
");
            var compilation = CSharpCompilation.Create("tests", new[] { syntax });
            var type = compilation.GetTypeByMetadataName("Tests.Props");
            var retriever = new MemberRetriever();
            var result = retriever.GetProperties(type);
            Assert.AreEqual(4, result.Count);
            var names = result.Select(r => r.Name).ToList();
            Assert.IsTrue(names.Contains("Int"));
            Assert.IsTrue(names.Contains("String"));
            Assert.IsTrue(names.Contains("Inherited"));
            Assert.IsTrue(names.Contains("InheritedBase"));
        }

        [TestMethod]
        public void Struct()
        {
            var syntax = CSharpSyntaxTree.ParseText(@"
namespace Tests
{
    public struct Props
    {
        public int Int {get;set;}
        public string String {get;set;}
    }
}
");
            var compilation = CSharpCompilation.Create("tests", new[] { syntax });
            var type = compilation.GetTypeByMetadataName("Tests.Props");
            var retriever = new MemberRetriever();
            var result = retriever.GetProperties(type);
            Assert.AreEqual(2, result.Count);
            var names = result.Select(r => r.Name).ToList();
            Assert.IsTrue(names.Contains("Int"));
            Assert.IsTrue(names.Contains("String"));
        }

        [TestMethod]
        public void Interface()
        {
            var syntax = CSharpSyntaxTree.ParseText(@"
namespace Tests
{
    public interface IProps
    {
        int Int {get;set;}
        string String {get;set;}
    }
}
");
            var compilation = CSharpCompilation.Create("tests", new[] { syntax });
            var type = compilation.GetTypeByMetadataName("Tests.IProps");
            var retriever = new MemberRetriever();
            var result = retriever.GetProperties(type);
            Assert.AreEqual(2, result.Count);
            var names = result.Select(r => r.Name).ToList();
            Assert.IsTrue(names.Contains("Int"));
            Assert.IsTrue(names.Contains("String"));
        }

        [TestMethod]
        public void InterfaceWithInheritedProperties()
        {
            var syntax = CSharpSyntaxTree.ParseText(@"
namespace Tests
{
    public interface IProps : IBase
    {
        public int Int {get;set;}
        public string String {get;set;}
    }

    public interface IBase
    {
        public int Inherited {get;set;}
    }
}
");
            var compilation = CSharpCompilation.Create("tests", new[] { syntax });
            var type = compilation.GetTypeByMetadataName("Tests.IProps");
            var retriever = new MemberRetriever();
            var result = retriever.GetProperties(type);
            Assert.AreEqual(3, result.Count);
            var names = result.Select(r => r.Name).ToList();
            Assert.IsTrue(names.Contains("Int"));
            Assert.IsTrue(names.Contains("String"));
            Assert.IsTrue(names.Contains("Inherited"));
        }

        [TestMethod]
        public void RunPolygon()
        {
            var syntax = CSharpSyntaxTree.ParseText(@"
namespace Tests
{
    public class Base
    {
        public virtual int Prop {get;}
    }

    public class Derived : Base
    {
    }
}
");
            var compilation = CSharpCompilation.Create("tests", new[] { syntax });
            var type = compilation.GetTypeByMetadataName("Tests.Derived");
            var method = type.GetMembers().Where(m => m.Kind == SymbolKind.Property && m.Name == "Prop").ToList();
        }
    }
}
