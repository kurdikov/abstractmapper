﻿namespace TsSoft.AbstractMapper.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Runtime.CompilerServices;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.AbstractMapper.Mappers;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.IncludeBuilder.Models;

    [TestClass]
    public class MapperTests
    {
        private void CheckEquality(From from, ToBase res, bool checkChildren = true, string msg = "")
        {
            Assert.AreEqual(from.Id, res.Id, msg);
            Assert.AreEqual(from.Id.ToString("D2"), res.Id1, msg);
            Assert.AreEqual(from.Id, res.Id2, msg);
            Assert.AreEqual(@from.Id, res.Id3, msg);
            Assert.IsNotNull(res.NullableId, msg);
            Assert.AreEqual(from.Id, res.NullableId.Value, msg);
            Assert.AreEqual(from.Id.ToString(), res.StringId, msg);
            Assert.AreEqual(from.Name, res.Name, msg);
            Assert.AreEqual(from.Guid, res.Guid, msg);
            Assert.AreEqual(from.AEnum1.ToString(), res.AEnum1, msg);
            Assert.AreEqual(from.AEnum1.ToString(), res.AutoAEnum1String, msg);
            Assert.AreEqual((int)from.AEnum1, res.AutoAEnum1Int, msg);
            Assert.AreEqual((long)from.AEnum1, res.AutoAEnum1Long, msg);
            Assert.AreEqual((AEnum)from.AEnumInt, res.AEnumInt, msg);
            Assert.AreEqual((int?)from.NullableEnum, res.NullableEnum, msg);
            Assert.AreEqual(from.Id.ToString(), res.StringId, msg);
            AEnum str;
            Enum.TryParse(from.AEnumStr, true, out str);
            Assert.AreEqual(str, res.AEnumStr, msg);
            Assert.AreEqual(from.Field, res.Field, msg);

            if (res is To)
            {
                var resTo = (To) res;
                Assert.AreEqual(TestEntityProcessor.Processor.Process(from), resTo.MappedBySelfProcessor, msg);
                Assert.AreEqual(
                    TestEntityProcessor.Processor.ProcessMany(from.Children), resTo.MappedByChildrenProcessor, msg);
                Assert.AreEqual(TestEntitySelectProcessor.Proc.Process(from), resTo.MappedBySelfSelectProcessor, msg);
                Assert.AreEqual(
                    TestEntitySelectProcessor.Proc.ProcessMany(from.Children), resTo.MappedByChildrenSelectProcessor, msg);
                Assert.AreEqual(TestEntityProcessor.Processor.Process(from.Parent), resTo.MappedByParentProcessor, msg);
                Assert.AreEqual(
                    TestEntitySelectProcessor.Proc.Process(from.Parent), resTo.MappedByParentSelectProcessor, msg);
                Assert.AreEqual(@from.Parent != null ? @from.Parent.Id : default(int), resTo.ComplexPathRule, msg);
                Assert.AreEqual(
                    TestEntityInterfaceProcessor.Proc.Process(from.Parent), resTo.MappedByParentInterfaceProcessor, msg);
                Assert.AreEqual(TestEntityInterfaceProcessor.Proc.Process(from), resTo.MappedBySelfInterfaceProcessor, msg);
                Assert.AreEqual(
                    TestEntityInterfaceSelectProcessor.Proc.Process(from.Parent),
                    resTo.MappedByParentSelectInterfaceProcessor, msg);
                Assert.AreEqual(
                    TestEntityInterfaceSelectProcessor.Proc.Process(from), resTo.MappedBySelfSelectInterfaceProcessor, msg);
                if (from.Children != null && checkChildren)
                {
                    CheckCollectionEquality(
                        from.Children.Select(c => c.Id).ToList(), resTo.ComplexPathRuleWithCollection, msg);
                }
                Assert.AreEqual(@from.NullProperty.HasValue ? @from.NullProperty.Value : 234, resTo.NullProperty, msg);
            }
        }

        private void CheckCollectionEquality(IList<From> from, IList<ToBase> to, string msg = "")
        {
            Assert.AreEqual(from.Count, to.Count);
            for (int i = 0; i < from.Count; ++i)
            {
                CheckEquality(from[i], to[i], msg: msg + " -> " + i);
            }
        }

        private void CheckCollectionEquality(IEnumerable<int> from, IEnumerable<int> to, string msg = "")
        {
            if (from == null)
            {
                Assert.IsNull(to, msg);
                return;
            }
            var fromList = from.ToReadOnlyListIfNeeded();
            var toList = to.ToReadOnlyListIfNeeded();
            Assert.AreEqual(fromList.Count, toList.Count);
            for (int i = 0; i < fromList.Count; ++i)
            {
                Assert.AreEqual(fromList[i], toList[i], msg + " -> " + i);
            }
        }

        private void CheckCollectionEquality(IEnumerable<IntContainerOne> from, IEnumerable<IntContainerTwo> to, string msg = "")
        {
            if (from == null)
            {
                Assert.IsNull(to, msg);
                return;
            }
            var fromList = from.ToReadOnlyListIfNeeded();
            var toList = to.ToReadOnlyListIfNeeded();
            Assert.AreEqual(fromList.Count, toList.Count);
            for (int i = 0; i < fromList.Count; ++i)
            {
                if (fromList[i] != null)
                {
                    Assert.IsNotNull(toList[i], msg + " -> " + i);
                    Assert.AreEqual(fromList[i].Int, toList[i].Int, msg + " -> " + i);
                }
                else
                {
                    Assert.IsNull(toList[i]);
                }
            }
        }
        public class TestMapper : Mapper<From, To>
        {
            public TestMapper(IAbstractMapperHelper helper)
                : base(helper)
            {
            }

            protected override IMapRules<To, From> MapRules
            {
                get
                {
                    return new MapRules<To, From>
                        {
                            {to => to.ChildrenArray, from => from.Children},
                            {to => to.ChildrenCollection, from => from.Children},
                            {to => to.ChildrenList, from => from.Children},
                            {to => to.ChildrenInterface, from => from.Children},
                            {to => to.ChildrenSet, from => from.Children},
                            {to => to.ComplexPathRule, from => from.Parent.Id},
                            {to => to.ComplexPathRuleWithCollection, from => from.Children.Select(c => c.Id)},
                            {to => to.NullProperty, (int? x) => x.Value, () => 234},
                            ToBaseRules<To>.MapRules,
                            ToProcessedPropsRules<To>.MapRules,
                        };
                }
            }

            protected override IIgnoreRules<To> IgnoreRules
            {
                get { return new EmptyIgnoreRules<To>(); }
            }
        }

        public class TestBaseMapper : Mapper<From, ToBase>
        {
            public TestBaseMapper(IAbstractMapperHelper helper)
                : base(helper)
            {
            }

            protected override IMapRules<ToBase, From> MapRules
            {
                get { return ToBaseRules<ToBase>.MapRules; }
            }

            protected override IIgnoreRules<ToBase> IgnoreRules
            {
                get { return new EmptyIgnoreRules<ToBase>(); }
            }
        }

        [TestMethod]
        public void TestMapWithSubobjectAndCollection()
        {
            var @from = new From
            {
                Id = 1,
                Guid = Guid.NewGuid(),
                Name = "123",
                AEnum1 = AEnum.A,
                AEnumInt = 1,
                AEnumStr = "A",
                Field = "Field!",
                Parent = new From
                {
                    Id = 2,
                    Guid = Guid.NewGuid(),
                    Name = "321",
                    NullProperty = 123,
                    Parent = new From
                    {
                        Id = 3,
                        Guid = Guid.NewGuid(),
                        Name = "222",
                    }
                },
                Children = new[]
                {
                    new From
                    {
                        Id = 4,
                        Guid = Guid.NewGuid(),
                        Name = "456",
                        AEnum1 = AEnum.B,
                        AEnumInt = 2,
                        AEnumStr = "B"
                    },
                    new From
                    {
                        Id = 5,
                        Guid = Guid.NewGuid(),
                        Name = "789",
                        AEnum1 = AEnum.B,
                        AEnumInt = 2,
                        AEnumStr = "B"
                    },
                }
            };
            var mapper = AbstractMapperFactory.Create<TestMapper>(
                MapperTypes.Are(typeof(TestMapper), typeof(TestBaseMapper)));
            var res = mapper.Map(@from);

            CheckEquality(from, res);

            Assert.IsNotNull(res.Parent);
            CheckEquality(from.Parent, res.Parent);

            Assert.IsNotNull(res.Parent.Parent);
            CheckEquality(from.Parent.Parent, res.Parent.Parent);

            Assert.IsNull(res.Parent.Parent.Parent);

            Assert.IsNotNull(res.Children);
            var fromList = from.Children.ToList();
            CheckCollectionEquality(fromList, res.Children.ToList());
            CheckCollectionEquality(fromList, res.ChildrenArray.ToList());
            CheckCollectionEquality(fromList, res.ChildrenInterface.ToList());
            CheckCollectionEquality(fromList, res.ChildrenCollection.ToList());
            CheckCollectionEquality(fromList, res.ChildrenList);
            CheckCollectionEquality(fromList, res.ChildrenSet.ToList());
        }

        public class TestMapperWithComplexConstructor : TestMapper
        {
            public TestMapperWithComplexConstructor(IAbstractMapperHelper helper, IIncludeProvider<From> ep)
                : base(helper)
            {
            }
        }

        [TestMethod]
        public void TestMapWithMockedInnerMapper()
        {
            var @from = new From
            {
                Id = 1,
                Guid = Guid.NewGuid(),
                Name = "123",
                AEnum1 = AEnum.A,
                AEnumInt = 1,
                AEnumStr = "A",
                Field = "Field!",
                Parent = new From
                {
                    Id = 2,
                    Guid = Guid.NewGuid(),
                    Name = "321",
                    NullProperty = 123,
                },
                Children = new[]
                {
                    new From
                    {
                        Id = 4,
                        Guid = Guid.NewGuid(),
                        Name = "456",
                        AEnum1 = AEnum.B,
                        AEnumInt = 2,
                        AEnumStr = "B"
                    },
                }
            };
            var mock = new Mock<IMapper<From, ToBase>>(MockBehavior.Loose).Object;
            var mock2 = new Mock<IMapper<From, To>>(MockBehavior.Loose).Object;
            var mapper = AbstractMapperFactory.Create<TestMapper>(MockObjects.Are(mock, mock2));
            var res = mapper.Map(@from);
            CheckEquality(from, res, false);

            mapper =
                AbstractMapperFactory.Create<TestMapperWithComplexConstructor>(
                    MockObjects.Are(mock, mock2, TestEntityProcessor.Processor));
            res = mapper.Map(@from);
            CheckEquality(from, res, false);
        }

        [TestMethod]
        public void TestMapPropertyThrowingException()
        {
            var mapper = AbstractMapperFactory
                .Create<LooseMapper<ExceptionFrom, ExceptionTo>>(
                    MapperTypes.Are(typeof(LooseMapper<ExceptionFrom, ExceptionTo>)));
            ExceptAssert.Throws<InnerExpressionException>(() => mapper.Map(new ExceptionFrom()));
        }


        public class WithNotMappedPropertyThrows : Mapper<From, ToWithNotMappedProperty>
        {
            public WithNotMappedPropertyThrows(IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<ToWithNotMappedProperty, From> MapRules
            {
                get { return new MapRules<ToWithNotMappedProperty, From>(); }
            }

            protected override IIgnoreRules<ToWithNotMappedProperty> IgnoreRules
            {
                get { return new EmptyIgnoreRules<ToWithNotMappedProperty>(); }
            }
        }

        [TestMethod]
        public void TestStrictAutoPropertyBehavior()
        {
            var from = new From {Id = 1};
            var throwingMapper = AbstractMapperFactory.Create<WithNotMappedPropertyThrows>(
                MapperTypes.Are(typeof(WithNotMappedPropertyThrows)));
            ExceptAssert.Throws<InvalidRulesException>(() => throwingMapper.Map(from));
        }


        public class WithNotMappedPropertyDoesNotThrow : WithNotMappedPropertyThrows
        {
            public WithNotMappedPropertyDoesNotThrow(IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override AutoPropertiesBehavior AutoPropertiesBehavior
            {
                get
                {
                    return AutoPropertiesBehavior.Loose;
                }
            }
        }

        [TestMethod]
        public void TestLooseAutoPropertyBehavior()
        {
            var from = new From { Id = 1 };
            var notThrowingMapper = AbstractMapperFactory.Create<WithNotMappedPropertyDoesNotThrow>(
                MapperTypes.Are(typeof(WithNotMappedPropertyDoesNotThrow)));
            var result = notThrowingMapper.Map(from);
            Assert.AreEqual(from.Id, result.Id);
            Assert.AreEqual(default(int), result.NotInFrom);
        }


        public class TestMapperWithEnumerablesInMapRules : Mapper<From, SimpleToWithCollection>
        {
            public TestMapperWithEnumerablesInMapRules([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleToWithCollection, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleToWithCollection, From>
                    {
                        {t => t.Children, f => f.Children, c => c.Select(cc => new SimpleTo {Id = cc.Id})},
                    };
                }
            }

            protected override IIgnoreRules<SimpleToWithCollection> IgnoreRules
            {
                get
                {
                    return new IgnoreRules<SimpleToWithCollection>
                    {
                        t => t.Children2,
                    };
                }
            }
        }

        [TestMethod]
        public void TestCollectionsAsEnumerablesInRules()
        {
            var from = new From {Children = new[] {new From {Id = 1}, new From {Id = 2}}};
            var mapper = AbstractMapperFactory.Create<TestMapperWithEnumerablesInMapRules>(
                MapperTypes.Are(typeof(TestMapperWithEnumerablesInMapRules)));
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.Children.Count, 2);
            Assert.AreEqual(actual.Children.ElementAt(0).Id, 1);
            Assert.AreEqual(actual.Children.ElementAt(1).Id, 2);
        }


        public class MappedAndIgnoredMapper : Mapper<From, SimpleTo>
        {
            public MappedAndIgnoredMapper([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                {
                    {t => t.Id, f => f.Id}
                };
                }
            }

            protected override IIgnoreRules<SimpleTo> IgnoreRules
            {
                get { return new IgnoreRules<SimpleTo> { t => t.Id }; }
            }
        }

        [TestMethod]
        public void TestMappedAndIgnoredProperty()
        {
            var mapper = AbstractMapperFactory.Create<MappedAndIgnoredMapper>(MapperTypes.None);
            ExceptAssert.Throws<InvalidRulesException>(() => mapper.Map(new From()));
        }


        public class InnerMapperForConditionTest : IMapper<IEnumerable<TestEntity>, int>
        {
            public int Map(IEnumerable<TestEntity> @from)
            {
                var x = from.FirstOrDefault();
                return x != null && x.Prop3.HasValue ? x.Prop3.Value : 999;
            }
        }

        public class MapperForConditionTest : Mapper<TestEntity, MappedTestEntity1>
        {
            public MapperForConditionTest([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<MappedTestEntity1, TestEntity> MapRules
            {
                get
                {
                    return new MapRules<MappedTestEntity1, TestEntity>
                {
                    {e => e.Child1, g => g.Children.Where(c => c.Prop1 == g.Prop1)},
                    {e => e.Child2, h => h.Children.FirstOrDefault(d => d.Prop3 == h.Prop3), entity => entity.Prop1, () => 777},
                };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithMergedConditions()
        {
            var mapper = AbstractMapperFactory.Create<MapperForConditionTest>(MapperTypes.Are(typeof(InnerMapperForConditionTest)));
            var actual = mapper.Map(new TestEntity {Prop1 = 1});
            Assert.AreEqual(1, actual.Prop1);
            Assert.AreEqual(0, actual.Child1);
            Assert.AreEqual(777, actual.Child2);

            actual = mapper.Map(new TestEntity { Prop1 = 2, Prop3 = 3, Children = new[]
            {
                new TestEntity { Prop1 = 3, Prop3 = 1 }, 
                new TestEntity { Prop1 = 2, Prop3 = 20 },
                new TestEntity { Prop1 = 1, Prop3 = 3 },
            }});
            Assert.AreEqual(2, actual.Prop1);
            Assert.AreEqual(20, actual.Child1);
            Assert.AreEqual(1, actual.Child2);

            actual = mapper.Map(new TestEntity
            {
                Prop1 = 4,
                Prop3 = 3,
                Children = new[]
                {
                    new TestEntity { Prop1 = 3, Prop3 = 1 }, 
                    new TestEntity { Prop1 = 1, Prop3 = 3 },
                }
            });
            Assert.AreEqual(4, actual.Prop1);
            Assert.AreEqual(999, actual.Child1);
            Assert.AreEqual(1, actual.Child2);
        }


        public class GenericMapper<T> : Mapper<T, MappedTestEntity2> where T : class, ITestEntity
        {
            public GenericMapper([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<MappedTestEntity2, T> MapRules
            {
                get
                {
                    return new MapRules<MappedTestEntity2, T>
                {
                    {e => e.Int, e => e.Prop1},
                    {e => e.String, e => e.Prop2},
                };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithRulesUsingFromPropertiesDefinedOnInterface()
        {
            var mapper = AbstractMapperFactory.Create<GenericMapper<TestEntity>>(MapperTypes.None);
            var actual = mapper.Map(new TestEntity{Prop1 = 1, Prop2 = "2"});
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actual.Int);
            Assert.AreEqual("2", actual.String);
        }


        public class GenericMapper2<T> : Mapper<TestEntity, T> where T : class, IMappedEntity, new()
        {
            public GenericMapper2([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<T, TestEntity> MapRules
            {
                get
                {
                    return new MapRules<T, TestEntity>
                {
                    {e => e.Int, e => e.Prop1},
                    {e => e.String, e => e.Prop2},
                };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithRulesUsingToPropertiesDefinedOnInterface()
        {
            var mapper = AbstractMapperFactory.Create<GenericMapper2<MappedTestEntity2>>(MapperTypes.None);
            var actual = mapper.Map(new TestEntity { Prop1 = 1, Prop2 = "2" });
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actual.Int);
            Assert.AreEqual("2", actual.String);
        }


        public class GenericMapper3<T> : Mapper<TestEntity, T> where T : class, IMappedEntity, new()
        {
            public GenericMapper3([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IIgnoreRules<T> IgnoreRules
            {
                get
                {
                    return new IgnoreRules<T>
                    {
                        e => e.String,
                        e => e.Int,
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithIgnoreRulesUsingToPropertiesDefinedOnInterface()
        {
            var mapper = AbstractMapperFactory.Create<GenericMapper3<MappedTestEntity2>>(MapperTypes.None);
            var actual = mapper.Map(new TestEntity { Prop1 = 1, Prop2 = "2" });
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.Int);
            Assert.IsNull(actual.String);
        }

        public class OrderDependentMapper : Mapper<TestEntity, MappedTestEntity1>
        {
            public OrderDependentMapper([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<MappedTestEntity1, TestEntity> MapRules
            {
                get
                {
                    return new MapRules<MappedTestEntity1, TestEntity>
                    {
                        {m => m.Child1, AfterMapRule.Rule, entity1 => entity1.Prop1},
                        {m => m.Child2, AfterMapRule.Rule, entity1 => entity1.Child1 + 1},
                    };
                }
            }
        }

        public class OrderDependentMapper2 : Mapper<TestEntity, MappedTestEntity1>
        {
            public OrderDependentMapper2([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<MappedTestEntity1, TestEntity> MapRules
            {
                get
                {
                    return new MapRules<MappedTestEntity1, TestEntity>
                    {
                        {m => m.Child2, AfterMapRule.Rule, entity1 => entity1.Prop1},
                        {m => m.Child1, AfterMapRule.Rule, entity1 => entity1.Child2 + 1},
                    };
                }
            }
        }
        public class OrderDependentMapper3 : Mapper<TestEntity, MappedTestEntity1>
        {
            public OrderDependentMapper3([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<MappedTestEntity1, TestEntity> MapRules
            {
                get
                {
                    return new MapRules<MappedTestEntity1, TestEntity>
                    {
                        {m => m.Child1, AfterMapRule.Rule, entity1 => entity1.Child2 + 1},
                        {m => m.Child2, AfterMapRule.Rule, entity1 => entity1.Prop1},
                    };
                }
            }
        }
        public class OrderDependentMapper4 : Mapper<TestEntity, MappedTestEntity1>
        {
            public OrderDependentMapper4([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<MappedTestEntity1, TestEntity> MapRules
            {
                get
                {
                    return new MapRules<MappedTestEntity1, TestEntity>
                    {
                        {m => m.Child2, AfterMapRule.Rule, entity1 => entity1.Child1 + 1},
                        {m => m.Child1, AfterMapRule.Rule, entity1 => entity1.Prop1},
                    };
                }
            }
        }

        [TestMethod]
        public void TestOrderDependentMappers()
        {
            var res = new TestEntity() {Prop1 = 10};
            var mapper1 = AbstractMapperFactory.Create<OrderDependentMapper>(MockObjects.None);
            var mapper2 = AbstractMapperFactory.Create<OrderDependentMapper2>(MockObjects.None);
            var mapper3 = AbstractMapperFactory.Create<OrderDependentMapper3>(MockObjects.None);
            var mapper4 = AbstractMapperFactory.Create<OrderDependentMapper4>(MockObjects.None);
            var mapped = mapper1.Map(res);
            Assert.IsNotNull(mapped);
            Assert.AreEqual(10, mapped.Child1);
            Assert.AreEqual(11, mapped.Child2);
            mapped = mapper2.Map(res);
            Assert.IsNotNull(mapped);
            Assert.AreEqual(10, mapped.Child2);
            Assert.AreEqual(11, mapped.Child1);
            mapped = mapper3.Map(res);
            Assert.IsNotNull(mapped);
            Assert.AreEqual(1, mapped.Child1);
            Assert.AreEqual(10, mapped.Child2);
            mapped = mapper4.Map(res);
            Assert.IsNotNull(mapped);
            Assert.AreEqual(1, mapped.Child2);
            Assert.AreEqual(10, mapped.Child1);
        }


        public class NoMapTimeInjectionMapper : Mapper<From, SimpleNestedTo>
        {
            public NoMapTimeInjectionMapper([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override InnerMapperStrategy InnerMapperStrategy
            {
                get { return InnerMapperStrategy.StaticTypes | InnerMapperStrategy.ResolveOnMapperBuilding; }
            }
        }

        [TestMethod]
        public void TestBuildTimeResolutionMapper()
        {
            var entityTypesRetriever = new Mock<IEntityTypesRetriever>(MockBehavior.Strict);
            var mapperResolver = new Mock<IMapperResolver>(MockBehavior.Strict);
            var mockMapper = new Mock<IMapper<From, SimpleNestedTo>>(MockBehavior.Strict);
            var simpleTo = new SimpleNestedTo();
            mockMapper.Setup(m => m.Map(It.IsAny<From>())).Returns(simpleTo);
            mapperResolver.Setup(r => r.TryGet<From, SimpleNestedTo>()).Returns(mockMapper.Object).Verifiable();
            var mapper = AbstractMapperFactory.Create<NoMapTimeInjectionMapper>(entityTypesRetriever.Object, mapperResolver.Object);
            var from = new From {Id = 3, Parent = new From {Id = 4}, Children = new[] {new From {Id = 5}, new From {Id = 6}}};

            var res = mapper.Map(from);
            Assert.IsNotNull(res);
            Assert.IsNotNull(res.Children);
            Assert.AreEqual(from.Id, res.Id);
            Assert.AreSame(simpleTo, res.Parent);
            var children = res.Children.ToList();
            Assert.AreEqual(2, children.Count());
            Assert.AreSame(simpleTo, children[0]);
            Assert.AreSame(simpleTo, children[1]);

            res = mapper.Map(from);
            Assert.IsNotNull(res);
            Assert.IsNotNull(res.Children);
            Assert.AreEqual(from.Id, res.Id);
            Assert.AreSame(simpleTo, res.Parent);
            children = res.Children.ToList();
            Assert.AreEqual(2, children.Count());
            Assert.AreSame(simpleTo, children[0]);
            Assert.AreSame(simpleTo, children[1]);

            mapperResolver.Verify(r => r.TryGet<From, SimpleNestedTo>(), Times.Exactly(2));
        }


        public class MapTimeInjectionMapper : Mapper<From, SimpleNestedTo>
        {
            public MapTimeInjectionMapper([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override InnerMapperStrategy InnerMapperStrategy
            {
                get { return InnerMapperStrategy.StaticTypes | InnerMapperStrategy.ResolveOnMapping; }
            }
        }

        [TestMethod]
        public void TestMapTimeResolutionMapper()
        {
            var entityTypesRetriever = new Mock<IEntityTypesRetriever>(MockBehavior.Strict);
            var mapperResolver = new Mock<IMapperResolver>(MockBehavior.Strict);
            var mockMapper = new Mock<IMapper<From, SimpleNestedTo>>(MockBehavior.Strict);
            var simpleTo = new SimpleNestedTo();
            mockMapper.Setup(m => m.Map(It.IsAny<From>())).Returns(simpleTo);
            mapperResolver.Setup(r => r.TryGet<From, SimpleNestedTo>()).Returns(mockMapper.Object).Verifiable();
            var mapper = AbstractMapperFactory.Create<MapTimeInjectionMapper>(entityTypesRetriever.Object, mapperResolver.Object);
            var from = new From { Id = 3, Parent = new From { Id = 4 }, Children = new[] { new From { Id = 5 }, new From { Id = 6 } } };

            var res = mapper.Map(from);
            Assert.IsNotNull(res);
            Assert.IsNotNull(res.Children);
            Assert.AreEqual(from.Id, res.Id);
            Assert.AreSame(simpleTo, res.Parent);
            var children = res.Children.ToList();
            Assert.AreEqual(2, children.Count());
            Assert.AreSame(simpleTo, children[0]);
            Assert.AreSame(simpleTo, children[1]);

            res = mapper.Map(from);
            Assert.IsNotNull(res);
            Assert.IsNotNull(res.Children);
            Assert.AreEqual(from.Id, res.Id);
            Assert.AreSame(simpleTo, res.Parent);
            children = res.Children.ToList();
            Assert.AreEqual(2, children.Count());
            Assert.AreSame(simpleTo, children[0]);
            Assert.AreSame(simpleTo, children[1]);

            mapperResolver.Verify(r => r.TryGet<From, SimpleNestedTo>(), Times.Exactly(4));
        }


        public class MapToInterfaceWithoutSetter<T> : Mapper<From, T> where T : class, ISimpleTo, new()
        {
            public MapToInterfaceWithoutSetter([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<T, From> MapRules
            {
                get
                {
                    return new MapRules<T, From>
                        {
                            {t => t.Id, f => f.Id, id => id + 1},
                        };
                }
            }
        }

        [TestMethod]
        public void TestMapRuleWithInterfacePropertyWithoutSetter()
        {
            var mapper = AbstractMapperFactory.Create<MapToInterfaceWithoutSetter<SimpleTo>>(MapperTypes.None);
            var from = new From() {Id = 1, Name = "2"};
            var to = mapper.Map(from);
            Assert.AreEqual(from.Id + 1, to.Id);
        }


        public class InterfacePropertyMapper<T> : Mapper<T, SimpleTo> where T : class, IFrom
        {
            public static class InterfaceMapRuleProvider<TInt> where TInt : IFrom
            {
                [NotNull]
                public static MapRules<SimpleTo, TInt> Provide()
                {
                    return new MapRules<SimpleTo, TInt>
                {
                    {t => t.Id, f => f.Id},
                };
                }
            }

            public InterfacePropertyMapper([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, T> MapRules
            {
                get { return InterfaceMapRuleProvider<T>.Provide(); }
            }

            public MapRule GetMapRule()
            {
                return MapRules.First();
            }
        }

        [TestMethod]
        public void TestMapRuleWithConvertOnParameter()
        {
            var mapper = AbstractMapperFactory.Create<InterfacePropertyMapper<From>>(MapperTypes.None);
            var from = new From() {Id = 1};
            var to = mapper.Map(from);
            var mr = mapper.GetMapRule();
            var par = Expression.Parameter(typeof(From));
            ExprAssert.AreEqual(
                mr.FromAccess.First().FromAccess as Expression<Func<From, int>>,
                Expression.Lambda<Func<From, int>>(
                    Expression.Property(Expression.Convert(par, typeof(IFrom)), "Id"),
                    par));
            Assert.AreEqual(from.Id, to.Id);
        }


        public class NullablePathMapper : Mapper<From, MappedTestEntity3>
        {
            public NullablePathMapper([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<MappedTestEntity3, From> MapRules
            {
                get
                {
                    return new MapRules<MappedTestEntity3, From>
                    {
                        {e => e.Result, f => f.Parent.Id, (int? i) => i},
                    };
                }
            }
        }

        [TestMethod]
        public void TestNullablePathMapper()
        {
            var mapper = AbstractMapperFactory.Create<NullablePathMapper>(MapperTypes.None);
            var from = new From {Parent = new From {Id = 1}};
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actual.Result);

            from = new From();
            actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.IsNull(actual.Result);

        }


        public class MapperWithListReturningRule : Mapper<From, SimpleEnumerableTo>
        {
            public MapperWithListReturningRule([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleEnumerableTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleEnumerableTo, From>
                    {
                        {f => f.Ids, f => f.Id, i => i == 1 ? new List<int> {1,2} : (List<int>)null},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithListReturningRule()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithListReturningRule>(MapperTypes.None);
            var from = new From {Id = 1};
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Ids);
            CollectionAssert.AreEqual(new List<int> {1,2}, actual.Ids.ToList());

            from = new From {Id = 2};
            actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.IsNull(actual.Ids);
        }


        public class MapperWithIenumerableReturningRule : Mapper<From, SimpleListTo>
        {
            public MapperWithIenumerableReturningRule([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleListTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleListTo, From>
                    {
                        {f => f.Ids, f => f.Id, i => i == 1 ? Enumerable.Repeat(1, 2) : (IEnumerable<int>)null},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithEnumerableReturningRule()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithIenumerableReturningRule>(MapperTypes.None);
            var from = new From { Id = 1 };
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Ids);
            CollectionAssert.AreEqual(new List<int> { 1, 1 }, actual.Ids);

            from = new From { Id = 2 };
            actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.IsNull(actual.Ids);
        }


        public class DictionaryTo
        {
            public IDictionary<int, IReadOnlyCollection<int>> Dict { get; set; }
        }

        public class DictionaryInterfaceMapper : Mapper<From, DictionaryTo>
        {
            public DictionaryInterfaceMapper([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<DictionaryTo, From> MapRules
            {
                get
                {
                    return new MapRules<DictionaryTo, From>
                    {
                        {t => t.Dict, f => f.Children, cc => cc.ToDictionary(c => c.Id, c => new[] {c.Id} as IReadOnlyCollection<int>)}
                    };
                }
            }
        }

        [TestMethod]
        public void TestDictionaryInterfaceMapper()
        {
            var mapper = AbstractMapperFactory.Create<DictionaryInterfaceMapper>(MapperTypes.None);
            var from = new From {Id = 1, Children = new From[] {new From {Id = 2}, new From() {Id = 3}}};
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Dict);
            Assert.AreEqual(2, actual.Dict.Count);
            CollectionAssert.AreEqual(new[] {2}, actual.Dict[2].ToList());
            CollectionAssert.AreEqual(new[] { 3 }, actual.Dict[3].ToList());
        }


        private class MapperWithPathReferencingOuterParameter : Mapper<From, SimpleTo>
        {
            public MapperWithPathReferencingOuterParameter([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Id == f.Id), froms => froms.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithPathReferencingOuterParameter()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithPathReferencingOuterParameter>(MapperTypes.None);
            var from = new From
            {
                Id = 1,
                Children = new[] {new From {Id = 2}, new From {Id = 1}, new From {Id = 3}, new From {Id = 1}}
            };
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(2, actual.Id);
        }


        private class MapperWithConditionWithNot : Mapper<From, SimpleTo>
        {
            public MapperWithConditionWithNot([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => !c.Bool), froms => froms.Sum(f => f.Id)},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithNotInCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithConditionWithNot>(MapperTypes.None);
            var from = new From
            {
                Id = 1,
                Children = new[] {new From{Id = 2, Bool = true}, null, new From{Id = 4}, null, new From {Id = 8}, null, new From {Id = 16, Bool = true}}
            };
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(12, actual.Id);
        }


        private class MapperWithConditionWithAndAlso : Mapper<From, SimpleTo>
        {
            public MapperWithConditionWithAndAlso([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Bool && c.AEnumInt == 1), froms => froms.Sum(f => f.Id)}
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithAndAlsoInCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithConditionWithAndAlso>(MapperTypes.None);
            var from = new From
            {
                Id = 1,
                Children = new[]
                {
                    new From { Id = 2, Bool = true, AEnumInt = 1},
                    new From { Id = 4, Bool = false, AEnumInt = 2},
                    new From { Id = 8, Bool = false, AEnumInt = 1},
                    new From { Id = 16, Bool = true, AEnumInt = 2},
                    null,
                }
            };
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(2, actual.Id);
        }


        private class MapperWithConditionWithAnd : Mapper<From, SimpleTo>
        {
            public MapperWithConditionWithAnd([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Bool & c.AEnumInt == 1), froms => froms.Sum(f => f.Id)}
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithAndInCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithConditionWithAnd>(MapperTypes.None);
            var from = new From
            {
                Id = 1,
                Children = new[]
                {
                    new From { Id = 2, Bool = true, AEnumInt = 1},
                    new From { Id = 4, Bool = false, AEnumInt = 2},
                    new From { Id = 8, Bool = false, AEnumInt = 1},
                    new From { Id = 16, Bool = true, AEnumInt = 2},
                    null,
                }
            };
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(2, actual.Id);
        }


        private class MapperWithConditionWithOrElse : Mapper<From, SimpleTo>
        {
            public MapperWithConditionWithOrElse([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Bool || c.AEnumInt == 1), froms => froms.Sum(f => f.Id)}
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithOrElseInCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithConditionWithOrElse>(MapperTypes.None);
            var from = new From
            {
                Id = 1,
                Children = new[]
                {
                    new From { Id = 2, Bool = true, AEnumInt = 1},
                    new From { Id = 4, Bool = false, AEnumInt = 2},
                    new From { Id = 8, Bool = false, AEnumInt = 1},
                    new From { Id = 16, Bool = true, AEnumInt = 2},
                    null,
                }
            };
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(26, actual.Id);
        }


        private class MapperWithConditionWithOr : Mapper<From, SimpleTo>
        {
            public MapperWithConditionWithOr([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Bool | c.AEnumInt == 1), froms => froms.Sum(f => f.Id)}
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithOrInCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithConditionWithOr>(MapperTypes.None);
            var from = new From
            {
                Id = 1,
                Children = new[]
                {
                    new From { Id = 2, Bool = true, AEnumInt = 1},
                    new From { Id = 4, Bool = false, AEnumInt = 2},
                    new From { Id = 8, Bool = false, AEnumInt = 1},
                    new From { Id = 16, Bool = true, AEnumInt = 2},
                    null,
                }
            };
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(26, actual.Id);
        }


        private class MapperWithConditionWithXor : Mapper<From, SimpleTo>
        {
            public MapperWithConditionWithXor([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Bool ^ c.AEnumInt == 1), froms => froms.Sum(f => f.Id)}
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithXorInCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithConditionWithXor>(MapperTypes.None);
            var from = new From
            {
                Id = 1,
                Children = new[]
                {
                    new From { Id = 2, Bool = true, AEnumInt = 1},
                    new From { Id = 4, Bool = false, AEnumInt = 2},
                    new From { Id = 8, Bool = false, AEnumInt = 1},
                    new From { Id = 16, Bool = true, AEnumInt = 2},
                    null,
                }
            };
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(24, actual.Id);
        }


        private class MapperWithConditionWithCoalesce : Mapper<From, SimpleTo>
        {
            public MapperWithConditionWithCoalesce([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.NullableBool ?? c.Bool), froms => froms.Sum(f => f.Id)}
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithCoalesceInCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithConditionWithCoalesce>(MapperTypes.None);
            var from = new From
            {
                Id = 1,
                Children = new[]
                {
                    new From { Id = 2, Bool = true, NullableBool = null},
                    new From { Id = 4, Bool = false, NullableBool = null},
                    new From { Id = 8, Bool = false, NullableBool = false},
                    new From { Id = 16, Bool = true, NullableBool = false},
                    new From { Id = 32, Bool = false, NullableBool = true},
                    new From { Id = 64, Bool = true, NullableBool = true},
                    null,
                }
            };
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(98, actual.Id);
        }


        private class MapperWithConditionWithConditionalOperator : Mapper<From, SimpleTo>
        {
            public MapperWithConditionWithConditionalOperator([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {f => f.Id, f => f.Children.Where(c => c.Bool ? c.AEnumInt == 1 : c.AEnumInt == 2), froms => froms.Sum(f => f.Id)}
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithConditionalOperatorInCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithConditionWithConditionalOperator>(MapperTypes.None);
            var from = new From
            {
                Id = 1,
                Children = new[]
                {
                    new From { Id = 2, Bool = true, AEnumInt = 1},
                    new From { Id = 4, Bool = false, AEnumInt = 1},
                    new From { Id = 8, Bool = false, AEnumInt = 2},
                    new From { Id = 16, Bool = true, AEnumInt = 2},
                    new From { Id = 32, Bool = false, AEnumInt = 3},
                    new From { Id = 64, Bool = true, AEnumInt = 3},
                    null,
                }
            };
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(10, actual.Id);
        }


        private class MapperWithConditionWithConditionalOperatorWithIfFalseTrueOnNull : Mapper<From, SimpleTo>
        {
            public MapperWithConditionWithConditionalOperatorWithIfFalseTrueOnNull([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {f => f.Id, f => f.Children.Where(c => c.Bool ? c.AEnumInt == 1 : true), froms => froms.Sum(f => f != null ? f.Id : 32)}
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithConditionWithConditionalOperatorWithIfFalseTrueOnNull()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithConditionWithConditionalOperatorWithIfFalseTrueOnNull>(MapperTypes.None);
            var from = new From
            {
                Id = 1,
                Children = new[]
                {
                    new From { Id = 2, Bool = true, AEnumInt = 1},
                    new From { Id = 4, Bool = false, AEnumInt = 1},
                    new From { Id = 8, Bool = false, AEnumInt = 2},
                    new From { Id = 16, Bool = true, AEnumInt = 2},
                    null,
                }
            };
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(46, actual.Id);
        }


        private class NestedPrivateFrom
        {
            public int Id { get; set; }

            public List<NestedPrivateFromChild> Children { get; set; }
        }

        private class NestedPrivateFromChild
        {
            public int Id { get; set; }
        }

        private class NestedPrivateTo
        {
            public int Id { get; set; }

            public List<NestedPrivateToChild> Children { get; set; }
        }

        private class NestedPrivateToChild
        {
            public int Id { get; set; }
        }

        [TestMethod]
        public void TestNestedPrivateClassesMapperWithCollection()
        {
            var mapper = AbstractMapperFactory.Create<Mapper<NestedPrivateFrom, NestedPrivateTo>>(
                MapperTypes.Are(typeof(Mapper<NestedPrivateFromChild, NestedPrivateToChild>)));
            var from = new NestedPrivateFrom
            {
                Id = 1,
                Children =
                    new List<NestedPrivateFromChild>
                    {
                        new NestedPrivateFromChild {Id = 2},
                        new NestedPrivateFromChild {Id = 3}
                    }
            };
            var result = mapper.Map(from);
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
            Assert.IsNotNull(result.Children);
            Assert.AreEqual(2, result.Children.Count);
            Assert.AreEqual(2, result.Children[0].Id);
            Assert.AreEqual(3, result.Children[1].Id);
        }


        public class CollectionOuterFrom
        {
            public IEnumerable<CollectionInnerFrom> Coll { get; set; }
            public CollectionInnerFrom Obj { get; set; }
        }

        public class CollectionInnerFrom
        {
            public int Id { get; set; }
        }

        public class CollectionOuterTo
        {
            public CollectionOuterTo()
            {
                Obj = 165;
                Coll = new HashSet<CollectionInnerTo>();
            }
            public ICollection<CollectionInnerTo> Coll { get; set; }
            public int Obj { get; set; }
        }

        public class CollectionInnerTo
        {
            public int Id { get; set; }
        }

        public class CollectionInnerFromToIntMapper : IMapper<CollectionInnerFrom, int>
        {
            public int Map(CollectionInnerFrom @from)
            {
                throw new NotImplementedException();
            }
        }

        [TestMethod]
        public void TestNoAssignmentOnNullComingFromAutoRule()
        {
            var mapper = AbstractMapperFactory.Create<Mapper<CollectionOuterFrom, CollectionOuterTo>>(
                MapperTypes.Are(typeof(Mapper<CollectionInnerFrom, CollectionInnerTo>), typeof(CollectionInnerFromToIntMapper)));
            var res = mapper.Map(new CollectionOuterFrom());
            Assert.IsNotNull(res);
            Assert.IsNotNull(res.Coll);
            Assert.AreEqual(165, res.Obj);
        }


        public class InvalidConvertParameterCountMapper : Mapper<From, SimpleTo>
        {
            public InvalidConvertParameterCountMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    Expression<Func<SimpleTo, int>> toAccess = t => t.Id;
                    Expression<Func<int, int>> convert = i => i;
                    return new MapRules<SimpleTo, From>
                    {
                        new MapRule
                        {
                            FromAccess = new MapSource[0],
                            Convert = convert,
                            ToAccess = toAccess,
                        }
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithRuleWithWrongConvertParameterCountThrows()
        {
            var mapper = AbstractMapperFactory.Create<InvalidConvertParameterCountMapper>(MapperTypes.None);
            ExceptAssert.Throws<InvalidRulesException>(() => mapper.Map(new From()));
        }


        public class RuleWithoutToMapper : Mapper<From, SimpleTo>
        {
            public RuleWithoutToMapper([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    Expression<Func<int>> convert = () => 2;
                    return new MapRules<SimpleTo, From>
                    {
                        new MapRule
                        {
                            FromAccess = new MapSource[0],
                            Convert = convert,
                        }
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithRuleWithoutToThrows()
        {
            var mapper = AbstractMapperFactory.Create<RuleWithoutToMapper>(MapperTypes.None);
            ExceptAssert.Throws<InvalidRulesException>(() => mapper.Map(new From()));
        }


        public class ToInterfaceMapper : Mapper<From, IFrom>
        {
            public ToInterfaceMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<IFrom, From> MapRules
            {
                get
                {
                    return new MapRules<IFrom, From>
                    {
                        {f => f.Id, f => f.Id, i => i + 1},
                    };
                }
            }
        }

        [TestMethod]
        public void TestToInterfaceMapper()
        {
            var mapper = AbstractMapperFactory.Create<ToInterfaceMapper>(MapperTypes.None);
            var res = mapper.Map(new From {Id = 2, Name = "2"});
            Assert.IsNotNull(res);
            Assert.AreEqual(3, res.Id);
            Assert.AreEqual("2", res.Name);
        }


        public class RegisteringFrom
        {
            private int _id;
            private int _count;

            public int Id {get { _count++; return _id; } set { _id = value; }}
            public int Count {get { return _count; }}
        }

        public class MapperWithReusedParameterInRule : Mapper<RegisteringFrom, SimpleTo>
        {
            public MapperWithReusedParameterInRule([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, RegisteringFrom> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, RegisteringFrom>
                    {
                        {t => t.Id, f => f.Id, i => i + i},
                    };
                }
            }
        }

        [TestMethod]
        public void TestReusingParameterInRuleDoesNotRecomputeValue()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithReusedParameterInRule>(MapperTypes.None);
            var from = new RegisteringFrom {Id = 2};
            var res = mapper.Map(from);
            Assert.IsNotNull(res);
            Assert.AreEqual(4, res.Id);
            Assert.AreEqual(1, from.Count);
        }


        private class CollectionContainerMapper<T1, T2> : Mapper<CollectionContainer<T1>, CollectionContainerMapped<T2>>
        {
            public CollectionContainerMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<CollectionContainerMapped<T2>, CollectionContainer<T1>> MapRules
            {
                get
                {
                    return new MapRules<CollectionContainerMapped<T2>, CollectionContainer<T1>>
                    {
                        {m => m.EnumerableOneArray, m => m.EnumerableOne},
                        {m => m.EnumerableOneRoc, m => m.EnumerableOne},
                        {m => m.EnumerableOneC, m => m.EnumerableOne},
                        {m => m.EnumerableOneList, m => m.EnumerableOne},

                        {m => m.EnumerableTwoArray, m => m.EnumerableTwo},
                        {m => m.EnumerableTwoRoc, m => m.EnumerableTwo},
                        {m => m.EnumerableTwoC, m => m.EnumerableTwo},
                        {m => m.EnumerableTwoList, m => m.EnumerableTwo},

                        {m => m.ReadOnlyCollectionArray, m => m.ReadOnlyCollection},
                        {m => m.ReadOnlyCollectionRoc, m => m.ReadOnlyCollection},
                        {m => m.ReadOnlyCollectionC, m => m.ReadOnlyCollection},
                        {m => m.ReadOnlyCollectionList, m => m.ReadOnlyCollection},

                        {m => m.CollectionArray, m => m.Collection},
                        {m => m.CollectionRoc, m => m.Collection},
                        {m => m.CollectionC, m => m.Collection},
                        {m => m.CollectionList, m => m.Collection},

                        {m => m.ListArray, m => m.List},
                        {m => m.ListRoc, m => m.List},
                        {m => m.ListC, m => m.List},
                        {m => m.ListList, m => m.List},

                        {m => m.ArrayArray, m => m.Array},
                        {m => m.ArrayRoc, m => m.Array},
                        {m => m.ArrayC, m => m.Array},
                        {m => m.ArrayList, m => m.Array},
                    };
                }
            }
        }


        [TestMethod]
        public void TestIntCollectionMapper()
        {
            var mapper = AbstractMapperFactory.Create<CollectionContainerMapper<int, int>>(MapperTypes.None);
            var from = new CollectionContainer<int>
            {
                EnumerableOne = Enumerable.Range(1, 3),
                EnumerableTwo = new[] {1, 2, 3},
                Array = new[] {1, 2, 3},
                List = new List<int> {1, 2, 3},
                Collection = new Collection<int> {1, 2, 3},
                ReadOnlyCollection = new ReadOnlyCollection<int>(new List<int> {1, 2, 3}),
            };
            var result = mapper.Map(from);
            Assert.IsNotNull(result);
            CheckCollectionEquality(from.EnumerableOne, result.EnumerableOne, "EnumerableOne");
            CheckCollectionEquality(from.EnumerableOne, result.EnumerableOneArray, "EnumerableOneArray");
            CheckCollectionEquality(from.EnumerableOne, result.EnumerableOneC, "EnumerableOneC");
            CheckCollectionEquality(from.EnumerableOne, result.EnumerableOneList, "EnumerableOneList");
            CheckCollectionEquality(from.EnumerableOne, result.EnumerableOneRoc, "EnumerableOneRoc");

            CheckCollectionEquality(from.EnumerableTwo, result.EnumerableTwo, "EnumerableTwo");
            CheckCollectionEquality(from.EnumerableTwo, result.EnumerableTwoArray, "EnumerableTwoArray");
            CheckCollectionEquality(from.EnumerableTwo, result.EnumerableTwoC, "EnumerableTwoC");
            CheckCollectionEquality(from.EnumerableTwo, result.EnumerableTwoList, "EnumerableTwoList");
            CheckCollectionEquality(from.EnumerableTwo, result.EnumerableTwoRoc, "EnumerableTwoRoc");

            CheckCollectionEquality(from.ReadOnlyCollection, result.ReadOnlyCollection, "ReadOnlyCollection");
            CheckCollectionEquality(from.ReadOnlyCollection, result.ReadOnlyCollectionArray, "ReadOnlyCollectionArray");
            CheckCollectionEquality(from.ReadOnlyCollection, result.ReadOnlyCollectionC, "ReadOnlyCollectionC");
            CheckCollectionEquality(from.ReadOnlyCollection, result.ReadOnlyCollectionList, "ReadOnlyCollectionList");
            CheckCollectionEquality(from.ReadOnlyCollection, result.ReadOnlyCollectionRoc, "ReadOnlyCollectionRoc");

            CheckCollectionEquality(from.Collection, result.Collection, "Collection");
            CheckCollectionEquality(from.Collection, result.CollectionArray, "CollectionArray");
            CheckCollectionEquality(from.Collection, result.CollectionC, "CollectionC");
            CheckCollectionEquality(from.Collection, result.CollectionList, "CollectionList");
            CheckCollectionEquality(from.Collection, result.CollectionRoc, "CollectionRoc");

            CheckCollectionEquality(from.Array, result.Array, "Array");
            CheckCollectionEquality(from.Array, result.ArrayArray, "ArrayArray");
            CheckCollectionEquality(from.Array, result.ArrayC, "ArrayC");
            CheckCollectionEquality(from.Array, result.ArrayList, "ArrayList");
            CheckCollectionEquality(from.Array, result.ArrayRoc, "ArrayRoc");

            CheckCollectionEquality(from.List, result.List, "List");
            CheckCollectionEquality(from.List, result.ListArray, "ListArray");
            CheckCollectionEquality(from.List, result.ListC, "ListC");
            CheckCollectionEquality(from.List, result.ListList, "ListList");
            CheckCollectionEquality(from.List, result.ListRoc, "ListRoc");
        }


        private class IntContainerOne
        {
            public int Int { get; set; }

            public IntContainerOne(int i)
            {
                Int = i;
            }
        }

        private class IntContainerTwo
        {
            public int Int { get; set; }
        }

        [TestMethod]
        public void TestClassCollectionMapper()
        {
            var mapper = AbstractMapperFactory.Create<CollectionContainerMapper<IntContainerOne, IntContainerTwo>>(MapperTypes.Are(typeof(Mapper<IntContainerOne, IntContainerTwo>)));
            var from = new CollectionContainer<IntContainerOne>
            {
                EnumerableOne = Enumerable.Range(1, 3).Select(i => new IntContainerOne(i)),
                EnumerableTwo = new[] { new IntContainerOne(1), new IntContainerOne(2), new IntContainerOne(3),  },
                Array = new[] { new IntContainerOne(1), new IntContainerOne(2), new IntContainerOne(3), },
                List = new List<IntContainerOne> { new IntContainerOne(1), new IntContainerOne(2), new IntContainerOne(3), },
                Collection = new Collection<IntContainerOne> { new IntContainerOne(1), new IntContainerOne(2), new IntContainerOne(3), },
                ReadOnlyCollection = new ReadOnlyCollection<IntContainerOne>(new List<IntContainerOne> { new IntContainerOne(1), new IntContainerOne(2), new IntContainerOne(3), }),
            };
            var result = mapper.Map(from);
            Assert.IsNotNull(result);
            CheckCollectionEquality(from.EnumerableOne, result.EnumerableOne, "EnumerableOne");
            CheckCollectionEquality(from.EnumerableOne, result.EnumerableOneArray, "EnumerableOneArray");
            CheckCollectionEquality(from.EnumerableOne, result.EnumerableOneC, "EnumerableOneC");
            CheckCollectionEquality(from.EnumerableOne, result.EnumerableOneList, "EnumerableOneList");
            CheckCollectionEquality(from.EnumerableOne, result.EnumerableOneRoc, "EnumerableOneRoc");

            CheckCollectionEquality(from.EnumerableTwo, result.EnumerableTwo, "EnumerableTwo");
            CheckCollectionEquality(from.EnumerableTwo, result.EnumerableTwoArray, "EnumerableTwoArray");
            CheckCollectionEquality(from.EnumerableTwo, result.EnumerableTwoC, "EnumerableTwoC");
            CheckCollectionEquality(from.EnumerableTwo, result.EnumerableTwoList, "EnumerableTwoList");
            CheckCollectionEquality(from.EnumerableTwo, result.EnumerableTwoRoc, "EnumerableTwoRoc");

            CheckCollectionEquality(from.ReadOnlyCollection, result.ReadOnlyCollection, "ReadOnlyCollection");
            CheckCollectionEquality(from.ReadOnlyCollection, result.ReadOnlyCollectionArray, "ReadOnlyCollectionArray");
            CheckCollectionEquality(from.ReadOnlyCollection, result.ReadOnlyCollectionC, "ReadOnlyCollectionC");
            CheckCollectionEquality(from.ReadOnlyCollection, result.ReadOnlyCollectionList, "ReadOnlyCollectionList");
            CheckCollectionEquality(from.ReadOnlyCollection, result.ReadOnlyCollectionRoc, "ReadOnlyCollectionRoc");

            CheckCollectionEquality(from.Collection, result.Collection, "Collection");
            CheckCollectionEquality(from.Collection, result.CollectionArray, "CollectionArray");
            CheckCollectionEquality(from.Collection, result.CollectionC, "CollectionC");
            CheckCollectionEquality(from.Collection, result.CollectionList, "CollectionList");
            CheckCollectionEquality(from.Collection, result.CollectionRoc, "CollectionRoc");

            CheckCollectionEquality(from.Array, result.Array, "Array");
            CheckCollectionEquality(from.Array, result.ArrayArray, "ArrayArray");
            CheckCollectionEquality(from.Array, result.ArrayC, "ArrayC");
            CheckCollectionEquality(from.Array, result.ArrayList, "ArrayList");
            CheckCollectionEquality(from.Array, result.ArrayRoc, "ArrayRoc");

            CheckCollectionEquality(from.List, result.List, "List");
            CheckCollectionEquality(from.List, result.ListArray, "ListArray");
            CheckCollectionEquality(from.List, result.ListC, "ListC");
            CheckCollectionEquality(from.List, result.ListList, "ListList");
            CheckCollectionEquality(from.List, result.ListRoc, "ListRoc");
        }


        private class MapperWithCastedDefaultRule : Mapper<From, SimpleTo>
        {
            public MapperWithCastedDefaultRule([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => (int?)f.Parent.Id, i => i.Value, () => 4},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithCastedDefaultRule()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithCastedDefaultRule>(
                MapperTypes.None);
            var from = new From {Parent = new From {Id = 1}};
            var result = mapper.Map(from);
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);

            from = new From();
            result = mapper.Map(from);
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Id);
        }


        private class ConvertibleFrom
        {
            public byte Byte { get; set; }
            public sbyte Sbyte { get; set; }
        }

        private class ConvertibleTo
        {
            public int Int { get; set; }
            public uint Uint { get; set; }
            public decimal Decimal { get; set; }
            public double Double { get; set; }
            public int? NInt { get; set; }
            public uint? NUint { get; set; }
            public decimal? NDecimal { get; set; }
            public double? NDouble { get; set; }
        }

        private class MapperWithImplicitConversions : Mapper<ConvertibleFrom, ConvertibleTo>
        {
            public MapperWithImplicitConversions([NotNull]IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<ConvertibleTo, ConvertibleFrom> MapRules
            {
                get
                {
                    return new MapRules<ConvertibleTo, ConvertibleFrom>
                    {
                        { t => t.Int, f => f.Sbyte },
                        { t => t.Decimal, f => f.Sbyte },
                        { t => t.Double, f => f.Sbyte },
                        { t => t.Uint, f => f.Byte },
                        { t => t.NInt, f => f.Sbyte },
                        { t => t.NDecimal, f => f.Sbyte },
                        { t => t.NDouble, f => f.Sbyte },
                        { t => t.NUint, f => f.Byte },
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithImplicitConversions()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithImplicitConversions>(MapperTypes.None);
            var from = new ConvertibleFrom
            {
                Byte = 1,
                Sbyte = -1,
            };
            var actual = mapper.Map(from);
            Assert.IsNotNull(actual);
            Assert.AreEqual(-1, actual.Int);
            Assert.AreEqual(-1m, actual.Decimal);
            Assert.AreEqual(-1d, actual.Double);
            Assert.AreEqual(1u, actual.Uint);
            Assert.AreEqual(-1, actual.NInt);
            Assert.AreEqual(-1m, actual.NDecimal);
            Assert.AreEqual(-1d, actual.NDouble);
            Assert.AreEqual(1u, actual.NUint);
        }


        private class SameTypeMapper : Mapper<SimpleTo, SimpleTo>
        {
            public SameTypeMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, SimpleTo> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, SimpleTo>
                    {
                        { s => s.Id, s => s.Id, i => i + 1 },
                    };
                }
            }
        }

        [TestMethod]
        public void TestSameTypeMapper()
        {
            var mapper = AbstractMapperFactory.Create<SameTypeMapper>(MapperTypes.None);
            var from = new SimpleTo { Id = 1 };
            var result = mapper.Map(from);
            Assert.AreNotSame(from, result);
            Assert.AreEqual(2, result.Id);
        }


        private class WithInternalPropMapper : Mapper<From, WithInternalProp>
        {
            public WithInternalPropMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<WithInternalProp, From> MapRules
            {
                get
                {
                    return new MapRules<WithInternalProp, From>
                    {
                        { t => t.Internal, f => f.Id },
                    };
                }
            }
        }

        [TestMethod]
        public void TestInternalPropertyMapping()
        {
            var mapper = AbstractMapperFactory.Create<WithInternalPropMapper>(MapperTypes.None, Entities.None);
            var from = new From { Id = 1 };
            var result = mapper.Map(from);
            Assert.AreEqual(1, result.Internal);
        }


        private class ClassWithPropertyWithoutSetter
        {
            public int Id { get; set; }
            public int Idd { get { return Id + 1; } }
        }

        private class IgnorePropertyWithoutSetterMapper : Mapper<From, ClassWithPropertyWithoutSetter>
        {
            public IgnorePropertyWithoutSetterMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IIgnoreRules<ClassWithPropertyWithoutSetter> IgnoreRules
            {
                get
                {
                    return new IgnoreRules<ClassWithPropertyWithoutSetter>
                    {
                        c => c.Idd,
                    };
                }
            }
        }

        [TestMethod]
        public void TestIgnoredPropertyWithoutSetter()
        {
            var mapper = AbstractMapperFactory.Create<IgnorePropertyWithoutSetterMapper>(MapperTypes.None, Entities.None);
            var from = new From { Id = 1 };
            var result = mapper.Map(from);
            Assert.AreEqual(1, result.Id);
        }

    }

    public class WithInternalProp
    {
        internal int Internal { get; set; }
    }
}
