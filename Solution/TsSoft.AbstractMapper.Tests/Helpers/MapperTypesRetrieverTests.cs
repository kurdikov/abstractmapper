﻿namespace TsSoft.AbstractMapper.Tests.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Helpers;
    using TsSoft.Expressions.Helpers.Reflection;

    [TestClass]
    public class MapperTypesRetrieverTests
    {
        [TestMethod]
        public void TestRetrieve()
        {
            var mockHelper = new Mock<IMapperTypeHelper>(MockBehavior.Strict);
            mockHelper.Setup(h => h.IsAbstractMapperType(It.IsAny<Type>()))
                .Returns((Type t) => new[] {typeof(InstantiableMapper), typeof(NonInstantiableMapper)}.Contains(t));
            var retriever = new MapperTypesRetriever(mockHelper.Object);
            var mapperTypes = retriever.GetMapperTypes(GetType().GetTypeInfo().Assembly, new TestAssemblyTypesRetriever())
                .ToList();
            Assert.AreEqual(1, mapperTypes.Count);
            Assert.AreSame(typeof(InstantiableMapper), mapperTypes[0]);
        }

        private class TestAssemblyTypesRetriever : IAssemblyTypesRetriever
        {
            public IEnumerable<Type> GetTypes(Assembly assembly)
            {
                return new[]
                {
                    typeof(NotMapper),
                    typeof(NonInstantiableMapper),
                    typeof(InstantiableMapper),
                };
            }
        }

        private class NotMapper
        {
        }

        private abstract class NonInstantiableMapper : Mapper<From, To>
        {
            protected NonInstantiableMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }
        }

        private class InstantiableMapper : Mapper<From, To>
        {
            public InstantiableMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }
        }
    }
}
