﻿namespace TsSoft.AbstractMapper.Tests.Helpers
{
    using System.Reflection.Emit;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.AbstractMapper.Helpers;

    [TestClass]
    public class MapperTypeHelperTests
    {
        [TestMethod]
        public void TestIsAbstractMapperType()
        {
            var helper = new MapperTypeHelper();
            Assert.IsFalse(helper.IsAbstractMapperType(typeof(NotAbstractMapperType)));
            Assert.IsTrue(helper.IsAbstractMapperType(typeof(AbstractMapperType)));
            Assert.IsTrue(helper.IsAbstractMapperType(typeof(GenericByFromAbstractMapperType<>)));
            Assert.IsTrue(helper.IsAbstractMapperType(typeof(GenericByFromAbstractMapperType<From>)));
            Assert.IsTrue(helper.IsAbstractMapperType(typeof(GenericByToAbstractMapperType<>)));
            Assert.IsTrue(helper.IsAbstractMapperType(typeof(GenericByToAbstractMapperType<To>)));
            Assert.IsTrue(helper.IsAbstractMapperType(typeof(GenericMapper<,>)));
            Assert.IsTrue(helper.IsAbstractMapperType(typeof(GenericMapper<From, To>)));
            Assert.IsTrue(helper.IsAbstractMapperType(typeof(DerivedMapper)));
            Assert.IsTrue(helper.IsAbstractMapperType(typeof(DerivedGenericMapper)));
        }
        
        [TestMethod]
        public void TestIsAbstractMapperInstance()
        {
            var helper = new MapperTypeHelper();
            Assert.IsFalse(helper.IsAbstractMapperInstance(new NotAbstractMapperType()));
            Assert.IsTrue(helper.IsAbstractMapperInstance(AbstractMapperFactory.Create<AbstractMapperType>(MapperTypes.None)));
            Assert.IsTrue(helper.IsAbstractMapperInstance(AbstractMapperFactory.Create<GenericByFromAbstractMapperType<From>>(MapperTypes.None)));
            Assert.IsTrue(helper.IsAbstractMapperInstance(AbstractMapperFactory.Create<GenericByToAbstractMapperType<To>>(MapperTypes.None)));
            Assert.IsTrue(helper.IsAbstractMapperInstance(AbstractMapperFactory.Create<GenericMapper<From, To>>(MapperTypes.None)));
            Assert.IsTrue(helper.IsAbstractMapperInstance(AbstractMapperFactory.Create<DerivedMapper>(MapperTypes.None)));
            Assert.IsTrue(helper.IsAbstractMapperInstance(AbstractMapperFactory.Create<DerivedGenericMapper>(MapperTypes.None)));
        }

        [TestMethod]
        public void TestGetAbstractMapperBaseType()
        {
            var helper = new MapperTypeHelper();
            Assert.IsNull(helper.GetMapperBaseType(typeof(NotAbstractMapperType)));
            Assert.AreSame(typeof(Mapper<From, To>), helper.GetMapperBaseType(typeof(AbstractMapperType)));
            Assert.AreSame(typeof(Mapper<From, To>), helper.GetMapperBaseType(typeof(GenericByFromAbstractMapperType<From>)));
            Assert.AreSame(typeof(Mapper<From, To>), helper.GetMapperBaseType(typeof(GenericByToAbstractMapperType<To>)));
            Assert.AreSame(typeof(Mapper<From, To>), helper.GetMapperBaseType(typeof(GenericMapper<From, To>)));
            Assert.AreSame(typeof(Mapper<From, To>), helper.GetMapperBaseType(typeof(DerivedMapper)));
            Assert.AreSame(typeof(Mapper<From, To>), helper.GetMapperBaseType(typeof(DerivedGenericMapper)));
        }

        private class AbstractMapperType : Mapper<From, To>
        {
            public AbstractMapperType([NotNull] IAbstractMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }
        }

        private class NotAbstractMapperType : IMapper<From, To>
        {
            public To Map(From @from)
            {
                throw new System.NotImplementedException();
            }
        }

        private class GenericByFromAbstractMapperType<TFrom> : Mapper<TFrom, To>
            where TFrom: From
        {
            public GenericByFromAbstractMapperType([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }
        }

        private class GenericByToAbstractMapperType<TTo> : Mapper<From, TTo>
            where TTo: To, new()
        {
            public GenericByToAbstractMapperType([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }
        }

        private class GenericMapper<TFrom, TTo> : Mapper<TFrom, TTo> where TFrom : class where TTo : new()
        {
            public GenericMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }
        }

        private class DerivedMapper : AbstractMapperType
        {
            public DerivedMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }
        }

        private class DerivedGenericMapper : GenericMapper<From, To>
        {
            public DerivedGenericMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }
        }
    }
}
