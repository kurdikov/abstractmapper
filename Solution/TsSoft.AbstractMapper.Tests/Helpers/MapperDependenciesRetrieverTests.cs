﻿namespace TsSoft.AbstractMapper.Tests.Helpers
{
    using System.Collections.Generic;
    using System.Linq;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Engine;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.AbstractMapper.Helpers;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Models.AbstractMapper;

    [TestClass]
    public class MapperDependenciesRetrieverTests
    {
        [TestMethod]
        public void WithoutMapperDependencies()
        {
            var mockExpressionCreator = new Mock<IMapExpressionCreator>(MockBehavior.Strict);
            mockExpressionCreator.Setup(
                mec => mec.GetExpression<From, To>(
                    TestMapper.TestMapRules,
                    false,
                    null))
                .Returns(
                    new MapperExpression<From, To>
                    {
                        MappedPaths = new List<MappedPathDescription>
                        {
                            new MappedPathDescription {},
                            new MappedPathDescription {},
                        }
                    });
            var retriever = new MapperDependenciesRetriever(mockExpressionCreator.Object);
            var dependencies = retriever.GetDependencies(AbstractMapperFactory.Create<TestMapper>(MapperTypes.None))
                .ToList();
            Assert.AreEqual(0, dependencies.Count);
        }

        [TestMethod]
        public void WithOneDependency()
        {
            var mockExpressionCreator = new Mock<IMapExpressionCreator>(MockBehavior.Strict);
            mockExpressionCreator.Setup(
                mec => mec.GetExpression<From, To>(
                    TestMapper.TestMapRules,
                    false,
                    null))
                .Returns(
                    new MapperExpression<From, To>
                    {
                        MappedPaths = new List<MappedPathDescription>
                        {
                            new MappedPathDescription {},
                            new MappedPathDescription {MapperDescription = new MapperDescription(typeof(To), typeof(To), typeof(From))},
                        }
                    });
            var retriever = new MapperDependenciesRetriever(mockExpressionCreator.Object);
            var dependencies = retriever.GetDependencies(AbstractMapperFactory.Create<TestMapper>(MapperTypes.None))
                .ToList();
            Assert.AreEqual(1, dependencies.Count);
            Assert.AreSame(typeof(To), dependencies.First().Key);
            Assert.AreSame(typeof(From), dependencies.First().Value);
        }

        private class TestMapper : Mapper<From, To>
        {
            public TestMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            public static FullMapRules TestMapRules = new FullMapRules(typeof(To));
            public static IgnoreRules<To> TestIgnoreRules = new IgnoreRules<To>();
            public static AutoPropertiesBehavior TestAutoPropertiesBehavior = (AutoPropertiesBehavior)(0-1);
            public static InnerMapperStrategy TestInnerMapperStrategy = (InnerMapperStrategy)(0 - 1);

            protected internal override IFullMapRules FullMapRules
            {
                get { return TestMapRules; }
            }

            protected override IIgnoreRules<To> IgnoreRules
            {
                get { return TestIgnoreRules; }
            }

            protected override AutoPropertiesBehavior AutoPropertiesBehavior
            {
                get { return TestAutoPropertiesBehavior; }
            }

            protected override InnerMapperStrategy InnerMapperStrategy
            {
                get { return TestInnerMapperStrategy; }
            }
        }
    }
}
