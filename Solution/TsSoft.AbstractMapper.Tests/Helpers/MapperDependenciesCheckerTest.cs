﻿namespace TsSoft.AbstractMapper.Tests.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.AbstractMapper.Helpers;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.Expressions.Helpers.Extensions;

    [TestClass]
    public class MapperDependenciesCheckerTest
    {
        private Mock<IMapperDependenciesRetriever> _mockDependenciesRetriever;
        private Mock<IMapperResolver> _mockMapperResolver;
        private IMapperTypeHelper _typeHelper;
        private Mock<IAbstractMapperHelper> _mockMapperHelper;

        private MapperDependenciesChecker _checker;

        private static MethodInfo _setupMockResolver;

        [ClassInitialize]
        public static void InitOnce(TestContext context)
        {
            _setupMockResolver =
                StaticHelpers.MemberInfo.GetGenericDefinitionMethodInfo((MapperDependenciesCheckerTest test) => test.SetupResolver<object, object>(null));
        }

        [TestInitialize]
        public void Init()
        {
            _mockDependenciesRetriever = new Mock<IMapperDependenciesRetriever>(MockBehavior.Strict);
            _mockMapperResolver = new Mock<IMapperResolver>(MockBehavior.Strict);
            _typeHelper = new MapperTypeHelper();
            _mockMapperHelper = new Mock<IAbstractMapperHelper>(MockBehavior.Strict);


            _checker = new MapperDependenciesChecker(
                _mockDependenciesRetriever.Object,
                _mockMapperResolver.Object,
                StaticHelpers.MemberInfo,
                _typeHelper);
        }

        void SetupResolver<TFrom, TTo>(IMapper<TFrom, TTo> obj)
        {
            if (obj != null)
            {
                _mockMapperResolver.Setup(mr => mr.TryGet<TFrom, TTo>())
                    .Returns(obj);
            }
            else
            {
                _mockMapperResolver.Setup(mr => mr.TryGet<TFrom, TTo>())
                    .Throws(new TestResolverException(typeof(TFrom), typeof(TTo)));
            }
        }
        
        Mapper<TFrom, TTo> GetTestMapper<TFrom, TTo>(params IMapperDependency[] dependencies)
            where TFrom: class 
            where TTo: class, new()
        {
            var mapper = new Mapper<TFrom, TTo>(_mockMapperHelper.Object);
            _mockDependenciesRetriever.Setup(dr => dr.GetDependencies(mapper))
                .Returns(dependencies.Select(d => new KeyValuePair<Type, Type>(d.From, d.To)));
            foreach (var dependency in dependencies)
            {
                if (!dependency.Self)
                {
                    _setupMockResolver.MakeGenericMethod(dependency.From, dependency.To)
                        .Invoke(this, new[] { dependency.Mapper });
                }
                else
                {
                    SetupResolver(mapper);
                }
            }
            return mapper;
        }

        Mapper<TFrom, TTo> GetInvalidMapper<TFrom, TTo>()
            where TFrom : class
            where TTo : class, new()
        {
            var mapper = new Mapper<TFrom, TTo>(_mockMapperHelper.Object);
            _mockDependenciesRetriever.Setup(dr => dr.GetDependencies(mapper))
                .Throws<TestMapperException<TFrom, TTo>>();
            return mapper;
        }

        MapperDependency<TFrom, TTo> Dependency<TFrom, TTo>(IMapper<TFrom, TTo> satisfaction)
        {
            return new MapperDependency<TFrom, TTo> {Mapper = satisfaction};
        }

        MapperDependency<TFrom, TTo> Dependency<TFrom, TTo>()
        {
            return new MapperDependency<TFrom, TTo> { Mapper = new ManualMapper<TFrom, TTo>() };
        }

        MapperDependency<TFrom, TTo> UnsatisfiedDependency<TFrom, TTo>()
        {
            return new MapperDependency<TFrom, TTo>();
        }

        [TestMethod]
        public void MapperWithoutDependencies()
        {
            var mapper = GetTestMapper<From, To>();
            var result = _checker.GetUnresolved<TestResolverException>(mapper.ToEnumerable())
                .ToList();
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void MapperWithOneSatisfiedNonAbstractDependency()
        {
            var mapper = GetTestMapper<From, To>(Dependency<From, SimpleTo>());
            var result = _checker.GetUnresolved<TestResolverException>(mapper.ToEnumerable())
                .ToList();
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void MapperWithOneSatisfiedAbstractDependency()
        {
            var mapper = GetTestMapper<From, To>(Dependency(GetTestMapper<From, SimpleTo>()));
            var result = _checker.GetUnresolved<TestResolverException>(mapper.ToEnumerable())
                .ToList();
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void MapperWithSelfDependency()
        {
            var mapper = GetTestMapper<From, To>(new MapperDependency<From, To> {Self = true});
            var result = _checker.GetUnresolved<TestResolverException>(mapper.ToEnumerable())
                .ToList();
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void MapperWithUnsatisfiedSelfDependency()
        {
            var mapper = GetTestMapper<From, To>(UnsatisfiedDependency<From, To>());
            var result = _checker.GetUnresolved<TestResolverException>(mapper.ToEnumerable())
                .ToList();
            Assert.AreEqual(1, result.Count);
            Assert.AreSame(mapper.GetType(), result[0].MapperType);
            Assert.AreSame(typeof(From), result[0].Exception.As<TestResolverException>().From);
            Assert.AreSame(typeof(To), result[0].Exception.As<TestResolverException>().To);
        }

        [TestMethod]
        public void MapperWithBadRules()
        {
            var mapper = GetInvalidMapper<From, To>();
            var result = _checker.GetUnresolved<TestResolverException>(mapper.ToEnumerable())
                .ToList();
            Assert.AreEqual(1, result.Count);
            Assert.AreSame(mapper.GetType(), result[0].MapperType);
            Assert.AreSame(typeof(TestMapperException<From, To>), result[0].Exception.GetType());
        }

        [TestMethod]
        public void MapperWithSatisfiedSecondLevelDependencies()
        {
            var mapper = GetTestMapper<From, To>(
                Dependency(GetTestMapper<From, SimpleTo>(
                    Dependency(GetTestMapper<From, NotSoSimpleTo>()),
                    Dependency<From, ThreePropertiesTo>())));
            var result = _checker.GetUnresolved<TestResolverException>(mapper.ToEnumerable())
                .ToList();
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void MapperWithUnsatisfiedSecondLevelDependencies()
        {
            var mapper = GetTestMapper<From, To>(
                Dependency(GetTestMapper<From, SimpleTo>(
                    UnsatisfiedDependency<From, NotSoSimpleTo>(),
                    UnsatisfiedDependency<From, ThreePropertiesTo>())));
            var result = _checker.GetUnresolved<TestResolverException>(mapper.ToEnumerable())
                .ToList();
            Assert.AreEqual(2, result.Count);
            Assert.AreSame(typeof(Mapper<From, SimpleTo>), result[0].MapperType);
            Assert.AreSame(typeof(From), result[0].Exception.As<TestResolverException>().From);
            Assert.AreSame(typeof(NotSoSimpleTo), result[0].Exception.As<TestResolverException>().To);
            Assert.AreSame(typeof(Mapper<From, SimpleTo>), result[1].MapperType);
            Assert.AreSame(typeof(From), result[1].Exception.As<TestResolverException>().From);
            Assert.AreSame(typeof(ThreePropertiesTo), result[1].Exception.As<TestResolverException>().To);
        }

        [TestMethod]
        public void MapperWithDependencyWithBadRules()
        {
            var mapper = GetTestMapper<From, To>(
                Dependency(GetInvalidMapper<From, SimpleTo>()));
            var result = _checker.GetUnresolved<TestResolverException>(mapper.ToEnumerable())
                .ToList();
            Assert.AreEqual(1, result.Count);
            Assert.AreSame(typeof(Mapper<From, SimpleTo>), result[0].MapperType);
            Assert.AreSame(typeof(TestMapperException<From, SimpleTo>), result[0].Exception.GetType());
        }

        private class TestResolverException : Exception
        {
            public Type From { get; set; }
            public Type To { get; set; }

            public TestResolverException(Type from, Type to)
            {
                From = @from;
                To = to;
            }
        }

        private class TestMapperException<TFrom, TTo> : Exception
        {
        }

        private class ManualMapper<TFrom, TTo> : IMapper<TFrom, TTo>
        {
            public TTo Map(TFrom @from)
            {
                throw new NotImplementedException();
            }
        }

        private class TestMapper : Mapper<From, To>
        {
            public TestMapper([NotNull] IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }
        }

        private interface IMapperDependency
        {
            [NotNull]
            Type From { get; }
            [NotNull]
            Type To { get; }
            bool Self { get; }
            [CanBeNull]
            object Mapper { get;}
        }

        private class MapperDependency<TFrom, TTo> : IMapperDependency
        {
            public IMapper<TFrom, TTo> Mapper { get; set; }

            public Type From { get { return typeof(TFrom); } }
            public Type To { get { return typeof(TTo); } }

            public bool Self { get; set; }

            object IMapperDependency.Mapper
            {
                get { return Mapper; }
            }
        }
    }
}
