﻿namespace TsSoft.AbstractMapper.Tests
{
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.AbstractMapper.Mappers;

    [TestClass]
    public class DynamicEntityMapperTests
    {
        [TestMethod]
        public void TestDynamicEntityMappingFromAnonymousClass()
        {
            var anon = new {Prop1 = 1, Prop2 = "2"};
            var mapper = AbstractMapperDependenciesFactory.GetDependency<IDynamicEntityToEntityMapper<TestEntity>>(
                    MockObjects.None, Entities.Are(typeof(TestEntity)), External.None);
            var result = mapper.Map(anon);
            Assert.AreEqual(1, result.Prop1);
            Assert.AreEqual("2", result.Prop2);
            Assert.IsNull(result.Prop3);
        }
    }
}
