﻿namespace TsSoft.AbstractMapper.Rules.Factories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Metadata;

    // TODO rename tests semantically
    [TestClass]
    public class AttributeIgnoreRuleFactoryTests
    {
        [TestMethod]
        public void CreateRulesTest()
        {
            var factory = new AttributeIgnoreRulesFactory();
            var rules = factory.CreateRules<IgnoreClass>().ToArray();
            Assert.IsNotNull(rules);
            Assert.AreEqual(2, rules.Length);
            Expression<Func<IgnoreClass, object>> expression = x => x.A;
            ExprAssert.AreEqual(rules[0], expression);
            Expression<Func<IgnoreClass, object>> expression1 = x => x.B;
            ExprAssert.AreEqual(rules[1], expression1);
        }

        [TestMethod]
        public void CreateRulesNotIgnoreTest()
        {
            var factory = new AttributeIgnoreRulesFactory();
            var rules = factory.CreateRules<NotIgnoreClass>().ToArray();
            Assert.IsNotNull(rules);
            Assert.AreEqual(0, rules.Length);
        }

        [TestMethod]
        public void CreateRulesFromTest()
        {
            var factory = new AttributeIgnoreRulesFactory();
            var rules = factory.CreateRules<To, From>().ToArray();
            Assert.IsNotNull(rules);
            Assert.AreEqual(2, rules.Length);
            Expression<Func<To, object>> expression = x => x.A;
            ExprAssert.AreEqual(rules[0], expression);

            expression = x => x.B;
            ExprAssert.AreEqual(rules[1], expression);
        }

        [TestMethod]
        public void CreateRulesFrom2Test()
        {
            var factory = new AttributeIgnoreRulesFactory();
            var rules = factory.CreateRules<To, From2>().ToArray();
            Assert.IsNotNull(rules);
            Assert.AreEqual(1, rules.Length);
            Expression<Func<To, object>> expression = x => x.A;
            ExprAssert.AreEqual(rules[0], expression);
        }

        [TestMethod]
        public void CreateRulesUsersTest()
        {
            var factory = new AttributeIgnoreRulesFactory();
            var rules = factory.CreateRules<UserLogicModel, User>().ToArray();
            Assert.IsNotNull(rules);
            Assert.AreEqual(2, rules.Length);
            Expression<Func<UserLogicModel, object>> expression = x => x.Roles;

            ExprAssert.AreEqual(rules[0], expression);
            
            expression = x => x.Claims;
            ExprAssert.AreEqual(rules[1], expression);

        }

        public class From
        {
        }

        public class From2
        {
        }

        public class IgnoreClass
        {
            [MapIgnore]
            public string A { get; set; }

            [MapIgnore]
            public int B { get; set; }

            public Guid C { get; set; }
        }

        public class NotIgnoreClass
        {
            public string A { get; set; }

            public int B { get; set; }

            public Guid C { get; set; }
        }

        public class To
        {
            [MapIgnore(typeof (From), typeof (From2))]
            public string A { get; set; }

            [MapIgnore(typeof (From))]
            public int B { get; set; }
        }

        public class UserLogicModel
        {
            [MapIgnore(typeof(User))]
            public IEnumerable<string> Roles { get; set; }

            [MapIgnore(typeof(User))]
            public IEnumerable<string> Claims { get; set; }
        }

        public class User
        {
          
        }
    }
}