﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Выражение для представления класса
    /// </summary>
    public class TypeExpression : Expression
    {
        /// <summary>
        /// Выражение для представления класса
        /// </summary>
        public TypeExpression([NotNull]Type type)
        {
            if (type == null) throw new ArgumentNullException("type");
            TypeObj = type;
        }

        /// <summary>
        /// Тип узла
        /// </summary>
        public override ExpressionType NodeType
        {
            get { return ExpressionType.Extension; }
        }

        /// <summary>
        /// Тип выражения
        /// </summary>
        public override Type Type
        {
            get { return typeof(TypeExpression); }
        }

        /// <summary>
        /// Класс
        /// </summary>
        [NotNull]
        public Type TypeObj { get; set; }
    }
}
