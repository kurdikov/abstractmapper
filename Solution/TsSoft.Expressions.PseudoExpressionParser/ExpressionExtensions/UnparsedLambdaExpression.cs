﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using ParserGenerator;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator;

    /// <summary>
    /// Заглушка для лямбды, охватывающий контекст которой ещё не определён
    /// </summary>
    public class UnparsedLambdaExpression : Expression
    {
        /// <summary>
        /// Тип узла
        /// </summary>
        public override ExpressionType NodeType
        {
            get { return ExpressionType.Extension; }
        }

        /// <summary>
        /// Тип выражения
        /// </summary>
        public override Type Type
        {
            get { return typeof(UnparsedLambdaExpression); }
        }

        /// <summary>
        /// Имена параметров лямбды
        /// </summary>
        [NotNull]
        public IReadOnlyList<UnparsedLambdaParameter> Parameters { get; set; }

        /// <summary>
        /// Контекст лямбды
        /// </summary>
        [NotNull]
        public PseudoExpressionParser.ExpressionContext Context { get; set; }

        [NotNull]
        internal PseudoExpressionVisitor Visitor { get; set; }

        /// <summary>
        /// Параметр лямбды
        /// </summary>
        public struct UnparsedLambdaParameter
        {
            /// <summary>
            /// Имя параметра
            /// </summary>
            public string Name { get; private set; }
            /// <summary>
            /// Тип параметра (если указан)
            /// </summary>
            public Type Type { get; private set; }

            /// <summary>
            /// Параметр лямбды
            /// </summary>
            public UnparsedLambdaParameter([NotNull] string name, Type type) : this()
            {
                if (name == null) throw new ArgumentNullException("name");
                Name = name;
                Type = type;
            }
        }
    }
}
