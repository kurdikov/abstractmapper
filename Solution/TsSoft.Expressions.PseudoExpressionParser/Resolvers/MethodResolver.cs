﻿// ReSharper disable ForCanBeConvertedToForeach
// ReSharper disable LoopCanBeConvertedToQuery
namespace TsSoft.Expressions.PseudoExpressionParser.Resolvers
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions;
    
    /// <summary>
    /// Получает метод типа по имени и типам передаваемых аргументов
    /// </summary>
    public class MethodResolver : IMethodResolver
    {
        [NotNull] private readonly IDelegateTypeHelper _delegateTypeHelper;
        [NotNull] private readonly ILambdaBodyMaker _lambdaBodyMaker;
        [NotNull] private readonly IConversionChecker _conversionChecker;

        /// <summary>
        /// Получает метод типа по имени и типам передаваемых аргументов
        /// </summary>
        public MethodResolver(
            [NotNull] IDelegateTypeHelper delegateTypeHelper,
            [NotNull] ILambdaBodyMaker lambdaBodyMaker,
            [NotNull] IConversionChecker conversionChecker)
        {
            if (delegateTypeHelper == null) throw new ArgumentNullException("delegateTypeHelper");
            if (lambdaBodyMaker == null) throw new ArgumentNullException("lambdaBodyMaker");
            if (conversionChecker == null) throw new ArgumentNullException("conversionChecker");
            _delegateTypeHelper = delegateTypeHelper;
            _lambdaBodyMaker = lambdaBodyMaker;
            _conversionChecker = conversionChecker;
        }

        /// <summary>
        /// Попытаться получить метод
        /// </summary>
        /// <param name="instance">Тип объекта, метод которого ищется</param>
        /// <param name="methodName">Имя метода</param>
        /// <param name="arguments">Аргументы</param>
        /// <param name="typeArguments">Типы-параметры генерик-метода</param>
        /// <param name="candidates">Кандидаты при неоднозначности</param>
        /// <returns>Найденный метод или null</returns>
        public MethodInfo TryResolveInstance(Type instance, string methodName, Expression[] arguments, Type[] typeArguments, out IReadOnlyCollection<MethodInfo> candidates)
        {
            var methods = instance.GetTypeInfo().GetMember(
                methodName,
                MemberTypes.Method,
                BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
            var methodWithArguments = TryResolve(methods, arguments, typeArguments, out candidates);
            return ReplaceArguments(methodWithArguments, arguments);
        }

        /// <summary>
        /// Попытаться получить статический метод или метод расширения
        /// </summary>
        /// <param name="extensionType">Тип со статическими методами</param>
        /// <param name="methodName">Имя метода</param>
        /// <param name="arguments">Аргументы, включая this-аргумент для метода расширения</param>
        /// <param name="typeArguments">Типы-параметры генерик-метода</param>
        /// <param name="candidates">Кандидаты при неоднозначности</param>
        /// <returns>Найденный метод или null</returns>
        public MethodInfo TryResolveStatic(Type extensionType, string methodName, Expression[] arguments, Type[] typeArguments, out IReadOnlyCollection<MethodInfo> candidates)
        {
            var methods = extensionType.GetTypeInfo().GetMember(methodName, MemberTypes.Method, BindingFlags.Public | BindingFlags.Static);
            var methodWithArguments = TryResolve(methods, arguments, typeArguments, out candidates);
            return ReplaceArguments(methodWithArguments, arguments);
        }

        /// <summary>
        /// Попытаться получить статический метод или метод расширения
        /// </summary>
        /// <param name="extensionTypes">Типы со статическими методами</param>
        /// <param name="methodName">Имя метода</param>
        /// <param name="arguments">Аргументы, включая this-аргумент для метода расширения</param>
        /// <param name="typeArguments">Типы-параметры генерик-метода</param>
        /// <param name="candidates">Кандидаты при неоднозначности</param>
        /// <returns>Найденный метод или null</returns>
        public MethodInfo TryResolveStatic(IEnumerable<Type> extensionTypes, string methodName, Expression[] arguments, Type[] typeArguments, out IReadOnlyCollection<MethodInfo> candidates)
        {
            var methods = extensionTypes
                .Where(t => t != null)
                .SelectMany(t => t.GetTypeInfo().GetMember(methodName, MemberTypes.Method, BindingFlags.Public | BindingFlags.Static));
            var methodWithArguments = TryResolve(methods, arguments, typeArguments, out candidates);
            return ReplaceArguments(methodWithArguments, arguments);
        }

        private MethodInfo ReplaceArguments(MethodDescription methodDescription, [NotNull]Expression[] arguments)
        {
            if (methodDescription == null)
            {
                return null;
            }
            for (int i = 0; i < arguments.Length; ++i)
            {
                arguments[i] = methodDescription.Arguments[i].ThrowIfNull().GetArgument();
            }
            return methodDescription.Method;
        }

        internal struct LambdaWithParameterTypes : IEquatable<LambdaWithParameterTypes>
        {
            [NotNull]public UnparsedLambdaExpression Expression { get; private set; }
            [NotNull]public Type[] ParameterTypes { get; private set; }

            public LambdaWithParameterTypes([NotNull] UnparsedLambdaExpression expression, [NotNull] Type[] parameterTypes) : this()
            {
                if (expression == null) throw new ArgumentNullException("expression");
                if (parameterTypes == null) throw new ArgumentNullException("parameterTypes");
                Expression = expression;
                ParameterTypes = parameterTypes;
            }

            public bool Equals(LambdaWithParameterTypes other)
            {
                return Expression == other.Expression && TypeArrayComparer.Instance.Equals(ParameterTypes, other.ParameterTypes);
            }

            public override int GetHashCode()
            {
                return Expression.GetHashCode() * 397 ^ TypeArrayComparer.Instance.GetHashCode(ParameterTypes);
            }
        }

        private MethodDescription TryResolve(
            [NotNull]IEnumerable<MemberInfo> methods,
            [NotNull]Expression[] arguments,
            [CanBeNull]Type[] typeArguments,
            out IReadOnlyCollection<MethodInfo> candidateMethods)
        {
            var innerLambdaParseVariants = new ConcurrentDictionary<LambdaWithParameterTypes, LambdaDescription>();

            // 7.6.5.1
            // The set of candidate methods for the method invocation is constructed.
            var candidates = new HashSet<MethodDescription>();
            var candidateDeclaringTypes = new HashSet<Type>();
            var candidateCheckingCtx = new CandidateCheckingContext();
            foreach (var method in methods.OfType<MethodInfo>())
            {
                MethodDescription methodDescription;
                if (method != null && (methodDescription = IsCandidate(method, arguments, typeArguments, innerLambdaParseVariants, candidateCheckingCtx)) != null)
                {
                    candidates.Add(methodDescription);
                    if (method.DeclaringType != null)
                    {
                        candidateDeclaringTypes.Add(method.DeclaringType);
                    }
                }
            }

            // The set of candidate methods is reduced to contain only methods from the most derived types
            if (candidateDeclaringTypes.Count > 1)
            {
                foreach (var type in candidateDeclaringTypes)
                {
                    candidates.RemoveWhere(t => t == null
                        || t.Method.DeclaringType != null && t.Method.DeclaringType != type && t.Method.DeclaringType.GetTypeInfo().IsAssignableFrom(type));
                }
            }

            // If the resulting set of candidate methods is empty, then further processing along the following steps are abandoned
            // The best method of the set of candidate methods is identified using the overload resolution rules
            return GetBestMethod(candidates, arguments, out candidateMethods);
        }

        internal class LambdaDescription
        {
            [NotNull]
            public Expression Body { get; private set; }

            [NotNull]
            public ParameterExpression[] Parameters { get; private set; }

            public LambdaDescription([NotNull] Expression body, [NotNull] ParameterExpression[] parameters)
            {
                if (body == null) throw new ArgumentNullException("body");
                if (parameters == null) throw new ArgumentNullException("parameters");
                Body = body;
                Parameters = parameters;
            }
        }

        [NotNull]
        private MethodArgumentDescription[] GetMethodArgumentDescriptions(
            [NotNull] ParameterInfo[] parameters,
            [NotNull] Expression[] arguments)
        {
            var argumentDescriptions = new MethodArgumentDescription[parameters.Length];
            for (int i = 0; i < argumentDescriptions.Length; ++i)
            {
                var argument = arguments[i].ThrowIfNull();
                var parameter = parameters[i].ThrowIfNull();
                var isDelegate = _delegateTypeHelper.IsDelegateType(parameter.ParameterType);
                var isExpression = parameter.ParameterType.GetTypeInfo().IsGenericType
                    && parameter.ParameterType.GetGenericTypeDefinition() == typeof(Expression<>);
                var delegateType = isDelegate
                    ? parameter.ParameterType
                    : isExpression ? parameter.ParameterType.GetTypeInfo().GetGenericArguments()[0] : null;
                var delegateDescription = delegateType != null
                    ? _delegateTypeHelper.GetDelegateDescription(delegateType)
                    : null;
                argumentDescriptions[i] = new MethodArgumentDescription(
                    argument,
                    parameter,
                    isDelegate,
                    isExpression,
                    delegateDescription);
            }
            return argumentDescriptions;
        }

        private class CandidateCheckingContext
        {
            public Type EnumerableType { get; set; }

            public Type LambdaArgumentOutputType { get; set; }
        }

        [NotNull]
        private static readonly HashSet<Type> EnumerableNumericSelectorOutputTypes = new HashSet<Type>
        {
            typeof(decimal),
            typeof(decimal?),
            typeof(double),
            typeof(double?),
            typeof(float),
            typeof(float?),
            typeof(int),
            typeof(int?),
            typeof(long),
            typeof(long?),
        };

        private Type GetReturnType(
            [NotNull]Expression delegateArgument,
            [NotNull]ConcurrentDictionary<LambdaWithParameterTypes, LambdaDescription> innerLambdaDescriptions,
            [NotNull]Type[] lambdaParameterTypes)
        {
            Type outputType;
            var lambdaArgument = delegateArgument as UnparsedLambdaExpression;
            if (lambdaArgument == null)
            {
                outputType = _delegateTypeHelper.IsDelegateType(delegateArgument.Type)
                    ? _delegateTypeHelper.GetDelegateDescription(delegateArgument.Type).ReturnType
                    : null;
            }
            else
            {
                var lambda = innerLambdaDescriptions.GetOrAdd(
                    new LambdaWithParameterTypes(lambdaArgument, lambdaParameterTypes),
                    TryMakeLambdaBody);
                outputType = lambda != null ? lambda.Body.Type : null;
            }
            return outputType;
        }

        private bool CheckEnumerableMethodWithNumericSelector(
            [NotNull] MethodInfo method,
            [NotNull] CandidateCheckingContext context,
            [NotNull] ParameterInfo[] parameters,
            [NotNull] Expression[] arguments,
            [NotNull] ConcurrentDictionary<LambdaWithParameterTypes, LambdaDescription> innerLambdaDescriptions,
            ref Type[] typeArguments,
            ref Type[] typeParameters,
            ref MethodArgumentDescription[] argumentDescriptions)
        {
            // these methods have one or two generic parameters
            // and the first one is determined the same way for all of the overloads
            // so here we make sure we infer types only once for all these overloads
            Type[] lambdaArgumentParameterTypes;
            if (typeArguments == null)
            {
                typeParameters = method.GetGenericArguments();
                if (context.EnumerableType == null)
                {
                    argumentDescriptions = GetMethodArgumentDescriptions(parameters, arguments);
                    typeArguments = TryInferTypes(typeParameters, argumentDescriptions, innerLambdaDescriptions);
                    if (typeArguments == null || typeArguments.Length != typeParameters.Length)
                    {
                        return false;
                    }
                    lambdaArgumentParameterTypes = typeParameters.Length == 1 ? typeArguments : new[] { typeArguments[0] };
                    context.EnumerableType = typeArguments[0];
                }
                else
                {
                    lambdaArgumentParameterTypes = new[] { context.EnumerableType };
                    if (typeParameters.Length == 1)
                    {
                        typeArguments = lambdaArgumentParameterTypes;
                    }
                }
            }
            else
            {
                if (typeArguments.Length < 1)
                {
                    return false;
                }
                lambdaArgumentParameterTypes = new[] { typeArguments[0] };
            }

            context.LambdaArgumentOutputType = context.LambdaArgumentOutputType
                ?? GetReturnType(arguments[1].ThrowIfNull(), innerLambdaDescriptions, lambdaArgumentParameterTypes);

            if (context.LambdaArgumentOutputType == null)
            {
                return false;
            }

            if (EnumerableNumericSelectorOutputTypes.Contains(context.LambdaArgumentOutputType))
            {
                var currentMethodParameterReturnType = argumentDescriptions != null
                    ? argumentDescriptions[1].ThrowIfNull().DelegateParameterDescription.ThrowIfNull().ReturnType
                    : _delegateTypeHelper.GetDelegateDescription(parameters[1].ThrowIfNull().ParameterType).ReturnType;
                if (currentMethodParameterReturnType != context.LambdaArgumentOutputType)
                {
                    return false;
                }
            }

            return true;
        }

        private bool IsEnumerableMethodWithNumericSelector([NotNull] MethodInfo method, [NotNull]ParameterInfo[] parameters)
        {
            return parameters.Length == 2
                   && method.IsGenericMethodDefinition
                   && method.DeclaringType == typeof(Enumerable)
                   && (method.Name == "Sum" || method.Name == "Average" || method.Name == "Min" || method.Name == "Max");
        }

        private MethodDescription IsCandidate(
            [NotNull]MethodInfo method,
            [NotNull]Expression[] arguments,
            Type[] typeArguments,
            [NotNull]ConcurrentDictionary<LambdaWithParameterTypes, LambdaDescription> innerLambdaDescriptions,
            [NotNull]CandidateCheckingContext context)
        {
            var parameters = method.GetParameters();
            if (parameters.Length != arguments.Length)
            {
                return null;
            }

            Type[] typeParameters = null;
            MethodArgumentDescription[] argumentDescriptions = null;

            // shortcut for known methods having an immense amount of overloads
            // with delegate parameters having non-generic output:
            // Enumerable.Max, Enumerable.Min, Enumerable.Average, Enumerable.Sum
            if (IsEnumerableMethodWithNumericSelector(method, parameters))
            {
                // these methods are terrible for resolving because they are all generic and they are all candidates by c# specification
                // so all bounding work and generic method creating is done for each method
                // here we select only one candidate depending on return type the lambda argument has
                // all others are rejected immediately as we see the return type does not fit
                // this speeds up parsing of expressions with such methods significantly
                if (!CheckEnumerableMethodWithNumericSelector(
                    method, context, parameters, arguments, innerLambdaDescriptions,
                    ref typeArguments,
                    ref typeParameters,
                    ref argumentDescriptions))
                {
                    return null;
                }
            }

            argumentDescriptions = argumentDescriptions ?? GetMethodArgumentDescriptions(parameters, arguments);

            // If F is non-generic, F is a candidate when:
            // M has no type argument list, and
            // F is applicable with respect to A
            if (!method.IsGenericMethodDefinition)
            {
                return typeArguments == null || typeArguments.Length == 0
                    ? IsApplicable(method, parameters, argumentDescriptions, innerLambdaDescriptions)
                    : null;
            }

            typeParameters = typeParameters ?? method.GetGenericArguments();

            // If F is generic and M has no type argument list, F is a candidate when:
            // Type inference (§7.5.2) succeeds and
            // Once the inferred type arguments are substituted for the corresponding method type parameters,
            // all constructed types in the parameter list of F satisfy their constraints (§4.4.4)
            // and the parameter list of F is applicable with respect to A (§7.5.3.1).
            if (typeArguments == null)
            {
                typeArguments = TryInferTypes(typeParameters, argumentDescriptions, innerLambdaDescriptions);
            }
            // If F is generic and M includes a type argument list, F is a candidate when:
            // F has the same number of method type parameters as were supplied in the type argument list, and
            // Once the type arguments are substituted for the corresponding method type parameters,
            // all constructed types in the parameter list of F satisfy their constraints (§4.4.4),
            // and the parameter list of F is applicable with respect to A (§7.5.3.1).
            if (typeArguments == null || typeParameters.Length != typeArguments.Length)
            {
                return null;
            }
            try
            {
                method = method.MakeGenericMethod(typeArguments);
            }
            catch (ArgumentException)    // generic constraint violated
            {
                return null;
            }
            var genericDescriptions = GetMethodArgumentDescriptions(method.GetParameters(), arguments);
            return IsApplicable(method, parameters, genericDescriptions, innerLambdaDescriptions);
        }

        private MethodDescription IsApplicable(
            [NotNull]MethodInfo method,
            [NotNull]ParameterInfo[] originalParameters,
            [NotNull]MethodArgumentDescription[] descriptions,
            [NotNull]ConcurrentDictionary<LambdaWithParameterTypes, LambdaDescription> innerLambdaDescriptions)
        {
            // 7.5.3.1 Applicable function member
            // A function member is said to be an applicable function member with respect to an argument list A when all of the following are true:
            // Each argument in A corresponds to a parameter in the function member declaration as described in §7.5.1.1,
            // and any parameter to which no argument corresponds is an optional parameter.

            // For each argument in A, the parameter passing mode of the argument (i.e., value, ref, or out) is identical to the parameter passing mode of the corresponding parameter

            // for a value parameter or a parameter array, an implicit conversion (§6.1) exists from the argument to the type of the corresponding parameter, or
            // for a ref or out parameter, the type of the argument is identical to the type of the corresponding parameter.
            // After all, a ref or out parameter is an alias for the argument passed.
            var result = new MethodDescription(method, originalParameters, new ResolvedArgumentDescription[descriptions.Length]);
            for (int i = 0; i < descriptions.Length; ++i)
            {
                var description = descriptions[i].ThrowIfNull();
                var argument = description.Argument;
                var lambdaArgument = argument as UnparsedLambdaExpression;
                if (lambdaArgument != null && description.DelegateParameterDescription != null)
                {
                    var lambdaDescription = innerLambdaDescriptions.GetOrAdd(
                        new LambdaWithParameterTypes(lambdaArgument, description.DelegateParameterDescription.ParameterTypes),
                        TryMakeLambdaBody);
                    if (lambdaDescription == null)
                    {
                        return null;
                    }
                    if (!_conversionChecker.IsImplicitlyConvertible(
                        lambdaDescription.Body.Type, description.DelegateParameterDescription.ReturnType))
                    {
                        return null;
                    }
                    result.Arguments[i] = new ResolvedLambdaArgumentDescription(
                        description.Parameter.ParameterType,
                        lambdaDescription,
                        description.DelegateParameterDescription,
                        description.IsExpressionParameter);
                }
                else if (!description.Parameter.ParameterType.IsByRef)
                {
                    if (!FirstImplicitlyConvertibleToSecond(argument.Type, description.Parameter.ParameterType))
                    {
                        return null;
                    }
                    result.Arguments[i] = new ResolvedRegularArgumentDescription(description.Parameter.ParameterType, argument);
                }
                else
                {
                    if (argument.Type != description.Parameter.ParameterType)
                    {
                        return null;
                    }
                    result.Arguments[i] = new ResolvedRegularArgumentDescription(description.Parameter.ParameterType, argument);
                }
            }

            return result;        // param arrays are not supported

            // For a function member that includes a parameter array, if the function member is applicable by the above rules,
            // it is said to be applicable in its normal form.
            // If a function member that includes a parameter array is not applicable in its normal form, the function member may instead be applicable in its expanded form:
            // The expanded form is constructed by replacing the parameter array in the function member declaration with zero or more value parameters of the element type of the parameter array such that the number of arguments in the argument list A matches the total number of parameters. If A has fewer arguments than the number of fixed parameters in the function member declaration, the expanded form of the function member cannot be constructed and is thus not applicable.
            // Otherwise, the expanded form is applicable if for each argument in A 
            // the parameter passing mode of the argument is identical to the parameter passing mode of the corresponding parameter, and
            // for a fixed value parameter or a value parameter created by the expansion, an implicit conversion (§6.1) exists from the type of the argument to the type of the corresponding parameter, or
            // for a ref or out parameter, the type of the argument is identical to the type of the corresponding parameter.
        }


        private Type[] TryInferTypes(
            [NotNull]Type[] typeParameters,
            [NotNull]MethodArgumentDescription[] descriptions,
            [NotNull]ConcurrentDictionary<LambdaWithParameterTypes, LambdaDescription> lambdaDescriptions)
        {
            // 7.5.2
            // X - type parameter
            // S - type argument
            // E - argument
            // x - parameter

            var states = typeParameters.ToDictionary(p => p, p => new TypeArgumentInferenceState());

            // 7.5.2.1
            // For each of the method arguments Ei:
            for (int i = 0; i < descriptions.Length; ++i)
            {
                var description = descriptions[i].ThrowIfNull();
                var parameter = description.Parameter;
                var argument = description.Argument;

                var lambdaArgument = argument as UnparsedLambdaExpression;
                if (lambdaArgument != null)
                {
                    if (description.DelegateParameterDescription == null
                        || description.DelegateParameterDescription.ParameterTypes.Length != lambdaArgument.Parameters.Count)
                    {
                        return null;
                    }
                    // If Ei is an anonymous function, an explicit parameter type inference (§7.5.2.7) is made from Ei to Ti
                    MakeExplicitParameterInference(lambdaArgument, description.DelegateParameterDescription, states);
                }
                else if (!parameter.ParameterType.IsByRef)
                {
                    // Otherwise, if Ei has a type U and xi is a value parameter then a lower-bound inference is made from U to Ti.
                    MakeLowerBoundInference(argument.Type, parameter.ParameterType, states);
                }
                else
                {
                    // Otherwise, if Ei has a type U and xi is a ref or out parameter then an exact inference is made from U to Ti. 
                    // Otherwise, no inference is made for this argument.
                    MakeExactInference(argument.Type, parameter.ParameterType, states);
                }
            }

            // 7.5.2.2 The second phase
            // The second phase proceeds as follows:
            for (int j = 0; j < typeParameters.Length; ++j) // on each iteration a type parameter is fixed, so the number of iterations cannot exceed the number of parameters
            {
                // All unfixed type variables Xi which do not depend on (§7.5.2.5) any Xj are fixed (§7.5.2.10).
                // If no such type variables exist, all unfixed type variables Xi are fixed for which all of the following hold:
                //      There is at least one type variable Xj that depends on Xi
                //      Xi has a non-empty set of bounds
                bool fixingSuccessful;
                bool @fixed = FixIndependent(typeParameters, states, descriptions, out fixingSuccessful)
                    || FixWithDependenciesAndBounds(typeParameters, states, descriptions, out fixingSuccessful);

                if (!fixingSuccessful)
                {
                    return null;
                }

                // If no such type variables exist and there are still unfixed type variables, type inference fails. 
                // Otherwise, if no further unfixed type variables exist, type inference succeeds.
                if (states.Values.All(s => s.ThrowIfNull().IsFixed))
                {
                    var result = new Type[typeParameters.Length];
                    for (int i = 0; i < result.Length; ++i)
                    {
                        result[i] = states.GetState(typeParameters[i]).FixedTo;
                    }
                    return result;
                }
                if (!@fixed)
                {
                    return null;
                }
                // Otherwise, for all arguments Ei with corresponding parameter type Ti where the output types (§7.5.2.4) contain unfixed type variables Xj
                // but the input types (§7.5.2.3) do not, an output type inference (§7.5.2.6) is made from Ei to Ti.
                for (int i = 0; i < descriptions.Length; ++i)
                {
                    var description = descriptions[i].ThrowIfNull();
                    if (description.DelegateParameterDescription != null
                        && states.Any(s => !s.Value.ThrowIfNull().IsFixed && FirstOccursInSecond(s.Key.ThrowIfNull(), description.DelegateParameterDescription.ReturnType))
                        && !states.Any(s => !s.Value.ThrowIfNull().IsFixed && description.DelegateParameterDescription.ParameterTypes.Any(
                            pt => FirstOccursInSecond(s.Key.ThrowIfNull(), pt.ThrowIfNull()))))
                    {
                        MakeOutputTypeInference(description, description.Argument, description.Parameter.ParameterType, description.DelegateParameterDescription, states, lambdaDescriptions);
                    }
                }
                // Then the second phase is repeated.
            }
            return null;
        }

        private void MakeOutputTypeInference(
            [NotNull]MethodArgumentDescription argumentDescription,
            [NotNull]Expression e,
            [NotNull]Type t,
            [NotNull]DelegateTypeDescription tDesc,
            [NotNull]IDictionary<Type, TypeArgumentInferenceState> states,
            [NotNull]ConcurrentDictionary<LambdaWithParameterTypes, LambdaDescription> lambdaDescriptions)
        {
            // 7.5.2.6 Output type inferences
            // An output type inference is made from an expression E to a type T in the following way:
            // If E is an anonymous function with inferred return type  U (§7.5.2.12)
            // and T is a delegate type or expression tree type with return type Tb, then a lower-bound inference (§7.5.2.9) is made from U to Tb.
            var lambdaStubE = e as UnparsedLambdaExpression;
            if (lambdaStubE != null && argumentDescription.DelegateParameterDescription != null)
            {
                var returnType = InferReturnType(lambdaStubE, argumentDescription.DelegateParameterDescription, states, lambdaDescriptions);
                if (returnType != null)
                {
                    MakeLowerBoundInference(returnType, tDesc.ReturnType, states);
                }
            }
            // Otherwise, if E is a method group and T is a delegate type or expression tree type with parameter types T1…Tk and return type Tb,
            // and overload resolution of E with the types T1…Tk yields a single method with return type U, then a lower-bound inference is made from U to Tb.
            // -- METHOD GROUPS NOT SUPPORTED
            // Otherwise, if E is an expression with type U, then a lower-bound inference is made from U to T.
            else
            {
                MakeLowerBoundInference(e.Type, t, states);
            }
            // Otherwise, no inferences are made
        }

        private LambdaDescription TryMakeLambdaBody(
            LambdaWithParameterTypes lambdaWithParameterTypes)
        {
            Expression lambdaBody;
            ParameterExpression[] parameters;
            if (!_lambdaBodyMaker.TryMakeLambdaBody(lambdaWithParameterTypes.Expression, lambdaWithParameterTypes.ParameterTypes, out lambdaBody, out parameters))
            {
                return null;
            }
            return new LambdaDescription(lambdaBody, parameters);
        }

        private Type InferReturnType(
            [NotNull]UnparsedLambdaExpression expression,
            [NotNull]DelegateTypeDescription parameterDescription,
            [NotNull]IDictionary<Type, TypeArgumentInferenceState> states,
            [NotNull]ConcurrentDictionary<LambdaWithParameterTypes, LambdaDescription> lambdaDescriptions)
        {
            var argumentTypes = new Type[expression.Parameters.Count];
            for (int i = 0; i < argumentTypes.Length; ++i)
            {
                argumentTypes[i] = expression.Parameters[i].Type
                    ?? ReplaceGenericParameters(parameterDescription.ParameterTypes[i].ThrowIfNull(), states);
            }
            var parsed = lambdaDescriptions.GetOrAdd(
                new LambdaWithParameterTypes(expression, argumentTypes), 
                TryMakeLambdaBody);
            return parsed != null ? parsed.Body.Type : null;
        }

        [NotNull]
        private Type GetFixation(
            [NotNull] Type genericTypeParameter,
            [NotNull] IDictionary<Type, TypeArgumentInferenceState> states)
        {
            TypeArgumentInferenceState state;
            if (states.TryGetValue(genericTypeParameter, out state) && state != null)
            {
                if (state.FixedTo == null)
                {
                    throw new InvalidOperationException(string.Format("{0} is not yet fixed", genericTypeParameter));
                }
                return state.FixedTo;
            }
            throw new InvalidOperationException(string.Format("{0} is not a generic parameter", genericTypeParameter));
        }

        [NotNull]
        private Type ReplaceGenericParameters([NotNull]Type parameterType, [NotNull]IDictionary<Type, TypeArgumentInferenceState> states)
        {
            if (parameterType.IsGenericParameter)
            {
                return GetFixation(parameterType, states);
            }
            if (!parameterType.GetTypeInfo().IsGenericType)
            {
                return parameterType;
            }
            var arguments = parameterType.GetTypeInfo().GetGenericArguments();
            var replacedArguments = new Type[arguments.Length];
            for (int i = 0; i < arguments.Length; ++i)
            {
                replacedArguments[i] = ReplaceGenericParameters(arguments[i].ThrowIfNull(), states);
            }
            var definition = parameterType.GetGenericTypeDefinition().ThrowIfNull();
            return definition.MakeGenericType(replacedArguments);
        }

        [NotNull]
        private static readonly IReadOnlyCollection<Type> GenericCollectionInterfaces = new[]
        {typeof(IEnumerable<>), typeof(ICollection<>), typeof(IList<>)};

        private Type GetUniqueBaseTypeWithGenericDefinition(
            [NotNull] Type specimen,
            [NotNull] Type genericDefinition)
        {
            if (genericDefinition.GetTypeInfo().IsInterface)
            {
                return specimen.GetTypeInfo().GetInterfaces().AppendOne(specimen)
                    .Where(i => i != null && i.GetTypeInfo().IsGenericType && i.GetGenericTypeDefinition() == genericDefinition)
                    .FirstIfSingleOtherwiseDefault();
            }
            var current = specimen;
            while (current != typeof(object) && current != null)
            {
                if (current.GetTypeInfo().IsGenericType && current.GetGenericTypeDefinition() == genericDefinition)
                {
                    return current;
                }
                current = current.GetTypeInfo().BaseType;
            }
            return null;
        }

        private void MakeLowerBoundInference(
            [NotNull]Type u,
            [NotNull]Type v,
            [NotNull]IDictionary<Type, TypeArgumentInferenceState> states)
        {
            TypeArgumentInferenceState state;
            Type[] uelements = null;
            Type[] velements = null;
            Type[] vgenericTypeParams = null;
            Type ubase;
            // 7.5.2.9 Lower-bound inferences
            // A lower-bound inference from a type U to a type V is made as follows:
            // If V is one of the unfixed Xi then U is added to the set of lower bounds for Xi.
            if (v.IsGenericParameter && states.TryGetValue(v, out state) && state != null)
            {
                state.LowerBounds.Add(u);
            }
            // Otherwise, if V is the type V1? and U is the type U1? then a lower bound inference is made from U1 to V1.
            else if (v.IsNullableStruct() && u.IsNullableStruct())
            {
                MakeLowerBoundInference(u.NullableStructUnderlyingType(), v.NullableStructUnderlyingType(), states);
            }
            // Otherwise, sets U1…Uk and V1…Vk are determined by checking if any of the following cases apply:
            //    V is an array type V1[…]and U is an array type U1[…] (or a type parameter whose effective base type is U1[…]) of the same rank
            else if (v.IsArray && u.IsArray && v.GetArrayRank() == u.GetArrayRank())
            {
                uelements = new[] {u.GetElementType()};
                velements = new[] {v.GetElementType()};
            }
            //    V is one of IEnumerable<V1>, ICollection<V1> or IList<V1> and U is a one-dimensional array type U1[](or a type parameter whose effective base type is U1[]) 
            else if (u.IsArray && u.GetArrayRank() == 1 && v.GetTypeInfo().IsGenericType
                     && GenericCollectionInterfaces.Contains(v.GetGenericTypeDefinition()))
            {
                uelements = new[] {u.GetElementType()};
                velements = new[] {v.GetGenericEnumerableArgument()};
            }
            //    V is a constructed class, struct, interface or delegate type C<V1…Vk> and there is a unique type C<U1…Uk> such that U 
            //    (or, if U is a type parameter, its effective base class or any member of its effective interface set)
            //    is identical to, inherits from (directly or indirectly), or implements (directly or indirectly) C<U1…Uk>.
            //    (The “uniqueness” restriction means that in the case interface C<T>{} class U: C<X>, C<Y>{}, 
            //    then no inference is made when inferring from U to C<T> because U1 could be X or Y.)
            else if (v.GetTypeInfo().IsGenericType && (ubase = GetUniqueBaseTypeWithGenericDefinition(u, v.GetGenericTypeDefinition().ThrowIfNull())) != null)
            {
                uelements = ubase.GetTypeInfo().GetGenericArguments();
                velements = v.GetTypeInfo().GetGenericArguments();
                vgenericTypeParams = v.GetGenericTypeDefinition().ThrowIfNull().GetTypeInfo().GetGenericArguments();
                if (uelements.Length != velements.Length)
                {
                    uelements = velements = null;
                }
            }
            // If any of these cases apply then an inference is made from each Ui to the corresponding Vi as follows:
            if (uelements != null)
            {
                for (int i = 0; i < uelements.Length; ++i)
                {
                    var uelement = uelements[i].ThrowIfNull();
                    var velement = velements[i].ThrowIfNull();
                    // If Ui is not known to be a reference type then an exact inference is made
                    if (uelement.GetTypeInfo().IsValueType)
                    {
                        MakeExactInference(uelement, velement, states);
                    }
                    // Otherwise, if U is an array type then a lower-bound inference is made
                    else if (u.IsArray)
                    {
                        MakeLowerBoundInference(uelement, velement, states);
                    }
                    // Otherwise, if V is C<V1…Vk> then inference depends on the i-th type parameter of C:
                    else if (vgenericTypeParams != null)
                    {
                        var attrs = vgenericTypeParams[i].ThrowIfNull().GetTypeInfo().GenericParameterAttributes;
                        // If it is covariant then a lower-bound inference is made.
                        if ((attrs & GenericParameterAttributes.Covariant) != 0)
                        {
                            MakeLowerBoundInference(uelement, velement, states);
                        }
                        // If it is contravariant then an upper-bound inference is made.
                        else if ((attrs & GenericParameterAttributes.Contravariant) != 0)
                        {
                            MakeUpperBoundInference(uelement, velement, states);
                        }
                        // If it is invariant then an exact inference is made.
                        else
                        {
                            MakeExactInference(uelement, velement, states);
                        }
                    }
                }
            }
            // Otherwise, no inferences are made.
        }

        private void MakeUpperBoundInference(
            [NotNull]Type u,
            [NotNull]Type v,
            [NotNull]IDictionary<Type, TypeArgumentInferenceState> states)
        {
            TypeArgumentInferenceState state;
            Type[] uelements = null;
            Type[] velements = null;
            Type[] ugenericTypeParams = null;
            Type vbase;
            // 7.5.2.10 Upper-bound inferences
            // An upper-bound inference from a type U to a type V is made as follows:
            // If V is one of the unfixed Xi then U is added to the set of upper bounds for Xi.
            if (v.IsGenericParameter && states.TryGetValue(v, out state) && state != null)
            {
                state.UpperBounds.Add(u);
            }
            // Otherwise, sets V1…Vk and U1…Uk are determined by checking if any of the following cases apply:
            //    U is an array type U1[…]and V is an array type V1[…]of the same rank
            else if (v.IsArray && u.IsArray && v.GetArrayRank() == u.GetArrayRank())
            {
                uelements = new[] { u.GetElementType() };
                velements = new[] { v.GetElementType() };
            }
            //    U is one of IEnumerable<Ue>, ICollection<Ue> or IList<Ue> and V is a one-dimensional array type Ve[]
            else if (v.IsArray && u.GetArrayRank() == 1 && u.GetTypeInfo().IsGenericType
                     && GenericCollectionInterfaces.Contains(u.GetGenericTypeDefinition()))
            {
                uelements = new[] { u.GetGenericEnumerableArgument() };
                velements = new[] { v.GetElementType() };
            }
            //    U is the type U1? and V is the type V1?
            else if (v.IsNullableStruct() && u.IsNullableStruct())
            {
                uelements = new[] {u.NullableStructUnderlyingType()};
                velements = new[] {v.NullableStructUnderlyingType()};
            }
            //    U is constructed class, struct, interface or delegate type C<U1…Uk>
            //    and V is a class, struct, interface or delegate type which is identical to, inherits from (directly or indirectly),
            //    or implements (directly or indirectly) a unique type C<V1…Vk>
            //    (The “uniqueness” restriction means that if we have interface C<T>{} class V<Z>: C<X<Z>>, C<Y<Z>>{}, 
            //    then no inference is made when inferring from C<U1> to V<Q>. Inferences are not made from U1 to either X<Q> or Y<Q>.)
            else if (v.GetTypeInfo().IsGenericType && (vbase = GetUniqueBaseTypeWithGenericDefinition(u, v.GetGenericTypeDefinition().ThrowIfNull())) != null)
            {
                uelements = u.GetTypeInfo().GetGenericArguments();
                velements = vbase.GetTypeInfo().GetGenericArguments();
                ugenericTypeParams = u.GetGenericTypeDefinition().ThrowIfNull().GetTypeInfo().GetGenericArguments();
                if (uelements.Length != velements.Length)
                {
                    uelements = velements = null;
                }
            }
            // If any of these cases apply then an inference is made from each Ui to the corresponding Vi as follows:
            if (uelements != null)
            {
                for (int i = 0; i < uelements.Length; ++i)
                {
                    var uelement = uelements[i].ThrowIfNull();
                    var velement = velements[i].ThrowIfNull();
                    // If Ui is not known to be a reference type then an exact inference is made
                    if (uelement.GetTypeInfo().IsValueType)
                    {
                        MakeExactInference(uelement, velement, states);
                    }
                    // Otherwise, if V is an array type then an upper-bound inference is made
                    else if (v.IsArray)
                    {
                        MakeUpperBoundInference(uelement, velement, states);
                    }
                    // Otherwise, if U is C<U1…Uk> then inference depends on the i-th type parameter of C:
                    else if (ugenericTypeParams != null)
                    {
                        var attrs = ugenericTypeParams[i].ThrowIfNull().GetTypeInfo().GenericParameterAttributes;
                        // If it is covariant then an upper-bound inference is made.
                        if ((attrs & GenericParameterAttributes.Covariant) != 0)
                        {
                            MakeUpperBoundInference(uelement, velement, states);
                        }
                        // If it is contravariant then a lower-bound inference is made.
                        else if ((attrs & GenericParameterAttributes.Contravariant) != 0)
                        {
                            MakeLowerBoundInference(uelement, velement, states);
                        }
                        // If it is invariant then an exact inference is made.
                        else
                        {
                            MakeExactInference(uelement, velement, states);
                        }
                    }
                }
            }
            // Otherwise, no inferences are made.
        }

        private void MakeExactInference(
            [NotNull]Type u,
            [NotNull]Type v,
            [NotNull]IDictionary<Type, TypeArgumentInferenceState> states)
        {
            TypeArgumentInferenceState state;
            // 7.5.2.8 Exact inferences
            // An exact inference from a type U to a type V is made as follows:
            // If V is one of the unfixed Xi then U is added to the set of exact bounds for Xi.
            if (v.IsGenericParameter && states.TryGetValue(v, out state) && state != null)
            {
                state.ExactBounds.Add(u);
            }
            // Otherwise, sets V1…Vk and U1…Uk are determined by checking if any of the following cases apply:
            //       V is an array type V1[…] and U is an array type U1[…]  of the same rank
            else if (u.IsArray && v.IsArray && u.GetArrayRank() == v.GetArrayRank())
            {
                MakeExactInference(u.GetElementType().ThrowIfNull(), v.GetElementType().ThrowIfNull(), states);
            }
            //       V is the type V1? and U is the type U1?
            else if (u.IsNullableStruct() && v.IsNullableStruct())
            {
                MakeExactInference(u.NullableStructUnderlyingType(), v.NullableStructUnderlyingType(), states);
            }
            //       V is a constructed type C<V1…Vk> and U is a constructed type C<U1…Uk> 
            else if (u.GetTypeInfo().IsGenericType && v.GetTypeInfo().IsGenericType)
            {
                var up = u.GetTypeInfo().GetGenericArguments();
                var vp = v.GetTypeInfo().GetGenericArguments();
                if (up.Length == vp.Length)
                {
                    for (int i = 0; i < up.Length; ++i)
                    {
                        MakeExactInference(up[i].ThrowIfNull(), vp[i].ThrowIfNull(), states);
                    }
                }
            }
            // If any of these cases apply then an exact inference is made from each Ui to the corresponding Vi.
            // Otherwise no inferences are made.
        }

        private void MakeExplicitParameterInference(
            [NotNull]UnparsedLambdaExpression lambdaArgument,
            [NotNull]DelegateTypeDescription parameterDescription,
            [NotNull]IDictionary<Type, TypeArgumentInferenceState> states)
        {
            // 7.5.2.7 Explicit parameter type inferences
            // An explicit parameter type inference is made from an expression E to a type T in the following way:
            // If E is an explicitly typed anonymous function with parameter types U1…Uk and T is a delegate type or expression tree type with parameter types V1…Vk 
            // then for each Ui an exact inference (§7.5.2.8) is made from Ui to the corresponding Vi.
            if (lambdaArgument.Parameters.Count != parameterDescription.ParameterTypes.Length)
            {
                return;
            }
            for (int i = 0; i < parameterDescription.ParameterTypes.Length; ++i)
            {
                if (lambdaArgument.Parameters[i].Type != null)
                {
                    MakeExactInference(lambdaArgument.Parameters[i].Type, parameterDescription.ParameterTypes[i].ThrowIfNull(), states);
                }
            }
        }

        private bool FixIndependent(
            [NotNull]Type[] typeParameters,
            [NotNull]IDictionary<Type, TypeArgumentInferenceState> states,
            [NotNull]MethodArgumentDescription[] descriptions,
            out bool fixingSuccessful)
        {
            // All unfixed type variables Xi which do not depend on (§7.5.2.5) any Xj are fixed (§7.5.2.10).
            var result = false;
            fixingSuccessful = true;
            for (int i = 0; i < typeParameters.Length; ++i)
            {
                var typeParameter = typeParameters[i].ThrowIfNull();
                var state = states.GetState(typeParameter);
                if (!state.IsFixed
                    && states.All(s => s.Key == typeParameter || !FirstDependsDirectlyOnSecond(typeParameter, s.Key.ThrowIfNull(), descriptions)))
                {
                    fixingSuccessful = Fix(state);
                    if (!fixingSuccessful)
                    {
                        return true;
                    }
                    result = true;
                }
            }
            return result;
        }

        private bool Fix([NotNull]TypeArgumentInferenceState state)
        {
            // 7.5.2.11 Fixing
            // An unfixed type variable Xi with a set of bounds is fixed as follows:
            //   The set of candidate types Uj starts out as the set of all types in the set of bounds for Xi.
            //   We then examine each bound for Xi in turn: For each exact bound U of Xi all types Uj which are not identical to U are removed from the candidate set.
            //   For each lower bound U of Xi all types Uj to which there is not an implicit conversion from U are removed from the candidate set.
            //   For each upper bound U of Xi all types Uj from which there is not an implicit conversion to U are removed from the candidate set.
            //   If among the remaining candidate types Uj there is a unique type V from which there is an implicit conversion to all the other candidate types, then Xi is fixed to V.
            //   Otherwise, type inference fails.
            var candidates = new HashSet<Type>();
            candidates.UnionWith(state.ExactBounds);
            candidates.UnionWith(state.LowerBounds);
            candidates.UnionWith(state.UpperBounds);
            foreach (var exactBound in state.ExactBounds)
            {
                candidates.RemoveWhere(uj => uj != exactBound);
            }
            foreach (var lowerBound in state.LowerBounds)
            {
                candidates.RemoveWhere(uj => !FirstImplicitlyConvertibleToSecond(lowerBound, uj));
            }
            foreach (var upperBound in state.UpperBounds)
            {
                candidates.RemoveWhere(uj => !FirstImplicitlyConvertibleToSecond(uj, upperBound));
            }
            if (candidates.Count != 1)
            {
                return false;
            }
            state.FixedTo = candidates.First();
            return true;
        }

        private bool FirstImplicitlyConvertibleToSecond(
            Type first,
            Type second)
        {
            return first != null && second != null && _conversionChecker.IsImplicitlyConvertible(first, second);
        }

        private bool FixWithDependenciesAndBounds(
            [NotNull]Type[] typeParameters,
            [NotNull]IDictionary<Type, TypeArgumentInferenceState> states,
            [NotNull]MethodArgumentDescription[] descriptions,
            out bool fixingSuccessful)
        {
            // all unfixed type variables Xi are fixed for which all of the following hold:
            //      There is at least one type variable Xj that depends on Xi
            //      Xi has a non-empty set of bounds
            var result = false;
            fixingSuccessful = true;
            for (int i = 0; i < typeParameters.Length; ++i)
            {
                var typeParameter = typeParameters[i].ThrowIfNull();
                var state = states.GetState(typeParameter);
                if (!state.IsFixed && state.HasBounds
                    && states.Any(s => s.Key != typeParameter || FirstDependsDirectlyOnSecond(s.Key.ThrowIfNull(), typeParameter, descriptions)))
                {
                    fixingSuccessful = Fix(state);
                    if (!fixingSuccessful)
                    {
                        return true;
                    }
                    result = true;
                }
            }
            return result;
        }

        private bool FirstDependsDirectlyOnSecond(
            [NotNull]Type first,
            [NotNull]Type second,
            [NotNull]MethodArgumentDescription[] descriptions)
        {
            for (int i = 0; i < descriptions.Length; ++i)
            {
                var description = descriptions[i].ThrowIfNull();
                if (!(description.Argument is UnparsedLambdaExpression) || description.DelegateParameterDescription == null)
                {
                    continue;
                }
                if (description.DelegateParameterDescription.ParameterTypes.Any(pt => FirstOccursInSecond(second, pt.ThrowIfNull()))
                    && FirstOccursInSecond(first, description.DelegateParameterDescription.ReturnType))
                {
                    return true;
                }
            }
            return false;
        }

        private bool FirstOccursInSecond(
            [NotNull]Type first,
            [NotNull]Type second)
        {
            if (first == second)
            {
                return true;
            }
            var secondInfo = second.GetTypeInfo();
            if (!secondInfo.IsGenericType)
            {
                return false;
            }
            var genericArguments = secondInfo.GetGenericArguments();
            return genericArguments.Any(ga => FirstOccursInSecond(first, ga.ThrowIfNull()));
        }

        internal class TypeArgumentInferenceState
        {
            public bool IsFixed { get { return FixedTo != null; } }

            public Type FixedTo { get; set; }

            [NotNull]
            public HashSet<Type> LowerBounds { get; private set; }

            [NotNull]
            public HashSet<Type> UpperBounds { get; private set; }

            [NotNull]
            public HashSet<Type> ExactBounds { get; private set; }

            public bool HasBounds { get { return LowerBounds.Count > 0 || UpperBounds.Count > 0 || ExactBounds.Count > 0; } }

            public TypeArgumentInferenceState()
            {
                LowerBounds = new HashSet<Type>();
                UpperBounds = new HashSet<Type>();
                ExactBounds = new HashSet<Type>();
            }
        }

        internal class MethodArgumentDescription
        {
            [NotNull]
            public Expression Argument { get; private set; }

            [NotNull]
            public ParameterInfo Parameter { get; private set; }

            public bool IsDelegateParameter { get; private set; }

            public bool IsExpressionParameter { get; private set; }

            public MaterializedDelegateTypeDescription DelegateParameterDescription { get; private set; }

            public MethodArgumentDescription(
                [NotNull] Expression argument,
                [NotNull] ParameterInfo parameter,
                bool isDelegate,
                bool isExpression,
                MaterializedDelegateTypeDescription delegateParameterDescription)
            {
                if (argument == null) throw new ArgumentNullException("argument");
                if (parameter == null) throw new ArgumentNullException("parameter");
                Argument = argument;
                Parameter = parameter;
                IsDelegateParameter = isDelegate;
                IsExpressionParameter = isExpression;
                DelegateParameterDescription = delegateParameterDescription;
            }
        }

        internal class MethodDescription
        {
            [NotNull]
            public MethodInfo Method { get; private set; }

            [NotNull]
            public ParameterInfo[] OriginalParameters { get; private set; }

            [NotNull]
            public ResolvedArgumentDescription[] Arguments { get; private set; }

            public MethodDescription([NotNull] MethodInfo method, [NotNull] ParameterInfo[] originalParameters, [NotNull] ResolvedArgumentDescription[] arguments)
            {
                if (method == null) throw new ArgumentNullException("method");
                if (originalParameters == null) throw new ArgumentNullException("originalParameters");
                if (arguments == null) throw new ArgumentNullException("arguments");
                Method = method;
                OriginalParameters = originalParameters;
                Arguments = arguments;
            }
        }

        internal abstract class ResolvedArgumentDescription
        {
            [NotNull] 
            public Type ArgumentType { get; private set; }

            protected ResolvedArgumentDescription([NotNull] Type argumentType)
            {
                if (argumentType == null) throw new ArgumentNullException("argumentType");
                ArgumentType = argumentType;
            }

            public abstract Expression GetArgument();

            [NotNull]
            protected Expression MakeConvertIfNeeded(
                [NotNull] Expression source,
                [NotNull] Type targetType)
            {
                Expression result;
                var targetTypeInfo = targetType.GetTypeInfo();
                if (targetTypeInfo.IsAssignableFrom(source.Type)
                    && !(source.Type.GetTypeInfo().IsValueType && !targetTypeInfo.IsValueType)
                    && !(targetType.IsNullableStruct() && source.Type != targetType))
                {
                    result = source;
                }
                else
                {
                    result = Expression.Convert(source, targetType);
                }
                return result;
            }
        }

        internal class ResolvedRegularArgumentDescription : ResolvedArgumentDescription
        {
            [NotNull]
            private readonly Expression _argument;

            public ResolvedRegularArgumentDescription([NotNull]Type argumentType, [NotNull]Expression argument) : base(argumentType)
            {
                if (argument == null) throw new ArgumentNullException("argument");
                _argument = argument;
            }

            public override Expression GetArgument()
            {
                return MakeConvertIfNeeded(_argument, ArgumentType);
            }
        }

        internal class ResolvedLambdaArgumentDescription : ResolvedArgumentDescription
        {
            [NotNull] private readonly LambdaDescription _lambdaDescription;
            private readonly bool _isQuoted;

            [NotNull]
            public MaterializedDelegateTypeDescription Description { get; private set; }

            [NotNull]
            public Type InferredReturnType { get { return _lambdaDescription.Body.Type; } }
            
            public ResolvedLambdaArgumentDescription(
                [NotNull] Type argumentType,
                [NotNull] LambdaDescription lambdaDescription,
                [NotNull] MaterializedDelegateTypeDescription delegateDescription,
                bool isQuoted)
                : base(argumentType)
            {
                if (lambdaDescription == null) throw new ArgumentNullException("lambdaDescription");
                if (delegateDescription == null) throw new ArgumentNullException("delegateDescription");
                _lambdaDescription = lambdaDescription;
                _isQuoted = isQuoted;
                Description = delegateDescription;
            }

            public override Expression GetArgument()
            {
                var lambdaBody = _lambdaDescription.Body;
                lambdaBody = MakeConvertIfNeeded(lambdaBody, Description.ReturnType);
                Expression argument = Expression.Lambda(Description.DelegateType, lambdaBody, _lambdaDescription.Parameters);
                if (_isQuoted)
                {
                    argument = Expression.Quote(argument);
                }
                return argument;
            }
        }

        private MethodDescription GetBestMethod(
            [NotNull]HashSet<MethodDescription> candidates,
            [NotNull]Expression[] originalArguments,
            out IReadOnlyCollection<MethodInfo> candidateMethods)
        {
            candidateMethods = null;
            if (candidates.Count <= 0)
            {
                return null;
            }
            if (candidates.Count == 1)
            {
                return candidates.First();
            }
            foreach (var candidate in candidates.ToList())
            {
                if (candidate == null)
                {
                    continue;
                }
                if (candidates.Any(potentiallyBetter => potentiallyBetter != null
                    && IsFirstBetterThanSecond(potentiallyBetter, candidate, originalArguments)))
                {
                    candidates.Remove(candidate);
                }
            }
            if (candidates.Count != 1)
            {
                candidateMethods = candidates.Count > 0 ? candidates.Where(c => c != null).Select(c => c.Method).ToList() : null;
                return null;
            }
            return candidates.First();
        }

        private bool IsFirstBetterThanSecond(
            [NotNull]MethodDescription first,
            [NotNull]MethodDescription second,
            [NotNull]Expression[] originalArguments)
        {
            if (first == second)
            {
                return false;
            }
            // 7.5.3.2 Better function member
            // Given an argument list A with a set of argument expressions { E1, E2, ..., EN }
            // and two applicable function members MP and MQ with parameter types { P1, P2, ..., PN } and { Q1, Q2, ..., QN }, 
            // MP is defined to be a better function member than MQ if
            // for each argument, the implicit conversion from EX to QX is not better than the implicit conversion from EX to PX, and
            // for at least one argument, the conversion from EX to PX is better than the conversion from EX to QX.
            bool firstHasBetterConversion = false;
            bool secondHasBetterConversion = false;
            for (int i = 0; i < originalArguments.Length; ++i)
            {
                var originalArgument = originalArguments[i].ThrowIfNull();
                var firstArgument = first.Arguments[i].ThrowIfNull();
                var secondArgument = second.Arguments[i].ThrowIfNull();
                if (firstArgument.ArgumentType == secondArgument.ArgumentType)
                {
                    continue;
                }
                var betterConversion = BetterConversion(originalArgument, firstArgument, secondArgument);
                if (betterConversion == BetterIs.First)
                {
                    firstHasBetterConversion = true;
                }
                else if (betterConversion == BetterIs.Second)
                {
                    secondHasBetterConversion = true;
                }
            }
            if (firstHasBetterConversion && !secondHasBetterConversion)
            {
                return true;
            }

            for (int i = 0; i < originalArguments.Length; ++i)
            {
                if (first.Arguments[i].ThrowIfNull().ArgumentType != second.Arguments[i].ThrowIfNull().ArgumentType)
                {
                    return false;
                }
            }


            // In case the parameter type sequences {P1, P2, …, PN} and {Q1, Q2, …, QN} are equivalent 
            // (i.e. each Pi has an identity conversion to the corresponding Qi),
            // the following tie-breaking rules are applied, in order, to determine the better function member. 
            // If MP is a non-generic method and MQ is a generic method, then MP is better than MQ.
            if (!first.Method.IsGenericMethod && second.Method.IsGenericMethod)
            {
                return true;
            }

            // Otherwise, if MP is applicable in its normal form and MQ has a params array and is applicable only in its expanded form, then MP is better than MQ.
            // Otherwise, if MP has more declared parameters than MQ, then MP is better than MQ. This can occur if both methods have params arrays and are applicable only in their expanded forms.
            // -- params calls are not supported
            // Otherwise if all parameters of MP have a corresponding argument whereas default arguments need to be substituted for at least one optional parameter in MQ then MP is better than MQ. 
            // -- optional arguments are not supported

            // Otherwise, if MP has more specific parameter types than MQ, then MP is better than MQ.
            // Let {R1, R2, …, RN} and {S1, S2, …, SN} represent the uninstantiated and unexpanded parameter types of MP and MQ.
            // MP’s parameter types are more specific than MQ’s if, for each parameter, RX is not less specific than SX, and, for at least one parameter, RX is more specific than SX
            var firstHasMoreSpecific = false;
            var secondHasMoreSpecific = false;
            for (int i = 0; i < first.OriginalParameters.Length; ++i)
            {
                var firstParam = first.OriginalParameters[i].ThrowIfNull().ParameterType;
                var secondParam = second.OriginalParameters[i].ThrowIfNull().ParameterType;
                var moreSpecific = MoreSpecific(firstParam, secondParam);
                if (moreSpecific == BetterIs.First)
                {
                    firstHasMoreSpecific = true;
                }
                else if (moreSpecific == BetterIs.Second)
                {
                    secondHasMoreSpecific = true;
                }
            }
            if (firstHasMoreSpecific && !secondHasMoreSpecific)
            {
                return true;
            }

            // Otherwise if one member is a non-lifted operator and  the other is a lifted operator, the non-lifted one is better.
            // -- operators aren't supported

            // Otherwise, neither function member is better.
            return false;
        }

        private enum BetterIs
        {
            None,
            First,
            Second,
        }

        private BetterIs MoreSpecific([NotNull]Type firstParam, [NotNull]Type secondParam)
        {
            // A type parameter is less specific than a non-type parameter.
            var firstIsGenericParam = firstParam.IsGenericParameter;
            var secondIsGenericParam = secondParam.IsGenericParameter;
            if (firstIsGenericParam && !secondIsGenericParam)
            {
                return BetterIs.Second;
            }
            if (!firstIsGenericParam && secondIsGenericParam)
            {
                return BetterIs.First;
            }
            var firstParamInfo = firstParam.GetTypeInfo();
            var secondParamInfo = secondParam.GetTypeInfo();
            // Recursively, a constructed type is more specific than another constructed type (with the same number of type arguments)
            //  if at least one type argument is more specific and no type argument is less specific than the corresponding type argument in the other.
            if (firstParamInfo.IsGenericType && secondParamInfo.IsGenericType)
            {
                var firstGenericParams = firstParamInfo.GetGenericArguments();
                var secondGenericParams = secondParamInfo.GetGenericArguments();
                if (firstGenericParams.Length != secondGenericParams.Length)
                {
                    return BetterIs.None;
                }
                var firstHasMoreSpecific = false;
                var secondHasMoreSpecific = false;
                for (int i = 0; i < firstGenericParams.Length; ++i)
                {
                    var firstGenericParam = firstGenericParams[i].ThrowIfNull(firstParam);
                    var secondGenericParam = secondGenericParams[i].ThrowIfNull(secondParam);
                    var moreSpecific = MoreSpecific(firstGenericParam, secondGenericParam);
                    if (moreSpecific == BetterIs.First)
                    {
                        firstHasMoreSpecific = true;
                    }
                    else if (moreSpecific == BetterIs.Second)
                    {
                        secondHasMoreSpecific = true;
                    }
                }
                if (firstHasMoreSpecific && !secondHasMoreSpecific)
                {
                    return BetterIs.First;
                }
                if (secondHasMoreSpecific && !firstHasMoreSpecific)
                {
                    return BetterIs.Second;
                }
                return BetterIs.None;
            }
            // An array type is more specific than another array type (with the same number of dimensions)
            //  if the element type of the first is more specific than the element type of the second.
            if (firstParam.IsArray && secondParam.IsArray && firstParam.GetArrayRank() == secondParam.GetArrayRank())
            {
                return MoreSpecific(firstParam.GetElementType().ThrowIfNull(firstParam), secondParam.GetElementType().ThrowIfNull(secondParam));
            }
            return BetterIs.None;
        }

        private BetterIs BetterConversion(
            [NotNull]Expression originalArgument,
            [NotNull]ResolvedArgumentDescription first,
            [NotNull]ResolvedArgumentDescription second)
        {
            // 7.5.3.3 Better conversion from expression
            // Given an implicit conversion C1 that converts from an expression E to a type T1,
            // and an implicit conversion C2 that converts from an expression E to a type T2,
            // C1 is a better conversion than C2 if at least one of the following holds:
            var originalLambdaArgument = originalArgument as UnparsedLambdaExpression;
            if (originalLambdaArgument == null)
            {
                // E has a type S and an identity conversion exists from S to T1 but not from S to T2
                // E is not an anonymous function and T1 is a better conversion target than T2 (§7.5.3.5)
                return BetterConversion(originalArgument.Type, first.ArgumentType, second.ArgumentType);
            }
            // E is an anonymous function,
            // T1 is either a delegate type D1 or an expression tree type Expression<D1>,
            // T2 is either a delegate type D2 or an expression tree type Expression<D2> and one of the following holds:
            // D1 is a better conversion target than D2
            // -- surely it never holds, is it?
            // D1 and D2 have identical parameter lists, and one of the following holds:
            var firstLambda = first as ResolvedLambdaArgumentDescription;
            var secondLambda = second as ResolvedLambdaArgumentDescription;
            if (firstLambda != null
                && secondLambda != null
                && firstLambda.Description.ParameterTypes.SequenceEqual(secondLambda.Description.ParameterTypes))
            {
                var inferredType = firstLambda.InferredReturnType;
                // D1 has a return type Y, and D2 is void returning
                if (firstLambda.Description.ReturnType != typeof(void)
                    && secondLambda.Description.ReturnType == typeof(void))
                {
                    return BetterIs.First;
                }
                if (firstLambda.Description.ReturnType == typeof(void)
                    && secondLambda.Description.ReturnType != typeof(void))
                {
                    return BetterIs.Second;
                }
                // D1 has a return type Y1, and D2 has a return type Y2, an inferred return type X exists for E in the context of that parameter list (§7.5.2.12),
                //  and the conversion from X to Y1 is better than the conversion from X to Y2
                return BetterConversion(
                    inferredType,
                    firstLambda.Description.ReturnType,
                    secondLambda.Description.ReturnType);
                // E is async, D1 has a return type Task<Y1>, and D2 has a return type Task<Y2>, an inferred return type Task<X> exists for E in the context of that parameter list (§7.5.2.12),
                //  and the conversion from X to Y1 is better than the conversion from X to Y2
                // -- async lambdas are not supported
            }

            return BetterIs.None;
        }

        private BetterIs BetterConversion(
            [NotNull]Type originalType,
            [NotNull]Type first,
            [NotNull]Type second)
        {
            // 7.5.3.4 Better conversion from type
            // Given a conversion C1 that converts from a type S to a type T1,
            // and a conversion C2 that converts from a type S to a type T2,
            // C1 is a better conversion than C2 if at least one of the following holds:
            // An identity conversion exists from S to T1 but not from S to T2
            if (originalType == first && originalType != second)
            {
                return BetterIs.First;
            }
            if (originalType == second && originalType != first)
            {
                return BetterIs.Second;
            }
            // T1 is a better conversion target than T2 (§7.5.3.5)
            // 7.5.3.5 Better conversion target
            // Given two different types T1 and T2, T1 is a better conversion target than T2 if at least one of the following holds:
            // An implicit conversion from T1 to T2 exists, and no implicit conversion from T2 to T1 exists
            var firstToSecond = _conversionChecker.IsImplicitlyConvertible(first, second);
            var secondToFirst = _conversionChecker.IsImplicitlyConvertible(second, first);
            if (firstToSecond && !secondToFirst)
            {
                return BetterIs.First;
            }
            if (secondToFirst && !firstToSecond)
            {
                return BetterIs.Second;
            }
            // T1 is a signed integral type and T2 is an unsigned integral type. Specifically:
            // T1 is sbyte and T2 is byte, ushort, uint, or ulong
            // T1 is short and T2 is ushort, uint, or ulong
            // T1 is int and T2 is uint, or ulong
            // T1 is long and T2 is ulong
            IReadOnlyList<Type> worse;
            if (SignedToLargerUnsigned.TryGetValue(first, out worse) && worse != null && worse.Contains(second))
            {
                return BetterIs.First;
            }
            if (SignedToLargerUnsigned.TryGetValue(second, out worse) && worse != null && worse.Contains(first))
            {
                return BetterIs.Second;
            }
            return BetterIs.None;
        }

        [NotNull]
        private readonly static IReadOnlyDictionary<Type, IReadOnlyList<Type>> SignedToLargerUnsigned = new Dictionary<Type, IReadOnlyList<Type>>
        {
            {typeof(sbyte), new List<Type> {typeof(byte), typeof(ushort), typeof(uint), typeof(ulong)}},
            {typeof(short), new List<Type> {typeof(ushort), typeof(uint), typeof(ulong)}},
            {typeof(int), new List<Type> {typeof(uint), typeof(ulong)}},
            {typeof(long), new List<Type> {typeof(ulong)}},
        };
    }

    internal static class InferenceStateExtensions
    {
        [NotNull]
        public static MethodResolver.TypeArgumentInferenceState GetState(
            [NotNull]this IDictionary<Type, MethodResolver.TypeArgumentInferenceState> states,
            Type typeArgument)
        {
            return states[typeArgument.ThrowIfNull()].ThrowIfNull();
        }
    }
}
// ReSharper restore ForCanBeConvertedToForeach
// ReSharper restore LoopCanBeConvertedToQuery
