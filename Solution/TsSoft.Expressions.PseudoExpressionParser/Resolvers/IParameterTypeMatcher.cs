﻿namespace TsSoft.Expressions.PseudoExpressionParser.Resolvers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Определяет, можно ли связать параметр с аргументом
    /// </summary>
    public interface IParameterTypeMatcher
    {
        /// <summary>
        /// Можно ли в качестве параметра типа parameterType передать argument
        /// </summary>
        /// <param name="parameterType">Тип параметра метода</param>
        /// <param name="argument">Передаваемый аргумент</param>
        /// <returns>Подходит ли аргумент</returns>
        bool IsMatch([NotNull]Type parameterType, [NotNull]ref Expression argument);

        /// <summary>
        /// Можно ли в качестве параметра типа parameterType передать в генерик-метод argument
        /// </summary>
        /// <param name="parameterType">Тип параметра метода</param>
        /// <param name="argument">Передаваемый аргумент</param>
        /// <param name="genericArgumentsBinding">Уже связанные генерик-параметры</param>
        /// <returns>Подходит ли аргумент</returns>
        bool IsGenericMatch([NotNull]Type parameterType, [NotNull]ref Expression argument, [NotNull]IDictionary<Type, Type> genericArgumentsBinding);
    }
}
