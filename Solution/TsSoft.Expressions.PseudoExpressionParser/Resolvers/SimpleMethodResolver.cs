﻿namespace TsSoft.Expressions.PseudoExpressionParser.Resolvers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Получает метод типа по имени и типам передаваемых аргументов
    /// </summary>
    public class SimpleMethodResolver : IMethodResolver
    {
        [NotNull]private readonly IParameterTypeMatcher _parameterTypeMatcher;

        /// <summary>
        /// Получает метод типа по имени и типам передаваемых аргументов
        /// </summary>
        public SimpleMethodResolver([NotNull]IParameterTypeMatcher parameterTypeMatcher)
        {
            if (parameterTypeMatcher == null) throw new ArgumentNullException("parameterTypeMatcher");
            _parameterTypeMatcher = parameterTypeMatcher;
        }

        private bool IsMatch(MethodInfo method, [NotNull]Expression[] arguments, out IDictionary<Type, Type> genericArgumentBinding)
        {
            genericArgumentBinding = null;
            if (method == null)
            {
                return false;
            }
            var parameters = method.GetParameters();
            if (parameters.Length != arguments.Length)
            {
                return false;
            }
            bool result = true;
            if (!method.IsGenericMethodDefinition)
            {
                for (var i = 0; i < parameters.Length; ++i)
                {
                    var parameter = parameters[i];
                    var argument = arguments[i];
                    if (parameter == null)
                    {
                        throw new InvalidOperationException("method.GetParameters() returned collection with null");
                    }
                    if (argument == null)
                    {
                        throw new ArgumentException("arguments contains null");
                    }
                    result = result && _parameterTypeMatcher.IsMatch(parameter.ParameterType, ref argument);
                    arguments[i] = argument;
                }
            }
            else
            {
                genericArgumentBinding = new Dictionary<Type, Type>();
                var savedArguments = new Expression[arguments.Length];
                arguments.CopyTo(savedArguments, 0);
                for (var i = 0; i < parameters.Length; ++i)
                {
                    var parameter = parameters[i];
                    var argument = arguments[i];
                    if (parameter == null)
                    {
                        throw new InvalidOperationException("method.GetParameters() returned collection with null");
                    }
                    if (argument == null)
                    {
                        throw new ArgumentException("arguments contains null");
                    }
                    result = result && _parameterTypeMatcher.IsGenericMatch(parameter.ParameterType, ref argument, genericArgumentBinding);
                    arguments[i] = argument;
                }
                if (!result)
                {
                    savedArguments.CopyTo(arguments, 0);
                }
            }
            return result;
        }

        private MethodInfo TryResolve([NotNull]IEnumerable<MemberInfo> candidates, [NotNull]Expression[] arguments)
        {
            IDictionary<Type, Type> genericBinding = null;
            var result = candidates.OfType<MethodInfo>().OrderBy(m => m != null && m.IsGenericMethod).FirstOrDefault(method => IsMatch(method, arguments, out genericBinding));
            // TODO : maybe we should try not just a first method, but then backtracking is needed
            if (result != null && genericBinding != null)
            {
                var genericArguments = result.GetGenericArguments().Select(a => genericBinding[a]).ToArray();
                result = result.MakeGenericMethod(genericArguments);
            }
            return result;
        }

        /// <summary>
        /// Попытаться получить метод
        /// </summary>
        /// <param name="instance">Тип объекта, метод которого ищется</param>
        /// <param name="methodName">Имя метода</param>
        /// <param name="arguments">Аргументы</param>
        /// <param name="typeArguments">Типы-параметры генерик-метода</param>
        /// <param name="candidates">Кандидаты при неоднозначности</param>
        /// <returns>Найденный метод или null</returns>
        public MethodInfo TryResolveInstance(Type instance, string methodName, Expression[] arguments, Type[] typeArguments, out IReadOnlyCollection<MethodInfo> candidates)
        {
            candidates = null;
            var methods = instance.GetTypeInfo().GetMember(
                methodName, MemberTypes.Method,
                BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
            return TryResolve(methods, arguments);
        }

        /// <summary>
        /// Попытаться получить статический метод или метод расширения
        /// </summary>
        /// <param name="extensionType">Тип со статическими методами</param>
        /// <param name="methodName">Имя метода</param>
        /// <param name="arguments">Аргументы, включая this-аргумент для метода расширения</param>
        /// <param name="typeArguments">Типы-параметры генерик-метода</param>
        /// <param name="candidates">Кандидаты при неоднозначности</param>
        public MethodInfo TryResolveStatic(Type extensionType, string methodName, Expression[] arguments, Type[] typeArguments, out IReadOnlyCollection<MethodInfo> candidates)
        {
            candidates = null;
            var methods = extensionType.GetTypeInfo().GetMember(methodName, MemberTypes.Method, BindingFlags.Public | BindingFlags.Static);
            return TryResolve(methods, arguments);
        }

        /// <summary>
        /// Попытаться получить статический метод или метод расширения
        /// </summary>
        /// <param name="extensionTypes">Типы со статическими методами</param>
        /// <param name="methodName">Имя метода</param>
        /// <param name="arguments">Аргументы, включая this-аргумент для метода расширения</param>
        /// <param name="typeArguments">Типы-параметры генерик-метода</param>
        /// <param name="candidates">Кандидаты при неоднозначности</param>
        /// <returns>Найденный метод или null</returns>
        public MethodInfo TryResolveStatic(IEnumerable<Type> extensionTypes, string methodName, Expression[] arguments, Type[] typeArguments, out IReadOnlyCollection<MethodInfo> candidates)
        {
            candidates = null;
            var methods = extensionTypes
                .Where(t => t != null)
                .SelectMany(t => t.GetTypeInfo().GetMember(methodName, MemberTypes.Method, BindingFlags.Public | BindingFlags.Static));
            return TryResolve(methods, arguments);
        }
    }
}
