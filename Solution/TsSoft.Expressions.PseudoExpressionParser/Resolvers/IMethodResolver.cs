﻿namespace TsSoft.Expressions.PseudoExpressionParser.Resolvers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Получает метод типа по имени и типам передаваемых аргументов
    /// </summary>
    public interface IMethodResolver
    {
        /// <summary>
        /// Попытаться получить метод
        /// </summary>
        /// <param name="instance">Тип объекта, метод которого ищется</param>
        /// <param name="methodName">Имя метода</param>
        /// <param name="arguments">Аргументы</param>
        /// <param name="typeArguments">Типы-параметры генерик-метода</param>
        /// <param name="candidates">Кандидаты при неоднозначности</param>
        /// <returns>Найденный метод или null</returns>
        [CanBeNull]
        MethodInfo TryResolveInstance(
            [NotNull]Type instance,
            [NotNull]string methodName,
            [NotNull]Expression[] arguments,
            [CanBeNull]Type[] typeArguments,
            out IReadOnlyCollection<MethodInfo> candidates);

        /// <summary>
        /// Попытаться получить статический метод или метод расширения
        /// </summary>
        /// <param name="extensionType">Тип со статическими методами</param>
        /// <param name="methodName">Имя метода</param>
        /// <param name="arguments">Аргументы, включая this-аргумент для метода расширения</param>
        /// <param name="typeArguments">Типы-параметры генерик-метода</param>
        /// <param name="candidates">Кандидаты при неоднозначности</param>
        /// <returns>Найденный метод или null</returns>
        [CanBeNull]
        MethodInfo TryResolveStatic(
            [NotNull]Type extensionType,
            [NotNull]string methodName,
            [NotNull]Expression[] arguments,
            [CanBeNull]Type[] typeArguments,
            out IReadOnlyCollection<MethodInfo> candidates);

        /// <summary>
        /// Попытаться получить статический метод или метод расширения
        /// </summary>
        /// <param name="extensionTypes">Типы со статическими методами</param>
        /// <param name="methodName">Имя метода</param>
        /// <param name="arguments">Аргументы, включая this-аргумент для метода расширения</param>
        /// <param name="typeArguments">Типы-параметры генерик-метода</param>
        /// <param name="candidates">Кандидаты при неоднозначности</param>
        /// <returns>Найденный метод или null</returns>
        [CanBeNull]
        MethodInfo TryResolveStatic(
            [NotNull]IEnumerable<Type> extensionTypes,
            [NotNull]string methodName,
            [NotNull]Expression[] arguments,
            [CanBeNull]Type[] typeArguments,
            out IReadOnlyCollection<MethodInfo> candidates);
    }
}
