namespace TsSoft.Expressions.PseudoExpressionParser.Resolvers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions;

    internal class ParameterTypeMatcher : IParameterTypeMatcher
    {
        [NotNull]private readonly ILambdaBodyMaker _lambdaBodyMaker;
        [NotNull]private readonly IDelegateTypeHelper _delegateTypeHelper;

        [CanBeNull] private Expression _null;

        public ParameterTypeMatcher([NotNull] ILambdaBodyMaker lambdaBodyMaker, [NotNull] IDelegateTypeHelper delegateTypeHelper)
        {
            if (lambdaBodyMaker == null) throw new ArgumentNullException("lambdaBodyMaker");
            if (delegateTypeHelper == null) throw new ArgumentNullException("delegateTypeHelper");
            _lambdaBodyMaker = lambdaBodyMaker;
            _delegateTypeHelper = delegateTypeHelper;
        }

        private bool IsMatch([NotNull] Type parameterType, [NotNull] Type argumentType, ref Expression argument)
        {
            var parameterTypeInfo = parameterType.GetTypeInfo();
            var argumentTypeInfo = argumentType.GetTypeInfo();
            var result = parameterTypeInfo.IsAssignableFrom(argumentType);
            if (result && parameterTypeInfo.IsClass && argumentTypeInfo.IsValueType)
            {
                if (argument != null)
                {
                    argument = Expression.Convert(argument, parameterType);
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        private bool IsDelegateMatch(
            [NotNull]Type delegateType,
            [NotNull]IReadOnlyList<Type> delegateParameters, 
            [NotNull]Type outType,
            ref Expression argument)
        {
            var lambdaStub = argument as UnparsedLambdaExpression;
            if (lambdaStub == null)
            {
                return false;
            }
            Expression body;
            ParameterExpression[] parameters;
            var result = _lambdaBodyMaker.TryMakeLambdaBody(lambdaStub, delegateParameters, out body, out parameters);
            if (result && body != null && (outType == typeof(void) && body.Type != typeof(void) || IsMatch(outType, body.Type, ref _null)))
            {
                argument = Expression.Lambda(delegateType, body, parameters);
                return true;
            }
            return false;
        }

        public bool IsMatch(Type parameterType, ref Expression argument)
        {
            if (argument == null)
            {
                return false;
            }
            if (_delegateTypeHelper.IsDelegateType(parameterType))
            {
                var description = _delegateTypeHelper.GetDelegateDescription(parameterType);
                return IsDelegateMatch(parameterType, description.ParameterTypes, description.ReturnType, ref argument);
            }
            return IsMatch(parameterType, argument.Type, ref argument);
        }

        private bool IsGenericMatch([NotNull] Type parameterType, [NotNull] Type argumentType, [NotNull] IDictionary<Type, Type> genericArgumentsBinding)
        {
            if (parameterType.IsGenericParameter)
            {
                Type boundType;
                if (genericArgumentsBinding.TryGetValue(parameterType, out boundType))
                {
                    return boundType == argumentType;
                }
                genericArgumentsBinding[parameterType] = argumentType;
                return true;
            }
            var parameterTypeInfo = parameterType.GetTypeInfo();
            var argumentTypeInfo = argumentType.GetTypeInfo();
            if (!parameterTypeInfo.IsGenericType)
            {
                return IsMatch(parameterType, argumentType, ref _null);
            }
            if (parameterTypeInfo.IsInterface)
            {
                var parameterDefinition = parameterType.GetGenericTypeDefinition();
                var suitableInterfaces = argumentTypeInfo.GetInterfaces().AppendOne(argumentType).Where(
                    i => i != null && i.GetTypeInfo().IsGenericType && i.GetGenericTypeDefinition() == parameterDefinition);
                var suitableInterface = suitableInterfaces.FirstOrDefault();    // TODO: check all interfaces, not just the first one - the problem with this is the need of backtracking
                if (suitableInterface == null)
                {
                    return false;
                }
                var parameterArguments = parameterTypeInfo.GetGenericArguments();
                var argumentArguments = suitableInterface.GetTypeInfo().GetGenericArguments();
                return IsGenericMatch(parameterArguments, argumentArguments, genericArgumentsBinding);
            }
            if (parameterTypeInfo.IsClass)
            {
                var parameterDefinition = parameterType.GetGenericTypeDefinition();
                if (!argumentTypeInfo.IsGenericType)
                {
                    return argumentTypeInfo.BaseType != null && IsGenericMatch(parameterType, argumentTypeInfo.BaseType, genericArgumentsBinding);
                }
                var argumentDefinition = argumentType.GetGenericTypeDefinition();
                if (argumentDefinition != parameterDefinition)
                {
                    return argumentTypeInfo.BaseType != null && IsGenericMatch(parameterType, argumentTypeInfo.BaseType, genericArgumentsBinding);
                }
                var parameterArguments = parameterTypeInfo.GetGenericArguments();
                var argumentArguments = argumentTypeInfo.GetGenericArguments();
                return IsGenericMatch(parameterArguments, argumentArguments, genericArgumentsBinding);
            }
            return false;
        }

        private bool IsGenericMatch([NotNull]Type[] parameterArguments, [NotNull]Type[] argumentArguments, [NotNull]IDictionary<Type, Type> genericArgumentsBinding)
        {
            if (parameterArguments.Length != argumentArguments.Length)
            {
                return false;   // TODO delete this
            }
            var result = true;
            for (int i = 0; i < parameterArguments.Length; ++i)
            {
                result = result
                    && parameterArguments[i] != null
                    && argumentArguments[i] != null
                    && IsGenericMatch(parameterArguments[i], argumentArguments[i], genericArgumentsBinding);
            }
            return result;
        }

        private bool IsDelegateGenericMatch(
            [NotNull]Type delegateType, 
            [NotNull]IReadOnlyList<Type> delegateParameters, 
            [NotNull]Type outType,
            [NotNull]IDictionary<Type, Type> genericArgumentsBinding,
            ref Expression argument)
        {
            var lambdaStub = argument as UnparsedLambdaExpression;
            if (lambdaStub == null)
            {
                return false;
            }
            Expression body;
            ParameterExpression[] parameters;
            var result = _lambdaBodyMaker.TryMakeLambdaBody(lambdaStub, delegateParameters, out body, out parameters);
            if (result && body != null && IsGenericMatch(outType, body.Type, genericArgumentsBinding))
            {
                var delegateTypeInfo = delegateType.GetTypeInfo();
                if (delegateTypeInfo.IsGenericType)
                {
                    var definition = delegateType.GetGenericTypeDefinition();
                    if (definition == null)
                    {
                        throw new NullReferenceException(string.Format("Type.GetGenericTypeDefinition returned null for {0}", delegateType));
                    }
                    var arguments = delegateTypeInfo.GetGenericArguments();
                    arguments = TrySubstituteGenericArguments(arguments, genericArgumentsBinding);
                    if (arguments == null)
                    {
                        return false;
                    }
                    delegateType = definition.MakeGenericType(arguments);
                }
                argument = Expression.Lambda(delegateType, body, parameters);
                return true;
            }
            return false;
        }

        public bool IsGenericMatch(Type parameterType, ref Expression argument, IDictionary<Type, Type> genericArgumentsBinding)
        {
            if (argument == null)
            {
                return false;
            }
            if (_delegateTypeHelper.IsDelegateType(parameterType))
            {
                var delegateDescription = _delegateTypeHelper.GetDelegateDescription(parameterType);
                var parameters = TrySubstituteGenericArguments(delegateDescription.ParameterTypes, genericArgumentsBinding);
                if (parameters == null)
                {
                    return false;
                }
                return IsDelegateGenericMatch(parameterType, parameters, delegateDescription.ReturnType, genericArgumentsBinding, ref argument);
            }
            return IsGenericMatch(parameterType, argument.Type, genericArgumentsBinding);
        }

        private Type[] TrySubstituteGenericArguments([NotNull]IReadOnlyList<Type> types, [NotNull] IDictionary<Type, Type> genericArgumentsBinding)
        {
            var processedTypes = new Type[types.Count];
            for (int i = 0; i < types.Count; ++i)
            {
                processedTypes[i] = TrySubstituteGenericArguments(
                    types[i], genericArgumentsBinding);
                if (processedTypes[i] == null)
                {
                    return null;
                }
            }
            return processedTypes;
        }

        private Type TrySubstituteGenericArguments(Type type, [NotNull]IDictionary<Type, Type> genericArgumentsBinding)
        {
            if (type == null)
            {
                return null;
            }
            var typeInfo = type.GetTypeInfo();
            if (typeInfo.IsGenericType)
            {
                var arguments = typeInfo.GetGenericArguments();
                var processed = TrySubstituteGenericArguments(arguments, genericArgumentsBinding);
                var definition = type.GetGenericTypeDefinition();
                if (processed == null || definition == null)
                {
                    return null;
                }
                return definition.MakeGenericType(processed);
            }
            if (type.IsGenericParameter)
            {
                Type result;
                genericArgumentsBinding.TryGetValue(type, out result);
                return result;
            }
            return type;
        }
    }
}
