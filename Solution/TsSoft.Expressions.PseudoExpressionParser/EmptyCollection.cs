﻿namespace TsSoft.Expressions.PseudoExpressionParser
{
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Пустая коллекция чего угодно
    /// </summary>
    public class EmptyCollection :
        IReadOnlyCollection<object>,
        IReadOnlyCollection<int>,
        IReadOnlyCollection<long>,
        IReadOnlyCollection<float>,
        IReadOnlyCollection<double>,
        IReadOnlyCollection<decimal>,
        IReadOnlyCollection<char>,
        IReadOnlyCollection<string>,
        IReadOnlyCollection<int?>,
        IReadOnlyCollection<long?>,
        IReadOnlyCollection<float?>,
        IReadOnlyCollection<double?>,
        IReadOnlyCollection<decimal?>,
        IReadOnlyCollection<char?>
    {
        /// <summary>
        /// Пустая коллекция чего угодно
        /// </summary>
        public static EmptyCollection Instance = new EmptyCollection();

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<int?> IEnumerable<int?>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<long?> IEnumerable<long?>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<float?> IEnumerable<float?>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<double?> IEnumerable<double?>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<decimal?> IEnumerable<decimal?>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<char?> IEnumerable<char?>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<object> IEnumerable<object>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<int> IEnumerable<int>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<long> IEnumerable<long>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<float> IEnumerable<float>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<double> IEnumerable<double>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<string> IEnumerable<string>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<decimal> IEnumerable<decimal>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустой коллекции
        /// </summary>
        IEnumerator<char> IEnumerable<char>.GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Нуль
        /// </summary>
        public int Count 
        {
            get { return 0; }
        }
    }
}
