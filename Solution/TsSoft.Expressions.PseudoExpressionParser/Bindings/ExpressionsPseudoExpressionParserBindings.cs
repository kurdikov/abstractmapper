﻿namespace TsSoft.Bindings
{
    using TsSoft.BindingsDescription;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator;
    using TsSoft.Expressions.PseudoExpressionParser.Resolvers;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.Expressions.PseudoExpressionParser
    /// </summary>
    public class ExpressionsPseudoExpressionParserBindings : BindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке TsSoft.Expressions.PseudoExpressionParser
        /// </summary>
        public ExpressionsPseudoExpressionParserBindings()
        {
            Bind<IExpressionContextFactory, ExpressionContextFactory>();
            Bind<IExpressionCreator, ExpressionCreator>();
            Bind<ILambdaBodyMaker, LambdaBodyMaker>();
            Bind<IMethodResolver, SimpleMethodResolver>();
            Bind<IParameterTypeMatcher, ParameterTypeMatcher>();
        }
    }
}
