﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using ParserGenerator;

    internal partial class PseudoExpressionVisitor
    {
        public override Expression VisitArrayConstant([NotNull]PseudoExpressionParser.ArrayConstantContext context)
        {
            var paramContext = context.expression();
            if (paramContext == null)
            {
                throw new NullReferenceException("Array constant contains no expression");
            }
            var param = new Expression[paramContext.Length];
            var paramTypes = new Type[paramContext.Length];
            for (int i = 0; i < paramContext.Length; ++i)
            {
                param[i] = Visit(paramContext[i]);
                if (param[i] == null)
                {
                    throw new NullReferenceException(string.Format("Visit returned null for {0}", paramContext[i]));
                }
                paramTypes[i] = param[i].Type;
                if (paramTypes[i] != paramTypes[0])
                {
                    throw new InvalidSyntaxException(
                        context, 
                        string.Format("array type mismatch : element #0 has type {0}, element #{2} has type {1}",
                            paramTypes[0],
                            paramTypes[i],
                            i));
                }
            }
            return Expression.NewArrayInit(paramTypes[0], param);
        }
    }
}
