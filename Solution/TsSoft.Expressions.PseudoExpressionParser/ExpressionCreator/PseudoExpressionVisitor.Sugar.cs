﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using ParserGenerator;
    using TsSoft.Expressions.Helpers.Reflection;

    internal partial class PseudoExpressionVisitor
    {
        [NotNull]private readonly IMemberInfoLibrary _memberLibrary;

        [NotNull]
        private Expression MakeInOperator(
            [NotNull]PseudoExpressionParser.ExpressionContext objContext,
            [NotNull]PseudoExpressionParser.ExpressionContext collContext)
        {
            var obj = Visit(objContext);
            if (obj == null)
            {
                throw new NullReferenceException("obj");
            }
            var coll = Visit(collContext);
            if (coll == null)
            {
                throw new NullReferenceException("coll");
            }
            if (obj.Type == typeof(string) && coll.Type == typeof(string))
            {
                return Expression.Call(coll, _memberLibrary.StringContains(), new[] { obj });
            }
            var collInterface = coll.Type.GetGenericEnumerableInterfaces()
                .FirstOrDefault(i => i != null && i.GetTypeInfo().GetGenericArguments()[0] == obj.Type);
            if (collInterface == null)
            {
                throw new InvalidSyntaxException(objContext.Parent, 
                    string.Format("Invalid IN operator: {0} has type {1} and does not implement IEnumerable[{2}]",
                        collContext.GetText(), coll.Type, obj.Type));
            }
            return Expression.Call(_memberLibrary.EnumerableContains(obj.Type), coll, obj);
        }

        public override Expression VisitInOperator([NotNull]PseudoExpressionParser.InOperatorContext context)
        {
            var result = MakeInOperator(context.expression(0), context.expression(1));
            return result;
        }

        public override Expression VisitNotInOperator([NotNull]PseudoExpressionParser.NotInOperatorContext context)
        {
            var result = MakeInOperator(context.expression(0), context.expression(1));
            return Expression.Not(result);
        }

        [NotNull]
        private Expression MakeIsNotEmptyOperator([NotNull]PseudoExpressionParser.ExpressionContext collContext)
        {
            var coll = Visit(collContext);
            if (coll == null)
            {
                throw new NullReferenceException("coll");
            }
            var collInterface = coll.Type.GetGenericEnumerableInterfaces().FirstOrDefault();
            if (collInterface == null)
            {
                throw new InvalidSyntaxException(collContext.Parent,
                    string.Format("Invalid IN operator: {0} has type {1} and does not implement IEnumerable[T]",
                        collContext.GetText(), coll.Type));
            }
            return Expression.Call(_memberLibrary.EnumerableAny(collInterface.GetTypeInfo().GetGenericArguments()[0]), coll);
        }

        public override Expression VisitIsEmptyOperator([NotNull]PseudoExpressionParser.IsEmptyOperatorContext context)
        {
            var result = MakeIsNotEmptyOperator(context.expression());
            return Expression.Not(result);
        }

        public override Expression VisitIsNotEmptyOperator([NotNull]PseudoExpressionParser.IsNotEmptyOperatorContext context)
        {
            var result = MakeIsNotEmptyOperator(context.expression());
            return result;
        }

        private Expression MakeStringOperator(
            PseudoExpressionParser.ExpressionContext strContext,
            PseudoExpressionParser.ExpressionContext patternContext, 
            [NotNull]MethodInfo method)
        {
            var str = Visit(strContext);
            var pattern = Visit(patternContext);
            if (str == null || str.Type != typeof(string))
            {
                throw new InvalidSyntaxException(strContext.Parent, string.Format("Invalid {0} operator: {1} is not a string",
                    method.Name.ToUpperInvariant(), strContext.GetText()));
            }
            return Expression.Call(str, method, new [] {pattern});
        }

        public override Expression VisitContainsOperator([NotNull]PseudoExpressionParser.ContainsOperatorContext context)
        {
            var result = MakeStringOperator(context.expression(0), context.expression(1), _memberLibrary.StringContains());
            return result;
        }

        public override Expression VisitStartsWithOperator([NotNull]PseudoExpressionParser.StartsWithOperatorContext context)
        {
            var result = MakeStringOperator(context.expression(0), context.expression(1), _memberLibrary.StringStartsWith());
            return result;
        }

        public override Expression VisitEndsWithOperator([NotNull]PseudoExpressionParser.EndsWithOperatorContext context)
        {
            var result = MakeStringOperator(context.expression(0), context.expression(1), _memberLibrary.StringEndsWith());
            return result;
        }
    }
}
