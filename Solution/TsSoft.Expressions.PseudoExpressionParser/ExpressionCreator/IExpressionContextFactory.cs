namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using JetBrains.Annotations;

    /// <summary>
    /// ������� ���������� ������� ���������
    /// </summary>
    public interface IExpressionContextFactory
    {
        /// <summary>
        /// ������� �������� ������� ���������
        /// </summary>
        [NotNull]
        IExpressionContext CreateContext([NotNull]ExpressionContextSettings settings);
    }
}
