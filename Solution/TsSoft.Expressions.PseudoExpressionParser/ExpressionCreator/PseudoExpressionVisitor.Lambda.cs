﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using ParserGenerator;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions;

    internal partial class PseudoExpressionVisitor
    {
        public override Expression VisitLambda([NotNull]PseudoExpressionParser.LambdaContext context)
        {
            var expression = context.expression();
            var id = context.ID();
            if (expression == null || id == null)
            {
                throw new InvalidSyntaxException(context, "invalid lambda");
            }
            return new UnparsedLambdaExpression {Context = expression, Visitor = this, Parameters = new List<UnparsedLambdaExpression.UnparsedLambdaParameter>
            {
                new UnparsedLambdaExpression.UnparsedLambdaParameter(id.GetText(), null)
            }};
        }

        public override Expression VisitMultipleParametersLambda([NotNull]PseudoExpressionParser.MultipleParametersLambdaContext context)
        {
            var expression = context.expression();
            var param = context.lambdaparam().Cast<PseudoExpressionParser.LambdaParameterContext>();
            if (expression == null)
            {
                throw new InvalidSyntaxException(context, "invalid lambda");
            }
            return new UnparsedLambdaExpression
            {
                Context = expression,
                Visitor = this,
                Parameters = param.Select(CreateParameter).ToList(),
            };
        }

        private UnparsedLambdaExpression.UnparsedLambdaParameter CreateParameter(
            PseudoExpressionParser.LambdaParameterContext context)
        {
            var type = context.type();
            return new UnparsedLambdaExpression.UnparsedLambdaParameter(
                context.name.Text,
                type != null ? ResolveType(context, type.GetText()) : null);
        }
    }
}

