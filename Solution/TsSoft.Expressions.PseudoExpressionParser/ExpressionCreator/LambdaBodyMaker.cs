﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions;

    class LambdaBodyMaker : ILambdaBodyMaker
    {
        public bool TryMakeLambdaBody(
            [NotNull]UnparsedLambdaExpression expression, 
            [NotNull]IReadOnlyList<Type> paramTypes, 
            out Expression body,
            out ParameterExpression[] parameters)
        {
            body = null;
            parameters = null;
            if (paramTypes.Count != expression.Parameters.Count)
            {
                return false;
            }
            var oldVariables = expression.Visitor.Variables;
            var newVariables = new Dictionary<string, Expression>(oldVariables);
            parameters = new ParameterExpression[paramTypes.Count];
            for (int i = 0; i < paramTypes.Count; ++i)
            {
                if (paramTypes[i] == null)
                {
                    parameters = null;
                    return false;
                }
                newVariables[expression.Parameters[i].Name] = parameters[i] = Expression.Parameter(paramTypes[i], expression.Parameters[i].Name);
            }
            expression.Visitor.Variables = newVariables;
            //try
            //{
                body = expression.Visitor.Visit(expression.Context);
                expression.Visitor.Variables = oldVariables;
            //}
            //catch (InvalidSyntaxException)
            //{
            //    return false;
            //}
            return true;
        }
    }
}
