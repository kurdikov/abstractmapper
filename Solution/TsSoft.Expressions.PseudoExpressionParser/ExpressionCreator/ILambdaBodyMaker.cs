﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions;

    /// <summary>
    /// Конструирует тело лямбды в соответствии с контекстом
    /// </summary>
    public interface ILambdaBodyMaker
    {
        /// <summary>
        /// Попытаться сконструировать тело лямбды по её синтаксическому дереву и входным типам
        /// </summary>
        /// <param name="expression">Синтаксическое дерево</param>
        /// <param name="paramTypes">Типы параметров лямбды</param>
        /// <param name="body">Построенное тело</param>
        /// <param name="parameters">Использованные параметры</param>
        /// <returns>Успешность построения</returns>
        /// <exception cref="InvalidSyntaxException">Ошибка в теле лямбды</exception>
        [ContractAnnotation("=> true, body: notnull, parameters: notnull; => false, body: null, parameters: null")]
        bool TryMakeLambdaBody(UnparsedLambdaExpression expression, IReadOnlyList<Type> paramTypes, out Expression body, out ParameterExpression[] parameters);
    }
}
