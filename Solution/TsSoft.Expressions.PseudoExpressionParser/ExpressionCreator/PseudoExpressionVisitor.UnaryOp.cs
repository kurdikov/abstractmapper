﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Linq.Expressions;
    using Antlr4.Runtime;
    using JetBrains.Annotations;
    using ParserGenerator;

    internal partial class PseudoExpressionVisitor
    {
        [NotNull]
        private Func<Expression, UnaryExpression> GetUnaryFactory(IToken sign)
        {
            if (sign == null)
            {
                throw new ArgumentNullException("sign");
            }
            switch (sign.Type)
            {
                case PseudoExpressionLexer.PLUS:
                    return Expression.UnaryPlus;
                case PseudoExpressionLexer.MINUS:
                    return Expression.Negate;
                case PseudoExpressionLexer.NOT:
                    return Expression.Not;
                case PseudoExpressionLexer.COMPLEMENT:
                    return Expression.OnesComplement;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("Invalid token type {0}", sign.Type));
            }
        }

        public override Expression VisitUnary([NotNull]PseudoExpressionParser.UnaryContext context)
        {
            var op = Visit(context.expression());
            var factory = GetUnaryFactory(context.sign);
            return factory(op);
        }
    }
}
