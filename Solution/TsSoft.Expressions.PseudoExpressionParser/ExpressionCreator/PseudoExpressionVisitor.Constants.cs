﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text.RegularExpressions;
    using JetBrains.Annotations;
    using ParserGenerator;
    using EClosure;

    internal partial class PseudoExpressionVisitor
    {
        private enum NumberType
        {
            Byte,
            Short,
            Int,
            Long,
            Decimal,
            Float,
            Double,
        }

        private Expression MakeConstant<T>(T constant, bool forceClosure = false)
        {
            if (_closureOverConstants || forceClosure)
            {
                return EClosure.Closure(constant);
            }
            return Expression.Constant(constant, typeof(T));
        }

        public override Expression VisitBooleanConstant([NotNull]PseudoExpressionParser.BooleanConstantContext context)
        {
            return MakeConstant(bool.Parse(context.BOOLEAN().GetText()), context.closure != null);
        }

        private Expression MakeNumeric(NumberType type, [NotNull]string str, NumberStyles styles, bool forceClosure = false)
        {
            switch (type)
            {
                case NumberType.Byte:
                    return MakeConstant(byte.Parse(str, styles), forceClosure);
                case NumberType.Short:
                    return MakeConstant(short.Parse(str, styles), forceClosure);
                case NumberType.Int:
                    return MakeConstant(int.Parse(str, styles), forceClosure);
                case NumberType.Long:
                    return MakeConstant(long.Parse(str, styles), forceClosure);
                case NumberType.Decimal:
                    return MakeConstant(decimal.Parse(str, styles, CultureInfo.InvariantCulture), forceClosure);
                case NumberType.Float:
                    return MakeConstant(float.Parse(str, styles, CultureInfo.InvariantCulture), forceClosure);
                case NumberType.Double:
                    return MakeConstant(double.Parse(str, styles, CultureInfo.InvariantCulture), forceClosure);
                default:
                    throw new ArgumentOutOfRangeException("type", type, "Invalid numeric type");
            }
        }

        private Expression MakeHexInteger(NumberType type, string str, bool forceClosure, bool suffix = true)
        {
            return MakeNumeric(type, !suffix ? str.Substring(2) : str.Substring(2, str.Length - 3), NumberStyles.HexNumber | NumberStyles.AllowHexSpecifier, forceClosure);
        }

        private Expression MakeFloat(NumberType type, string str, bool forceClosure, bool suffix = true)
        {
            return MakeNumeric(type, !suffix ? str : str.Substring(0, str.Length - 1), NumberStyles.Float | NumberStyles.AllowDecimalPoint, forceClosure);
        }

        private Expression MakeInteger(NumberType type, string str, bool forceClosure, bool suffix = true)
        {
            return MakeNumeric(type, !suffix ? str : str.Substring(0, str.Length - 1), NumberStyles.Integer, forceClosure);
        }

        public override Expression VisitHexIntegerConstant([NotNull]PseudoExpressionParser.HexIntegerConstantContext context)
        {
            return MakeHexInteger(NumberType.Int, context.HEXINTEGER().GetText(), context.closure != null, false);
        }

        public override Expression VisitIntegerConstant([NotNull]PseudoExpressionParser.IntegerConstantContext context)
        {
            return MakeInteger(NumberType.Int, context.INTEGER().GetText(), context.closure != null, false);
        }

        public override Expression VisitHexByteIntegerConstant([NotNull]PseudoExpressionParser.HexByteIntegerConstantContext context)
        {
            return MakeHexInteger(NumberType.Byte, context.HEXBYTEINTEGER().GetText(), context.closure != null);
        }

        public override Expression VisitByteIntegerConstant([NotNull]PseudoExpressionParser.ByteIntegerConstantContext context)
        {
            return MakeInteger(NumberType.Byte, context.BYTEINTEGER().GetText(), context.closure != null);
        }

        public override Expression VisitHexShortIntegerConstant([NotNull]PseudoExpressionParser.HexShortIntegerConstantContext context)
        {
            return MakeHexInteger(NumberType.Short, context.HEXSHORTINTEGER().GetText(), context.closure != null);
        }

        public override Expression VisitShortIntegerConstant([NotNull]PseudoExpressionParser.ShortIntegerConstantContext context)
        {
            return MakeInteger(NumberType.Short, context.SHORTINTEGER().GetText(), context.closure != null);
        }

        public override Expression VisitHexLongIntegerConstant([NotNull]PseudoExpressionParser.HexLongIntegerConstantContext context)
        {
            return MakeHexInteger(NumberType.Long, context.HEXLONGINTEGER().GetText(), context.closure != null);
        }

        public override Expression VisitLongIntegerConstant([NotNull]PseudoExpressionParser.LongIntegerConstantContext context)
        {
            return MakeInteger(NumberType.Long, context.LONGINTEGER().GetText(), context.closure != null);
        }

        public override Expression VisitDecimalConstant([NotNull]PseudoExpressionParser.DecimalConstantContext context)
        {
            return MakeFloat(NumberType.Decimal, context.DECIMAL().GetText(), context.closure != null);
        }
        
        public override Expression VisitFloatConstant([NotNull]PseudoExpressionParser.FloatConstantContext context)
        {
            return MakeFloat(NumberType.Float, context.SINGLE().GetText(), context.closure != null);
        }

        public override Expression VisitDoubleConstant([NotNull]PseudoExpressionParser.DoubleConstantContext context)
        {
            var literal = context.DOUBLE().GetText();
            return MakeFloat(NumberType.Double, literal, context.closure != null, literal.Last() == 'd');
        }

        public override Expression VisitStringConstant([NotNull]PseudoExpressionParser.StringConstantContext context)
        {
            var literal = context.STRING().GetText();
            if (literal == null || literal.Length < 2)
            {
                throw new InvalidOperationException(string.Format("Invalid string literal '{0}'", literal));
            }
            return MakeConstant(Regex.Unescape(literal.Substring(1, literal.Length - 2)), context.closure != null);
        }

        public override Expression VisitNull([NotNull]PseudoExpressionParser.NullContext context)
        {
            return Expression.Constant(null);
        }

        public override Expression VisitDateConstant([NotNull]PseudoExpressionParser.DateConstantContext context)
        {
            var literal = context.DATE().GetText();
            var dateStr = literal.Substring(5, literal.Length - 6);
            if (string.IsNullOrEmpty(dateStr))
            {
                return Expression.Property(null, typeof(DateTime), "Now");
            }
            return MakeConstant(DateTime.Parse(dateStr, CultureInfo.CurrentCulture), context.closure != null);
        }

        public override Expression VisitTimeConstant([NotNull]PseudoExpressionParser.TimeConstantContext context)
        {
            var literal = context.TIME().GetText();
            var timeStr = literal.Substring(5, literal.Length - 6);
            if (string.IsNullOrEmpty(timeStr))
            {
                return  MakeConstant(TimeSpan.Zero);
            }
            return MakeConstant(TimeSpan.Parse(timeStr, CultureInfo.CurrentCulture), context.closure != null);
        }

        public override Expression VisitUuidConstant([NotNull]PseudoExpressionParser.UuidConstantContext context)
        {
            var literal = context.UUID().GetText();
            var uuidStr = literal.Substring(5, literal.Length - 6);
            return MakeConstant(Guid.Parse(uuidStr), context.closure != null);
        }

        public override Expression VisitCharConstant([NotNull]PseudoExpressionParser.CharConstantContext context)
        {
            var literal = context.CHAR().GetText();
            var charStr = literal.Substring(1, literal.Length - 2);
            return MakeConstant(char.Parse(charStr), context.closure != null);
        }
    }
}
