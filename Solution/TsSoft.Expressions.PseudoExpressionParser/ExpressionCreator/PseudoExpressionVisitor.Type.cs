﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using ParserGenerator;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions;

    internal partial class PseudoExpressionVisitor
    {
        public override Expression VisitTypeOperator([NotNull]PseudoExpressionParser.TypeOperatorContext context)
        {
            var type = context.type().ThrowIfNull();
            var typeObj = ResolveType(type, type.GetText().ThrowIfNull());
            var expr = Visit(context.expression());
            switch (context.op.ThrowIfNull().Type)
            {
                case PseudoExpressionLexer.CAST:
                    return Expression.Convert(expr, typeObj);
                case PseudoExpressionLexer.TYPEAS:
                    typeObj = !typeObj.IsNullableType() ? typeof(Nullable<>).MakeGenericType(typeObj) : typeObj;
                    return Expression.TypeAs(expr, typeObj);
                case PseudoExpressionLexer.TYPEIS:
                    return Expression.TypeIs(expr, typeObj);
                default:
                    throw new InvalidSyntaxException(context, "Unknown type operator");
            }
        }

        public override Expression VisitTypeConstant([NotNull]PseudoExpressionParser.TypeConstantContext context)
        {
            var type = context.TYPE().GetText();
            return Expression.Constant(ResolveType(context, type.Substring(5, type.Length - 6)), typeof(Type));
        }

        public override Expression VisitStaticMembersType(PseudoExpressionParser.StaticMembersTypeContext context)
        {
            if (!_allowStaticCallsOnTypeConstants)
            {
                throw new InvalidSyntaxException(context, "Dynamically specified types for static members access are not allowed");
            }
            var type = context.STATICTYPE().GetText();
            return new TypeExpression(ResolveType(context, type.Substring(7, type.Length - 8)));
        }
    }
}
