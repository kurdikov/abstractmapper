﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Создаёт выражение по строковому представлению
    /// </summary>
    public interface IExpressionCreator
    {
        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="T">Тип параметра</typeparam>
        /// <param name="paramName">Имя параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <returns>Выражение-фильтр</returns>
        [NotNull]
        Expression<Func<T, bool>> CreateFilter<T>([NotNull] string paramName, [NotNull] string expression);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="T">Тип параметра</typeparam>
        /// <param name="paramName">Имя параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="staticTypes">Типы со статическими методами, свойствам, полями в выражении</param>
        /// <returns>Выражение-фильтр</returns>
        [NotNull]
        Expression<Func<T, bool>> CreateFilter<T>([NotNull] string paramName, [NotNull] string expression, params Type[] staticTypes);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="T1">Тип первого параметра</typeparam>
        /// <typeparam name="T2">Тип второго параметра</typeparam>
        /// <param name="firstParamName">Имя первого параметра</param>
        /// <param name="secondParamName">Имя второго параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <returns>Выражение-фильтр</returns>
        [NotNull]
        Expression<Func<T1, T2, bool>> CreateFilter<T1, T2>([NotNull] string firstParamName, [NotNull] string secondParamName, [NotNull] string expression);

        /// <summary>
        /// Построить выражение-фильтр
        /// </summary>
        /// <typeparam name="T1">Тип первого параметра</typeparam>
        /// <typeparam name="T2">Тип второго параметра</typeparam>
        /// <param name="firstParamName">Имя первого параметра</param>
        /// <param name="secondParamName">Имя второго параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="staticTypes">Типы со статическими методами, свойствам, полями в выражении</param>
        /// <returns>Выражение-фильтр</returns>
        [NotNull]
        Expression<Func<T1, T2, bool>> CreateFilter<T1, T2>([NotNull] string firstParamName, [NotNull] string secondParamName, [NotNull] string expression, params Type[] staticTypes);

        /// <summary>
        /// Построить выражение, отображающее объект в строку
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="paramName">Имя параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <returns>Выражение</returns>
        [NotNull]
        Expression<Func<T, string>> CreateStringifier<T>([NotNull] string paramName, [NotNull] string expression);

        /// <summary>
        /// Построить выражение, отображающее объект в строку
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="paramName">Имя параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="staticTypes">Типы со статическими методами, свойствам, полями в выражении</param>
        /// <returns>Выражение</returns>
        [NotNull]
        Expression<Func<T, string>> CreateStringifier<T>([NotNull] string paramName, [NotNull] string expression, params Type[] staticTypes);

        /// <summary>
        /// Построить выражение без параметров
        /// </summary>
        /// <typeparam name="TOut">Тип выхода</typeparam>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="registerString">Использовать статические методы класса System.String</param>
        /// <returns>Выражение</returns>
        [NotNull]
        Expression<Func<TOut>> CreateExpression<TOut>([NotNull] string expression, bool registerString = false);

        /// <summary>
        /// Построить выражение без параметров
        /// </summary>
        /// <typeparam name="TOut">Тип выхода</typeparam>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="staticTypes">Типы со статическими методами, свойствам, полями в выражении</param>
        /// <returns>Выражение</returns>
        [NotNull]
        Expression<Func<TOut>> CreateExpression<TOut>([NotNull] string expression, params Type[] staticTypes);

        /// <summary>
        /// Построить выражение без параметров
        /// </summary>
        /// <typeparam name="TOut">Тип выхода</typeparam>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="settings">Настройки генерации</param>
        /// <param name="staticTypes">Типы со статическими методами, свойствам, полями в выражении</param>
        /// <returns>Выражение</returns>
        [NotNull]
        Expression<Func<TOut>> CreateExpression<TOut>(
            [NotNull]string expression, 
            [NotNull]ExpressionContextSettings settings,
            params Type[] staticTypes);

        /// <summary>
        /// Построить выражение с одним параметром
        /// </summary>
        /// <typeparam name="T">Тип входного параметра</typeparam>
        /// <typeparam name="TOut">Тип выхода</typeparam>
        /// <param name="paramName">Имя параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="registerString">Использовать статические методы класса System.String</param>
        /// <returns>Выражение</returns>
        [NotNull]
        Expression<Func<T, TOut>> CreateExpression<T, TOut>(
            [NotNull]string paramName, 
            [NotNull]string expression,
            bool registerString = false);

        /// <summary>
        /// Построить выражение с одним параметром
        /// </summary>
        /// <typeparam name="T">Тип входного параметра</typeparam>
        /// <typeparam name="TOut">Тип выхода</typeparam>
        /// <param name="paramName">Имя параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="staticTypes">Типы со статическими методами, свойствам, полями в выражении</param>
        /// <returns>Выражение</returns>
        [NotNull]
        Expression<Func<T, TOut>> CreateExpression<T, TOut>(
            [NotNull]string paramName,
            [NotNull]string expression,
            params Type[] staticTypes);

        /// <summary>
        /// Построить выражение с одним параметром
        /// </summary>
        /// <typeparam name="T">Тип входного параметра</typeparam>
        /// <typeparam name="TOut">Тип выхода</typeparam>
        /// <param name="paramName">Имя параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="settings">Настройки генерации</param>
        /// <param name="staticTypes">Типы со статическими методами, свойствам, полями в выражении</param>
        /// <returns>Выражение</returns>
        [NotNull]
        Expression<Func<T, TOut>> CreateExpression<T, TOut>(
            [NotNull]string paramName,
            [NotNull]string expression,
            [NotNull]ExpressionContextSettings settings,
            params Type[] staticTypes);

        /// <summary>
        /// Построить выражение с одним параметром
        /// </summary>
        /// <typeparam name="T1">Тип первого входного параметра</typeparam>
        /// <typeparam name="T2">Тип второго входного параметра</typeparam>
        /// <typeparam name="TOut">Тип выхода</typeparam>
        /// <param name="firstParamName">Имя первого параметра</param>
        /// <param name="secondParamName">Имя второго параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="registerString">Использовать статические методы класса System.String</param>
        /// <returns>Выражение</returns>
        [NotNull]
        Expression<Func<T1, T2, TOut>> CreateExpression<T1, T2, TOut>(
            [NotNull] string firstParamName,
            [NotNull] string secondParamName,
            [NotNull] string expression,
            bool registerString = false);

        /// <summary>
        /// Построить выражение с одним параметром
        /// </summary>
        /// <typeparam name="T1">Тип первого входного параметра</typeparam>
        /// <typeparam name="T2">Тип второго входного параметра</typeparam>
        /// <typeparam name="TOut">Тип выхода</typeparam>
        /// <param name="firstParamName">Имя первого параметра</param>
        /// <param name="secondParamName">Имя второго параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="staticTypes">Типы со статическими методами, свойствам, полями в выражении</param>
        /// <returns>Выражение</returns>
        [NotNull]
        Expression<Func<T1, T2, TOut>> CreateExpression<T1, T2, TOut>(
            [NotNull] string firstParamName,
            [NotNull] string secondParamName,
            [NotNull] string expression,
            params Type[] staticTypes);

        /// <summary>
        /// Построить выражение с одним параметром
        /// </summary>
        /// <typeparam name="T1">Тип первого входного параметра</typeparam>
        /// <typeparam name="T2">Тип второго входного параметра</typeparam>
        /// <typeparam name="TOut">Тип выхода</typeparam>
        /// <param name="firstParamName">Имя первого параметра</param>
        /// <param name="secondParamName">Имя второго параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="settings">Настройки генерации</param>
        /// <param name="staticTypes">Типы со статическими методами, свойствам, полями в выражении</param>
        /// <returns>Выражение</returns>
        [NotNull]
        Expression<Func<T1, T2, TOut>> CreateExpression<T1, T2, TOut>(
            [NotNull]string firstParamName,
            [NotNull]string secondParamName,
            [NotNull]string expression,
            [NotNull]ExpressionContextSettings settings,
            params Type[] staticTypes);
    }
}
