﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions;

    /// <summary>
    /// Контекст разбора выражения
    /// </summary>
    public interface IExpressionContext
    {
        /// <summary>
        /// Добавить в контекст идентификатор-параметр
        /// </summary>
        /// <param name="type">Тип параметра</param>
        /// <param name="name">Имя параметра</param>
        ParameterExpression AddParameter([NotNull]Type type, [NotNull]string name);
        
        /// <summary>
        /// Добавить тип с методами расширения
        /// </summary>
        /// <param name="extensionType">Тип с методами расширения</param>
        void AddExtensionType([NotNull]Type extensionType);

        /// <summary>
        /// Добавить тип со статическими методами
        /// </summary>
        /// <param name="type">Тип со статическими методами</param>
        /// <param name="name">Идентификатор для обращения к статическим методам (по умолчанию - имя типа)</param>
        void AddClass([NotNull]Type type, string name = null);

        /// <summary>
        /// Добавить тип со статическими методами
        /// </summary>
        /// <param name="type">Тип со статическими методами</param>
        /// <param name="name">Идентификатор для обращения к статическим методам (по умолчанию - имя типа)</param>
        void AddClass([NotNull]TypeExpression type, string name = null);

        /// <summary>
        /// Разобрать строку с выражением
        /// </summary>
        /// <param name="expression">Строковое представление выражения</param>
        /// <returns>Разобранное выражение</returns>
        [NotNull]
        Expression Parse(string expression);
    }
}