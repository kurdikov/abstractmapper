﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using Antlr4.Runtime;
    using JetBrains.Annotations;
    using ParserGenerator;

    internal partial class PseudoExpressionVisitor
    {
        private BinaryExpression MakeBinary(Expression left, Expression right,
            [NotNull]Func<Expression, Expression, BinaryExpression> factory)
        {
            return factory(left, right);
        }

        public override Expression VisitParens([NotNull]PseudoExpressionParser.ParensContext context)
        {
            return Visit(context.expression());
        }

        public override Expression VisitBinary(PseudoExpressionParser.BinaryContext context)
        {
            var left = Visit(context.expression(0));
            var right = Visit(context.expression(1));
            var factory = GetBinaryFactory(context.sign);
            var type = context.sign.Type;
            if (type == PseudoExpressionLexer.EQUALS || type == PseudoExpressionLexer.NOTEQUALS)
            {
                TryTransformToEnum(ref left, ref right);
            }
            else if (type == PseudoExpressionLexer.BITAND || type == PseudoExpressionLexer.BITOR
                     || type == PseudoExpressionLexer.BITXOR)
            {
                TryTransformToEnumUnderlying(ref left, ref right);
            }
            else if (type == PseudoExpressionLexer.PLUS)
            {
                if (left != null && right != null && left.Type == typeof(string) && right.Type == typeof(string))
                {
                    return Expression.Add(
                        left,
                        right,
                        _memberLibrary.StringConcatTwo());
                }
            }
            return MakeBinary(left, right, factory);
        }


        private void TryTransformToEnum([NotNull]ref Expression left, [NotNull]ref Expression right)
        {
            if (left != null && right != null && left.Type != right.Type)
            {
                var leftTypeInfo = left.Type.GetTypeInfo();
                var rightTypeInfo = right.Type.GetTypeInfo();
                if (leftTypeInfo.IsEnum && leftTypeInfo.GetEnumUnderlyingType() == right.Type)
                {
                    right = Expression.Convert(right, left.Type);
                }
                else if (rightTypeInfo.IsEnum && rightTypeInfo.GetEnumUnderlyingType() == left.Type)
                {
                    left = Expression.Convert(left, right.Type);
                }
            }
        }

        private void TryTransformToEnumUnderlying([NotNull] ref Expression left, [NotNull] ref Expression right)
        {
            if (left != null && right != null)
            {
                var leftTypeInfo = left.Type.GetTypeInfo();
                var rightTypeInfo = right.Type.GetTypeInfo();
                if (leftTypeInfo.IsEnum && rightTypeInfo.IsEnum && left.Type == right.Type)
                {
                    left = Expression.Convert(left, leftTypeInfo.GetEnumUnderlyingType());
                    right = Expression.Convert(right, rightTypeInfo.GetEnumUnderlyingType());
                }
                else if (leftTypeInfo.IsEnum && leftTypeInfo.GetEnumUnderlyingType() == right.Type)
                {
                    left = Expression.Convert(left, right.Type);
                }
                else if (rightTypeInfo.IsEnum && rightTypeInfo.GetEnumUnderlyingType() == left.Type)
                {
                    right = Expression.Convert(right, left.Type);
                }
            }
        }

        [NotNull]
        private Func<Expression, Expression, BinaryExpression> GetBinaryFactory(IToken sign)
        {
            if (sign == null)
            {
                throw new ArgumentNullException("sign");
            }
            switch (sign.Type)
            {
                case PseudoExpressionLexer.EQUALS:
                    return Expression.Equal;
                case PseudoExpressionLexer.NOTEQUALS:
                    return Expression.NotEqual;
                case PseudoExpressionLexer.LESS:
                    return Expression.LessThan;
                case PseudoExpressionLexer.LESSEQUALS:
                    return Expression.LessThanOrEqual;
                case PseudoExpressionLexer.GREATER:
                    return Expression.GreaterThan;
                case PseudoExpressionLexer.GREATEREQUALS:
                    return Expression.GreaterThanOrEqual;
                case PseudoExpressionLexer.PLUS:
                    return Expression.Add;
                case PseudoExpressionLexer.MINUS:
                    return Expression.Subtract;
                case PseudoExpressionLexer.TIMES:
                    return Expression.Multiply;
                case PseudoExpressionLexer.DIVIDE:
                    return Expression.Divide;
                case PseudoExpressionLexer.MODULO:
                    return Expression.Modulo;
                case PseudoExpressionLexer.LEFTSHIFT:
                    return Expression.LeftShift;
                case PseudoExpressionLexer.RIGHTSHIFT:
                    return Expression.RightShift;
                case PseudoExpressionLexer.OR:
                    return Expression.OrElse;
                case PseudoExpressionLexer.AND:
                    return Expression.AndAlso;
                case PseudoExpressionLexer.BITOR:
                    return Expression.Or;
                case PseudoExpressionLexer.BITXOR:
                    return Expression.ExclusiveOr;
                case PseudoExpressionLexer.BITAND:
                    return Expression.And;
                case PseudoExpressionLexer.COALESCE:
                    return Expression.Coalesce;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("Invalid token type {0}", sign.Type));
            }
        }
    }
}
