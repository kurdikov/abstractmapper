﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions;

    class ExpressionCreator : IExpressionCreator
    {
        [NotNull]private readonly IExpressionContextFactory _contextFactory;
        [NotNull] private readonly TypeExpression _stringType = new TypeExpression(typeof(string));
        [NotNull] private readonly Type[] _stringTypeArray = {typeof(string)};

        public ExpressionCreator([NotNull]IExpressionContextFactory contextFactory)
        {
            if (contextFactory == null) throw new ArgumentNullException("contextFactory");
            _contextFactory = contextFactory;
        }

        public Expression<Func<T, bool>> CreateFilter<T>(string paramName, string expression)
        {
            return CreateExpression<T, bool>(paramName, expression);
        }

        public Expression<Func<T1, T2, bool>> CreateFilter<T1, T2>(string firstParamName, string secondParamName, string expression)
        {
            return CreateExpression<T1, T2, bool>(firstParamName, secondParamName, expression);
        }

        public Expression<Func<T, string>> CreateStringifier<T>(string paramName, string expression)
        {
            return CreateExpression<T, string>(paramName, expression, true);
        }

        private void CheckBodyType<TOut>([NotNull]Expression body, string expression)
        {
            if (body == null)
            {
                throw new InvalidOperationException(string.Format("Invalid expression '{0}'", expression));
            }
            if (body.Type != typeof(TOut)) // TODO replace with IsAssignableFrom, add converts as needed
            {
                throw new InvalidOperationException(string.Format("Provided expression '{0}' has type {1} instead of expected {2}",
                    expression, body.Type, typeof(TOut)));
            }
        }

        public Expression<Func<T, TOut>> CreateExpression<T, TOut>(
            string paramName, 
            string expression,
            bool registerString = false)
        {
            return CreateExpression<T, TOut>(paramName, expression, new ExpressionContextSettings(), registerString ? _stringTypeArray : Type.EmptyTypes);
        }

        public Expression<Func<T1, T2, TOut>> CreateExpression<T1, T2, TOut>(
            string firstParamName,
            string secondParamName,
            string expression,
            bool registerString = false)
        {
            return CreateExpression<T1, T2, TOut>(firstParamName, secondParamName, expression, new ExpressionContextSettings(), registerString ? _stringTypeArray : Type.EmptyTypes);
        }

        public Expression<Func<TOut>> CreateExpression<TOut>(string expression, bool registerString = false)
        {
            return CreateExpression<TOut>(expression, new ExpressionContextSettings(), registerString ? _stringTypeArray : Type.EmptyTypes);
        }

        private void RegisterString([NotNull]IExpressionContext context)
        {
            context.AddClass(_stringType, "string");
            context.AddClass(_stringType, "String");
        }

        public Expression<Func<T, bool>> CreateFilter<T>(string paramName, string expression, params Type[] staticTypes)
        {
            return CreateExpression<T, bool>(paramName, expression, staticTypes);
        }

        public Expression<Func<T1, T2, bool>> CreateFilter<T1, T2>(string firstParamName, string secondParamName, string expression,
            params Type[] staticTypes)
        {
            return CreateExpression<T1, T2, bool>(firstParamName, secondParamName, expression, staticTypes);
        }

        public Expression<Func<T, string>> CreateStringifier<T>(string paramName, string expression, params Type[] staticTypes)
        {
            return CreateExpression<T, string>(paramName, expression, staticTypes);
        }

        private void RegisterTypes(IEnumerable<Type> staticTypes, [NotNull]IExpressionContext context)
        {
            foreach (var type in staticTypes ?? Enumerable.Empty<Type>())
            {
                if (type == null)
                {
                    continue;
                }
                if (type == typeof(string))
                {
                    RegisterString(context);
                }
                else
                {
                    context.AddClass(type);
                }
            }
        }

        public Expression<Func<TOut>> CreateExpression<TOut>(string expression, params Type[] staticTypes)
        {
            return CreateExpression<TOut>(expression, new ExpressionContextSettings(), staticTypes);
        }

        public Expression<Func<TOut>> CreateExpression<TOut>(string expression, ExpressionContextSettings settings, params Type[] staticTypes)
        {
            var context = _contextFactory.CreateContext(settings);
            RegisterTypes(staticTypes, context);
            var body = context.Parse(expression);
            CheckBodyType<TOut>(body, expression);
            return Expression.Lambda<Func<TOut>>(body);
        }

        public Expression<Func<T, TOut>> CreateExpression<T, TOut>(string paramName, string expression, params Type[] staticTypes)
        {
            return CreateExpression<T, TOut>(paramName, expression, new ExpressionContextSettings(), staticTypes);
        }

        public Expression<Func<T, TOut>> CreateExpression<T, TOut>(string paramName, string expression, ExpressionContextSettings settings, params Type[] staticTypes)
        {
            var context = _contextFactory.CreateContext(settings);
            var param = context.AddParameter(typeof(T), paramName);
            RegisterTypes(staticTypes, context);
            var body = context.Parse(expression);
            CheckBodyType<TOut>(body, expression);
            return Expression.Lambda<Func<T, TOut>>(body, new [] {param});
        }

        public Expression<Func<T1, T2, TOut>> CreateExpression<T1, T2, TOut>(string firstParamName, string secondParamName, string expression,
            params Type[] staticTypes)
        {
            return CreateExpression<T1, T2, TOut>(firstParamName, secondParamName, expression, new ExpressionContextSettings(), staticTypes);
        }

        public Expression<Func<T1, T2, TOut>> CreateExpression<T1, T2, TOut>(string firstParamName, string secondParamName, string expression, ExpressionContextSettings settings,
            params Type[] staticTypes)
        {
            var context = _contextFactory.CreateContext(settings);
            var param1 = context.AddParameter(typeof(T1), firstParamName);
            var param2 = context.AddParameter(typeof(T2), secondParamName);
            RegisterTypes(staticTypes, context);
            var body = context.Parse(expression);
            CheckBodyType<TOut>(body, expression);
            return Expression.Lambda<Func<T1, T2, TOut>>(body, new [] {param1, param2});
        }
    }
}
