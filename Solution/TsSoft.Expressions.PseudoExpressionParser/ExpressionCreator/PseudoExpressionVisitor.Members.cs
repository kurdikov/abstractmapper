﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using ParserGenerator;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions;
    using TsSoft.Expressions.PseudoExpressionParser.Resolvers;

    internal partial class PseudoExpressionVisitor
    {
        [NotNull]private readonly IMethodResolver _methodResolver;

        [NotNull]private readonly ICollection<Type> _extensionTypes = new List<Type> { typeof(Enumerable) };

        [NotNull]private readonly Expression[] _noArguments = new Expression[0];

        public void AddExtensionType(Type type)
        {
            if (!_extensionTypes.Contains(type))
            {
                _extensionTypes.Add(type);
            }
        }

        public override Expression VisitIdentifier([NotNull]PseudoExpressionParser.IdentifierContext context)
        {
            Expression identifierValue;
            var id = context.ID();
            string idName;
            if (id == null || (idName = id.GetText()) == null)
            {
                throw new InvalidSyntaxException(context, "no id");
            }
            if (Variables.TryGetValue(idName, out identifierValue))
            {
                return identifierValue;
            }
            throw new InvalidSyntaxException(context, string.Format("unknown identifier {0}", idName));
        }

        private Expression MakeMethodCall(
            [NotNull]Expression obj,
            [NotNull]string methodName,
            [NotNull]Expression[] arguments,
            [CanBeNull]Type[] typeArguments,
            out IReadOnlyCollection<MethodInfo> candidates)
        {
            Expression result = null;
            var objType = GetTypeObject(obj);
            MethodInfo method;
            if (objType != null)
            {
                method = _methodResolver.TryResolveStatic(objType, methodName, arguments, typeArguments, out candidates);
                if (method != null)
                {
                    result = Expression.Call(method, arguments);
                }
            }
            else
            {
                method = _methodResolver.TryResolveInstance(obj.Type, methodName, arguments, typeArguments, out candidates);
                if (method != null)
                {
                    result = Expression.Call(obj, method, arguments);
                }
                else if (candidates == null)
                {
                    var extArgs = new Expression[arguments.Length + 1];
                    arguments.CopyTo(extArgs, 1);
                    extArgs[0] = obj;
                    method = _methodResolver.TryResolveStatic(_extensionTypes, methodName, extArgs, typeArguments, out candidates);
                    if (method != null)
                    {
                        result = Expression.Call(method, extArgs);
                    }
                }
            }
            return result;
        }

        public override Expression VisitMethodCall([NotNull]PseudoExpressionParser.MethodCallContext context)
        {
            var obj = Visit(context.expression());
            if (obj == null)
            {
                throw new InvalidOperationException(string.Format("{0} has no primary", context.GetText()));
            }
            var id = context.ID();
            if (id == null)
            {
                throw new InvalidSyntaxException(context, string.Format("no method name"));
            }
            var methodName = id.GetText();
            if (methodName == null)
            {
                throw new InvalidSyntaxException(context, string.Format("no method name"));
            }
            IReadOnlyCollection<MethodInfo> candidates;
            var result = MakeMethodCall(obj, methodName, _noArguments, GetTypeParams(context.typeparams()), out candidates);
            if (result == null)
            {
                throw new UnresolvedMethodException(context, methodName, obj.Type);
            }
            return result;
        }

        public override Expression VisitMethodWithParametersCall([NotNull]PseudoExpressionParser.MethodWithParametersCallContext context)
        {
            var obj = Visit(context.expression(0));
            if (obj == null)
            {
                throw new InvalidOperationException(string.Format("{0} has no primary", context.GetText()));
            }
            var id = context.ID();
            if (id == null)
            {
                throw new InvalidSyntaxException(context, "no method name");
            }
            var methodName = id.GetText();
            if (methodName == null)
            {
                throw new InvalidSyntaxException(context, "no method name");
            }
            var paramContext = context._argument;
            if (paramContext == null)
            {
                throw new InvalidSyntaxException(context, "no arguments");
            }
            var param = new Expression[paramContext.Count];
            var paramTypes = new Type[paramContext.Count];
            for (int i = 0; i < paramContext.Count; ++i)
            {
                param[i] = Visit(paramContext[i]);
                paramTypes[i] = param[i].Type;
            }
            IReadOnlyCollection<MethodInfo> candidates;
            var result = MakeMethodCall(obj, methodName, param, GetTypeParams(context.typeparams()), out candidates);
            if (result == null)
            {
                throw new UnresolvedMethodException(context, methodName, obj.Type, paramTypes, candidates);
            }
            return result;
        }

        private Type[] GetTypeParams(PseudoExpressionParser.TypeparamsContext typeparams)
        {
            var ctx = typeparams as PseudoExpressionParser.TypeParametersContext;
            if (ctx == null)
            {
                return null;
            }
            var types = ctx.type().ThrowIfNull();
            var result = new Type[types.Length];
            for (int i = 0; i < result.Length; ++i)
            {
                result[i] = ResolveType(typeparams, types[i].ThrowIfNull().GetText());
            }
            return result;
        }

        private Expression TryMakePropertyPath([NotNull]Expression obj, [NotNull]string memberName)
        {
            var enumerableArgument = obj.Type.GetGenericEnumerableArgumentOrSelf();
            if (enumerableArgument != obj.Type)
            {
                var elemProperty = enumerableArgument.GetTypeInfo().GetProperty(memberName);
                if (elemProperty != null)
                {
                    var elemPropertyEnumerableArgument =
                        elemProperty.PropertyType.GetGenericEnumerableArgumentOrSelf();
                    var lambdaParam = Expression.Parameter(enumerableArgument);
                    var lambda = Expression.Lambda(typeof(Func<,>).MakeGenericType(enumerableArgument, elemProperty.PropertyType),
                        Expression.Property(lambdaParam, elemProperty),
                        new[] { lambdaParam });
                    return elemPropertyEnumerableArgument != elemProperty.PropertyType
                        ? Expression.Call(
                            _memberLibrary.EnumerableSelectMany(enumerableArgument, elemPropertyEnumerableArgument),
                            obj,
                            lambda)
                        : Expression.Call(
                            _memberLibrary.EnumerableSelect(enumerableArgument, elemProperty.PropertyType),
                            obj,
                            lambda);
                }
            }
            return null;
        }

        public override Expression VisitMemberAccess([NotNull]PseudoExpressionParser.MemberAccessContext context)
        {
            var obj = Visit(context.expression());
            var objType = GetTypeObject(obj);
            var memberName = context.ID().ThrowIfNull("context.ID").GetText();
            if (memberName == null)
            {
                throw new InvalidSyntaxException(context, "no member name");
            }
            MemberInfo member;
            if (objType == null)
            {
                var typeInfo = obj.Type.GetTypeInfo();
                member = (MemberInfo)typeInfo.GetProperty(memberName) ?? typeInfo.GetField(memberName);
                if (member == null)
                {
                    var propertyPath = TryMakePropertyPath(obj, memberName);
                    if (propertyPath == null)
                    {
                        throw new InvalidSyntaxException(context, string.Format("unable to resolve member {0} of {1}", memberName, obj.Type));
                    }
                    return propertyPath;
                }
            }
            else
            {
                var typeInfo = objType.GetTypeInfo();
                member = (MemberInfo)typeInfo.GetProperty(memberName, BindingFlags.Public | BindingFlags.Static)
                         ?? typeInfo.GetField(memberName, BindingFlags.Public | BindingFlags.Static);
                if (member == null)
                {
                    throw new InvalidSyntaxException(context, string.Format("unable to resolve static member {0} of {1}", memberName, objType));
                }
                if (typeInfo.IsEnum)
                {
                    return Expression.Constant(Enum.Parse(objType, memberName));
                }
                obj = null;
            }
            // ReSharper disable AssignNullToNotNullAttribute
            // come on, R#, you should know null first parameter means static member here
            return Expression.MakeMemberAccess(obj, member);
            // ReSharper restore AssignNullToNotNullAttribute
        }

        public override Expression VisitArrayAccess([NotNull]PseudoExpressionParser.ArrayAccessContext context)
        {
            var obj = Visit(context.expression(0));
            var index = Visit(context.expression(1));
            if (!obj.Type.IsArray)
            {
                throw new InvalidSyntaxException(context, string.Format("unable to apply array index to non-array type {0}", obj.Type));
            }
            if (index.Type != typeof(int))
            {
                throw new InvalidSyntaxException(context, string.Format("unable to apply array index with index type {0}", index.Type));
            }
            return Expression.ArrayIndex(obj, index);
        }

        private Type GetTypeObject([NotNull]Expression obj)
        {
            var objType = obj as TypeExpression;
            if (objType != null)
            {
                return objType.TypeObj;
            }
            return null;
        }
    }
}
