﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Antlr4.Runtime;
    using Antlr4.Runtime.Misc;
    using Antlr4.Runtime.Tree;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions;

    /// <summary>
    /// Неправильно сформированное выражение
    /// </summary>
    public class InvalidSyntaxException : Exception
    {
        /// <summary>
        /// Неправильно сформированное выражение
        /// </summary>
        /// <param name="element">Элемент дерева разбора, в котором обнаружена ошибка</param>
        /// <param name="message">Сообщение об ошибке</param>
        /// <param name="inner">Причина ошибки</param>
        public InvalidSyntaxException(IParseTree element, string message, RecognitionException inner = null)
            : base(string.Format("Invalid expression '{0}' : {1}", element != null ? element.GetText() : string.Empty, message), inner)
        {
        }

        /// <summary>
        /// Неправильно сформированное выражение
        /// </summary>
        /// <param name="element">Токен, при анализе которого обнаружена ошибка</param>
        /// <param name="message">Сообщение об ошибке</param>
        /// <param name="inner">Причина ошибки</param>
        public InvalidSyntaxException(IToken element, string message, RecognitionException inner = null)
            : base(string.Format("Invalid expression '{0}' : {1}", element != null ? element.Text : string.Empty, message), inner)
        {
        }

        /// <summary>
        /// Неправильно сформированное выражение
        /// </summary>
        /// <param name="symbol">Символ, в котором обнаружена ошибка</param>
        /// <param name="message">Сообщение об ошибке</param>
        /// <param name="inner">Причина ошибки</param>
        public InvalidSyntaxException(int symbol, string message, RecognitionException inner = null)
            : base(string.Format("Invalid symbol in position {0} : {1}", symbol, message), inner)
        {
        }
    }

    /// <summary>
    /// Не удалось найти подходящий метод
    /// </summary>
    public class UnresolvedMethodException : InvalidSyntaxException
    {
        /// <summary>
        /// Не удалось найти подходящий метод
        /// </summary>
        public UnresolvedMethodException(
            IParseTree element,
            string methodName,
            Type instanceType)
            : base(element, GetMessage(methodName, instanceType), null)
        {
        }

        /// <summary>
        /// Не удалось найти подходящий метод
        /// </summary>
        public UnresolvedMethodException(
            IParseTree element,
            string methodName,
            Type instanceType,
            IEnumerable<Type> paramTypes,
            IEnumerable<MethodInfo> candidates)
            : base(element, GetMessage(methodName, instanceType, paramTypes, candidates), null)
        {
        }

        private static string GetMessage(string methodName, Type instanceType)
        {
            return string.Format("unable to resolve method {0} of {1}", methodName, instanceType);
        }

        private static string GetMessage(string methodName, Type instanceType, IEnumerable<Type> paramTypes, IEnumerable<MethodInfo> candidates)
        {
            return string.Format("unable to resolve method {0} of {1} with parameters {2}{3}", 
                methodName,
                instanceType,
                string.Join(", ", (paramTypes ?? Enumerable.Empty<Type>()).Select(t => t == null || t == typeof(UnparsedLambdaExpression) ? "unparsed lambda" : t.ToString())),
                candidates != null
                    ? string.Format("Candidates: {0}", string.Join(", ", candidates))
                    : string.Empty);
        }
    }
}
