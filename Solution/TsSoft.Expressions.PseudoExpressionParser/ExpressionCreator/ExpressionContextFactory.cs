﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.PseudoExpressionParser.Resolvers;

    internal class ExpressionContextFactory : IExpressionContextFactory
    {
        [NotNull]private readonly IMethodResolver _methodResolver;
        [NotNull]private readonly IMemberInfoLibrary _memberInfoLibrary;

        public ExpressionContextFactory([NotNull]IMethodResolver methodResolver, [NotNull]IMemberInfoLibrary memberInfoLibrary)
        {
            if (methodResolver == null) throw new ArgumentNullException("methodResolver");
            if (memberInfoLibrary == null) throw new ArgumentNullException("memberInfoLibrary");
            _methodResolver = methodResolver;
            _memberInfoLibrary = memberInfoLibrary;
        }

        public IExpressionContext CreateContext(ExpressionContextSettings settings)
        {
            return new ExpressionContext(_methodResolver, _memberInfoLibrary, settings);
        }
    }
}
