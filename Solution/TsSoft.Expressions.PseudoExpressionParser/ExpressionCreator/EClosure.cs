﻿// ReSharper disable once CheckNamespace
namespace EClosure
{
    using System;
    using System.Linq.Expressions;

    internal class EClosure
    {
        public static Expression Closure<T>(T value)
        {
            Expression<Func<T>> expr = () => value;
            return expr.Body;
        }
    }
}
