namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// ��������� ������� ���������
    /// </summary>
    public class ExpressionContextSettings
    {
        /// <summary>
        /// ����������� ��� ��������� � ���������
        /// </summary>
        public bool ClosureOverConstants { get; set; }

        /// <summary>
        /// ��������� ���������� � ����������� ������ ������������ �������
        /// </summary>
        public bool AllowStaticCallsOnTypeConstants { get; set; }

        /// <summary>
        /// ���� ����������
        /// </summary>
        public IReadOnlyCollection<Type> ExtensionTypes { get; set; }
    }
}
