﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.PseudoExpressionParser.Resolvers;

    /// <summary>
    /// Статическая фабрика-парсер выражений
    /// </summary>
    public static class ExpressionFactory
    {
        private static IExpressionCreator _engine;

        [NotNull]
        private static IExpressionCreator Engine
        {
            get
            {
                return _engine ?? (_engine = new ExpressionCreator(
                       new ExpressionContextFactory(
                           new SimpleMethodResolver(new ParameterTypeMatcher(new LambdaBodyMaker(), new DelegateTypeHelper())),
                           new MemberInfoLibrary(new MemberInfoHelper()))));
            }
        }

        /// <summary>
        /// Разобрать выражение без параметров
        /// </summary>
        /// <typeparam name="TOut">Тип результата</typeparam>
        /// <param name="expression">Строковое представление выражения</param>
        /// <returns>Разобранное выражение</returns>
        public static Expression<Func<TOut>> Parse<TOut>([NotNull]string expression)
        {
            return Engine.CreateExpression<TOut>(expression, true);
        }

        /// <summary>
        /// Разобрать выражение с одним параметром
        /// </summary>
        /// <typeparam name="TOut">Тип результата</typeparam>
        /// <typeparam name="TIn">Тип входного параметра</typeparam>
        /// <param name="inParam">Имя входного параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <returns>Разобранное выражение</returns>
        public static Expression<Func<TIn, TOut>> Parse<TIn, TOut>([NotNull] string inParam, [NotNull]string expression)
        {
            return Engine.CreateExpression<TIn, TOut>(inParam, expression, true);
        }

        /// <summary>
        /// Разобрать выражение с двумя параметрами
        /// </summary>
        /// <typeparam name="TOut">Тип результата</typeparam>
        /// <typeparam name="TIn1">Тип первого параметра</typeparam>
        /// <typeparam name="TIn2">Тип второго параметра</typeparam>
        /// <param name="inParamTwo">Имя второго параметра</param>
        /// <param name="expression">Строковое представление выражения</param>
        /// <param name="inParamOne">Имя первого параметра</param>
        /// <returns>Разобранное выражение</returns>
        public static Expression<Func<TIn1, TIn2, TOut>> Parse<TIn1, TIn2, TOut>([NotNull] string inParamOne, [NotNull] string inParamTwo, [NotNull]string expression)
        {
            return Engine.CreateExpression<TIn1, TIn2, TOut>(inParamOne, inParamTwo, expression, true);
        }
    }
}
