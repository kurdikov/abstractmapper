﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using Antlr4.Runtime;
    using JetBrains.Annotations;
    using ParserGenerator;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionExtensions;
    using TsSoft.Expressions.PseudoExpressionParser.Resolvers;

    /// <summary>
    /// Контекст разбора выражения
    /// </summary>
    public class ExpressionContext : IExpressionContext, IAntlrErrorListener<int>, IAntlrErrorListener<IToken>
    {
        [NotNull]private readonly PseudoExpressionVisitor _pseudoExpressionVisitor;

        /// <summary>
        /// Создать контекст разбора выражения
        /// </summary>
        /// <param name="methodResolver">Разрешитель перегрузок методов</param>
        /// <param name="memberLibrary">Библиотека стандартных методов</param>
        /// <param name="settings">Настройки</param>
        public ExpressionContext(
            [NotNull]IMethodResolver methodResolver, 
            [NotNull]IMemberInfoLibrary memberLibrary,
            [NotNull]ExpressionContextSettings settings)
        {
            if (methodResolver == null) throw new ArgumentNullException("methodResolver");
            if (memberLibrary == null) throw new ArgumentNullException("memberLibrary");
            if (settings == null) throw new ArgumentNullException("settings");
            _pseudoExpressionVisitor = new PseudoExpressionVisitor(
                methodResolver,
                memberLibrary,
                new Dictionary<string, Expression>(),
                settings.ClosureOverConstants,
                settings.AllowStaticCallsOnTypeConstants);
            if (settings.ExtensionTypes != null)
            {
                foreach (var type in settings.ExtensionTypes)
                {
                    if (type != null)
                    {
                        AddExtensionType(type);
                    }
                }
            }
        }

        /// <summary>
        /// Добавить параметр в контекст
        /// </summary>
        /// <param name="type">Тип параметра</param>
        /// <param name="name">Имя параметра</param>
        /// <returns>Добавленное выражение-параметр</returns>
        public ParameterExpression AddParameter(Type type, string name)
        {
            if (_pseudoExpressionVisitor.Variables.ContainsKey(name))
            {
                throw new ArgumentException(string.Format("Context already contains an identifier {0}", name));
            }
            var param = Expression.Parameter(type, name);
            _pseudoExpressionVisitor.Variables.Add(name, param);
            return param;
        }

        /// <summary>
        /// Добавить в контекст поиска методов тип расширения
        /// </summary>
        /// <param name="extensionType">Тип расширения</param>
        public void AddExtensionType(Type extensionType)
        {
            _pseudoExpressionVisitor.AddExtensionType(extensionType);
        }

        /// <summary>
        /// Добавить в контекст поиска методов тип со статическими методами
        /// </summary>
        /// <param name="type">Тип со статическими методами</param>
        /// <param name="name">Идентификатор для обращения к методам</param>
        public void AddClass(Type type, string name = null)
        {
            name = name ?? type.Name;
            if (_pseudoExpressionVisitor.Variables.ContainsKey(name))
            {
                throw new ArgumentException(string.Format("Context already contains an identifier {0}", name));
            }
            _pseudoExpressionVisitor.Variables.Add(name, new TypeExpression(type));
        }

        /// <summary>
        /// Разобрать выражение
        /// </summary>
        /// <param name="expression">Строковое представление выражения</param>
        /// <returns>Разобранное выражение</returns>
        public Expression Parse(string expression)
        {
            var input = new AntlrInputStream(expression);
            var lexer = new PseudoExpressionLexer(input);
            lexer.AddErrorListener(this);
            var tokens = new CommonTokenStream(lexer);
            var parser = new PseudoExpressionParser(tokens);
            parser.AddErrorListener(this);
            var tree = parser.expression();
            return _pseudoExpressionVisitor.Visit(tree);
        }

        void IAntlrErrorListener<int>.SyntaxError(IRecognizer recognizer, int offendingSymbol, int line, int charPositionInLine, string msg,
            RecognitionException e)
        {
            throw new InvalidSyntaxException(offendingSymbol, msg, e);
        }

        void IAntlrErrorListener<IToken>.SyntaxError(IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg,
            RecognitionException e)
        {
            throw new InvalidSyntaxException(offendingSymbol, msg, e);
        }

        /// <summary>
        /// Добавить тип со статическими методами
        /// </summary>
        /// <param name="type">Тип со статическими методами</param>
        /// <param name="name">Идентификатор для обращения к статическим методам (по умолчанию - имя типа)</param>
        public void AddClass(TypeExpression type, string name = null)
        {
            _pseudoExpressionVisitor.Variables.Add(name ?? type.TypeObj.Name, type);
        }
    }
}
