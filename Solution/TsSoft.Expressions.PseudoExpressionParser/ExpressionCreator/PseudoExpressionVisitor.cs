﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Antlr4.Runtime.Tree;
    using JetBrains.Annotations;
    using ParserGenerator;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.PseudoExpressionParser.Resolvers;

    internal partial class PseudoExpressionVisitor : PseudoExpressionBaseVisitor<Expression>
    {
        [NotNull]
        public IDictionary<string, Expression> Variables { get; set; }

        private readonly bool _closureOverConstants;
        private readonly bool _allowStaticCallsOnTypeConstants;

        public PseudoExpressionVisitor(
            [NotNull]IMethodResolver methodResolver,
            [NotNull]IMemberInfoLibrary memberLibrary, 
            [NotNull]IDictionary<string, Expression> variables,
            bool closureOverConstants,
            bool allowStaticCallsOnTypeConstants)
        {
            if (methodResolver == null) throw new ArgumentNullException("methodResolver");
            if (memberLibrary == null) throw new ArgumentNullException("memberLibrary");
            if (variables == null) throw new ArgumentNullException("variables");
            Variables = variables;
            _memberLibrary = memberLibrary;
            _methodResolver = methodResolver;
            _closureOverConstants = closureOverConstants;
            _allowStaticCallsOnTypeConstants = allowStaticCallsOnTypeConstants;
        }

        [NotNull]
        public override Expression Visit(IParseTree tree)
        {
            var result = base.Visit(tree);
            if (result == null)
            {
                throw new InvalidSyntaxException(tree, "visitor returned null");
            }
            return result;
        }

        [NotNull]
        private readonly static IReadOnlyDictionary<string, Type> Builtin = new Dictionary<string, Type>
        {
            {"bool", typeof(bool)},
            {"byte", typeof(byte)},
            {"sbyte", typeof(sbyte)},
            {"char", typeof(char)},
            {"decimal", typeof(decimal)},
            {"double", typeof(double)},
            {"float", typeof(float)},
            {"int", typeof(int)},
            {"uint", typeof(uint)},
            {"long", typeof(long)},
            {"ulong", typeof(ulong)},
            {"object", typeof(object)},
            {"short", typeof(short)},
            {"ushort", typeof(ushort)},
            {"string", typeof(string)},
        };

        [ContractAnnotation("type: null => null; type: notnull => notnull")]
        private Type ResolveType(IParseTree tree, string type)
        {
            if (type == null)
            {
                return null;
            }
            Type result;
            if (!Builtin.TryGetValue(type, out result) || result == null)
            {
                result = Type.GetType(type.Replace('/', ','));
            }
            if (result == null)
            {
                throw new InvalidSyntaxException(tree, string.Format("Unknown type {0}", type));
            }
            return result;
        }
    }
}
