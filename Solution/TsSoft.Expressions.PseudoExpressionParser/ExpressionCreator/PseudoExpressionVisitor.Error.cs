﻿namespace TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator
{
    using System.Linq.Expressions;
    using Antlr4.Runtime.Tree;

    partial class PseudoExpressionVisitor
    {
        public override Expression VisitErrorNode(IErrorNode node)
        {
            throw new InvalidSyntaxException(node, "");
        }
    }
}
