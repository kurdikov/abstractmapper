﻿#if NET45
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using BaseTestClasses;
using BaseTestClasses.Database;
using JetBrains.Annotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TsSoft.AbstractMapper.Factory;
using TsSoft.AbstractMapper.Rules;
using TsSoft.Expressions.Helpers.DbFunctions;

namespace TsSoft.AbstractMapper.Select
{
    [TestClass]
    public class SelectProviderMapperDbFunctionsTests
    {
        private class MapperWithDbFunctionInCondition : SelectProviderMapper<Zero, SimpleTo>
        {
            public MapperWithDbFunctionInCondition([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, Zero> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, Zero>
                    {
                        {t => t.Id, z => z.Firsts.Where(f => DbFunctions.Reverse(f.Name) == "abc").Select(f => f.Id), s => s.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithDbFunctionInCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithDbFunctionInCondition>(
                MapperTypes.None, Entities.Are(typeof(Zero), typeof(First)));
            var actual = mapper.Select;
            Expression<Func<Zero, object>> expected =
                z => new Zero
                {
                    Firsts = z.Firsts.Where(f => DbFunctionsImplementation.Reverse(f.Name) == "abc").Select(f => new First
                    {
                        Id = f.Id, Name = f.Name
                    }).ToList()
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }


        private class MapperWithDbFunctionWithConstantParametersInCondition : SelectProviderMapper<Zero, SimpleTo>
        {
            public MapperWithDbFunctionWithConstantParametersInCondition([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, Zero> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, Zero>
                    {
                        {t => t.Id, z => z.Firsts.Where(f => f.ImportantMoment == DbFunctions.CreateDateTime(f.Id, f.Id, f.Id, f.Id, f.Id, 23)).Select(f => f.Name), s => s.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithDbFunctionWithConstantParametersInCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithDbFunctionWithConstantParametersInCondition>(
                MapperTypes.None, Entities.Are(typeof(Zero), typeof(First)));
            var actual = mapper.Select;
            Expression<Func<Zero, object>> expected =
                z => new Zero
                {
                    Firsts = z.Firsts.Where(f => f.ImportantMoment == DbFunctionsImplementation.CreateDateTime(f.Id, f.Id, f.Id, f.Id, f.Id, 23)).Select(f => new First
                    {
                        Id = f.Id,
                        Name = f.Name,
                        ImportantMoment = f.ImportantMoment,
                    }).ToList()
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }
    }
}
#endif
