﻿// ReSharper disable ClassNeverInstantiated.Local
namespace TsSoft.AbstractMapper.Select.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using BaseTestClasses.Database;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Helpers.DbFunctions;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.SelectBuilder.Models;

    [TestClass]
    public class SelectProviderMapperTests
    {

        private class MapperWithPathReferencingOuterParameter : SelectProviderMapper<From, SimpleToWithCollection>
        {
            public MapperWithPathReferencingOuterParameter(IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleToWithCollection, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleToWithCollection, From>
                    {
                        {t => t.Children, f => f.Children.Where(c => c.Id == f.Id)},
                        {t => t.Children2, f => f.Children.Where(c => c.Bool)},
                    };
                }
            }

            protected override IIgnoreRules<SimpleToWithCollection> IgnoreRules
            {
                get { return new IgnoreRules<SimpleToWithCollection>(); }
            }
        }

        [TestMethod]
        public void TestPathWithWhereWithReferenceToOuterParameter()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithPathReferencingOuterParameter>(
                MapperTypes.Are(typeof(NotAMapper)), Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected = f =>
                    new DynamicFrom
                    {
                        Children = f.Children.Where(c => c.Id == f.Id || c.Bool).Select(c => new From {Id = c.Id, Bool = c.Bool}),
                        Id = f.Id
                    };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithPathWithCondition : SelectProviderMapper<From, SimpleToWithCollection>
        {
            public MapperWithPathWithCondition(IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleToWithCollection, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleToWithCollection, From>
                    {
                        {t => t.Children2, f => f.Children.Where(c => c.Bool)}
                    };
                }
            }

            protected override IIgnoreRules<SimpleToWithCollection> IgnoreRules
            {
                get
                {
                    return new IgnoreRules<SimpleToWithCollection>
                    {
                        t => t.Children
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithWhere()
        {
            var mapper2 = AbstractMapperFactory.Create<MapperWithPathWithCondition>(MapperTypes.Are(typeof(NotAMapper)), Entities.Are(typeof(From)));
            var actual = mapper2.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom { Children = f.Children.Where(c => c.Bool).Select(c => new From { Bool = c.Bool }) };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithPathWithConditionWithUnaryOperator : SelectProviderMapper<From, SimpleToWithCollection>
        {
            public MapperWithPathWithConditionWithUnaryOperator(IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleToWithCollection, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleToWithCollection, From>
                    {
                        {t => t.Children2, f => f.Children.Where(c => !c.Bool)}
                    };
                }
            }

            protected override IIgnoreRules<SimpleToWithCollection> IgnoreRules
            {
                get
                {
                    return new IgnoreRules<SimpleToWithCollection>
                    {
                        t => t.Children
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithWhereWithUnaryOperator()
        {
            var mapper3 = AbstractMapperFactory.Create<MapperWithPathWithConditionWithUnaryOperator>(MapperTypes.Are(typeof(NotAMapper)), Entities.Are(typeof(From)));
            var actual = mapper3.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom { Children = f.Children.Where(c => !c.Bool).Select(c => new From { Bool = c.Bool }) };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class ClassWithoutProcessorMapper : SelectProviderMapper<TestEntity, SimpleToWithCollection>
        {
            public ClassWithoutProcessorMapper(IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleToWithCollection, TestEntity> MapRules
            {
                get
                {
                    return new MapRules<SimpleToWithCollection, TestEntity>
                    {
                        {t => t.Children, f => f.Children, entities => new List<SimpleTo>() },
                        {t => t.Children2, f => f.Parent, p => new List<SimpleTo>()}
                    };
                }
            }

            protected override IIgnoreRules<SimpleToWithCollection> IgnoreRules
            {
                get { return new IgnoreRules<SimpleToWithCollection>(); }
            }
        }

        [TestMethod]
        public void TestPathsEndingOnEntities()
        {
            var mapperWithoutInner = AbstractMapperFactory.Create<ClassWithoutProcessorMapper>(MapperTypes.None, Entities.Are(typeof(TestEntity)));
            var tActual = mapperWithoutInner.Select;
            Expression<Func<TestEntity, object>> tExpected = f => new DynamicTestEntity
            {
                Children = f.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1, Prop2 = c.Prop2, Prop3 = c.Prop3, ExternalId = c.ExternalId }),
                Parent = f.Parent != null ? new DynamicTestEntity { Prop1 = f.Parent.Prop1, Prop2 = f.Parent.Prop2, Prop3 = f.Parent.Prop3, ExternalId = f.Parent.ExternalId } : null,
            };
            SelectAssert.AreEqual(tActual.Select, tExpected);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(tActual.Select));
        }


        private class InterfaceCastingMapper : SelectProviderMapper<From, SimpleTo>
        {
            public InterfaceCastingMapper(IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new EntityMapRules<SimpleTo, From>
                    {
                        {t => t.Id, t => (IFrom)t, SelectProviderRule.Rule, TestEntitySelectProcessor.Proc, (@from, processor) => processor.Process(from as From)}
                    };
                }
            }

            protected override IIgnoreRules<SimpleTo> IgnoreRules
            {
                get { return new IgnoreRules<SimpleTo>(); }
            }
        }

        [TestMethod]
        public void TestPathEndingOnEntityInterfaceWithProcessor()
        {
            var mapperWithInterfaceCast = AbstractMapperFactory.Create<InterfaceCastingMapper>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapperWithInterfaceCast.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom { Id = f.Id, Name = f.Name };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class DerivedMapper : SelectProviderMapper<FromDerived, NotSoSimpleTo>
        {
            public DerivedMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<NotSoSimpleTo, FromDerived> MapRules
            {
                get
                {
                    return new MapRules<NotSoSimpleTo, FromDerived>
                    {
                        {t => t.NotSimple, f => f.Derived},
                        {t => t.Id , f => f.Id},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithBaseClassProperty()
        {
            var derivedMapper = AbstractMapperFactory.Create<DerivedMapper>(
                MapperTypes.None, Entities.Are(typeof(FromDerived)));
            var derivedActual = derivedMapper.Select;
            Expression<Func<FromDerived, object>> derivedExpected =
                f => new DynamicFrom { Id = f.Id, Derived = f.Derived };
            SelectAssert.AreEqual(derivedActual.Select, derivedExpected);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(derivedActual.Select));
        }


        private class MapperWithDependentProps : SelectProviderMapper<From, NotSoSimpleTo>
        {
            public MapperWithDependentProps([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<NotSoSimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<NotSoSimpleTo, From>
                    {
                        { t => t.Id, f => f.Dependent, s => 0},
                        {t => t.NotSimple, f => f.Id},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithNotMappedDependentProperty()
        {
            var dependMapper = AbstractMapperFactory.Create<MapperWithDependentProps>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = dependMapper.Select;
            Expression<Func<From, object>> expected = f => new DynamicFrom { Id = f.Id, Bool = f.Bool, NullableBool = f.NullableBool };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class WrappedNavigationMapper : SelectProviderMapper<From, NotSoSimpleTo>
        {
            public WrappedNavigationMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<NotSoSimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<NotSoSimpleTo, From>
                    {
                        {t => t.Id, f => f.Id},
                        {t => t.NotSimple, f => f.AllChildren.Select(c => c.Id), ints => ints.First()}
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithNotMappedPropertyWrappingTwoNavigations()
        {
            var wnMapper = AbstractMapperFactory.Create<WrappedNavigationMapper>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = wnMapper.Select;
            Expression<Func<From, object>> expected = f => new DynamicFrom { Id = f.Id, Children = f.Children.Select(c => new DynamicFrom { Id = c.Id }), Bastards = f.Bastards.Select(c => new DynamicFrom { Id = c.Id }) };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class WrappedNavigationWithConditionMapper : SelectProviderMapper<From, NotSoSimpleTo>
        {
            public WrappedNavigationWithConditionMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<NotSoSimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<NotSoSimpleTo, From>
                    {
                        {t => t.Id, f => f.Id},
                        {t => t.NotSimple, f => f.AllChildren.Where(c => c.Name == "2").Select(c => c.Id), ints => ints.First()}
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithNotMappedPropertyWrappingTwoNavigationsAndWhere()
        {
            var wnMapper2 = AbstractMapperFactory.Create<WrappedNavigationWithConditionMapper>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = wnMapper2.Select;
            Expression<Func<From, object>> expected = f => new DynamicFrom
            {
                Id = f.Id,
                Children = f.Children.Where(c => c.Name == "2").Select(c => new DynamicFrom { Id = c.Id, Name = c.Name }),
                Bastards = f.Bastards.Where(c => c.Name == "2").Select(c => new DynamicFrom { Id = c.Id, Name = c.Name })
            };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithAdditionalSelect : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithAdditionalSelect(
                [NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IReadOnlySelectExpression<From> AdditionalSelect
            {
                get { return new SelectExpression<From> { Select = f => new { f.Name } }; }
            }
        }

        [TestMethod]
        public void TestMapperWithAdditionalSelect()
        {
            var additionalSelectMapper = AbstractMapperFactory.Create<MapperWithAdditionalSelect>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = additionalSelectMapper.Select;
            Expression<Func<From, object>> expected = f => new DynamicFrom { Id = f.Id, Name = f.Name };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class NestedSelectMapper : SelectProviderMapper<From, SimpleNestedTo>
        {
            public NestedSelectMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            public override int MaxNestingLevel
            {
                get { return 0; }
            }
        }

        [TestMethod]
        public void TestNestedMapperWithZeroNesting()
        {
            var mapper = AbstractMapperFactory.Create<NestedSelectMapper>(
                MapperTypes.Are(typeof(NestedSelectMapper)), Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected = f => new DynamicFrom {Id = f.Id};
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class NestedSelectMapper2 : NestedSelectMapper
        {
            public NestedSelectMapper2([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            public override int MaxNestingLevel
            {
                get { return 1; }
            }
        }

        [TestMethod]
        public void TestNestedMapperWithOneNestingLevel()
        {
            var mapper = AbstractMapperFactory.Create<NestedSelectMapper2>(
                MapperTypes.Are(typeof(NestedSelectMapper2)), Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected = f =>
                new DynamicFrom
                {
                    Id = f.Id,
                    Parent = f.Parent != null ? new DynamicFrom {Id = f.Parent.Id} : null,
                    Children = f.Children.Select(c => new DynamicFrom {Id = c.Id})
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class NestedSelectMapper3 : NestedSelectMapper
        {
            public NestedSelectMapper3([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            public override int MaxNestingLevel
            {
                get { return 2; }
            }
        }

        [TestMethod]
        public void TestNestedMapperWithTwoNestingLevels()
        {
            var mapper = AbstractMapperFactory.Create<NestedSelectMapper3>(
                MapperTypes.Are(typeof(NestedSelectMapper3)), Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected = f => new DynamicFrom
            {
                Id = f.Id, 
                Parent = f.Parent != null ? new DynamicFrom
                {
                    Id = f.Parent.Id, 
                    Parent = f.Parent.Parent != null ? new DynamicFrom {Id = f.Parent.Parent.Id} : null, 
                    Children = f.Parent.Children.Select(c => new DynamicFrom {Id = c.Id}),
                } : null, 
                Children = f.Children.Select(c => new DynamicFrom
                {
                    Id = c.Id,
                    Parent = c.Parent != null ? new DynamicFrom {Id = c.Parent.Id} : null,
                    Children = c.Children.Select(cc => new DynamicFrom {Id = cc.Id}),
                })
            };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class NoMapTimeInjectionMapper : SelectProviderMapper<From, SimpleNestedTo>
        {
            public NoMapTimeInjectionMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override InnerMapperStrategy InnerMapperStrategy
            {
                get { return InnerMapperStrategy.StaticTypes | InnerMapperStrategy.ResolveOnMapperBuilding; }
            }
        }

        [TestMethod]
        public void TestBuildTimeResolutionMapper()
        {
            var entityTypesRetriever = new CustomEnvironmentTestEntityTypesRetriever(Entities.Are(typeof(From)), External.None);
            var mapperResolver = new Mock<IMapperResolver>(MockBehavior.Strict);
            var mockMapper = new Mock<ISelectProviderMapper<From, SimpleNestedTo>>(MockBehavior.Strict);
            mockMapper.Setup(m => m.Select).Returns(new SelectExpression<From>{Select = f => new {}});
            mapperResolver.Setup(r => r.TryGet<From, SimpleNestedTo>()).Returns(mockMapper.Object).Verifiable();
            var mapper = AbstractMapperFactory.Create<NoMapTimeInjectionMapper>(entityTypesRetriever, mapperResolver.Object);

            var res = mapper.GenerateSelect(new GeneratorContext(mapper.GetType(), 0));
            Assert.IsNotNull(res);

            res = mapper.GenerateSelect(new GeneratorContext(mapper.GetType(), 0));
            Assert.IsNotNull(res);

            mapperResolver.Verify(r => r.TryGet<From, SimpleNestedTo>(), Times.Exactly(2));
        }


        private class MapTimeInjectionMapper : SelectProviderMapper<From, SimpleNestedTo>
        {
            public MapTimeInjectionMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override InnerMapperStrategy InnerMapperStrategy
            {
                get { return InnerMapperStrategy.StaticTypes | InnerMapperStrategy.ResolveOnMapping; }
            }
        }

        [TestMethod]
        public void TestMapTimeResolutionMapper()
        {
            var entityTypesRetriever = new CustomEnvironmentTestEntityTypesRetriever(Entities.Are(typeof(From)), External.None);
            var mapperResolver = new Mock<IMapperResolver>(MockBehavior.Strict);
            var mockMapper = new Mock<ISelectProviderMapper<From, SimpleNestedTo>>(MockBehavior.Strict);
            mockMapper.Setup(m => m.Select).Returns(new SelectExpression<From> { Select = f => new { } });
            mapperResolver.Setup(r => r.TryGet<From, SimpleNestedTo>()).Returns(mockMapper.Object).Verifiable();
            var mapper = AbstractMapperFactory.Create<MapTimeInjectionMapper>(entityTypesRetriever, mapperResolver.Object);

            var res = mapper.GenerateSelect(new GeneratorContext(mapper.GetType(), 0));
            Assert.IsNotNull(res);

            res = mapper.GenerateSelect(new GeneratorContext(mapper.GetType(), 0));
            Assert.IsNotNull(res);

            mapperResolver.Verify(r => r.TryGet<From, SimpleNestedTo>(), Times.Exactly(4));
        }


        private class MapperWithoutNullChecks : SelectProviderMapper<ThirdWithPkPk, SimpleTo>
        {
            public MapperWithoutNullChecks([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, ThirdWithPkPk> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, ThirdWithPkPk>
                {
                    {t => t.Id, p => p.Second.First.Zero.Id}
                };
                }
            }

            protected override SelectSettings SelectSettings
            {
                get { return new SelectSettings { SkipNullChecks = true }; }
            }
        }

        private class MapperWithoutNullChecksWithException : MapperWithoutNullChecks
        {
            public MapperWithoutNullChecksWithException([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override SelectSettings SelectSettings
            {
                get
                {
                    var result = base.SelectSettings;
                    result.AlwaysCheckNull = new HashSet<ValueHoldingMember>(MemberInfoComparer.Instance)
                    {
                        new ValueHoldingMember(typeof(SecondWithFkPk).GetTypeInfo().GetProperty("First"))
                    };
                    return result;
                }
            }
        }

        [TestMethod]
        public void TestMapperWithoutNullChecks()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithoutNullChecks>(
                MapperTypes.None,
                Entities.Are(typeof(ThirdWithPkPk), typeof(SecondWithFkPk), typeof(First), typeof(Zero)));
            var actual = mapper.Select;
            Expression<Func<ThirdWithPkPk, object>> expected = t => 
                new DynamicEntity
                {
                    Second = t.Second != null 
                        ? new DynamicEntity
                        {
                            First = new DynamicEntity {Zero = t.Second.First.Zero != null ? new DynamicEntity {Id = t.Second.First.Zero.Id} : null}
                        } 
                        : null
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));

            mapper = AbstractMapperFactory.Create<MapperWithoutNullChecksWithException>(
                MapperTypes.None,
                Entities.Are(typeof(ThirdWithPkPk), typeof(SecondWithFkPk), typeof(First), typeof(Zero)));
            actual = mapper.Select;
            expected = t =>
                new DynamicEntity
                {
                    Second = t.Second != null
                        ? new DynamicEntity
                        {
                            First = t.Second.First != null ? new DynamicEntity { Zero = t.Second.First.Zero != null ? new DynamicEntity { Id = t.Second.First.Zero.Id } : null } : null
                        }
                        : null
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private static class RuleProvider
        {
            public static MapRules<SimpleTo, TEntity> MakeRule<TEntity>()
                where TEntity : class, IFrom
            {
                return new MapRules<SimpleTo, TEntity>
                {
                    {m => m.Id, e => e.Id}
                };
            }

            public static MapRules<NotSoSimpleTo, TEntity> MakeRuleForTwoProps<TEntity>()
                where TEntity : class, IComplexFrom
            {
                return new MapRules<NotSoSimpleTo, TEntity>
                {
                    {m => m.Id, e => e.InterfaceChildren.Where(c => c.Name == "1").Select(c => c.Id), ints => ints.Sum()},
                };
            }
        }

        private class MapperWithGenericRuleProvider : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithGenericRuleProvider([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get { return RuleProvider.MakeRule<From>(); }
            }
        }

        [TestMethod]
        public void TestMapperWithInterfaceRuleProvider()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithGenericRuleProvider>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected = f => new DynamicFrom {Id = f.Id};
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }



        private class MapperWithGenericRuleWithConditionFromProvider
            : SelectProviderMapper<From, NotSoSimpleTo>
        {
            public MapperWithGenericRuleWithConditionFromProvider([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<NotSoSimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<NotSoSimpleTo, From>
                    {
                        RuleProvider.MakeRuleForTwoProps<From>(),
                        {t => t.NotSimple, f => f.Children.Where(c => c.Bool).Select(c => c.Guid), guids => guids.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithProvidedRuleWithCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithGenericRuleWithConditionFromProvider>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Bool || c.Name == "1").Select(c => new DynamicFrom { Id = c.Id, Name = c.Name, Bool = c.Bool, Guid = c.Guid }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class InterfaceWrappedNavigationMapper<T> : SelectProviderMapper<T, SimpleTo>
            where T : class, ITwoFroms
        {
            public InterfaceWrappedNavigationMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, T> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, T>
                    {
                        {t => t.Id, f => f.AllChildren.Select(c => c.Id), ints => ints.First()}
                    };
                }
            }
        }

        [TestMethod]
        public void TestNavigationWrapperInterfaceMapper()
        {
            var mapper = AbstractMapperFactory.Create<InterfaceWrappedNavigationMapper<From>>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f =>
                    new DynamicFrom
                    {
                        Children = f.Children.Select(c => new DynamicFrom {Id = c.Id}),
                        Bastards = f.Bastards.Select(c => new DynamicFrom {Id = c.Id})
                    };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithCollectionProvider : SelectProviderMapper<From, SimpleTo>
        {
            private class InnerProvider : ISelectProvider<From>
            {
                public IReadOnlySelectExpression<From> Select { get { return new SelectExpression<From> { Select = f => new { f.Id } }; } }

                public int Extract([NotNull][UsedImplicitly]IReadOnlyCollection<From> collection)
                {
                    return collection.Sum(f => f.Id);
                }
            }

            public MapperWithCollectionProvider([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    var rules = new EntityMapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children, SelectCollectionProviderRule.Rule, new InnerProvider(), (froms, provider) => provider.Extract(froms.ToReadOnlyCollectionIfNeeded())},
                    };
                    return rules;
                }
            }
        }

        [TestMethod]
        public void TestMapperWithCollectionProvider()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithCollectionProvider>(MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                    {
                        Children = f.Children.Select(c => new DynamicFrom { Id = c.Id })
                    };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));

            var from = new From {Children = new From[] {new From {Id = 5}, new From {Id = 6},}};
            Assert.AreEqual(11, mapper.Map(from).Id);
        }



        private class MapperWithRuleWithConditionWithAny : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithRuleWithConditionWithAny([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Children.Any()).Select(c => c.Id), froms => froms.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithPathWithConditionWithAny()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithRuleWithConditionWithAny>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Children.Any()).Select(c => new DynamicFrom { Id = c.Id, Children = c.Children.Select(cc => new DynamicFrom { Id = cc.Id }), })
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithInnerObjectMapperHavingPathReferencingOuterParameter :
            SelectProviderMapper<From, SimpleToWithCollectionWrapper>
        {
            public MapperWithInnerObjectMapperHavingPathReferencingOuterParameter([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleToWithCollectionWrapper, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleToWithCollectionWrapper, From>
                    {
                        {f => f.SimpleToWithCollection, f => f.Parent}
                    };
                }
            }
        }


        private class MapperWithRuleWithConditionWithAnyWithLambda : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithRuleWithConditionWithAnyWithLambda([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Children.Any(cc => cc.Bool)).Select(c => c.Id), froms => froms.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithPathWithConditionWithAnyWithLambda()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithRuleWithConditionWithAnyWithLambda>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Children.Any(cc => cc.Bool)).Select(c => new DynamicFrom { Id = c.Id, Children = c.Children.Select(cc => new DynamicFrom { Bool = cc.Bool }), })
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }

        [TestMethod]
        public void TestMapperWithInnerObjectMapperHavingPathReferencingOuterParameter()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithInnerObjectMapperHavingPathReferencingOuterParameter>(
                MapperTypes.Are(typeof(MapperWithPathReferencingOuterParameter), typeof(NotAMapper)), Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Parent = f.Parent != null
                    ? new DynamicFrom
                    {
                        Children = f.Parent.Children.Where(c => c.Id == f.Parent.Id || c.Bool).Select(c => new DynamicFrom {Id = c.Id, Bool = c.Bool}),
                        Id = f.Parent.Id,
                    }
                    : null,
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithInnerCollectionMapperHavingPathReferencingOuterParameter :
            SelectProviderMapper<From, SimpleToWithCollectionCollectionWrapper>
        {
            public MapperWithInnerCollectionMapperHavingPathReferencingOuterParameter([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleToWithCollectionCollectionWrapper, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleToWithCollectionCollectionWrapper, From>
                    {
                        {f => f.SimpleToWithCollectionCollection, f => f.Children}
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithInnerCollectionMapperHavingPathReferencingOuterParameter()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithInnerCollectionMapperHavingPathReferencingOuterParameter>(
                MapperTypes.Are(typeof(MapperWithPathReferencingOuterParameter), typeof(NotAMapper)), Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Select(fc => new DynamicFrom
                    {
                        Children = fc.Children.Where(c => c.Id == fc.Id || c.Bool).Select(c => new DynamicFrom { Id = c.Id, Bool = c.Bool }),
                        Id = fc.Id,
                    })
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithThreeConditions :
            SelectProviderMapper<From, ThreePropertiesTo>
        {
            public MapperWithThreeConditions([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<ThreePropertiesTo, From> MapRules
            {
                get
                {
                    return new MapRules<ThreePropertiesTo, From>
                    {
                        {t => t.Prop1, f => f.Children.Where(c => c.Id == 1).Select(c => c.Id), froms => froms.Count()},
                        {t => t.Prop2, f => f.Children.Where(c => c.Id == 2).Select(c => c.Id), froms => froms.Count()},
                        {t => t.Prop3, f => f.Children.Where(c => c.Id == 3).Select(c => c.Id), froms => froms.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithThreeConditions()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithThreeConditions>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Id == 1 || c.Id == 2 || c.Id == 3).Select(c => new DynamicFrom { Id = c.Id }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithNullConditionAmongMany :
            SelectProviderMapper<From, ThreePropertiesTo>
        {
            public MapperWithNullConditionAmongMany([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<ThreePropertiesTo, From> MapRules
            {
                get
                {
                    return new MapRules<ThreePropertiesTo, From>
                    {
                        {t => t.Prop1, f => f.Children.Where(c => c.Id == 1).Select(c => c.Id), froms => froms.Count()},
                        {t => t.Prop2, f => f.Children.Select(c => c.Bool), froms => froms.Count()},
                        {t => t.Prop3, f => f.Children.Where(c => c.Id == 3).Select(c => c.Id), froms => froms.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithNullConditionAmongMany()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithNullConditionAmongMany>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Select(c => new DynamicFrom { Id = c.Id, Bool = c.Bool }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithFirstNullCondition :
            SelectProviderMapper<From, ThreePropertiesTo>
        {
            public MapperWithFirstNullCondition([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<ThreePropertiesTo, From> MapRules
            {
                get
                {
                    return new MapRules<ThreePropertiesTo, From>
                    {
                        {t => t.Prop1, f => f.Children.Select(c => c.Bool), froms => froms.Count()},
                        {t => t.Prop2, f => f.Children.Where(c => c.Id > 2).Select(c => c.Bool), froms => froms.Count()},
                        {t => t.Prop3, f => f.Children.Where(c => c.Id < 2).Select(c => c.Bool), froms => froms.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithFirstNullCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithFirstNullCondition>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Select(c => new DynamicFrom { Id = c.Id, Bool = c.Bool }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithRulesOnWrappedAndNonWrapped :
            SelectProviderMapper<From, ThreePropertiesTo>
        {
            public MapperWithRulesOnWrappedAndNonWrapped([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<ThreePropertiesTo, From> MapRules
            {
                get
                {
                    return new MapRules<ThreePropertiesTo, From>
                    {
                        {t => t.Prop1, f => f.Children.Where(c => c.AEnumStr == "1").Select(d => d.Bool), bools => bools.Count()},
                        {t => t.Prop2, f => f.InterfaceChildren.Where(e => e.Name == "123").Select(g => g.Id), ints => ints.Sum()}
                    };
                }
            }

            protected override IIgnoreRules<ThreePropertiesTo> IgnoreRules
            {
                get { return new IgnoreRules<ThreePropertiesTo> { t => t.Prop3 }; }
            }
        }

        [TestMethod]
        public void TestMapperWithSelectOnSameCollectionAsWrappedAndNonWrapped()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithRulesOnWrappedAndNonWrapped>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.AEnumStr == "1" || c.Name == "123").Select(c => new DynamicFrom { Id = c.Id, Bool = c.Bool, Name = c.Name, AEnumStr = c.AEnumStr }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class GenericMapperWithCondition<T> :
            SelectProviderMapper<T, SimpleTo> where T : class, IComplexFrom
        {
            public GenericMapperWithCondition([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, T> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, T>
                    {
                        {t => t.Id, f => f.InterfaceChildren.Where(c => c.Name == "123").Select(c => c.Id), s => s.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestGenericMapperWithCondition()
        {
            var mapper = AbstractMapperFactory.Create<GenericMapperWithCondition<From>>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name == "123").Select(c => new DynamicFrom { Id = c.Id, Name = c.Name}),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class GenericMapperWithTwoConditions<T> :
            SelectProviderMapper<T, NotSoSimpleTo> where T : class, IComplexFrom
        {
            public GenericMapperWithTwoConditions([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<NotSoSimpleTo, T> MapRules
            {
                get
                {
                    return new MapRules<NotSoSimpleTo, T>
                    {
                        {t => t.Id, f => f.InterfaceChildren.Where(c => c.Name == "1").Select(c => c.Name), s => s.Count()},
                        {t => t.NotSimple, f => f.InterfaceChildren.Where(c => c.Id == 2).Select(c => c.Id), s => s.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestGenericMapperWithTwoConditions()
        {
            var mapper = AbstractMapperFactory.Create<GenericMapperWithTwoConditions<From>>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name == "1" || c.Id == 2).Select(c => new DynamicFrom { Id = c.Id, Name = c.Name }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }



        private class MapperWithStringIsNullOrEmpty : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringIsNullOrEmpty([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => string.IsNullOrEmpty(c.Name)).Select(c => c.Id), froms => froms.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringIsNullOrEmpty()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringIsNullOrEmpty>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => string.IsNullOrEmpty(c.Name)).Select(c => new DynamicFrom { Id = c.Id, Name = c.Name }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithStringConcatArray : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringConcatArray([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {
                            t => t.Id,
                            f => f.Children.Where(c => c.AEnumStr == string.Concat(c.Name, c.Parent.Name, c.Parent.AEnumStr, c.Parent.Parent.Name, c.Parent.Parent.AEnumStr))
                                .Select(c => c.Id),
                            ints => ints.Count()
                        }
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringConcatArray()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringConcatArray>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.AEnumStr == string.Concat(c.Name, c.Parent.Name, c.Parent.AEnumStr, c.Parent.Parent.Name, c.Parent.Parent.AEnumStr))
                    .Select(c => new DynamicFrom
                    {
                        Parent = c.Parent != null ? new DynamicFrom
                        {
                            Name = c.Parent.Name,
                            AEnumStr = c.Parent.AEnumStr,
                            Parent = c.Parent.Parent != null ? new DynamicFrom
                            {
                                Name = c.Parent.Parent.Name,
                                AEnumStr = c.Parent.Parent.AEnumStr,
                            } : null,
                        } : null,
                        Id = c.Id,
                        Name = c.Name,
                        AEnumStr = c.AEnumStr
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }



        private class MapperWithStringInsert : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringInsert([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Name.Insert(c.Id, "0") == "10").Select(c => c.AEnumStr), ints => ints.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringInsert()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringInsert>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name.Insert(c.Id, "0") == "10")
                    .Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        Name = c.Name,
                        AEnumStr = c.AEnumStr,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithStringSubstring : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringSubstring([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Name.Substring(c.Id) == "10").Select(c => c.AEnumStr), ints => ints.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringSubstring()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringSubstring>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name.Substring(c.Id) == "10")
                    .Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        Name = c.Name,
                        AEnumStr = c.AEnumStr,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithStringSubstringWithLength : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringSubstringWithLength([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Name.Substring(c.Id, c.AEnumInt) == "10").Select(c => c.AEnumStr), ints => ints.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringSubstringWithLength()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringSubstringWithLength>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name.Substring(c.Id, c.AEnumInt) == "10")
                    .Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        Name = c.Name,
                        AEnumInt = c.AEnumInt,
                        AEnumStr = c.AEnumStr,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }



        private class MapperWithStringRemove : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringRemove([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Name.Remove(c.Id) == "10").Select(c => c.AEnumStr), ints => ints.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringRemove()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringRemove>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name.Remove(c.Id) == "10")
                    .Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        Name = c.Name,
                        AEnumStr = c.AEnumStr,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithStringRemoveWithLength : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringRemoveWithLength([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Name.Remove(c.Id, c.AEnumInt) == "10").Select(c => c.AEnumStr), ints => ints.Count()},
                    };
                }
            }
        }


        [TestMethod]
        public void TestPathWithStringRemoveWithLength()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringRemoveWithLength>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name.Remove(c.Id, c.AEnumInt) == "10")
                    .Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        Name = c.Name,
                        AEnumInt = c.AEnumInt,
                        AEnumStr = c.AEnumStr,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }

        private class MapperWithObjectToString : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithObjectToString([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Id.ToString() == "20").Select(c => c.Name), ints => ints.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithObjectToString()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithObjectToString>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Id.ToString() == "20")
                    .Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        Name = c.Name,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }



        private class MapperWithStringToLower : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringToLower([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Name.ToLower() == "abc").Select(c => c.Id), ints => ints.Sum()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringToLower()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringToLower>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name.ToLower() == "abc")
                    .Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        Name = c.Name,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithStringToUpper : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringToUpper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Name.ToUpper() == "ABC").Select(c => c.Id), ints => ints.Sum()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringToUpper()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringToUpper>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name.ToUpper() == "ABC")
                    .Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        Name = c.Name,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithStringTrim : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringTrim([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Name.Trim() == "abc").Select(c => c.Id), ints => ints.Sum()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringTrim()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringTrim>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name.Trim() == "abc")
                    .Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        Name = c.Name,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithStringTrimWithParameter : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringTrimWithParameter([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Name.Trim(' ', 'x') == "abc").Select(c => c.Id), ints => ints.Sum()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringTrimWithParameter()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringTrimWithParameter>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name.Trim(' ', 'x') == "abc")
                    .Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        Name = c.Name,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithStringTrimEnd : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringTrimEnd([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Name.TrimEnd(' ', 'x') == "abc").Select(c => c.Id), ints => ints.Sum()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringTrimEnd()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringTrimEnd>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name.TrimEnd(' ', 'x') == "abc")
                    .Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        Name = c.Name,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithStringTrimStart : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringTrimStart([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Name.TrimStart(' ', 'x') == "abc").Select(c => c.Id), ints => ints.Sum()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringTrimStart()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringTrimStart>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name.TrimStart(' ', 'x') == "abc")
                    .Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        Name = c.Name,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithEnumHasFlagWithPathArgument : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithEnumHasFlagWithPathArgument([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.AEnum1.HasFlag(c.Parent.AEnum1)).Select(c => c.Id), ints => ints.Sum()}
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithEnumHasFlagWithPathArgument()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithEnumHasFlagWithPathArgument>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.AEnum1.HasFlag(c.Parent.AEnum1)).Select(c => new DynamicFrom
                    {
                        Id = c.Id, AEnum1 = c.AEnum1, Parent = c.Parent != null ? new DynamicFrom {AEnum1 = c.Parent.AEnum1} : null 
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithEnumHasFlagWithConstantArgument : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithEnumHasFlagWithConstantArgument([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.AEnum1.HasFlag(AEnum.A)).Select(c => c.Id), ints => ints.Sum()}
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithEnumHasFlagWithConstantArgument()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithEnumHasFlagWithConstantArgument>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.AEnum1.HasFlag(AEnum.A)).Select(c => new DynamicFrom
                    {
                        Id = c.Id,
                        AEnum1 = c.AEnum1,
                    }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }



        private class MapperWithDateTimeProperty : SelectProviderMapper<DateContainer, SimpleTo>
        {
            public MapperWithDateTimeProperty([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, DateContainer> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, DateContainer>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Date.Year == 3000).Select(c => c.Time), spans => spans.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithDateTimeProperty()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithDateTimeProperty>(
                MapperTypes.None, Entities.Are(typeof(DateContainer)));
            var actual = mapper.Select;
            Expression<Func<DateContainer, object>> expected =
                f => new DateContainer
                {
                    Children = f.Children.Where(c => c.Date.Year == 3000).Select(c => new DateContainer { Date = c.Date, Time = c.Time }).ToList(),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithStringProperty : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithStringProperty([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Name.Length == 1).Select(c => c.Id), ints => ints.Sum()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithStringProperty()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithStringProperty>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Name.Length == 1).Select(c => new DynamicFrom { Id = c.Id, Name = c.Name }),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));

            var mapped = mapper.Map(
                new From
                {
                    Children = new[]
                    {
                        new From {Id = 1}, new From {Id = 2, Name = "1"}, new From {Id = 3, Name = "22"}, new From {Id = 4, Name = "3"}, new From {Id = 5},
                    }
                });
            Assert.IsNotNull(mapped);
            Assert.AreEqual(6, mapped.Id);
        }

        private class MapperWithNullableHasValue : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithNullableHasValue([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => !c.NullableBool.HasValue).Select(c => c.Id), ints => ints.Sum()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPathWithNullableHasValue()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithNullableHasValue>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => !c.NullableBool.HasValue).Select(c => new DynamicFrom { Id = c.Id, NullableBool =  c.NullableBool}),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));

            var mapped = mapper.Map(
                new From
                {
                    Children = new[]
                    {
                        new From {Id = 1}, new From {Id = 2, NullableBool = false}, new From {Id = 3}, new From {Id = 4, NullableBool = true}, new From {Id = 5},
                    }
                });
            Assert.IsNotNull(mapped);
            Assert.AreEqual(9, mapped.Id);
        }



        private class MapperWithInterfaceProvider : SelectProviderMapper<From, SimpleTo>
        {
            private class InnerProvider : ISelectProvider<IFrom>
            {
                public IReadOnlySelectExpression<IFrom> Select { get { return new SelectExpression<IFrom> { Select = f => new { f.Id, f.Name } }; } }
            }

            private readonly InnerProvider _provider = new InnerProvider();

            public MapperWithInterfaceProvider([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new EntityMapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => (IFrom)f, SelectProviderRule.Rule, _provider, (@from, provider) => from.Id},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithInterfaceProvider()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithInterfaceProvider>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Id = f.Id,
                    Name = f.Name,
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithPathRuleAndInterfaceProvider : SelectProviderMapper<From, SimpleTo>
        {
            private class InnerProvider : ISelectProvider<IFrom>
            {
                public IReadOnlySelectExpression<IFrom> Select { get { return new SelectExpression<IFrom> { Select = f => new { f.Id, f.Name } }; } }
            }

            private readonly InnerProvider _provider = new InnerProvider();

            public MapperWithPathRuleAndInterfaceProvider([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new EntityMapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => (IFrom)(f.Parent), SelectProviderRule.Rule, _provider, (@from, provider) => from.Id},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithObjectPathRuleAndInterfaceProvider()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithPathRuleAndInterfaceProvider>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Parent = f.Parent != null ? new DynamicFrom
                    {
                        Id = f.Parent.Id,
                        Name = f.Parent.Name,
                    } : null,
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithWhereInsideSelect : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithWhereInsideSelect([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.SelectMany(c => c.Children.Where(cc => cc.Bool == c.Bool)).Select(c => c.Id), ints => ints.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithWhereInsideSelect()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithWhereInsideSelect>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Select(c => new DynamicFrom
                    {
                        Bool = c.Bool,
                        Children = c.Children.Where(cc => cc.Bool == c.Bool).Select(cc => new DynamicFrom
                        {
                            Bool = cc.Bool,
                            Id = cc.Id,
                        })
                    })
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithSumWithLambdaInWhere : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithSumWithLambdaInWhere([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Id == c.Children.Sum(cc => cc.Id)).Select(c => c.Bool), bools => bools.Count(b => b)},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithSumWithLambdaInWhere()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithSumWithLambdaInWhere>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Id == c.Children.Sum(cc => cc.Id)).Select(c => new DynamicFrom
                    {
                        Bool = c.Bool,
                        Id = c.Id,
                        Children = c.Children.Select(cc => new DynamicFrom
                        {
                            Id = cc.Id,
                        })
                    })
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithAvgWithLambdaInWhere : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithAvgWithLambdaInWhere([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Id < c.Children.Average(cc => cc.Id)).Select(c => c.Bool), bools => bools.Count(b => b)},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithAvgWithLambdaInWhere()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithAvgWithLambdaInWhere>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Id < c.Children.Average(cc => cc.Id)).Select(c => new DynamicFrom
                    {
                        Bool = c.Bool,
                        Id = c.Id,
                        Children = c.Children.Select(cc => new DynamicFrom
                        {
                            Id = cc.Id,
                        })
                    })
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithMinWithLambdaInWhere : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithMinWithLambdaInWhere([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Id < c.Children.Min(cc => cc.Id)).Select(c => c.Bool), bools => bools.Count(b => b)},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithMinWithLambdaInWhere()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithMinWithLambdaInWhere>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Id < c.Children.Min(cc => cc.Id)).Select(c => new DynamicFrom
                    {
                        Bool = c.Bool,
                        Id = c.Id,
                        Children = c.Children.Select(cc => new DynamicFrom
                        {
                            Id = cc.Id,
                        })
                    })
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithMaxWithLambdaInWhere : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithMaxWithLambdaInWhere([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Id < c.Children.Max(cc => cc.Id)).Select(c => c.Bool), bools => bools.Count(b => b)},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithMaxWithLambdaInWhere()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithMaxWithLambdaInWhere>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected =
                f => new DynamicFrom
                {
                    Children = f.Children.Where(c => c.Id < c.Children.Max(cc => cc.Id)).Select(c => new DynamicFrom
                    {
                        Bool = c.Bool,
                        Id = c.Id,
                        Children = c.Children.Select(cc => new DynamicFrom
                        {
                            Id = cc.Id,
                        })
                    })
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithDeepConditionOnSingleEntity : SelectProviderMapper<Zero, SimpleTo>
        {
            public MapperWithDeepConditionOnSingleEntity([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, Zero> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, Zero>
                    {
                        {
                            t => t.Id,
                            z => z.Firsts
                                .SelectMany(f => f.SecondWithFkPks)
                                .Select(s => s.Third)
                                .Where(t => t.Data == 1)
                                .Select(t => t.Id)
                        },
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithDeepCondition()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithDeepConditionOnSingleEntity>(
                MapperTypes.None, Entities.Are(typeof(Zero), typeof(First), typeof(SecondWithFkPk), typeof(ThirdWithPkPk)));
            var actual = mapper.Select;
            Expression<Func<Zero, object>> expected =
                z => new Zero
                {
                    Firsts = z.Firsts.Select(
                        f => new First
                        {
                            SecondWithFkPks = f.SecondWithFkPks.Select(s => new SecondWithFkPk
                            {
                                Third = s.Third != null ? new ThirdWithPkPk {Id = s.Third.Id, Data = s.Third.Data} : null
                            }).ToList()
                        }).ToList()
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithDeepConditionOnSingleEntityAndProvider : SelectProviderMapper<Zero, SimpleTo>, ISelectProvider<ThirdWithPkPk>
        {
            public MapperWithDeepConditionOnSingleEntityAndProvider([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, Zero> MapRules
            {
                get
                {
                    return new EntityMapRules<SimpleTo, Zero>
                    {
                        {
                            t => t.Id,
                            z => z.Firsts
                                .SelectMany(f => f.SecondWithFkPks)
                                .Select(s => s.Third)
                                .Where(t => t.Data == 1),
                            SelectCollectionProviderRule.Rule,
                            this,
                            (pks, provider) => pks.Count()
                        },
                    };
                }
            }

            IReadOnlySelectExpression<ThirdWithPkPk> ISelectProvider<ThirdWithPkPk>.Select { get {return new SelectExpression<ThirdWithPkPk>{Select = t => new {t.Data}};} }
        }

        [TestMethod]
        public void TestMapperWithDeepConditionAndProvider()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithDeepConditionOnSingleEntityAndProvider>(
                MapperTypes.None, Entities.Are(typeof(Zero), typeof(First), typeof(SecondWithFkPk), typeof(ThirdWithPkPk)));
            var actual = mapper.Select;
            Expression<Func<Zero, object>> expected =
                z => new Zero
                {
                    Firsts = z.Firsts.Select(
                        f => new First
                        {
                            SecondWithFkPks = f.SecondWithFkPks.Select(s => new SecondWithFkPk
                            {
                                Third = s.Third != null ? new ThirdWithPkPk { Data = s.Third.Data } : null
                            }).ToList()
                        }).ToList()
                };
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        public class SimpleFrom
        {
            public int Id { get; set; }
            public string Data { get; set; }
        }

        private class MapperWithZeroLengthPathRule : SelectProviderMapper<SimpleFrom, SimpleTo>
        {
            public MapperWithZeroLengthPathRule([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, SimpleFrom> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, SimpleFrom>
                    {
                        {f => f.Id, f => f, @from => from.Id},
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithZeroLengthPathRule()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithZeroLengthPathRule>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<SimpleFrom, object>> expected =
                f => new SimpleFrom {Id = f.Id, Data = f.Data};
            SelectAssert.AreEqual(expected, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }


        private class MapperWithRuleWithTwoProcessors : SelectProviderMapper<First, SimpleTo>
        {
            private class ZeroProvider : ISelectProvider<Zero>
            {
                public IReadOnlySelectExpression<Zero> Select
                {
                    get
                    {
                        return new SelectExpression<Zero>
                        {
                            Select = z => new {z.Id, z.Guid},
                        };
                    }
                }
            }

            private class SecondProvider : ISelectProvider<Second>
            {
                public IReadOnlySelectExpression<Second> Select
                {
                    get
                    {
                        return new SelectExpression<Second>
                        {
                            Select = s => new {s.Id, s.Bool},
                        };
                    }
                }
            }

            public MapperWithRuleWithTwoProcessors([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, First> MapRules
            {
                get
                {
                    Expression<Func<First, ICollection<Second>>> firstPath = f => f.Seconds;
                    Expression<Func<First, Zero>> secondPath = f => f.Zero;
                    Expression<Func<ICollection<Second>, Zero, ZeroProvider, SecondProvider, int>> convert =
                        (seconds, zero, arg3, arg4) => seconds.Count + zero.Id;
                    Expression<Func<SimpleTo, int>> to = t => t.Id;
                    return new MapRules<SimpleTo, First>
                    {
                        new MapRule
                        {
                            FromAccess = new[]
                            {
                                new MapSource(firstPath, new ProcessorDescription(new SecondProvider(), typeof(Second))),
                                new MapSource(secondPath, new ProcessorDescription(new ZeroProvider(), typeof(Zero))),
                            },
                            Convert = convert,
                            ToAccess = to,
                        }
                    };
                }
            }
        }

        [TestMethod]
        public void TestMapperWithMultipleProcessors()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithRuleWithTwoProcessors>(
                MapperTypes.None, Entities.Are(typeof(Zero), typeof(First), typeof(Second)));
            var select = mapper.Select;
            SelectAssert.AreEqual(f => new First
            {
                Zero = f.Zero != null ? new Zero {Id = f.Zero.Id, Guid = f.Zero.Guid} : null,
                Seconds = f.Seconds.Select(s => new Second { Id = s.Id, Bool = s.Bool }).ToList(),
            }, select.Select);
            SelectAssert.NoReusedTypes(select);
            var res =
                mapper.Map(new First {Zero = new Zero {Id = 1,}, Seconds = new[] {new Second(), null, new Second(),}});
            Assert.IsNotNull(res);
            Assert.AreEqual(4, res.Id);
        }


        private class MapperWithTwoWhereInARow : SelectProviderMapper<From, SimpleTo>
        {
            public MapperWithTwoWhereInARow([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<SimpleTo, From> MapRules
            {
                get
                {
                    return new MapRules<SimpleTo, From>
                    {
                        {t => t.Id, f => f.Children.Where(c => c.Children.Any(cc => cc.Id == 2)).Where(c => c.Id == 1).Select(cc => cc.AEnum1), froms => froms.Count()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestTwoWhereInARow()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithTwoWhereInARow>(
                MapperTypes.None, Entities.Are(typeof(From)));
            //ExceptAssert.Throws<InvalidOperationException>(() => mapper.Select.ThrowIfNull());
            Expression<Func<From, object>> expected = f => new From
            {
                Children = f.Children.Where(c => c.Children.Any(cc => cc.Id == 2) && c.Id == 1).Select(c => new From
                {
                    Id = c.Id,
                    AEnum1 = c.AEnum1,
                    Children = c.Children.Select(cc => new From()
                    {
                        Id = cc.Id,
                    }).ToList(),
                }).ToList(),
            };
            var actual = mapper.Select;
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }



        private class BaseEntity
        {
            public int Id { get; set; }
        }

        private class DerivedEntity : BaseEntity
        {
            public string Data { get; set; }
        }

        private class Model
        {
            public int Id { get; set; }
            public string Data { get; set; }
        }

        private class DerivedEntityMapper : SelectProviderMapper<DerivedEntity, Model>
        {
            public DerivedEntityMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }
        }

        [TestMethod]
        public void TestDerivedEntityMapper()
        {
            var mapper = AbstractMapperFactory.Create<DerivedEntityMapper>(
                MapperTypes.None, Entities.Are(typeof(DerivedEntity)));
            Expression<Func<DerivedEntity, object>> expected = e => new DerivedEntity {Id = e.Id, Data = e.Data};
            var actual = mapper.Select;
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }


        private class BaseEntityWithVirtualPrivateSetter
        {
            public BaseEntityWithVirtualPrivateSetter()
            {
                Collection = new List<SubentityForVirtualPrivateSetter>();
            }

            public virtual ICollection<SubentityForVirtualPrivateSetter> Collection { get; private set; }
        }

        private class SubentityForVirtualPrivateSetter
        {
            public int Id { get; set; }
        }

        private class DerivedEntityWithOverriddenPrivateSetter : BaseEntityWithVirtualPrivateSetter
        {
            public int Id { get; set; }

            [System.ComponentModel.Description("desc")]
            public override ICollection<SubentityForVirtualPrivateSetter> Collection
            {
                get { return base.Collection; }
            }
        }

        private class NormalEntity
        {
            public int Id { get; set; }

            public IEnumerable<SubentityForVirtualPrivateSetter> Collection { get; set; }
        }

        private class EntityWithOverriddenPrivateSetterMapper :
            SelectProviderMapper<DerivedEntityWithOverriddenPrivateSetter, NotSoSimpleTo>
        {
            public EntityWithOverriddenPrivateSetterMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<NotSoSimpleTo, DerivedEntityWithOverriddenPrivateSetter> MapRules
            {
                get
                {
                    return new MapRules<NotSoSimpleTo, DerivedEntityWithOverriddenPrivateSetter>
                    {
                        {s => s.NotSimple, de => de.Collection.Select(c => c.Id), ints => ints.Sum()},
                    };
                }
            }
        }

        [TestMethod]
        public void TestPrivateEntityWithOverriddenPrivateSetterMapper()
        {
            var mapper = AbstractMapperFactory.Create<EntityWithOverriddenPrivateSetterMapper>(
                MapperTypes.None,
                Entities.Are(typeof(DerivedEntityWithOverriddenPrivateSetter), typeof(SubentityForVirtualPrivateSetter)));
            Expression<Func<DerivedEntityWithOverriddenPrivateSetter, object>> expected = e =>
                new NormalEntity
                {
                    Id = e.Id,
                    Collection = e.Collection.Select(c => new SubentityForVirtualPrivateSetter { Id = c.Id }),
                };
            var actual = mapper.Select;
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }


        private class MapperWithConditionsInSingleElementMethods :
            SelectProviderMapper<From, MapperWithConditionsInSingleElementMethods.Model>
        {
            public class Model
            {
                public int First { get; set; }
                public int FirstOrDefault { get; set; }
                public int Single { get; set; }
                public int SingleOrDefault { get; set; }
                public int Last { get; set; }
                public int LastOrDefault { get; set; }
            }

            public MapperWithConditionsInSingleElementMethods([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<Model, From> MapRules
            {
                get
                {
                    return new MapRules<Model, From>
                    {
                        { t => t.First, f => f.Children.First(c => c.Id == 1).AEnumInt},
                        { t => t.FirstOrDefault, f => f.Children.FirstOrDefault(c => c.Id == 2).AEnumInt},
                        { t => t.Single, f => f.Children.Single(c => c.Id == 3).AEnumInt },
                        { t => t.SingleOrDefault, f => f.Children.SingleOrDefault(c => c.Id == 4).AEnumInt },
                        { t => t.Last, f => f.Children.Last(c => c.Id == 5).AEnumInt },
                        { t => t.LastOrDefault, f => f.Children.LastOrDefault(c => c.Id == 6).AEnumInt },
                    };
                }
            }
        }

        [TestMethod]
        public void TestConditionsInSingleElementMethods()
        {
            var mapper = AbstractMapperFactory.Create<MapperWithConditionsInSingleElementMethods>(
                MapperTypes.None, Entities.Are(typeof(From)));
            var actual = mapper.Select;
            Expression<Func<From, object>> expected = e =>
                new From
                {
                    Children = e.Children
                        .Where(c => c.Id == 1 || c.Id == 2 || c.Id == 3 || c.Id == 4 || c.Id == 5 || c.Id == 6)
                        .Select(c => new From { Id = c.Id, AEnumInt = c.AEnumInt })
                        .ToList(),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
            var mapped = mapper.Map(new From
            {
                Children = Enumerable.Range(1, 6).Select(i => new From { Id = i, AEnumInt = i + 1 }).ToList()
            });
            Assert.AreEqual(2, mapped.First);
            Assert.AreEqual(3, mapped.FirstOrDefault);
            Assert.AreEqual(4, mapped.Single);
            Assert.AreEqual(5, mapped.SingleOrDefault);
            Assert.AreEqual(6, mapped.Last);
            Assert.AreEqual(7, mapped.LastOrDefault);
        }


        public class EntityWithCollectionWithoutSetterMapper : SelectProviderMapper<EntityWithCollectionWithoutSetter, NotSoSimpleTo>
        {
            public EntityWithCollectionWithoutSetterMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<NotSoSimpleTo, EntityWithCollectionWithoutSetter> MapRules
            {
                get
                {
                    return new MapRules<NotSoSimpleTo, EntityWithCollectionWithoutSetter>
                    {
                        { t => t.Id, e => e.Id },
                        { t => t.NotSimple, e => e.Subentities.Select(s => s.Id), ids => ids.Sum() },
                    };
                }
            }
        }

        public class DerivedEntityWithCollectionWithoutSetterMapper : SelectProviderMapper<DerivedEntityWithCollectionWithoutSetter, NotSoSimpleTo>
        {
            public DerivedEntityWithCollectionWithoutSetterMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<NotSoSimpleTo, DerivedEntityWithCollectionWithoutSetter> MapRules
            {
                get
                {
                    return new MapRules<NotSoSimpleTo, DerivedEntityWithCollectionWithoutSetter>
                    {
                        { t => t.Id, e => e.Id },
                        { t => t.NotSimple, e => e.Subentities.Select(s => s.Id), ids => ids.Sum() },
                    };
                }
            }
        }

        public class EntityWithoutSetterContainerMapper : SelectProviderMapper<EntityWithoutSetterContainer, ToContainer>
        {
            public EntityWithoutSetterContainerMapper([NotNull] IAbstractSelectProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }
        }

        [TestMethod]
        public void TestEntityWithCollectionWithoutSetter()
        {
            var mapper = AbstractMapperFactory.Create<EntityWithCollectionWithoutSetterMapper>(
                MapperTypes.None, Entities.Are(typeof(EntityWithCollectionWithoutSetter), typeof(SubentityWithCollectionWithoutSetter)));
            var actual = mapper.Select;
            Expression<Func<EntityWithCollectionWithoutSetter, object>> expected = e =>
                new DynamicEntityWithCollectionWithoutSetter
                {
                    Id = e.Id,
                    Shadow_Subentities = e.Subentities
                        .Select(s => new SubentityWithCollectionWithoutSetter { Id = s.Id })
                        .ToList(),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }

        [TestMethod]
        public void TestDerivedEntityWithCollectionWithoutSetter()
        {
            var mapper = AbstractMapperFactory.Create<DerivedEntityWithCollectionWithoutSetterMapper>(
                MapperTypes.None, Entities.Are(typeof(DerivedEntityWithCollectionWithoutSetter), typeof(SubentityWithCollectionWithoutSetter)));
            var actual = mapper.Select;
            Expression<Func<DerivedEntityWithCollectionWithoutSetter, object>> expected = e =>
                new DynamicEntityWithCollectionWithoutSetter
                {
                    Id = e.Id,
                    Shadow_Subentities = e.Subentities
                        .Select(s => new SubentityWithCollectionWithoutSetter { Id = s.Id })
                        .ToList(),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }

        [TestMethod]
        public void TestEntityWithoutSetterContainer()
        {
            var mapper = AbstractMapperFactory.Create<EntityWithoutSetterContainerMapper>(
                MapperTypes.Are(
                    typeof(DerivedEntityWithCollectionWithoutSetterMapper)),
                Entities.Are(
                    typeof(EntityWithoutSetterContainer),
                    typeof(DerivedEntityWithCollectionWithoutSetter),
                    typeof(SubentityWithCollectionWithoutSetter)));
            var actual = mapper.Select;
            Expression<Func<EntityWithoutSetterContainer, object>> expected = c =>
                new DynamicEntityWithoutSetterContainer
                {
                    Id = c.Id,
                    Entities = c.Entities
                        .Select(e => new DynamicEntityWithCollectionWithoutSetter
                        {
                            Id = e.Id,
                            Shadow_Subentities = e.Subentities
                                .Select(s => new SubentityWithCollectionWithoutSetter { Id = s.Id })
                                .ToList()
                        })
                        .ToList(),
                };
            SelectAssert.AreEqual(expected, actual.Select);
            SelectAssert.NoReusedTypes(actual);
        }

        public class DynamicEntity
        {
            public object Second { get; set; }
            public object First { get; set; }
            public object Zero { get; set; }
            public int Id { get; set; }
        }


        private class NotAMapper : ISelectProviderMapper<From, SimpleTo>
        {
            public SimpleTo Map(From from)
            {
                throw new NotImplementedException();
            }

            public IReadOnlySelectExpression<From> Select
            {
                get { return new SelectExpression<From> { Select = f => new object()}; }
            }
        }
    }

    public class EntityWithCollectionWithoutSetter
    {
        public EntityWithCollectionWithoutSetter()
        {
            Subentities = new List<SubentityWithCollectionWithoutSetter>();
        }

        public int Id { get; set; }

        public virtual ICollection<SubentityWithCollectionWithoutSetter> Subentities { get; private set; }
    }

    public class DerivedEntityWithCollectionWithoutSetter : EntityWithCollectionWithoutSetter
    {
    }

    public class EntityWithoutSetterContainer
    {
        public int Id { get; set; }

        public ICollection<DerivedEntityWithCollectionWithoutSetter> Entities { get; set; }
    }

    public class DynamicEntityWithCollectionWithoutSetter
    {
        public int Id { get; set; }

        public ICollection<SubentityWithCollectionWithoutSetter> Shadow_Subentities { get; set; }
    }

    public class DynamicEntityWithoutSetterContainer
    {
        public int Id { get; set; }

        public ICollection<DynamicEntityWithCollectionWithoutSetter> Entities { get; set; }
    }

    public class SubentityWithCollectionWithoutSetter
    {
        public int Id { get; set; }
    }

    public class ToContainer
    {
        public int Id { get; set; }

        public IEnumerable<NotSoSimpleTo> Entities { get; set; }
    }
}
// ReSharper restore ClassNeverInstantiated.Local
