﻿namespace TsSoft.AbstractMapper.Select.Tests.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Engine;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Models;

    [TestClass()]
    public class SelectExpressionCreatorTests
    {
        [NotNull] private SelectExpressionCreator _helper;

        [NotNull]
        private ValueHoldingMember TestEntityProp(string name)
        {
            return new ValueHoldingMember(typeof(TestEntity).GetProperty(name));
        }

        private IReadOnlyList<ValueHoldingMember> TestEntityProps(params string[] names)
        {
            return names.Select(TestEntityProp).ToList();
        }

        [NotNull]
        private SelectExpressionCreator CreateCreator(Type[] entityTypes, Type[] externalTypes)
        {
            return AbstractMapperDependenciesFactory.GetDependency<SelectExpressionCreator>(
                                                                   MapperTypes.None, Entities.Are(entityTypes),
                                                                   External.Are(externalTypes));
        }

        private ParsedPath Path(params string[] props)
        {
            return new ParsedPath(elements: props.Select(p => new PathElement(TestEntityProp(p))).ToList(), start: Expression.Parameter(typeof(TestEntity)));
        }

        [TestInitialize]
        public void TestInit()
        {
            _helper = CreateCreator(new[] { typeof(TestEntity) }, new[] { typeof(ExternalEntity) });
        }

        [TestMethod]
        public void TestCreatePrimitivePropertyPaths()
        {
            // simple paths
            var paths = new[]
                {
                    new MappedPathDescription {Path = Path("Prop1")},
                    new MappedPathDescription {Path = Path("Prop2")},
                };
            Expression<Func<TestEntity, object>> expectedSelect = e => new TestEntity { Prop1 = e.Prop1, Prop2 = e.Prop2 };
            var actual = _helper.Create<TestEntity>(paths, new GeneratorContext());
            SelectAssert.AreEqual(
                expectedSelect,
                actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.IsTrue(!actual.ExternalIncludes.Any());
        }

        [TestMethod]
        public void TestCreateExternalProperty()
        {
            // external path
            var paths = new[]
                {
                    new MappedPathDescription {Path = Path("External")},
                };
            Expression<Func<TestEntity, object>> expectedSelect = e => new TestEntity { ExternalId = e.ExternalId };
            var actual = _helper.Create<TestEntity>(paths, new GeneratorContext());
            SelectAssert.AreEqual(
                expectedSelect,
                actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(1, actual.ExternalIncludes.Count);
            IncludeAssert.AreEqual(x => x.External, actual.ExternalIncludes.First());
        }

        [TestMethod]
        public void TestCreatePropertyWithIncludeProviderProcessor()
        {
            // path with processor having includes
            var mockProcessor = new Mock<IIncludeProvider<TestEntity>>(MockBehavior.Strict);
            mockProcessor.Setup(mp => mp.Includes).Returns(new Includes<TestEntity>
                {
                    e => e.Parent.External,
                    e => e.Children,
                });
            var paths = new[]
                {
                    new MappedPathDescription {Path = Path("Parent"), ProcessorDescription = new ProcessorDescription(processor: mockProcessor.Object, processedType: typeof(TestEntity))}
                };
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Parent = e.Parent != null ? new DynamicTestEntity
                {
                    Prop1 = e.Parent.Prop1,
                    Prop2 = e.Parent.Prop2,
                    Prop3 = e.Parent.Prop3,
                    ExternalId = e.Parent.ExternalId,
                    Parent = e.Parent.Parent != null
                    ? new DynamicTestEntity { Prop1 = e.Parent.Parent.Prop1, Prop2 = e.Parent.Parent.Prop2, Prop3 = e.Parent.Parent.Prop3, ExternalId = e.Parent.Parent.ExternalId }
                    : null,
                    Children = e.Parent.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1, Prop2 = c.Prop2, Prop3 = c.Prop3, ExternalId = c.ExternalId })
                } : null,
            };
            var actual = _helper.Create<TestEntity>(paths, new GeneratorContext());
            SelectAssert.AreEqual(
                expectedSelect,
                actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(1, actual.ExternalIncludes.Count);
            IncludeAssert.AreEqual(x => x.Parent.Parent.External, actual.ExternalIncludes.First());
        }

        [TestMethod]
        public void TestCreateSelfIncludeProviderProcessor()
        {
            // path with self processor having includes
            var mockProcessor = new Mock<IIncludeProvider<TestEntity>>(MockBehavior.Strict);
            mockProcessor.Setup(mp => mp.Includes).Returns(new Includes<TestEntity>
                {
                    e => e.Parent.External,
                    e => e.Children,
                });
            var paths = new[]
                {
                    new MappedPathDescription {Path = Path(), ProcessorDescription = new ProcessorDescription(processor: mockProcessor.Object, processedType: typeof(TestEntity))}
                };
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Prop1 = e.Prop1,
                Prop2 = e.Prop2,
                Prop3 = e.Prop3,
                ExternalId = e.ExternalId,
                Parent = e.Parent != null
                ? new DynamicTestEntity { Prop1 = e.Parent.Prop1, Prop2 = e.Parent.Prop2, Prop3 = e.Parent.Prop3, ExternalId = e.Parent.ExternalId }
                : null,
                Children = e.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1, Prop2 = c.Prop2, Prop3 = c.Prop3, ExternalId = c.ExternalId })
            };
            var actual = _helper.Create<TestEntity>(paths, new GeneratorContext());
            SelectAssert.AreEqual(
                expectedSelect,
                actual.Select);
            Assert.AreEqual(1, actual.ExternalIncludes.Count);
            IncludeAssert.AreEqual(x => x.Parent.External, actual.ExternalIncludes.First());
        }

        [TestMethod]
        public void TestCreatePropertyWithSelectProviderProcessor()
        {
            // path with processor having select
            var mockSelectProcessor = new Mock<ISelectProvider<TestEntity>>(MockBehavior.Strict);
            mockSelectProcessor.Setup(msp => msp.Select).Returns(new SelectExpression<TestEntity>
            {
                Select = e => new DynamicTestEntity
                {
                    Prop1 = e.Prop1,
                    Parent = e.Parent != null ? new DynamicTestEntity { Prop2 = e.Prop2 } : null,
                    Children = e.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 })
                },
                ExternalIncludes = new List<Expression<Func<TestEntity, object>>>
                        {
                            e => e.External,
                        }
            });
            var paths = new[]
                {
                    new MappedPathDescription { Path = Path("Prop1") },
                    new MappedPathDescription {Path = Path("Parent"), ProcessorDescription = new ProcessorDescription(processor: mockSelectProcessor.Object, processedType: typeof(TestEntity))}
                };
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Prop1 = e.Prop1,
                Parent = e.Parent != null ? new DynamicTestEntity
                {
                    Prop1 = e.Parent.Prop1,
                    Parent = e.Parent.Parent != null
                    ? new DynamicTestEntity { Prop2 = e.Parent.Parent.Prop2 }
                    : null,
                    Children = e.Parent.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 }),
                } : null,
            };
            var actual = _helper.Create<TestEntity>(paths, new GeneratorContext());
            SelectAssert.AreEqual(
                expectedSelect,
                actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(1, actual.ExternalIncludes.Count);
            IncludeAssert.AreEqual(x => x.Parent.External, actual.ExternalIncludes.First());
        }
    }
}
