﻿namespace TsSoft.Expressions.IncludeBuilder.Tests
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;

    [TestClass]
    public class IncludeDescriptionHelperTests
    {
        [NotNull]readonly Expression<Func<Parent, object>> _exprNmc = p => p.NotMappedChild;
        [NotNull]readonly Expression<Func<Parent, object>> _exprFbNmc = p => p.Firstborn.NotMappedChild;
        [NotNull]readonly Expression<Func<Parent, object>> _exprFb = p => p.Firstborn;
        [NotNull]readonly Expression<Func<Parent, object>> _exprFbFb = p => p.Firstborn.Firstborn;
        [NotNull]readonly Expression<Func<Parent, object>> _exprFbFbNmc = p => p.Firstborn.Firstborn.NotMappedChild;
        [NotNull]readonly Expression<Func<Parent, object>> _exprFbCh = p => p.Firstborn.Children;
        [NotNull]readonly Expression<Func<Parent, object>> _exprFbChNmc = p => p.Firstborn.Children.Select(c => c.NotMappedChild);
        [NotNull]readonly Expression<Func<Parent, object>> _exprChNmc = p => p.Children.Select(c => c.NotMappedChild);
        [NotNull]readonly Expression<Func<Parent, object>> _exprChFb = p => p.Children.Select(c => c.Firstborn);
        [NotNull]readonly Expression<Func<Parent, object>> _exprChFbNmc = p => p.Children.Select(c => c.Firstborn.NotMappedChild);
        [NotNull]readonly Expression<Func<Parent, object>> _exprChFbFb = p => p.Children.Select(c => c.Firstborn.Firstborn);
        [NotNull]readonly Expression<Func<Parent, object>> _exprChFbFbNmc = p => p.Children.Select(c => c.Firstborn.Firstborn.NotMappedChild);
        [NotNull]readonly Expression<Func<Parent, object>> _exprChFbCh = p => p.Children.Select(c => c.Firstborn.Children);
        [NotNull]readonly Expression<Func<Parent, object>> _exprChFbChNmc = p => p.Children.Select(c => c.Firstborn.Children.Select(cc => cc.NotMappedChild));
        [NotNull]readonly Expression<Func<Parent, object>> _exprChFbChFb = p => p.Children.Select(c => c.Firstborn.Children.Select(cc => cc.Firstborn));
        [NotNull]readonly Expression<Func<Parent, object>> _exprChFbChFbNmc = p => p.Children.Select(c => c.Firstborn.Children.Select(cc => cc.Firstborn.NotMappedChild));
        [NotNull]readonly Expression<Func<Parent, object>> _exprChCh = p => p.Children.Select(c => c.Children);
        [NotNull]readonly Expression<Func<Parent, object>> _exprChChNmc = p => p.Children.Select(c => c.Children.Select(cc => cc.NotMappedChild));
        [NotNull]readonly Expression<Func<Parent, object>> _exprChChCh = p => p.Children.Select(c => c.Children.Select(cc => cc.Children));
        [NotNull]readonly Expression<Func<Parent, object>> _exprChChChNmc = p => p.Children.Select(c => c.Children.Select(cc => cc.Children.Select(ccc => ccc.NotMappedChild)));

        [NotNull]
        private IncludeDescriptionHelper _helper;

        [TestInitialize]
        public void Init()
        {
            var mockHelper = new Mock<IEntityTypesHelper>(MockBehavior.Strict);
            mockHelper.Setup(h => h.IsExternalProperty(It.IsAny<ValueHoldingMember>()))
                .Returns((ValueHoldingMember pi) => pi.HasAttribute<NotMappedAttribute>());
            mockHelper.Setup(h => h.TryGetExternalKey(It.Is<ValueHoldingMember>(pi => pi.Property == typeof(Parent).GetProperty("NotMappedChild"))))
                .Returns(new ValueHoldingMember(typeof(Parent).GetProperty("NotMappedChildId")));
            mockHelper.Setup(h => h.TryGetExternalKey(It.Is<ValueHoldingMember>(pi => pi.Property == typeof(Child).GetProperty("NotMappedChild"))))
                .Returns(new ValueHoldingMember(typeof(Child).GetProperty("NotMappedChildId")));
            mockHelper.Setup(h => h.TryGetExternalKey(It.Is<ValueHoldingMember>(pi => pi.Property == typeof(GrandChild).GetProperty("NotMappedChild"))))
                .Returns(new ValueHoldingMember(typeof(GrandChild).GetProperty("NotMappedChildId")));

            _helper = new IncludeDescriptionHelper(StaticHelpers.FlatPathParser, StaticHelpers.CollectionHelper, StaticHelpers.ExpressionBuilder, mockHelper.Object);
        }

        [NotNull]
        private Parent[] GetObjects()
        {
            var entities = new[]
            {
                new Parent
                {
                    Id = 1,
                    Firstborn = new Child {Id = 2},
                    Children = new[]
                    {
                        new Child
                        {
                            Id = 3,
                            Firstborn = new GrandChild {Id = 5},
                            Children = new[]
                            {
                                new GrandChild {Id = 6},
                            }
                        },
                        new Child
                        {
                            Id = 4,
                            Children = new GrandChild[0],
                        }
                    }
                }
            };
            return entities;
        }

        [NotNull]
        private IncludeDescription<Parent> GetExtDesc([NotNull]Expression<Func<Parent, object>> include)
        {
            return _helper.IncludeToDescription(include, null);
        }

        [TestMethod]
        public void ApplicationDescriptionNoInternalSteps()
        {
            var entities = GetObjects();
            var result = _helper.IncludeToApplicationDescription(GetExtDesc(_exprNmc), entities);
            var obj = result.DbEntities.ToList();
            Assert.AreEqual(1, obj.Count);
            Assert.AreSame(entities[0], obj[0]);
        }

        [TestMethod]
        public void ApplicationDescriptionOneInternalObjectStep()
        {
            var entities = GetObjects();
            var result = _helper.IncludeToApplicationDescription(GetExtDesc(_exprFbNmc), entities);
            var obj = result.DbEntities.ToList();
            Assert.AreEqual(1, obj.Count);
            Assert.AreSame(entities[0].Firstborn, obj[0]);
        }

        [TestMethod]
        public void ApplicationDescriptionTwoInternalObjectStepsLeadingToNowhere()
        {
            var entities = GetObjects();
            var result = _helper.IncludeToApplicationDescription(GetExtDesc(_exprFbFbNmc), entities);
            var obj = result.DbEntities.ToList();
            Assert.AreEqual(0, obj.Count);
        }

        [TestMethod]
        public void ApplicationDescriptionOneInternalCollectionStep()
        {
            var entities = GetObjects();
            var result = _helper.IncludeToApplicationDescription(GetExtDesc(_exprChNmc), entities);
            var obj = result.DbEntities.ToList();
            Assert.AreEqual(2, obj.Count);
            Assert.AreSame(entities[0].Children.First(), obj[0]);
            Assert.AreSame(entities[0].Children.Skip(1).First(), obj[1]);
        }

        [TestMethod]
        public void ApplicationDescriptionOneInternalCollectionOneObjectStep()
        {
            var entities = GetObjects();
            var result = _helper.IncludeToApplicationDescription(GetExtDesc(_exprChFbNmc), entities);
            var obj = result.DbEntities.ToList();
            Assert.AreEqual(1, obj.Count);
            Assert.AreEqual(entities[0].Children.First().Firstborn, obj[0]);
        }

        [TestMethod]
        public void ApplicationDescriptionTwoInternalCollectionSteps()
        {
            var entities = GetObjects();
            var result = _helper.IncludeToApplicationDescription(GetExtDesc(_exprChChNmc), entities);
            var obj = result.DbEntities.ToList();
            Assert.AreEqual(1, obj.Count);
        }

        [TestMethod]
        public void DescriptionNoInternalStepsWithExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprNmc);
            Assert.IsNull(result.DbInclude);
            Assert.AreEqual(typeof(Child), result.ExternalType);
        }

        [TestMethod]
        public void DescriptionOneObjectStepWithExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprFbNmc);
            ExprAssert.AreEqual(_exprFb, result.DbInclude);
            Assert.AreEqual(typeof(GrandChild), result.ExternalType);
        }

        [TestMethod]
        public void DescriptionOneObjectStepWithoutExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprFb);
            ExprAssert.AreEqual(_exprFb, result.DbInclude);
            Assert.IsNull(result.ExternalType);
        }

        [TestMethod]
        public void DescriptionTwoObjectStepsWithoutExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprFbFb);
            ExprAssert.AreEqual(_exprFbFb, result.DbInclude);
            Assert.IsNull(result.ExternalType);
        }

        [TestMethod]
        public void DescriptionTwoObjectStepsWithExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprFbFbNmc);
            ExprAssert.AreEqual(_exprFbFb, result.DbInclude);
            Assert.AreEqual(typeof(Child), result.ExternalType);
        }

        [TestMethod]
        public void DescriptionOneObjectStepOneCollectionStepWithoutExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprFbCh);
            ExprAssert.AreEqual(_exprFbCh, result.DbInclude);
            Assert.IsNull(result.ExternalType);
        }

        [TestMethod]
        public void DescriptionOneObjectStepOneCollectionStepWithExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprFbChNmc);
            ExprAssert.AreEqual(_exprFbCh, result.DbInclude);
            Assert.AreEqual(typeof(Child), result.ExternalType);
        }

        [TestMethod]
        public void DescriptionOneCollectionStepOneObjectStepWithoutExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprChFb);
            ExprAssert.AreEqual(_exprChFb, result.DbInclude);
            Assert.IsNull(result.ExternalType);
        }

        [TestMethod]
        public void DescriptionOneCollectionStepOneObjectStepWithExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprChFbNmc);
            ExprAssert.AreEqual(_exprChFb, result.DbInclude);
            Assert.AreEqual(typeof(Child), result.ExternalType);
        }

        [TestMethod]
        public void DescriptionOneCollectionStepTwoObjectStepsWithoutExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprChFbFb);
            ExprAssert.AreEqual(_exprChFbFb, result.DbInclude);
            Assert.IsNull(result.ExternalType);
        }

        [TestMethod]
        public void DescriptionOneCollectionStepTwoObjectStepsWithExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprChFbFbNmc);
            ExprAssert.AreEqual(_exprChFbFb, result.DbInclude);
            Assert.AreEqual(typeof(GrandChild), result.ExternalType);
        }

        [TestMethod]
        public void DescriptionOneCollectionStepOneObjectStepOneCollectionStepWithoutExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprChFbCh);
            ExprAssert.AreEqual(_exprChFbCh, result.DbInclude);
            Assert.IsNull(result.ExternalType);
        }

        [TestMethod]
        public void DescriptionOneCollectionStepOneObjectStepOneCollectionStepWithExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprChFbChNmc);
            ExprAssert.AreEqual(_exprChFbCh, result.DbInclude);
            Assert.AreEqual(typeof(GrandChild), result.ExternalType);
        }

        [TestMethod]
        public void DescriptionOneCollectionStepOneObjectStepOneCollectionStepOneObjectStepWithoutExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprChFbChFb);
            ExprAssert.AreEqual(_exprChFbChFb, result.DbInclude);
            Assert.IsNull(result.ExternalType);
        }

        [TestMethod]
        public void DescriptionOneCollectionStepOneObjectStepOneCollectionStepOneObjectStepWithExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprChFbChFbNmc);
            ExprAssert.AreEqual(_exprChFbChFb, result.DbInclude);
            Assert.AreEqual(typeof(Child), result.ExternalType);
        }

        [TestMethod]
        public void DescriptionTwoCollectionStepsWithoutExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprChCh);
            ExprAssert.AreEqual(_exprChCh, result.DbInclude);
            Assert.IsNull(result.ExternalType);
        }

        [TestMethod]
        public void DescriptionTwoCollectionStepsWithExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprChChNmc);
            ExprAssert.AreEqual(_exprChCh, result.DbInclude);
            Assert.AreEqual(typeof(Child), result.ExternalType);
        }

        [TestMethod]
        public void DescriptionThreeCollectionStepsWithoutExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprChChCh);
            ExprAssert.AreEqual(_exprChChCh, result.DbInclude);
            Assert.IsNull(result.ExternalType);
        }

        [TestMethod]
        public void DescriptionThreeCollectionStepsWithExternalStep()
        {
            var result = _helper.IncludeToDescription(_exprChChChNmc);
            ExprAssert.AreEqual(_exprChChCh, result.DbInclude);
            Assert.AreEqual(typeof(GrandChild), result.ExternalType);
        }

        public class Child
        {
            public int Id { get; set; }

            public GrandChild Firstborn { get; set; }
            public IEnumerable<GrandChild> Children { get; set; }

            public Guid NotMappedChildId { get; set; }

            [NotMapped]
            public GrandChild NotMappedChild { get; set; }
        }

        public class GrandChild
        {
            public int Id { get; set; }

            public Child Firstborn { get; set; }
            public IEnumerable<Child> Children { get; set; }

            public Guid? NotMappedChildId { get; set; }

            [NotMapped]
            public Child NotMappedChild { get; set; }
        }

        public class Parent
        {
            public int Id { get; set; }

            public int FirstbornId { get; set; }

            public Child Firstborn { get; set; }
            public IEnumerable<Child> Children { get; set; }

            public Guid? NotMappedChildId { get; set; }

            [NotMapped]
            public Child NotMappedChild { get; set; }
        }
    }
}
