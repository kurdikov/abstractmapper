﻿namespace TsSoft.Expressions.IncludeBuilder.Tests
{
    using System.Linq;
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.SelectBuilder.Models;

    [TestClass()]
    public class SelectToIncludeConverterTests
    {
        private SelectToIncludeConverter _converter;

        [TestInitialize]
        public void Init()
        {
            var mockHelper = new Mock<IEntityTypesHelper>(MockBehavior.Strict);
            mockHelper.Setup(h => h.IsNavigationalSingleProperty(It.Is<ValueHoldingMember>(pi =>
                pi.ValueType == typeof(TestEntity)))).Returns(true);
            mockHelper.Setup(h => h.IsNavigationalSingleProperty(It.Is<ValueHoldingMember>(pi =>
                pi.ValueType != typeof(TestEntity)))).Returns(false);
            mockHelper.Setup(h => h.IsNavigationalCollectionProperty(It.Is<ValueHoldingMember>(pi =>
                pi.ValueType.IsGenericEnumerable() && pi.ValueType.GetGenericEnumerableArgument() == typeof(TestEntity)))).Returns(true);
            mockHelper.Setup(h => h.IsNavigationalCollectionProperty(It.Is<ValueHoldingMember>(pi =>
                !pi.ValueType.IsGenericEnumerable() || pi.ValueType.GetGenericEnumerableArgument() != typeof(TestEntity)))).Returns(false);
            mockHelper.Setup(h => h.IsExternalProperty(It.Is<ValueHoldingMember>(pi =>
                pi.ValueType == typeof(ExternalEntity)))).Returns(true);
            mockHelper.Setup(h => h.IsExternalProperty(It.Is<ValueHoldingMember>(pi =>
                pi.ValueType != typeof(ExternalEntity)))).Returns(false);
            _converter = new SelectToIncludeConverter(
                StaticHelpers.NewExpressionHelper,
                StaticHelpers.ExpressionBuilder,
                mockHelper.Object);
        }

        [TestMethod()]
        public void WithoutNavigation()
        {
            var select = new SelectExpression<TestEntity>
            {
                Select = e => new TestEntity {Prop1 = e.Prop1, Prop2 = e.Prop2},
            };
            var actual = _converter.ConvertToIncludes(select);
            Assert.AreEqual(0, actual.Count);
        }

        [TestMethod()]
        public void WithExternalInclude()
        {
            var select = new SelectExpression<TestEntity>
            {
                Select = e => new TestEntity {ExternalId = e.ExternalId},
                ExternalIncludes = new Includes<TestEntity> {x => x.External},
            };
            var actual = _converter.ConvertToIncludes<TestEntity>(select);
            Assert.AreEqual(1, actual.Count);
            ExprAssert.AreEqual(x => x.External, actual.First());
        }

        [TestMethod()]
        public void WithObjectAndCollection()
        {
            var select = new SelectExpression<TestEntity>
            {
                Select = e => new DynamicTestEntity
                {
                    Parent = new DynamicTestEntity
                    {
                        Prop1 = e.Parent.Prop1,
                        Prop2 = e.Parent.Prop2,
                        ExternalId = e.Parent.ExternalId,
                        Parent = new DynamicTestEntity { Prop1 = e.Parent.Parent.Prop1, Prop2 = e.Parent.Parent.Prop2, ExternalId = e.Parent.Parent.ExternalId },
                        Children = e.Parent.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1, Prop2 = c.Prop2, ExternalId = c.ExternalId })
                    },
                },
            };
            var actual = _converter.ConvertToIncludes<TestEntity>(select);
            Assert.AreEqual(2, actual.Count);
            IncludeAssert.Contains(x => x.Parent.Parent, actual);
            IncludeAssert.Contains(x => x.Parent.Children, actual);
        }

        [TestMethod()]
        public void WithObjectAndCollectionWithSubobject()
        {
            var select = new SelectExpression<TestEntity>
            {
                Select = e => new DynamicTestEntity
                {
                    Prop1 = e.Prop1,
                    Prop2 = e.Prop2,
                    ExternalId = e.ExternalId,
                    Parent = new DynamicTestEntity { Prop1 = e.Parent.Prop1, Prop2 = e.Parent.Prop2, ExternalId = e.Parent.ExternalId },
                    Children = e.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1, Prop2 = c.Prop2, ExternalId = c.ExternalId,
                        Parent = new DynamicTestEntity {Prop1 = c.Parent.Prop1}})
                },
            };
            var actual = _converter.ConvertToIncludes<TestEntity>(select);
            Assert.AreEqual(2, actual.Count);
            IncludeAssert.Contains(x => x.Parent, actual);
            IncludeAssert.Contains(x => x.Children.Select(y => y.Parent), actual);
        }
    }
}
