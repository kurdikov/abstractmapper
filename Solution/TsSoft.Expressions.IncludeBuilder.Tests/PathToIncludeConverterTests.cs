﻿namespace TsSoft.Expressions.IncludeBuilder.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using Moq;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Collections;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.IncludeBuilder;
    using TsSoft.Expressions.Models;

    [TestClass]
    public class PathToIncludeConverterTests
    {
        [TestMethod]
        public void TestBuildIncludeByPath()
        {
            var mockBuilder = new Mock<IExpressionBuilder>(MockBehavior.Strict);
            var mockPathMapper = new Mock<IFlatPathParser>(MockBehavior.Strict);
            var mockRetriever = new Mock<IEntityTypesRetriever>(MockBehavior.Strict);

            var converter = new PathToDbIncludeConverter(mockPathMapper.Object, mockBuilder.Object, mockRetriever.Object, new CollectionHelper());

            Expression<Func<T1, object>> expr = t1 => t1.Obj.Obj.End.End;
            Expression<Func<T1, object>> expr2 = t1 => t1.Coll.Select(c => c.Obj.EndColl.Select(e => e.End));

            Expression<Func<T1, object>> testResult = t1 => t1;
            Expression<Func<T1, object>> testResult2 = t1 => t1;
            var prop = new PropertyInfo[]
                {
                    typeof (T1).GetProperty("Obj"),
                    typeof (T1).GetProperty("Obj"),
                    typeof (T1).GetProperty("End"),
                    typeof (NonEntity).GetProperty("Obj"),
                };
            var prop2 = new PropertyInfo[]
                {
                    typeof (T1).GetProperty("Coll"),
                    typeof (T1).GetProperty("Obj"),
                    typeof (T1).GetProperty("EndColl"),
                    typeof (NonEntity).GetProperty("End"),
                };

            mockRetriever.Setup(r => r.IsEntityType(typeof(T1))).Returns(true);
            mockRetriever.Setup(r => r.IsEntityType(typeof(ICollection<T1>))).Returns(false);
            mockRetriever.Setup(r => r.IsEntityCollectionType(typeof(T1))).Returns(false);
            mockRetriever.Setup(r => r.IsEntityCollectionType(typeof(ICollection<T1>))).Returns(true);
            mockRetriever.Setup(r => r.IsEntityType(typeof(NonEntity))).Returns(false);
            mockRetriever.Setup(r => r.IsEntityCollectionType(typeof(NonEntity))).Returns(false);
            mockRetriever.Setup(r => r.IsEntityType(typeof(ICollection<NonEntity>))).Returns(false);
            mockRetriever.Setup(r => r.IsEntityCollectionType(typeof(ICollection<NonEntity>))).Returns(false);

            mockPathMapper.Setup(r => r.Parse(expr)).Returns(prop.Select(p => new ValueHoldingMember(p)).ToArray());
            mockPathMapper.Setup(r => r.Parse(expr2)).Returns(prop2.Select(p => new ValueHoldingMember(p)).ToArray());

            mockBuilder.Setup(r => r.BuildIncludeByPath<T1>(It.Is<IEnumerable<ValueHoldingMember>>(p => p.Count() == 2 && p.First().Property == prop[0] && p.Skip(1).First().Property == prop[1]))).Returns(testResult);
            mockBuilder.Setup(r => r.BuildIncludeByPath<T1>(It.Is<IEnumerable<ValueHoldingMember>>(p => p.Count() == 2 && p.First().Property == prop2[0] && p.Skip(1).First().Property == prop2[1]))).Returns(testResult2);

            Assert.AreEqual(testResult, converter.ToDbInclude(expr));
            Assert.AreEqual(testResult2, converter.ToDbInclude(expr2));
        }

        public class T1
        {
            public T1 Obj { get; set; }

            public ICollection<T1> Coll { get; set; } 
        
            public NonEntity End { get; set; }

            public ICollection<NonEntity> EndColl { get; set; }
        }

        public class NonEntity
        {
            public NonEntity End { get; set; }

        }

    }
}
