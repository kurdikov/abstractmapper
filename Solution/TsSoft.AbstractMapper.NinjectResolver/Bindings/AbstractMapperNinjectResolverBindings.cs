﻿namespace TsSoft.Bindings
{
    using TsSoft.AbstractMapper.NinjectResolver;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.BindingsDescription;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.AbstractMapper.NinjectResolver
    /// </summary>
    public class AbstractMapperNinjectResolverBindings : BindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке TsSoft.AbstractMapper.NinjectResolver
        /// </summary>
        public AbstractMapperNinjectResolverBindings()
        {
            Bind<IMapperResolver, NinjectMapperResolver>();
        }
    }
}
