﻿namespace TsSoft.AbstractMapper.NinjectResolver
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using Ninject;
    using Ninject.Modules;
    using Ninject.Syntax;
    using TsSoft.AbstractMapper.Bindings;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Создатель ninject-связываний для мапперов
    /// </summary>
    public class NinjectMapperBinder
    {
        [NotNull]
        private static Action<MapperBinding> Bind([NotNull]IBindingRoot root)
        {
            return binding =>
            {
                if (binding == null)
                {
                    return;
                }
                // ReSharper disable PossibleNullReferenceException
                var ninjectBinding = root.Bind(binding.Interfaces).To(binding.Implementation).InSingletonScope();
                if (binding.Name != null)
                {
                    ninjectBinding.Named(binding.Name);
                }
                // ReSharper restore PossibleNullReferenceException
            };
        }

        /// <summary>
        /// Связать интерфейсы мапперов из заданных модулями сборок с реализациями
        /// </summary>
        /// <param name="kernel">Ninject-ядро</param>
        /// <param name="retriever">Получатель типов из сборки</param>
        /// <param name="module">Ninject-модули из сборок, содержащих мапперы</param>
        /// <param name="genericMapperInterfaces">Интерфейсы, которые следует связать</param>
        public static void BindAllMappersFromAssembliesWithModulesInSingletonScope(
            [NotNull]IKernel kernel, 
            [NotNull]INinjectModule[] module,
            [NotNull]IReadOnlyList<Type> genericMapperInterfaces,
            [CanBeNull]IAssemblyTypesRetriever retriever = null)
        {
            var assemblies = module.Where(m => m != null).Select(m => m.GetType().Assembly).Distinct();
            MapperBinder.Bind(Bind(kernel), genericMapperInterfaces, assemblies, retriever);
        }

        /// <summary>
        /// Связать интерфейсы мапперов из сборки с модулем
        /// </summary>
        /// <param name="module">Модуль</param>
        /// <param name="genericMapperInterfaces">Интерфейсы, которые следует связать</param>
        /// <param name="retriever">Получатель типов из сборки</param>
        public static void BindAllMappersInSingletonScope(
            [NotNull] NinjectModule module,
            [NotNull] IReadOnlyList<Type> genericMapperInterfaces,
            [CanBeNull]IAssemblyTypesRetriever retriever = null)
        {
            var assemblies = new[] {module.GetType().Assembly};
            MapperBinder.Bind(Bind(module), genericMapperInterfaces, assemblies, retriever);
        }

        /// <summary>
        /// Связать интерфейсы мапперов из заданных модулями сборок с реализациями
        /// </summary>
        /// <param name="kernel">Корень биндинга (ядро, модуль)</param>
        /// <param name="retriever">Получатель типов из сборки</param>
        /// <param name="assemblies">Сборки</param>
        /// <param name="genericMapperInterfaces">Интерфейсы, которые следует связать</param>
        public static void BindAllMappersInSingletonScope(
            [NotNull]IBindingRoot kernel, 
            [NotNull]IAssemblyTypesRetriever retriever, 
            [NotNull]IEnumerable<Assembly> assemblies, 
            [NotNull]IReadOnlyList<Type> genericMapperInterfaces)
        {
            MapperBinder.Bind(Bind(kernel), genericMapperInterfaces, assemblies, retriever);
        }


        /// <summary>
        /// Забиндить маппер для всех его интерфейсов
        /// </summary>
        /// <typeparam name="TMapper">Тип маппера</typeparam>
        /// <param name="bindingRoot">Корень биндинга (ядро, модуль)</param>
        /// <param name="name">Имя биндинга</param>
        /// <param name="baseMapperInterface">Корневой интерфейс маппера (по умолчанию - <cref see="IMapper`2"></cref>)</param>
        public static void BindAllInterfacesInSingletonScope<TMapper>([NotNull]IBindingRoot bindingRoot, string name = null, Type baseMapperInterface = null)
        {
            MapperBinder.BindAllInterfaces<TMapper>(Bind(bindingRoot), name, baseMapperInterface);
        }
    }

}
