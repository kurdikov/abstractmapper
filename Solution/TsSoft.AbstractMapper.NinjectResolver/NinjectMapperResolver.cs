﻿namespace TsSoft.AbstractMapper.NinjectResolver
{
    using Ninject;
    using TsSoft.AbstractMapper.Mappers;
    using TsSoft.AbstractMapper.Resolvers;

    internal class NinjectMapperResolver : IMapperResolver
    {
        private readonly IKernel _kernel;

        public NinjectMapperResolver(IKernel kernel)
        {
            _kernel = kernel;
        }

        public IMapper<TFrom, TTo> TryGet<TFrom, TTo>()
        {
            return _kernel.TryGet<IMapper<TFrom, TTo>>()
                ?? (typeof(TFrom) == typeof(object) ? _kernel.TryGet<IDynamicEntityToEntityMapper<TTo>>() as IMapper<TFrom, TTo> : null);
        }

        public LooseMapper<TFrom, TTo> GetDefault<TFrom, TTo>()
            where TFrom : class
            where TTo : class, new()
        {
            return _kernel.Get<LooseMapper<TFrom, TTo>>();
        }

        public DynamicMapper<TFrom, TTo> GetDynamic<TFrom, TTo>()
            where TFrom : class
            where TTo : class, new()
        {
            return _kernel.Get<DynamicMapper<TFrom, TTo>>();
        }

        public IDynamicEntityToEntityMapper<TTo> GetTypeDeterminingMapper<TTo>() where TTo : class, new()
        {
            return _kernel.Get<IDynamicEntityToEntityMapper<TTo>>();
        }
    }
}
