﻿namespace TsSoft.AbstractMapper.NinjectResolver.Attributes
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Bindings;

    /// <summary>
    /// Указать автобиндеру мапперов, что этот класс следует связать именованным биндингом
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class NinjectNamedBindingAttribute : NamedBindingAttribute
    {
        /// <summary>
        /// Указать автобиндеру мапперов, что этот класс следует связать именованным биндингом
        /// </summary>
        public NinjectNamedBindingAttribute([NotNull] string name)
            : base(name)
        {
        }
    }
}
