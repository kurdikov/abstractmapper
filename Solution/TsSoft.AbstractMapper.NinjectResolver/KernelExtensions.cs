﻿namespace TsSoft.AbstractMapper.NinjectResolver
{
    using System;
    using System.Reflection;
    using JetBrains.Annotations;
    using Ninject;
    using Ninject.Modules;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Расширения ядра Ninject
    /// </summary>
    public static class KernelExtensions
    {
        /// <summary>
        /// Забиндить мапперы как синглтоны
        /// </summary>
        /// <typeparam name="TModule">Ninject-модуль сборки, мапперы из которой нужно забиндить</typeparam>
        /// <param name="kernel">Ядро</param>
        /// <param name="genericMapperInterfaces">Интерфейсы мапперов (GenericTypeDefinition)</param>
        public static void BindMappersInSingletonScope<TModule>(
            [NotNull]this IKernel kernel,
            [NotNull]params Type[] genericMapperInterfaces)
            where TModule : INinjectModule
        {
            NinjectMapperBinder.BindAllMappersInSingletonScope(
                kernel,
                new AssemblyTypesRetriever(),
                new [] {Assembly.GetAssembly(typeof(TModule))},
                genericMapperInterfaces);
        }

        /// <summary>
        /// Забиндить мапперы как синглтоны
        /// </summary>
        /// <typeparam name="TModule">Ninject-модуль сборки, мапперы из которой нужно забиндить</typeparam>
        /// <typeparam name="TModel">Класс из сборки, в которой содержатся модели, в которые происходит маппинг</typeparam>
        /// <param name="kernel">Ядро</param>
        /// <param name="genericMapperInterfaces">Интерфейсы мапперов (GenericTypeDefinition)</param>
        public static void BindMappersInSingletonScope<TModule, TModel>(
            [NotNull]this IKernel kernel,
            [NotNull]params Type[] genericMapperInterfaces)
            where TModule : INinjectModule
        {
            NinjectMapperBinder.BindAllMappersInSingletonScope(
                kernel,
                new AssemblyTypesRetriever(),
                new[] { Assembly.GetAssembly(typeof(TModule)), Assembly.GetAssembly(typeof(TModel)) },
                genericMapperInterfaces);
        }

        /// <summary>
        /// Забиндить мапперы как синглтоны
        /// </summary>
        /// <typeparam name="TModule">Ninject-модуль сборки, мапперы из которой нужно забиндить</typeparam>
        /// <typeparam name="TModel1">Класс из сборки, в которой содержатся модели, в которые происходит маппинг</typeparam>
        /// <typeparam name="TModel2">Класс из сборки, в которой содержатся модели, в которые происходит маппинг</typeparam>
        /// <param name="kernel">Ядро</param>
        /// <param name="genericMapperInterfaces">Интерфейсы мапперов (GenericTypeDefinition)</param>
        public static void BindMappersInSingletonScope<TModule, TModel1, TModel2>(
            [NotNull]this IKernel kernel,
            [NotNull]params Type[] genericMapperInterfaces)
            where TModule : INinjectModule
        {
            NinjectMapperBinder.BindAllMappersInSingletonScope(
                kernel,
                new AssemblyTypesRetriever(),
                new[] { Assembly.GetAssembly(typeof(TModule)), Assembly.GetAssembly(typeof(TModel1)), Assembly.GetAssembly(typeof(TModel2)) },
                genericMapperInterfaces);
        }
    }
}
