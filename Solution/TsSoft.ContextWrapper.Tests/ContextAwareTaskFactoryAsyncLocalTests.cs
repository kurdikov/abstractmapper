﻿#if (NETCORE || NET46)
namespace TsSoft.ContextWrapper.Tests
{
    using System.Threading.Tasks;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ContextAwareTaskFactoryAsyncLocalTests
    {
        [NotNull]
        private IItemStore _store;

        [TestInitialize]
        public void Init()
        {
            _store = new AsyncLocalStore();
            DisposableThing.Reset();
            _store.Clear();
        }

        [TestMethod]
        public void TestRunWithContextSwitch()
        {
            var factory = new ContextAwareTaskFactory(_store);
            _store.Initialize("key", key => new DisposableThing());
            var thisContext = _store.Get<DisposableThing>("key");
            Assert.AreEqual(1, thisContext.Id);
            var task = factory.RunWithContextSwitch(SleepCheck);
            task.Wait();
            thisContext = _store.Get<DisposableThing>("key");
            Assert.AreEqual(1, thisContext.Id);
            Assert.IsFalse(thisContext.IsDisposed);
            var rettask = factory.RunWithContextSwitch(SleepCheckReturn);
            rettask.Wait();
            Assert.AreEqual(3, rettask.Result.Id);
            Assert.IsTrue(rettask.Result.IsDisposed);
            thisContext = _store.Get<DisposableThing>("key");
            Assert.AreEqual(1, thisContext.Id);
            Assert.IsFalse(thisContext.IsDisposed);
        }

        [TestMethod]
        public void TestStartNewWithContextSwitch()
        {
            var factory = new ContextAwareTaskFactory(_store);
            _store.Initialize("key", key => new DisposableThing());
            var thisContext = _store.Get<DisposableThing>("key");
            Assert.AreEqual(1, thisContext.Id);
            DisposableThing action = null;
            var task = factory.StartNewWithContextSwitch(() =>
                {
                    action = _store.Get<DisposableThing>("key");
                    Assert.AreEqual(2, action.Id);
                    Assert.IsFalse(action.IsDisposed);
                });
            task.Wait();
            Assert.IsNotNull(action);
            Assert.IsTrue(action.IsDisposed);

            var functask = factory.StartNewWithContextSwitch(() =>
            {
                var func = _store.Get<DisposableThing>("key");
                Assert.AreEqual(3, func.Id);
                Assert.IsFalse(func.IsDisposed);
                return func;
            });
            functask.Wait();
            var funcResult = functask.Result;
            Assert.IsNotNull(funcResult);
            Assert.AreEqual(3, funcResult.Id);
            Assert.IsTrue(funcResult.IsDisposed);
        }

        private async Task SleepCheck()
        {
            await Task.Delay(200);
            var thing = _store.Get<DisposableThing>("key");
            Assert.AreEqual(2, thing.Id);
            Assert.IsFalse(thing.IsDisposed);
        }

        private async Task<DisposableThing> SleepCheckReturn()
        {
            await Task.Delay(200);
            var thing = _store.Get<DisposableThing>("key");
            Assert.AreEqual(3, thing.Id);
            Assert.IsFalse(thing.IsDisposed);
            return thing;
        }

        [TestCleanup]
        public void Cleanup()
        {
            _store.Clear();
        }
    }
}
#endif
