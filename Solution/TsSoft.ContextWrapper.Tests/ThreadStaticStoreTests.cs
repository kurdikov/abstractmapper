﻿namespace TsSoft.ContextWrapper.Tests
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ThreadStaticStoreTests
    {
        [TestMethod]
        public void TestStore()
        {
            var store = new ThreadStaticStore();
            DisposableThing item;
            var mainThreadId = Thread.CurrentThread.ManagedThreadId;

            Assert.IsFalse(store.IsInitialized<DisposableThing>("1"));
            Assert.IsFalse(store.IsValueCreated<DisposableThing>("1"));
            ExceptAssert.Throws<InvalidOperationException>(() => store.Get<DisposableThing>("1"));

            var newItem = new DisposableThing();
            store.Initialize("1", ss => newItem);
            Assert.IsTrue(store.IsInitialized<DisposableThing>("1"));
            Assert.IsFalse(store.IsValueCreated<DisposableThing>("1"));

            item = store.Get<DisposableThing>("1");
            Assert.AreSame(newItem, item);
            Assert.IsFalse(item.IsDisposed);

            Assert.IsTrue(store.IsInitialized<DisposableThing>("1"));
            Assert.IsTrue(store.IsValueCreated<DisposableThing>("1"));

            item = store.Get<DisposableThing>("1");
            Assert.AreSame(newItem, item);
            Assert.IsFalse(item.IsDisposed);

            Assert.IsTrue(store.IsValueCreated<DisposableThing>("1"));

            Assert.IsFalse(store.IsInitialized<DisposableThing>("2"));
            Assert.IsFalse(store.IsValueCreated<DisposableThing>("2"));
            ExceptAssert.Throws<InvalidOperationException>(() => store.Get<DisposableThing>("2"));

            const string secondItem = "test";
            store.Initialize("2", ss => secondItem);
            Assert.IsFalse(store.IsValueCreated<string>("2"));
            Assert.IsTrue(store.IsInitialized<string>("2"));

            var s = store.Get<string>("2");
            Assert.AreSame(secondItem, s);

            Assert.IsTrue(store.IsInitialized<string>("2"));
            Assert.IsTrue(store.IsValueCreated<string>("2"));

            s = store.Get<string>("2");
            Assert.AreSame(secondItem, s);

            ExceptAssert.Throws<ArgumentException>(() => store.Get<DisposableThing>(null));

            ExceptAssert.Throws<ArgumentException>(() => store.Initialize<string>("123", null));

            store.Initialize<string>("123", ss => null);
            Assert.IsTrue(store.IsInitialized<string>("123"));
            Assert.IsFalse(store.IsValueCreated<string>("123"));
            ExceptAssert.Throws<InvalidOperationException>(() => store.Get<string>("123"));

            var innerTask = Task.Run(async () =>
                {
                    await Task.Delay(1);
                    Assert.AreNotEqual(Thread.CurrentThread.ManagedThreadId, mainThreadId, "Fluke: task started in the same thread");
                    DisposableThing taskItem;

                    Assert.IsFalse(store.IsInitialized<DisposableThing>("1"));
                    Assert.IsFalse(store.IsValueCreated<DisposableThing>("1"));

                    ExceptAssert.Throws<InvalidOperationException>(() => store.Get<DisposableThing>("1"));

                    var newTaskItem = new DisposableThing();
                    store.Initialize("1", ss => newTaskItem);

                    Assert.IsTrue(store.IsInitialized<DisposableThing>("1"));
                    Assert.IsFalse(store.IsValueCreated<DisposableThing>("1"));

                    taskItem = store.Get<DisposableThing>("1");
                    Assert.AreSame(newTaskItem, taskItem);
                    Assert.IsFalse(taskItem.IsDisposed);
                    
                    Assert.IsTrue(store.IsInitialized<DisposableThing>("1"));
                    Assert.IsTrue(store.IsValueCreated<DisposableThing>("1"));

                    taskItem = store.Get<DisposableThing>("1");
                    Assert.AreSame(newTaskItem, taskItem);
                    Assert.IsFalse(taskItem.IsDisposed);

                    Assert.IsTrue(store.IsInitialized<DisposableThing>("1"));
                    Assert.IsTrue(store.IsValueCreated<DisposableThing>("1"));

                    var newNewTaskItem = new DisposableThing();
                    Assert.IsFalse(store.IsInitialized<DisposableThing>("3"));
                    Assert.IsFalse(store.IsValueCreated<DisposableThing>("3"));
                    store.Initialize("3", ss => newNewTaskItem);
                    Assert.AreNotSame(newTaskItem, newNewTaskItem);

                    Assert.IsTrue(store.IsInitialized<DisposableThing>("3"));
                    Assert.IsFalse(store.IsValueCreated<DisposableThing>("3"));

                    taskItem = store.Get<DisposableThing>("3");
                    Assert.AreSame(newNewTaskItem, taskItem);
                    Assert.IsFalse(taskItem.IsDisposed);

                    Assert.IsTrue(store.IsInitialized<DisposableThing>("3"));
                    Assert.IsTrue(store.IsValueCreated<DisposableThing>("3"));

                    ThreadStaticStore.Dispose();
                    Assert.IsTrue(newTaskItem.IsDisposed);
                    Assert.IsTrue(newNewTaskItem.IsDisposed);
                });
            Assert.IsNotNull(innerTask);
            innerTask.Wait();

            Assert.IsTrue(store.IsInitialized<DisposableThing>("1"));
            Assert.IsTrue(store.IsValueCreated<DisposableThing>("1"));

            item = store.Get<DisposableThing>("1");
            Assert.AreSame(newItem, item);
            Assert.IsFalse(item.IsDisposed);

            Assert.IsTrue(store.IsInitialized<DisposableThing>("1"));
            Assert.IsTrue(store.IsValueCreated<DisposableThing>("1"));

            Assert.IsFalse(store.IsInitialized<DisposableThing>("4"));
            Assert.IsFalse(store.IsValueCreated<DisposableThing>("4"));
            var newNewItem = new DisposableThing();
            store.Initialize("4", ss => newNewItem);
            Assert.IsTrue(store.IsInitialized<DisposableThing>("4"));
            Assert.IsFalse(store.IsValueCreated<DisposableThing>("4"));

            item = store.Get<DisposableThing>("4");
            Assert.AreNotSame(newItem, newNewItem);
            Assert.AreSame(newNewItem, item);
            Assert.IsFalse(item.IsDisposed);

            Assert.IsTrue(store.IsInitialized<DisposableThing>("4"));
            Assert.IsTrue(store.IsValueCreated<DisposableThing>("4"));

            item = store.Get<DisposableThing>("4");
            Assert.AreSame(newNewItem, item);
            Assert.IsFalse(item.IsDisposed);

            ThreadStaticStore.Dispose();
            Assert.IsTrue(newItem.IsDisposed);
            Assert.IsTrue(newNewItem.IsDisposed);

            Assert.IsTrue(store.IsInitialized<DisposableThing>("4"));
            Assert.IsTrue(store.IsValueCreated<DisposableThing>("4"));
            item = store.Get<DisposableThing>("4");
            Assert.AreSame(newNewItem, item);
        }
    }
}
