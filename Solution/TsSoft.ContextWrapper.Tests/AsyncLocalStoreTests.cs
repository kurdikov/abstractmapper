﻿#if (NETCORE || NET46)
using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using BaseTestClasses;
using JetBrains.Annotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TsSoft.ContextWrapper
{
    [TestClass]
    public class AsyncLocalStoreTests
    {
        [NotNull]
        private AsyncLocalStore _store;

        [TestInitialize]
        public void Init()
        {
            _store = new AsyncLocalStore();
            _store.Clear();
        }

        [TestMethod]
        public void Tasks_AsyncLocal()
        {
            DisposableThing item;
            var mainThreadId = Thread.CurrentThread.ManagedThreadId;

            Assert.IsFalse(_store.IsInitialized<DisposableThing>("1"));
            Assert.IsFalse(_store.IsValueCreated<DisposableThing>("1"));
            ExceptAssert.Throws<InvalidOperationException>(() => _store.Get<DisposableThing>("1"));

            var newItem = new DisposableThing();
            var innerTaskItem = new DisposableThing();
            _store.Initialize("1", ss => Thread.CurrentThread.ManagedThreadId == mainThreadId ? newItem : innerTaskItem);
            Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
            Assert.IsFalse(_store.IsValueCreated<DisposableThing>("1"));
            item = _store.Get<DisposableThing>("1");
            Assert.AreSame(newItem, item);
            Assert.IsFalse(item.IsDisposed);

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
            Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

            item = _store.Get<DisposableThing>("1");
            Assert.AreSame(newItem, item);
            Assert.IsFalse(item.IsDisposed);

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
            Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

            Assert.IsFalse(_store.IsInitialized<string>("2"));
            Assert.IsFalse(_store.IsValueCreated<string>("2"));
            ExceptAssert.Throws<InvalidOperationException>(() => _store.Get<string>("2"));

            const string secondItem = "test";
            _store.Initialize("2", ss => secondItem);
            Assert.IsTrue(_store.IsInitialized<string>("2"));
            Assert.IsFalse(_store.IsValueCreated<string>("2"));

            var s = _store.Get<string>("2");
            Assert.AreSame(secondItem, s);

            Assert.IsTrue(_store.IsInitialized<string>("2"));
            Assert.IsTrue(_store.IsValueCreated<string>("2"));

            s = _store.Get<string>("2");
            Assert.AreSame(secondItem, s);

            Assert.IsTrue(_store.IsInitialized<string>("2"));
            Assert.IsTrue(_store.IsValueCreated<string>("2"));

            ExceptAssert.Throws<ArgumentException>(() => _store.Get<DisposableThing>(null));

            ExceptAssert.Throws<ArgumentException>(() => _store.Initialize<string>("123", null));

            Assert.IsFalse(_store.IsInitialized<string>("123"));
            Assert.IsFalse(_store.IsValueCreated<string>("123"));
            _store.Initialize<string>("123", ss => null);
            Assert.IsTrue(_store.IsInitialized<string>("123"));
            Assert.IsFalse(_store.IsValueCreated<string>("123"));
            ExceptAssert.Throws<InvalidOperationException>(() => _store.Get<string>("123"));

            var innerTask = Task.Run(() =>
            {
                Assert.AreNotEqual(Thread.CurrentThread.ManagedThreadId, mainThreadId, "Fluke: task started in the same thread");
                DisposableThing taskItem;

                Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
                Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

                taskItem = _store.Get<DisposableThing>("1");
                Assert.AreSame(newItem, taskItem);
                Assert.IsFalse(taskItem.IsDisposed);

                Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
                Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

                taskItem = _store.Get<DisposableThing>("1");
                Assert.AreSame(newItem, taskItem);
                Assert.IsFalse(taskItem.IsDisposed);

                var newNewTaskItem = new DisposableThing();
                Assert.IsFalse(_store.IsInitialized<DisposableThing>("3"));
                Assert.IsFalse(_store.IsValueCreated<DisposableThing>("3"));
                _store.Initialize("3", ss => newNewTaskItem);
                Assert.IsTrue(_store.IsInitialized<DisposableThing>("3"));
                Assert.IsFalse(_store.IsValueCreated<DisposableThing>("3"));

                taskItem = _store.Get<DisposableThing>("3");
                Assert.AreSame(newNewTaskItem, taskItem);
                Assert.IsFalse(taskItem.IsDisposed);

                Assert.IsTrue(_store.IsInitialized<DisposableThing>("3"));
                Assert.IsTrue(_store.IsValueCreated<DisposableThing>("3"));

                AsyncLocalStore.Dispose();
                Assert.IsTrue(taskItem.IsDisposed);
                Assert.IsTrue(newNewTaskItem.IsDisposed);

                Assert.IsTrue(_store.IsInitialized<DisposableThing>("3"));
                Assert.IsTrue(_store.IsValueCreated<DisposableThing>("3"));

                var taskFactory = new ContextAwareTaskFactory(_store);
                var innerTask2 = taskFactory.StartNewWithContextSwitch(() =>
                {
                    Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
                    Assert.IsFalse(_store.IsValueCreated<DisposableThing>("1"));
                    var copied = _store.Get<DisposableThing>("1");
                    Assert.AreEqual(innerTaskItem, copied);
                    var item2 = new DisposableThing();
                    _store.Initialize("1", ss => item2);
                    Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
                    Assert.IsFalse(_store.IsValueCreated<DisposableThing>("1"));
                    var newContextItem = _store.Get<DisposableThing>("1");
                    Assert.AreSame(item2, newContextItem);
                });
                innerTask2.Wait();

                Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
                Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

                taskItem = _store.Get<DisposableThing>("1");
                Assert.AreSame(newItem, taskItem);
                Assert.IsTrue(taskItem.IsDisposed);

                Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
                Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));
            });
            Assert.IsNotNull(innerTask);
            innerTask.Wait();

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
            Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

            item = _store.Get<DisposableThing>("1");
            Assert.AreSame(newItem, item);
            Assert.IsTrue(item.IsDisposed);

            var newNewItem = new DisposableThing();
            _store.Initialize("3", ss => newNewItem);

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("3"));
            Assert.IsFalse(_store.IsValueCreated<DisposableThing>("3"));

            item = _store.Get<DisposableThing>("3");
            Assert.AreNotSame(newItem, newNewItem);
            Assert.AreSame(newNewItem, item);
            Assert.IsFalse(item.IsDisposed);

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("3"));
            Assert.IsTrue(_store.IsValueCreated<DisposableThing>("3"));

            item = _store.Get<DisposableThing>("3");
            Assert.AreSame(newNewItem, item);
            Assert.IsFalse(item.IsDisposed);

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("3"));
            Assert.IsTrue(_store.IsValueCreated<DisposableThing>("3"));

            AsyncLocalStore.Dispose();
            Assert.IsTrue(newItem.IsDisposed);
            Assert.IsTrue(newNewItem.IsDisposed);

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("3"));
            Assert.IsTrue(_store.IsValueCreated<DisposableThing>("3"));
        }

        [TestMethod]
        public void AsyncAwait_AsyncLocal()
        {
            var thing = new DisposableThing();
            Assert.IsFalse(_store.IsInitialized<DisposableThing>("1"));
            Assert.IsFalse(_store.IsValueCreated<DisposableThing>("1"));

            _store.Initialize("1", ss => thing);
            Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
            Assert.IsFalse(_store.IsValueCreated<DisposableThing>("1"));

            var task = AsyncMethod(thing);
            task.Wait();

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
            Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

            var actual = _store.Get<DisposableThing>("1");
            Assert.AreEqual(thing, actual);

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
            Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

            NoSuchThing("2");
            NoSuchThing("3");
        }

        private void NoSuchThing([NotNull]string key, [CallerLineNumber] int line = 0)
        {
            Assert.IsFalse(_store.IsInitialized<DisposableThing>(key));
            Assert.IsFalse(_store.IsValueCreated<DisposableThing>(key));
            ExceptAssert.Throws<InvalidOperationException>(() => _store.Get<DisposableThing>(key), "Line: {0}", line);
        }

        private async Task AsyncMethod(DisposableThing expected)
        {
            Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
            Assert.IsFalse(_store.IsValueCreated<DisposableThing>("1"));

            var actual = _store.Get<DisposableThing>("1");
            Assert.AreSame(expected, actual);

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
            Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

            NoSuchThing("2");

            var second = new DisposableThing();

            Assert.IsFalse(_store.IsInitialized<DisposableThing>("2"));
            Assert.IsFalse(_store.IsValueCreated<DisposableThing>("2"));

            _store.Initialize("2", ss => second);

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("2"));
            Assert.IsFalse(_store.IsValueCreated<DisposableThing>("2"));

            actual = _store.Get<DisposableThing>("2");
            Assert.AreSame(second, actual);

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("2"));
            Assert.IsTrue(_store.IsValueCreated<DisposableThing>("2"));

            NoSuchThing("3");
            await Sleep(expected, second).ConfigureAwait(false);

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
            Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

            actual = _store.Get<DisposableThing>("1");
            Assert.AreSame(expected, actual);

            Assert.IsTrue(_store.IsInitialized<DisposableThing>("2"));
            Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

            actual = _store.Get<DisposableThing>("2");
            Assert.AreSame(second, actual);

            NoSuchThing("3");
        }

        [NotNull]
        private Task Sleep(DisposableThing expectedFirst, DisposableThing expectedSecond)
        {
            var task = new Task(() =>
            {
                Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
                Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

                var first = _store.Get<DisposableThing>("1");
                Assert.AreSame(expectedFirst, first);

                Assert.IsTrue(_store.IsInitialized<DisposableThing>("1"));
                Assert.IsTrue(_store.IsValueCreated<DisposableThing>("1"));

                Assert.IsTrue(_store.IsInitialized<DisposableThing>("2"));
                Assert.IsTrue(_store.IsValueCreated<DisposableThing>("2"));

                var second = _store.Get<DisposableThing>("2");
                Assert.AreSame(expectedSecond, second);

                Assert.IsTrue(_store.IsInitialized<DisposableThing>("2"));
                Assert.IsTrue(_store.IsValueCreated<DisposableThing>("2"));

                var third = new DisposableThing();

                Assert.IsFalse(_store.IsInitialized<DisposableThing>("3"));
                Assert.IsFalse(_store.IsValueCreated<DisposableThing>("3"));
                _store.Initialize("3", ss => third);
                Assert.IsTrue(_store.IsInitialized<DisposableThing>("3"));
                Assert.IsFalse(_store.IsValueCreated<DisposableThing>("3"));

                var thirdGot = _store.Get<DisposableThing>("3");
                Assert.AreSame(third, thirdGot);

                Assert.IsTrue(_store.IsInitialized<DisposableThing>("3"));
                Assert.IsTrue(_store.IsValueCreated<DisposableThing>("3"));
            });
            task.Start();
            return task;
        }

        [TestCleanup]
        public void Cleanup()
        {
            _store.Clear();
        }
    }
}
#endif
