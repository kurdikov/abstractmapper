﻿namespace TsSoft.Expressions.Models.Tuples
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Интерфейс сравнимых кортежей
    /// </summary>
    public interface IEquatableTuple
    {
        /// <summary>
        /// Размер кортежа
        /// </summary>
        int Size { get; }
    }

    /// <summary>
    /// Фабрика кортежей
    /// </summary>
    public static class EquatableTuple
    {
        /// <summary>
        /// Создать кортеж размера 1
        /// </summary>
        public static EquatableTuple<T1> Create<T1>(T1 item)
        {
            return new EquatableTuple<T1>(item);
        }
        /// <summary>
        /// Создать кортеж размера 2
        /// </summary>
        public static EquatableTuple<T1, T2> Create<T1, T2>(T1 item, T2 item2)
        {
            return new EquatableTuple<T1, T2>(item, item2);
        }
        /// <summary>
        /// Создать кортеж размера 3
        /// </summary>
        public static EquatableTuple<T1, T2, T3> Create<T1, T2, T3>(T1 item, T2 item2, T3 item3)
        {
            return new EquatableTuple<T1, T2, T3>(item, item2, item3);
        }
        /// <summary>
        /// Создать кортеж размера 4
        /// </summary>
        public static EquatableTuple<T1, T2, T3, T4> Create<T1, T2, T3, T4>(T1 item, T2 item2, T3 item3, T4 item4)
        {
            return new EquatableTuple<T1, T2, T3, T4>(item, item2, item3, item4);
        }
        /// <summary>
        /// Создать кортеж размера 5
        /// </summary>
        public static EquatableTuple<T1, T2, T3, T4, T5> Create<T1, T2, T3, T4, T5>(T1 item, T2 item2, T3 item3, T4 item4, T5 item5)
        {
            return new EquatableTuple<T1, T2, T3, T4, T5>(item, item2, item3, item4, item5);
        }
        /// <summary>
        /// Создать кортеж размера 6
        /// </summary>
        public static EquatableTuple<T1, T2, T3, T4, T5, T6> Create<T1, T2, T3, T4, T5, T6>(T1 item, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6)
        {
            return new EquatableTuple<T1, T2, T3, T4, T5, T6>(item, item2, item3, item4, item5, item6);
        }
        /// <summary>
        /// Создать кортеж размера 7
        /// </summary>
        public static EquatableTuple<T1, T2, T3, T4, T5, T6, T7> Create<T1, T2, T3, T4, T5, T6, T7>(T1 item, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7)
        {
            return new EquatableTuple<T1, T2, T3, T4, T5, T6, T7>(item, item2, item3, item4, item5, item6, item7);
        }
        /// <summary>
        /// Создать кортеж размера 8
        /// </summary>
        public static EquatableTuple<T1, T2, T3, T4, T5, T6, T7, T8> Create<T1, T2, T3, T4, T5, T6, T7, T8>(T1 item, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, T8 item8)
        {
            return new EquatableTuple<T1, T2, T3, T4, T5, T6, T7, T8>(item, item2, item3, item4, item5, item6, item7, item8);
        }

        /// <summary>
        /// Получить свойство с элементом кортежа
        /// </summary>
        /// <param name="equatableTupleType">Тип кортежа</param>
        /// <param name="itemIndex">Индекс элемента (нумерация начинается с 0)</param>
        [NotNull]
        public static PropertyInfo GetItemProperty([NotNull]Type equatableTupleType, int itemIndex)
        {
            var result = equatableTupleType.GetTypeInfo().GetProperty("Item" + (itemIndex + 1));
            if (result == null)
            {
                throw new ArgumentOutOfRangeException("itemIndex", string.Format("Type {0} does not have item {1}", equatableTupleType, itemIndex));
            }
            return result;
        }
    }

    /// <summary>
    /// Кортеж размера 1
    /// </summary>
    public struct EquatableTuple<T1> : IEquatable<EquatableTuple<T1>>, IEquatableTuple
    {
        private readonly T1 _item1;

        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T1 Item1 {get { return  _item1; }}

        /// <summary>
        /// Кортеж размера 1
        /// </summary>
        public EquatableTuple(T1 item1)
        {
            _item1 = item1;
        }

        /// <summary>
        /// Равно
        /// </summary>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is EquatableTuple<T1> && Equals((EquatableTuple<T1>)obj);
        }

        /// <summary>
        /// Хэш
        /// </summary>
        public override int GetHashCode()
        {
            return EqualityComparer<T1>.Default.GetHashCode(_item1);
        }

        /// <summary>
        /// Равно
        /// </summary>
        public bool Equals(EquatableTuple<T1> other)
        {
            return EqualityComparer<T1>.Default.Equals(_item1, other._item1);
        }

        /// <summary>
        /// Равно
        /// </summary>
        public static bool operator ==(EquatableTuple<T1> first, EquatableTuple<T1> second)
        {
            return first.Equals(second);
        }

        /// <summary>
        /// Не равно
        /// </summary>
        public static bool operator !=(EquatableTuple<T1> first, EquatableTuple<T1> second)
        {
            return !(first == second);
        }

        /// <summary>
        /// Размер кортежа
        /// </summary>
        public int Size {get { return 1; }}
    }

    /// <summary>
    /// Кортеж размера 2
    /// </summary>
    public struct EquatableTuple<T1, T2> : IEquatable<EquatableTuple<T1, T2>>, IEquatableTuple
    {
        private readonly T1 _item1;
        private readonly T2 _item2;

        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T1 Item1 { get { return _item1; } }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T2 Item2 { get { return _item2; } }
        /// <summary>
        /// Кортеж размера 2
        /// </summary>
        public EquatableTuple(T1 item1, T2 item2)
        {
            _item1 = item1;
            _item2 = item2;
        }
        /// <summary>
        /// Равно
        /// </summary>
        public bool Equals(EquatableTuple<T1, T2> other)
        {
            return EqualityComparer<T1>.Default.Equals(_item1, other._item1) && EqualityComparer<T2>.Default.Equals(_item2, other._item2);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is EquatableTuple<T1, T2> && Equals((EquatableTuple<T1, T2>) obj);
        }
        /// <summary>
        /// Хэш
        /// </summary>
        public override int GetHashCode()
        {
            unchecked
            {
                return (EqualityComparer<T1>.Default.GetHashCode(_item1) * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_item2);
            }
        }
        /// <summary>
        /// Равно
        /// </summary>
        public static bool operator ==(EquatableTuple<T1, T2> first, EquatableTuple<T1, T2> second)
        {
            return first.Equals(second);
        }
        /// <summary>
        /// Не равно
        /// </summary>
        public static bool operator !=(EquatableTuple<T1, T2> first, EquatableTuple<T1, T2> second)
        {
            return !(first == second);
        }
        /// <summary>
        /// Размер кортежа
        /// </summary>
        public int Size { get { return 2; } }
    }

    /// <summary>
    /// Кортеж размера 3
    /// </summary>
    public struct EquatableTuple<T1, T2, T3> : IEquatable<EquatableTuple<T1, T2, T3>>, IEquatableTuple
    {
        private readonly T1 _item1;
        private readonly T2 _item2;
        private readonly T3 _item3;
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T1 Item1
        {
            get { return _item1; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T2 Item2
        {
            get { return _item2; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T3 Item3 { get { return _item3; }}
        /// <summary>
        /// Кортеж размера 3
        /// </summary>
        public EquatableTuple(T1 item1, T2 item2, T3 item3)
        {
            _item1 = item1;
            _item2 = item2;
            _item3 = item3;
        }
        /// <summary>
        /// Размер кортежа
        /// </summary>
        public int Size { get { return 3; } }
        /// <summary>
        /// Равно
        /// </summary>
        public static bool operator ==(EquatableTuple<T1, T2, T3> first, EquatableTuple<T1, T2, T3> second)
        {
            return first.Equals(second);
        }
        /// <summary>
        /// Не равно
        /// </summary>
        public static bool operator !=(EquatableTuple<T1, T2, T3> first, EquatableTuple<T1, T2, T3> second)
        {
            return !(first == second);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public bool Equals(EquatableTuple<T1, T2, T3> other)
        {
            return EqualityComparer<T1>.Default.Equals(_item1, other._item1) && EqualityComparer<T2>.Default.Equals(_item2, other._item2) && EqualityComparer<T3>.Default.Equals(_item3, other._item3);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is EquatableTuple<T1, T2, T3> && Equals((EquatableTuple<T1, T2, T3>) obj);
        }
        /// <summary>
        /// Хэш
        /// </summary>
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(_item1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_item2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(_item3);
                return hashCode;
            }
        }
    }

    /// <summary>
    /// Кортеж размера 4
    /// </summary>
    public struct EquatableTuple<T1, T2, T3, T4> : IEquatable<EquatableTuple<T1, T2, T3, T4>>, IEquatableTuple
    {
        private readonly T1 _item1;
        private readonly T2 _item2;
        private readonly T3 _item3;
        private readonly T4 _item4;
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T1 Item1
        {
            get { return _item1; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T2 Item2
        {
            get { return _item2; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T3 Item3
        {
            get { return _item3; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T4 Item4
        {
            get { return _item4; }
        }
        /// <summary>
        /// Кортеж размера 4
        /// </summary>
        public EquatableTuple(T1 item1, T2 item2, T3 item3, T4 item4)
        {
            _item1 = item1;
            _item2 = item2;
            _item3 = item3;
            _item4 = item4;
        }
        /// <summary>
        /// Размер кортежа
        /// </summary>
        public int Size { get { return 4; } }
        /// <summary>
        /// Равно
        /// </summary>
        public static bool operator ==(EquatableTuple<T1, T2, T3, T4> first, EquatableTuple<T1, T2, T3, T4> second)
        {
            return first.Equals(second);
        }
        /// <summary>
        /// Не равно
        /// </summary>
        public static bool operator !=(EquatableTuple<T1, T2, T3, T4> first, EquatableTuple<T1, T2, T3, T4> second)
        {
            return !(first == second);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public bool Equals(EquatableTuple<T1, T2, T3, T4> other)
        {
            return EqualityComparer<T1>.Default.Equals(_item1, other._item1) && EqualityComparer<T2>.Default.Equals(_item2, other._item2) && EqualityComparer<T3>.Default.Equals(_item3, other._item3) && EqualityComparer<T4>.Default.Equals(_item4, other._item4);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is EquatableTuple<T1, T2, T3, T4> && Equals((EquatableTuple<T1, T2, T3, T4>) obj);
        }
        /// <summary>
        /// Хэш
        /// </summary>
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(_item1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_item2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(_item3);
                hashCode = (hashCode * 397) ^ EqualityComparer<T4>.Default.GetHashCode(_item4);
                return hashCode;
            }
        }
    }

    /// <summary>
    /// Кортеж размера 5
    /// </summary>
    public struct EquatableTuple<T1, T2, T3, T4, T5> : IEquatable<EquatableTuple<T1, T2, T3, T4, T5>>, IEquatableTuple
    {
        private readonly T1 _item1;
        private readonly T2 _item2;
        private readonly T3 _item3;
        private readonly T4 _item4;
        private readonly T5 _item5;
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T1 Item1
        {
            get { return _item1; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T2 Item2
        {
            get { return _item2; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T3 Item3
        {
            get { return _item3; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T4 Item4
        {
            get { return _item4; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T5 Item5
        {
            get { return _item5; }
        }
        /// <summary>
        /// Кортеж размера 5
        /// </summary>
        public EquatableTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5)
        {
            _item1 = item1;
            _item2 = item2;
            _item3 = item3;
            _item4 = item4;
            _item5 = item5;
        }
        /// <summary>
        /// Размер кортежа
        /// </summary>
        public int Size
        {
            get { return 5; }
        }
        /// <summary>
        /// Равно
        /// </summary>
        public static bool operator ==(EquatableTuple<T1, T2, T3, T4, T5> first, EquatableTuple<T1, T2, T3, T4, T5> second)
        {
            return first.Equals(second);
        }
        /// <summary>
        /// Не равно
        /// </summary>
        public static bool operator !=(EquatableTuple<T1, T2, T3, T4, T5> first, EquatableTuple<T1, T2, T3, T4, T5> second)
        {
            return !(first == second);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public bool Equals(EquatableTuple<T1, T2, T3, T4, T5> other)
        {
            return EqualityComparer<T1>.Default.Equals(_item1, other._item1) && EqualityComparer<T2>.Default.Equals(_item2, other._item2) && EqualityComparer<T3>.Default.Equals(_item3, other._item3) && EqualityComparer<T4>.Default.Equals(_item4, other._item4) && EqualityComparer<T5>.Default.Equals(_item5, other._item5);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is EquatableTuple<T1, T2, T3, T4, T5> && Equals((EquatableTuple<T1, T2, T3, T4, T5>) obj);
        }
        /// <summary>
        /// Хэш
        /// </summary>
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(_item1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_item2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(_item3);
                hashCode = (hashCode * 397) ^ EqualityComparer<T4>.Default.GetHashCode(_item4);
                hashCode = (hashCode * 397) ^ EqualityComparer<T5>.Default.GetHashCode(_item5);
                return hashCode;
            }
        }
    }

    /// <summary>
    /// Кортеж размера 6
    /// </summary>
    public struct EquatableTuple<T1, T2, T3, T4, T5, T6> : IEquatable<EquatableTuple<T1, T2, T3, T4, T5, T6>>, IEquatableTuple
    {
        private readonly T1 _item1;
        private readonly T2 _item2;
        private readonly T3 _item3;
        private readonly T4 _item4;
        private readonly T5 _item5;
        private readonly T6 _item6;
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T1 Item1
        {
            get { return _item1; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T2 Item2
        {
            get { return _item2; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T3 Item3
        {
            get { return _item3; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T4 Item4
        {
            get { return _item4; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T5 Item5
        {
            get { return _item5; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T6 Item6
        {
            get { return _item6; }
        }
        /// <summary>
        /// Кортеж размера 6
        /// </summary>
        public EquatableTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6)
        {
            _item1 = item1;
            _item2 = item2;
            _item3 = item3;
            _item4 = item4;
            _item5 = item5;
            _item6 = item6;
        }
        /// <summary>
        /// Размер кортежа
        /// </summary>
        public int Size
        {
            get { return 6; }
        }
        /// <summary>
        /// Равно
        /// </summary>
        public static bool operator ==(
            EquatableTuple<T1, T2, T3, T4, T5, T6> first, EquatableTuple<T1, T2, T3, T4, T5, T6> second)
        {
            return first.Equals(second);
        }
        /// <summary>
        /// Не равно
        /// </summary>
        public static bool operator !=(
            EquatableTuple<T1, T2, T3, T4, T5, T6> first, EquatableTuple<T1, T2, T3, T4, T5, T6> second)
        {
            return !(first == second);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public bool Equals(EquatableTuple<T1, T2, T3, T4, T5, T6> other)
        {
            return EqualityComparer<T1>.Default.Equals(_item1, other._item1) && EqualityComparer<T2>.Default.Equals(_item2, other._item2) && EqualityComparer<T3>.Default.Equals(_item3, other._item3) && EqualityComparer<T4>.Default.Equals(_item4, other._item4) && EqualityComparer<T5>.Default.Equals(_item5, other._item5) && EqualityComparer<T6>.Default.Equals(_item6, other._item6);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is EquatableTuple<T1, T2, T3, T4, T5, T6> && Equals((EquatableTuple<T1, T2, T3, T4, T5, T6>) obj);
        }
        /// <summary>
        /// Хэш
        /// </summary>
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(_item1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_item2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(_item3);
                hashCode = (hashCode * 397) ^ EqualityComparer<T4>.Default.GetHashCode(_item4);
                hashCode = (hashCode * 397) ^ EqualityComparer<T5>.Default.GetHashCode(_item5);
                hashCode = (hashCode * 397) ^ EqualityComparer<T6>.Default.GetHashCode(_item6);
                return hashCode;
            }
        }
    }

    /// <summary>
    /// Кортеж размера 7
    /// </summary>
    public struct EquatableTuple<T1, T2, T3, T4, T5, T6, T7> : IEquatable<EquatableTuple<T1, T2, T3, T4, T5, T6, T7>>, IEquatableTuple
    {
        private readonly T1 _item1;
        private readonly T2 _item2;
        private readonly T3 _item3;
        private readonly T4 _item4;
        private readonly T5 _item5;
        private readonly T6 _item6;
        private readonly T7 _item7;
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T1 Item1
        {
            get { return _item1; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T2 Item2
        {
            get { return _item2; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T3 Item3
        {
            get { return _item3; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T4 Item4
        {
            get { return _item4; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T5 Item5
        {
            get { return _item5; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T6 Item6
        {
            get { return _item6; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T7 Item7
        {
            get { return _item7; }
        }
        /// <summary>
        /// Кортеж размера 7
        /// </summary>
        public EquatableTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7)
        {
            _item1 = item1;
            _item2 = item2;
            _item3 = item3;
            _item4 = item4;
            _item5 = item5;
            _item6 = item6;
            _item7 = item7;
        }
        /// <summary>
        /// Размер кортежа
        /// </summary>
        public int Size
        {
            get { return 7; }
        }
        /// <summary>
        /// Равно
        /// </summary>
        public static bool operator ==(
            EquatableTuple<T1, T2, T3, T4, T5, T6, T7> first, EquatableTuple<T1, T2, T3, T4, T5, T6, T7> second)
        {
            return first.Equals(second);
        }
        /// <summary>
        /// Не равно
        /// </summary>
        public static bool operator !=(
            EquatableTuple<T1, T2, T3, T4, T5, T6, T7> first, EquatableTuple<T1, T2, T3, T4, T5, T6, T7> second)
        {
            return !(first == second);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public bool Equals(EquatableTuple<T1, T2, T3, T4, T5, T6, T7> other)
        {
            return EqualityComparer<T1>.Default.Equals(_item1, other._item1) && EqualityComparer<T2>.Default.Equals(_item2, other._item2) && EqualityComparer<T3>.Default.Equals(_item3, other._item3) && EqualityComparer<T4>.Default.Equals(_item4, other._item4) && EqualityComparer<T6>.Default.Equals(_item6, other._item6) && EqualityComparer<T7>.Default.Equals(_item7, other._item7) && EqualityComparer<T5>.Default.Equals(_item5, other._item5);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is EquatableTuple<T1, T2, T3, T4, T5, T6, T7> && Equals((EquatableTuple<T1, T2, T3, T4, T5, T6, T7>)obj);
        }
        /// <summary>
        /// Хэш
        /// </summary>
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(_item1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_item2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(_item3);
                hashCode = (hashCode * 397) ^ EqualityComparer<T4>.Default.GetHashCode(_item4);
                hashCode = (hashCode * 397) ^ EqualityComparer<T6>.Default.GetHashCode(_item6);
                hashCode = (hashCode * 397) ^ EqualityComparer<T7>.Default.GetHashCode(_item7);
                hashCode = (hashCode * 397) ^ EqualityComparer<T5>.Default.GetHashCode(_item5);
                return hashCode;
            }
        }
    }

    /// <summary>
    /// Кортеж размера 8
    /// </summary>
    public struct EquatableTuple<T1, T2, T3, T4, T5, T6, T7, T8> : IEquatable<EquatableTuple<T1, T2, T3, T4, T5, T6, T7, T8>>,
        IEquatableTuple
    {
        private readonly T1 _item1;
        private readonly T2 _item2;
        private readonly T3 _item3;
        private readonly T4 _item4;
        private readonly T5 _item5;
        private readonly T6 _item6;
        private readonly T7 _item7;
        private readonly T8 _item8;
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T1 Item1
        {
            get { return _item1; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T2 Item2
        {
            get { return _item2; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T3 Item3
        {
            get { return _item3; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T4 Item4
        {
            get { return _item4; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T5 Item5
        {
            get { return _item5; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T6 Item6
        {
            get { return _item6; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T7 Item7
        {
            get { return _item7; }
        }
        /// <summary>
        /// Элемент кортежа
        /// </summary>
        public T8 Item8
        {
            get { return _item8; }
        }
        /// <summary>
        /// Кортеж размера 8
        /// </summary>
        public EquatableTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, T8 item8)
        {
            _item1 = item1;
            _item2 = item2;
            _item3 = item3;
            _item4 = item4;
            _item5 = item5;
            _item6 = item6;
            _item7 = item7;
            _item8 = item8;
        }
        /// <summary>
        /// Размер кортежа
        /// </summary>
        public int Size
        {
            get { return 8; }
        }
        /// <summary>
        /// Равно
        /// </summary>
        public static bool operator ==(
            EquatableTuple<T1, T2, T3, T4, T5, T6, T7, T8> first, EquatableTuple<T1, T2, T3, T4, T5, T6, T7, T8> second)
        {
            return first.Equals(second);
        }
        /// <summary>
        /// Не равно
        /// </summary>
        public static bool operator !=(
            EquatableTuple<T1, T2, T3, T4, T5, T6, T7, T8> first, EquatableTuple<T1, T2, T3, T4, T5, T6, T7, T8> second)
        {
            return !(first == second);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public bool Equals(EquatableTuple<T1, T2, T3, T4, T5, T6, T7, T8> other)
        {
            return EqualityComparer<T1>.Default.Equals(_item1, other._item1) && EqualityComparer<T2>.Default.Equals(_item2, other._item2) && EqualityComparer<T3>.Default.Equals(_item3, other._item3) && EqualityComparer<T4>.Default.Equals(_item4, other._item4) && EqualityComparer<T5>.Default.Equals(_item5, other._item5) && EqualityComparer<T6>.Default.Equals(_item6, other._item6) && EqualityComparer<T7>.Default.Equals(_item7, other._item7) && EqualityComparer<T8>.Default.Equals(_item8, other._item8);
        }
        /// <summary>
        /// Равно
        /// </summary>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is EquatableTuple<T1, T2, T3, T4, T5, T6, T7, T8> && Equals((EquatableTuple<T1, T2, T3, T4, T5, T6, T7, T8>) obj);
        }
        /// <summary>
        /// Хэш
        /// </summary>
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(_item1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_item2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(_item3);
                hashCode = (hashCode * 397) ^ EqualityComparer<T4>.Default.GetHashCode(_item4);
                hashCode = (hashCode * 397) ^ EqualityComparer<T5>.Default.GetHashCode(_item5);
                hashCode = (hashCode * 397) ^ EqualityComparer<T6>.Default.GetHashCode(_item6);
                hashCode = (hashCode * 397) ^ EqualityComparer<T7>.Default.GetHashCode(_item7);
                hashCode = (hashCode * 397) ^ EqualityComparer<T8>.Default.GetHashCode(_item8);
                return hashCode;
            }
        }
    }
}
