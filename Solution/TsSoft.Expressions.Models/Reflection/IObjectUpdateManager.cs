﻿namespace TsSoft.Expressions.Models.Reflection
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Настройки обновления свойств
    /// </summary>
    public interface IObjectUpdateManager
    {
        /// <summary>
        /// Обновлять копированием (примитивные свойства)
        /// </summary>
        bool IsPrimitive(Type type);

        /// <summary>
        /// Обновлять по свойствам (свойства-объекты)
        /// </summary>
        bool IsObject(Type type);

        /// <summary>
        /// Обновлять поэлементно (свойства-коллекции)
        /// </summary>
        bool IsCollection(Type type);

        /// <summary>
        /// Построить выражение для очищения коллекции
        /// </summary>
        /// <param name="collectionProperty">Свойство-коллекция</param>
        /// <param name="objectType">Тип содержащего коллекцию объекта</param>
        /// <param name="target">Содержащий коллекцию объект</param>
        /// <returns>Выражение, очищающее коллекцию</returns>
        [NotNull]
        Expression MakeRemoveCollectionExpression([NotNull]ValueHoldingMember collectionProperty, [NotNull]Type objectType, [NotNull]Expression target);

        /// <summary>
        /// Построить выражение для асинхронного очищения коллекции
        /// </summary>
        /// <param name="collectionProperty">Свойство-коллекция</param>
        /// <param name="objectType">Тип содержащего коллекцию объекта</param>
        /// <param name="target">Содержащий коллекцию объект</param>
        /// <returns>Выражение, очищающее коллекцию</returns>
        [NotNull]
        Expression MakeAsyncRemoveCollectionExpression([NotNull]ValueHoldingMember collectionProperty, [NotNull]Type objectType, [NotNull]Expression target);

        /// <summary>
        /// Построить выражение, удаляющее элементы коллекции, присутствующие в target, но отсутствующие в source
        /// </summary>
        /// <param name="source">Источник обновления - какие элементы надо сохранить</param>
        /// <param name="target">Цель обновления - какие элементы были (среди них могут быть удалённые)</param>
        [NotNull]
        Expression MakeRemoveCollectionPartExpression([NotNull]Expression source, [NotNull]Expression target);

        /// <summary>
        /// Построить выражение, асинхронно удаляющее элементы коллекции, присутствующие в target, но отсутствующие в source
        /// </summary>
        /// <param name="source">Источник обновления - какие элементы надо сохранить</param>
        /// <param name="target">Цель обновления - какие элементы были (среди них могут быть удалённые)</param>
        [NotNull]
        Expression MakeAsyncRemoveCollectionPartExpression([NotNull]Expression source, [NotNull]Expression target);

        /// <summary>
        /// Построить выражение для получения ключа объекта
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>Выражение, получающее ключ объекта</returns>
        [NotNull]
        Expression MakeExtractKeyExpression([NotNull]Expression obj);
    }
}
