﻿namespace TsSoft.Expressions.Models.Reflection
{
    using System;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Настройки копирования свойств
    /// </summary>
    public class ClonedPropertyManager
    {
        /// <summary>
        /// Делать shallow copy свойства
        /// </summary>
        [NotNull]
        public Func<ValueHoldingMember, bool> Copy { get; private set; }

        /// <summary>
        /// Делать deep copy свойства
        /// </summary>
        [NotNull]
        public Func<ValueHoldingMember, bool> SingleClone { get; private set; }

        /// <summary>
        /// Делать deep copy элементов свойства-коллекции
        /// </summary>
        [NotNull]
        public Func<ValueHoldingMember, bool> CollectionClone { get; private set; }

        /// <summary>
        /// Настройки копирования свойств
        /// </summary>
        public ClonedPropertyManager(
            [NotNull] Func<ValueHoldingMember, bool> copy,
            [NotNull] Func<ValueHoldingMember, bool> singleClone,
            [NotNull] Func<ValueHoldingMember, bool> collectionClone)
        {
            if (copy == null) throw new ArgumentNullException("copy");
            if (singleClone == null) throw new ArgumentNullException("singleClone");
            if (collectionClone == null) throw new ArgumentNullException("collectionClone");
            Copy = copy;
            SingleClone = singleClone;
            CollectionClone = collectionClone;
        }

        /// <summary>
        /// Настройки копирования свойств
        /// </summary>
        public ClonedPropertyManager(
            [NotNull] Func<PropertyInfo, bool> copy,
            [NotNull] Func<PropertyInfo, bool> singleClone,
            [NotNull] Func<PropertyInfo, bool> collectionClone)
        {
            if (copy == null) throw new ArgumentNullException("copy");
            if (singleClone == null) throw new ArgumentNullException("singleClone");
            if (collectionClone == null) throw new ArgumentNullException("collectionClone");
            Copy = vh => vh != null && vh.Property != null && copy(vh.Property);
            SingleClone = vh => vh != null && vh.Property != null && singleClone(vh.Property);
            CollectionClone = vh => vh != null && vh.Property != null && collectionClone(vh.Property);
        }
    }
}
