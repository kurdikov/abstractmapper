﻿namespace TsSoft.Expressions.Models.Path
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Выражение с условием возможности его вычисления
    /// </summary>
    public class ExpressionWithPrecondition : Expression
    {
        /// <summary>
        /// Выражение
        /// </summary>
        [NotNull]
        public Expression Expression { get; private set; }

        /// <summary>
        /// Условие, при невыполнении которого выражение выполнить невозможно
        /// </summary>
        [NotNull]
        public Expression Precondition { get; private set; }

        /// <summary>
        /// Выражение с условием возможности его вычисления
        /// </summary>
        public ExpressionWithPrecondition([NotNull] Expression expression, [NotNull]Expression precondition)
        {
            if (expression == null) throw new ArgumentNullException("expression");
            if (precondition == null) throw new ArgumentNullException("precondition");
            Expression = expression;
            Precondition = precondition;
        }

        /// <summary>
        /// Тип выражения
        /// </summary>
        public override Type Type
        {
            get { return Expression.Type; }
        }

        /// <summary>
        /// Тип узла
        /// </summary>
        public override ExpressionType NodeType
        {
            get { return ExpressionType.Extension; }
        }
    }
}
