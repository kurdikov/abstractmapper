﻿namespace TsSoft.Expressions.Models.Path
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Text;
    using JetBrains.Annotations;

    /// <summary>
    /// Элемент пути из свойств
    /// </summary>
    public class PathElement
    {
        /// <summary>
        /// Свойство-шаг
        /// </summary>
        [NotNull]
        public ValueHoldingMember Step { get; private set; }

        /// <summary>
        /// Условие на шаг
        /// </summary>
        [CanBeNull]
        public IReadOnlyCollection<PathCondition> Conditions { get { return _conditions; } }

        private List<PathCondition> _conditions;

        /// <summary>
        /// Параметр, которому соответствует результат применения шага
        /// </summary>
        public ParameterExpression Parameter { get; private set; }
        
        /// <summary>
        /// Элемент пути из свойств
        /// </summary>
        public PathElement([NotNull]ValueHoldingMember step, PathCondition condition = null)
        {
            if (step == null) throw new ArgumentNullException("step");
            Step = step;
            AddCondition(condition);
        }

        /// <summary>
        /// Навесить условие на элемент пути
        /// </summary>
        public PathElement AddCondition(PathCondition condition)
        {
            if (condition == null)
            {
                return this;
            }
            _conditions = _conditions ?? new List<PathCondition>();
            _conditions.Add(condition);
            return this;
        }

        /// <summary>
        /// Установить параметр, которому соответствует результат применения шага
        /// </summary>
        public PathElement SetParameter(ParameterExpression parameter)
        {
            Parameter = parameter;
            return this;
        }

        /// <summary>
        /// Записать строковое представление
        /// </summary>
        public void AppendTo([NotNull] StringBuilder builder)
        {
            builder.Append(Step.Name);
            if (Conditions != null)
            {
                builder.Append(".Where( ");
                foreach (var c in Conditions)
                {
                    if (c == null)
                    {
                        continue;
                    }
                    builder.Append(c.DbCondition).Append(" ");
                }
                builder.Append(')');
            }
        }

        /// <summary>
        /// Получить строковое представление
        /// </summary>
        public override string ToString()
        {
            var sb = new StringBuilder();
            AppendTo(sb);
            return sb.ToString();
        }
    }
}
