namespace TsSoft.Expressions.Models.Path
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// ������� ������ ������� ��� �������
    /// </summary>
    public class SelectTreeNode
    {
        /// <summary>
        /// ��� ��������� ��������
        /// </summary>
        [NotNull]
        public Type Type { get; private set; }

        /// <summary>
        /// ��������� �� �������� �������� � �� ����������
        /// </summary>
        [CanBeNull]
        public SelectTreeLevel NextLevel { get; set; }

        /// <summary>
        /// ��������� Enumerable.Select
        /// </summary>
        [NotNull]
        public IEnumerable<ParameterExpression> SelectParameters { get { return _selectParameters ?? Enumerable.Empty<ParameterExpression>(); } }

        /// <summary>
        /// ��������� Enumerable.Select
        /// </summary>
        [NotNull]
        public IEnumerable<ParameterExpression> ConditionParameters { get { return _conditionParameters ?? Enumerable.Empty<ParameterExpression>(); } }

        /// <summary>
        /// �������
        /// </summary>
        [CanBeNull]
        public Expression Condition { get; private set; }

        private HashSet<ParameterExpression> _selectParameters;
        private HashSet<ParameterExpression> _conditionParameters;
        private bool _hasConstantTrueCondition;

        /// <summary>
        /// ������� ������ ������� ��� �������
        /// </summary>
        /// <param name="type">��� ��������� ��������</param>
        public SelectTreeNode([NotNull]Type type)
        {
            if (type == null) throw new ArgumentNullException("type");
            Type = type;
        }

        /// <summary>
        /// ������� ������ ������� ��� �������
        /// </summary>
        /// <param name="type">��� ��������� ��������</param>
        /// <param name="condition">������� �� ��������</param>
        public SelectTreeNode([NotNull] Type type, LambdaExpression condition) : this(type)
        {
            AddCondition(condition);
        }

        /// <summary>
        /// �������� �������� Enumerable.Select
        /// </summary>
        public void AddSelectParameter([NotNull]ParameterExpression param)
        {
            _selectParameters = _selectParameters ?? new HashSet<ParameterExpression>();
            _selectParameters.Add(param);
        }

        /// <summary>
        /// �������� ��������� Enumerable.Select
        /// </summary>
        public void AddSelectParameters([NotNull] IEnumerable<ParameterExpression> param)
        {
            _selectParameters = _selectParameters ?? new HashSet<ParameterExpression>();
            _selectParameters.UnionWith(param);
        }

        /// <summary>
        /// �������� �������� Enumerable.Where
        /// </summary>
        private void AddConditionParameter([NotNull]ParameterExpression param)
        {
            _conditionParameters = _conditionParameters ?? new HashSet<ParameterExpression>();
            _conditionParameters.Add(param);
        }

        /// <summary>
        /// �������� ������� � �����������
        /// </summary>
        public void AddCondition(LambdaExpression condition)
        {
            if (_hasConstantTrueCondition)
            {
                return;
            }
            if (condition == null)
            {
                AddNullCondition();
            }
            else
            {
                var param = condition.Parameters[0];
                if (param == null)
                {
                    throw new InvalidOperationException(string.Format("Null parameter in lambda {0}", condition));
                }
                Condition = Condition != null
                    ? Expression.OrElse(Condition, condition.Body)
                    : condition.Body;
                AddConditionParameter(param);
            }
        }

        /// <summary>
        /// �������� ������� � �����������, ��������� �� AndAlso
        /// </summary>
        public void AddConditions([NotNull]IEnumerable<LambdaExpression> conditions)
        {
            if (_hasConstantTrueCondition)
            {
                return;
            }
            Expression currentCondition = null;
            foreach (var condition in conditions)
            {
                if (condition == null)
                {
                    continue;
                }
                var param = condition.Parameters[0];
                if (param == null)
                {
                    throw new InvalidOperationException(string.Format("Null parameter in lambda {0}", condition));
                }
                currentCondition = currentCondition != null
                    ? Expression.AndAlso(currentCondition, condition.Body)
                    : condition.Body;
                AddConditionParameter(param);
            }
            if (currentCondition != null)
            {
                Condition = Condition != null
                    ? Expression.OrElse(Condition, currentCondition)
                    : currentCondition;
            }
            else
            {
                AddNullCondition();
            }
        }

        /// <summary>
        /// �������� ������� � �����������
        /// </summary>
        /// <param name="addedCondition">���� �������</param>
        /// <param name="parameters">�������������� � ���� ��� ����������� �������� ���������</param>
        public void AddCondition(Expression addedCondition, [NotNull] IEnumerable<ParameterExpression> parameters)
        {
            if (_hasConstantTrueCondition)
            {
                return;
            }
            if (addedCondition != null)
            {
                Condition = Condition != null
                    ? Expression.OrElse(Condition, addedCondition)
                    : addedCondition;
                _conditionParameters = _conditionParameters ?? new HashSet<ParameterExpression>();
                _conditionParameters.UnionWith(parameters);
            }
            else
            {
                AddNullCondition();
            }
        }

        /// <summary>
        /// ������� �������
        /// </summary>
        public void AddNullCondition()
        {
            Condition = null;
            _conditionParameters = null;
            _hasConstantTrueCondition = true;
        }
    }
}
