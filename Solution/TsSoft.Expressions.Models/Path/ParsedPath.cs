﻿namespace TsSoft.Expressions.Models.Path
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using JetBrains.Annotations;

    /// <summary>
    /// Результат разбора выражения пути
    /// </summary>
    public class ParsedPath
    {
        /// <summary>
        /// Элементы пути - свойства и условия
        /// </summary>
        [NotNull]
        [ItemNotNull]
        public IReadOnlyList<PathElement> Elements { get; private set; }

        /// <summary>
        /// Выражение, к которому применяется путь
        /// </summary>
        [NotNull]
        public ParameterExpression Start { get; private set; }

        /// <summary>
        /// Дополнительно используемые пути - из условий на свойства
        /// </summary>
        [CanBeNull]
        public IEnumerable<ParsedPath> AdditionalPaths { get { return Branches != null ? Branches[Start] : null; } }

        /// <summary>
        /// Ветвления основного пути
        /// </summary>
        [CanBeNull]
        public ILookup<ParameterExpression, ParsedPath> Branches { get; private set; }
        
        /// <summary>
        /// Результат разбора выражения пути
        /// </summary>
        public ParsedPath([NotNull]IReadOnlyList<PathElement> elements, [NotNull]ParameterExpression start, ILookup<ParameterExpression, ParsedPath> branches = null)
        {
            if (elements == null) throw new ArgumentNullException("elements");
            if (start == null) throw new ArgumentNullException("start");
            Elements = elements;
            Start = start;
            Branches = branches ?? new Multidictionary<ParameterExpression, ParsedPath>();
        }

        /// <summary>
        /// Получить элемент пути по индексу
        /// </summary>
        [NotNull]
        public PathElement ElementAt(int index)
        {
            var result = Elements[index];
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Null element in path"));
            }
            return result;
        }

        /// <summary>
        /// Получить последний элемент пути
        /// </summary>
        /// <returns></returns>
        [NotNull]
        public PathElement LastElement()
        {
            return ElementAt(Elements.Count - 1);
        }

        /// <summary>
        /// Получить строковое представление
        /// </summary>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(Start.Name);
            foreach (var element in Elements)
            {
                if (element == null)
                {
                    throw new NullReferenceException("element");
                }
                sb.Append('.');
                element.AppendTo(sb);
            }
            return sb.ToString();
        }
    }
}
