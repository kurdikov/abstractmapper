﻿namespace TsSoft.Expressions.Models.Path
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Внутреннее условие пути
    /// </summary>
    public class PathCondition
    {
        /// <summary>
        /// Параметр лямбды с условием
        /// </summary>
        [CanBeNull]
        public ParameterExpression Start { get; private set; }

        /// <summary>
        /// Условие для передачи в EF
        /// </summary>
        [CanBeNull]
        public LambdaExpression DbCondition { get; private set; }

        /// <summary>
        /// Условие для обработки на сервере (оснащённое проверками на null)
        /// </summary>
        [CanBeNull]
        public LambdaExpression ServerCondition { get; private set; }

        [NotNull]
        private readonly ILookup<ParameterExpression, ParsedPath> _branches;

        /// <summary>
        /// Использованные при построении условия пути
        /// </summary>
        [NotNull]
        public IEnumerable<ParsedPath> UsedPaths { get { return Start != null ? _branches[Start] : Enumerable.Empty<ParsedPath>(); } }

        /// <summary>
        /// Метод, в котором было использовано условие
        /// </summary>
        [CanBeNull]
        public MethodInfo ConditionMethod { get; private set; }

        /// <summary>
        /// Внутреннее условие пути
        /// </summary>
        public PathCondition(
            [CanBeNull]ParameterExpression start, 
            LambdaExpression dbCondition, 
            LambdaExpression serverCondition, 
            [NotNull] ILookup<ParameterExpression, ParsedPath> branches, 
            MethodInfo conditionMethod)
        {
            if (branches == null) throw new ArgumentNullException("branches");
            Start = start;
            DbCondition = dbCondition;
            ServerCondition = serverCondition;
            _branches = branches;
            ConditionMethod = conditionMethod;
        }
    }
}
