﻿namespace TsSoft.Expressions.Models.Path
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Выражение, использующее множество путей
    /// </summary>
    public class PathExpressionContainer : Expression
    {
        [NotNull]private readonly Type _type;

        /// <summary>
        /// Пути
        /// </summary>
        [NotNull]public IReadOnlyCollection<ParsedPath> InnerPaths { get; private set; }

        /// <summary>
        /// Исходное выражение
        /// </summary>
        [NotNull]public Expression Expression { get; private set; }

        /// <summary>
        /// Является ли выражение сравнением с null
        /// </summary>
        public bool IsNullEqualityComparison { get; private set; }

        /// <summary>
        /// Тип выражения
        /// </summary>
        public override Type Type { get { return _type; } }

        /// <summary>
        /// Создать выражение, использующее множество путей
        /// </summary>
        /// <param name="innerPaths">Используемые пути</param>
        /// <param name="expression">Выражение</param>
        /// <param name="isNullEqualityComparison">Является ли выражение сравнением с null</param>
        public PathExpressionContainer(
            [NotNull]IReadOnlyCollection<ParsedPath> innerPaths, 
            [NotNull]Expression expression,
            bool isNullEqualityComparison)
        {
            if (innerPaths == null) throw new ArgumentNullException("innerPaths");
            if (expression == null) throw new ArgumentNullException("expression");
            InnerPaths = innerPaths;
            Expression = expression;
            IsNullEqualityComparison = isNullEqualityComparison;
            _type = expression.Type;
        }
    }
}
