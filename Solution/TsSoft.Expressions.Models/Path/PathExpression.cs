﻿namespace TsSoft.Expressions.Models.Path
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Выражение для представления пути
    /// </summary>
    public class PathExpression : Expression
    {
        [NotNull]private readonly Type _type;

        /// <summary>
        /// Путь
        /// </summary>
        public ParsedPath Path { get; private set; }
        /// <summary>
        /// Тип, получаемый на пути
        /// </summary>
        public override Type Type { get { return _type; } }

        /// <summary>
        /// Создать выражение для представления пути
        /// </summary>
        /// <param name="path">Путь</param>
        /// <param name="type">Тип, получаемый на пути</param>
        public PathExpression(ParsedPath path, [NotNull]Type type)
        {
            if (type == null) throw new ArgumentNullException("type");
            Path = path;
            _type = type;
        }
    }
}
