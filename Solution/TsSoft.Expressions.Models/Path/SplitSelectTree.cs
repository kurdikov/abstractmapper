﻿namespace TsSoft.Expressions.Models.Path
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Разбиение дерева свойств для выборки на внутренние и внешние свойства
    /// </summary>
    public class SplitSelectTree
    {
        /// <summary>
        /// Дерево внутренних свойств
        /// </summary>
        [NotNull]
        public SelectTree InternalTree { get; private set; }

        /// <summary>
        /// Пути к внешним свойствам и деревья выбора внутри них
        /// </summary>
        [CanBeNull]
        public IReadOnlyCollection<KeyValuePair<IReadOnlyCollection<ValueHoldingMember>, SelectTreeNode>> ExternalTrees { get; private set; }

        /// <summary>
        /// Разбиение дерева свойств для выборки на внутренние и внешние свойства
        /// </summary>
        public SplitSelectTree([NotNull] SelectTree internalTree, [CanBeNull] IReadOnlyCollection<KeyValuePair<IReadOnlyCollection<ValueHoldingMember>, SelectTreeNode>> externalTrees)
        {
            if (internalTree == null) throw new ArgumentNullException("internalTree");
            InternalTree = internalTree;
            ExternalTrees = externalTrees;
        }
    }
}
