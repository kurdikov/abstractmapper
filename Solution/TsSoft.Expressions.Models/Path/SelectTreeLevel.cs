namespace TsSoft.Expressions.Models.Path
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// ����� �������, ��������� �� ��������
    /// </summary>
    public class SelectTreeLevel : Dictionary<ValueHoldingMember, SelectTreeNode>
    {
        /// <summary>
        /// ����� �������, ��������� �� ��������
        /// </summary>
        public SelectTreeLevel()
            : base(NameComparer.Instance)
        {
        }

        /// <summary>
        /// ����� �������, ��������� �� ��������
        /// </summary>
        /// <param name="nodes">�������� � �������������</param>
        public SelectTreeLevel([NotNull]IEnumerable<KeyValuePair<ValueHoldingMember, SelectTreeNode>> nodes)
            : this()
        {
            if (nodes == null) throw new ArgumentNullException("nodes");
            foreach (var node in nodes)
            {
                if (node.Key == null)
                {
                    continue;
                }
                this[node.Key] = node.Value;
            }
        }

        private class NameComparer : IEqualityComparer<ValueHoldingMember>
        {
            [NotNull]
            public static readonly NameComparer Instance = new NameComparer();

            public bool Equals(ValueHoldingMember x, ValueHoldingMember y)
            {
                return x == null && y == null || x != null && y != null && x.Name == y.Name;
            }

            public int GetHashCode(ValueHoldingMember obj)
            {
                return obj != null ? obj.GetHashCode() : 0;
            }
        }
        
    }
}
