﻿namespace TsSoft.Expressions.Models.Path
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Построитель пути
    /// </summary>
    public class ParsedPathBuilder
    {
        [CanBeNull]private readonly ParsedPathBuilder _head;
        [NotNull]private readonly ParameterExpression _start;
        [CanBeNull]private readonly PathElement _element;

        /// <summary>
        /// Создать построитель пути по началу пути
        /// </summary>
        /// <param name="start">Параметр пути</param>
        public ParsedPathBuilder([NotNull]ParameterExpression start)
        {
            if (start == null) throw new ArgumentNullException("start");
            _start = start;
        }

        private ParsedPathBuilder([NotNull] ParsedPathBuilder head, [NotNull] PathElement nextElement)
        {
            _head = head;
            _element = nextElement;
            _start = _head._start;
        }

        /// <summary>
        /// Добавить элемент в построитель пути
        /// </summary>
        /// <param name="element">Новый элемент</param>
        /// <returns>Построитель пути с добавленным элементом</returns>
        [NotNull]
        public ParsedPathBuilder AddElement([NotNull] PathElement element)
        {
            if (element == null) throw new ArgumentNullException("element");
            return new ParsedPathBuilder(this, element);
        }

        /// <summary>
        /// Извлечь путь из построителя
        /// </summary>
        [NotNull]
        public ParsedPath ToPath()
        {
            return new ParsedPath(EnumerateElements(), _start);
        }

        [NotNull]
        private List<PathElement> EnumerateElements()
        {
            var result = _head != null
                ? _head.EnumerateElements()
                : new List<PathElement>();
            if (_element != null)
            {
                result.Add(_element);
            }
            return result;
        }
    }
}
