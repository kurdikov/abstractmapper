﻿namespace TsSoft.Expressions.Models.Path
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Дерево свойств для выборки
    /// </summary>
    public class SelectTree
    {
        /// <summary>
        /// Специальный экземпляр дерева свойств для представления пустого продолжения
        /// </summary>
        public static SelectTree ExhaustedNesting = new SelectTree(Expression.Parameter(typeof(object)));

        /// <summary>
        /// Стартовый параметр путей
        /// </summary>
        [NotNull]
        public ParameterExpression Start { get; private set; }

        /// <summary>
        /// Узел верхнего уровня (узел выбираемой сущности)
        /// </summary>
        [NotNull]
        public SelectTreeNode RootNode { get; private set; }

        /// <summary>
        /// Внешние инклюды
        /// </summary>
        [CanBeNull]
        public IReadOnlyCollection<LambdaExpression> ExternalIncludes { get; set; }
        
        /// <summary>
        /// Дерево свойств для выборки
        /// </summary>
        /// <param name="start">Стартовый параметр</param>
        public SelectTree([NotNull] ParameterExpression start)
            : this(start, new SelectTreeNode(GetParameterType(start)))
        {
        }

        [NotNull]
        private static Type GetParameterType([NotNull]ParameterExpression start)
        {
            if (start == null) throw new ArgumentNullException("start");
            return start.Type;
        }

        /// <summary>
        /// Дерево свойств для выборки
        /// </summary>
        /// <param name="start">Стартовый параметр</param>
        /// <param name="node">Корневой узел</param>
        public SelectTree([NotNull] ParameterExpression start, [NotNull]SelectTreeNode node)
        {
            if (start == null) throw new ArgumentNullException("start");
            if (node == null) throw new ArgumentNullException("node");
            Start = start;
            RootNode = node;
            RootNode.AddSelectParameter(start);
        }

        /// <summary>
        /// Дерево свойств для выборки
        /// </summary>
        /// <param name="start">Стартовый параметр</param>
        /// <param name="firstLevel">Первый уровень дерева выбранных свойств</param>
        public SelectTree([NotNull] ParameterExpression start, [NotNull]SelectTreeLevel firstLevel)
            : this(start)
        {
            if (firstLevel == null) throw new ArgumentNullException("firstLevel");
            RootNode.NextLevel = firstLevel;
        }
    }
}
