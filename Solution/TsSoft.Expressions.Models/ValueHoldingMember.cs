﻿namespace TsSoft.Expressions.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Свойство или поле
    /// </summary>
    public class ValueHoldingMember : IEquatable<ValueHoldingMember>, IEquatable<MemberInfo>, ICustomAttributeProvider
    {
        private readonly MemberInfo _member;

        /// <summary>
        /// Является ли пустым
        /// </summary>
        public bool IsNull
        {
            get { return _member == null; }
        }

        /// <summary>
        /// Поле
        /// </summary>
        public FieldInfo Field
        {
            get { return _member as FieldInfo; }
        }

        /// <summary>
        /// Свойство
        /// </summary>
        public PropertyInfo Property
        {
            get { return _member as PropertyInfo; }
        }

        /// <summary>
        /// Объявивший тип
        /// </summary>
        [NotNull]
        public Type DeclaringType
        {
            get
            {
                if (_member == null) throw new NullReferenceException();
                var result = _member.DeclaringType;
                if (result == null)
                {
                    throw new InvalidOperationException(string.Format("Member {0} has no declaring type", _member));
                }
                return result;
            }
        }

        /// <summary>
        /// Тип хранимого значения
        /// </summary>
        /// <exception cref="NullReferenceException">Пустой член</exception>
        [NotNull]
        public Type ValueType
        {
            get
            {
                if (Property != null) return Property.PropertyType;
                if (Field != null) return Field.FieldType;
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Можно ли записать значение
        /// </summary>
        public bool IsWriteable
        {
            get
            {
                if (Property != null) return Property.CanWrite;
                if (Field != null) return !Field.IsInitOnly;
                return false;
            }
        }

        /// <summary>
        /// Можно ли считать значение
        /// </summary>
        public bool IsReadable
        {
            get
            {
                if (Property != null) return Property.CanRead;
                if (Field != null) return true;
                return false;
            }
        }

        /// <summary>
        /// Имя
        /// </summary>
        [NotNull]
        public string Name
        {
            get
            {
                if (_member == null) throw new NullReferenceException();
                return _member.Name;
            }
        }

        /// <summary>
        /// Получить атрибут
        /// </summary>
        /// <typeparam name="T">Тип атрибута</typeparam>
        public T GetCustomAttribute<T>() where T : Attribute
        {
            if (_member != null) return _member.GetCustomAttribute<T>();
            return null;
        }

        /// <summary>
        /// Получить атрибуты
        /// </summary>
        /// <typeparam name="T">Тип атрибута</typeparam>
        [NotNull]
        public IEnumerable<T> GetCustomAttributes<T>() where T : Attribute
        {
            if (_member != null) return _member.GetCustomAttributes<T>() ?? Enumerable.Empty<T>();
            return Enumerable.Empty<T>();
        }

        /// <summary>
        /// Имеет ли член атрибут
        /// </summary>
        /// <typeparam name="T">Тип атрибута</typeparam>
        public bool HasAttribute<T>() where T : Attribute
        {
            return _member != null && _member.IsDefined(typeof(T));
        }

        /// <summary>
        /// Является ли непубличным свойством с геттером
        /// </summary>
        public bool IsNonPublicPropertyWithGetter
        {
            get
            {
                var property = Property;
                return property != null && property.GetMethod != null && !property.GetMethod.IsPublic;
            }
        }

        /// <summary>
        /// Получить значение
        /// </summary>
        public object GetValue(object obj)
        {
            if (Property != null) return Property.GetValue(obj);
            if (Field != null) return Field.GetValue(obj);
            return null;
        }

        /// <summary>
        /// Установить значение
        /// </summary>
        public void SetValue(object obj, object value)
        {
            if (Property != null)
            {
                Property.SetValue(obj, value);
            }
            else if (Field != null)
            {
                Field.SetValue(obj, value);
            }
        }

        /// <summary>
        /// Создать обёртку для поля или свойства
        /// </summary>
        public ValueHoldingMember([NotNull]MemberInfo member)
        {
            if (member == null) throw new ArgumentNullException("member");
            if (member.MemberType != MemberTypes.Field && member.MemberType != MemberTypes.Property)
            {
                throw new ArgumentException(string.Format("{0} defined on {1} is neither a property nor a field", member, member.DeclaringType));
            }
            _member = member;
        }

        /// <summary>
        /// Создать обёртку для поля
        /// </summary>
        public ValueHoldingMember(FieldInfo field)
        {
            _member = field;
        }

        /// <summary>
        /// Создать обёртку для свойства
        /// </summary>
        public ValueHoldingMember(PropertyInfo property)
        {
            _member = property;
        }

        ///// <summary>
        ///// Создать обёртку для поля
        ///// </summary>
        //public static implicit operator ValueHoldingMember(FieldInfo field)
        //{
        //    return new ValueHoldingMember(field);
        //}

        ///// <summary>
        ///// Создать обёртку для свойства
        ///// </summary>
        //public static implicit operator ValueHoldingMember(PropertyInfo property)
        //{
        //    return new ValueHoldingMember(property);
        ////}

        ///// <summary>
        ///// Равны ли члены
        ///// </summary>
        //public static bool operator ==(ValueHoldingMember x, MemberInfo y)
        //{
        //    return (ReferenceEquals(x, null) && ReferenceEquals(y, null)) || !ReferenceEquals(x, null) && x.Equals(y);
        //}

        ///// <summary>
        ///// Не равны ли члены
        ///// </summary>
        //public static bool operator !=(ValueHoldingMember x, MemberInfo y)
        //{
        //    return !ReferenceEquals(x, null) && !(x.Equals(y))
        //        || x == null && y != null;
        //}

        /// <summary>
        /// Получить обёрнутый член
        /// </summary>
        /// <exception cref="NullReferenceException">Член - null</exception>
        [NotNull]
        public MemberInfo Member
        {
            get
            {
                var result = (MemberInfo) Property ?? Field;
                if (result == null)
                {
                    throw new NullReferenceException();
                }
                return result;
            }
        }


        /// <summary>
        /// Равны ли члены
        /// </summary>
        public bool Equals(MemberInfo other)
        {
            if (Property != null) return MemberInfoComparer.Instance.Equals(Property, other);
            if (Field != null) return MemberInfoComparer.Instance.Equals(Field, other);
            return other == null;
        }

        /// <summary>
        /// Равны ли члены
        /// </summary>
        public bool Equals(ValueHoldingMember other)
        {
            return other != null && MemberInfoComparer.Instance.Equals(_member, other._member);
        }

        /// <summary>
        /// Равны ли члены
        /// </summary>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return IsNull;
            }
            var vhm = obj as ValueHoldingMember;
            if (vhm != null) return Equals(vhm);
            var mi = obj as MemberInfo;
            if (mi != null) return Equals(mi);
            return false;
        }

        /// <summary>
        /// Хэш
        /// </summary>
        public override int GetHashCode()
        {
            unchecked
            {
                if (Property != null) return MemberInfoComparer.Instance.GetHashCode(Property);
                if (Field != null) return MemberInfoComparer.Instance.GetHashCode(Field);
                return 0;
            }
        }

        /// <summary>
        /// Получить член по типу и имени
        /// </summary>
        public static ValueHoldingMember GetValueHoldingMember([NotNull] Type type, [NotNull] string name)
        {
            return GetValueHoldingMember(type.GetTypeInfo(), name);
        }

        /// <summary>
        /// Получить член по типу и имени
        /// </summary>
        public static ValueHoldingMember GetValueHoldingMember([NotNull] TypeInfo type, [NotNull] string name)
        {
            var property = type.GetProperty(name, Type.EmptyTypes);
            if (property != null) return new ValueHoldingMember(property);
            var field = type.GetField(name);
            if (field != null) return new ValueHoldingMember(field);
            return null;
        }

        /// <summary>
        /// Получить все члены типа, содержащие значение
        /// </summary>
        [NotNull]
        public static IEnumerable<ValueHoldingMember> GetValueHoldingMembers([NotNull] Type type)
        {
            return GetValueHoldingMembers(type, BindingFlags.Public | BindingFlags.Instance);
        }

        /// <summary>
        /// Получить все члены типа, содержащие значение
        /// </summary>
        [NotNull]
        public static IEnumerable<ValueHoldingMember> GetValueHoldingMembers([NotNull] TypeInfo type)
        {
            return GetValueHoldingMembers(type, BindingFlags.Public | BindingFlags.Instance);
        }

        /// <summary>
        /// Получить все члены типа, содержащие значение
        /// </summary>
        public static IEnumerable<ValueHoldingMember> GetValueHoldingMembers([NotNull] Type type, BindingFlags bindingFlags)
        {
            return GetValueHoldingMembers(type.GetTypeInfo(), bindingFlags);
        }

        /// <summary>
        /// Получить все члены типа, содержащие значение
        /// </summary>
        [NotNull]
        public static IEnumerable<ValueHoldingMember> GetValueHoldingMembers([NotNull] TypeInfo type, BindingFlags bindingFlags)
        {
            if (type.IsInterface)
            {
                return type.GetProperties(bindingFlags)
                        .Concat(type.GetInterfaces().Where(i => i != null).SelectMany(i => i.GetTypeInfo().GetProperties(bindingFlags)))
                        .Select(p => new ValueHoldingMember(p));
            }
            return type.GetProperties(bindingFlags)
                .Where(p => p != null && p.GetIndexParameters().Length == 0)
                .Select(p => new ValueHoldingMember(p))
                .Concat(type.GetFields(bindingFlags).Select(f => new ValueHoldingMember(f)));
        }

        /// <summary>
        /// Получить член из выражения
        /// </summary>
        [NotNull]
        public static ValueHoldingMember GetFromExpression([NotNull] MemberExpression expression)
        {
            if (expression.Member.MemberType == MemberTypes.Property)
            {
                return new ValueHoldingMember((PropertyInfo)expression.Member);
            }
            if (expression.Member.MemberType == MemberTypes.Field)
            {
                return new ValueHoldingMember((FieldInfo)expression.Member);
            }
            throw new InvalidOperationException(string.Format("Expression {0} does not contain a property or field access", expression));
        }

        /// <summary>
        /// Строковое представление
        /// </summary>
        public override string ToString()
        {
            return _member != null ? _member.ToString() : "null";
        }

        /// <summary>
        /// Получить атрибуты
        /// </summary>
        /// <param name="attributeType">Тип атрибута</param>
        /// <param name="inherit">Считывать ли атрибуты с базовых типов</param>
        public object[] GetCustomAttributes(Type attributeType, bool inherit)
        {
            if (Property != null)
            {
                return Property.GetCustomAttributes(attributeType, inherit).ToArray<object>();
            }
            if (Field != null)
            {
                return Field.GetCustomAttributes(attributeType, inherit).ToArray<object>();
            }
            return new object[0];
        }

        /// <summary>
        /// Получить атрибуты
        /// </summary>
        /// <param name="inherit">Считывать ли атрибуты с базовых типов</param>
        public object[] GetCustomAttributes(bool inherit)
        {
            if (Property != null)
            {
                return Property.GetCustomAttributes(inherit).ToArray<object>();
            }
            if (Field != null)
            {
                return Field.GetCustomAttributes(inherit).ToArray<object>();
            }
            return new object[0];
        }

        /// <summary>
        /// Висит ли атрибут
        /// </summary>
        /// <param name="attributeType">Тип атрибута</param>
        /// <param name="inherit">Считывать ли атрибуты с базовых типов</param>
        public bool IsDefined(Type attributeType, bool inherit)
        {
            if (Property != null)
            {
                return Property.IsDefined(attributeType, inherit);
            }
            if (Field != null)
            {
                return Field.IsDefined(attributeType, inherit);
            }
            return false;
        }

        /// <summary>
        /// Является ли член публичным
        /// </summary>
        public bool IsPublic
        {
            get
            {
                if (Property != null)
                {
                    return Property.GetGetMethod() != null
                        || Property.GetSetMethod() != null;
                }
                else if (Field != null)
                {
                    return Field.IsPublic;
                }
                return false;
            }
        }
    }
}
