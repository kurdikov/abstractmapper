﻿namespace TsSoft.Expressions.Models.Differences
{
    using System.Collections.Generic;

    /// <summary>
    /// Различие в коллекциях
    /// </summary>
    /// <typeparam name="T">Тип элемента коллекции</typeparam>
    public class CollectionDiff<T> : ICollectionDiff<T>
    {
        /// <summary>
        /// Присутствующие в первой и отсутствующие во второй коллекции элементы
        /// </summary>
        public IReadOnlyCollection<T> FirstExcess { get; set; }

        /// <summary>
        /// Присутствующие во второй и отсутствующие в первой коллекции элементы
        /// </summary>
        public IReadOnlyCollection<T> SecondExcess { get; set; }

        /// <summary>
        /// Присутствующие в обеих коллекциях элементы и различия между ними (экземпляры объектов берутся из первой коллекции)
        /// </summary>
        public IReadOnlyCollection<IObjectWithDiff<T>> Diffs { get; set; }

        /// <summary>
        /// Является ли первая коллекция null
        /// </summary>
        public bool FirstIsNull { get; set; }

        /// <summary>
        /// Является ли вторая коллекция null
        /// </summary>
        public bool SecondIsNull { get; set; }
    }
}
