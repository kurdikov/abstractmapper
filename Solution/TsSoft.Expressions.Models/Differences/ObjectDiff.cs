﻿namespace TsSoft.Expressions.Models.Differences
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Различия в значениях объекта
    /// </summary>
    public class ObjectDiff : IObjectDiff
    {
        /// <summary>
        /// Различия в свойствах объекта
        /// </summary>
        [NotNull]
        private readonly IDictionary<string, IDiff> _propertyDiffs = new Dictionary<string, IDiff>();

        private TDiff Diff<TDiff>(string propertyName)
            where TDiff: class, IDiff
        {
            if (propertyName == null) throw new ArgumentNullException("propertyName");
            IDiff result;
            if (!_propertyDiffs.TryGetValue(propertyName, out result) || result == null)
            {
                return null;
            }
            var typedResult = result as TDiff;
            if (typedResult == null)
            {
                throw new ArgumentOutOfRangeException(string.Format("Diff for property {0} has type {1} and is not castable to {2}",
                    propertyName, result.GetType(), typeof(TDiff)));
            }
            return typedResult;
        }

        /// <summary>
        /// Получить различие в значениях простого свойства объекта
        /// </summary>
        /// <typeparam name="T">Тип значения свойства</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Различие в значениях свойства или null, если различий нет</returns>
        public IPrimitiveDiff<T> PrimitiveDiff<T>(string propertyName)
        {
            return Diff<IPrimitiveDiff<T>>(propertyName);
        }

        /// <summary>
        /// Получить различие в значениях простого свойства объекта
        /// </summary>
        /// <typeparam name="T">Тип значения свойства</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Различие в значениях свойства или null, если различий нет</returns>
        public IObjectDiff<T> SubobjectDiff<T>(string propertyName)
        {
            return Diff<IObjectDiff<T>>(propertyName);
        }

        /// <summary>
        /// Получить различие в значениях простого свойства объекта
        /// </summary>
        /// <typeparam name="T">Тип значения элемента свойства</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Различие в значениях свойства или null, если различий нет</returns>
        public ICollectionDiff<T> CollectionDiff<T>(string propertyName)
        {
            return Diff<ICollectionDiff<T>>(propertyName);
        }

        /// <summary>
        /// Является ли первый объект null
        /// </summary>
        public bool FirstIsNull { get; set; }

        /// <summary>
        /// Является ли второй объект null
        /// </summary>
        public bool SecondIsNull { get; set; }

        /// <summary>
        /// Добавить различие в свойстве
        /// </summary>
        /// <param name="property">Свойство</param>
        /// <param name="diff">Различие</param>
        public void AddDiff([NotNull]PropertyInfo property, [NotNull]IDiff diff)
        {
            _propertyDiffs.Add(property.Name, diff);
        }

        /// <summary>
        /// Имеются ли различия
        /// </summary>
        public bool HasDiffs {get { return _propertyDiffs.Any(); }}
    }

    /// <summary>
    /// Различия в объектах
    /// <typeparam name="T">Тип объекта</typeparam>
    /// </summary>
    public class ObjectDiff<T> : ObjectDiff, IObjectDiff<T>
    {
    }
}
