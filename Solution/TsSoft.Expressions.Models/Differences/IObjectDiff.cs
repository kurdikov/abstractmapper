﻿namespace TsSoft.Expressions.Models.Differences
{
    using JetBrains.Annotations;

    /// <summary>
    /// Различия в объектах
    /// </summary>
    public interface IObjectDiff : IDiff
    {
        /// <summary>
        /// Получить различие в значениях простого свойства объекта
        /// </summary>
        /// <typeparam name="TProp">Тип значения свойства</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Различие в значениях свойства или null, если различий нет</returns>
        IPrimitiveDiff<TProp> PrimitiveDiff<TProp>([NotNull]string propertyName);

        /// <summary>
        /// Получить различие в значениях простого свойства объекта
        /// </summary>
        /// <typeparam name="TProp">Тип значения свойства</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Различие в значениях свойства или null, если различий нет</returns>
        IObjectDiff<TProp> SubobjectDiff<TProp>([NotNull] string propertyName);

        /// <summary>
        /// Получить различие в значениях простого свойства объекта
        /// </summary>
        /// <typeparam name="TPropElem">Тип значения элемента свойства</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns>Различие в значениях свойства или null, если различий нет</returns>
        ICollectionDiff<TPropElem> CollectionDiff<TPropElem>([NotNull] string propertyName); 

        /// <summary>
        /// Является ли первый объект null
        /// </summary>
        bool FirstIsNull { get; }

        /// <summary>
        /// Является ли второй объект null
        /// </summary>
        bool SecondIsNull { get; }
    }

    /// <summary>
    /// Различия в объектах
    /// <typeparam name="T">Тип объекта</typeparam>
    /// </summary>
    public interface IObjectDiff<out T> : IObjectDiff
    {
    }
}
