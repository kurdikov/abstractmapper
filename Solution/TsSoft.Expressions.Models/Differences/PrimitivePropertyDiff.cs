﻿namespace TsSoft.Expressions.Models.Differences
{
    using System;

    /// <summary>
    /// Изменение простого свойства
    /// </summary>
    public class PrimitiveDiff<T> : IPrimitiveDiff<T>
    {
        /// <summary>
        /// Тип свойства
        /// </summary>
        public Type Type { get { return typeof(T); } }

        /// <summary>
        /// Значение свойства в первом объекте
        /// </summary>
        object IPrimitiveDiff.First
        {
            get { return First; }
        }

        /// <summary>
        /// Значение свойства во втором объекте
        /// </summary>
        object IPrimitiveDiff.Second
        {
            get { return Second; }
        }

        /// <summary>
        /// Значение свойства в первом объекте
        /// </summary>
        public T First { get; set; }

        /// <summary>
        /// Значение свойства во втором объекте
        /// </summary>
        public T Second { get; set; }
    }
}
