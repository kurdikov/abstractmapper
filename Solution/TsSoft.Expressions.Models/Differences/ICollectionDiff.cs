﻿namespace TsSoft.Expressions.Models.Differences
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Различие в коллекциях
    /// </summary>
    public interface ICollectionDiff : IPrimitiveCollectionDiff
    {
    }

    /// <summary>
    /// Различие в коллекциях
    /// </summary>
    /// <typeparam name="T">Тип элемента коллекции</typeparam>
    public interface ICollectionDiff<out T> : ICollectionDiff, IPrimitiveCollectionDiff<T>
    {
        /// <summary>
        /// Присутствующие в обеих коллекциях элементы и различия между ними (экземпляры объектов берутся из первой коллекции)
        /// </summary>
        [NotNull]
        IReadOnlyCollection<IObjectWithDiff<T>> Diffs { get; }
    }
}
