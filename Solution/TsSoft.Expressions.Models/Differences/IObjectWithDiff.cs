﻿namespace TsSoft.Expressions.Models.Differences
{
    /// <summary>
    /// Объект и его различия с другим объектом
    /// </summary>
    public interface IObjectWithDiff<out T>
    {
        /// <summary>
        /// Объект
        /// </summary>
        T Object { get; }

        /// <summary>
        /// Различия
        /// </summary>
        IObjectDiff<T> Diff { get; }
    }

    /// <summary>
    /// Объект и его различия с другим объектом
    /// </summary>
    public class ObjectWithDiff<T> : IObjectWithDiff<T>
    {
        /// <summary>
        /// Объект
        /// </summary>
        public T Object { get; set; }

        /// <summary>
        /// Различия
        /// </summary>
        public IObjectDiff<T> Diff { get; set; }
    }
}
