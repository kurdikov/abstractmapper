﻿namespace TsSoft.Expressions.Models.Differences
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Различия в коллекциях простых элементов
    /// </summary>
    public interface IPrimitiveCollectionDiff : IDiff
    {
        /// <summary>
        /// Является ли первая коллекция null
        /// </summary>
        bool FirstIsNull { get; }

        /// <summary>
        /// Является ли вторая коллекция null
        /// </summary>
        bool SecondIsNull { get; }
    }

    /// <summary>
    /// Различия в коллекциях простых элементов
    /// </summary>
    public interface IPrimitiveCollectionDiff<out T> : IPrimitiveCollectionDiff
    {
        /// <summary>
        /// Присутствующие в первой и отсутствующие во второй коллекции элементы
        /// </summary>
        [NotNull]
        IReadOnlyCollection<T> FirstExcess { get; }

        /// <summary>
        /// Присутствующие во второй и отсутствующие в первой коллекции элементы
        /// </summary>
        [NotNull]
        IReadOnlyCollection<T> SecondExcess { get; }
    }
}
