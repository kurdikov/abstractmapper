﻿namespace TsSoft.Expressions.Models.Differences
{
    using System;

    /// <summary>
    /// Различие в значениях простого свойства
    /// </summary>
    public interface IPrimitiveDiff : IDiff
    {
        /// <summary>
        /// Тип значения свойства
        /// </summary>
        Type Type { get; }

        /// <summary>
        /// Значение в первом объекте
        /// </summary>
        object First { get; }

        /// <summary>
        /// Значение во втором объекте
        /// </summary>
        object Second { get; }
    }

    /// <summary>
    /// Различие в значениях простого свойства
    /// </summary>
    /// <typeparam name="T">Тип свойства</typeparam>
    public interface IPrimitiveDiff<out T> : IPrimitiveDiff
    {
        /// <summary>
        /// Значение в первом объекте
        /// </summary>
        new T First { get; }

        /// <summary>
        /// Значение во втором объекте
        /// </summary>
        new T Second { get; }
    }
}
