﻿// ReSharper disable CheckNamespace
namespace TsSoft.Expressions.Helpers.Reflection
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Сравнивает MemberInfo на равенство, считая равными члены, полученные из базового типа и типа-наследника, если наследник не переопределил член
    /// </summary>
    public class MemberInfoComparer :
        IEqualityComparer<MemberInfo>,
        IEqualityComparer<PropertyInfo>,
        IEqualityComparer<ValueHoldingMember>
    {
        /// <summary>
        /// Сравнивает MemberInfo на равенство, считая равными члены, полученные из базового типа и типа-наследника, если наследник не переопределил член
        /// </summary>
        [NotNull]
        public static readonly MemberInfoComparer Instance = new MemberInfoComparer();

        /// <summary>
        /// Совпадают ли члены
        /// </summary>
        public bool Equals(MemberInfo x, MemberInfo y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            return x != null && y != null && x.MetadataToken == y.MetadataToken && x.Module.Equals(y.Module) && x.DeclaringType == y.DeclaringType;
        }

        /// <summary>
        /// Получить хэш члена
        /// </summary>
        public int GetHashCode([NotNull]MemberInfo obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }
            return obj.MetadataToken.GetHashCode() ^ obj.Module.GetHashCode() ^ (obj.DeclaringType != null ? obj.DeclaringType.GetHashCode() : 0);
        }

        /// <summary>
        /// Совпадают ли свойства
        /// </summary>
        public bool Equals(PropertyInfo x, PropertyInfo y)
        {
            return Equals((MemberInfo)x, y);
        }

        /// <summary>
        /// Получить хэш свойства
        /// </summary>
        public int GetHashCode([NotNull]PropertyInfo obj)
        {
            return GetHashCode((MemberInfo) obj);
        }

        /// <summary>
        /// Равны ли члены
        /// </summary>
        public bool Equals(ValueHoldingMember x, ValueHoldingMember y)
        {
            return x != null && x.Equals(y) || x == null && y == null;
        }

        /// <summary>
        /// Получить хэш члена
        /// </summary>
        public int GetHashCode(ValueHoldingMember obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }
            return obj.GetHashCode();
        }
    }
}
