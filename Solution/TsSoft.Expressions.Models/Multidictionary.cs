﻿namespace TsSoft.Expressions.Models
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    
    /// <summary>
    /// Мультисловарь
    /// </summary>
    /// <typeparam name="TKey">Тип ключа</typeparam>
    /// <typeparam name="TValue">Тип значения</typeparam>
    public class Multidictionary<TKey, TValue> : IDictionary<TKey, ISet<TValue>>, ILookup<TKey, TValue>
    {
        [NotNull] private readonly IDictionary<TKey, HashSet<TValue>> _dictionary;
        [NotNull] private readonly IEqualityComparer<TValue> _valueComparer;

        /// <summary>
        /// Мультисловарь
        /// </summary>
        public Multidictionary(IEqualityComparer<TKey> keyComparer = null, IEqualityComparer<TValue> valueComparer = null)
        {
            _dictionary = new Dictionary<TKey, HashSet<TValue>>(keyComparer);
            _valueComparer = valueComparer ?? EqualityComparer<TValue>.Default;
        }

        private class Grouping : IGrouping<TKey, TValue>
        {
            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public IEnumerator<TValue> GetEnumerator()
            {
                return Values.GetEnumerator();
            }

            public TKey Key { get; private set; }
            [NotNull]private IEnumerable<TValue> Values { get; set; }

            public Grouping(TKey key, [NotNull] IEnumerable<TValue> values)
            {
                if (values == null) throw new ArgumentNullException("values");
                Key = key;
                Values = values;
            }
        }

        IEnumerator<IGrouping<TKey, TValue>> IEnumerable<IGrouping<TKey, TValue>>.GetEnumerator()
        {
            return _dictionary.Select(kv => new Grouping(kv.Key, kv.Value ?? Enumerable.Empty<TValue>())).GetEnumerator();
        }

        /// <summary>
        /// Содержится ли ключ в словаре
        /// </summary>
        public bool Contains(TKey key)
        {
            return _dictionary.ContainsKey(key);
        }

        IEnumerable<TValue> ILookup<TKey, TValue>.this[TKey key]
        {
            get { return Contains(key) ? Get(key) : Enumerable.Empty<TValue>(); }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        [NotNull]
        private HashSet<TValue> Get(TKey key)
        {
            HashSet<TValue> result;
            if (!_dictionary.TryGetValue(key, out result) || result == null)
            {
                result = _dictionary[key] = new HashSet<TValue>(_valueComparer);
            }
            return result;
        }

        /// <summary>
        /// Добавить значения для ключа
        /// </summary>
        public void Add(KeyValuePair<TKey, ISet<TValue>> item)
        {
            Add(item.Key, item.Value);
        }

        /// <summary>
        /// Добавить значение для ключа
        /// </summary>
        public void Add(TKey key, TValue value)
        {
            Get(key).Add(value);
        }

        /// <summary>
        /// Получить перечислитель
        /// </summary>
        public IEnumerator<KeyValuePair<TKey, ISet<TValue>>> GetEnumerator()
        {
            return _dictionary.Select(kv => new KeyValuePair<TKey, ISet<TValue>>(kv.Key, kv.Value)).GetEnumerator();
        }

        /// <summary>
        /// Очистить словарь
        /// </summary>
        public void Clear()
        {
            _dictionary.Clear();
        }

        /// <summary>
        /// Содержит ли словарь все ключи для значения
        /// </summary>
        public bool Contains(KeyValuePair<TKey, ISet<TValue>> item)
        {
            return Get(item.Key).IsSupersetOf(item.Value ?? Enumerable.Empty<TValue>());
        }

        /// <summary>
        /// Скопировать словарь в массив
        /// </summary>
        public void CopyTo(KeyValuePair<TKey, ISet<TValue>>[] array, int arrayIndex)
        {
            _dictionary.Select(kv => new KeyValuePair<TKey, ISet<TValue>>(kv.Key, kv.Value)).ToList().CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Удалить значения из ключа
        /// </summary>
        public bool Remove(KeyValuePair<TKey, ISet<TValue>> item)
        {
            if (item.Value == null)
            {
                return false;
            }
            Get(item.Key).ExceptWith(item.Value);
            return true;
        }

        /// <summary>
        /// Количество ключей в словаре
        /// </summary>
        public int Count { get { return _dictionary.Count(); } }
        /// <summary>
        /// False
        /// </summary>
        public bool IsReadOnly { get { return false; } }
        /// <summary>
        /// Содержит ли словарь ключ
        /// </summary>
        public bool ContainsKey(TKey key)
        {
            return _dictionary.ContainsKey(key);
        }

        /// <summary>
        /// Добавить значения для ключа
        /// </summary>
        public void Add(TKey key, ISet<TValue> value)
        {
            if (value == null)
            {
                return;
            }
            Get(key).UnionWith(value);
        }

        /// <summary>
        /// Удалить ключ из словаря
        /// </summary>
        public bool Remove(TKey key)
        {
            return _dictionary.Remove(key);
        }

        /// <summary>
        /// Попытаться получить значения по ключу
        /// </summary>
        public bool TryGetValue(TKey key, out ISet<TValue> value)
        {
            HashSet<TValue> outResult;
            var result = _dictionary.TryGetValue(key, out outResult);
            value = outResult;
            return result;
        }

        /// <summary>
        /// Получить значения по ключу
        /// </summary>
        public ISet<TValue> this[TKey key]
        {
            get { return _dictionary[key]; }
            set { _dictionary[key] = new HashSet<TValue>(value ?? Enumerable.Empty<TValue>(), _valueComparer); }
        }

        /// <summary>
        /// Ключи
        /// </summary>
        public ICollection<TKey> Keys { get { return _dictionary.Keys; } }
        /// <summary>
        /// Значения
        /// </summary>
        ICollection<ISet<TValue>> IDictionary<TKey, ISet<TValue>>.Values { get { return _dictionary.Values.Cast<ISet<TValue>>().ToList(); } }
    }
}
