﻿namespace TsSoft.Expressions.Models.AbstractMapper
{
    using System.Linq.Expressions;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Описание пути
    /// </summary>
    public class PathDescription
    {
        /// <summary>
        /// Последовательность свойств в пути
        /// </summary>
        public ParsedPath Path { get; set; }

        /// <summary>
        /// Expression, выдёргивающий значение из размапливаемого объекта
        /// </summary>
        public Expression Expression { get; set; }
    }
}
