﻿namespace TsSoft.Expressions.Models.AbstractMapper
{
    using System.Linq.Expressions;

    /// <summary>
    /// Описание маппинга для свойства
    /// </summary>
    public class MappedPropertyDescription
    {
        /// <summary>
        /// Построенное выражение
        /// </summary>
        public Expression MapExpression { get; set; }

        /// <summary>
        /// Описание необходимого для маппинга маппера
        /// </summary>
        public MapperDescription MapperDescription { get; set; }
    }
}
