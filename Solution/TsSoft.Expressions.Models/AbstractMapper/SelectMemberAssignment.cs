﻿namespace TsSoft.Expressions.Models.AbstractMapper
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;

    /// <summary>
    /// Модель элемента селект-выражения
    /// </summary>
    public class SelectMemberAssignment
    {
        /// <summary>
        /// Использованное свойство сущности
        /// </summary>
        public ValueHoldingMember UsedMember { get; set; }

        /// <summary>
        /// Условие, наложенное на свойство-коллекцию
        /// </summary>
        public LambdaExpression Condition { get; set; }

        /// <summary>
        /// Селект-выражение для свойства-коллекции
        /// </summary>
        public LambdaExpression InnerSelect { get; set; }

        /// <summary>
        /// Выражение инициализации для свойства-класса
        /// </summary>
        public ICollection<KeyValuePair<MemberInfo, Expression>>  InnerAssignment { get; set; }
    }
}
