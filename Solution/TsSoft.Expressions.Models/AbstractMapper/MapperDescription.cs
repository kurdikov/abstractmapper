﻿namespace TsSoft.Expressions.Models.AbstractMapper
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Описание маппера
    /// </summary>
    public class MapperDescription
    {
        /// <summary>
        /// Генерик-параметр интерфейса использованного маппера 
        /// </summary>
        [NotNull]
        public Type MapperFromType { get; private set; }

        /// <summary>
        /// В случае, если MapperFromType - коллекция, - тип элемента коллекции, иначе MapperFromType
        /// </summary>
        [NotNull]
        public Type MapperFromSingleType { get; private set; }

        /// <summary>
        /// Генерик-параметр интерфейса использованного маппера 
        /// </summary>
        [NotNull]
        public Type MapperToType { get; private set; }

        /// <summary>
        /// В случае, если MapperToType - коллекция, - тип элемента коллекции, иначе MapperToType
        /// </summary>
        [NotNull]
        public Type MapperToSingleType { get; private set; }
        
        /// <summary>
        /// Маппер (заполняется, только если внутренний маппер создаётся во время создания внешнего)
        /// </summary>
        [CanBeNull]
        public object MapperObject { get; set; }

        /// <summary>
        /// Описание маппера
        /// </summary>
        public MapperDescription([NotNull] Type mapperFromType, [NotNull] Type mapperFromSingleType, [NotNull] Type mapperToType)
        {
            if (mapperFromType == null) throw new ArgumentNullException("mapperFromType");
            if (mapperFromSingleType == null) throw new ArgumentNullException("mapperFromSingleType");
            if (mapperToType == null) throw new ArgumentNullException("mapperToType");
            MapperFromType = mapperFromType;
            MapperFromSingleType = mapperFromSingleType;
            MapperToType = mapperToType;
            MapperToSingleType = mapperToType;
        }

        /// <summary>
        /// Описание маппера
        /// </summary>
        public MapperDescription([NotNull] Type mapperFromType, [NotNull] Type mapperFromSingleType, [NotNull] Type mapperToType, [NotNull] Type mapperToSingleType)
        {
            if (mapperFromType == null) throw new ArgumentNullException("mapperFromType");
            if (mapperFromSingleType == null) throw new ArgumentNullException("mapperFromSingleType");
            if (mapperToType == null) throw new ArgumentNullException("mapperToType");
            if (mapperToSingleType == null) throw new ArgumentNullException("mapperToSingleType");
            MapperFromType = mapperFromType;
            MapperFromSingleType = mapperFromSingleType;
            MapperToType = mapperToType;
            MapperToSingleType = mapperToSingleType;
        }

        /// <summary>
        /// Строковое представление
        /// </summary>
        public override string ToString()
        {
            return string.Format("mapper from {0} to {1}", MapperFromType, MapperToType);
        }
    }

}
