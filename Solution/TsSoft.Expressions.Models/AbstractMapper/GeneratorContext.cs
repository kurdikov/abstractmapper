﻿namespace TsSoft.Expressions.Models.AbstractMapper
{
    using System;
    using System.Collections.Concurrent;
    using JetBrains.Annotations;

    /// <summary>
    /// Контекст генерации селектов или инклюдов
    /// </summary>
    public class GeneratorContext : INestedGenerationContext
    {
        /// <summary>
        /// Остающееся количество уровней вложенности для типов генераторов
        /// </summary>
        public ConcurrentDictionary<Type, int> RemainingNestingLevel { get; set; }

        /// <summary>
        /// Контекст генерации селектов или инклюдов
        /// </summary>
        public GeneratorContext()
        {
            RemainingNestingLevel = new ConcurrentDictionary<Type, int>();
        }

        /// <summary>
        /// Контекст генерации селектов или инклюдов
        /// </summary>
        public GeneratorContext([NotNull]Type generatorType, int level)
        {
            if (generatorType == null) throw new ArgumentNullException("generatorType");
            RemainingNestingLevel = new ConcurrentDictionary<Type, int>();
            if (!RemainingNestingLevel.TryAdd(generatorType, level))
            {
                throw new InvalidOperationException("Could not create generator context");
            }
        }
    }
}
