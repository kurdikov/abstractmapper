﻿namespace TsSoft.Expressions.Models.AbstractMapper
{
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Описание использованного при построении expression для маппера пути
    /// </summary>
    public class MappedPathDescription
    {
        /// <summary>
        /// Путь - последовательность используемых свойств
        /// </summary>
        public ParsedPath Path { get; set; }

        /// <summary>
        /// Куда размаплено
        /// </summary>
        public ValueHoldingMember ToPath { get; set; }

        /// <summary>
        /// Описание маппера, необходимого для преобразования
        /// </summary>
        public MapperDescription MapperDescription { get; set; }

        /// <summary>
        /// Описание используемого при преобразовании внутреннего обработчика
        /// </summary>
        public ProcessorDescription ProcessorDescription { get; set; }
    }
}
