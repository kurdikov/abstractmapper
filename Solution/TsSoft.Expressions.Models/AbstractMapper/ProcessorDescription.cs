﻿namespace TsSoft.Expressions.Models.AbstractMapper
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Описание внутреннего для маппера преобразователя
    /// </summary>
    public class ProcessorDescription
    {
        /// <summary>
        /// Внутренний преобразователь
        /// </summary>
        [NotNull]
        public object Processor { get; private set; }

        /// <summary>
        /// Тип, принимаемый внутренним преобразователем
        /// </summary>
        [NotNull]
        public Type ProcessedType { get; private set; }

        /// <summary>
        /// Описание внутреннего для маппера преобразователя
        /// </summary>
        public ProcessorDescription([NotNull] object processor, [NotNull] Type processedType)
        {
            if (processor == null) throw new ArgumentNullException("processor");
            if (processedType == null) throw new ArgumentNullException("processedType");
            Processor = processor;
            ProcessedType = processedType;
        }
    }
}
