﻿namespace TsSoft.Expressions.Models.AbstractMapper
{
    /// <summary>
    /// Интерфейс генератора, способного включить себя в качестве зависимого генератора
    /// </summary>
    public interface ICyclePreventingGenerator
    {
        /// <summary>
        /// Максимальное количество вложений генератора в себя
        /// </summary>
        int MaxNestingLevel { get; }
    }
}
