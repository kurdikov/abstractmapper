﻿namespace TsSoft.Expressions.Models.AbstractMapper
{
    using System;
    using System.Collections.Concurrent;
    using JetBrains.Annotations;

    /// <summary>
    /// Интерфейс контекста для генерации с возможным вложением
    /// </summary>
    public interface INestedGenerationContext
    {
        /// <summary>
        /// Остающееся количество уровней вложенности для типов генераторов
        /// </summary>
        [NotNull]
        ConcurrentDictionary<Type, int> RemainingNestingLevel { get; }
    }
}
