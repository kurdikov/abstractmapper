﻿namespace TsSoft.Expressions.Models.AbstractMapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Выражение для маппинга
    /// </summary>
    /// <typeparam name="TFrom">Тип, из которого происходит маппинг</typeparam>
    /// <typeparam name="TTo">Тип, в который происходит маппинг</typeparam>
    public class MapperExpression<TFrom, TTo>
    {
        /// <summary>
        /// Выражение-преобразователь
        /// </summary>
        [NotNull]
        public Expression<Func<TFrom, TTo>> Expression { get; set; }

        /// <summary>
        /// Использованные при преобразовании пути
        /// </summary>
        [NotNull]
        public IReadOnlyCollection<MappedPathDescription> MappedPaths { get; set; }
    }
}
