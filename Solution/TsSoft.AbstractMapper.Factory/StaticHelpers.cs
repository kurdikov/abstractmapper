﻿namespace TsSoft.AbstractMapper.Factory
{
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Collections;
    using TsSoft.Expressions.Helpers.NestedGenerators;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Хелперы для фабрики мапперов
    /// </summary>
    public static class StaticHelpers
    {
        /// <summary>
        /// Хелпер для извлечения членов типа
        /// </summary>
        [NotNull]
        public static IMemberInfoHelper MemberInfo = new MemberInfoHelper();
        /// <summary>
        /// Библиотека методов
        /// </summary>
        [NotNull]
        public static IMemberInfoLibrary Library = new MemberInfoLibrary(MemberInfo);
        /// <summary>
        /// Преобразователь выражений в линейные пути
        /// </summary>
        [NotNull]
        public static IFlatPathParser FlatPathParser = new FlatPathParser();
        /// <summary>
        /// Построитель выражений
        /// </summary>
        [NotNull]
        public static IExpressionBuilder ExpressionBuilder = new ExpressionBuilder(MemberInfo, Library);

        /// <summary>
        /// Хелпер для оборачивания исключений из выражений
        /// </summary>
        [NotNull]
        public static IExceptionWrapperHelper ExceptionWrapperHelper = new ExceptionWrapperHelper(MemberInfo);

        /// <summary>
        /// Хелпер для построения коллекций с неизвестными на этапе компиляции типами
        /// </summary>
        [NotNull]
        public static ICollectionReflectionHelper CollectionReflectionHelper = new CollectionReflectionHelper(MemberInfo);

        /// <summary>
        /// Хелпер для работы с типами
        /// </summary>
        [NotNull]
        public static ITypeHelper TypeHelper = new TypeHelper();

        /// <summary>
        /// Хелпер для работы с коллекциями
        /// </summary>
        [NotNull]
        public static ICollectionHelper CollectionHelper = new CollectionHelper();

        /// <summary>
        /// Обходчик линейного пути
        /// </summary>
        [NotNull]
        public static IFlatPathApplicator PropertyApplicator = new FlatPathApplicator();

        /// <summary>
        /// Хелпер для работы с путями
        /// </summary>
        [NotNull]
        public static IPathHelper PathHelper = new PathHelper();

        /// <summary>
        /// Преобразователь выражения в путь
        /// </summary>
        [NotNull]
        public static IPathParser PathParser = new PathParser(Library, ExpressionBuilder, PathHelper);

        /// <summary>
        /// Хелпер для поиска соответствующих членов
        /// </summary>
        [NotNull]
        public static ICorrespondingMemberHelper CorrespondingMemberHelper = new CorrespondingMemberHelper();

        /// <summary>
        /// Определяет именование специальных членов класса
        /// </summary>
        [NotNull]
        public static IMemberNamingHelper MemberNamingHelper = new MemberNamingHelper();

        /// <summary>
        /// Хелпер для работы с выражениями-конструкторами
        /// </summary>
        [NotNull]
        public static INewExpressionHelper NewExpressionHelper = new NewExpressionHelper(CorrespondingMemberHelper, Library, MemberNamingHelper);

        /// <summary>
        /// Фабрика обновляторов объектов
        /// </summary>
        [NotNull]
        public static IUpdateObjectActionFactory UpdateObjectActionFactory = new UpdateObjectActionFactory(FlatPathParser, ExceptionWrapperHelper, MemberInfo, ExpressionBuilder, Library);

        /// <summary>
        /// Хелпер для построения объектов в контексте
        /// </summary>
        [NotNull]
        public static INestedGeneratorHelper NestedGeneratorHelper = new NestedGeneratorHelper();

        /// <summary>
        /// Хелпер для проверки существования неявных преобразований
        /// </summary>
        [NotNull]
        public static IConversionChecker ConversionChecker = new ConversionChecker();
    }
}
