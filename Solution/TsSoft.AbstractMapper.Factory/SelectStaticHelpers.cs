﻿namespace TsSoft.AbstractMapper.Factory
{
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.DynamicTypes;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.SelectBuilder.DynamicTypes;
    using TsSoft.Expressions.SelectBuilder.Helpers;

    /// <summary>
    /// Статические хелперы для фабрики мапперов
    /// </summary>
    public static class SelectStaticHelpers
    {
        /// <summary>
        /// Создатель типов
        /// </summary>
        [NotNull]
        public readonly static ITypeBuilderCreator TypeBuilderCreator = new TypeBuilderCreator();

        /// <summary>
        /// Создатель свойств
        /// </summary>
        [NotNull]
        public readonly static IPropertyCreator PropertyCreator = new PropertyCreator();

        /// <summary>
        /// Копировальщик типов
        /// </summary>
        [NotNull]
        public readonly static ITypeClonemaker TypeClonemaker = new TypeClonemaker(PropertyCreator);

        /// <summary>
        /// Создатель связываний свойств
        /// </summary>
        [NotNull]
        public readonly static IPropertyBindingCreator PropertyBindingCreator = new PropertyBindingCreator(
            StaticHelpers.Library, StaticHelpers.CorrespondingMemberHelper, StaticHelpers.MemberNamingHelper);
    }
}
