﻿namespace TsSoft.AbstractMapper.Factory
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Пустая коллекция типов
    /// </summary>
    public static class NoTypes
    {
        /// <summary>
        /// Пустая коллекция типов
        /// </summary>
        public static Type[] ZeroTypes = new Type[0];
    }

    /// <summary>
    /// Типы сущностей
    /// </summary>
    public class Entities
    {
        [NotNull]
        private static readonly Entities NoEntities = new Entities(NoTypes.ZeroTypes);

        /// <summary>
        /// Типы сущностей
        /// </summary>
        [NotNull]public readonly ICollection<Type> Types;
        private Entities([NotNull]ICollection<Type> types) { Types = types; }
        /// <summary>
        /// Создать окружение с типами сущностей
        /// </summary>
        /// <param name="types">Типы сущностей</param>
        [NotNull]
        public static Entities Are(params Type[] types)
        {
            return new Entities(types ?? None.Types);
        }
        /// <summary>
        /// Создать окружение с типами сущностей
        /// </summary>
        /// <param name="types">Типы сущностей</param>
        [NotNull]
        public static Entities Are([NotNull]ICollection<Type> types)
        {
            return new Entities(types);
        }
        /// <summary>
        /// Окружение без типов сущностей
        /// </summary>
        [NotNull]
        public static Entities None { get { return NoEntities; } }
    }

    /// <summary>
    /// Типы внешних сущностей
    /// </summary>
    public class External
    {
        [NotNull]
        private static readonly External NoExternal = new External(NoTypes.ZeroTypes);

        /// <summary>
        /// Типы внешних сущностей
        /// </summary>
        [NotNull]public readonly ICollection<Type> Types;
        private External([NotNull]ICollection<Type> types) { Types = types; }
        /// <summary>
        /// Создать окружение с типами внешних сущностей
        /// </summary>
        /// <param name="types">Типы внешних сущностей</param>
        [NotNull]
        public static External Are(params Type[] types)
        {
            return new External(types ?? None.Types);
        }
        /// <summary>
        /// Создать окружение с типами внешних сущностей
        /// </summary>
        /// <param name="types">Типы внешних сущностей</param>
        [NotNull]
        public static External Are([NotNull]ICollection<Type> types)
        {
            return new External(types);
        }
        /// <summary>
        /// Окружение без типов внешних сущностей
        /// </summary>
        [NotNull]
        public static External None { get { return NoExternal; } }
    }

    /// <summary>
    /// Специальное окружение из типов мапперов
    /// </summary>
    public class MapperTypes
    {
        [NotNull]
        private static readonly MapperTypes NoMappers = new MapperTypes(NoTypes.ZeroTypes);

        /// <summary>
        /// Типы мапперов
        /// </summary>
        [NotNull]
        public readonly ICollection<Type> Types;
        private MapperTypes([NotNull]ICollection<Type> types) { Types = types; }
        /// <summary>
        /// Создать специальное окружение из типов мапперов
        /// </summary>
        /// <param name="types">Типы мапперов</param>
        [NotNull]
        public static MapperTypes Are(params Type[] types)
        {
            return new MapperTypes(types ?? None.Types);
        }
        /// <summary>
        /// Создать специальное окружение из типов мапперов
        /// </summary>
        /// <param name="types">Типы мапперов</param>
        [NotNull]
        public static MapperTypes Are([NotNull]ICollection<Type> types)
        {
            return new MapperTypes(types);
        }
        /// <summary>
        /// Специальное окружение без типов мапперов
        /// </summary>
        [NotNull]
        public static MapperTypes None { get { return NoMappers; } }
    }

    /// <summary>
    /// Специальное окружение из объектов мапперов
    /// </summary>
    public class MockObjects
    {
        [NotNull]
        private static readonly MockObjects NoMocks = new MockObjects(new object[0]);

        /// <summary>
        /// Объекты-мапперы
        /// </summary>
        [NotNull]
        public readonly ICollection<object> Objects;
        private MockObjects([NotNull]ICollection<object> objects) { Objects = objects; }
        /// <summary>
        /// Создать специальное окружение из объектов-мапперов
        /// </summary>
        /// <param name="objects">Объекты</param>
        [NotNull]
        public static MockObjects Are(params object[] objects)
        {
            return new MockObjects(objects ?? None.Objects);
        }
        /// <summary>
        /// Создать специальное окружение из объектов-мапперов
        /// </summary>
        /// <param name="objects">Объекты</param>
        [NotNull]
        public static MockObjects Are([NotNull]ICollection<object> objects)
        {
            return new MockObjects(objects);
        }
        /// <summary>
        /// Специальное окружение без объектов-мапперов
        /// </summary>
        [NotNull]
        public static MockObjects None { get { return NoMocks; } }
    }
}
