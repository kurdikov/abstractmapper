﻿namespace TsSoft.AbstractMapper.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.FuncCreators;
    using TsSoft.AbstractMapper.Mappers;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.Expressions.Helpers.Entity;

    /// <summary>
    /// Разрешитель зависимостей мапперов для специального окружения
    /// </summary>
    public class CustomEnvironmentMapperResolver : IMapperResolver
    {
        [NotNull]private readonly IDictionary<Type, KeyValuePair<Type, object>> _cache = new Dictionary<Type, KeyValuePair<Type, object>>();
        [NotNull]private readonly IEntityTypesRetriever _retriever;
        [NotNull]private readonly FactoryInstance _factory;

        /// <summary>
        /// Разрешитель зависимостей мапперов для специального окружения
        /// </summary>
        /// <param name="mapperTypes">Типы мапперов</param>
        /// <param name="mapperObjects">Экземпляры мапперов</param>
        /// <param name="retriever">Хранилище типов сущностей</param>
        public CustomEnvironmentMapperResolver(
            [NotNull]IEnumerable<Type> mapperTypes,
            [NotNull]ICollection<object> mapperObjects,
            [NotNull]IEntityTypesRetriever retriever)
            : this(mapperTypes, mapperObjects, retriever, AbstractMapperDependenciesFactory.Instance)
        {
        }


        /// <summary>
        /// Разрешитель зависимостей мапперов для специального окружения
        /// </summary>
        /// <param name="mapperTypes">Типы мапперов</param>
        /// <param name="mapperObjects">Экземпляры мапперов</param>
        /// <param name="retriever">Хранилище типов сущностей</param>
        /// <param name="factory">Фабрика зависимостей</param>
        public CustomEnvironmentMapperResolver(
            [NotNull]IEnumerable<Type> mapperTypes,
            [NotNull]ICollection<object> mapperObjects,
            [NotNull]IEntityTypesRetriever retriever,
            [NotNull]FactoryInstance factory)
        {
            _retriever = retriever;

            _cache = mapperTypes.Concat(mapperObjects.Where(o => o != null).Select(o => o.GetType())).Distinct()
                .SelectMany(t => t.GetTypeInfo().GetInterfaces()
                    .Where(i => i.GetTypeInfo().IsGenericType && i.GetGenericTypeDefinition() == typeof(IMapper<,>))
                    .Select(i => new { Type = t, Interface = i }))
                .ToDictionary(ti => ti.Interface, ti => new KeyValuePair<Type, object>(
                    ti.Type,
                    mapperObjects.SingleOrDefault(o => o != null && o.GetType() == ti.Type)));
            _factory = factory;
        }

        /// <summary>
        /// Получить экземпляр маппера
        /// </summary>
        /// <typeparam name="TFrom">Тип преобразуемых объектов</typeparam>
        /// <typeparam name="TTo">Тип-результат</typeparam>
        /// <returns>Экземпляр маппера</returns>
        public IMapper<TFrom, TTo> TryGet<TFrom, TTo>()
        {
            KeyValuePair<Type, object> mapper;
            _cache.TryGetValue(typeof(IMapper<TFrom, TTo>), out mapper);
            if (mapper.Value != null)
            {
                return mapper.Value as IMapper<TFrom, TTo>;
            }
            if (mapper.Key != null)
            {
                var constructor = mapper.Key.GetTypeInfo().GetConstructors().Single();
                var parameters = constructor.GetParameters();
                IMapper<TFrom, TTo> mapperObject;
                if (!parameters.Any())
                {
                    mapperObject = Activator.CreateInstance(mapper.Key) as IMapper<TFrom, TTo>;
                }
                else
                {
                    var param = parameters.Single().ParameterType;
                    mapperObject = Activator.CreateInstance(
                        mapper.Key,
                        _factory.GetDependency(param, _retriever, this))
                        as IMapper<TFrom, TTo>;
                }
                _cache[typeof(IMapper<TFrom, TTo>)] = new KeyValuePair<Type, object>(mapper.Key, mapperObject);
                return mapperObject;
            }
            return null;
        }

        /// <summary>
        /// Получить стандартный маппер
        /// </summary>
        /// <typeparam name="TFrom">Тип преобразуемых объектов</typeparam>
        /// <typeparam name="TTo">Тип-результат</typeparam>
        /// <returns>Экземпляр маппера</returns>
        public LooseMapper<TFrom, TTo> GetDefault<TFrom, TTo>()
            where TFrom : class
            where TTo : class, new()
        {
            return new LooseMapper<TFrom, TTo>(
                _factory.GetDependency<IAbstractMapperHelper>(
                    _retriever, this));
        }

        /// <summary>
        /// Получить абстрактный маппер из TFrom в TTo, определяющий типы внутренних сущностей во время выполнения
        /// </summary>
        public DynamicMapper<TFrom, TTo> GetDynamic<TFrom, TTo>()
            where TFrom : class
            where TTo : class, new()
        {
            return new DynamicMapper<TFrom, TTo>(
                _factory.GetDependency<IAbstractMapperHelper>(
                    _retriever, this));
        }

        /// <summary>
        /// Получить абстрактный маппер из объекта в TTo, определяющий тип объекта во время выполнения
        /// </summary>
        public IDynamicEntityToEntityMapper<TTo> GetTypeDeterminingMapper<TTo>() where TTo : class, new()
        {
            return new DynamicEntityToEntityMapper<TTo>(
                _factory.GetDependency<IDynamicEntityMapFuncCache>(
                    _retriever, this));
        }
    }
}
