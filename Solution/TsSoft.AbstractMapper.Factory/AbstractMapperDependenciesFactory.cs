﻿namespace TsSoft.AbstractMapper.Factory
{
    using System;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.BindingsDescription;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Фабрика зависимостей мапперов
    /// </summary>
    public static class AbstractMapperDependenciesFactory
    {
        [NotNull]internal static readonly FactoryInstance Instance;

        static AbstractMapperDependenciesFactory()
        {
            var assemblies = new AssemblyRetriever().GetAssembliesWithReferences(new[]
            {
                typeof(Mapper<,>),
                typeof(IncludeProviderMapper<,>),
                typeof(SelectProviderMapper<,>),
                typeof(UpdatePathProviderMapper<,>),
                typeof(UpdateFuncProviderMapper<,>),
            });
            var assemblyTypesRetriever = new AssemblyTypesRetriever();
            var bindingDescriptions = assemblies
                                     .Where(a => a != null)
                                     .SelectMany(assemblyTypesRetriever.GetTypes)
                                     .Where(t => t != null && !t.GetTypeInfo().IsAbstract && typeof(IBindingsDescription).GetTypeInfo().IsAssignableFrom(t))
                                     .Select(t => Activator.CreateInstance(t) as IBindingsDescription)
                                     .Where(d => d != null)
                                     .ToArray();
            var staticHelperContainers = new[] { typeof(StaticHelpers), typeof(MapperStaticHelpers), typeof(SelectStaticHelpers) };
            Instance = new FactoryInstance(bindingDescriptions, staticHelperContainers);
        }

        /// <summary>
        /// Добавить связи интерфейсов с реализациями зависимостей
        /// </summary>
        /// <param name="description">Описание связей</param>
        public static void AddBindings([NotNull]IBindingsDescription description)
        {
            Instance.AddBindings(description);
        }

        /// <summary>
        /// Удалить связь интерфейса с реализацией
        /// </summary>
        /// <typeparam name="T">Тип интерфейса</typeparam>
        public static void RemoveBinding<T>()
        {
            Instance.RemoveBinding<T>();
        }

        /// <summary>
        /// Добавить статический хелпер
        /// </summary>
        /// <typeparam name="T">Интерфейс хелпера</typeparam>
        /// <param name="helper">Хелпер</param>
        public static void AddStaticHelper<T>(T helper)
        {
            Instance.AddStaticHelper(helper);
        }

        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <typeparam name="TDependency">Тип зависимости</typeparam>
        /// <param name="types">Типы мапперов</param>
        /// <param name="objects">Экземпляры мапперов</param>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public static TDependency GetDependency<TDependency>([NotNull]MapperTypes types, [NotNull]MockObjects objects, [NotNull]Entities entities, [NotNull]External external)
        {
            return Instance.GetDependency<TDependency>(types, objects, entities, external);
        }
        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <typeparam name="TDependency">Тип зависимости</typeparam>
        /// <param name="types">Типы мапперов</param>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public static TDependency GetDependency<TDependency>([NotNull]MapperTypes types, [NotNull]Entities entities, [NotNull]External external)
        {
            return Instance.GetDependency<TDependency>(types, entities, external);
        }
        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <typeparam name="TDependency">Тип зависимости</typeparam>
        /// <param name="objects">Экземпляры мапперов</param>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public static TDependency GetDependency<TDependency>([NotNull]MockObjects objects, [NotNull]Entities entities, [NotNull]External external)
        {
            return Instance.GetDependency<TDependency>(objects, entities, external);
        }

        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <typeparam name="TDependency">Тип зависимости</typeparam>
        /// <param name="retriever">Хранилище типов сущностей</param>
        /// <param name="resolver">Разрешитель зависимостей мапперов</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public static TDependency GetDependency<TDependency>([NotNull]IEntityTypesRetriever retriever, [NotNull]IMapperResolver resolver)
        {
            return Instance.GetDependency<TDependency>(retriever, resolver);
        }

        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <param name="dependencyType">Тип зависимости</param>
        /// <param name="types">Типы мапперов</param>
        /// <param name="objects">Экземпляры мапперов</param>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public static object GetDependency([NotNull]Type dependencyType, [NotNull]MapperTypes types, [NotNull]MockObjects objects, [NotNull]Entities entities, [NotNull]External external)
        {
            return Instance.GetDependency(dependencyType, types, objects, entities, external);
        }
        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <param name="dependencyType">Тип зависимости</param>
        /// <param name="types">Типы мапперов</param>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public static object GetDependency([NotNull]Type dependencyType, [NotNull]MapperTypes types, [NotNull]Entities entities, [NotNull]External external)
        {
            return Instance.GetDependency(dependencyType, types, entities, external);
        }
        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <param name="dependencyType">Тип зависимости</param>
        /// <param name="objects">Экземпляры мапперов</param>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public static object GetDependency([NotNull]Type dependencyType, [NotNull]MockObjects objects, [NotNull]Entities entities, [NotNull]External external)
        {
            return Instance.GetDependency(dependencyType, objects, entities, external);
        }

        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <param name="dependencyType">Тип зависимости</param>
        /// <param name="retriever">Хранилище типов сущностей</param>
        /// <param name="resolver">Разрешитель зависимостей мапперов</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public static object GetDependency([NotNull]Type dependencyType, [NotNull]IEntityTypesRetriever retriever, [NotNull]IMapperResolver resolver)
        {
            return Instance.GetDependency(dependencyType, retriever, resolver);
        }

        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <param name="dependencyType">Тип зависимости</param>
        /// <param name="retriever">Хранилище типов сущностей</param>
        /// <param name="resolver">Разрешитель зависимостей мапперов</param>
        /// <param name="objects">Экземпляры зависимостей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public static object GetDependency([NotNull]Type dependencyType, [NotNull]IEntityTypesRetriever retriever, [NotNull]IMapperResolver resolver, [NotNull]MockObjects objects)
        {
            return Instance.GetDependency(dependencyType, retriever, resolver);
        }
    }
}
