﻿namespace TsSoft.AbstractMapper.Factory
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Извлекает типы сущностей из специального окружения
    /// </summary>
    public class CustomEnvironmentTestEntityTypesRetriever : IEntityTypesRetriever
    {
        [NotNull]private readonly ICollection<Type> _entityTypes;
        [NotNull]private readonly ICollection<Type> _externalTypes;

        /// <summary>
        /// Извлекает типы сущностей из специального окружения
        /// </summary>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        public CustomEnvironmentTestEntityTypesRetriever(Entities entities, External external)
            : this(
                entities != null ? entities.Types : Entities.None.Types,
                external != null ? external.Types : External.None.Types)
        {
        }

        /// <summary>
        /// Извлекает типы сущностей из специального окружения
        /// </summary>
        /// <param name="entityTypes">Типы сущностей</param>
        /// <param name="externalTypes">Типы внешних сущностей</param>
        public CustomEnvironmentTestEntityTypesRetriever(ICollection<Type> entityTypes, ICollection<Type> externalTypes)
        {
            _entityTypes = entityTypes ?? Entities.None.Types;
            _externalTypes = externalTypes ?? External.None.Types;
        }

        /// <summary>
        /// Является ли типом сущности
        /// </summary>
        public bool IsEntityType(Type type)
        {
            return _entityTypes.Contains(type);
        }

        /// <summary>
        /// Является ли типом коллекции сущностей
        /// </summary>
        public bool IsEntityCollectionType(Type type)
        {
            return type.IsGenericEnumerable() && _entityTypes.Contains(type.GetGenericEnumerableArgument());
        }

        /// <summary>
        /// Получить все типы сущностей
        /// </summary>
        public IEnumerable<Type> GetEntityTypes()
        {
            return _entityTypes;
        }

        /// <summary>
        /// Является ли типом внешней сущности
        /// </summary>
        public bool IsExternalEntityType(Type type)
        {
            return _externalTypes.Contains(type);
        }

        /// <summary>
        /// Получить все типы внешних сущностей
        /// </summary>
        public IEnumerable<Type> GetExternalEntityTypes()
        {
            return _externalTypes;
        }
    }
}
