﻿namespace TsSoft.AbstractMapper.Factory
{
    using TsSoft.AbstractMapper.Engine;
    using TsSoft.AbstractMapper.Helpers;
    using TsSoft.AbstractMapper.RuleConverters;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Статические хелперы для фабрики мапперов
    /// </summary>
    public static class MapperStaticHelpers
    {
        /// <summary>
        /// Преобразователь правил игнорирования
        /// </summary>
        public static IIgnoreRulesConverter IgnoreRulesConverter = new IgnoreRulesConverter(StaticHelpers.MemberInfo);

        /// <summary>
        /// Хелпер для построения извлекающих выражений
        /// </summary>
        public static IGetterAccessHelper GetterAccessHelper = new GetterAccessHelper();

        /// <summary>
        /// Хелпер для построения преобразований коллекций
        /// </summary>
        public static ICollectionConvertExpressionCreator CollectionConvertExpressionCreator =
            new CollectionConvertExpressionCreator(StaticHelpers.Library);

        /// <summary>
        /// Хелпер для построения преобразований примитивных свойств
        /// </summary>
        public static IPrimitiveConvertExpressionCreator PrimitiveConvertExpressionCreator =
            new PrimitiveConvertExpressionCreator(StaticHelpers.MemberInfo, StaticHelpers.Library, StaticHelpers.ConversionChecker);
    }
}
