﻿namespace TsSoft.AbstractMapper.Factory
{
    using System;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

    /// <summary>
    /// Фабрика мапперов
    /// </summary>
    public static class AbstractMapperFactory
    {
        /// <summary>
        /// Создать маппер
        /// </summary>
        /// <typeparam name="TMapper">Тип маппера</typeparam>
        /// <param name="mappers">Типы внутренних мапперов</param>
        /// <param name="entityTypes">Типы сущностей</param>
        /// <param name="externalEntityTypes">Типы внешних сущностей</param>
        /// <returns>Экземпляр маппера</returns>
        [NotNull]
        public static TMapper Create<TMapper>(
            MapperTypes mappers,
            Entities entityTypes = null,
            External externalEntityTypes = null)
            where TMapper : class
        {
            mappers = mappers ?? MapperTypes.None;
            entityTypes = entityTypes ?? Entities.None;
            externalEntityTypes = externalEntityTypes ?? External.None;
            var constructor = GetConstructor(typeof(TMapper));
            var constructorParameters = constructor.GetParameters()
                .Select(p => p.ThrowIfNull("p").ParameterType)
                .Select(t => AbstractMapperDependenciesFactory.GetDependency(t.ThrowIfNull("t"), mappers, entityTypes, externalEntityTypes))
                .ToArray(); 
            return (TMapper)Activator.CreateInstance(typeof(TMapper), constructorParameters);
        }

        /// <summary>
        /// Создать маппер
        /// </summary>
        /// <typeparam name="TMapper">Тип маппера</typeparam>
        /// <param name="mocks">Экземпляры внутренних мапперов</param>
        /// <param name="entityTypes">Типы сущностей</param>
        /// <param name="externalEntityTypes">Типы внешних сущностей</param>
        /// <param name="useMocksForHelper">Использовать mocks для выбора </param>
        /// <returns>Экземпляр маппера</returns>
        [NotNull]
        public static TMapper Create<TMapper>(
            MockObjects mocks,
            Entities entityTypes = null,
            External externalEntityTypes = null,
            bool useMocksForHelper = false)
            where TMapper : class
        {
            mocks = mocks ?? MockObjects.None;
            entityTypes = entityTypes ?? Entities.None;
            externalEntityTypes = externalEntityTypes ?? External.None;
            var constructor = GetConstructor(typeof(TMapper));
            var constructorParameters = constructor.GetParameters()
                .Select(p => p.ThrowIfNull("p").ParameterType)
                .Select(t =>
                    typeof(IAbstractMapperHelper).GetTypeInfo().IsAssignableFrom(t) && !useMocksForHelper
                    ? AbstractMapperDependenciesFactory.GetDependency(t.ThrowIfNull("t"), mocks, entityTypes, externalEntityTypes)
                    : mocks.Objects.FirstOrDefault(o => t.ThrowIfNull("t").GetTypeInfo().IsInstanceOfType(o)))
                .ToArray();
            return (TMapper)Activator.CreateInstance(typeof(TMapper), constructorParameters);
        }

        /// <summary>
        /// Создать маппер
        /// </summary>
        /// <typeparam name="TMapper">Тип маппера</typeparam>
        /// <param name="retriever">Хранилище типов сущностей</param>
        /// <param name="resolver">Разрешитель зависимостей мапперов</param>
        /// <returns>Экземпляр маппера</returns>
        [NotNull]
        public static TMapper Create<TMapper>(
            [NotNull]IEntityTypesRetriever retriever,
            [NotNull]IMapperResolver resolver)
            where TMapper: class
        {
            var constructor = GetConstructor(typeof(TMapper));
            var constructorParameters = constructor.GetParameters()
                .Select(p => p.ThrowIfNull("p").ParameterType)
                .Select(t => AbstractMapperDependenciesFactory.GetDependency(t.ThrowIfNull("t"), retriever, resolver))
                .ToArray();
            return (TMapper)Activator.CreateInstance(typeof(TMapper), constructorParameters);
        }

        [NotNull]
        private static ConstructorInfo GetConstructor([NotNull]Type type)
        {
            var constructors = type.GetTypeInfo().GetConstructors();
            if (constructors.Length != 1)
            {
                throw new InvalidOperationException(string.Format("Constructor of {0} has more than one constructor", type));
            }
            var result = constructors[0];
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Unable to find constructor of {0}", type));
            }
            return result;
        }
    }
}
