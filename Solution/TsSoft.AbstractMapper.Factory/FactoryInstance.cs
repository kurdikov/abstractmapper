﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;
using TsSoft.AbstractMapper.Resolvers;
using TsSoft.BindingsDescription;
using TsSoft.Expressions.Helpers.Entity;
using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

namespace TsSoft.AbstractMapper.Factory
{
    /// <summary>
    /// Экземпляр фабрики зависимостей
    /// </summary>
    public class FactoryInstance
    {
        [NotNull]private readonly IDictionary<Type, Type> _bindings = new Dictionary<Type, Type>();
        [NotNull]private readonly IDictionary<Type, object> _staticHelpers = new Dictionary<Type, object>();

        /// <summary>
        /// Создать экземпляр фабрики зависимостей
        /// </summary>
        /// <param name="bindingDescriptions">Описания привязки интерфейсов к реализациям</param>
        /// <param name="staticHelperContainers">Контейнеры статических зависимостей</param>
        public FactoryInstance(
            IBindingsDescription[] bindingDescriptions,
            Type[] staticHelperContainers = null)
        {
            foreach (var bindingDescription in bindingDescriptions)
            {
                if (bindingDescription == null)
                {
                    continue;
                }
                foreach (var binding in bindingDescription.GetBindings() ?? new Dictionary<Type, Type>())
                {
                    _bindings.Add(binding);
                }
            }

            if (staticHelperContainers != null)
            {
                var fields = staticHelperContainers.SelectMany(c => c != null ? c.GetTypeInfo().GetFields(BindingFlags.Static | BindingFlags.Public) : new FieldInfo[0]);
                foreach (var field in fields)
                {
                    if (field == null)
                    {
                        return;
                    }
                    _staticHelpers.Add(field.FieldType, field.GetValue(null));
                }
            }
        }

        /// <summary>
        /// Добавить связи интерфейсов с реализациями зависимостей
        /// </summary>
        /// <param name="description">Описание связей</param>
        public void AddBindings([NotNull]IBindingsDescription description)
        {
            foreach (var binding in description.GetBindings())
            {
                _bindings.Add(binding);
            }
        }

        /// <summary>
        /// Добавить связи интерфейсов с реализациями зависимостей
        /// </summary>
        /// <param name="interface">Тип интерфейса</param>
        /// <param name="implementation">Тип реализации</param>
        public void AddBinding([NotNull]Type @interface, [NotNull]Type implementation)
        {
            _bindings.Add(@interface, implementation);
        }

        /// <summary>
        /// Удалить связь интерфейса с реализацией
        /// </summary>
        /// <typeparam name="T">Тип интерфейса</typeparam>
        public void RemoveBinding<T>()
        {
            _bindings.Remove(typeof(T));
        }

        /// <summary>
        /// Добавить статический хелпер
        /// </summary>
        /// <typeparam name="T">Интерфейс хелпера</typeparam>
        /// <param name="helper">Хелпер</param>
        public void AddStaticHelper<T>(T helper)
        {
            _staticHelpers[typeof(T)] = helper;
        }

        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <typeparam name="TDependency">Тип зависимости</typeparam>
        /// <param name="types">Типы мапперов</param>
        /// <param name="objects">Экземпляры мапперов</param>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public TDependency GetDependency<TDependency>([NotNull]MapperTypes types, [NotNull]MockObjects objects, [NotNull]Entities entities, [NotNull]External external)
        {
            var retriever = new CustomEnvironmentTestEntityTypesRetriever(entities, external);
            return GetDependency<TDependency>(retriever,
                new CustomEnvironmentMapperResolver(types.Types, objects.Objects, retriever, this));
        }
        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <typeparam name="TDependency">Тип зависимости</typeparam>
        /// <param name="types">Типы мапперов</param>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public TDependency GetDependency<TDependency>([NotNull]MapperTypes types, [NotNull]Entities entities, [NotNull]External external)
        {
            var retriever = new CustomEnvironmentTestEntityTypesRetriever(entities, external);
            return GetDependency<TDependency>(retriever,
                new CustomEnvironmentMapperResolver(types.Types, new object[0], retriever, this));
        }
        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <typeparam name="TDependency">Тип зависимости</typeparam>
        /// <param name="objects">Экземпляры мапперов</param>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public TDependency GetDependency<TDependency>([NotNull]MockObjects objects, [NotNull]Entities entities, [NotNull]External external)
        {
            var retriever = new CustomEnvironmentTestEntityTypesRetriever(entities, external);
            return GetDependency<TDependency>(retriever,
                new CustomEnvironmentMapperResolver(Enumerable.Empty<Type>(), objects.Objects, retriever, this));
        }

        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <typeparam name="TDependency">Тип зависимости</typeparam>
        /// <param name="retriever">Хранилище типов сущностей</param>
        /// <param name="resolver">Разрешитель зависимостей мапперов</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public TDependency GetDependency<TDependency>([NotNull]IEntityTypesRetriever retriever, [NotNull]IMapperResolver resolver)
        {
            var contextObjects = new Dictionary<Type, object>
            {
                {typeof(IEntityTypesRetriever), retriever},
                {typeof(IMapperResolver), resolver},
            };
            return (TDependency)InternalGetDependency(typeof(TDependency), contextObjects);
        }

        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <param name="dependencyType">Тип зависимости</param>
        /// <param name="types">Типы мапперов</param>
        /// <param name="objects">Экземпляры мапперов</param>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public object GetDependency([NotNull]Type dependencyType, [NotNull]MapperTypes types, [NotNull]MockObjects objects, [NotNull]Entities entities, [NotNull]External external)
        {
            var retriever = new CustomEnvironmentTestEntityTypesRetriever(entities, external);
            return GetDependency(dependencyType, retriever,
                new CustomEnvironmentMapperResolver(types.Types, objects.Objects, retriever, this));
        }
        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <param name="dependencyType">Тип зависимости</param>
        /// <param name="types">Типы мапперов</param>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public object GetDependency([NotNull]Type dependencyType, [NotNull]MapperTypes types, [NotNull]Entities entities, [NotNull]External external)
        {
            var retriever = new CustomEnvironmentTestEntityTypesRetriever(entities, external);
            return GetDependency(dependencyType, retriever,
                new CustomEnvironmentMapperResolver(types.Types, new object[0], retriever, this));
        }
        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <param name="dependencyType">Тип зависимости</param>
        /// <param name="objects">Экземпляры мапперов</param>
        /// <param name="entities">Типы сущностей</param>
        /// <param name="external">Типы внешних сущностей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public object GetDependency([NotNull]Type dependencyType, [NotNull]MockObjects objects, [NotNull]Entities entities, [NotNull]External external)
        {
            var retriever = new CustomEnvironmentTestEntityTypesRetriever(entities, external);
            return GetDependency(dependencyType, retriever,
                new CustomEnvironmentMapperResolver(Enumerable.Empty<Type>(), objects.Objects, retriever, this));
        }

        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <param name="dependencyType">Тип зависимости</param>
        /// <param name="retriever">Хранилище типов сущностей</param>
        /// <param name="resolver">Разрешитель зависимостей мапперов</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public object GetDependency([NotNull]Type dependencyType, [NotNull]IEntityTypesRetriever retriever, [NotNull]IMapperResolver resolver)
        {
            var contextObjects = new Dictionary<Type, object>
            {
                {typeof(IEntityTypesRetriever), retriever},
                {typeof(IMapperResolver), resolver},
            };
            return InternalGetDependency(dependencyType, contextObjects);
        }

        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <param name="dependencyType">Тип зависимости</param>
        /// <param name="retriever">Хранилище типов сущностей</param>
        /// <param name="resolver">Разрешитель зависимостей мапперов</param>
        /// <param name="objects">Экземпляры зависимостей</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        public object GetDependency([NotNull]Type dependencyType, [NotNull]IEntityTypesRetriever retriever, [NotNull]IMapperResolver resolver, [NotNull]MockObjects objects)
        {
            var contextObjects = new Dictionary<Type, object>
            {
                {typeof(IEntityTypesRetriever), retriever},
                {typeof(IMapperResolver), resolver},
            };
            foreach (var obj in objects.Objects)
            {
                if (obj == null)
                {
                    continue;
                }
                foreach (var inter in obj.GetType().GetTypeInfo().GetInterfaces())
                {
                    if (inter == null)
                    {
                        continue;
                    }
                    contextObjects[inter] = obj;
                }
            }
            return InternalGetDependency(dependencyType, contextObjects);
        }

        /// <summary>
        /// Создать зависимость
        /// </summary>
        /// <param name="dependencyType">Тип зависимости</param>
        /// <param name="contextObjects">Уже созданные зависимости</param>
        /// <returns>Экземпляр зависимости</returns>
        [NotNull]
        private object InternalGetDependency(
            [NotNull]Type dependencyType,
            [NotNull] IDictionary<Type, object> contextObjects)
        {
            object result;
            var alreadyCreated =
                _staticHelpers.TryGetValue(dependencyType, out result) ||
                contextObjects.TryGetValue(dependencyType, out result);
            if (!alreadyCreated)
            {
                var classType = dependencyType.GetTypeInfo().IsInterface ? GetBinding(_bindings, dependencyType) : dependencyType;
                var constructor = GetConstructor(classType);
                var parameters = constructor
                    .GetParameters()
                    .Select(p => p.ThrowIfNull("p").ParameterType)
                    .Select(t => InternalGetDependency(t.ThrowIfNull("t"), contextObjects))
                    .ToArray();
                result = Activator.CreateInstance(classType, parameters);
                contextObjects.Add(dependencyType, result);
            }
            return result.ThrowIfNull(dependencyType.FullName);
        }

        [NotNull]
        private static Type GetBinding([NotNull]IDictionary<Type, Type> bindings, [NotNull]Type dependencyType)
        {
            Type binding;
            if (!bindings.TryGetValue(dependencyType, out binding) || binding == null)
            {
                if (dependencyType.GetTypeInfo().IsGenericType)
                {
                    var dependencyGenericDefinition = dependencyType.GetGenericTypeDefinition();
                    if (dependencyGenericDefinition == null
                        || !bindings.TryGetValue(dependencyGenericDefinition, out binding)
                        || binding == null)
                    {
                        throw new InvalidOperationException(string.Format("No binding for {0} or {1}", dependencyType, dependencyGenericDefinition));
                    }
                    return binding.MakeGenericType(dependencyType.GetTypeInfo().GetGenericArguments());
                }
                throw new InvalidOperationException(string.Format("No binding for {0}", dependencyType));
            }
            return binding;
        }

        [NotNull]
        private static ConstructorInfo GetConstructor([NotNull] Type type)
        {
            var constructors = type.GetTypeInfo().GetConstructors();
            if (constructors.Length != 1)
            {
                throw new InvalidOperationException(string.Format("Constructor of {0} has more than one constructor", type));
            }
            var result = constructors[0];
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Unable to find constructor of {0}", type));
            }
            return result;
        }
    }
}
