﻿namespace TsSoft.Expressions.SelectBuilder.Models
{
    /// <summary>
    /// Поведение построителя селектов при обработке пути, заканчивающегося сущностью или коллекцией сущностей
    /// </summary>
    public enum PathDeadendBehavior
    {
        /// <summary>
        /// Выбрать все столбцы сущности
        /// </summary>
        TakeAllPrimitiveProperties = 0,

        /// <summary>
        /// Выбрать только первичный ключ
        /// </summary>
        TakeOnlyId = 1,

        /// <summary>
        /// Ничего не выбирать
        /// </summary>
        IgnorePath = 2,

        /// <summary>
        /// Выбрасывать исключение
        /// </summary>
        ThrowException = 3,
    }
}
