﻿namespace TsSoft.Expressions.SelectBuilder.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    /// <summary>
    /// Селект-выражение
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    public interface IReadOnlySelectExpression<T>
    {
        /// <summary>
        /// Селект-выражение
        /// </summary>
        Expression<Func<T, object>> Select { get; }

        /// <summary>
        /// Внешние инклюды
        /// </summary>
        IReadOnlyCollection<Expression<Func<T, object>>> ExternalIncludes { get; } 
    }
}
