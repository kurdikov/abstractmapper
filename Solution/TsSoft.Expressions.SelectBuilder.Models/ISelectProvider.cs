﻿namespace TsSoft.Expressions.SelectBuilder.Models
{
    using JetBrains.Annotations;

    /// <summary>
    /// Обработчик сущности, отдающий необходимое ему селект-выражение
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface ISelectProvider<TEntity>
    {
        /// <summary>
        /// Минимальный селект, необходимый для обработки сущности
        /// </summary>
        [NotNull]
        IReadOnlySelectExpression<TEntity> Select { get; }
    }
}
