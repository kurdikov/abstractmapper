namespace TsSoft.Expressions.SelectBuilder.Models
{
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// ��������� ���������� ��������, ���������� �������� ���� � �������� ���������� ����������
    /// </summary>
    /// <typeparam name="TFrom">��� ��������</typeparam>
    public interface ICyclePreventingSelectGenerator<TFrom> : ICyclePreventingGenerator, ISelectProvider<TFrom>
    {
        /// <summary>
        /// ������������� ������ � �������� ���������
        /// </summary>
        SelectExpression<TFrom> GenerateSelect(GeneratorContext context);
    }
}
