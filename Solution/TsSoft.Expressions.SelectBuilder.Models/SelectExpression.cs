﻿namespace TsSoft.Expressions.SelectBuilder.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Селект-выражение
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    public class SelectExpression<T> : IReadOnlySelectExpression<T>
    {
        /// <summary>
        /// Селект-выражение для передачи в репозиторий
        /// </summary>
        public Expression<Func<T, object>> Select { get; set; }

        /// <summary>
        /// Внешние инклюды
        /// </summary>
        [NotNull]
        public List<Expression<Func<T, object>>> ExternalIncludes { get; set; }

        /// <summary>
        /// Селект-выражение
        /// </summary>
        public SelectExpression()
        {
            ExternalIncludes = new List<Expression<Func<T, object>>>();
        }

        /// <summary>
        /// Внешние инклюды
        /// </summary>
        IReadOnlyCollection<Expression<Func<T, object>>> IReadOnlySelectExpression<T>.ExternalIncludes
        {
            get { return ExternalIncludes; }
        }
    }
}
