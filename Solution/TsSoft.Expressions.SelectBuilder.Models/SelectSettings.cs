﻿namespace TsSoft.Expressions.SelectBuilder.Models
{
    using System.Collections.Generic;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Настройки для генерации селекта 
    /// </summary>
    public class SelectSettings
    {
        /// <summary>
        /// Не делать проверки навигации на null, если внешний ключ обязателен
        /// </summary>
        public bool SkipNullChecks { get; set; }

        /// <summary>
        /// Множество навигационных свойств, на которые всегда нужно вешать проверки на null
        /// <remarks>В качестве IEqualityComparer следует использовать MemberInfoEqualityComparer.Instance</remarks>
        /// </summary>
        public ISet<ValueHoldingMember> AlwaysCheckNull { get; set; }
    }
}
