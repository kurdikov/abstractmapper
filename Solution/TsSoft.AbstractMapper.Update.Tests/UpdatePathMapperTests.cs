﻿namespace TsSoft.AbstractMapper.Update.Tests
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Models.AbstractMapper;

    [TestClass]
    public class UpdatePathMapperTests
    {
        [TestMethod]
        public void TestNestedMapperWithZeroNesting()
        {
            var mapper = AbstractMapperFactory.Create<NestedUpdatePathMapper>(
                MapperTypes.Are(typeof(NestedUpdatePathMapper)), Entities.Are(typeof(From)));
            var actual = mapper.Paths.ToList();
            Assert.AreEqual(1, actual.Count);
            Assert.IsTrue(actual.Contains(f => f.Id, IncludeEqualityComparer<SimpleNestedTo>.Instance));
        }

        [TestMethod]
        public void TestNestedMapperWithOneNestingLevel()
        {
            var mapper = AbstractMapperFactory.Create<NestedUpdatePathMapper2>(
                MapperTypes.Are(typeof(NestedUpdatePathMapper2)), Entities.Are(typeof(From)));
            var actual = mapper.Paths.ToList();
            Assert.AreEqual(3, actual.Count);
            Assert.IsTrue(actual.Contains(f => f.Id, IncludeEqualityComparer<SimpleNestedTo>.Instance));
            Assert.IsTrue(actual.Contains(f => f.Parent.Id, IncludeEqualityComparer<SimpleNestedTo>.Instance));
            Assert.IsTrue(
                actual.Contains(f => f.Children.Select(c => c.Id), IncludeEqualityComparer<SimpleNestedTo>.Instance));
        }

        [TestMethod]
        public void TestNestedMapperWithTwoNestingLevels()
        {
            var mapper = AbstractMapperFactory.Create<NestedUpdatePathMapper3>(
                MapperTypes.Are(typeof(NestedUpdatePathMapper3)), Entities.Are(typeof(From)));
            var actual = mapper.Paths.ToList();
            Assert.AreEqual(7, actual.Count);
            Assert.IsTrue(actual.Contains(f => f.Id, IncludeEqualityComparer<SimpleNestedTo>.Instance));
            Assert.IsTrue(actual.Contains(f => f.Parent.Id, IncludeEqualityComparer<SimpleNestedTo>.Instance));
            Assert.IsTrue(actual.Contains(f => f.Children.Select(c => c.Id), IncludeEqualityComparer<SimpleNestedTo>.Instance));
            Assert.IsTrue(actual.Contains(f => f.Parent.Parent.Id, IncludeEqualityComparer<SimpleNestedTo>.Instance));
            Assert.IsTrue(actual.Contains(f => f.Parent.Children.Select(c => c.Id), IncludeEqualityComparer<SimpleNestedTo>.Instance));
            Assert.IsTrue(actual.Contains(f => f.Children.Select(c => c.Parent.Id), IncludeEqualityComparer<SimpleNestedTo>.Instance));
            Assert.IsTrue(actual.Contains(f => f.Children.Select(c => c.Children.Select(cc => cc.Id)), IncludeEqualityComparer<SimpleNestedTo>.Instance));
        }

        [TestMethod]
        public void TestBuildTimeResolutionMapper()
        {
            var entityTypesRetriever = new CustomEnvironmentTestEntityTypesRetriever(Entities.Are(typeof(From)), External.None);
            var mapperResolver = new Mock<IMapperResolver>(MockBehavior.Strict);
            var mockMapper = new Mock<IUpdatePathProviderMapper<From, SimpleNestedTo>>(MockBehavior.Strict);
            mockMapper.Setup(m => m.Paths).Returns(Enumerable.Empty<Expression<Func<SimpleNestedTo, object>>>());
            mapperResolver.Setup(r => r.TryGet<From, SimpleNestedTo>()).Returns(mockMapper.Object).Verifiable();
            var mapper = AbstractMapperFactory.Create<NoMapTimeInjectionMapper>(entityTypesRetriever, mapperResolver.Object);

            var res = mapper.GenerateUpdatePaths(new GeneratorContext(mapper.GetType(), 0));    // хак для того, чтобы перегенерация состоялась
            Assert.IsNotNull(res);

            res = mapper.GenerateUpdatePaths(new GeneratorContext(mapper.GetType(), 0));
            Assert.IsNotNull(res);

            mapperResolver.Verify(r => r.TryGet<From, SimpleNestedTo>(), Times.Exactly(2));
        }

        [TestMethod]
        public void TestMapTimeResolutionMapper()
        {
            var entityTypesRetriever = new CustomEnvironmentTestEntityTypesRetriever(Entities.Are(typeof(From)), External.None);
            var mapperResolver = new Mock<IMapperResolver>(MockBehavior.Strict);
            var mockMapper = new Mock<IUpdatePathProviderMapper<From, SimpleNestedTo>>(MockBehavior.Strict);
            mockMapper.Setup(m => m.Paths).Returns(Enumerable.Empty<Expression<Func<SimpleNestedTo, object>>>());
            mapperResolver.Setup(r => r.TryGet<From, SimpleNestedTo>()).Returns(mockMapper.Object).Verifiable();
            var mapper = AbstractMapperFactory.Create<MapTimeInjectionMapper>(entityTypesRetriever, mapperResolver.Object);

            var res = mapper.GenerateUpdatePaths(new GeneratorContext(mapper.GetType(), 0));    // хак для того, чтобы перегенерация состоялась
            Assert.IsNotNull(res);

            res = mapper.GenerateUpdatePaths(new GeneratorContext(mapper.GetType(), 0));
            Assert.IsNotNull(res);

            mapperResolver.Verify(r => r.TryGet<From, SimpleNestedTo>(), Times.Exactly(4));
        }


        private class UpdateMapperWithMapToCollectionRule : UpdatePathProviderMapper<SimpleTo, From>
        {
            public UpdateMapperWithMapToCollectionRule([NotNull] IAbstractUpdatePathProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IMapRules<From, SimpleTo> MapRules
            {
                get
                {
                    return new MapRules<From, SimpleTo>
                    {
                        {f => f.Children, t => t.Id}
                    };
                }
            }

            public class IntToFroms : IMapper<int, ICollection<From>>, IUpdatePathProvider<From>
            {
                public ICollection<From> Map(int @from)
                {
                    return new From[] {new From {Id = 678}};
                }

                public IEnumerable<Expression<Func<From, object>>> Paths
                {
                    get
                    {
                        return new Expression<Func<From, object>>[]
                        {
                            f => f.Id,
                            f => f.Name,
                        };
                    }
                }

                public IEnumerable<Expression<Func<From, object>>> DeleteAndCreateOnPaths
                {
                    get
                    {
                        return new Expression<Func<From, object>>[]
                        {
                            f => f.Children
                        };
                    }
                }
            }
        }

        [TestMethod]
        public void TestUpdateMapperWithMapToCollectionRule()
        {
            var mapper = AbstractMapperFactory.Create<UpdateMapperWithMapToCollectionRule>(
                MapperTypes.Are(typeof(UpdateMapperWithMapToCollectionRule.IntToFroms)), Entities.Are(typeof(From)));
            var paths = mapper.Paths.ToList();
            IncludeAssert.Contains(f => f.Children.Select(c => c.Id), paths);
            IncludeAssert.Contains(f => f.Children.Select(c => c.Name), paths);
            Assert.AreEqual(2, paths.Count);
            //var dropCreate = mapper.DeleteAndCreateOnPaths.ToList();
            //IncludeAssert.Contains(f => f.Children.Select(c => c.Children), dropCreate);
            //Assert.AreEqual(1, dropCreate.Count);
        }


        public class NestedUpdatePathMapper : UpdatePathProviderMapper<From, SimpleNestedTo>
        {
            public NestedUpdatePathMapper([NotNull] IAbstractUpdatePathProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            public override int MaxNestingLevel
            {
                get { return 0; }
            }
        }

        public class NestedUpdatePathMapper2 : NestedUpdatePathMapper
        {
            public NestedUpdatePathMapper2([NotNull] IAbstractUpdatePathProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            public override int MaxNestingLevel
            {
                get { return 1; }
            }
        }

        public class NestedUpdatePathMapper3 : NestedUpdatePathMapper
        {
            public NestedUpdatePathMapper3([NotNull] IAbstractUpdatePathProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            public override int MaxNestingLevel
            {
                get { return 2; }
            }
        }

        public class NoMapTimeInjectionMapper : UpdatePathProviderMapper<From, SimpleNestedTo>
        {
            public NoMapTimeInjectionMapper([NotNull] IAbstractUpdatePathProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override InnerMapperStrategy InnerMapperStrategy
            {
                get { return InnerMapperStrategy.StaticTypes | InnerMapperStrategy.ResolveOnMapperBuilding; }
            }
        }

        public class MapTimeInjectionMapper : UpdatePathProviderMapper<From, SimpleNestedTo>
        {
            public MapTimeInjectionMapper([NotNull] IAbstractUpdatePathProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override InnerMapperStrategy InnerMapperStrategy
            {
                get { return InnerMapperStrategy.StaticTypes | InnerMapperStrategy.ResolveOnMapping; }
            }
        }
    }
}
