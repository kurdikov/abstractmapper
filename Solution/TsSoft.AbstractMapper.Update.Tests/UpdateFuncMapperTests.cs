﻿namespace TsSoft.AbstractMapper.Update.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Reflection;

    [TestClass]
    public class UpdateFuncMapperTests
    {
        [TestMethod]
        public void TestPrimitiveUpdater()
        {
            var mapper = AbstractMapperFactory.Create<TestUpdateMapper>(MapperTypes.None);

            var source = new TestM {Id = 1, Value = 2, String = "3", Nullable = 4};
            var target = new TestM {Id = 2, Value = 5, String = "6", Nullable = null};

            mapper.Updater(source, target);

            Assert.AreEqual(source.Value, target.Value);
            Assert.AreEqual(source.String, target.String);
            Assert.AreEqual(source.Nullable, target.Nullable);
            Assert.AreEqual(2, target.Id);
        }


        public class TestVm
        {
            public int Value { get; set; }

            public string String { get; set; }

            public int? Nullable { get; set; }
        }

        public class TestM
        {
            public int Id { get; set; }

            public int Value { get; set; }

            public string String { get; set; }

            public int? Nullable { get; set; }
        }

        public class TestUpdateMapper : UpdateFuncProviderMapper<TestVm, TestM>
        {
            [NotNull]
            private readonly TestObjectUpdateManager _mgr;

            public TestUpdateMapper([NotNull] IAbstractUpdateFuncProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
                _mgr = new TestObjectUpdateManager();
            }

            protected override IObjectUpdateManager UpdateManager
            {
                get { return _mgr; }
            }
        }

        public class TestObjectUpdateManager : IObjectUpdateManager
        {
            private readonly List<string> _removedProps = new List<string>();
            private readonly List<string> _partlyRemovedProps = new List<string>();

            public bool IsPrimitive(Type type)
            {
                return type.GetTypeInfo().IsValueType || type == typeof(string);
            }

            public bool IsObject(Type type)
            {
                return type.GetTypeInfo().IsClass && type != typeof(string);
            }

            public bool IsCollection(Type type)
            {
                return type.IsGenericEnumerable();
            }

            public Expression MakeAsyncRemoveCollectionExpression(ValueHoldingMember collectionProperty, Type objectType, Expression target)
            {
                throw new NotImplementedException();
            }

            public void Remove(PropertyInfo property)
            {
                _removedProps.Add(property.Name);
            }
            public void RemovePart()
            {
                _partlyRemovedProps.Add("");
            }

            public Expression MakeRemoveCollectionExpression(ValueHoldingMember collectionProperty, Type objectType, Expression target)
            {
                return Expression.Call(Expression.Constant(this), "Remove", new[] {typeof(PropertyInfo)}, Expression.Constant(collectionProperty));
            }

            public Expression MakeRemoveCollectionPartExpression(Expression source, Expression target)
            {
                return Expression.Call(Expression.Constant(this), "RemovePart", new Type[0]);
            }

            public Expression MakeAsyncRemoveCollectionPartExpression(Expression source, Expression target)
            {
                throw new NotImplementedException();
            }

            public Expression MakeExtractKeyExpression(Expression obj)
            {
                return Expression.Property(obj, "Id");
            }
        }
    }
}
