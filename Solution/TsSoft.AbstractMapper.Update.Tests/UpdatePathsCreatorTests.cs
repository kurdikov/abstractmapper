﻿namespace TsSoft.AbstractMapper.Update.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Engine;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.AbstractMapper;

    [TestClass()]
    public class UpdatePathsCreatorTests
    {
        private ValueHoldingMember T1Prop(string name)
        {
            return new ValueHoldingMember(typeof (T1).GetProperty(name));
        }
        private ValueHoldingMember T2Prop(string name)
        {
            return new ValueHoldingMember(typeof (T2).GetProperty(name));
        }

        [TestMethod]
        public void CreateTest()
        {
            var mockResolver = new Mock<IMapperResolver>(MockBehavior.Strict);
            var mockUpdateMapper = new Mock<IUpdatePathProviderMapper<T2, T2>>(MockBehavior.Strict);
            mockResolver.Setup(mr => mr.TryGet<T2, T2>()).Returns(mockUpdateMapper.Object);
            mockUpdateMapper.Setup(mum => mum.Paths).Returns(new Expression<Func<T2, object>>[]
                {
                    t => t.Prop1,
                    t => t.Prop2,
                });
            var mockTypeRetriever = new Mock<IEntityTypesRetriever>(MockBehavior.Strict);
            mockTypeRetriever.Setup(r => r.IsEntityType(It.IsAny<Type>())).Returns(false);
            var mockTypeHelper = new Mock<IEntityTypesHelper>(MockBehavior.Strict);
            mockTypeHelper.Setup(th => th.GetPrimaryKey(typeof (T1))).Returns(new List<ValueHoldingMember>());
            var helper = new UpdatePathsCreator(
                StaticHelpers.ExpressionBuilder, 
                mockResolver.Object,
                StaticHelpers.FlatPathParser,
                StaticHelpers.MemberInfo,
                mockTypeHelper.Object,
                StaticHelpers.NestedGeneratorHelper,
                mockTypeRetriever.Object);

            var mappedPaths = new MappedPathDescription[]
                {
                    new MappedPathDescription {ToPath = T1Prop("Prop1")},
                    new MappedPathDescription {ToPath = T1Prop("Prop2")},
                    new MappedPathDescription {ToPath = T1Prop("Parent"), MapperDescription = new MapperDescription(mapperFromType: typeof(T2), mapperToType: typeof(T2), mapperFromSingleType: typeof(T2))},
                    new MappedPathDescription {ToPath = T1Prop("Children"), MapperDescription = new MapperDescription(mapperFromType: typeof(T2), mapperToType: typeof(T2), mapperFromSingleType: typeof(T2))},
                };
            var result = helper.Create<T1>(mappedPaths).ToList();
            Assert.AreEqual(6, result.Count);
            Assert.IsTrue(result.Contains(x => x.Prop1, IncludeEqualityComparer<T1>.Instance));
            Assert.IsTrue(result.Contains(x => x.Prop2, IncludeEqualityComparer<T1>.Instance));
            Assert.IsTrue(result.Contains(x => x.Parent.Prop1, IncludeEqualityComparer<T1>.Instance));
            Assert.IsTrue(result.Contains(x => x.Parent.Prop2, IncludeEqualityComparer<T1>.Instance));
            Assert.IsTrue(result.Contains(x => x.Children.Select(c => c.Prop1), IncludeEqualityComparer<T1>.Instance));
            Assert.IsTrue(result.Contains(x => x.Children.Select(c => c.Prop2), IncludeEqualityComparer<T1>.Instance));
        }

        public class T1
        {
            public int Prop1 { get; set; }

            public long Prop2 { get; set; }

            public T2 Parent { get; set; }

            public IEnumerable<T2> Children { get; set; }
        }

        public class T2
        {
            public int Prop1 { get; set; }

            public long Prop2 { get; set; }
        }
    }
}
