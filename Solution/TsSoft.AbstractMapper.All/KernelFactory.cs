﻿namespace TsSoft.AbstractMapper.All
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Ninject;
    using Ninject.Activation;
    using Ninject.Modules;
    using TsSoft.AbstractMapper.NinjectResolver;
    using TsSoft.Bindings;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.NinjectBindings;

    /// <summary>
    /// Фабрика Ninject-ядер
    /// </summary>
    public static class KernelFactory
    {
        private enum RequestType
        {
            Default,
            FromEntity,
            FromEntityCollection,
            ToEntity,
        }

        private static RequestType GetRequestType(IRequest request, IEntityTypesRetriever retriever)
        {
            if (request == null || request.Service == null)
            {
                return RequestType.Default;
            }
            if (retriever == null)
            {
                return RequestType.Default;
            }
            var args = request.Service.GetGenericArguments();
            var fromType = args[0];
            var toType = args[1];
            var result = RequestType.Default;
            if (retriever.IsEntityType(fromType))
            {
                result = RequestType.FromEntity;
            }
            else if (retriever.IsEntityCollectionType(fromType))
            {
                result = RequestType.FromEntityCollection;
            }
            else if (retriever.IsEntityType(toType))
            {
                result = RequestType.ToEntity;
            }
            return result;
        }

        internal static IKernel CreateKernel(
            IAssemblyTypesRetriever retriever,
            IEnumerable<Type> libraryTypes,
            INinjectModule[] modules,
            bool bindDefaultMappers,
            IEntityTypesRetriever entityRetriever)
        {
            modules = modules ?? new INinjectModule[0];
            var kernel = new StandardKernel(modules);
            NinjectBinder.BindAllInSingletonScope(kernel, new[]
            {
                typeof(AbstractMapperBindings),
                typeof(AbstractMapperIncludeBindings),
                typeof(AbstractMapperNinjectResolverBindings),
                typeof(AbstractMapperSelectBindings),
                typeof(AbstractMapperUpdateBindings),
                typeof(EntityRepositoryBindings),
                typeof(EntityServiceBindings),
                typeof(ExpressionsHelpersBindings),
                typeof(ExpressionsHelpersEntityBindings),
                typeof(ExpressionsIncludeBuilderBindings),
                typeof(ExpressionsSelectBuilderBindings),
            }.Concat(libraryTypes ?? Enumerable.Empty<Type>()));
            NinjectMapperBinder.BindAllMappersFromAssembliesWithModulesInSingletonScope(
                kernel,
                modules,
                new[]
                {
                    typeof(IMapper<,>), 
                    typeof(IIncludeProviderMapper<,>),
                    typeof(IIncludeProviderCollectionMapper<,>),
                    typeof(ISelectProviderMapper<,>),
                    typeof(ISelectProviderCollectionMapper<,>),
                    typeof(IUpdatePathProviderMapper<,>)
                },
                retriever);
            if (bindDefaultMappers)
            {
                // ReSharper disable PossibleNullReferenceException
                kernel.Bind(typeof(IMapper<,>)).To(typeof(Mapper<,>))
                    .When(request => GetRequestType(request, entityRetriever) == RequestType.Default)
                    .InSingletonScope();
                kernel.Bind(typeof(IMapper<,>)).To(typeof(SelectProviderMapper<,>))
                    .When(request => GetRequestType(request, entityRetriever) == RequestType.FromEntity)
                    .InSingletonScope();
                // ReSharper restore PossibleNullReferenceException
            }
            return kernel;
        }

        /// <summary>
        /// Создать ninject-ядро, загрузить в него заданные модули, связывания для зависимостей абстрактных мапперов, мапперы из сборок с заданными модулями
        /// </summary>
        /// <param name="modules">Загружаемые модули</param>
        public static IKernel CreateKernel(params INinjectModule[] modules)
        {
            return CreateKernel(null, null, modules, false, null);
        }

        /// <summary>
        /// Создать ninject-ядро, загрузить в него заданные модули, связывания для зависимостей абстрактных мапперов, связывания из сборок с заданными типами, мапперы из сборок с заданными модулями
        /// </summary>
        /// <param name="libraryTypes">Типы, связывания из сборок с которыми нужно загрузить</param>
        /// <param name="modules">Загружаемые модули</param>
        public static IKernel CreateKernel(IEnumerable<Type> libraryTypes, params INinjectModule[] modules)
        {
            return CreateKernel(null, libraryTypes, modules, false, null);
        }

        /// <summary>
        /// Создать ninject-ядро, загрузить в него заданные модули, связывания для мапперов по умолчанию и их зависимостей, связывания из сборок с заданными типами, мапперы из сборок с заданными модулями
        /// </summary>
        /// <param name="retriever">Анализатор типов сущностей</param>
        /// <param name="modules">Загружаемые модули</param>
        public static IKernel CreateKernelWithDefaultMapperBinding(
            IEntityTypesRetriever retriever,
            params INinjectModule[] modules)
        {
            return CreateKernel(null, null, modules, true, retriever);
        }

        /// <summary>
        /// Создать ninject-ядро, загрузить в него заданные модули, связывания для мапперов по умолчанию и их зависимостей, связывания из сборок с заданными типами, мапперы из сборок с заданными модулями
        /// </summary>
        /// <param name="retriever">Анализатор типов сущностей</param>
        /// <param name="libraryTypes">Типы, связывания из сборок с которыми нужно загрузить</param>
        /// <param name="modules">Загружаемые модули</param>
        public static IKernel CreateKernelWithDefaultMapperBinding(
            IEntityTypesRetriever retriever,
            IEnumerable<Type> libraryTypes,
            params INinjectModule[] modules)
        {
            return CreateKernel(null, null, modules, true, retriever);
        }
    }
}
