﻿namespace TsSoft.AbstractMapper
{
    /// <summary>
    /// Маппер, предоставляющий делегат для обновления объекта
    /// </summary>
    /// <typeparam name="TFrom">Тип объекта, из которого осуществляется преобразование</typeparam>
    /// <typeparam name="TTo">Тип объекта, в который осуществляется преобразование</typeparam>
    public interface IUpdateFuncProviderMapper<in TFrom, TTo> : IUpdateFuncProvider<TTo>, IAsyncUpdateFuncProvider<TTo>, IMapper<TFrom, TTo>
    {
    }
}
