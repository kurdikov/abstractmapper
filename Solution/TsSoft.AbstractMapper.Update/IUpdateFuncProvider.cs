﻿namespace TsSoft.AbstractMapper
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// Предоставляет делегат для обновления объекта
    /// </summary>
    /// <typeparam name="TTo">Тип объекта</typeparam>
    public interface IUpdateFuncProvider<in TTo>
    {
        /// <summary>
        /// Делегат для обновления объекта
        /// </summary>
        Action<TTo, TTo> Updater { get; }
    }

    /// <summary>
    /// Предоставляет асинхронный делегат для обновления объекта
    /// </summary>
    /// <typeparam name="TTo">Тип объекта</typeparam>
    public interface IAsyncUpdateFuncProvider<in TTo>
    {
        /// <summary>
        /// Делегат для обновления объекта, возвращающий набор задач, возникающих при обновлении
        /// </summary>
        Func<TTo, TTo, IEnumerable<Func<Task>>> AsyncUpdater { get; }
    }
}
