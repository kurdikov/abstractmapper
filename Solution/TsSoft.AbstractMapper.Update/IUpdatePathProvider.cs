﻿namespace TsSoft.AbstractMapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Обработчик, отдающий изменяемые им пути
    /// </summary>
    /// <typeparam name="TTo">Тип, являющийся результатом обработки</typeparam>
    public interface IUpdatePathProvider<TTo>
    {
        /// <summary>
        /// Изменяемые пути
        /// </summary>
        [NotNull]
        IEnumerable<Expression<Func<TTo, object>>> Paths { get; }

        /// <summary>
        /// На каких путях пересоздавать сущности после удаления вместо слияния
        /// </summary>
        [NotNull]
        IEnumerable<Expression<Func<TTo, object>>> DeleteAndCreateOnPaths { get; }
    }
}