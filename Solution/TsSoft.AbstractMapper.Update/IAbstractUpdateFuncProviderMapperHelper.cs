﻿namespace TsSoft.AbstractMapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Reflection;

    /// <summary>
    /// Хелпер для абстрактного маппера, отдающего делегат обновления
    /// </summary>
    public interface IAbstractUpdateFuncProviderMapperHelper : IAbstractUpdatePathProviderMapperHelper
    {
        /// <summary>
        /// Получить делегат обновления по описанию маппинга
        /// </summary>
        /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
        /// <param name="paths">Описание маппинга</param>
        /// <param name="updateManager"></param>
        /// <param name="dropCreateOnPaths">На каких путях удалять и создавать элементы вложенных коллекций вместо обновления</param>
        /// <returns>Делегат обновления</returns>
        Action<TTo, TTo> GetUpdateFunc<TTo>(
            [NotNull]IEnumerable<Expression<Func<TTo, object>>> paths,
            [NotNull]IObjectUpdateManager updateManager,
            IEnumerable<Expression<Func<TTo, object>>> dropCreateOnPaths);

        /// <summary>
        /// Получить асинхронный делегат обновления по описанию маппинга
        /// </summary>
        /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
        /// <param name="paths">Описание маппинга</param>
        /// <param name="updateManager"></param>
        /// <param name="dropCreateOnPaths">На каких путях удалять и создавать элементы вложенных коллекций вместо обновления</param>
        /// <returns>Делегат обновления</returns>
        Func<TTo, TTo, IEnumerable<Func<Task>>> GetAsyncUpdateFunc<TTo>(
            [NotNull]IEnumerable<Expression<Func<TTo, object>>> paths,
            [NotNull]IObjectUpdateManager updateManager,
            IEnumerable<Expression<Func<TTo, object>>> dropCreateOnPaths);
    }
}
