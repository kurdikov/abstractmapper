﻿namespace TsSoft.AbstractMapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Engine;
    using TsSoft.AbstractMapper.Rules.Factories;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Closures;
    using TsSoft.Expressions.Models.Reflection;

    /// <summary>
    /// Хелпер для абстрактного маппера, отдающего делегат обновления
    /// </summary>
    public class AbstractUpdateFuncProviderMapperHelper : AbstractUpdatePathProviderMapperHelper, IAbstractUpdateFuncProviderMapperHelper
    {
        [NotNull] private readonly IUpdateObjectActionFactory _updateObjectActionFactory;

        /// <summary>
        /// Хелпер для абстрактного маппера, отдающего делегат обновления
        /// </summary>
        public AbstractUpdateFuncProviderMapperHelper(
            [NotNull] IMapExpressionCreator expressionCreator,
            [NotNull] IIgnoreRulesFactory ignoreRulesFactory,
            [NotNull] ILambdaCompiler lambdaCompiler,
            [NotNull] IFullMapRulesCreator fullMapRulesCreator,
            [NotNull] IUpdatePathsCreator updateCreator,
            [NotNull] IUpdateObjectActionFactory updateObjectActionFactory)
            : base(expressionCreator, ignoreRulesFactory, lambdaCompiler, fullMapRulesCreator, updateCreator)
        {
            if (updateObjectActionFactory == null) throw new ArgumentNullException("updateObjectActionFactory");
            _updateObjectActionFactory = updateObjectActionFactory;
        }

        /// <summary>
        /// Получить делегат обновления по описанию маппинга
        /// </summary>
        /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
        /// <param name="paths">Описание маппинга</param>
        /// <param name="updateManager"></param>
        /// <param name="dropCreateOnPaths">На каких путях удалять и создавать элементы вложенных коллекций вместо обновления</param>
        /// <returns>Делегат обновления</returns>
        public Action<TTo, TTo> GetUpdateFunc<TTo>(
            IEnumerable<Expression<Func<TTo, object>>> paths,
            IObjectUpdateManager updateManager,
            IEnumerable<Expression<Func<TTo, object>>> dropCreateOnPaths)
        {
            return _updateObjectActionFactory.MakeUpdateAction(paths, updateManager, dropCreateOnPaths);
        }

        /// <summary>
        /// Получить асинхронный делегат обновления по описанию маппинга
        /// </summary>
        /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
        /// <param name="paths">Описание маппинга</param>
        /// <param name="updateManager"></param>
        /// <param name="dropCreateOnPaths">На каких путях удалять и создавать элементы вложенных коллекций вместо обновления</param>
        /// <returns>Делегат обновления</returns>
        public Func<TTo, TTo, IEnumerable<Func<Task>>> GetAsyncUpdateFunc<TTo>(
            IEnumerable<Expression<Func<TTo, object>>> paths,
            IObjectUpdateManager updateManager,
            IEnumerable<Expression<Func<TTo, object>>> dropCreateOnPaths)
        {
            return _updateObjectActionFactory.MakeAsyncUpdateAction(paths, updateManager, dropCreateOnPaths);
        }
    }
}
