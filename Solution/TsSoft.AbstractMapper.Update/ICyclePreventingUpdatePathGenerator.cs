﻿namespace TsSoft.AbstractMapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Интерфейс генератора обновляемых путей, способного включить себя в качестве зависимого генератора
    /// </summary>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    public interface ICyclePreventingUpdatePathGenerator<TTo> : ICyclePreventingGenerator, IUpdatePathProvider<TTo>
    {
        /// <summary>
        /// Сгенерировать обновляемые пути в заданном контексте
        /// </summary>
        IEnumerable<Expression<Func<TTo, object>>> GenerateUpdatePaths(GeneratorContext context);
    }
}
