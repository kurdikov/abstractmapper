﻿namespace TsSoft.AbstractMapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Хелпер для абстрактного маппера, отдающего пути обновления
    /// </summary>
    public interface IAbstractUpdatePathProviderMapperHelper : IAbstractMapperHelper
    {
        /// <summary>
        /// Получить пути обновления по описанию маппинга
        /// </summary>
        /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
        /// <param name="paths">Описание маппинга</param>
        /// <param name="excludePrimaryKey">Не обновлять первичный ключ</param>
        /// <param name="context">Контекст генерации</param>
        /// <returns>Пути обновления</returns>
        [NotNull]
        ICollection<Expression<Func<TTo, object>>> GetUpdatePaths<TTo>(IEnumerable<MappedPathDescription> paths, bool excludePrimaryKey, GeneratorContext context);
    }
}
