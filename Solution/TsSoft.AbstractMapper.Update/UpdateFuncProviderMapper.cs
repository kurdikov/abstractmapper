﻿namespace TsSoft.AbstractMapper
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Models.Reflection;

    /// <summary>
    /// Абстрактный маппер, отдающий делегат для обновления
    /// </summary>
    /// <typeparam name="TFrom">Тип, из которого происходит преобразование</typeparam>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    public abstract class UpdateFuncProviderMapper<TFrom, TTo> : UpdatePathProviderMapper<TFrom, TTo>, IUpdateFuncProviderMapper<TFrom, TTo> 
        where TFrom : class 
        where TTo : class, new()
    {
        [NotNull]
        private readonly Lazy<Action<TTo, TTo>> _updater;
        [NotNull]
        private readonly Lazy<Func<TTo, TTo, IEnumerable<Func<Task>>>> _asyncUpdater;

        /// <summary>
        /// Абстрактный маппер, отдающий делегат для обновления
        /// </summary>
        protected UpdateFuncProviderMapper([NotNull] IAbstractUpdateFuncProviderMapperHelper mapperHelper)
            : base(mapperHelper)
        {
            _updater = new Lazy<Action<TTo, TTo>>(() =>
            {
                try
                {
                    return mapperHelper.GetUpdateFunc(Paths, UpdateManager, DeleteAndCreateOnPaths);
                }
                catch (UnableToBuildActionException ex)
                {
                    throw new InvalidOperationException(
                        string.Format(
                            "Unable to build an update delegate for {0}: unknown type {1}. Check that your IObjectUpdateManager implementation correctly handles {1} (by default it delegates to IEntityTypesRetriever and its IsEntityType and IsEntityCollectionType methods).",
                            GetType(),
                            ex.InvalidType),
                        ex);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(
                        string.Format("Unable to build update delegate for {0}", GetType()),
                        ex);
                }
            });

            _asyncUpdater = new Lazy<Func<TTo, TTo, IEnumerable<Func<Task>>>>(() =>
            {
                try
                {
                    return mapperHelper.GetAsyncUpdateFunc(Paths, UpdateManager, DeleteAndCreateOnPaths);
                }
                catch (UnableToBuildActionException ex)
                {
                    throw new InvalidOperationException(
                        string.Format(
                            "Unable to build an update delegate for {0}: unknown type {1}. Check that your IObjectUpdateManager implementation correctly handles {1} (by default it delegates to IEntityTypesRetriever and its IsEntityType and IsEntityCollectionType methods).",
                            GetType(),
                            ex.InvalidType),
                        ex);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(
                        string.Format("Unable to build update delegate for {0}", GetType()),
                        ex);
                }
            });
        }

        /// <summary>
        /// Поведение в случае наличия в TTo свойств, для которых не найдено соответствие
        /// </summary>
        protected override AutoPropertiesBehavior AutoPropertiesBehavior
        {
            get { return AutoPropertiesBehavior.Loose; }
        }

        /// <summary>
        /// Делегат для обновления
        /// </summary>
        [NotNull]
        public virtual Action<TTo, TTo> Updater { get { return GetLazyValueAndClearCacheIfProcessed(_updater); } }

        /// <summary>
        /// Делегат для обновления объекта, возвращающий набор задач, возникающих при обновлении
        /// </summary>
        [NotNull]
        public virtual Func<TTo, TTo, IEnumerable<Func<Task>>> AsyncUpdater { get { return GetLazyValueAndClearCacheIfProcessed(_asyncUpdater); } }

        /// <summary>
        /// Управляющий обновлением объект
        /// </summary>
        [NotNull]
        protected abstract IObjectUpdateManager UpdateManager { get; }
    }
}
