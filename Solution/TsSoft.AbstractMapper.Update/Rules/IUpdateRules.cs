﻿namespace TsSoft.AbstractMapper.Rules
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    /// <summary>
    /// Правила обновления
    /// </summary>
    /// <typeparam name="TTo">Тип обновляемой сущности</typeparam>
    public interface IUpdateRules<TTo> : IReadOnlyDictionary<Expression<Func<TTo, object>>, UpdateType>
    {
    }
}
