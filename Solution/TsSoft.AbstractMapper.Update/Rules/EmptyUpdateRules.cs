﻿namespace TsSoft.AbstractMapper.Rules
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// Пустые правила обновления
    /// </summary>
    /// <typeparam name="TTo">Тип, в который поисходит преобразование</typeparam>
    public class EmptyUpdateRules<TTo> : IUpdateRules<TTo>
    {
        /// <summary>
        /// Перечислитель пустых правил обновления
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Перечислитель пустых правил обновления
        /// </summary>
        public IEnumerator<KeyValuePair<Expression<Func<TTo, object>>, UpdateType>> GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Нуль
        /// </summary>
        public int Count { get { return 0; } }

        /// <summary>
        /// Ложь
        /// </summary>
        public bool ContainsKey(Expression<Func<TTo, object>> key)
        {
            return false;
        }

        /// <summary>
        /// Правило по умолчанию
        /// </summary>
        public bool TryGetValue(Expression<Func<TTo, object>> key, out UpdateType value)
        {
            value = default(UpdateType);
            return false;
        }

        /// <summary>
        /// Выбрасывает <exception cref="KeyNotFoundException"></exception>
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public UpdateType this[Expression<Func<TTo, object>> key]
        {
            get { throw new KeyNotFoundException(); }
        }

        /// <summary>
        /// Пустая коллекция ключей
        /// </summary>
        public IEnumerable<Expression<Func<TTo, object>>> Keys { get { return Enumerable.Empty<Expression<Func<TTo, object>>>(); } }
        /// <summary>
        /// Пустая коллекция значений
        /// </summary>
        public IEnumerable<UpdateType> Values { get { return Enumerable.Empty<UpdateType>(); } }
    }

}
