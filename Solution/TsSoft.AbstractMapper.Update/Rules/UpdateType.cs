﻿namespace TsSoft.AbstractMapper.Rules
{
    /// <summary>
    /// Тип обновления зависимых сущностей
    /// </summary>
    public enum UpdateType
    {
        /// <summary>
        /// При совпадении id обновлять подсущности, а не пересоздавать
        /// </summary>
        /// <remarks>Обновляемая коллекция получается в одном запросе вместе с обновляемой сущностью</remarks>
        PreserveByIds,

        /// <summary>
        /// Удалить всю коллекцию и создать все обновлённые элементы заново
        /// </summary>
        /// <remarks>Пересоздаваемая коллекция получается в отдельном запросе</remarks>
        DeleteThenCreate,
    }
}
