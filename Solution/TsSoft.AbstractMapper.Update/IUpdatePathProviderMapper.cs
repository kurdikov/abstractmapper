﻿namespace TsSoft.AbstractMapper
{
    /// <summary>
    /// Маппер, отдающий изменяемые им пути
    /// </summary>
    /// <typeparam name="TFrom">Тип, из которого происходит преобразование</typeparam>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    public interface IUpdatePathProviderMapper<in TFrom, TTo> : IMapper<TFrom, TTo>, IUpdatePathProvider<TTo>
    {
    }
}
