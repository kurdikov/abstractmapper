﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Создаёт пути обновления по описанию маппинга
    /// </summary>
    public interface IUpdatePathsCreator
    {
        /// <summary>
        /// Создать пути обновления сущности по описанию маппинга
        /// </summary>
        /// <typeparam name="TTo">Тип сущности</typeparam>
        /// <param name="paths">Описание маппинга</param>
        /// <param name="excludePrimaryKey">Не добавлять первичный ключ в обновляемые пути</param>
        /// <param name="context">Контекст генерации</param>
        [NotNull]
        ICollection<Expression<Func<TTo, object>>> Create<TTo>(IEnumerable<MappedPathDescription> paths, bool excludePrimaryKey = true, GeneratorContext context = null);
    }
}
