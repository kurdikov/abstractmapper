﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules.Factories;
    using TsSoft.Expressions.Helpers.Closures;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Хелпер для абстрактного маппера, отдающего пути обновления
    /// </summary>
    public class AbstractUpdatePathProviderMapperHelper : 
        AbstractMapperHelper,
        IAbstractUpdatePathProviderMapperHelper
    {
        [NotNull] private readonly IUpdatePathsCreator _updateCreator;

        /// <summary>
        /// Хелпер для абстрактного маппера, отдающего пути обновления
        /// </summary>
        public AbstractUpdatePathProviderMapperHelper(
            [NotNull] IMapExpressionCreator expressionCreator,
            [NotNull] IIgnoreRulesFactory ignoreRulesFactory,
            [NotNull] ILambdaCompiler lambdaCompiler,
            [NotNull] IFullMapRulesCreator fullMapRulesCreator,
            [NotNull] IUpdatePathsCreator updateCreator)
            : base(expressionCreator, ignoreRulesFactory, lambdaCompiler, fullMapRulesCreator)
        {
            if (updateCreator == null) throw new ArgumentNullException("updateCreator");
            _updateCreator = updateCreator;
        }

        /// <summary>
        /// Получить пути обновления по описанию маппинга
        /// </summary>
        /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
        /// <param name="paths">Описание маппинга</param>
        /// <param name="excludePrimaryKey">Не обновлять первичный ключ</param>
        /// <param name="context">Контекст генерации</param>
        /// <returns>Пути обновления</returns>
        public ICollection<Expression<Func<TTo, object>>> GetUpdatePaths<TTo>(
            IEnumerable<MappedPathDescription> paths, bool excludePrimaryKey, GeneratorContext context)
        {
            return _updateCreator.Create<TTo>(paths, excludePrimaryKey, context);
        }
    }
}
