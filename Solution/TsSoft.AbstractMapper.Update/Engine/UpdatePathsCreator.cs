﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.NestedGenerators;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.AbstractMapper;

    internal class UpdatePathsCreator : IUpdatePathsCreator
    {
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;
        [NotNull]private readonly IMapperResolver _mapperResolver;
        [NotNull]private readonly IFlatPathParser _pathParser;
        [NotNull]private readonly IEntityTypesRetriever _entityTypesRetriever;
        [NotNull]private readonly IEntityTypesHelper _entityTypesHelper;
        [NotNull]private readonly INestedGeneratorHelper _nestedGeneratorHelper;

        [NotNull]private readonly MethodInfo _tryGetMapperPaths;
        [NotNull]private readonly MethodInfo _extractFromProvider;

        public UpdatePathsCreator(
            [NotNull]IExpressionBuilder expressionBuilder, 
            [NotNull]IMapperResolver mapperResolver, 
            [NotNull]IFlatPathParser pathParser,
            [NotNull]IMemberInfoHelper memberInfoHelper, 
            [NotNull]IEntityTypesHelper entityTypesHelper, 
            [NotNull]INestedGeneratorHelper nestedGeneratorHelper, 
            [NotNull]IEntityTypesRetriever entityTypesRetriever)
        {
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (mapperResolver == null) throw new ArgumentNullException("mapperResolver");
            if (pathParser == null) throw new ArgumentNullException("pathParser");
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            if (nestedGeneratorHelper == null) throw new ArgumentNullException("nestedGeneratorHelper");
            if (entityTypesRetriever == null) throw new ArgumentNullException("entityTypesRetriever");
            _expressionBuilder = expressionBuilder;
            _mapperResolver = mapperResolver;
            _pathParser = pathParser;
            _entityTypesHelper = entityTypesHelper;
            _nestedGeneratorHelper = nestedGeneratorHelper;
            _entityTypesRetriever = entityTypesRetriever;

            _tryGetMapperPaths = memberInfoHelper.GetGenericDefinitionMethodInfo(() => TryGetMapperPaths<object, object, object>(null));
            _extractFromProvider = memberInfoHelper.GetGenericDefinitionMethodInfo(() => ExtractFromProvider<object>(null, null));
        }

        public ICollection<Expression<Func<TTo, object>>> Create<TTo>(IEnumerable<MappedPathDescription> paths, bool excludePrimaryKey = true, GeneratorContext context = null)
        {
            context = context ?? new GeneratorContext();
            var result = new List<Expression<Func<TTo, object>>>();
            foreach (var path in paths ?? Enumerable.Empty<MappedPathDescription>())
            {
                if (path == null || path.ToPath == null 
                    || excludePrimaryKey 
                        && _entityTypesRetriever.IsEntityType(typeof(TTo))
                        && _entityTypesHelper.GetPrimaryKey(typeof (TTo)).Contains(path.ToPath, MemberInfoNameComparer.Instance))
                {
                    continue;
                }
                if (path.MapperDescription != null)
                {
                    var innerPaths = path.MapperDescription.MapperObject == null
                        ? TryGetMapperPaths(path.MapperDescription, context)
                        : ExtractFromProvider(path.MapperDescription, context);
                    result.AddRange((innerPaths ?? Enumerable.Empty<ValueHoldingMember>().ToEnumerable())
                        .Where(ip => ip != null)
                        .Select(ip => _expressionBuilder.BuildIncludeByPath<TTo>(path.ToPath.ConcatWith(ip))));
                }
                else
                {
                    result.Add(_expressionBuilder.BuildIncludeByPath<TTo>(path.ToPath.ToEnumerable()));
                }
            }
            return result;
        }

        private IEnumerable<IReadOnlyList<ValueHoldingMember>> ParseUpdatePaths<TInnerTo>(
            [NotNull]IEnumerable<Expression<Func<TInnerTo, object>>> paths)
        {
            return paths.Select(_pathParser.Parse);
        }

        private IEnumerable<IReadOnlyList<ValueHoldingMember>> ExtractFromProvider(
            [NotNull]MapperDescription description,
            GeneratorContext context)
        {
            var method = _extractFromProvider.MakeGenericMethod(description.MapperToType);
            var @delegate = DelegateCreator.Create<Func<object, GeneratorContext, IEnumerable<IReadOnlyList<ValueHoldingMember>>>>(
                this, method);
            return @delegate(description.MapperObject, context);
        }

        private IEnumerable<IReadOnlyList<ValueHoldingMember>> ExtractFromProvider<TInnerTo>(object providerCandidate, GeneratorContext context)
        {
            var generator = providerCandidate as ICyclePreventingUpdatePathGenerator<TInnerTo>;
            if (generator != null)
            {
                return _nestedGeneratorHelper.GenerateInContext(
                    generator,
                    g => g.Paths,
                    (g, c) => g.GenerateUpdatePaths(c),
                    ParseUpdatePaths,
                    context,
                    Enumerable.Empty<IReadOnlyList<ValueHoldingMember>>());
            }
            var mapperWithPaths = providerCandidate as IUpdatePathProvider<TInnerTo>;
            return mapperWithPaths != null
                ? ParseUpdatePaths(mapperWithPaths.Paths)
                : null;
        }

        private IEnumerable<IReadOnlyList<ValueHoldingMember>> TryGetMapperPaths(
            [NotNull] MapperDescription description,
            GeneratorContext context)
        {
            var method = _tryGetMapperPaths.MakeGenericMethod(
                description.MapperFromType,
                description.MapperToType,
                description.MapperToSingleType);
            var @delegate = DelegateCreator.Create<Func<GeneratorContext, IEnumerable<IReadOnlyList<ValueHoldingMember>>>>(
                this, method);
            return @delegate(context);
        }

        private IEnumerable<IReadOnlyList<ValueHoldingMember>> TryGetMapperPaths<TInnerFrom, TInnerTo, TInnerSingleTo>(GeneratorContext context)
        {
            var mapper = _mapperResolver.TryGet<TInnerFrom, TInnerTo>();
            var paths = ExtractFromProvider<TInnerSingleTo>(mapper, context);
            return paths;
        }
    }
}
