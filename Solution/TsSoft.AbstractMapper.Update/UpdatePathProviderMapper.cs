﻿namespace TsSoft.AbstractMapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Преобразователь с контейнером обновляемых путей
    /// </summary>
    /// <typeparam name="TFrom">Тип, из которого происходит преобразование</typeparam>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    public class UpdatePathProviderMapper<TFrom, TTo> : Mapper<TFrom, TTo>, IUpdatePathProviderMapper<TFrom, TTo>, ICyclePreventingUpdatePathGenerator<TTo>
        where TFrom : class
        where TTo : class, new()
    {
        [NotNull]
        private readonly Lazy<ICollection<Expression<Func<TTo, object>>>> _paths;

        /// <summary>
        /// Преобразователь с контейнером обновляемых путей
        /// </summary>
        public UpdatePathProviderMapper([NotNull]IAbstractUpdatePathProviderMapperHelper mapperHelper)
            : base(mapperHelper)
        {
            _paths = new Lazy<ICollection<Expression<Func<TTo, object>>>>(() => GenerateUpdatePaths(new GeneratorContext(GetType(), MaxNestingLevel)).ToList());
        }

        /// <summary>
        /// Обновляемые пути
        /// </summary>
        public IEnumerable<Expression<Func<TTo, object>>> Paths { get { return GetLazyValueAndClearCacheIfProcessed(_paths); } }

        /// <summary>
        /// Поведение в случае наличия в TTo свойств, для которых не найдено соответствие
        /// </summary>
        protected override AutoPropertiesBehavior AutoPropertiesBehavior
        {
            get
            {
                return AutoPropertiesBehavior.Loose;
            }
        }

        /// <summary>
        /// Правила обновления
        /// </summary>
        [NotNull]
        protected virtual IUpdateRules<TTo> UpdateRules { get { return new EmptyUpdateRules<TTo>(); } }

        /// <summary>
        /// На каких путях удалять сущности и создавать новые вместо слияния
        /// </summary>
        public IEnumerable<Expression<Func<TTo, object>>> DeleteAndCreateOnPaths
        {
            get { return UpdateRules.Where(r => r.Value == UpdateType.DeleteThenCreate).Select(r => r.Key); }
        }

        /// <summary>
        /// Вся ли необходимая информация получена из построенного выражения
        /// </summary>
        protected override bool IsExpressionProcessed
        {
            get { return base.IsExpressionProcessed && _paths.IsValueCreated; }
        }

        /// <summary>
        /// Максимальное количество вложений в себя
        /// </summary>
        public virtual int MaxNestingLevel { get { return 0; } }

        /// <summary>
        /// Сгенерировать обновляемые пути в заданном контексте
        /// </summary>
        [NotNull]
        public IEnumerable<Expression<Func<TTo, object>>> GenerateUpdatePaths(GeneratorContext context)
        {
            try
            {
                var result = _paths.IsValueCreated// || (context == null || !context.RemainingNestingLevel.ContainsKey(GetType()))
                    ? _paths.Value
                    : ((IAbstractUpdatePathProviderMapperHelper)MapperHelper).GetUpdatePaths<TTo>(MapperExpression.MappedPaths, ExcludePrimaryKey, context);
                return result.ThrowIfNull("_paths.Value");
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(string.Format("Unable to build update paths for {0}", GetType()), ex);
            }
        }

        /// <summary>
        /// Исключить первичный ключ TTo из обновляемых путей
        /// </summary>
        protected virtual bool ExcludePrimaryKey { get { return true; } }
    }
}
