﻿namespace TsSoft.Bindings
{
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Engine;
    using TsSoft.BindingsDescription;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.AbstractMapper.Update
    /// </summary>
    public class AbstractMapperUpdateBindings : BindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке TsSoft.AbstractMapper.Update
        /// </summary>
        public AbstractMapperUpdateBindings()
        {
            Bind<IAbstractUpdatePathProviderMapperHelper, AbstractUpdatePathProviderMapperHelper>();
            Bind<IUpdatePathsCreator, UpdatePathsCreator>();
            Bind<IAbstractUpdateFuncProviderMapperHelper, AbstractUpdateFuncProviderMapperHelper>();
        }
    }
}
