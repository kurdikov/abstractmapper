﻿namespace TsSoft.EntityService.Tests
{
    using System;
    using System.Collections.Generic;
#if NET45
    using System.Data.Entity;
#elif NETCORE
    using Microsoft.EntityFrameworkCore;
#endif
    using System.Linq;
    using System.Threading.Tasks;
    using BaseTestClasses;
    using BaseTestClasses.Database;
    using TsSoft.Expressions.Models.Reflection;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.ContextWrapper;
    using TsSoft.EntityRepository;
    using TsSoft.EntityRepository.Factory;
    using TsSoft.EntityService.Mapper;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.BindingsDescription;
    using System.Reflection;
    using TsSoft.Bindings;

    [TestClass]
    public class DatabaseServiceIntegrationTests : RepositoryTestBase
    {
        private class Kernel
        {
            public FactoryInstance Factory { get; set; }
            public MapperTypes Mappers { get; set; }
            public Entities Entities { get; set; }
            public External External { get; set; }

            public T Get<T>()
            {
                return Factory.GetDependency<T>(Mappers, Entities, External);
            }
        }

        [NotNull]
        private Kernel CreateServiceKernel(params Type[] mapperTypes)
        {
            var factory = new FactoryInstance(
                new IBindingsDescription[0],
                null);
            factory.AddStaticHelper<IEntityTypesRetriever>(EntityStaticHelpers.EntityTypesRetriever);
            factory.AddStaticHelper<IItemWrapper<DbContext>>(ContextWrapper);
            factory.AddStaticHelper<IRepositoryFactory>(RepositoryFactory);
            factory.AddBindings(new EntityServiceBindings());
            factory.AddBindings(new AbstractMapperBindings());
            factory.AddBindings(new AbstractMapperSelectBindings());
            factory.AddBindings(new AbstractMapperUpdateBindings());
            factory.AddBindings(new EntityRepositoryBindings());
            factory.AddBindings(new ExpressionsHelpersBindings());
            factory.AddBindings(new ExpressionsHelpersEntityBindings());
            factory.AddBindings(new ExpressionsIncludeBuilderBindings());
            factory.AddBindings(new ExpressionsSelectBuilderBindings());
            foreach (var mapperType in mapperTypes)
            {
                foreach (var mapperInterface in mapperType.GetTypeInfo().GetInterfaces().Where(IsMapperInterface))
                {
                    factory.AddBinding(mapperInterface, mapperType);
                }
            }
            return new Kernel
            {
                Factory = factory,
                Mappers = MapperTypes.Are(mapperTypes),
                Entities = Entities.Are(EntityStaticHelpers.EntityTypes),
                External = External.Are(EntityStaticHelpers.ExternalTypes),
            };
        }

        private bool IsMapperInterface(Type inter)
        {
            var typeInfo = inter.GetTypeInfo();
            if (typeInfo.IsGenericType && typeInfo.GetGenericTypeDefinition() == typeof(IMapper<,>))
            {
                return true;
            }
            return typeInfo.GetInterfaces().Any(IsMapperInterface);
        }

        [TestMethod]
        public void TestGetUpdate()
        {
            var kernel = CreateServiceKernel(
                typeof(FirstServiceIntegrationTestMapper),
                typeof(SecondServiceIntegrationTestMapper),
                typeof(NotInterestingSecondServiceIntegrationTestMapper),
                typeof(FirstServiceIntegrationTestUpdater),
                typeof(SecondServiceIntegrationTestUpdater),
                typeof(NotInterestingSecondServiceIntegrationTestUpdater));

            var service = kernel.Get<IDatabaseService<First, FirstServiceIntegrationTestModel>>();
            var filteringMapper = kernel.Get<FirstWithFilteredSecondsIntegrationTestMapper>();
            var created = service.Create(new FirstServiceIntegrationTestModel
                {
                    Name = "created",
                    Seconds = new HashSet<SecondServiceIntegrationTestModel>
                        {
                            new SecondServiceIntegrationTestModel {Type = "ToPreserve"},
                            new SecondServiceIntegrationTestModel {Type = "ToDelete"},
                        },
                    NotInterestingSeconds = new HashSet<NotInterestingSecondServiceIntegrationTestModel>
                        {
                            new NotInterestingSecondServiceIntegrationTestModel {Guid = Guid.Parse("11111111-1111-1111-1111-111111111111")},
                            new NotInterestingSecondServiceIntegrationTestModel {Guid = Guid.Parse("22222222-2222-2222-2222-222222222222")},
                        }
                }, createAction: e => { e.ImportantMoment = new DateTime(1993, 10, 3); });

            Assert.AreEqual("created", created.Name);
            Assert.AreEqual(2, created.Seconds.Count());
            Assert.AreEqual(2, created.NotInterestingSeconds.Count());
            Assert.IsTrue(created.Seconds.Any(j => j.Type == "ToPreserve"));
            Assert.IsTrue(created.Seconds.Any(j => j.Type == "ToDelete"));
            Assert.IsTrue(created.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")));
            Assert.IsTrue(created.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("22222222-2222-2222-2222-222222222222")));

            var toPreserveId = created.Seconds.Single(j => j.Type == "ToPreserve").Id;
            var toDeleteId = created.Seconds.Single(j => j.Type == "ToDelete").Id;
            var idEmp1 = created.NotInterestingSeconds.Single(e => e.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")).Id;
            var idEmp2 = created.NotInterestingSeconds.Single(e => e.Guid == Guid.Parse("22222222-2222-2222-2222-222222222222")).Id;

            var got = service.GetSingle(created.Id);
            Assert.AreEqual(created.Id, got.Id);
            Assert.AreEqual("created", got.Name);
            Assert.AreEqual(2, got.Seconds.Count());
            Assert.AreEqual(toPreserveId, got.Seconds.Single(j => j.Type == "ToPreserve").Id);
            Assert.AreEqual(toDeleteId, got.Seconds.Single(j => j.Type == "ToDelete").Id);
            Assert.AreEqual(2, got.NotInterestingSeconds.Count());
            Assert.IsTrue(got.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")));
            Assert.IsTrue(got.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("22222222-2222-2222-2222-222222222222")));

            var updated = service.UpdateSingle(created.Id, new FirstServiceIntegrationTestModel
            {
                Name = "updated",
                Seconds = new HashSet<SecondServiceIntegrationTestModel>
                {
                    new SecondServiceIntegrationTestModel {Id = got.Seconds.SingleOrDefault(j => j.Type == "ToPreserve").Id, Type="Preserved"},
                    new SecondServiceIntegrationTestModel {Type = "BrandNew"},
                },
                NotInterestingSeconds = new HashSet<NotInterestingSecondServiceIntegrationTestModel>
                {
                    new NotInterestingSecondServiceIntegrationTestModel {Guid = Guid.Parse("11111111-1111-1111-1111-111111111111")},
                    new NotInterestingSecondServiceIntegrationTestModel {Guid = Guid.Parse("33333333-3333-3333-3333-333333333333")}
                },
            });
            Assert.AreEqual(created.Id, updated.Id);
            Assert.AreEqual("updated", updated.Name);
            Assert.AreEqual(2, updated.Seconds.Count());
            Assert.AreEqual(toPreserveId, updated.Seconds.Single(j => j.Type == "Preserved").Id);
            Assert.IsFalse(updated.Seconds.Any(j => j.Type == "ToDelete"));
            Assert.IsFalse(updated.Seconds.Any(j => j.Id == toDeleteId));
            Assert.IsTrue(updated.Seconds.Any(j => j.Type == "BrandNew"));
            Assert.AreEqual(2, updated.NotInterestingSeconds.Count());
            Assert.IsTrue(updated.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")));
            Assert.AreNotEqual(idEmp1, updated.NotInterestingSeconds.Single(c => c.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")).Id);
            Assert.IsTrue(updated.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("33333333-3333-3333-3333-333333333333")));
            var brandNewId = updated.Seconds.Single(j => j.Type == "BrandNew").Id;

            got = service.GetSingle(created.Id);
            Assert.AreEqual(created.Id, got.Id);
            Assert.AreEqual("updated", got.Name);
            Assert.AreEqual(2, got.Seconds.Count());
            Assert.AreEqual(toPreserveId, got.Seconds.Single(j => j.Type == "Preserved").Id);
            Assert.IsFalse(got.Seconds.Any(j => j.Type == "ToDelete"));
            Assert.IsFalse(got.Seconds.Any(j => j.Id == toDeleteId));
            Assert.IsTrue(got.Seconds.Any(j => j.Type == "BrandNew"));
            Assert.AreEqual(2, got.NotInterestingSeconds.Count());
            Assert.IsTrue(got.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")));
            Assert.AreNotEqual(idEmp1, got.NotInterestingSeconds.Single(c => c.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")).Id);
            Assert.IsTrue(got.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("33333333-3333-3333-3333-333333333333")));

            var firstRepo = kernel.Get<IBaseRepository<First>>();
            var firstWithFilteredSeconds = firstRepo.GetSingle(e => e.Id == created.Id, filteringMapper.Select);
            Assert.AreEqual(created.Id, firstWithFilteredSeconds.Id);
            Assert.AreEqual("updated", firstWithFilteredSeconds.Name);
            Assert.AreEqual(new DateTime(1993, 10, 3), firstWithFilteredSeconds.ImportantMoment);
            Assert.AreEqual(2, firstWithFilteredSeconds.Seconds.Count());
            Assert.IsTrue(firstWithFilteredSeconds.Seconds.All(j => j.Bool));
            Assert.AreEqual(toPreserveId, firstWithFilteredSeconds.Seconds.Single(j => j.Type == "Preserved").Id);
            Assert.AreEqual(brandNewId, firstWithFilteredSeconds.Seconds.Single(j => j.Type == "BrandNew").Id);
            Assert.AreEqual(0, firstWithFilteredSeconds.NotInterestingSeconds.Count());
            var mapped = filteringMapper.Map(firstWithFilteredSeconds);
            Assert.AreEqual(created.Id, mapped.Id);
            Assert.AreEqual("updated", mapped.Name);
            Assert.AreEqual(new DateTime(1993, 10, 3), firstWithFilteredSeconds.ImportantMoment);
            Assert.AreEqual(1, mapped.PreservedSeconds.Count());
            Assert.AreEqual(toPreserveId, mapped.PreservedSeconds.Single(j => j.Type == "Preserved").Id);
            Assert.AreEqual(1, mapped.BrandNewSeconds.Count());
            Assert.AreEqual(brandNewId, mapped.BrandNewSeconds.Single(j => j.Type == "BrandNew").Id);
        }

        [TestMethod]
        public void TestUpdateCompositeForeignKeysWithMerge()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<CompositePk, CompositePkIntegrationTestModel>),
                typeof(UpdatePathProviderMapper<CompositePkIntegrationTestModel, CompositePk>),
                typeof(SelectProviderMapper<CompositeFk, CompositeFkIntegrationTestModel>),
                typeof(UpdatePathProviderMapper<CompositeFkIntegrationTestModel, CompositeFk>));

            var firstId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var secondId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var data = new[]
            {
                new CompositePk
                {
                    Part1 = firstId.Item1, 
                    Part2 = firstId.Item2, 
                    Data = "1",
                    CompositeFks = new List<CompositeFk>
                    {
                        new CompositeFk {Data = "1.1"},
                        new CompositeFk {Data = "1.2"},
                    }
                },
                new CompositePk
                {
                    Part1 = secondId.Item1,
                    Part2 = secondId.Item2,
                    Data = "2",
                    CompositeFks = new List<CompositeFk>
                    {
                        new CompositeFk {Data = "2.1"},
                        new CompositeFk {Data = "2.2"},
                    }
                }
            };
            Seed(data);

            var service = kernel.Get<IDatabaseService<CompositePk, CompositePkIntegrationTestModel>>();

            var model = service.GetSingle(e => e.Part1 == firstId.Item1 && e.Part2 == firstId.Item2);
            Assert.AreEqual(data[0].Part1, model.Part1);
            Assert.AreEqual(data[0].Part2, model.Part2);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositeFks.Count, model.CompositeFks.Count);
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.2"));
            var fks = model.CompositeFks.ToList();
            model.CompositeFks = fks.Where(fk => fk.Data == "1.1").Concat(new CompositeFkIntegrationTestModel {Data = "1.3"}.ToEnumerable()).ToList();

            model = service.UpdateSingle(e => e.Part1 == firstId.Item1 && e.Part2 == firstId.Item2, model);
            Assert.AreEqual(data[0].Part1, model.Part1);
            Assert.AreEqual(data[0].Part2, model.Part2);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositeFks.Count, model.CompositeFks.Count);
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.3"));
        }

        [TestMethod]
        public void TestUpdateCompositeForeignKeysWithDeleteCreate()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<CompositePk, CompositePkIntegrationTestModel>),
                typeof(CompositePkIntegrationTestUpdateMapperDrop),
                typeof(SelectProviderMapper<CompositeFk, CompositeFkIntegrationTestModel>),
                typeof(UpdatePathProviderMapper<CompositeFkIntegrationTestModel, CompositeFk>));

            var firstId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var secondId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var data = new[]
            {
                new CompositePk
                {
                    Part1 = firstId.Item1, 
                    Part2 = firstId.Item2, 
                    Data = "1",
                    CompositeFks = new List<CompositeFk>
                    {
                        new CompositeFk {Data = "1.1"},
                        new CompositeFk {Data = "1.2"},
                    }
                },
                new CompositePk
                {
                    Part1 = secondId.Item1,
                    Part2 = secondId.Item2,
                    Data = "2",
                    CompositeFks = new List<CompositeFk>
                    {
                        new CompositeFk {Data = "2.1"},
                        new CompositeFk {Data = "2.2"},
                    }
                }
            };
            Seed(data);

            var service = kernel.Get<IDatabaseService<CompositePk, CompositePkIntegrationTestModel>>();

            var model = service.GetSingle(e => e.Part1 == firstId.Item1 && e.Part2 == firstId.Item2);
            Assert.AreEqual(data[0].Part1, model.Part1);
            Assert.AreEqual(data[0].Part2, model.Part2);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositeFks.Count, model.CompositeFks.Count);
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.2"));
            var fks = model.CompositeFks.ToList();
            model.CompositeFks = fks.Where(fk => fk.Data == "1.1").Concat(new CompositeFkIntegrationTestModel { Data = "1.3" }.ToEnumerable()).ToList();

            model = service.UpdateSingle(e => e.Part1 == firstId.Item1 && e.Part2 == firstId.Item2, model);
            Assert.AreEqual(data[0].Part1, model.Part1);
            Assert.AreEqual(data[0].Part2, model.Part2);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositeFks.Count, model.CompositeFks.Count);
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.3"));
        }

        [TestMethod]
        public void TestUpdateCompositePrimaryKeysWithMerge()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<CompositeRoot, CompositeRootIntegrationTestModel>),
                typeof(UpdatePathProviderMapper<CompositeRootIntegrationTestModel, CompositeRoot>),
                typeof(SelectProviderMapper<CompositePk, CompositePkIntegrationTestSimpleModel>),
                typeof(CompositePkUpdateMapperWithPk));

            var firstId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var secondId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var thirdId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var data = new[]
            {
                new CompositeRoot
                {
                    Data = "1",
                    CompositePks = new List<CompositePk>
                    {
                        new CompositePk {Part1 = firstId.Item1, Part2 = firstId.Item2, Data = "1.1"},
                        new CompositePk {Part1 = secondId.Item1, Part2 = secondId.Item2, Data = "1.2"},
                    }
                },
            };
            Seed(data);

            var service = kernel.Get<IDatabaseService<CompositeRoot, CompositeRootIntegrationTestModel>>();

            var rootId = data[0].Id;
            var model = service.GetSingle(e => e.Id == rootId);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositePks.Count, model.CompositePks.Count);
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.2"));
            var fks = model.CompositePks.ToList();
            model.CompositePks = fks.Where(fk => fk.Data == "1.1").Concat(new CompositePkIntegrationTestSimpleModel { Part1 = thirdId.Item1, Part2 = thirdId.Item2, Data = "1.3" }.ToEnumerable()).ToList();

            model = service.UpdateSingle(e => e.Id == rootId, model);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositePks.Count, model.CompositePks.Count);
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.3"));
        }

#if NETCORE
        [Ignore("В EF.Core не работает пересоздание сущности с тем же идентификатором после удаления")]
#endif
        [TestMethod]
        public void TestUpdateCompositePrimaryKeysWithDeleteCreate()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<CompositeRoot, CompositeRootIntegrationTestModel>),
                typeof(CompositeRootIntegrationTestUpdateMapperDrop),
                typeof(SelectProviderMapper<CompositePk, CompositePkIntegrationTestSimpleModel>),
                typeof(CompositePkUpdateMapperWithPk));

            var firstId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var secondId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var thirdId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var data = new[]
            {
                new CompositeRoot
                {
                    Data = "1",
                    CompositePks = new List<CompositePk>
                    {
                        new CompositePk {Part1 = firstId.Item1, Part2 = firstId.Item2, Data = "1.1"},
                        new CompositePk {Part1 = secondId.Item1, Part2 = secondId.Item2, Data = "1.2"},
                    }
                },
            };
            Seed(data);

            var service = kernel.Get<IDatabaseService<CompositeRoot, CompositeRootIntegrationTestModel>>();

            var rootId = data[0].Id;
            var model = service.GetSingle(e => e.Id == rootId);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositePks.Count, model.CompositePks.Count);
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.2"));
            var fks = model.CompositePks.ToList();
            model.CompositePks = fks.Where(fk => fk.Data == "1.1")
                .AppendOne(new CompositePkIntegrationTestSimpleModel { Part1 = thirdId.Item1, Part2 = thirdId.Item2, Data = "1.3" }).ToList();

            model = service.UpdateSingle(e => e.Id == rootId, model);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositePks.Count, model.CompositePks.Count);
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.3"));
        }

        [TestMethod]
        public void TestFkPkUpdatingFromPkPk()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<SecondWithFkPk, SecondWithFkPkModel>),
                typeof(SelectProviderMapper<ThirdWithPkPk, ThirdWithPkPkModel>),
                typeof(UpdateEntityMapper<SecondWithFkPkModel, SecondWithFkPk>),
                typeof(UpdateEntityMapper<ThirdWithPkPkModel, ThirdWithPkPk>));

            var service = kernel.Get<IDatabaseService<ThirdWithPkPk, ThirdWithPkPkModel>>();

            var id = Guid.NewGuid();

            var created = service.Create(
                new ThirdWithPkPkModel
                    {
                        Data = 1,
                        Second = new SecondWithFkPkModel { Type = "1" }
                    }, createAction: pk => pk.Id = id);
            Assert.IsNotNull(created);
            Assert.AreEqual(id, created.Id);
            Assert.AreEqual(1, created.Data);
            Assert.IsNotNull(created.Second);
            Assert.AreEqual(id, created.Second.Id);
            Assert.AreEqual("1", created.Second.Type);

            var updated = service.UpdateSingle(
                e => e.Id == id, new ThirdWithPkPkModel
                    {
                        Data = 2,
                        Second = new SecondWithFkPkModel { Type = "2" }
                    });
            Assert.IsNotNull(updated);
            Assert.AreEqual(id, updated.Id);
            Assert.AreEqual(2, updated.Data);
            Assert.IsNotNull(updated.Second);
            Assert.AreEqual(id, updated.Second.Id);
            Assert.AreEqual("2", updated.Second.Type);

            var got = service.GetSingle(e => e.Id == id);
            Assert.IsNotNull(got);
            Assert.AreEqual(id, got.Id);
            Assert.AreEqual(2, got.Data);
            Assert.IsNotNull(got.Second);
            Assert.AreEqual(id, got.Second.Id);
            Assert.AreEqual("2", got.Second.Type);

            var updatedWithDeletion = service.UpdateSingle(
                e => e.Id == id, new ThirdWithPkPkModel
                    {
                        Data = 3,
                    });
            Assert.IsNotNull(updatedWithDeletion);
            Assert.AreEqual(id, updatedWithDeletion.Id);
            Assert.AreEqual(3, updatedWithDeletion.Data);
            Assert.IsNull(updatedWithDeletion.Second);

            got = service.GetSingle(e => e.Id == id);
            Assert.IsNotNull(got);
            Assert.AreEqual(id, got.Id);
            Assert.AreEqual(3, got.Data);
            Assert.IsNull(got.Second);
        }

        [TestMethod]
        public void TestFkPkUpdatingFromFkPk()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<SecondWithFkPk, SecondWithFkPkModel2>),
                typeof(SelectProviderMapper<ThirdWithPkPk, ThirdWithPkPkModel2>),
                typeof(UpdateEntityMapper<SecondWithFkPkModel2, SecondWithFkPk>),
                typeof(UpdateEntityMapper<ThirdWithPkPkModel2, ThirdWithPkPk>));

            var service = kernel.Get<IDatabaseService<SecondWithFkPk, SecondWithFkPkModel2>>();

            var id = Guid.NewGuid();

            var created = service.Create(
                new SecondWithFkPkModel2
                    {
                        Type = "1",
                        Third = new ThirdWithPkPkModel2 {Data = 1}
                    }, createAction: pk => pk.Third = new ThirdWithPkPk {Id = id});
            Assert.IsNotNull(created);
            Assert.AreEqual(id, created.Id);
            Assert.AreEqual("1", created.Type);
            Assert.IsNotNull(created.Third);
            Assert.AreEqual(id, created.Third.Id);
            Assert.AreEqual(1, created.Third.Data);

            var updated = service.UpdateSingle(
                e => e.Id == id, new SecondWithFkPkModel2
                    {
                        Type = "2",
                        Third = new ThirdWithPkPkModel2 {Data = 2},
                    });
            Assert.IsNotNull(updated);
            Assert.AreEqual(id, updated.Id);
            Assert.AreEqual("2", updated.Type);
            Assert.IsNotNull(updated.Third);
            Assert.AreEqual(id, updated.Third.Id);
            Assert.AreEqual(2, updated.Third.Data);

            var got = service.GetSingle(e => e.Id == id);
            Assert.IsNotNull(got);
            Assert.AreEqual(id, got.Id);
            Assert.AreEqual("2", got.Type);
            Assert.IsNotNull(got.Third);
            Assert.AreEqual(id, got.Third.Id);
            Assert.AreEqual(2, got.Third.Data);
        }

        [TestMethod]
        public void TestTwoStepUpdate()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<First, FirstTwoStepModel>),
                typeof(UpdatePathProviderMapper<FirstTwoStepModel, First>),
                typeof(SelectProviderMapper<Second, SecondTwoStepModel>),
                typeof(UpdatePathProviderMapper<SecondTwoStepModel, Second>),
                typeof(SelectProviderMapper<Third, ThirdTwoStepModel>),
                typeof(UpdatePathProviderMapper<ThirdTwoStepModel, Third>));

            var mappers = MapperTypes.Are();

            var service = kernel.Get<IDatabaseService<First, FirstTwoStepModel>>();

            var created = service.Create(new FirstTwoStepModel
            {
                Name = "name",
                Seconds = new List<SecondTwoStepModel>
                {
                    new SecondTwoStepModel
                    {
                        Type = "1",
                        Thirds = new List<ThirdTwoStepModel>
                        {
                            new ThirdTwoStepModel {Type = "11"},
                            new ThirdTwoStepModel {Type = "12"},
                        }
                    },
                    new SecondTwoStepModel
                    {
                        Type = "2",
                        Thirds = new List<ThirdTwoStepModel>
                        {
                            new ThirdTwoStepModel {Type = "21"},
                            new ThirdTwoStepModel {Type = "22"},
                        }
                    }
                }
            });
            Assert.AreEqual("name", created.Name);
            Assert.AreEqual(2, created.Seconds.Count);
            var createdSecond1 = created.Seconds.FirstOrDefault(s => s.Type == "1");
            Assert.IsNotNull(createdSecond1);
            Assert.AreEqual(2, createdSecond1.Thirds.Count);
            var createdThird11 = createdSecond1.Thirds.FirstOrDefault(t => t.Type == "11");
            Assert.IsNotNull(createdThird11);
            Assert.IsNotNull(createdSecond1.Thirds.FirstOrDefault(t => t.Type == "12"));
            var createdSecond2 = created.Seconds.FirstOrDefault(s => s.Type == "2");
            Assert.IsNotNull(createdSecond2);
            Assert.AreEqual(2, createdSecond2.Thirds.Count);
            Assert.IsNotNull(createdSecond2.Thirds.FirstOrDefault(t => t.Type == "21"));
            Assert.IsNotNull(createdSecond2.Thirds.FirstOrDefault(t => t.Type == "22"));

            var updated = service.UpdateSingle(
                created.Id,
                new FirstTwoStepModel
                {
                    Name = "name updated",
                    Seconds = new List<SecondTwoStepModel>
                    {
                        new SecondTwoStepModel
                        {
                            Id = createdSecond1.Id,
                            Type = "1 updated",
                            Thirds = new List<ThirdTwoStepModel>
                            {
                                new ThirdTwoStepModel {Id = createdThird11.Id, Type = "11 updated"},
                                new ThirdTwoStepModel {Type = "13"}
                            }
                        },
                        new SecondTwoStepModel
                        {
                            Type = "3",
                            Thirds = new List<ThirdTwoStepModel>
                            {
                                new ThirdTwoStepModel {Type = "31"}
                            }
                        }
                    }
                });
            Assert.AreEqual("name updated", updated.Name);
            Assert.AreEqual(2, updated.Seconds.Count);
            var updatedSecond1 = updated.Seconds.FirstOrDefault(s => s.Type == "1 updated");
            Assert.IsNotNull(updatedSecond1);
            Assert.AreEqual(createdSecond1.Id, updatedSecond1.Id);
            Assert.AreEqual(2, updatedSecond1.Thirds.Count);
            var updatedThird11 = updatedSecond1.Thirds.FirstOrDefault(t => t.Type == "11 updated");
            Assert.IsNotNull(updatedThird11);
            Assert.AreEqual(createdThird11.Id, updatedThird11.Id);
            Assert.IsNotNull(updatedSecond1.Thirds.FirstOrDefault(t => t.Type == "13"));
            var updatedSecond2 = updated.Seconds.FirstOrDefault(s => s.Type == "3");
            Assert.IsNotNull(updatedSecond2);
            Assert.AreEqual(1, updatedSecond2.Thirds.Count);
            Assert.IsNotNull(updatedSecond2.Thirds.FirstOrDefault(t => t.Type == "31"));
        }
        
        [TestMethod]
        public void TestUpdateWithNullCollection()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<First, FirstSimpleIntegrationModel>),
                typeof(UpdatePathProviderMapper<FirstSimpleIntegrationModel, First>),
                typeof(SelectProviderMapper<Second, SecondSimpleIntegrationModel>),
                typeof(UpdatePathProviderMapper<SecondSimpleIntegrationModel, Second>));

            var service = kernel.Get<IDatabaseService<First, FirstSimpleIntegrationModel>>();
            var created = service.Create(new FirstSimpleIntegrationModel
            {
                Name = "name",
                Seconds = new List<SecondSimpleIntegrationModel>
                {
                    new SecondSimpleIntegrationModel{Type = "1"},
                    new SecondSimpleIntegrationModel{Type = "2"},
                }
            });
            Assert.AreEqual("name", created.Name);
            Assert.AreEqual(2, created.Seconds.Count);
            var createdSecond1 = created.Seconds.FirstOrDefault(s => s.Type == "1");
            Assert.IsNotNull(createdSecond1);
            var createdSecond2 = created.Seconds.FirstOrDefault(s => s.Type == "2");
            Assert.IsNotNull(createdSecond2);

            var updated = service.UpdateSingle(created.Id, new FirstSimpleIntegrationModel {Name = "name edited"});
            Assert.AreEqual(updated.Id, created.Id);
            Assert.AreEqual("name edited", updated.Name);
            Assert.AreEqual(0, updated.Seconds.Count);
        }



        [TestMethod]
        public async Task TestAsyncGetUpdate()
        {
            var kernel = CreateServiceKernel(
                typeof(FirstServiceIntegrationTestMapper),
                typeof(SecondServiceIntegrationTestMapper),
                typeof(NotInterestingSecondServiceIntegrationTestMapper),
                typeof(FirstServiceIntegrationTestUpdater),
                typeof(SecondServiceIntegrationTestUpdater),
                typeof(NotInterestingSecondServiceIntegrationTestUpdater));

            var service = kernel.Get<IDatabaseService<First, FirstServiceIntegrationTestModel>>();
            var filteringMapper = kernel.Get<FirstWithFilteredSecondsIntegrationTestMapper>();
            var created = await service.CreateAsync(new FirstServiceIntegrationTestModel
            {
                Name = "created",
                Seconds = new HashSet<SecondServiceIntegrationTestModel>
                        {
                            new SecondServiceIntegrationTestModel {Type = "ToPreserve"},
                            new SecondServiceIntegrationTestModel {Type = "ToDelete"},
                        },
                NotInterestingSeconds = new HashSet<NotInterestingSecondServiceIntegrationTestModel>
                        {
                            new NotInterestingSecondServiceIntegrationTestModel {Guid = Guid.Parse("11111111-1111-1111-1111-111111111111")},
                            new NotInterestingSecondServiceIntegrationTestModel {Guid = Guid.Parse("22222222-2222-2222-2222-222222222222")},
                        }
            }, createAction: e => { e.ImportantMoment = new DateTime(1993, 10, 3); });

            Assert.AreEqual("created", created.Name);
            Assert.AreEqual(2, created.Seconds.Count());
            Assert.AreEqual(2, created.NotInterestingSeconds.Count());
            Assert.IsTrue(created.Seconds.Any(j => j.Type == "ToPreserve"));
            Assert.IsTrue(created.Seconds.Any(j => j.Type == "ToDelete"));
            Assert.IsTrue(created.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")));
            Assert.IsTrue(created.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("22222222-2222-2222-2222-222222222222")));

            var toPreserveId = created.Seconds.Single(j => j.Type == "ToPreserve").Id;
            var toDeleteId = created.Seconds.Single(j => j.Type == "ToDelete").Id;
            var idEmp1 = created.NotInterestingSeconds.Single(e => e.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")).Id;
            var idEmp2 = created.NotInterestingSeconds.Single(e => e.Guid == Guid.Parse("22222222-2222-2222-2222-222222222222")).Id;

            var got = await service.GetSingleAsync(created.Id);
            Assert.AreEqual(created.Id, got.Id);
            Assert.AreEqual("created", got.Name);
            Assert.AreEqual(2, got.Seconds.Count());
            Assert.AreEqual(toPreserveId, got.Seconds.Single(j => j.Type == "ToPreserve").Id);
            Assert.AreEqual(toDeleteId, got.Seconds.Single(j => j.Type == "ToDelete").Id);
            Assert.AreEqual(2, got.NotInterestingSeconds.Count());
            Assert.IsTrue(got.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")));
            Assert.IsTrue(got.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("22222222-2222-2222-2222-222222222222")));

            var updated = await service.UpdateSingleAsync(created.Id, new FirstServiceIntegrationTestModel
            {
                Name = "updated",
                Seconds = new HashSet<SecondServiceIntegrationTestModel>
                        {
                            new SecondServiceIntegrationTestModel {Id = got.Seconds.SingleOrDefault(j => j.Type == "ToPreserve").Id, Type="Preserved"},
                            new SecondServiceIntegrationTestModel {Type = "BrandNew"},
                        },
                NotInterestingSeconds = new HashSet<NotInterestingSecondServiceIntegrationTestModel>
                        {
                            new NotInterestingSecondServiceIntegrationTestModel {Guid = Guid.Parse("11111111-1111-1111-1111-111111111111")},
                            new NotInterestingSecondServiceIntegrationTestModel {Guid = Guid.Parse("33333333-3333-3333-3333-333333333333")}
                        }
            });
            Assert.AreEqual(created.Id, updated.Id);
            Assert.AreEqual("updated", updated.Name);
            Assert.AreEqual(2, updated.Seconds.Count());
            Assert.AreEqual(toPreserveId, updated.Seconds.Single(j => j.Type == "Preserved").Id);
            Assert.IsFalse(updated.Seconds.Any(j => j.Type == "ToDelete"));
            Assert.IsFalse(updated.Seconds.Any(j => j.Id == toDeleteId));
            Assert.IsTrue(updated.Seconds.Any(j => j.Type == "BrandNew"));
            Assert.AreEqual(2, updated.NotInterestingSeconds.Count());
            Assert.IsTrue(updated.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")));
            Assert.AreNotEqual(idEmp1, updated.NotInterestingSeconds.Single(c => c.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")).Id);
            Assert.IsTrue(updated.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("33333333-3333-3333-3333-333333333333")));
            var brandNewId = updated.Seconds.Single(j => j.Type == "BrandNew").Id;

            got = await service.GetSingleAsync(created.Id);
            Assert.AreEqual(created.Id, got.Id);
            Assert.AreEqual("updated", got.Name);
            Assert.AreEqual(2, got.Seconds.Count());
            Assert.AreEqual(toPreserveId, got.Seconds.Single(j => j.Type == "Preserved").Id);
            Assert.IsFalse(got.Seconds.Any(j => j.Type == "ToDelete"));
            Assert.IsFalse(got.Seconds.Any(j => j.Id == toDeleteId));
            Assert.IsTrue(got.Seconds.Any(j => j.Type == "BrandNew"));
            Assert.AreEqual(2, got.NotInterestingSeconds.Count());
            Assert.IsTrue(got.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")));
            Assert.AreNotEqual(idEmp1, got.NotInterestingSeconds.Single(c => c.Guid == Guid.Parse("11111111-1111-1111-1111-111111111111")).Id);
            Assert.IsTrue(got.NotInterestingSeconds.Any(c => c.Guid == Guid.Parse("33333333-3333-3333-3333-333333333333")));

            var firstRepo = kernel.Get<IBaseRepository<First>>();
            var firstWithFilteredSeconds = await firstRepo.GetSingleAsync(e => e.Id == created.Id, filteringMapper.Select);
            Assert.AreEqual(created.Id, firstWithFilteredSeconds.Id);
            Assert.AreEqual("updated", firstWithFilteredSeconds.Name);
            Assert.AreEqual(new DateTime(1993, 10, 3), firstWithFilteredSeconds.ImportantMoment);
            Assert.AreEqual(2, firstWithFilteredSeconds.Seconds.Count());
            Assert.IsTrue(firstWithFilteredSeconds.Seconds.All(j => j.Bool));
            Assert.AreEqual(toPreserveId, firstWithFilteredSeconds.Seconds.Single(j => j.Type == "Preserved").Id);
            Assert.AreEqual(brandNewId, firstWithFilteredSeconds.Seconds.Single(j => j.Type == "BrandNew").Id);
            Assert.AreEqual(0, firstWithFilteredSeconds.NotInterestingSeconds.Count());
            var mapped = filteringMapper.Map(firstWithFilteredSeconds);
            Assert.AreEqual(created.Id, mapped.Id);
            Assert.AreEqual("updated", mapped.Name);
            Assert.AreEqual(new DateTime(1993, 10, 3), firstWithFilteredSeconds.ImportantMoment);
            Assert.AreEqual(1, mapped.PreservedSeconds.Count());
            Assert.AreEqual(toPreserveId, mapped.PreservedSeconds.Single(j => j.Type == "Preserved").Id);
            Assert.AreEqual(1, mapped.BrandNewSeconds.Count());
            Assert.AreEqual(brandNewId, mapped.BrandNewSeconds.Single(j => j.Type == "BrandNew").Id);
        }

        [TestMethod]
        public async Task TestAsyncUpdateCompositeForeignKeysWithMerge()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<CompositePk, CompositePkIntegrationTestModel>),
                typeof(UpdatePathProviderMapper<CompositePkIntegrationTestModel, CompositePk>),
                typeof(SelectProviderMapper<CompositeFk, CompositeFkIntegrationTestModel>),
                typeof(UpdatePathProviderMapper<CompositeFkIntegrationTestModel, CompositeFk>));

            var firstId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var secondId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var data = new[]
            {
                new CompositePk
                {
                    Part1 = firstId.Item1, 
                    Part2 = firstId.Item2, 
                    Data = "1",
                    CompositeFks = new List<CompositeFk>
                    {
                        new CompositeFk {Data = "1.1"},
                        new CompositeFk {Data = "1.2"},
                    }
                },
                new CompositePk
                {
                    Part1 = secondId.Item1,
                    Part2 = secondId.Item2,
                    Data = "2",
                    CompositeFks = new List<CompositeFk>
                    {
                        new CompositeFk {Data = "2.1"},
                        new CompositeFk {Data = "2.2"},
                    }
                }
            };
            Seed(data);

            var service = kernel.Get<IDatabaseService<CompositePk, CompositePkIntegrationTestModel>>();

            var model = await service.GetSingleAsync(e => e.Part1 == firstId.Item1 && e.Part2 == firstId.Item2);
            Assert.AreEqual(data[0].Part1, model.Part1);
            Assert.AreEqual(data[0].Part2, model.Part2);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositeFks.Count, model.CompositeFks.Count);
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.2"));
            var fks = model.CompositeFks.ToList();
            model.CompositeFks = fks.Where(fk => fk.Data == "1.1").Concat(new CompositeFkIntegrationTestModel { Data = "1.3" }.ToEnumerable()).ToList();

            model = await service.UpdateSingleAsync(e => e.Part1 == firstId.Item1 && e.Part2 == firstId.Item2, model);
            Assert.AreEqual(data[0].Part1, model.Part1);
            Assert.AreEqual(data[0].Part2, model.Part2);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositeFks.Count, model.CompositeFks.Count);
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.3"));
        }

        [TestMethod]
        public async Task TestAsyncUpdateCompositeForeignKeysWithDeleteCreate()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<CompositePk, CompositePkIntegrationTestModel>),
                typeof(CompositePkIntegrationTestUpdateMapperDrop),
                typeof(SelectProviderMapper<CompositeFk, CompositeFkIntegrationTestModel>),
                typeof(UpdatePathProviderMapper<CompositeFkIntegrationTestModel, CompositeFk>));

            var firstId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var secondId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var data = new[]
            {
                new CompositePk
                {
                    Part1 = firstId.Item1, 
                    Part2 = firstId.Item2, 
                    Data = "1",
                    CompositeFks = new List<CompositeFk>
                    {
                        new CompositeFk {Data = "1.1"},
                        new CompositeFk {Data = "1.2"},
                    }
                },
                new CompositePk
                {
                    Part1 = secondId.Item1,
                    Part2 = secondId.Item2,
                    Data = "2",
                    CompositeFks = new List<CompositeFk>
                    {
                        new CompositeFk {Data = "2.1"},
                        new CompositeFk {Data = "2.2"},
                    }
                }
            };
            Seed(data);

            var service = kernel.Get<IDatabaseService<CompositePk, CompositePkIntegrationTestModel>>();

            var model = await service.GetSingleAsync(e => e.Part1 == firstId.Item1 && e.Part2 == firstId.Item2);
            Assert.AreEqual(data[0].Part1, model.Part1);
            Assert.AreEqual(data[0].Part2, model.Part2);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositeFks.Count, model.CompositeFks.Count);
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.2"));
            var fks = model.CompositeFks.ToList();
            model.CompositeFks = fks.Where(fk => fk.Data == "1.1").Concat(new CompositeFkIntegrationTestModel { Data = "1.3" }.ToEnumerable()).ToList();

            model = await service.UpdateSingleAsync(e => e.Part1 == firstId.Item1 && e.Part2 == firstId.Item2, model);
            Assert.AreEqual(data[0].Part1, model.Part1);
            Assert.AreEqual(data[0].Part2, model.Part2);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositeFks.Count, model.CompositeFks.Count);
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositeFks.Any(fk => fk.Data == "1.3"));
        }

        [TestMethod]
        public async Task TestAsyncUpdateCompositePrimaryKeysWithMerge()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<CompositeRoot, CompositeRootIntegrationTestModel>),
                typeof(UpdatePathProviderMapper<CompositeRootIntegrationTestModel, CompositeRoot>),
                typeof(SelectProviderMapper<CompositePk, CompositePkIntegrationTestSimpleModel>),
                typeof(CompositePkUpdateMapperWithPk));

            var firstId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var secondId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var thirdId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var data = new[]
            {
                new CompositeRoot
                {
                    Data = "1",
                    CompositePks = new List<CompositePk>
                    {
                        new CompositePk {Part1 = firstId.Item1, Part2 = firstId.Item2, Data = "1.1"},
                        new CompositePk {Part1 = secondId.Item1, Part2 = secondId.Item2, Data = "1.2"},
                    }
                },
            };
            Seed(data);

            var service = kernel.Get<IDatabaseService<CompositeRoot, CompositeRootIntegrationTestModel>>();

            var rootId = data[0].Id;
            var model = await service.GetSingleAsync(e => e.Id == rootId);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositePks.Count, model.CompositePks.Count);
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.2"));
            var fks = model.CompositePks.ToList();
            model.CompositePks = fks.Where(fk => fk.Data == "1.1").Concat(new CompositePkIntegrationTestSimpleModel { Part1 = thirdId.Item1, Part2 = thirdId.Item2, Data = "1.3" }.ToEnumerable()).ToList();

            model = await service.UpdateSingleAsync(e => e.Id == rootId, model);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositePks.Count, model.CompositePks.Count);
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.3"));
        }

#if NETCORE
        [Ignore("В EF.Core не работает пересоздание сущности с тем же идентификатором после удаления")]
#endif
        [TestMethod]
        public async Task TestAsyncUpdateCompositePrimaryKeysWithDeleteCreate()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<CompositeRoot, CompositeRootIntegrationTestModel>),
                typeof(CompositeRootIntegrationTestUpdateMapperDrop),
                typeof(SelectProviderMapper<CompositePk, CompositePkIntegrationTestSimpleModel>),
                typeof(CompositePkUpdateMapperWithPk));

            var firstId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var secondId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var thirdId = new Tuple<Guid, Guid>(Guid.NewGuid(), Guid.NewGuid());
            var data = new[]
            {
                new CompositeRoot
                {
                    Data = "1",
                    CompositePks = new List<CompositePk>
                    {
                        new CompositePk {Part1 = firstId.Item1, Part2 = firstId.Item2, Data = "1.1"},
                        new CompositePk {Part1 = secondId.Item1, Part2 = secondId.Item2, Data = "1.2"},
                    }
                },
            };
            Seed(data);

            var service = kernel.Get<IDatabaseService<CompositeRoot, CompositeRootIntegrationTestModel>>();

            var rootId = data[0].Id;
            var model = await service.GetSingleAsync(e => e.Id == rootId);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositePks.Count, model.CompositePks.Count);
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.2"));
            var fks = model.CompositePks.ToList();
            model.CompositePks = fks.Where(fk => fk.Data == "1.1").Concat(new CompositePkIntegrationTestSimpleModel { Part1 = thirdId.Item1, Part2 = thirdId.Item2, Data = "1.3" }.ToEnumerable()).ToList();

            model = await service.UpdateSingleAsync(e => e.Id == rootId, model);
            Assert.AreEqual(data[0].Data, model.Data);
            Assert.AreEqual(data[0].CompositePks.Count, model.CompositePks.Count);
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.1"));
            Assert.IsTrue(model.CompositePks.Any(fk => fk.Data == "1.3"));
        }

        [TestMethod]
        public async Task TestAsyncFkPkUpdatingFromPkPk()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<SecondWithFkPk, SecondWithFkPkModel>),
                typeof(SelectProviderMapper<ThirdWithPkPk, ThirdWithPkPkModel>),
                typeof(UpdateEntityMapper<SecondWithFkPkModel, SecondWithFkPk>),
                typeof(UpdateEntityMapper<ThirdWithPkPkModel, ThirdWithPkPk>));

            var service = kernel.Get<IDatabaseService<ThirdWithPkPk, ThirdWithPkPkModel>>();

            var id = Guid.NewGuid();

            var created = await service.CreateAsync(
                new ThirdWithPkPkModel
                {
                    Data = 1,
                    Second = new SecondWithFkPkModel { Type = "1" }
                }, createAction: pk => pk.Id = id);
            Assert.IsNotNull(created);
            Assert.AreEqual(id, created.Id);
            Assert.AreEqual(1, created.Data);
            Assert.IsNotNull(created.Second);
            Assert.AreEqual(id, created.Second.Id);
            Assert.AreEqual("1", created.Second.Type);

            var updated = await service.UpdateSingleAsync(
                e => e.Id == id, new ThirdWithPkPkModel
                {
                    Data = 2,
                    Second = new SecondWithFkPkModel { Type = "2" }
                });
            Assert.IsNotNull(updated);
            Assert.AreEqual(id, updated.Id);
            Assert.AreEqual(2, updated.Data);
            Assert.IsNotNull(updated.Second);
            Assert.AreEqual(id, updated.Second.Id);
            Assert.AreEqual("2", updated.Second.Type);

            var got = await service.GetSingleAsync(e => e.Id == id);
            Assert.IsNotNull(got);
            Assert.AreEqual(id, got.Id);
            Assert.AreEqual(2, got.Data);
            Assert.IsNotNull(got.Second);
            Assert.AreEqual(id, got.Second.Id);
            Assert.AreEqual("2", got.Second.Type);

            var updatedWithDeletion = await service.UpdateSingleAsync(
                e => e.Id == id, new ThirdWithPkPkModel
                {
                    Data = 3,
                });
            Assert.IsNotNull(updatedWithDeletion);
            Assert.AreEqual(id, updatedWithDeletion.Id);
            Assert.AreEqual(3, updatedWithDeletion.Data);
            Assert.IsNull(updatedWithDeletion.Second);

            got = await service.GetSingleAsync(e => e.Id == id);
            Assert.IsNotNull(got);
            Assert.AreEqual(id, got.Id);
            Assert.AreEqual(3, got.Data);
            Assert.IsNull(got.Second);
        }

        [TestMethod]
        public async Task TestAsyncFkPkUpdatingFromFkPk()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<SecondWithFkPk, SecondWithFkPkModel2>),
                typeof(SelectProviderMapper<ThirdWithPkPk, ThirdWithPkPkModel2>),
                typeof(UpdateEntityMapper<SecondWithFkPkModel2, SecondWithFkPk>),
                typeof(UpdateEntityMapper<ThirdWithPkPkModel2, ThirdWithPkPk>));

            var service = kernel.Get<IDatabaseService<SecondWithFkPk, SecondWithFkPkModel2>>();

            var id = Guid.NewGuid();

            var created = await service.CreateAsync(
                new SecondWithFkPkModel2
                {
                    Type = "1",
                    Third = new ThirdWithPkPkModel2 { Data = 1 }
                }, createAction: pk => pk.Third = new ThirdWithPkPk { Id = id });
            Assert.IsNotNull(created);
            Assert.AreEqual(id, created.Id);
            Assert.AreEqual("1", created.Type);
            Assert.IsNotNull(created.Third);
            Assert.AreEqual(id, created.Third.Id);
            Assert.AreEqual(1, created.Third.Data);

            var updated = await service.UpdateSingleAsync(
                e => e.Id == id, new SecondWithFkPkModel2
                {
                    Type = "2",
                    Third = new ThirdWithPkPkModel2 { Data = 2 },
                });
            Assert.IsNotNull(updated);
            Assert.AreEqual(id, updated.Id);
            Assert.AreEqual("2", updated.Type);
            Assert.IsNotNull(updated.Third);
            Assert.AreEqual(id, updated.Third.Id);
            Assert.AreEqual(2, updated.Third.Data);

            var got = await service.GetSingleAsync(e => e.Id == id);
            Assert.IsNotNull(got);
            Assert.AreEqual(id, got.Id);
            Assert.AreEqual("2", got.Type);
            Assert.IsNotNull(got.Third);
            Assert.AreEqual(id, got.Third.Id);
            Assert.AreEqual(2, got.Third.Data);
        }

        [TestMethod]
        public async Task TestAsyncTwoStepUpdate()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<First, FirstTwoStepModel>),
                typeof(UpdatePathProviderMapper<FirstTwoStepModel, First>),
                typeof(SelectProviderMapper<Second, SecondTwoStepModel>),
                typeof(UpdatePathProviderMapper<SecondTwoStepModel, Second>),
                typeof(SelectProviderMapper<Third, ThirdTwoStepModel>),
                typeof(UpdatePathProviderMapper<ThirdTwoStepModel, Third>));

            var service = kernel.Get<IDatabaseService<First, FirstTwoStepModel>>();

            var created = await service.CreateAsync(new FirstTwoStepModel
            {
                Name = "name",
                Seconds = new List<SecondTwoStepModel>
                {
                    new SecondTwoStepModel
                    {
                        Type = "1",
                        Thirds = new List<ThirdTwoStepModel>
                        {
                            new ThirdTwoStepModel {Type = "11"},
                            new ThirdTwoStepModel {Type = "12"},
                        }
                    },
                    new SecondTwoStepModel
                    {
                        Type = "2",
                        Thirds = new List<ThirdTwoStepModel>
                        {
                            new ThirdTwoStepModel {Type = "21"},
                            new ThirdTwoStepModel {Type = "22"},
                        }
                    }
                }
            });
            Assert.AreEqual("name", created.Name);
            Assert.AreEqual(2, created.Seconds.Count);
            var createdSecond1 = created.Seconds.FirstOrDefault(s => s.Type == "1");
            Assert.IsNotNull(createdSecond1);
            Assert.AreEqual(2, createdSecond1.Thirds.Count);
            var createdThird11 = createdSecond1.Thirds.FirstOrDefault(t => t.Type == "11");
            Assert.IsNotNull(createdThird11);
            Assert.IsNotNull(createdSecond1.Thirds.FirstOrDefault(t => t.Type == "12"));
            var createdSecond2 = created.Seconds.FirstOrDefault(s => s.Type == "2");
            Assert.IsNotNull(createdSecond2);
            Assert.AreEqual(2, createdSecond2.Thirds.Count);
            Assert.IsNotNull(createdSecond2.Thirds.FirstOrDefault(t => t.Type == "21"));
            Assert.IsNotNull(createdSecond2.Thirds.FirstOrDefault(t => t.Type == "22"));

            var updated = await service.UpdateSingleAsync(
                created.Id,
                new FirstTwoStepModel
                {
                    Name = "name updated",
                    Seconds = new List<SecondTwoStepModel>
                    {
                        new SecondTwoStepModel
                        {
                            Id = createdSecond1.Id,
                            Type = "1 updated",
                            Thirds = new List<ThirdTwoStepModel>
                            {
                                new ThirdTwoStepModel {Id = createdThird11.Id, Type = "11 updated"},
                                new ThirdTwoStepModel {Type = "13"}
                            }
                        },
                        new SecondTwoStepModel
                        {
                            Type = "3",
                            Thirds = new List<ThirdTwoStepModel>
                            {
                                new ThirdTwoStepModel {Type = "31"}
                            }
                        }
                    }
                });
            Assert.AreEqual("name updated", updated.Name);
            Assert.AreEqual(2, updated.Seconds.Count);
            var updatedSecond1 = updated.Seconds.FirstOrDefault(s => s.Type == "1 updated");
            Assert.IsNotNull(updatedSecond1);
            Assert.AreEqual(createdSecond1.Id, updatedSecond1.Id);
            Assert.AreEqual(2, updatedSecond1.Thirds.Count);
            var updatedThird11 = updatedSecond1.Thirds.FirstOrDefault(t => t.Type == "11 updated");
            Assert.IsNotNull(updatedThird11);
            Assert.AreEqual(createdThird11.Id, updatedThird11.Id);
            Assert.IsNotNull(updatedSecond1.Thirds.FirstOrDefault(t => t.Type == "13"));
            var updatedSecond2 = updated.Seconds.FirstOrDefault(s => s.Type == "3");
            Assert.IsNotNull(updatedSecond2);
            Assert.AreEqual(1, updatedSecond2.Thirds.Count);
            Assert.IsNotNull(updatedSecond2.Thirds.FirstOrDefault(t => t.Type == "31"));
        }

        [TestMethod]
        public async Task TestAsyncUpdateWithNullCollection()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<First, FirstSimpleIntegrationModel>),
                typeof(UpdatePathProviderMapper<FirstSimpleIntegrationModel, First>),
                typeof(SelectProviderMapper<Second, SecondSimpleIntegrationModel>),
                typeof(UpdatePathProviderMapper<SecondSimpleIntegrationModel, Second>));

            var service = kernel.Get<IDatabaseService<First, FirstSimpleIntegrationModel>>();
            var created = await service.CreateAsync(new FirstSimpleIntegrationModel
            {
                Name = "name",
                Seconds = new List<SecondSimpleIntegrationModel>
                {
                    new SecondSimpleIntegrationModel{Type = "1"},
                    new SecondSimpleIntegrationModel{Type = "2"},
                }
            });
            Assert.AreEqual("name", created.Name);
            Assert.AreEqual(2, created.Seconds.Count);
            var createdSecond1 = created.Seconds.FirstOrDefault(s => s.Type == "1");
            Assert.IsNotNull(createdSecond1);
            var createdSecond2 = created.Seconds.FirstOrDefault(s => s.Type == "2");
            Assert.IsNotNull(createdSecond2);

            var updated = await service.UpdateSingleAsync(created.Id, new FirstSimpleIntegrationModel { Name = "name edited" });
            Assert.AreEqual(updated.Id, created.Id);
            Assert.AreEqual("name edited", updated.Name);
            Assert.AreEqual(0, updated.Seconds.Count);
        }


        [TestMethod]
        public void TestUnknownTypesOnPaths()
        {
            var kernel = CreateServiceKernel(
                typeof(SelectProviderMapper<RegisteredEntity, RegisteredEntityViewModel>),
                typeof(SelectProviderMapper<UnregisteredEntity, RegisteredEntityViewModel>),
                typeof(UpdateEntityMapper<RegisteredEntityViewModel, RegisteredEntity>),
                typeof(UpdateEntityMapper<RegisteredEntityViewModel, UnregisteredEntity>));

            ExceptAssert.Throws<TargetInvocationException>(
                () => kernel.Get<ICreateDatabaseService<RegisteredEntity, RegisteredEntityViewModel>>(),
                e => e.InnerException is InvalidOperationException
                    && e.InnerException.InnerException is UnableToBuildActionException
                    && e.InnerException.Message.Contains(typeof(IEntityTypesRetriever).Name)
                    && e.InnerException.Message.Contains(typeof(IObjectUpdateManager).Name));
        }

        public class FirstServiceIntegrationTestModel
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public IEnumerable<SecondServiceIntegrationTestModel> Seconds { get; set; }

            public IEnumerable<NotInterestingSecondServiceIntegrationTestModel> NotInterestingSeconds { get; set; }
        }

        public class FirstWithFilteredSecondsServiceIntegrationTestModel
        {
            public int Id { get; set; }

            public DateTime ImportantMoment { get; set; }

            public string Name { get; set; }

            public IEnumerable<SecondServiceIntegrationTestModel> PreservedSeconds { get; set; }

            public IEnumerable<SecondServiceIntegrationTestModel> BrandNewSeconds { get; set; }
        }

        public class SecondServiceIntegrationTestModel
        {
            public int Id { get; set; }

            public string Type { get; set; }
        }

        public class NotInterestingSecondServiceIntegrationTestModel
        {
            public int Id { get; set; }

            public Guid Guid { get; set; }
        }

        public class FirstServiceIntegrationTestMapper : SelectProviderMapper<First, FirstServiceIntegrationTestModel>
        {
            public FirstServiceIntegrationTestMapper(IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<FirstServiceIntegrationTestModel, First> MapRules
            {
                get { return new EmptyMapRules<FirstServiceIntegrationTestModel, First>(); }
            }

            protected override IIgnoreRules<FirstServiceIntegrationTestModel> IgnoreRules
            {
                get { return new EmptyIgnoreRules<FirstServiceIntegrationTestModel>(); }
            }
        }

        public class SecondServiceIntegrationTestMapper : SelectProviderMapper<Second, SecondServiceIntegrationTestModel>
        {
            public SecondServiceIntegrationTestMapper(IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<SecondServiceIntegrationTestModel, Second> MapRules
            {
                get { return new EmptyMapRules<SecondServiceIntegrationTestModel, Second>(); }
            }

            protected override IIgnoreRules<SecondServiceIntegrationTestModel> IgnoreRules
            {
                get { return new EmptyIgnoreRules<SecondServiceIntegrationTestModel>(); }
            }
        }

        public class NotInterestingSecondServiceIntegrationTestMapper : SelectProviderMapper<NotInterestingSecond, NotInterestingSecondServiceIntegrationTestModel>
        {
            public NotInterestingSecondServiceIntegrationTestMapper(IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<NotInterestingSecondServiceIntegrationTestModel, NotInterestingSecond> MapRules
            {
                get { return new EmptyMapRules<NotInterestingSecondServiceIntegrationTestModel, NotInterestingSecond>(); }
            }

            protected override IIgnoreRules<NotInterestingSecondServiceIntegrationTestModel> IgnoreRules
            {
                get { return new EmptyIgnoreRules<NotInterestingSecondServiceIntegrationTestModel>(); }
            }
        }

        public class FirstServiceIntegrationTestUpdater : UpdatePathProviderMapper<FirstServiceIntegrationTestModel, First>
        {
            public FirstServiceIntegrationTestUpdater(IAbstractUpdatePathProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<First, FirstServiceIntegrationTestModel> MapRules
            {
                get
                {
                    return new MapRules<First, FirstServiceIntegrationTestModel>
                    {
                    };
                }
            }

            protected override IUpdateRules<First> UpdateRules
            {
                get
                {
                    return new UpdateRules<First>
                    {
                        {e => e.NotInterestingSeconds, UpdateType.DeleteThenCreate},
                        {e => e.Seconds, UpdateType.PreserveByIds}
                    };
                }
            }

            protected override IIgnoreRules<First> IgnoreRules
            {
                get { return new IgnoreRules<First>(); }
            }
        }

        public class SecondServiceIntegrationTestUpdater : UpdatePathProviderMapper<SecondServiceIntegrationTestModel, Second>
        {
            public SecondServiceIntegrationTestUpdater(IAbstractUpdatePathProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<Second, SecondServiceIntegrationTestModel> MapRules
            {
                get
                {
                    return new MapRules<Second, SecondServiceIntegrationTestModel>
                    {
                        {j => j.Bool, () => true}
                    };
                }
            }

            protected override IUpdateRules<Second> UpdateRules
            {
                get { return new UpdateRules<Second>(); }
            }

            protected override IIgnoreRules<Second> IgnoreRules
            {
                get { return new IgnoreRules<Second>(); }
            }
        }

        public class NotInterestingSecondServiceIntegrationTestUpdater : UpdatePathProviderMapper<NotInterestingSecondServiceIntegrationTestModel, NotInterestingSecond>
        {
            public NotInterestingSecondServiceIntegrationTestUpdater(IAbstractUpdatePathProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<NotInterestingSecond, NotInterestingSecondServiceIntegrationTestModel> MapRules
            {
                get { return new MapRules<NotInterestingSecond, NotInterestingSecondServiceIntegrationTestModel>(); }
            }

            protected override IIgnoreRules<NotInterestingSecond> IgnoreRules
            {
                get { return new IgnoreRules<NotInterestingSecond>(); }
            }

            protected override IUpdateRules<NotInterestingSecond> UpdateRules
            {
                get { return new UpdateRules<NotInterestingSecond>(); }
            }
        }

        public class FirstWithFilteredSecondsIntegrationTestMapper : SelectProviderMapper<First, FirstWithFilteredSecondsServiceIntegrationTestModel>
        {
            public FirstWithFilteredSecondsIntegrationTestMapper(IAbstractSelectProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<FirstWithFilteredSecondsServiceIntegrationTestModel, First> MapRules
            {
                get
                {
                    return new MapRules<FirstWithFilteredSecondsServiceIntegrationTestModel, First>
                    {
                        {m => m.PreservedSeconds, e => e.Seconds.Where(j => j.Type == "Preserved" && j.Bool)},
                        {m => m.BrandNewSeconds, e => e.Seconds.Where(j => j.Type == "BrandNew" && j.Bool)}
                    };
                }
            }

            protected override IIgnoreRules<FirstWithFilteredSecondsServiceIntegrationTestModel> IgnoreRules
            {
                get { return new EmptyIgnoreRules<FirstWithFilteredSecondsServiceIntegrationTestModel>(); }
            }
        }

        public class CompositePkIntegrationTestSimpleModel
        {
            public Guid Part1 { get; set; }
            public Guid Part2 { get; set; }
            public string Data { get; set; }
        }

        public class CompositePkIntegrationTestModel : CompositePkIntegrationTestSimpleModel
        {
            public IReadOnlyCollection<CompositeFkIntegrationTestModel> CompositeFks { get; set; }
        }

        public class CompositeFkIntegrationTestModel
        {
            public int Id { get; set; }
            public Guid Part1Id { get; set; }
            public Guid Part2Id { get; set; }
            public string Data { get; set; }
        }

        public class CompositeRootIntegrationTestModel
        {
            public int Id { get; set; }
            public string Data { get; set; }
            public IReadOnlyCollection<CompositePkIntegrationTestSimpleModel> CompositePks { get; set; }
        }

        public class CompositePkIntegrationTestUpdateMapperDrop :
            UpdatePathProviderMapper<CompositePkIntegrationTestModel, CompositePk>
        {
            public CompositePkIntegrationTestUpdateMapperDrop([NotNull] IAbstractUpdatePathProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IUpdateRules<CompositePk> UpdateRules
            {
                get { return new UpdateRules<CompositePk>
                {
                    {pk => pk.CompositeFks, UpdateType.DeleteThenCreate}
                }; }
            }
        }

        public class CompositeRootIntegrationTestUpdateMapperDrop :
            UpdatePathProviderMapper<CompositeRootIntegrationTestModel, CompositeRoot>
        {
            public CompositeRootIntegrationTestUpdateMapperDrop([NotNull] IAbstractUpdatePathProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IUpdateRules<CompositeRoot> UpdateRules
            {
                get
                {
                    return new UpdateRules<CompositeRoot>
                    {
                        {pk => pk.CompositePks, UpdateType.DeleteThenCreate}
                    };
                }
            }
        }

        public class CompositePkUpdateMapperWithPk :
            UpdatePathProviderMapper<CompositePkIntegrationTestSimpleModel, CompositePk>
        {
            public CompositePkUpdateMapperWithPk([NotNull] IAbstractUpdatePathProviderMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override bool ExcludePrimaryKey
            {
                get { return false; }
            }
        }

        public class ThirdWithPkPkModel
        {
            public Guid Id { get; set; }
            public int Data { get; set; }
            public SecondWithFkPkModel Second { get; set; }
        }

        public class SecondWithFkPkModel
        {
            public Guid Id { get; set; }
            public string Type { get; set; }
        }

        public class ThirdWithPkPkModel2
        {
            public Guid Id { get; set; }
            public int Data { get; set; }
        }

        public class SecondWithFkPkModel2
        {
            public Guid Id { get; set; }
            public string Type { get; set; }
            public ThirdWithPkPkModel2 Third { get; set; }
        }

        public class FirstTwoStepModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public List<SecondTwoStepModel> Seconds { get; set; }
        }

        public class SecondTwoStepModel
        {
            public int Id { get; set; }
            public string Type { get; set; }
            public List<ThirdTwoStepModel> Thirds { get; set; }
        }

        public class ThirdTwoStepModel
        {
            public int Id { get; set; }
            public string Type { get; set; }
        }

        public class FirstSimpleIntegrationModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public List<SecondSimpleIntegrationModel> Seconds { get; set; }
        }

        public class SecondSimpleIntegrationModel
        {
            public int Id { get; set; }
            public string Type { get; set; }
        }

        public class RegisteredEntity
        {
            public int Id { get; set; }
            public UnregisteredEntity Unregistered { get; set; }
        }
        public class UnregisteredEntity
        {
            public int Id { get; set; }
        }
        public class RegisteredEntityViewModel
        {
            public int Id { get; set; }
            public RegisteredEntityViewModel Unregistered { get; set; }
        }
    }
}
