﻿namespace TsSoft.EntityService.Tests.UpdateAction
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Threading.Tasks;
    using BaseTestClasses;
    using BaseTestClasses.Database;
    using Expressions.Helpers.Entity;
    using Expressions.Helpers.Exceptions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.EntityService.UpdateAction;

    [TestClass]
    public class UpdateEntityActionFactoryTests
    {
        private First GetSource()
        {
            return new First
            {
                Id = 1,
                Name = "One",
                FirstEnum = FirstEnum.Val2,
                Zero = new Zero
                {
                    Id = 2,
                    Guid = Guid.Parse("11111111-1111-1111-1111-111111111111"),
                    Date = new DateTime(1993, 10, 4),
                    String = "Two",
                },
                Seconds = new[]
                {
                    new Second {Id = 3, Type = "Three"},
                    new Second {Id = 4, Type = "Four"},
                },
                SecondWithFkPks = new[]
                {
                    new SecondWithFkPk {Id = Guid.Parse("22222222-2222-2222-2222-222222222222"), Type = "Twos"},
                    new SecondWithFkPk {Id = Guid.Parse("33333333-3333-3333-3333-333333333333"), Type = "Threes"},
                }
            };
        }

        private First GetTarget()
        {
            return new First
            {
                Id = 1,
                Name = "edited",
                FirstEnum = FirstEnum.Val1,
                Zero = new Zero
                {
                    Id = 2,
                    Guid = Guid.Parse("44444444-4444-4444-4444-444444444444"),
                    Date = DateTime.Now,
                    String = "edited zero",
                },
                Seconds = new[]
                {
                    new Second {Id = 5, Type = "Five"},
                    new Second {Id = 3, Type = "edited"},
                },
                SecondWithFkPks = new[]
                {
                    new SecondWithFkPk
                    {
                        Id = Guid.Parse("33333333-3333-3333-3333-333333333333"),
                        Type = "edited threes"
                    },
                    new SecondWithFkPk {Id = Guid.Parse("55555555-5555-5555-5555-555555555555")}
                }
            };
        }

        private First GetTwoStepSource()
        {
            return new First
            {
                Id = 1,
                Name = "name_changed",
                Seconds = new List<Second>
                {
                    new Second
                    {
                        Id = 1,
                        Type = "1",
                        Thirds = new List<Third>
                        {
                            new Third {Id = 11, Type = "11"},
                            new Third {Id = 13, Type = "13"},
                        }
                    },
                    new Second
                    {
                        Id = 3,
                        Type = "3",
                        Thirds = new List<Third>
                        {
                            new Third {Id = 31, Type = "31"},
                        }
                    }
                }
            };
        }

        private First GetTwoStepTarget()
        {
            return new First
            {
                Id = 1,
                Name = "name",
                Seconds = new List<Second>
                {
                    new Second
                    {
                        Id = 1,
                        Type = "1",
                        Thirds = new List<Third>
                        {
                            new Third {Id = 11, Type = "11"},
                            new Third {Id = 12, Type = "12"},
                        }
                    },
                    new Second
                    {
                        Id = 2,
                        Type = "2",
                        Thirds = new List<Third>
                        {
                            new Third {Id = 21, Type = "21"},
                            new Third {Id = 22, Type = "22"},
                        }
                    }
                }
            };
        }

        [TestMethod]
        public void MakeUpdateActionTest()
        {
            var mockCollectionRemover = new Mock<ICollectionRemover>(MockBehavior.Strict);
            var factory = new UpdateEntityActionFactory(
                StaticHelpers.UpdateObjectActionFactory,
                new EntityUpdateManager(
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.EntityTypesRetriever,
                    EntityStaticHelpers.EntityTypesHelper,
                    mockCollectionRemover.Object,
                    EntityStaticHelpers.KeyExpressionBuilder));
            var paths = new Expression<Func<First, object>>[]
                {
                    e => e.Name,
                    e => e.Zero,
                    e => e.Seconds.Select(j => j.Id),
                    e => e.Seconds.Select(j => j.Type),
                    e => e.SecondWithFkPks.Select(idpe => idpe.Id),
                    e => e.SecondWithFkPks.Select(idpe => idpe.Type),
                };
            var action = factory.MakeUpdateAction(paths);

            var source = GetSource();
            var target = GetTarget();
            mockCollectionRemover.Setup(icr => icr.RemoveEntities<Second, int>(
                It.Is<ICollection<Second>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), target.Seconds.Select(j => j.Id))),
                It.Is<ICollection<Second>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), source.Seconds.Select(j => j.Id))),
                It.IsAny<Func<Second, int>>()
                )).Verifiable();
            mockCollectionRemover.Setup(icr => icr.RemoveEntities<SecondWithFkPk, Guid>(
                It.Is<ICollection<SecondWithFkPk>>(
                    coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), target.SecondWithFkPks.Select(j => j.Id))),
                It.Is<ICollection<SecondWithFkPk>>(
                    coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), source.SecondWithFkPks.Select(j => j.Id))),
                It.IsAny<Func<SecondWithFkPk, Guid>>()
                )).Verifiable();

            action(source, target);

            Assert.AreEqual(1, target.Id);
            Assert.AreEqual(source.Name, target.Name);
            Assert.AreEqual(FirstEnum.Val1, target.FirstEnum);
            Assert.IsNotNull(target.Zero);
            Assert.AreNotSame(source.Zero, target.Zero);
            Assert.AreEqual(source.Zero.Guid, target.Zero.Guid);
            Assert.AreEqual(source.Zero.Date, target.Zero.Date);
            Assert.AreEqual(source.Zero.String, target.Zero.String);
            Assert.AreEqual(source.Seconds.Count, target.Seconds.Count);
            foreach (var second in source.Seconds)
            {
                var targetSec = target.Seconds.SingleOrDefault(j => j.Id == second.Id);
                Assert.AreNotSame(second, targetSec);
                Assert.IsNotNull(targetSec, second.Id.ToString(CultureInfo.InvariantCulture));
                Assert.AreEqual(second.Type, targetSec.Type);
            }
            foreach (var second in source.SecondWithFkPks)
            {
                var targetSec = target.SecondWithFkPks.SingleOrDefault(d => d.Id == second.Id);
                Assert.AreNotSame(second, targetSec);
                Assert.IsNotNull(targetSec, second.Id.ToString());
                Assert.AreEqual(second.Type, targetSec.Type);
            }

            mockCollectionRemover.VerifyAll();
        }

        [TestMethod]
        public void MakeAsyncUpdateActionTest()
        {
            //var mockCollectionRemover = new {Object = new CollectionRemover(null, null)};
            var mockCollectionRemover = new Mock<ICollectionRemover>(MockBehavior.Strict);
            var factory = new UpdateEntityActionFactory(
                StaticHelpers.UpdateObjectActionFactory,
                new EntityUpdateManager(
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.EntityTypesRetriever,
                    EntityStaticHelpers.EntityTypesHelper,
                    mockCollectionRemover.Object,
                    EntityStaticHelpers.KeyExpressionBuilder));
            var paths = new Expression<Func<First, object>>[]
                {
                    e => e.Name,
                    e => e.Zero,
                    e => e.Seconds.Select(j => j.Id),
                    e => e.Seconds.Select(j => j.Type),
                    e => e.SecondWithFkPks.Select(idpe => idpe.Id),
                    e => e.SecondWithFkPks.Select(idpe => idpe.Type),
                };
            var action = factory.MakeAsyncUpdateAction(paths);

            var source = GetSource();
            var target = GetTarget();
            var targetUnchanged = GetTarget();

            var funcs = action(source, target).ToList();

            Assert.AreEqual(1, target.Id);
            Assert.AreEqual(source.Name, target.Name);
            Assert.AreEqual(FirstEnum.Val1, target.FirstEnum);
            Assert.IsNotNull(target.Zero);
            Assert.AreNotSame(source.Zero, target.Zero);
            Assert.AreEqual(source.Zero.Guid, target.Zero.Guid);
            Assert.AreEqual(source.Zero.Date, target.Zero.Date);
            Assert.AreEqual(source.Zero.String, target.Zero.String);
            Assert.AreEqual(source.Seconds.Count, target.Seconds.Count);
            foreach (var second in source.Seconds)
            {
                var targetSec = target.Seconds.SingleOrDefault(j => j.Id == second.Id);
                Assert.AreNotSame(second, targetSec);
                Assert.IsNotNull(targetSec, second.Id.ToString(CultureInfo.InvariantCulture));
                Assert.AreEqual(second.Type, targetSec.Type);
            }
            foreach (var second in source.SecondWithFkPks)
            {
                var targetSec = target.SecondWithFkPks.SingleOrDefault(d => d.Id == second.Id);
                Assert.AreNotSame(second, targetSec);
                Assert.IsNotNull(targetSec, second.Id.ToString());
                Assert.AreEqual(second.Type, targetSec.Type);
            }

            mockCollectionRemover.Setup(icr => icr.RemoveEntitiesAsync<Second, int>(
                It.Is<ICollection<Second>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), targetUnchanged.Seconds.Select(j => j.Id))),
                It.Is<ICollection<Second>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), source.Seconds.Select(j => j.Id))),
                It.IsAny<Func<Second, int>>()
                )).Returns(Task.FromResult(0)).Verifiable();
            mockCollectionRemover.Setup(icr => icr.RemoveEntitiesAsync<SecondWithFkPk, Guid>(
                It.Is<ICollection<SecondWithFkPk>>(
                    coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), targetUnchanged.SecondWithFkPks.Select(j => j.Id))),
                It.Is<ICollection<SecondWithFkPk>>(
                    coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), source.SecondWithFkPks.Select(j => j.Id))),
                It.IsAny<Func<SecondWithFkPk, Guid>>()
                )).Returns(Task.FromResult(0)).Verifiable();
            Assert.AreEqual(2, funcs.Count);
            foreach (var func in funcs)
            {
                Assert.IsNotNull(func);
                func().Wait();
            }
            mockCollectionRemover.VerifyAll();
        }

        [TestMethod]
        public void MakeUpdateActionCreatingSubobjectTest()
        {
            var mockCollectionRemover = new Mock<ICollectionRemover>(MockBehavior.Strict);
            var factory = new UpdateEntityActionFactory(
                StaticHelpers.UpdateObjectActionFactory,
                new EntityUpdateManager(
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.EntityTypesRetriever,
                    EntityStaticHelpers.EntityTypesHelper,
                    mockCollectionRemover.Object,
                    EntityStaticHelpers.KeyExpressionBuilder));
            var paths = new Expression<Func<ThirdWithPkPk, object>>[]
                {
                    e => e.Data,
                    e => e.Second,
                };
            var action = factory.MakeUpdateAction(paths);

            var id = Guid.NewGuid();
            var source = new ThirdWithPkPk { Data = 2, Second = new SecondWithFkPk { Id = id, Type = "2" } };
            var target = new ThirdWithPkPk { Id = id, Data = 1 };

            action(source, target);

            Assert.AreEqual(id, target.Id);
            Assert.AreEqual(2, target.Data);
            Assert.IsNotNull(target.Second);
            Assert.AreEqual(id, target.Second.Id);
            Assert.AreEqual("2", target.Second.Type);
        }

        [TestMethod]
        public void MakeAsyncUpdateActionCreatingSubobjectTest()
        {
            var mockCollectionRemover = new Mock<ICollectionRemover>(MockBehavior.Strict);
            var factory = new UpdateEntityActionFactory(
                StaticHelpers.UpdateObjectActionFactory,
                new EntityUpdateManager(
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.EntityTypesRetriever,
                    EntityStaticHelpers.EntityTypesHelper,
                    mockCollectionRemover.Object,
                    EntityStaticHelpers.KeyExpressionBuilder));
            var paths = new Expression<Func<ThirdWithPkPk, object>>[]
                {
                    e => e.Data,
                    e => e.Second,
                };
            var action = factory.MakeAsyncUpdateAction(paths);

            var id = Guid.NewGuid();
            var source = new ThirdWithPkPk { Data = 2, Second = new SecondWithFkPk { Id = id, Type = "2" } };
            var target = new ThirdWithPkPk { Id = id, Data = 1 };

            var tasks = action(source, target);

            Assert.AreEqual(id, target.Id);
            Assert.AreEqual(2, target.Data);
            Assert.IsNotNull(target.Second);
            Assert.AreEqual(id, target.Second.Id);
            Assert.AreEqual("2", target.Second.Type);

            Assert.AreEqual(0, tasks.Count());
        }

        [TestMethod]
        public void MakeUpdateActionDeletingSubobjectTest()
        {
            var mockCollectionRemover = new Mock<ICollectionRemover>(MockBehavior.Strict);
            var factory = new UpdateEntityActionFactory(
                StaticHelpers.UpdateObjectActionFactory,
                new EntityUpdateManager(
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.EntityTypesRetriever,
                    EntityStaticHelpers.EntityTypesHelper,
                    mockCollectionRemover.Object,
                    EntityStaticHelpers.KeyExpressionBuilder));
            var paths = new Expression<Func<ThirdWithPkPk, object>>[]
                {
                    e => e.Data,
                    e => e.Second,
                };
            var action = factory.MakeUpdateAction(paths);

            var id = Guid.NewGuid();
            var source = new ThirdWithPkPk { Data = 2, };
            var target = new ThirdWithPkPk { Id = id, Data = 1, Second = new SecondWithFkPk { Type = "1" } };

            action(source, target);

            Assert.AreEqual(id, target.Id);
            Assert.AreEqual(2, target.Data);
            Assert.IsNull(target.Second);
        }

        [TestMethod]
        public void MakeAsyncUpdateActionDeletingSubobjectTest()
        {
            var mockCollectionRemover = new Mock<ICollectionRemover>(MockBehavior.Strict);
            var factory = new UpdateEntityActionFactory(
                StaticHelpers.UpdateObjectActionFactory,
                new EntityUpdateManager(
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.EntityTypesRetriever,
                    EntityStaticHelpers.EntityTypesHelper,
                    mockCollectionRemover.Object,
                    EntityStaticHelpers.KeyExpressionBuilder));
            var paths = new Expression<Func<ThirdWithPkPk, object>>[]
                {
                    e => e.Data,
                    e => e.Second,
                };
            var action = factory.MakeAsyncUpdateAction(paths);

            var id = Guid.NewGuid();
            var source = new ThirdWithPkPk { Data = 2, };
            var target = new ThirdWithPkPk { Id = id, Data = 1, Second = new SecondWithFkPk { Type = "1" } };

            var tasks = action(source, target);

            Assert.AreEqual(id, target.Id);
            Assert.AreEqual(2, target.Data);
            Assert.IsNull(target.Second);

            Assert.AreEqual(0, tasks.Count());
        }

        [TestMethod]
        public void MakeTwoPreservingStepsUpdateActionTest()
        {
            var mockCollectionRemover = new Mock<ICollectionRemover>(MockBehavior.Strict);
            var factory = new UpdateEntityActionFactory(
                StaticHelpers.UpdateObjectActionFactory,
                new EntityUpdateManager(
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.EntityTypesRetriever,
                    EntityStaticHelpers.EntityTypesHelper,
                    mockCollectionRemover.Object,
                    EntityStaticHelpers.KeyExpressionBuilder));
            var paths = new Expression<Func<First, object>>[]
                {
                    e => e.Name,
                    e => e.Seconds.Select(j => j.Id),
                    e => e.Seconds.Select(j => j.Type),
                    e => e.Seconds.Select(j => j.Thirds.Select(t => t.Id)),
                    e => e.Seconds.Select(j => j.Thirds.Select(t => t.Type)),
                };
            var action = factory.MakeUpdateAction(paths);

            var target = GetTwoStepTarget();
            var targetUnchanged = GetTwoStepTarget();
            var source = GetTwoStepSource();

            mockCollectionRemover.Setup(icr => icr.RemoveEntities<Second, int>(
                It.Is<ICollection<Second>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), targetUnchanged.Seconds.Select(j => j.Id))),
                It.Is<ICollection<Second>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), source.Seconds.Select(j => j.Id))),
                It.IsAny<Func<Second, int>>()
                )).Verifiable();
            mockCollectionRemover.Setup(icr => icr.RemoveEntities<Third, int>(
                It.Is<ICollection<Third>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), targetUnchanged.Seconds.ElementAt(0).Thirds.Select(j => j.Id))),
                It.Is<ICollection<Third>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), source.Seconds.ElementAt(0).Thirds.Select(j => j.Id))),
                It.IsAny<Func<Third, int>>()
                )).Verifiable();
            mockCollectionRemover.Setup(icr => icr.RemoveEntities<Third, int>(
                It.Is<ICollection<Third>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), new int[0])),
                It.Is<ICollection<Third>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), source.Seconds.ElementAt(1).Thirds.Select(j => j.Id))),
                It.IsAny<Func<Third, int>>()
                )).Verifiable();

            action(source, target);

            Assert.AreEqual(1, target.Id);
            Assert.AreEqual("name_changed", target.Name);
            Assert.AreEqual(2, target.Seconds.Count);
            Assert.AreEqual(1, target.Seconds.ElementAt(0).Id);
            Assert.AreEqual("1", target.Seconds.ElementAt(0).Type);
            Assert.AreEqual(2, target.Seconds.ElementAt(0).Thirds.Count);
            Assert.AreEqual(11, target.Seconds.ElementAt(0).Thirds.ElementAt(0).Id);
            Assert.AreEqual("11", target.Seconds.ElementAt(0).Thirds.ElementAt(0).Type);
            Assert.AreEqual(13, target.Seconds.ElementAt(0).Thirds.ElementAt(1).Id);
            Assert.AreEqual("13", target.Seconds.ElementAt(0).Thirds.ElementAt(1).Type);
            Assert.AreEqual(3, target.Seconds.ElementAt(1).Id);
            Assert.AreEqual("3", target.Seconds.ElementAt(1).Type);
            Assert.AreEqual(1, target.Seconds.ElementAt(1).Thirds.Count);
            Assert.AreEqual(31, target.Seconds.ElementAt(1).Thirds.ElementAt(0).Id);
            Assert.AreEqual("31", target.Seconds.ElementAt(1).Thirds.ElementAt(0).Type);


            mockCollectionRemover.VerifyAll();
        }

        [TestMethod]
        public void MakeAsyncTwoPreservingStepsUpdateActionTest()
        {
            var mockCollectionRemover = new Mock<ICollectionRemover>(MockBehavior.Strict);
            var factory = new UpdateEntityActionFactory(
                StaticHelpers.UpdateObjectActionFactory,
                new EntityUpdateManager(
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.EntityTypesRetriever,
                    EntityStaticHelpers.EntityTypesHelper,
                    mockCollectionRemover.Object,
                    EntityStaticHelpers.KeyExpressionBuilder));
            var paths = new Expression<Func<First, object>>[]
                {
                    e => e.Name,
                    e => e.Seconds.Select(j => j.Id),
                    e => e.Seconds.Select(j => j.Type),
                    e => e.Seconds.Select(j => j.Thirds.Select(t => t.Id)),
                    e => e.Seconds.Select(j => j.Thirds.Select(t => t.Type)),
                };
            var action = factory.MakeAsyncUpdateAction(paths);

            var target = GetTwoStepTarget();
            var source = GetTwoStepSource();
            var targetUnchanged = GetTwoStepTarget();

            mockCollectionRemover.Setup(icr => icr.RemoveEntitiesAsync<Second, int>(
                It.Is<ICollection<Second>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), targetUnchanged.Seconds.Select(j => j.Id))),
                It.Is<ICollection<Second>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), source.Seconds.Select(j => j.Id))),
                It.IsAny<Func<Second, int>>()
                )).Returns(Task.FromResult(0)).Verifiable();
            mockCollectionRemover.Setup(icr => icr.RemoveEntitiesAsync<Third, int>(
                It.Is<ICollection<Third>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), targetUnchanged.Seconds.ElementAt(0).Thirds.Select(j => j.Id))),
                It.Is<ICollection<Third>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), source.Seconds.ElementAt(0).Thirds.Select(j => j.Id))),
                It.IsAny<Func<Third, int>>()
                )).Returns(Task.FromResult(0)).Verifiable();
            mockCollectionRemover.Setup(icr => icr.RemoveEntitiesAsync<Third, int>(
                It.Is<ICollection<Third>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), new int[0])),
                It.Is<ICollection<Third>>(coll => CollectionComparer.HaveSameElements(coll.Select(j => j.Id), source.Seconds.ElementAt(1).Thirds.Select(j => j.Id))),
                It.IsAny<Func<Third, int>>()
                )).Returns(Task.FromResult(0)).Verifiable();

            var tasks = action(source, target);
            foreach (var task in tasks)
            {
                task().Wait();
            }

            Assert.AreEqual(1, target.Id);
            Assert.AreEqual("name_changed", target.Name);
            Assert.AreEqual(2, target.Seconds.Count);
            Assert.AreEqual(1, target.Seconds.ElementAt(0).Id);
            Assert.AreEqual("1", target.Seconds.ElementAt(0).Type);
            Assert.AreEqual(2, target.Seconds.ElementAt(0).Thirds.Count);
            Assert.AreEqual(11, target.Seconds.ElementAt(0).Thirds.ElementAt(0).Id);
            Assert.AreEqual("11", target.Seconds.ElementAt(0).Thirds.ElementAt(0).Type);
            Assert.AreEqual(13, target.Seconds.ElementAt(0).Thirds.ElementAt(1).Id);
            Assert.AreEqual("13", target.Seconds.ElementAt(0).Thirds.ElementAt(1).Type);
            Assert.AreEqual(3, target.Seconds.ElementAt(1).Id);
            Assert.AreEqual("3", target.Seconds.ElementAt(1).Type);
            Assert.AreEqual(1, target.Seconds.ElementAt(1).Thirds.Count);
            Assert.AreEqual(31, target.Seconds.ElementAt(1).Thirds.ElementAt(0).Id);
            Assert.AreEqual("31", target.Seconds.ElementAt(1).Thirds.ElementAt(0).Type);

            mockCollectionRemover.VerifyAll();
        }

        [TestMethod]
        public void MakeActionWithInvalidObjectOnPath()
        {
            var mockCollectionRemover = new Mock<ICollectionRemover>(MockBehavior.Strict);
            var factory = new UpdateEntityActionFactory(
                StaticHelpers.UpdateObjectActionFactory,
                new EntityUpdateManager(
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.EntityTypesRetriever,
                    EntityStaticHelpers.EntityTypesHelper,
                    mockCollectionRemover.Object,
                    EntityStaticHelpers.KeyExpressionBuilder));
            var paths = new Expression<Func<RegisteredEntity, object>>[]
                {
                    e => e.Unregistered,
                };
            ExceptAssert.Throws<InvalidOperationException>(
                () => factory.MakeUpdateAction(paths),
                ex => ex.InnerException is UnableToBuildActionException
                    && ex.Message.Contains(typeof(IEntityTypesRetriever).Name)
                    && ex.Message.Contains(typeof(IEntityUpdateManager).Name));
        }

        [TestMethod]
        public void MakeAsyncActionWithInvalidObjectOnPath()
        {
            var mockCollectionRemover = new Mock<ICollectionRemover>(MockBehavior.Strict);
            var factory = new UpdateEntityActionFactory(
                StaticHelpers.UpdateObjectActionFactory,
                new EntityUpdateManager(
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.EntityTypesRetriever,
                    EntityStaticHelpers.EntityTypesHelper,
                    mockCollectionRemover.Object,
                    EntityStaticHelpers.KeyExpressionBuilder));
            var paths = new Expression<Func<RegisteredEntity, object>>[]
                {
                    e => e.Unregistered,
                };
            ExceptAssert.Throws<InvalidOperationException>(
                () => factory.MakeAsyncUpdateAction(paths),
                ex => ex.InnerException is UnableToBuildActionException
                    && ex.Message.Contains(typeof(IEntityTypesRetriever).Name)
                    && ex.Message.Contains(typeof(IEntityUpdateManager).Name));
        }

        [TestMethod]
        public void MakeActionWithInvalidCollectionOnPath()
        {
            var mockCollectionRemover = new Mock<ICollectionRemover>(MockBehavior.Strict);
            var factory = new UpdateEntityActionFactory(
                StaticHelpers.UpdateObjectActionFactory,
                new EntityUpdateManager(
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.EntityTypesRetriever,
                    EntityStaticHelpers.EntityTypesHelper,
                    mockCollectionRemover.Object,
                    EntityStaticHelpers.KeyExpressionBuilder));
            var paths = new Expression<Func<RegisteredEntity, object>>[]
                {
                    e => e.UnregisteredCollection,
                };
            ExceptAssert.Throws<InvalidOperationException>(
                () => factory.MakeUpdateAction(paths),
                ex => ex.InnerException is UnableToBuildActionException
                    && ex.Message.Contains(typeof(IEntityTypesRetriever).Name)
                    && ex.Message.Contains(typeof(IEntityUpdateManager).Name));
        }

        [TestMethod]
        public void MakeAsyncActionWithInvalidCollectionOnPath()
        {
            var mockCollectionRemover = new Mock<ICollectionRemover>(MockBehavior.Strict);
            var factory = new UpdateEntityActionFactory(
                StaticHelpers.UpdateObjectActionFactory,
                new EntityUpdateManager(
                    EntityStaticHelpers.MemberInfo,
                    EntityStaticHelpers.EntityTypesRetriever,
                    EntityStaticHelpers.EntityTypesHelper,
                    mockCollectionRemover.Object,
                    EntityStaticHelpers.KeyExpressionBuilder));
            var paths = new Expression<Func<RegisteredEntity, object>>[]
                {
                    e => e.UnregisteredCollection,
                };
            ExceptAssert.Throws<InvalidOperationException>(
                () => factory.MakeAsyncUpdateAction(paths),
                ex => ex.InnerException is UnableToBuildActionException
                    && ex.Message.Contains(typeof(IEntityTypesRetriever).Name)
                    && ex.Message.Contains(typeof(IEntityUpdateManager).Name));
        }

        public class RegisteredEntity
        {
            public int Id { get; set; }
            public UnregisteredEntity Unregistered { get; set; }
            public ICollection<UnregisteredEntity> UnregisteredCollection { get; set; }
        }
        public class UnregisteredEntity
        {
            public int Id { get; set; }
        }
    }
}
