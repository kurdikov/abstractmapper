﻿namespace TsSoft.ContextWrapper
{
    using JetBrains.Annotations;
    using System;

    /// <summary>
    /// Обёртка для хранилища
    /// </summary>
    public interface IItemWrapper : IDisposable
    {
        /// <summary>
        /// Ключ объекта
        /// </summary>
        string Key { get; set; }

        /// <summary>
        /// Инициализировать объект в хранилище
        /// </summary>
        void Initialize();

        /// <summary>
        /// Инициализировано ли хранилище
        /// </summary>
        bool IsInitialized { get; }

        /// <summary>
        /// Создано ли значение в хранилище
        /// </summary>
        bool IsValueCreated { get; }
    }

    /// <summary>
    /// Обёртка для хранилища
    /// </summary>
    /// <typeparam name="T">Тип хранимого объекта</typeparam>
    public interface IItemWrapper<out T> : IItemWrapper
        where T : class
    {
        /// <summary>
        /// Хранимый объект
        /// </summary>
        /// <exception cref="InvalidOperationException">Хранилище не инициализировано</exception>
        [NotNull]
        T Current { get; }
    }
}
