﻿namespace TsSoft.AbstractMapper.DependencyChecker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using Ninject.Modules;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.Helpers;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using Ninject;
    using Ninject.Activation;
    using Ninject.Activation.Caching;
    using Ninject.Parameters;
    using Ninject.Planning;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Проверяет разрешимость зависимостей абстрактных мапперов
    /// </summary>
    public static class AbstractMapperDependenciesChecker
    {
        /// <summary>
        /// Получить маппер из контекста ядра
        /// </summary>
        /// <param name="kernel">Ninject-ядро</param>
        /// <param name="interfaceType">Тип интерфейса маппера</param>
        /// <remarks>Маппер имеется в ядре, если его интерфейс уже запрашивался. В противном случае метод возвращает null.</remarks>
        [CanBeNull]
        public static object GetFromContext([NotNull]IKernel kernel, [NotNull]Type interfaceType)
        {
            var request = kernel.CreateRequest(interfaceType, null, Enumerable.Empty<IParameter>(), false, false)
                .ThrowIfNull("request");
            if (!kernel.CanResolve(request))
            {
                return null;
            }
            var bindings = kernel.GetBindings(request.Service).ThrowIfNull("bindings");
            var components = kernel.Components.ThrowIfNull("components");
            var cache = components.Get<ICache>().ThrowIfNull("cache");
            var context = new Context(kernel, request, bindings.First(), cache, components.Get<IPlanner>(), components.Get<IPipeline>());
            return cache.TryGet(context);
        }

        /// <summary>
        /// Имеется ли маппер в контексте ядра
        /// </summary>
        /// <param name="kernel">Ninject-ядро</param>
        /// <param name="interfaceType">Тип интерфейса маппера</param>
        /// <remarks>Маппер имеется в ядре, если его интерфейс уже запрашивался</remarks>
        public static bool HasInContext([NotNull] IKernel kernel, [NotNull] Type interfaceType)
        {
            return GetFromContext(kernel, interfaceType) != null;
        }

        /// <summary>
        /// Проверить разрешимость зависимостей мапперов, которые уже были запрошены из ядра ранее
        /// </summary>
        /// <param name="mapperTypes">Типы мапперов</param>
        /// <param name="kernel">Ядро</param>
        public static IEnumerable<string> CheckAlreadyRequestedMappers([NotNull]IEnumerable<Type> mapperTypes, [NotNull]IKernel kernel)
        {
            var checker = kernel.Get<IMapperDependenciesChecker>().ThrowIfNull("checker");
            var requestedMapperInstances = mapperTypes
                .Where(t => t != null)
                .Select(t => GetFromContextByClass(kernel, t))
                .Where(i => i != null);
            var unresolved = checker.GetUnresolved<ActivationException>(requestedMapperInstances);
            return unresolved.Select(Stringify);
        }

        private static object GetFromContextByClass([NotNull]IKernel kernel, [NotNull]Type mapperType)
        {
            var interfaces = mapperType.GetInterfaces()
                .Where(i => i != null && i.IsGenericType && typeof(IMapper<,>).IsAssignableFrom(i.GetGenericTypeDefinition()));
            return interfaces
                .Where(i => i != null)
                .Select(i => GetFromContext(kernel, i))
                .FirstOrDefault(c => c != null);
        }

        /// <summary>
        /// Проверить разрешимость зависимостей мапперов из заданной модулем сборки, которые уже были запрошены из ядра ранее
        /// </summary>
        /// <typeparam name="T">Тип модуля, определяющий сборку</typeparam>
        /// <param name="kernel">Ядро</param>
        public static IEnumerable<string> CheckAlreadyRequestedMappersFromAssemblyWithModule<T>([NotNull]IKernel kernel) where T : INinjectModule
        {
            return CheckAlreadyRequestedMappersFromAssemblyWithModule<T>(kernel, null);
        }

        /// <summary>
        /// Проверить разрешимость зависимостей мапперов из заданной модулем сборки, которые уже были запрошены из ядра ранее
        /// </summary>
        /// <typeparam name="T">Тип модуля, определяющий сборку</typeparam>
        /// <param name="kernel">Ядро</param>
        /// <param name="typesRetriever">Получатель типов сборки</param>
        public static IEnumerable<string> CheckAlreadyRequestedMappersFromAssemblyWithModule<T>([NotNull]IKernel kernel, IAssemblyTypesRetriever typesRetriever) where T : INinjectModule
        {
            return CheckAlreadyRequestedMappersFromAssemblyWithType(kernel, typeof(T), typesRetriever);
        }

        /// <summary>
        /// Проверить разрешимость зависимостей мапперов из заданной типом сборки, которые уже были запрошены из ядра ранее
        /// </summary>
        /// <param name="kernel">Ядро</param>
        /// <param name="type">Тип, сборка которого проверяется</param>
        public static IEnumerable<string> CheckAlreadyRequestedMappersFromAssemblyWithType([NotNull]IKernel kernel, [NotNull]Type type)
        {
            return CheckAlreadyRequestedMappersFromAssemblyWithType(kernel, type, null);
        }

        /// <summary>
        /// Проверить разрешимость зависимостей мапперов из заданной типом сборки, которые уже были запрошены из ядра ранее
        /// </summary>
        /// <param name="kernel">Ядро</param>
        /// <param name="type">Тип, сборка которого проверяется</param>
        /// <param name="typesRetriever">Получатель типов сборки</param>
        public static IEnumerable<string> CheckAlreadyRequestedMappersFromAssemblyWithType([NotNull]IKernel kernel, [NotNull]Type type, IAssemblyTypesRetriever typesRetriever)
        {
            if (kernel == null)
            {
                throw new ArgumentNullException("kernel");
            }
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }
            var assembly = type.Assembly;
            var mapperTypesRetriever = kernel.Get<IMapperTypesRetriever>().ThrowIfNull("typeRetriever");
            var mapperTypes = mapperTypesRetriever.GetMapperTypes(assembly, typesRetriever ?? new AssemblyTypesRetriever());
            return CheckAlreadyRequestedMappers(mapperTypes, kernel);
        }

        private static string Stringify([NotNull]IUnresolvedDependency unresolved)
        {
            return (unresolved.DependencyFromType != null)
                ? string.Format("Error resolving inner mappers for {0} : mapping from {1} to {2}, exception : {3}",
                    unresolved.MapperType,
                    unresolved.DependencyFromType,
                    unresolved.DependencyToType,
                    unresolved.Exception)
                : string.Format("Error processing {0} : {1}", unresolved.MapperType, unresolved.Exception);
        }
    }
}
