﻿namespace TsSoft.ContextWrapper.Web
{
    using System;
    using System.Web;
    using JetBrains.Annotations;

    /// <summary>
    /// Хранилище объектов с использованием HttpContext и CallContext
    /// </summary>
    public class HttpContextStore : CallContextStore
    {
        private const string StoreName = "HttpContextStore";

        /// <summary>
        /// Получить объект по ключу
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="key">Ключ</param>
        public override T Get<T>(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            return HttpContext.Current != null
                ? GetFromContext(key, GetFromHttpContext<ILazy<T>>, StoreName)
                : base.Get<T>(key);
        }

        /// <summary>
        /// Подготовить хранилище для нового контекста
        /// </summary>
        public override void NewContext()
        {
            HttpContext.Current = null;
            base.NewContext();
        }

        /// <summary>
        /// Инициализировать хранилище для объекта
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        /// <param name="contextElement">Элемент контекста с производящей объект функцией</param>
        protected override void Initialize(string key, IDisposableLazy<object> contextElement)
        {
            if (HttpContext.Current != null)
            {
                PutIntoContext(key, contextElement, PutIntoHttpContext, StoreName, Keys);
            }
            base.Initialize(key, contextElement);
        }

        /// <summary>
        /// Вызвать Dispose для объекта из хранилища
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public override void DisposeIfCreated(string key)
        {
            if (HttpContext.Current != null)
            {
                DisposeByKey(key, GetFromHttpContext<IDisposable>, StoreName);
            }
            base.DisposeIfCreated(key);
        }

        /// <summary>
        /// Вызвать Dispose для всех объектов из хранилища
        /// </summary>
        public override void DisposeAll()
        {
            Dispose();
        }

        /// <summary>
        /// Очистить хранилище
        /// </summary>
        public override void Clear()
        {
            ClearContext(Keys, DeleteFromHttpContext);
            base.Clear();
        }

        private static T GetFromHttpContext<T>([NotNull]string key)
            where T: class
        {
            if (HttpContext.Current == null)
            {
                throw new NullReferenceException("HttpContext.Current is null");
            }
            return HttpContext.Current.Items[key] as T;
        }

        private static void PutIntoHttpContext<T>([NotNull]string key, [NotNull]T item)
        {
            if (HttpContext.Current == null)
            {
                throw new NullReferenceException("HttpContext.Current is null");
            }
            HttpContext.Current.Items[key] = item;
        }

        private static void DeleteFromHttpContext([NotNull] string key)
        {
            if (HttpContext.Current == null)
            {
                throw new NullReferenceException("HttpContext.Current is null");
            }
            HttpContext.Current.Items.Remove(key);
        }

        /// <summary>
        /// Вызвать Dispose для всех объектов хранилища
        /// </summary>
        public new static void Dispose()
        {
            if (HttpContext.Current != null)
            {
                Dispose(Keys, GetFromHttpContext<IDisposable>, StoreName);
            }
            CallContextStore.Dispose();
        }

        /// <summary>
        /// Инициализировано ли хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public override bool IsInitialized<T>(string key)
        {
            if (HttpContext.Current != null)
            {
                return IsInitialized(key, GetFromHttpContext<ILazy<T>>);
            }
            return base.IsInitialized<T>(key);
        }

        /// <summary>
        /// Создано ли значение в хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public override bool IsValueCreated<T>(string key)
        {
            if (HttpContext.Current != null)
            {
                return IsValueCreated(key, GetFromHttpContext<ILazy<T>>);
            }
            return base.IsValueCreated<T>(key);
        }
    }
}
