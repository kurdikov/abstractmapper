﻿namespace TsSoft.AbstractMapper.Helpers
{
    using System;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;

    class MapperMethodHelper : IMapperMethodHelper
    {
        [NotNull]
        private readonly string _methodName;

        public MapperMethodHelper([NotNull]IMemberInfoHelper memberInfo)
        {
            if (memberInfo == null) throw new ArgumentNullException("memberInfo");
            _methodName = memberInfo.GetMethodInfo((IMapper<object, object> m) => m.Map(null)).Name;
        }

        public MethodInfo GetMapMethod(Type mapperType)
        {
            var method = mapperType.GetTypeInfo().GetMethod(_methodName);
            if (method == null)
            {
                throw new InvalidOperationException(string.Format("No {0}() method found in {1}", _methodName, mapperType));
            }
            return method;
        }
    }
}
