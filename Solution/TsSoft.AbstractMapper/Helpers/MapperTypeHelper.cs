﻿namespace TsSoft.AbstractMapper.Helpers
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Определяет, является ли нечто абстрактным маппером
    /// </summary>
    public class MapperTypeHelper : IMapperTypeHelper
    {
        /// <summary>
        /// Является ли тип абстрактным маппером
        /// </summary>
        public bool IsAbstractMapperType(Type mapperType)
        {
            return GetMapperBaseType(mapperType) != null;
        }

        /// <summary>
        /// Является ли объект абстрактным маппером
        /// </summary>
        public bool IsAbstractMapperInstance(object mapperInstance)
        {
            return mapperInstance != null && IsAbstractMapperType(mapperInstance.GetType());
        }

        /// <summary>
        /// Получить базовый класс маппера для типа
        /// </summary>
        public Type GetMapperBaseType(Type mapperType)
        {
            var baseType = mapperType;
            while (baseType != typeof(object) && baseType != null)
            {
                var baseTypeInfo = baseType.GetTypeInfo();
                if (baseTypeInfo.IsGenericType && baseType.GetGenericTypeDefinition() == typeof(Mapper<,>))
                {
                    return baseType;
                }
                baseType = baseTypeInfo.BaseType;
            }
            return null;
        }
    }
}
