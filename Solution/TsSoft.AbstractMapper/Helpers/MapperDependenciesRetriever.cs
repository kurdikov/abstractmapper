﻿namespace TsSoft.AbstractMapper.Helpers
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Engine;

    class MapperDependenciesRetriever : IMapperDependenciesRetriever
    {
        [NotNull] private readonly IMapExpressionCreator _expressionCreator;

        public MapperDependenciesRetriever([NotNull] IMapExpressionCreator expressionCreator)
        {
            if (expressionCreator == null) throw new ArgumentNullException("expressionCreator");
            _expressionCreator = expressionCreator;
        }

        public IEnumerable<KeyValuePair<Type, Type>> GetDependencies<TFrom, TTo>(Mapper<TFrom, TTo> mapper)
            where TFrom : class
            where TTo : new()
        {
            var mapRules = mapper.FullMapRules;
            var paths = _expressionCreator.GetExpression<TFrom, TTo>(mapRules, false, null).MappedPaths;
            foreach (var path in paths)
            {
                if (path != null && path.MapperDescription != null)
                {
                    yield return new KeyValuePair<Type, Type>(
                        path.MapperDescription.MapperFromType,
                        path.MapperDescription.MapperToType);
                }
            }
        }
    }
}
