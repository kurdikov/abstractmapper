﻿namespace TsSoft.AbstractMapper.Helpers
{
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Создаёт выражения для извлечения значения
    /// </summary>
    public interface IGetterAccessHelper
    {
        /// <summary>
        /// Создаёт выражения для извлечения значения из TFrom при заполнении заданного свойства с описанием использованного пути
        /// </summary>
        /// <param name="from">Аргумент выражения типа TFrom</param>
        /// <param name="propertyInfo">Заполняемое свойство</param>
        PathDescription GetGetterAccess<TFrom>(
            [NotNull]ParameterExpression from,
            [NotNull]PropertyInfo propertyInfo);
    }
}
