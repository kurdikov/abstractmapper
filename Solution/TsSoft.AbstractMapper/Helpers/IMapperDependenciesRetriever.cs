﻿namespace TsSoft.AbstractMapper.Helpers
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Получает мапперы, от которых зависит экземпляр абстрактного маппера
    /// </summary>
    public interface IMapperDependenciesRetriever
    {
        /// <summary>
        /// Получить интерфейсы мапперов, от которых зависит данный экземпляр
        /// </summary>
        /// <typeparam name="TFrom">Входной тип рассматриваемого маппера</typeparam>
        /// <typeparam name="TTo">Выходной тип рассматриваемого маппера</typeparam>
        /// <param name="mapper">Экземпляр маппера</param>
        /// <returns>Перечисление пар типов (откуда, куда) для мапперов, которые нужны для работы представленному экземпляру</returns>
        [NotNull]
        IEnumerable<KeyValuePair<Type, Type>> GetDependencies<TFrom, TTo>([NotNull]Mapper<TFrom, TTo> mapper)
            where TFrom : class
            where TTo : new();
    }
}
