﻿namespace TsSoft.AbstractMapper.Helpers
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Определяет, является ли нечто абстрактным маппером
    /// </summary>
    public interface IMapperTypeHelper
    {
        /// <summary>
        /// Является ли тип типом абстрактного маппера
        /// </summary>
        bool IsAbstractMapperType([NotNull] Type mapperType);

        /// <summary>
        /// Является ли объект абстрактным маппером
        /// </summary>
        bool IsAbstractMapperInstance(object mapperInstance);

        /// <summary>
        /// Получить базовый тип маппера, если mapperType - тип абстрактного маппера, и null в противном случае
        /// </summary>
        Type GetMapperBaseType([NotNull] Type mapperType);
    }
}
