namespace TsSoft.AbstractMapper.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;

    class MapperTypesRetriever : IMapperTypesRetriever
    {
        [NotNull] private readonly IMapperTypeHelper _mapperTypeHelper;

        public MapperTypesRetriever([NotNull] IMapperTypeHelper mapperTypeHelper)
        {
            if (mapperTypeHelper == null) throw new ArgumentNullException("mapperTypeHelper");
            _mapperTypeHelper = mapperTypeHelper;
        }

        public IEnumerable<Type> GetMapperTypes(Assembly assembly, IAssemblyTypesRetriever assemblyTypesRetriever = null)
        {
            return assemblyTypesRetriever.SafeGetTypes(assembly)
                .Where(t => t != null
                    && !t.GetTypeInfo().IsAbstract
                    && t.GetTypeInfo().GetInterfaces().Any(i => i != null
                                                  && i.GetTypeInfo().IsGenericType
                                                  && i.GetGenericTypeDefinition() == typeof(IMapper<,>))
                    && _mapperTypeHelper.IsAbstractMapperType(t));
        }
    }
}
