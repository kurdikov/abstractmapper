﻿namespace TsSoft.AbstractMapper.Helpers
{
    using System;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Получает метод интерфейса маппера
    /// </summary>
    public interface IMapperMethodHelper
    {
        /// <summary>
        /// Получить метод Map() по типу
        /// </summary>
        /// <param name="mapperType">Тип маппера</param>
        [NotNull]
        MethodInfo GetMapMethod([NotNull]Type mapperType);
    }
}
