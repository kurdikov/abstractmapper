namespace TsSoft.AbstractMapper.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;

    class MapperDependenciesChecker : IMapperDependenciesChecker
    {
        [NotNull] private readonly IMapperDependenciesRetriever _dependenciesRetriever;
        [NotNull] private readonly IMapperResolver _mapperResolver;
        [NotNull] private readonly IMapperTypeHelper _mapperTypeHelper;

        [NotNull] private readonly MethodInfo _mapperResolverTryGet;
        [NotNull] private readonly MethodInfo _checkMapperInstance;

        public MapperDependenciesChecker(
            [NotNull] IMapperDependenciesRetriever dependenciesRetriever,
            [NotNull] IMapperResolver mapperResolver,
            [NotNull] IMemberInfoHelper memberInfoHelper, 
            [NotNull] IMapperTypeHelper mapperTypeHelper)
        {
            if (dependenciesRetriever == null) throw new ArgumentNullException("dependenciesRetriever");
            if (mapperResolver == null) throw new ArgumentNullException("mapperResolver");
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (mapperTypeHelper == null) throw new ArgumentNullException("mapperTypeHelper");
            _dependenciesRetriever = dependenciesRetriever;
            _mapperResolver = mapperResolver;
            _mapperTypeHelper = mapperTypeHelper;

            _mapperResolverTryGet = memberInfoHelper.GetGenericDefinitionMethodInfo(() => mapperResolver.TryGet<object, object>());
            _checkMapperInstance = memberInfoHelper.GetGenericDefinitionMethodInfo(
                () => CheckMapperInstance<object, object, Exception>(null, null));
        }
        
        public IEnumerable<UnresolvedDependency> GetUnresolved<TResolverException>(
            IEnumerable<object> mapperInstances)
            where TResolverException: Exception
        {
            var checkedTypes = new HashSet<Type>();
            return mapperInstances
                .Where(m => m != null)
                .SelectMany(m =>
                    CheckMapperInstance<TResolverException>(m, checkedTypes)
                    ?? Enumerable.Empty<UnresolvedDependency>());
        }

        private IEnumerable<UnresolvedDependency> CheckMapperInstance<TResolverException>(
            [NotNull]object mapper,
            [NotNull]HashSet<Type> checkedTypes)
            where TResolverException : Exception
        {
            if (!checkedTypes.Add(mapper.GetType()))
            {
                return Enumerable.Empty<UnresolvedDependency>();
            }
            var baseType = _mapperTypeHelper.GetMapperBaseType(mapper.GetType());
            if (baseType == null)
            {
                return Enumerable.Empty<UnresolvedDependency>();
            }
            var genericArguments = baseType.GetTypeInfo().GetGenericArguments();
            var fromType = genericArguments[0].ThrowIfNull("genericArguments[0]");
            var toType = genericArguments[1].ThrowIfNull("genericArguments[1]");
            var checkMapperMethod = _checkMapperInstance.MakeGenericMethod(fromType, toType, typeof(TResolverException));
            var checkMapperDelegate = DelegateCreator.Create<Func<object, HashSet<Type>, IEnumerable<UnresolvedDependency>>>(
                this, checkMapperMethod);
            return checkMapperDelegate(mapper, checkedTypes);
        }

        private IEnumerable<UnresolvedDependency> CheckMapperInstance<TFrom, TTo, TResolverException>(
            [NotNull]object untypedMapperInstance,
            [NotNull]HashSet<Type> checkedTypes)
            where TFrom : class
            where TTo : class, new()
            where TResolverException: Exception
        {
            var mapperInstance = untypedMapperInstance as Mapper<TFrom, TTo>;
            if (mapperInstance == null)
            {
                throw new ArgumentException(string.Format("Expected {0}, got {1}", typeof(Mapper<TFrom, TTo>), untypedMapperInstance));
            }
            var result = new List<UnresolvedDependency>();
            try
            {
                var dependencies = _dependenciesRetriever.GetDependencies(mapperInstance);
                foreach (var dependency in dependencies)
                {
                    var method = _mapperResolverTryGet.MakeGenericMethod(
                        dependency.Key, dependency.Value);
                    var methodDelegate = DelegateCreator.Create<Func<object>>(_mapperResolver, method);
                    try
                    {
                        var dependencyInstance = methodDelegate();
                        if (dependencyInstance != null)
                        {
                            var internalErrors = CheckMapperInstance<TResolverException>(dependencyInstance, checkedTypes);
                            if (internalErrors != null)
                            {
                                result.AddRange(internalErrors);
                            }
                        }
                        else
                        {
                            result.Add(new UnresolvedDependency(mapperInstance.GetType(), dependency.Key, dependency.Value));
                        }
                    }
                    catch (TResolverException ex)
                    {
                        result.Add(new UnresolvedDependency(mapperInstance.GetType(), dependency.Key, dependency.Value, ex));
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add(new UnresolvedDependency(mapperInstance.GetType(), ex));
            }
            return result;
        }
    }
}
