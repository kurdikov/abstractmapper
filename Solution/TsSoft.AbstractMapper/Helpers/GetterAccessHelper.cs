﻿namespace TsSoft.AbstractMapper.Helpers
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.Models.Path;

    internal class GetterAccessHelper : IGetterAccessHelper
    {
        public PathDescription GetGetterAccess<TFrom>(
            ParameterExpression from,
            PropertyInfo propertyInfo)
        {
            PathDescription pathDescription = null;
            var member = ValueHoldingMember.GetValueHoldingMember(typeof(TFrom), propertyInfo.Name);
            if (member != null)
            {
                pathDescription = new PathDescription
                {
                    Path = new ParsedPath (elements: new List<PathElement>(1) { new PathElement(member) }, start: from),
                    Expression = Expression.MakeMemberAccess(from, member.Member),
                };
            }
            return pathDescription;
        }
    }
}
