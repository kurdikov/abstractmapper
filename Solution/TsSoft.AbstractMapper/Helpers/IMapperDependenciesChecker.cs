﻿namespace TsSoft.AbstractMapper.Helpers
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;

    //public interface IMapperProcessor
    //{
    //    void Process<TFrom, TTo>(Mapper<TFrom, TTo> mapper)
    //        where TFrom : class;
    //}

    //public interface IMapperProcessor<out TResult>
    //{
    //    TResult Process<TFrom, TTo>(Mapper<TFrom, TTo> mapper)
    //        where TFrom : class;
    //}

    //public interface IMapperProcessorHelper
    //{
    //    void ProcessAllMappers([NotNull]IMapperProcessor processor);

    //    void ProcessAllMappers([NotNull]IMapperProcessor processor, IEnumerable<object> mapperInstances);

    //    IEnumerable<TResult> ProcessAllMappers<TResult>([NotNull]IMapperProcessor<TResult> processor);
    //}

    //public class MapperProcessorHelper : IMapperProcessorHelper
    //{
    //    public void ProcessAllMappers(IMapperProcessor processor)
    //    {
            
    //    }

    //    public IEnumerable<TResult> ProcessAllMappers<TResult>(IMapperProcessor<TResult> processor)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    /// <summary>
    /// Проверяет зависимости абстрактных мапперов
    /// </summary>
    public interface IMapperDependenciesChecker
    {
        /// <summary>
        /// Получить зависимости, которые не удалось разрешить
        /// </summary>
        /// <param name="mapperInstances">Экземпляры мапперов, зависимости которых проверяются</param>
        [NotNull]
        IEnumerable<UnresolvedDependency> GetUnresolved<TResolverException>(
            [NotNull]IEnumerable<object> mapperInstances)
            where TResolverException: Exception;
    }
}
