﻿namespace TsSoft.AbstractMapper.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Получает типы мапперов из сборки
    /// </summary>
    public interface IMapperTypesRetriever
    {
        /// <summary>
        /// Получить типы неабстрактных наследников <see cref="Mapper{TFrom,TTo}"/> из сборки
        /// </summary>
        /// <param name="assembly">Сборка</param>
        /// <param name="assemblyTypesRetriever">Получатель типов из сборки</param>
        [NotNull]
        IEnumerable<Type> GetMapperTypes([NotNull]Assembly assembly, IAssemblyTypesRetriever assemblyTypesRetriever = null);
    }
}
