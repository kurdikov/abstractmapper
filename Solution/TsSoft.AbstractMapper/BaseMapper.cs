﻿namespace TsSoft.AbstractMapper
{
    using JetBrains.Annotations;

    /// <summary>
    /// Null-безопасный маппер
    /// </summary>
    /// <typeparam name="TFrom">Тип, из которого происходит преобразование</typeparam>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    public abstract class BaseMapper<TFrom, TTo> : IMapper<TFrom, TTo>
        where TFrom : class
    {
        /// <summary>
        /// Преобразовать экземпляр TFrom в экземпляр TTo
        /// </summary>
        public virtual TTo Map(TFrom @from)
        {
            return @from == null ? default(TTo) : RealMap(@from);
        }

        /// <summary>
        /// Преобразовать существующий экземпляр TFrom в экземпляр TTo
        /// </summary>
        protected abstract TTo RealMap([NotNull]TFrom @from);
    }
}
