﻿namespace TsSoft.AbstractMapper.Bindings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;

    /// <summary>
    /// Ищет реализации интерфейсов мапперов
    /// </summary>
    public static class MapperImplementationFinder
    {
        /// <summary>
        /// Интерфейс и его реализация
        /// </summary>
        private class InterfaceImplementation
        {
            /// <summary>
            /// Тип-интерфейс
            /// </summary>
            [NotNull]
            public Type Interface { get; set; }
            /// <summary>
            /// Тип-реализация
            /// </summary>
            [NotNull]
            public Type Type { get; set; }
        }

        /// <summary>
        /// Биндинг
        /// </summary>
        private class NamedInterfaceImplementation : InterfaceImplementation
        {
            /// <summary>
            /// Имя биндинга
            /// </summary>
            public string Name { get; set; }
        }


        internal class KernelState
        {
            public KernelState()
            {
                BoundInterfaces = new HashSet<Type>();
                AllowedMultipleInterfaces = new HashSet<Type>();
            }

            [NotNull]
            public HashSet<Type> BoundInterfaces { get; set; }
            [NotNull]
            public HashSet<Type> AllowedMultipleInterfaces { get; set; }
        }

        /// <summary>
        /// Получить реализации генерик-интерфейса
        /// </summary>
        /// <param name="classes">Классы, в которых ищутся реализации</param>
        /// <param name="genericInterface">Интерфейс (GenericTypeDefinition)</param>
        /// <returns>Набор реализаций</returns>
        [NotNull]
        private static IEnumerable<IGrouping<Type, InterfaceImplementation>> GetImplementations(
            [NotNull]IEnumerable<Type> classes,
            [NotNull]Type genericInterface)
        {
            return classes.Where(t => t != null)
                    .SelectMany(t => t.GetTypeInfo().GetInterfaces()
                        .Where(i => i != null && i.GetTypeInfo().IsGenericType && genericInterface == i.GetGenericTypeDefinition())
                        .Select(i => new InterfaceImplementation
                        {
                            Type = t,
                            Interface = i
                        }))
                    .Where(ti => ti != null)
                    .GroupBy(ti => ti.Interface);
        }

        /// <summary>
        /// Добавить биндинг
        /// </summary>
        /// <param name="bindings">Коллекция биндингов</param>
        /// <param name="inter">Интерфейс</param>
        /// <param name="impl">Реализация</param>
        /// <param name="state">Состояние ядра</param>
        private static void Bind([NotNull]ICollection<NamedInterfaceImplementation> bindings, [NotNull]Type inter, [NotNull]Type impl, [NotNull]KernelState state)
        {
            var implInfo = impl.GetTypeInfo();
            var attr = implInfo.GetCustomAttribute<NamedBindingAttribute>();
            var allowMultiple = implInfo.IsDefined(typeof(AllowMultipleBindingAttribute));
            if (!state.BoundInterfaces.Add(inter) && attr == null && (!allowMultiple || !state.AllowedMultipleInterfaces.Contains(inter)))
            {
                throw new MapperBinderException(string.Format(
                    "There are more than one implementation of {0} and no NinjectNamedAttribute is provided for {1}",
                    inter,
                    impl));
            }
            if (allowMultiple)
            {
                state.AllowedMultipleInterfaces.Add(inter);
            }
            bindings.Add(new NamedInterfaceImplementation { Interface = inter, Type = impl, Name = attr == null ? null : attr.Name });
        }

        /// <summary>
        /// Добавить биндинг интерфейса на генерик-класс
        /// </summary>
        /// <param name="bindings">Коллекция биндингов</param>
        /// <param name="interGeneric">Генерик-интерфейс маппера (GenericTypeDefinition)</param>
        /// <param name="implGeneric">Генерик-реализация</param>
        /// <param name="fromParameter">Откуда мапить</param>
        /// <param name="toParameter">Куда мапить</param>
        /// <param name="state">Состояние ядра</param>
        private static void BindGeneric(
            [NotNull]ICollection<NamedInterfaceImplementation> bindings,
            [NotNull]Type interGeneric,
            [NotNull]Type implGeneric,
            [NotNull]Type fromParameter,
            [NotNull]Type toParameter,
            [NotNull]KernelState state)
        {
            var constructedType = implGeneric.MakeGenericType(toParameter);
            var constructedInterface = interGeneric.MakeGenericType(fromParameter, toParameter);
            Bind(bindings, constructedInterface, constructedType, state);
        }

        /// <summary>
        /// Забиндить классы мапперов, не являющиеся генериками
        /// </summary>
        /// <param name="bindings">Коллекция биндингов</param>
        /// <param name="allSelfBindableTypes">Классы, не являющиеся генериками, среди которых нужно найти те, что можно забиндить</param>
        /// <param name="genericInterface">Генерик-интерфейс маппера (GenericTypeDefinition)</param>
        /// <param name="state">Состояние ядра</param>
        private static void BindNonGenericMappers(
            [NotNull] ICollection<NamedInterfaceImplementation> bindings,
            [NotNull] IReadOnlyCollection<Type> allSelfBindableTypes,
            [NotNull] Type genericInterface,
            [NotNull] KernelState state)
        {
            var selfBindableImplementations = GetImplementations(allSelfBindableTypes, genericInterface);
            foreach (var inter in selfBindableImplementations)
            {
                if (inter == null)
                {
                    continue;
                }
                foreach (var impl in inter)
                {
                    if (impl == null)
                    {
                        continue;
                    }
                    Bind(bindings, impl.Interface, impl.Type, state);
                }
            }
        }

        /// <summary>
        /// Забиндить генерик-класс маппера, у которого констрейнт TTo - некий класс, на маппинг в этот класс и те классы, что указаны в его <see cref="ExtendedModelsMappedAttribute"/>
        /// </summary>
        /// <param name="bindings">Коллекция биндингов</param>
        /// <param name="genericInterface">Интерфейс маппера (GenericTypeDefinition)</param>
        /// <param name="impl">Генерик-реализация</param>
        /// <param name="interfaceArgument">Первый аргумент интерфейса (TFrom)</param>
        /// <param name="constraint">Констрейнт на генерик-реализации</param>
        /// <param name="state">Состояние ядра</param>
        private static void BindMapperToConstraintAndExtended(
            [NotNull] ICollection<NamedInterfaceImplementation> bindings,
            [NotNull] Type genericInterface,
            [NotNull] InterfaceImplementation impl,
            [NotNull] Type interfaceArgument,
            [NotNull] Type constraint,
            [NotNull] KernelState state)
        {
            BindGeneric(bindings, genericInterface, impl.Type, interfaceArgument, constraint, state);
            var attr = impl.Type.GetTypeInfo().GetCustomAttribute<ExtendedModelsMappedAttribute>();
            if (attr != null && attr.ModelTypes != null)
            {
                var constraintInfo = constraint.GetTypeInfo();
                foreach (var extended in attr.ModelTypes.Where(constraintInfo.IsAssignableFrom))
                {
                    if (extended == null)
                    {
                        continue;
                    }
                    BindGeneric(
                        bindings,
                        genericInterface,
                        impl.Type,
                        interfaceArgument,
                        extended,
                        state);
                }
            }
        }

        /// <summary>
        /// Забиндить генерик-класс маппера, у которого констрейнт TTo - некий интерфейс, на маппинг во все известные реализации этого интерфейса
        /// </summary>
        /// <param name="bindings">Коллекция биндингов</param>
        /// <param name="genericInterface">Генерик-интерфейс маппера (GenericTypeDefinition)</param>
        /// <param name="impl">Генерик-реализация</param>
        /// <param name="allTypes">Известные типы</param>
        /// <param name="interfaceArgument">Первый аргумент интерфейса (TFrom)</param>
        /// <param name="constraint">Констрейнт на генерик-реализации</param>
        /// <param name="state">Состояние ядра</param>
        /// <param name="doNotBindToInterface">Не подставлять сам интерфейс в качестве генерика</param>
        private static void BindMapperToInterfaceImplementations(
            [NotNull] ICollection<NamedInterfaceImplementation> bindings,
            [NotNull] Type genericInterface,
            [NotNull] InterfaceImplementation impl,
            [NotNull] IEnumerable<Type> allTypes,
            [NotNull] Type interfaceArgument,
            [NotNull] Type constraint,
            [NotNull] KernelState state,
            bool doNotBindToInterface)
        {
            foreach (var constraintSatisfaction in allTypes.Where(
                t => (t != constraint || !doNotBindToInterface) && constraint.GetTypeInfo().IsAssignableFrom(t)))
            {
                if (constraintSatisfaction == null)
                {
                    continue;
                }
                BindGeneric(
                    bindings,
                    genericInterface,
                    impl.Type,
                    interfaceArgument,
                    constraintSatisfaction,
                    state);
            }
        }

        /// <summary>
        /// Забиндить генерик-классы
        /// </summary>
        /// <param name="bindings">Коллекция биндингов</param>
        /// <param name="allTypes">Все известные типы</param>
        /// <param name="allGenericClasses">Все известные генерики, среди которых нужно найти мапперы, которые можно забиндить</param>
        /// <param name="genericInterface">Интерфейс маппера (GenericTypeDefinition)</param>
        /// <param name="state">Состояние ядра</param>
        private static void BindGenericMappers(
            [NotNull] ICollection<NamedInterfaceImplementation> bindings,
            [NotNull] IReadOnlyCollection<Type> allTypes,
            [NotNull] IReadOnlyCollection<Type> allGenericClasses,
            [NotNull] Type genericInterface,
            [NotNull] KernelState state)
        {
            var genericImplementations = GetImplementations(allGenericClasses, genericInterface);
            foreach (var inter in genericImplementations)
            {
                if (inter == null)
                {
                    continue;
                }
                foreach (var impl in inter)
                {
                    if (impl == null)
                    {
                        continue;
                    }
                    var arguments = impl.Type.GetTypeInfo().GetGenericArguments();
                    var interfaceArguments = impl.Interface.GetTypeInfo().GetGenericArguments();
                    if (interfaceArguments[0] == null)
                    {
                        continue;
                    }
                    // bind only generic mappers with one parameter, like SpecialMapper<T> : IMapper<X, T>
                    // having specific generic constraint, like "where T: SpecialModel"
                    // if SpecialModel is class, bind IMapper<X, SpecialModel> to SpecialMapper<SpecialModel> only
                    // if SpecialModel is interface, bind IMapper<X, Y> to SpecialMapper<Y> for each Y: SpecialModel
                    if (arguments.Length == 1 && arguments[0] == interfaceArguments[1] && arguments[0] != null)
                    {
                        var constraints = arguments[0].GetTypeInfo().GetGenericParameterConstraints();
                        if (constraints.Length != 1)
                        {
                            continue;   // we bind only mappers having one specific constraint
                        }
                        if (constraints[0] == null)
                        {
                            continue;
                        }
                        var constraintInfo = constraints[0].GetTypeInfo();
                        if (constraintInfo.IsClass)
                        {
                            BindMapperToConstraintAndExtended(
                                bindings, genericInterface, impl, interfaceArguments[0], constraints[0], state);
                        }
                        else if (constraintInfo.IsInterface)
                        {
                            BindMapperToInterfaceImplementations(
                                bindings, genericInterface, impl, allTypes, interfaceArguments[0], constraints[0], state,
                                arguments[0].GetTypeInfo().GenericParameterAttributes.HasFlag(GenericParameterAttributes.DefaultConstructorConstraint));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Получить список реализаций интерфейсов мапперов
        /// </summary>
        /// <param name="types">Все известные типы, среди которых ищутся мапперы</param>
        /// <param name="genericMapperInterfaces">Генерик-интерфейсы мапперов (GenericTypeDefinition-ы)</param>
        /// <param name="bindGeneric">Связывать ли мапперы с генерик-типами</param>
        /// <param name="bindNonGeneric">Связывать ли мапперы с негенерик-типами</param>
        [NotNull]
        public static IReadOnlyCollection<MapperBinding> GetAutoBindings(
            [NotNull] IReadOnlyCollection<Type> types,
            [NotNull] IReadOnlyList<Type> genericMapperInterfaces,
            bool bindGeneric = true,
            bool bindNonGeneric = true)
        {
            var impls = new List<NamedInterfaceImplementation>();
            var allSelfBindableTypes = types
                .Select(t => t?.GetTypeInfo())
                .Where(t => t != null && !t.IsAbstract && !t.IsInterface && !t.IsGenericTypeDefinition)
                .Select(t => t.AsType())
                .ToList();
            var allGenericClasses = types
                .Select(t => t?.GetTypeInfo())
                // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                .Where(t => t != null && !t.IsAbstract && !t.IsInterface && t.IsGenericTypeDefinition)
                .Select(t => t.AsType())
                .ToList();
            var state = new KernelState();
            foreach (var genericInterface in genericMapperInterfaces)
            {
                if (genericInterface == null)
                {
                    continue;
                }
                if (bindNonGeneric)
                {
                    BindNonGenericMappers(impls, allSelfBindableTypes, genericInterface, state);
                }
                if (bindGeneric)
                {
                    BindGenericMappers(impls, types, allGenericClasses, genericInterface, state);
                }
            }
            var groupedByImpl = impls.Where(i => i != null).GroupBy(i => new {i.Type, i.Name});
            var result = new List<MapperBinding>();
            foreach (var group in groupedByImpl)
            {
                if (group == null || group.Key == null)
                {
                    throw new ArgumentNullException();
                }
                result.Add(new MapperBinding(group.Select(g => g.Interface), group.Key.Type, group.Key.Name));
            }
            return result;
        }
    }
}
