namespace TsSoft.AbstractMapper.Bindings
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;

    /// <summary>
    /// �������� ���������� ����������� ������� � �����������
    /// </summary>
    public class MapperBinding
    {
        /// <summary>
        /// ����������
        /// </summary>
        [NotNull]
        public Type[] Interfaces { get; private set; }

        /// <summary>
        /// ����������
        /// </summary>
        [NotNull]
        public Type Implementation { get; private set; }

        /// <summary>
        /// ��� ����������
        /// </summary>
        [CanBeNull]
        public string Name { get; private set; }

        /// <summary>
        /// �������� ���������� ����������� ������� � �����������
        /// </summary>
        public MapperBinding([NotNull] IEnumerable<Type> interfaces, [NotNull] Type implementation, string name)
        {
            if (interfaces == null) throw new ArgumentNullException("interfaces");
            if (implementation == null) throw new ArgumentNullException("implementation");
            Interfaces = interfaces.ToArrayIfNeeded();
            Implementation = implementation;
            Name = name;
        }
    }
}
