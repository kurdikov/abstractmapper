﻿namespace TsSoft.AbstractMapper.Bindings
{
    using System;

    /// <summary>
    /// Разрешить биндинг нескольких классов для одного интерфейса
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class AllowMultipleBindingAttribute : Attribute
    {
    }
}
