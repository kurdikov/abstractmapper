﻿namespace TsSoft.AbstractMapper.Bindings
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Отметить маппер для именованного связывания
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class NamedBindingAttribute : Attribute
    {
        /// <summary>
        /// Имя
        /// </summary>
        [NotNull]
        public readonly string Name;

        /// <summary>
        /// Отметить маппер для именованного связывания
        /// </summary>
        public NamedBindingAttribute([NotNull]string name)
        {
            if (name == null) throw new ArgumentNullException("name");
            Name = name;
        }
    }
}
