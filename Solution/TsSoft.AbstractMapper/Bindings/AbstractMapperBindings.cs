﻿namespace TsSoft.Bindings
{
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Engine;
    using TsSoft.AbstractMapper.FuncCreators;
    using TsSoft.AbstractMapper.Helpers;
    using TsSoft.AbstractMapper.Mappers;
    using TsSoft.AbstractMapper.RuleConverters;
    using TsSoft.AbstractMapper.Rules.Factories;
    using TsSoft.BindingsDescription;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.AbstractMapper
    /// </summary>
    public class AbstractMapperBindings : BindingsDescription
    {
        /// <summary>
        /// Создать экземпляр описания интерфейсов и их реализаций в сборке TsSoft.AbstractMapper
        /// </summary>
        public AbstractMapperBindings()
        {
            Bind<IAbstractMapperHelper, AbstractMapperHelper>();
            Bind<ICollectionConvertExpressionCreator, CollectionConvertExpressionCreator>();
            Bind<IConvertExpressionCreator, ConvertExpressionCreator>();
            Bind<IMapExpressionCreator, MapExpressionCreator>();
            Bind<IIgnoreRulesFactory, AttributeIgnoreRulesFactory>();
            
            Bind<IPrimitiveConvertExpressionCreator, PrimitiveConvertExpressionCreator>();
            Bind<IGetterAccessHelper, GetterAccessHelper>();
            Bind<IIgnoreRulesConverter, IgnoreRulesConverter>();
            Bind<IMapRulesConverter, MapRulesConverter>();
            Bindings.Add(typeof(IDynamicEntityToEntityMapper<>), typeof(DynamicEntityToEntityMapper<>));
            Bind<IDynamicEntityMapFuncCreator, DynamicEntityMapFuncCreator>();
            Bind<IDynamicEntityMapFuncCache, DynamicEntityMapFuncCache>();
            Bind<IMapperMethodHelper, MapperMethodHelper>();
            Bind<IMapperDependenciesRetriever, MapperDependenciesRetriever>();
            Bind<IMapperTypeHelper, MapperTypeHelper>();
            Bind<IMapperDependenciesChecker, MapperDependenciesChecker>();
            Bind<IMapperTypesRetriever, MapperTypesRetriever>();
            Bind<IFullMapRulesCreator, FullMapRulesCreator>();
            Bind<IImplicitMapRulesProvider, ImplicitMapRulesProvider>();
            Bind<IAutoConvertProvider, AutoConvertProvider>();
            Bind<IDirectCollectionConvertExpressionCreator, DirectCollectionConvertExpressionCreator>();
            Bind<ICollectionConverter, CollectionConverter>();
        }
    }
}
