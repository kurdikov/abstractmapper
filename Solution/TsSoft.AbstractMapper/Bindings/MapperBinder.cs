﻿namespace TsSoft.AbstractMapper.Bindings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Базовый класс для связывания интерфейсов маппера с реализациями
    /// </summary>
    public class MapperBinder
    {
        /// <summary>
        /// Связать мапперы из сборки автоматически
        /// </summary>
        /// <param name="bindSingle">Функция связывания одной реализации</param>
        /// <param name="genericMapperInterfaces">Интерфейсы мапперов с двумя параметрами-типами</param>
        /// <param name="assemblies">Сборки с мапперами</param>
        /// <param name="retriever">Получатель типов из сборки</param>
        public static void Bind(
            [NotNull]Action<MapperBinding> bindSingle,
            [NotNull]IReadOnlyList<Type> genericMapperInterfaces,
            [NotNull]IEnumerable<Assembly> assemblies,
            [CanBeNull]IAssemblyTypesRetriever retriever)
        {
            retriever = retriever ?? new AssemblyTypesRetriever();
            var allTypes = assemblies
                .SelectMany(retriever.GetTypes)
                .Distinct()
                .ToList();
            var bindings = MapperImplementationFinder.GetAutoBindings(allTypes, genericMapperInterfaces);
            foreach (var singleTypeBinding in bindings)
            {
                if (singleTypeBinding == null)
                {
                    continue;
                }
                bindSingle(singleTypeBinding);
            }
        }

        private static bool HasSameGenericArguments([NotNull]Type inter, [NotNull]Type baseMapperInterface)
        {
            var baseInter = inter.GetTypeInfo().GetInterfaces().AppendOne(inter).FirstOrDefault(i => i != null && i.GetTypeInfo().IsGenericType && i.GetGenericTypeDefinition() == baseMapperInterface);
            if (baseInter == null)
            {
                return false;
            }
            var genericArgs = inter.GenericTypeArguments;
            var baseGenericArgs = baseInter.GenericTypeArguments;
            return genericArgs != null && baseGenericArgs != null
                   && genericArgs.Length == baseGenericArgs.Length
                   && genericArgs.Zip(baseGenericArgs, (arg, baseArg) => arg == baseArg).All(eq => eq);
        }

        private static bool IsMapperInterface(Type inter, [NotNull]Type baseMapperInterface)
        {
            return inter != null
                   && inter.GetTypeInfo().IsGenericType
                   && HasSameGenericArguments(inter, baseMapperInterface);
        }

        /// <summary>
        /// Связать маппер для всех его интерфейсов
        /// </summary>
        /// <typeparam name="TMapper">Тип маппера</typeparam>
        /// <param name="bindSingle">Функция связывания одной реализации</param>
        /// <param name="name">Имя биндинга</param>
        /// <param name="baseMapperInterface">Корневой интерфейс маппера (по умолчанию - <cref see="IMapper`2"></cref>)</param>
        public static void BindAllInterfaces<TMapper>([NotNull]Action<MapperBinding> bindSingle, string name = null, Type baseMapperInterface = null)
        {
            baseMapperInterface = baseMapperInterface ?? typeof(IMapper<,>);
            var mapperInterfaces = typeof(TMapper).GetTypeInfo().GetInterfaces().Where(i => IsMapperInterface(i, baseMapperInterface));
            bindSingle(new MapperBinding(mapperInterfaces, typeof(TMapper), name));
        }
    }
}
