﻿namespace TsSoft.AbstractMapper.Bindings
{
    using System;

    /// <summary>
    /// Указать автобиндеру мапперов, что этот генерик-класс мапит в несколько типов
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ExtendedModelsMappedAttribute :Attribute
    {
        /// <summary>
        /// Типы сущностей, которые производит маппер
        /// </summary>
        public readonly Type[] ModelTypes;

        /// <summary>
        /// Указать автобиндеру мапперов, что этот генерик-класс мапит в несколько типов
        /// </summary>
        public ExtendedModelsMappedAttribute(params Type[] modelTypes)
        {
            ModelTypes = modelTypes;
        }
    }
}
