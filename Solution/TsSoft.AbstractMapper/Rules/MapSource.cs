namespace TsSoft.AbstractMapper.Rules
{
    using System;
    using System.Linq.Expressions;
    using System.Text;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// �������� �������������� ��������
    /// </summary>
    public class MapSource
    {
        /// <summary>
        /// ������ ����� �������� � TFrom
        /// </summary>
        [NotNull]
        public LambdaExpression FromAccess { get; set; }

        /// <summary>
        /// ���������� ��������
        /// </summary>
        public ProcessorDescription ProcessorDescription { get; set; }

        /// <summary>
        /// �� ����������� ������������, ���� �� TFrom ������ null
        /// </summary>
        public bool SkipAssignmentOnNull { get; set; }

        /// <summary>
        /// �������� �������������� ��������
        /// </summary>
        public MapSource([NotNull]LambdaExpression fromAccess, ProcessorDescription processor = null)
        {
            if (fromAccess == null)
            {
                throw new ArgumentNullException("fromAccess");
            }
            FromAccess = fromAccess;
            ProcessorDescription = processor;
        }

        /// <summary>
        /// �������� �������������� ��������
        /// </summary>
        public MapSource([NotNull]MapSource source)
        {
            if (source == null) throw new ArgumentNullException("source");
            FromAccess = source.FromAccess;
            ProcessorDescription = source.ProcessorDescription;
            SkipAssignmentOnNull = source.SkipAssignmentOnNull;
        }

        /// <summary>
        /// �������� ��������� �������������
        /// </summary>
        public void AppendTo([NotNull] StringBuilder sb)
        {
            sb.Append(FromAccess);
            if (ProcessorDescription != null)
            {
                sb.Append(" using ").Append(ProcessorDescription.Processor);
            }
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        public override string ToString()
        {
            var sb = new StringBuilder();
            AppendTo(sb);
            return sb.ToString();
        }

        /// <summary>
        /// ���������� ������������� ��������
        /// </summary>
        [NotNull]
        public static readonly MapSource[] NoSources = new MapSource[0];
    }
}
