﻿namespace TsSoft.AbstractMapper.Rules
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Полные правила преобразования
    /// </summary>
    public class FullMapRules : IFullMapRules
    {
        /// <summary>
        /// Список правил
        /// </summary>
        [NotNull] protected readonly List<MemberedMapRule> Rules = new List<MemberedMapRule>();
        /// <summary>
        /// Тип, в который идёт преобразование
        /// </summary>
        public Type TargetType { get; private set; }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Получить перечислитель
        /// </summary>
        public IEnumerator<MemberedMapRule> GetEnumerator()
        {
            return Rules.GetEnumerator();
        }

        /// <summary>
        /// Добавить правило
        /// </summary>
        /// <param name="rule"></param>
        public void Add([NotNull]MemberedMapRule rule)
        {
            if (rule == null)
            {
                throw new ArgumentNullException("rule");
            }
            Rules.Add(rule);
        }

        /// <summary>
        /// Количество правил
        /// </summary>
        public int Count { get { return Rules.Count; } }

        /// <summary>
        /// Создать полные правила преобразования
        /// </summary>
        /// <param name="targetType">Тип, в который идёт преобразование</param>
        public FullMapRules([NotNull]Type targetType)
        {
            if (targetType == null) throw new ArgumentNullException("targetType");
            TargetType = targetType;
        }
    }
}
