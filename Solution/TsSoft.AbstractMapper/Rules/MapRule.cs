﻿namespace TsSoft.AbstractMapper.Rules
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;

    /// <summary>
    /// Правило преобразования
    /// </summary>
    public class MapRule
    {
        /// <summary>
        /// Откуда брать значения в TFrom
        /// </summary>
        public IReadOnlyCollection<MapSource> FromAccess { get; set; }
        /// <summary>
        /// Куда писать значение в TTo
        /// </summary>
        public LambdaExpression ToAccess { get; set; }
        /// <summary>
        /// Как превращать полученные из TFrom значения в значение в TTo
        /// </summary>
        public LambdaExpression Convert { get; set; }
        /// <summary>
        /// Значение по умолчанию
        /// </summary>
        public LambdaExpression Default { get; set; }
        /// <summary>
        /// Передавать ли TTo в лямбду маппинга
        /// </summary>
        public bool ToPassedToMap { get; set; }

        /// <summary>
        /// Количество получаемых из TFrom значений
        /// </summary>
        public int FromPropertyCount { get { return FromAccess != null ? FromAccess.Count : 1; } }
        /// <summary>
        /// Количество внутренних обработчиков
        /// </summary>
        public int ProcessorCount { get { return FromAccess != null ? FromAccess.Count(fa => fa != null && fa.ProcessorDescription != null) : 0; } }

        /// <summary>
        /// Строковое представление
        /// </summary>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("to ").Append(ToAccess).Append(" from (");
            foreach (var from in FromAccess ?? MapSource.NoSources)
            {
                if (from != null)
                {
                    from.AppendTo(sb);
                    sb.Append(',');
                }
                else
                {
                    sb.Append("NULL,");
                }
            }
            sb.Append(") by ").Append(Convert);
            if (Default != null)
            {
                sb.Append(" default ").Append(Default);
            }
            if (ToPassedToMap)
            {
                sb.Append(" after map");
            }
            return sb.ToString();
        }
    }
}
