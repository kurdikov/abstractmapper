﻿namespace TsSoft.AbstractMapper.Rules
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    /// <summary>
    /// Не игнорировать ничего
    /// </summary>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    public class EmptyIgnoreRules<TTo> : IIgnoreRules<TTo>
    {
        /// <summary>
        /// Перечислитель пустых правил игнорирования
        /// </summary>
        public IEnumerator<Expression<Func<TTo, object>>> GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустых правил игнорирования
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Нуль
        /// </summary>
        public int Count
        {
            get { return 0; }
        }
    }

}
