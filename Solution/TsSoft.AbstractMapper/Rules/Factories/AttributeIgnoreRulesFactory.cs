﻿namespace TsSoft.AbstractMapper.Rules.Factories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Metadata;

    internal class AttributeIgnoreRulesFactory : IIgnoreRulesFactory
    {
        public IIgnoreRules<TTo> CreateRules<TTo, TFrom>()
        {
            return CreateRulesInternal<TTo, TFrom>();
        }

        public IIgnoreRules<TTo> CreateRules<TTo>()
        {
            return CreateRules<TTo, Stub>();
        }

        [NotNull]
        private static IIgnoreRules<TTo> CreateRulesInternal<TTo, TFrom>()
        {
            Func<PropertyInfo, bool> predicate = x => x != null && x.IsDefined(typeof(MapIgnoreAttribute));
            var propertyInfos = typeof (TTo).GetTypeInfo().GetProperties()
                                            .Where(predicate)
                                            .ToDictionary(p => p, p => p.GetCustomAttributes<MapIgnoreAttribute>());
            var result = new IgnoreRules<TTo>();
            var tfrom = typeof (TFrom);
            foreach (var propertyAndIgnoreAttributes in propertyInfos)
            {
                if (propertyAndIgnoreAttributes.Key == null)
                {
                    continue;
                }
                if (!InIgnoredTypes(propertyAndIgnoreAttributes, tfrom))
                {
                    continue;
                }
                var param = Expression.Parameter(typeof (TTo), "tto");
                Expression makeMemberAccess = Expression.MakeMemberAccess(param, propertyAndIgnoreAttributes.Key);
                if (propertyAndIgnoreAttributes.Key.PropertyType.GetTypeInfo().IsValueType)
                {
                    makeMemberAccess = Expression.Convert(makeMemberAccess, typeof (object));
                }
                result.Add(Expression.Lambda<Func<TTo, object>>(makeMemberAccess, param));
            }
            return result;
        }

        private static bool InIgnoredTypes(KeyValuePair<PropertyInfo, IEnumerable<MapIgnoreAttribute>> propertyInfo, Type tfrom)
        {
            return tfrom == typeof(Stub)
                || propertyInfo.Value != null && propertyInfo.Value.Any(
                    mapIgnoreAttribute => mapIgnoreAttribute != null && (mapIgnoreAttribute.FromIgnoredTypes == null || mapIgnoreAttribute.FromIgnoredTypes.Contains(tfrom)));
        }

        private class Stub
        {
        }
    }
}
