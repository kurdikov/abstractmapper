﻿namespace TsSoft.AbstractMapper.Rules.Factories
{
    using JetBrains.Annotations;

    /// <summary>
    /// Фабрика правил игнорирования
    /// </summary>
    public interface IIgnoreRulesFactory
    {
        /// <summary>
        /// Создать правила игнорирования при маппинге TFrom в TTo
        /// </summary>
        /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
        /// <typeparam name="TFrom">Тип, из которого осуществляется преобразование</typeparam>
        /// <returns>Правила игнорирования</returns>
        [NotNull]
        IIgnoreRules<TTo> CreateRules<TTo, TFrom>();

        /// <summary>
        /// Создать правила игнорирования при маппинге в TTo
        /// </summary>
        /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
        /// <returns>Правила игнорирования</returns>
        [NotNull]
        IIgnoreRules<TTo> CreateRules<TTo>();
    }
}