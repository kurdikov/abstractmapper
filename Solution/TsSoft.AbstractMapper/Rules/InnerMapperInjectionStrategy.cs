﻿namespace TsSoft.AbstractMapper.Rules
{
    using System;

    /// <summary>
    /// Как и какие мапперы для подсущностей получать
    /// </summary>
    [Flags]
    public enum InnerMapperStrategy
    {
        /// <summary>
        /// Получать внутренние мапперы для статически определяемых типов во время преобразования
        /// </summary>
        Default = 0,

        /// <summary>
        /// Получать внутренние мапперы для статически определяемых типов
        /// </summary>
        StaticTypes = 0,

        /// <summary>
        /// Определять типы внутренних сущностей во время выполнения
        /// </summary>
        DynamicTypes = 1,

        /// <summary>
        /// Получать внутренние мапперы по необходимости во время преобразования
        /// </summary>
        ResolveOnMapping = 0,

        /// <summary>
        /// Получать внутренние мапперы во время построения внешнего маппера
        /// </summary>
        ResolveOnMapperBuilding = 2,
    }

    internal static class InnerMapperStrategyExtensions
    {
        public static bool FlagEquals(this InnerMapperStrategy strategy, InnerMapperStrategy flag)
        {
            return (strategy & flag) == flag;
        }
    }
}
