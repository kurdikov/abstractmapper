﻿// ReSharper disable UnusedTypeParameter
// this interface is made generic to make Intellisense usage with Mapper base class better
namespace TsSoft.AbstractMapper.Rules
{
    using System.Collections.Generic;

    /// <summary>
    /// Правила преобразования
    /// </summary>
    /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
    /// <typeparam name="TFrom">Тип, из которого осуществляется преобразование</typeparam>
    public interface IMapRules<TTo, TFrom> : IReadOnlyCollection<MapRule>
    {
    }
}
// ReSharper restore UnusedTypeParameter
