﻿namespace TsSoft.AbstractMapper.Rules
{
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Пустая коллекция правил преобразования
    /// </summary>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    /// <typeparam name="TFrom">Тип, из которого происходит преобразование</typeparam>
    public class EmptyMapRules<TTo, TFrom> : IMapRules<TTo, TFrom>
    {
        /// <summary>
        /// Нуль
        /// </summary>
        public int Count
        {
            get { return 0; }
        }

        /// <summary>
        /// Перечислитель пустых правил преобразования
        /// </summary>
        public IEnumerator<MapRule> GetEnumerator()
        {
            yield break;
        }

        /// <summary>
        /// Перечислитель пустых правил преобразования
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

}
