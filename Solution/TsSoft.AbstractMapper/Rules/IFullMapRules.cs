﻿namespace TsSoft.AbstractMapper.Rules
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Правила перобразования
    /// </summary>
    public interface IFullMapRules : IReadOnlyCollection<MemberedMapRule>
    {
        /// <summary>
        /// Конкретный тип результата
        /// </summary>
        [NotNull]
        Type TargetType { get; }
    }
}
