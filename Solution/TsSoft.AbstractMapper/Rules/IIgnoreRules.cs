﻿namespace TsSoft.AbstractMapper.Rules
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    /// <summary>
    /// Правила игнорирования
    /// </summary>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    public interface IIgnoreRules<TTo> : IReadOnlyCollection<Expression<Func<TTo, object>>>
    {
    }
}
