﻿namespace TsSoft.AbstractMapper.Rules
{
    using System;
    using System.Linq;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Правило преобразования с указанной целью
    /// </summary>
    public class MemberedMapRule : MapRule
    {
        /// <summary>
        /// Куда писать значение в TTo
        /// </summary>
        [NotNull]
        public ValueHoldingMember ToMember { get; set; }
        /// <summary>
        /// Описание маппера
        /// </summary>
        public MapperDescription MapperDescription { get; set; }

        /// <summary>
        /// Правило преобразования с указанной целью
        /// </summary>
        public MemberedMapRule([NotNull]ValueHoldingMember member)
        {
            if (member == null) throw new ArgumentNullException("member");
            ToMember = member;
        }

        /// <summary>
        /// Правило преобразования
        /// </summary>
        public MemberedMapRule([NotNull]MapRule rule, [NotNull]ValueHoldingMember member)
        {
            if (rule == null) throw new ArgumentNullException("rule");
            if (member == null) throw new ArgumentNullException("member");
            FromAccess = rule.FromAccess != null ? rule.FromAccess.Select(fa => new MapSource(fa)).ToList() : null;
            ToAccess = rule.ToAccess;
            Convert = rule.Convert;
            Default = rule.Default;
            ToPassedToMap = rule.ToPassedToMap;
            ToMember = member;
        }
    }
}
