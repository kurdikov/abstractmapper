﻿namespace TsSoft.AbstractMapper.Rules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// Правила игнорирования
    /// </summary>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    public class IgnoreRules<TTo> : List<Expression<Func<TTo, object>>>, IIgnoreRules<TTo>
    {
        /// <summary>
        /// Добавить коллекцию правил игнорирования
        /// </summary>
        /// <param name="rules">Тип, в который происходит преобразование</param>
        public void Add(IIgnoreRules<TTo> rules)
        {
            AddRange(rules ?? Enumerable.Empty<Expression<Func<TTo, object>>>());
        }
    }
}
