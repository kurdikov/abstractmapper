﻿namespace TsSoft.AbstractMapper.Rules
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Описание преобразуемого
    /// </summary>
    public class ReflectedMapSource
    {
        /// <summary>
        /// Путь из свойств
        /// </summary>
        [NotNull]
        public ParsedPath Path { get; set; }

        /// <summary>
        /// Тип, который должен вернуть путь
        /// </summary>
        [NotNull]
        public Type PathType { get; set; }

        /// <summary>
        /// Не производить присваивание, если из TFrom пришёл null
        /// </summary>
        public bool SkipAssignmentOnNull { get; set; }

        /// <summary>
        /// Обработчик
        /// </summary>
        [CanBeNull]
        public ProcessorDescription ProcessorDescription { get; set; }
    }
}
