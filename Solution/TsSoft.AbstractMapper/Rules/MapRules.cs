﻿namespace TsSoft.AbstractMapper.Rules
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    ///     Коллекция правил преобразования
    /// </summary>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    /// <typeparam name="TFrom">Тип, из которого происходит преобразование</typeparam>
    public class MapRules<TTo, TFrom> : IMapRules<TTo, TFrom>, ICollection<MapRule>
    {
        /// <summary>
        ///     Список правил преобразования
        /// </summary>
        [NotNull] protected readonly List<MapRule> Rules = new List<MapRule>();

        /// <summary>
        /// Добавить правило преобразования
        /// </summary>
        /// <param name="item">Правило преобразования</param>
        public void Add([NotNull] MapRule item)
        {
            if (item == null) throw new ArgumentNullException("item");
            Rules.Add(item);
        }

        /// <summary>
        /// Очистить коллекцию
        /// </summary>
        public void Clear()
        {
            Rules.Clear();
        }

        /// <summary>
        /// Содержит ли коллекция заданный экземпляр правила
        /// </summary>
        public bool Contains(MapRule item)
        {
            return Rules.Contains(item);
        }

        /// <summary>
        /// Скопировать правила в массив
        /// </summary>
        public void CopyTo(MapRule[] array, int arrayIndex)
        {
            Rules.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Удалить правило
        /// </summary>
        public bool Remove(MapRule item)
        {
            return Rules.Remove(item);
        }

        /// <summary>
        /// Является ли коллекцией только для чтения
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        ///     Перечислитель правил преобразования
        /// </summary>
        public IEnumerator<MapRule> GetEnumerator()
        {
            return Rules.GetEnumerator();
        }

        /// <summary>
        ///     Перечислитель правил преобразования
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        ///     Количество правил преобразования в коллекции
        /// </summary>
        public int Count
        {
            get { return Rules.Count; }
        }

        /// <summary>
        ///     Добавить коллекцию правил преобразования
        /// </summary>
        public void Add(IMapRules<TTo, TFrom> rules)
        {
            Rules.AddRange(rules ?? Enumerable.Empty<MapRule>());
        }

        /// <summary>
        ///     Добавить коллекцию правил преобразования
        /// </summary>
        public void Add(MapRules<TTo, TFrom> rules)
        {
            Add((IMapRules<TTo, TFrom>) rules);
        }

        // pure map rule
        /// <summary>
        ///     Добавить правило
        /// </summary>
        /// <typeparam name="TToProperty">Тип свойства, в которое идёт преобразование</typeparam>
        /// <typeparam name="TFromProperty">Тип свойства, из которого идёт преобразование</typeparam>
        /// <param name="toAccess">Куда</param>
        /// <param name="convert">Как</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        public void Add<TToProperty, TFromProperty>(
            Expression<Func<TTo, TToProperty>> toAccess,
            Expression<Func<TFromProperty, TToProperty>> convert,
            Expression<Func<TToProperty>> defaultValue = null)
        {
            Rules.Add(new MapRule
            {
                ToAccess = toAccess,
                Convert = convert,
                Default = defaultValue,
            });
        }

        // map constant value
        /// <summary>
        ///     Добавить правило
        /// </summary>
        /// <typeparam name="TToProperty">Тип свойства, в которое идёт преобразование</typeparam>
        /// <param name="toAccess">Куда</param>
        /// <param name="value">Что</param>
        public void Add<TToProperty>(
            Expression<Func<TTo, TToProperty>> toAccess,
            Expression<Func<TToProperty>> value)
        {
            Rules.Add(new MapRule
            {
                ToAccess = toAccess,
                Convert = value,
                FromAccess = MapSource.NoSources,
            });
        }

        // map one property to one property with default value (defined at runtime)
        /// <summary>
        ///     Добавить правило
        /// </summary>
        /// <typeparam name="TToProperty">Тип свойства, в которое идёт преобразование</typeparam>
        /// <typeparam name="TFromProperty">Тип свойства, из которого идёт преобразование</typeparam>
        /// <param name="toAccess">Куда</param>
        /// <param name="fromAccess">Что</param>
        /// <param name="convert">Как</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        public void Add<TFromProperty, TToProperty>(
            Expression<Func<TTo, TToProperty>> toAccess,
            Expression<Func<TFrom, TFromProperty>> fromAccess,
            Expression<Func<TFromProperty, TToProperty>> convert = null,
            Expression<Func<TToProperty>> defaultValue = null)
        {
            Rules.Add(new MapRule
            {
                ToAccess = toAccess,
                FromAccess = new[] {new MapSource(fromAccess), },
                Convert = convert,
                Default = defaultValue,
            });
        }

        // map two properties to one property
        /// <summary>
        /// </summary>
        /// <typeparam name="TToProperty">Тип свойства, в которое идёт преобразование</typeparam>
        /// <typeparam name="TFromProperty1">Тип первого свойства, из которого идёт преобразование</typeparam>
        /// <typeparam name="TFromProperty2">Тип второго свойства, из которого идёт преобразование</typeparam>
        /// <param name="toAccess">Куда</param>
        /// <param name="fromFirstAccess">Что #1</param>
        /// <param name="fromSecondAccess">Что #2</param>
        /// <param name="convert">Как</param>
        public void Add<TFromProperty1, TFromProperty2, TToProperty>(
            Expression<Func<TTo, TToProperty>> toAccess,
            Expression<Func<TFrom, TFromProperty1>> fromFirstAccess,
            Expression<Func<TFrom, TFromProperty2>> fromSecondAccess,
            Expression<Func<TFromProperty1, TFromProperty2, TToProperty>> convert)
        {
            Rules.Add(new MapRule
            {
                ToAccess = toAccess,
                FromAccess = new[] {new MapSource(fromFirstAccess), new MapSource(fromSecondAccess), },
                Convert = convert,
            });
        }

        /// <summary>
        /// </summary>
        /// <typeparam name="TToProperty">Тип свойства, в которое идёт преобразование</typeparam>
        /// <param name="toAccess">Куда</param>
        /// <param name="dummy">Выполнить после остальных правил</param>
        /// <param name="convert">Как</param>
        public void Add<TToProperty>(
            Expression<Func<TTo, TToProperty>> toAccess,
            AfterMapRule dummy,
            Expression<Func<TTo, TToProperty>> convert)
        {
            Rules.Add(new MapRule
            {
                ToAccess = toAccess,
                Convert = convert,
                FromAccess = MapSource.NoSources,
                ToPassedToMap = true,
            });
        }
    }
}
