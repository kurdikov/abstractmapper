﻿namespace TsSoft.AbstractMapper.Rules
{
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Правило преобразования в удобном для рефлексии виде
    /// </summary>
    public class ReflectedMapRule
    {
        /// <summary>
        /// Куда
        /// </summary>
        [NotNull]
        public ValueHoldingMember To { get; set; }

        /// <summary>
        /// Откуда и с помощью чего
        /// </summary>
        [NotNull]
        public IReadOnlyList<ReflectedMapSource> From { get; set; }

        /// <summary>
        /// Описание маппера
        /// </summary>
        [CanBeNull]
        public MapperDescription MapperDescription { get; set; }

        /// <summary>
        /// Выражение для преобразования
        /// </summary>
        [NotNull]
        public LambdaExpression Convert { get; set; }

        /// <summary>
        /// Передаётся ли в Convert объект-результат преобразования
        /// </summary>
        public bool ToPassedToMap { get; set; }

        /// <summary>
        /// Строковое представление
        /// </summary>
        public override string ToString()
        {
            return string.Format("To: {0}", To);
        }
    }
}
