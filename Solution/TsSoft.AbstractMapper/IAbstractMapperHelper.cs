﻿namespace TsSoft.AbstractMapper
{
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Helpers.Closures;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Хелпер для абстрактных мапперов
    /// </summary>
    public interface IAbstractMapperHelper
    {
        /// <summary>
        /// Создать полную коллекцию правил с заполненными источниками преобразования и выражениями преобразования
        /// </summary>
        /// <typeparam name="TTo">Тип, в который идёт преобразование</typeparam>
        /// <typeparam name="TFrom">Тип, из которого идёт преобразование</typeparam>
        /// <param name="explicitRules">Указанные в маппере правила</param>
        /// <param name="ignoreRules">Правила игнорирования</param>
        /// <param name="autoPropertiesBehavior">Поведение в случае, когда в TFrom не найдены подходящие свойства для построения автоправил заполнения TTo</param>
        /// <param name="innerMapperStrategy">Как получать внутренние мапперы</param>
        /// <returns>Упорядоченный список правил преобразования</returns>
        /// <exception cref="InvalidRulesException">Не удалось построить автоправила для некоторых свойств</exception>
        [NotNull]
        IFullMapRules MakeFullRules<TFrom, TTo>(
            IMapRules<TTo, TFrom> explicitRules,
            IIgnoreRules<TTo> ignoreRules,
            AutoPropertiesBehavior autoPropertiesBehavior,
            InnerMapperStrategy innerMapperStrategy);

            /// <summary>
        /// Создать выражение для преобразования
        /// </summary>
        /// <typeparam name="TFrom">Тип, из которого осуществляется преобразование</typeparam>
        /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
        /// <param name="fullMapRules">Правила преобразования</param>
        /// <param name="addCatchBlocks">Оборачивать ли в try-catch выражения маппинга</param>
        /// <param name="exceptionMessage">Сообщение внутреннего исключения</param>
        /// <returns>Выражение преобразования</returns>
        [NotNull]
        MapperExpression<TFrom, TTo> CreateMapperExpression<TFrom, TTo>(
            [NotNull]IFullMapRules fullMapRules,
            bool addCatchBlocks,
            string exceptionMessage);

        /// <summary>
        /// Создать правила игнорирования
        /// </summary>
        IIgnoreRules<TTo> CreateIgnoreRules<TTo>();

        /// <summary>
        /// Создать правила игнорирования
        /// </summary>
        IIgnoreRules<TTo> CreateIgnoreRules<TTo, TFrom>();

        /// <summary>
        /// Скомпилировать выражение
        /// </summary>
        /// <typeparam name="TDelegate">Тип делегата</typeparam>
        /// <param name="lambda">Выражение</param>
        /// <param name="compilationType">Тип компиляции</param>
        /// <returns>Делегат</returns>
        [NotNull]
        TDelegate Compile<TDelegate>([NotNull]Expression<TDelegate> lambda, LambdaCompilationType compilationType = LambdaCompilationType.PrecompileInner);
    }
}
