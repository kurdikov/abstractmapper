﻿namespace TsSoft.AbstractMapper
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Базовый класс для абстрактных мапперов
    /// </summary>
    /// <typeparam name="TFrom"></typeparam>
    /// <typeparam name="TTo"></typeparam>
    public class Mapper<TFrom, TTo> : BaseMapper<TFrom, TTo>
        where TFrom : class
    {
        /// <summary>
        /// Хелпер для абстрактного маппера
        /// </summary>
        [NotNull] protected readonly IAbstractMapperHelper MapperHelper;
        [NotNull] private readonly Lazy<IIgnoreRules<TTo>> _ignoreRules;
        [CanBeNull]private Lazy<MapperExpression<TFrom, TTo>> _mapperExpression;

        [NotNull]private readonly Lazy<Func<TFrom, TTo>> _mapper;

        /// <summary>
        /// Создать маппер
        /// </summary>
        /// <param name="mapperHelper">Хелпер абстрактного маппера</param>
        public Mapper([NotNull] IAbstractMapperHelper mapperHelper)
        {
            if (mapperHelper == null)
            {
                throw new ArgumentNullException("mapperHelper");
            }
            MapperHelper = mapperHelper;
            _mapperExpression = new Lazy<MapperExpression<TFrom, TTo>>(
                () => MapperHelper.CreateMapperExpression<TFrom, TTo>(
                    FullMapRules,
                    GenerateCatchBlocks,
                    string.Format("An error occurred in {0}.Map().", GetType())));
            _mapper = new Lazy<Func<TFrom, TTo>>(CompileMapper);
            _ignoreRules = new Lazy<IIgnoreRules<TTo>>(mapperHelper.CreateIgnoreRules<TTo, TFrom>);
        }

        /// <summary>
        /// Выражение маппинга
        /// </summary>
        [NotNull]
        protected MapperExpression<TFrom, TTo> MapperExpression
        {
            get
            {
                if (_mapperExpression == null)
                {
                    throw new InvalidOperationException("Mapper expression is already disposed");
                }
                if (_mapperExpression.Value == null)
                {
                    throw new NullReferenceException("_mapperExpression.Value");
                }
                return _mapperExpression.Value;
            }
        }

        /// <summary>
        /// Правила маппинга
        /// </summary>
        [NotNull]
        protected virtual IMapRules<TTo, TFrom> MapRules
        {
            get { return new EmptyMapRules<TTo, TFrom>(); }
        }

        /// <summary>
        /// Правила маппинга, дополненные автоправилами
        /// </summary>
        [NotNull]
        protected internal virtual IFullMapRules FullMapRules
        {
            get { return MapperHelper.MakeFullRules(MapRules, IgnoreRules, AutoPropertiesBehavior, InnerMapperStrategy); }
        }

            /// <summary>
        /// Правила игнорирования
        /// </summary>
        [NotNull]
        protected virtual IIgnoreRules<TTo> IgnoreRules
        {
            get { return _ignoreRules.Value ?? new EmptyIgnoreRules<TTo>(); }
        }

        /// <summary>
        /// Поведение в случае наличия в TTo свойств, для которых не найдено соответствие
        /// </summary>
        protected virtual AutoPropertiesBehavior AutoPropertiesBehavior
        {
            get { return AutoPropertiesBehavior.Strict; }
        }

        /// <summary>
        /// Стратегия маппинга подсущностей
        /// </summary>
        protected virtual InnerMapperStrategy InnerMapperStrategy
        {
            get { return InnerMapperStrategy.Default; }
        }

        /// <summary>
        /// Вся ли необходимая информация получена из построенного выражения
        /// </summary>
        protected virtual bool IsExpressionProcessed
        {
            get { return _mapper.IsValueCreated; }
        }

        /// <summary>
        /// Оборачивать ли в try выражения маппинга
        /// </summary>
        protected virtual bool GenerateCatchBlocks
        {
            get { return true; }
        }

        /// <summary>
        /// Применить преобразование
        /// </summary>
        /// <param name="from">Преобразуемый объект</param>
        /// <returns>Преобразованный объект</returns>
        protected override TTo RealMap(TFrom @from)
        {
            return GetMapper()(@from);
        }

        /// <summary>
        /// Получить делегат для преобразования
        /// </summary>
        /// <returns>Делегат для преобразования</returns>
        [NotNull]
        protected Func<TFrom, TTo> GetMapper()
        {
            return GetLazyValueAndClearCacheIfProcessed(_mapper);
        }

        /// <summary>
        /// Скомпилировать выражение для преобразования в делегат
        /// </summary>
        /// <returns></returns>
        [NotNull]
        protected virtual Func<TFrom, TTo> CompileMapper()
        {
            var result = MapperHelper.Compile(MapperExpression.Expression);
            return result;
        }

        /// <summary>
        /// Получить лениво создаваемый по MapperExpression объект и удалить ссылку на MapperExpression, если она больше не нужна
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="lazy">Ленивая обёртка</param>
        /// <returns>Созданный объект</returns>
        [NotNull]
        protected T GetLazyValueAndClearCacheIfProcessed<T>([NotNull]Lazy<T> lazy)
            where T: class
        {
            var result = lazy.Value;
            if (result == null)
            {
                throw new NullReferenceException(string.Format("Lazy value of type {0} in {1} is null", typeof(T), GetType()));
            }
            if (_mapperExpression != null && IsExpressionProcessed)
            {
                _mapperExpression = null;
            }
            return result;
        }
    }
}
