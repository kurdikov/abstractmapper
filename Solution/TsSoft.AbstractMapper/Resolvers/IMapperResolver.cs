﻿namespace TsSoft.AbstractMapper.Resolvers
{
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Mappers;

    /// <summary>
    /// Разрешитель зависимостей мапперов
    /// </summary>
    public interface IMapperResolver
    {
        /// <summary>
        /// Попытаться получить экземпляр маппера из TFrom в TTo
        /// </summary>
        /// <typeparam name="TFrom">Тип, из которого идёт преобразование</typeparam>
        /// <typeparam name="TTo">Тип, в который идёт преобразование</typeparam>
        IMapper<TFrom, TTo> TryGet<TFrom, TTo>();

        /// <summary>
        /// Получить стандартный абстрактный маппер из TFrom в TTo
        /// </summary>
        LooseMapper<TFrom, TTo> GetDefault<TFrom, TTo>()
            where TFrom: class
            where TTo: class, new();

        /// <summary>
        /// Получить абстрактный маппер из TFrom в TTo, определяющий типы внутренних сущностей во время выполнения
        /// </summary>
        DynamicMapper<TFrom, TTo> GetDynamic<TFrom, TTo>()
            where TFrom : class
            where TTo : class, new();

        /// <summary>
        /// Получить абстрактный маппер из объекта в TTo, определяющий тип объекта во время выполнения
        /// </summary>
        IDynamicEntityToEntityMapper<TTo> GetTypeDeterminingMapper<TTo>()
            where TTo : class, new();
    }
}
