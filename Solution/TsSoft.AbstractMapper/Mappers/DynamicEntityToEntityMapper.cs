﻿namespace TsSoft.AbstractMapper.Mappers
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.FuncCreators;

    internal class DynamicEntityToEntityMapper<TEntity> : IDynamicEntityToEntityMapper<TEntity>
        where TEntity: class, new()
    {
        [NotNull]
        private readonly IDynamicEntityMapFuncCache _mapFuncCache;

        public DynamicEntityToEntityMapper([NotNull]IDynamicEntityMapFuncCache mapFuncCache)
        {
            if (mapFuncCache == null) throw new ArgumentNullException("mapFuncCache");
            _mapFuncCache = mapFuncCache;
        }

        public TEntity Map(object from)
        {
            if (from == null)
            {
                return null;
            }
            var mapFunc = _mapFuncCache.Get<TEntity>(from.GetType());
            if (mapFunc == null)
            {
                throw new NullReferenceException(string.Format("IDynamicEntityMapFuncCache.Get[{0}] returned null for {1}", typeof(TEntity), from.GetType()));
            }
            return mapFunc(from);
        }
    }
}
