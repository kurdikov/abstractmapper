﻿namespace TsSoft.AbstractMapper.Mappers
{
    using TsSoft.AbstractMapper;

    /// <summary>
    /// Преобразователь произвольных объектов к типу TEntity, копирующий значения одноимённых свойств
    /// </summary>
    public interface IDynamicEntityToEntityMapper<out TEntity> : IMapper<object, TEntity>
    {
    }
}
