﻿namespace TsSoft.AbstractMapper.Mappers
{
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules;

    /// <summary>
    /// Базовый класс для абстрактного маппера, определяющего типы вложенных сущностей во время выполнения
    /// </summary>
    /// <typeparam name="TFrom">Тип, из которого происходит преобразование</typeparam>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    public class DynamicMapper<TFrom, TTo> : LooseMapper<TFrom, TTo>
        where TFrom : class
        where TTo : class
    {
        /// <summary>
        /// Базовый класс для абстрактного маппера, определяющего типы вложенных сущностей во время выполнения
        /// </summary>
        public DynamicMapper([NotNull] IAbstractMapperHelper mapperHelper)
            : base(mapperHelper)
        {
        }

        /// <summary>
        /// Стратегия маппинга подсущностей
        /// </summary>
        protected override InnerMapperStrategy InnerMapperStrategy
        {
            get { return InnerMapperStrategy.ResolveOnMapperBuilding | InnerMapperStrategy.DynamicTypes; }
        }
    }
}
