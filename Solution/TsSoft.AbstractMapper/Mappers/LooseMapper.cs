﻿namespace TsSoft.AbstractMapper.Mappers
{
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Rules;

    /// <summary>
    /// Базовый класс для абстрактного маппера, тихо игнорирующего свойства, для которых не найдено соответствие
    /// </summary>
    /// <typeparam name="TFrom">Тип, из которого происходит преобразование</typeparam>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    public class LooseMapper<TFrom, TTo> : Mapper<TFrom, TTo>
        where TFrom: class 
        where TTo: class
    {
        /// <summary>
        /// Базовый класс для абстрактного маппера, тихо игнорирующего свойства, для которых не найдено соответствие
        /// </summary>
        public LooseMapper([NotNull]IAbstractMapperHelper mapperHelper)
            : base(mapperHelper)
        {
        }

        /// <summary>
        /// Поведение в случае наличия в TTo свойств, для которых не найдено соответствие
        /// </summary>
        protected override AutoPropertiesBehavior AutoPropertiesBehavior
        {
            get
            {
                return AutoPropertiesBehavior.Loose;
            }
        }
    }
}
