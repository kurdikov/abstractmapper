﻿namespace TsSoft.AbstractMapper.Exceptions
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Зависимость абстрактного маппера, которую не удалось разрешить
    /// </summary>
    public interface IUnresolvedDependency
    {
        /// <summary>
        /// Тип маппера, зависимость которого не удалось разрешить
        /// </summary>
        [NotNull]
        Type MapperType { get; }

        /// <summary>
        /// Тип свойства в TFrom, породившего неразрешённую зависимость
        /// </summary>
        [CanBeNull]
        Type DependencyFromType { get; }

        /// <summary>
        /// Тип свойства в TTo, породившего неразрешённую зависимость
        /// </summary>
        [CanBeNull]
        Type DependencyToType { get; }

        /// <summary>
        /// Исключение разрешителя
        /// </summary>
        [CanBeNull]
        Exception Exception { get; }
    }
}
