﻿namespace TsSoft.AbstractMapper.Exceptions
{
    using System;

    /// <summary>
    /// Исключение, возникающее при реализации двумя классами одного интерфейса маппера
    /// </summary>
    public class MapperBinderException : Exception
    {
        /// <summary>
        /// Исключение, возникающее при реализации двумя классами одного интерфейса маппера
        /// </summary>
        public MapperBinderException(string message)
            : base(message)
        {
        }
    }
}
