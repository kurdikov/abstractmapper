﻿namespace TsSoft.AbstractMapper.Exceptions
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Исключение разрешителя зависимостей мапперов
    /// </summary>
    public class AbstractMapperResolverException : Exception
    {
        /// <summary>
        /// Исключение разрешителя зависимостей мапперов
        /// </summary>
        /// <param name="mapperType">Тип маппера, который не удалось завершить</param>
        public AbstractMapperResolverException([CanBeNull]Type mapperType)
            : base(string.Format("Unable to resolve {0}", mapperType))
        {
        }
    }
}
