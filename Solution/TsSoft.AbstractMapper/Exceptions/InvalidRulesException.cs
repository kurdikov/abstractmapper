﻿namespace TsSoft.AbstractMapper.Exceptions
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Неверные правила маппера
    /// </summary>
    public class InvalidRulesException : Exception
    {
        /// <summary>
        /// Неверные правила маппера
        /// </summary>
        public InvalidRulesException([CanBeNull]string message)
            : base(message)
        {
        }
    }
}
