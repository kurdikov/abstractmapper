﻿namespace TsSoft.AbstractMapper.Exceptions
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Зависимость абстрактного маппера, которую не удалось разрешить
    /// </summary>
    public class UnresolvedDependency : IUnresolvedDependency
    {
        /// <summary>
        /// Тип маппера, зависимость которого не удалось разрешить
        /// </summary>
        public Type MapperType { get; private set; }

        /// <summary>
        /// Тип свойства в TFrom, породившего неразрешённую зависимость
        /// </summary>
        public Type DependencyFromType { get; private set; }

        /// <summary>
        /// Тип свойства в TTo, породившего неразрешённую зависимость
        /// </summary>
        public Type DependencyToType { get; private set; }

        /// <summary>
        /// Исключение разрешителя или создателя выражения преобразования
        /// </summary>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Зависимость абстрактного маппера, которую не удалось разрешить
        /// </summary>
        public UnresolvedDependency([NotNull] Type mapperType, [CanBeNull]Type dependencyFromType, [CanBeNull]Type dependencyToType, [CanBeNull]Exception exception)
        {
            if (mapperType == null) throw new ArgumentNullException("mapperType");
            MapperType = mapperType;
            DependencyFromType = dependencyFromType;
            DependencyToType = dependencyToType;
            Exception = exception;
        }

        /// <summary>
        /// Зависимость абстрактного маппера, которую не удалось разрешить
        /// </summary>
        public UnresolvedDependency([NotNull] Type mapperType, [CanBeNull]Type dependencyFromType, [CanBeNull]Type dependencyToType)
            : this(mapperType, dependencyFromType, dependencyToType, null)
        {
        }

        /// <summary>
        /// Зависимость абстрактного маппера, которую не удалось разрешить
        /// </summary>
        public UnresolvedDependency([NotNull] Type mapperType, [CanBeNull]Exception exception)
            : this(mapperType, null, null, exception)
        {
        }
    }
}
