﻿namespace TsSoft.AbstractMapper.Metadata
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Игнорировать свойство при преобразовании абстрактным маппером
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class MapIgnoreAttribute : Attribute
    {
        private readonly IReadOnlyCollection<Type> _fromIgnoredTypes;

        /// <summary>
        /// Игнорировать свойство при преобразовании абстрактным маппером
        /// </summary>
        public MapIgnoreAttribute()
        {
        }

        /// <summary>
        /// Игнорировать свойство при преобразовании абстрактным маппером из указанного типа
        /// </summary>
        public MapIgnoreAttribute([CanBeNull]Type fromIgnoredType)
            : this(new[] { fromIgnoredType })
        {
        }

        /// <summary>
        /// Игнорировать свойство при преобразовании абстрактным маппером из указанных типов
        /// </summary>
        public MapIgnoreAttribute([CanBeNull]params Type[] fromIgnoredTypes)
        {
            _fromIgnoredTypes = fromIgnoredTypes;
        }

        /// <summary>
        /// При преобразовании из каких типов игнорировать свойство
        /// </summary>
        public IReadOnlyCollection<Type> FromIgnoredTypes
        {
            get { return _fromIgnoredTypes; }
        }
    }
}
