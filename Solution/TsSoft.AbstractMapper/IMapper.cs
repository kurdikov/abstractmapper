﻿namespace TsSoft.AbstractMapper
{
    /// <summary>
    /// Интерфейс маппера
    /// </summary>
    /// <typeparam name="TFrom">Тип, из которого осуществляется преобразование</typeparam>
    /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
    public interface IMapper<in TFrom, out TTo>
    {
        /// <summary>
        /// Преобразовать экземпляр TFrom в экземпляр TTo
        /// </summary>
        TTo Map(TFrom from);
    }
}
