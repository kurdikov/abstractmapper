﻿namespace TsSoft.AbstractMapper.FuncCreators
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    class DynamicEntityMapFuncCache : IDynamicEntityMapFuncCache
    {
        [NotNull]
        private readonly IDynamicEntityMapFuncCreator _mapFuncCreator;

        [NotNull]
        private readonly ConcurrentDictionary<KeyValuePair<Type, Type>, MulticastDelegate> _cache =
            new ConcurrentDictionary<KeyValuePair<Type, Type>, MulticastDelegate>();

        public DynamicEntityMapFuncCache(
            [NotNull]IDynamicEntityMapFuncCreator mapFuncCreator)
        {
            if (mapFuncCreator == null) throw new ArgumentNullException("mapFuncCreator");
            _mapFuncCreator = mapFuncCreator;
        }

        public Func<object, TEntity> Get<TEntity>(Type dynamicType)
            where TEntity : class, new()
        {
            var result = _cache.GetOrAdd(
                new KeyValuePair<Type, Type>(dynamicType, typeof(TEntity)),
                pair => _mapFuncCreator.CreateMapperFunc<TEntity>(dynamicType)) as Func<object, TEntity>;
            if (result == null)
            {
                throw new NullReferenceException(string.Format(
                    "Null returned from cache when trying to get mapper func from {0} to {1}", dynamicType, typeof(TEntity)));
            }
            return result;
        }
    }
}
