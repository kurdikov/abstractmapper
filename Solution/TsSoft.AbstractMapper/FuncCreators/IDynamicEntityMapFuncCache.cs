﻿namespace TsSoft.AbstractMapper.FuncCreators
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Кэш делегатов, мапящих динамические типы в обычные
    /// </summary>
    public interface IDynamicEntityMapFuncCache
    {
        /// <summary>
        /// Получить делегат для преобразования объекта динамического типа в обычный
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности БД</typeparam>
        /// <param name="dynamicType">Динамический тип сущности</param>
        /// <returns>Делегат для преобразования</returns>
        [NotNull]
        Func<object, TEntity> Get<TEntity>([NotNull]Type dynamicType)
            where TEntity: class, new();
    }
}
