﻿namespace TsSoft.AbstractMapper.FuncCreators
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Helpers;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;

    internal class DynamicEntityMapFuncCreator : IDynamicEntityMapFuncCreator
    {
        [NotNull]private readonly IMapperResolver _mapperResolver;
        [NotNull] private readonly IMapperMethodHelper _mapperMethodHelper;

        [NotNull]
        private readonly MethodInfo _resolverGetDefault;

        public DynamicEntityMapFuncCreator(
            [NotNull]IMapperResolver mapperResolver, 
            [NotNull]IMemberInfoHelper memberInfoHelper, 
            [NotNull]IMapperMethodHelper mapperMethodHelper)
        {
            if (mapperResolver == null) throw new ArgumentNullException("mapperResolver");
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (mapperMethodHelper == null) throw new ArgumentNullException("mapperMethodHelper");
            _mapperResolver = mapperResolver;
            _mapperMethodHelper = mapperMethodHelper;

            _resolverGetDefault = memberInfoHelper.GetGenericDefinitionMethodInfo(() => mapperResolver.GetDynamic<object, object>());
        }
        
        public Func<object, TEntity> CreateMapperFunc<TEntity>(Type dynamicType)
            where TEntity: class, new()
        {
            if (typeof(TEntity).GetTypeInfo().IsAssignableFrom(dynamicType))
            {
                return o => (TEntity)o;
            }
            var resolverMethod = _resolverGetDefault.MakeGenericMethod(dynamicType, typeof (TEntity));
            var resolverDelegate = DelegateCreator.Create<Func<object>>(_mapperResolver, resolverMethod);
            var mapperType = typeof (IMapper<,>).MakeGenericType(dynamicType, typeof (TEntity));
            var mapper = resolverDelegate();
            var mapperMethod = _mapperMethodHelper.GetMapMethod(mapperType);
            var param = Expression.Parameter(typeof (object));
            var expression =
                Expression.Lambda<Func<object, TEntity>>(
                    Expression.Call(
                        Expression.Constant(mapper, mapperType),
                        mapperMethod,
                        new Expression[] {Expression.Convert(
                            param,
                            dynamicType)}),
                    param);
            return expression.Compile();
        }
    }
}
