﻿namespace TsSoft.AbstractMapper.FuncCreators
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Создаёт делегат для преобразования объекта заданного типа к типу TEntity с помощью копирования одноимённых свойств
    /// </summary>
    public interface IDynamicEntityMapFuncCreator
    {
        /// <summary>
        /// Создаёт делегат для преобразования объекта заданного типа к типу TEntity с помощью копирования одноимённых свойств
        /// </summary>
        /// <typeparam name="TEntity">Тип, в который преобразуется объект</typeparam>
        /// <param name="dynamicType">Тип объекта</param>
        [NotNull]
        Func<object, TEntity> CreateMapperFunc<TEntity>([NotNull]Type dynamicType)
             where TEntity : class, new();
    }
}
