﻿namespace TsSoft.AbstractMapper.RuleConverters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    internal class IgnoreRulesConverter : IIgnoreRulesConverter
    {
        [NotNull]private readonly IMemberInfoHelper _memberInfoHelper;

        public IgnoreRulesConverter([NotNull]IMemberInfoHelper memberInfoHelper)
        {
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            _memberInfoHelper = memberInfoHelper;
        }

        public IEnumerable<ValueHoldingMember> Convert<TTo>(IEnumerable<Expression<Func<TTo, object>>> ignoreRules)
        {
            if (ignoreRules == null)
            {
                return Enumerable.Empty<ValueHoldingMember>();
            }
            return ignoreRules.Select(_memberInfoHelper.GetValueHoldingMember);
        }
    }
}
