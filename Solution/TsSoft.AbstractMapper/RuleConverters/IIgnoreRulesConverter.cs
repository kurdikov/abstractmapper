﻿namespace TsSoft.AbstractMapper.RuleConverters
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Преобразователь правил игнорирования
    /// </summary>
    public interface IIgnoreRulesConverter
    {
        /// <summary>
        /// Получить из правил игнорирования свойства, которые не следует заполнять
        /// </summary>
        /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
        /// <param name="ignoreRules">Правила игнорирования</param>
        /// <returns>Перечисление игнорируемых свойств типа</returns>
        [NotNull]
        IEnumerable<ValueHoldingMember> Convert<TTo>(IEnumerable<Expression<Func<TTo, object>>> ignoreRules);
    }
}
