﻿namespace TsSoft.AbstractMapper.RuleConverters
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules;

    /// <summary>
    /// Преобразователь правил маппинга
    /// </summary>
    public interface IMapRulesConverter
    {
        /// <summary>
        /// Преобразует правила преобразования в удобный для рефлексии объект
        /// </summary>
        /// <param name="fullMapRules">Правила преобразования</param>
        /// <returns>Свойство, в которое идёт преобразование => описание пути</returns>
        [NotNull]
        IReadOnlyCollection<ReflectedMapRule> Convert([NotNull]IFullMapRules fullMapRules);
    }
}
