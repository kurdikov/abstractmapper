﻿namespace TsSoft.AbstractMapper.RuleConverters
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;

    internal class MapRulesConverter : IMapRulesConverter
    {
        [NotNull]private readonly IPathParser _pathParser;

        public MapRulesConverter([NotNull] IPathParser pathParser)
        {
            if (pathParser == null) throw new ArgumentNullException("pathParser");
            _pathParser = pathParser;
        }

        public IReadOnlyCollection<ReflectedMapRule> Convert(IFullMapRules fullMapRules)
        {
            var result = new List<ReflectedMapRule>(fullMapRules.Count);
            foreach (var rule in fullMapRules)
            {
                if (rule == null)
                {
                    throw new InvalidRulesException("null rule");
                }
                if (rule.Convert == null)
                {
                    throw new InvalidRulesException(string.Format("Convert is null for rule mapping into {0}", rule.ToMember));
                }
                if (rule.FromAccess == null)
                {
                    throw new InvalidRulesException(string.Format("FromAccess is null for rule mapping into {0}", rule.ToMember));
                }
                var from = new List<ReflectedMapSource>(rule.FromAccess.Count);
                var expectedParameterCount = rule.FromPropertyCount + rule.ProcessorCount + (rule.ToPassedToMap ? 1 : 0);
                if (rule.Convert.Parameters.Count != expectedParameterCount)
                {
                    throw new InvalidRulesException(string.Format(
                        "Invalid map rule for property {0}: (number of accessed properties {1}) + (number of inner processors {2}) + (TTo {3}) != (number of properties processed by convert lambda {4})",
                        rule.ToMember,
                        rule.FromPropertyCount,
                        rule.ProcessorCount,
                        rule.ToPassedToMap ? 1 : 0,
                        rule.Convert.Parameters.Count));
                }
                foreach (var fromAccess in rule.FromAccess)
                {
                    if (fromAccess == null)
                    {
                        throw new InvalidRulesException(string.Format("FromAccess contains null for rule mapping into {0}", rule.ToAccess));
                    }
                    if (fromAccess.FromAccess.ReturnType == null)
                    {
                        throw new InvalidRulesException(string.Format("FromAccess contains lambda {1} with null return type for rule mapping into {0}", rule.ToAccess, fromAccess));
                    }
                    from.Add(new ReflectedMapSource
                    {
                        Path = _pathParser.Parse(fromAccess.FromAccess),
                        PathType = fromAccess.FromAccess.ReturnType,
                        ProcessorDescription = fromAccess.ProcessorDescription,
                        SkipAssignmentOnNull = fromAccess.SkipAssignmentOnNull,
                    });
                }
                var convert = rule.Convert;
                if (rule.Default != null && from.Count == 1 && from[0] != null && from[0].PathType.IsNullableType())
                {
                    if (rule.Default.Parameters.Count > 0)
                    {
                        throw new InvalidRulesException(string.Format("Default with parameters in rule for {0}: {1}", rule.ToMember, rule.Default));
                    }
                    var checkedParam = rule.Convert.ParameterAt(0);
                    convert = Expression.Lambda(
                        Expression.Condition(
                            Expression.NotEqual(checkedParam, Expression.Constant(null, checkedParam.Type)),
                            convert.Body,
                            rule.Default.Body),
                        convert.Parameters).ThrowIfNull("convert");
                }
                result.Add(new ReflectedMapRule
                {
                    To = rule.ToMember,
                    ToPassedToMap = rule.ToPassedToMap,
                    Convert = convert,
                    From = from,
                    MapperDescription = rule.MapperDescription,
                });
            }
            return result;
        }
    }
}
