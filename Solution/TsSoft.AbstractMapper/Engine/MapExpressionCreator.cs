﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.RuleConverters;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.Models.AbstractMapper;

    internal class MapExpressionCreator : IMapExpressionCreator
    {
        [NotNull]private readonly IMapRulesConverter _mapRulesConverter;
        [NotNull]private readonly IExceptionWrapperHelper _exceptionWrapperHelper;
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;

        public MapExpressionCreator(
            [NotNull] IMapRulesConverter mapRulesConverter,
            [NotNull] IExceptionWrapperHelper exceptionWrapperHelper,
            [NotNull] IExpressionBuilder expressionBuilder)
        {
            if (mapRulesConverter == null) throw new ArgumentNullException("mapRulesConverter");
            if (exceptionWrapperHelper == null) throw new ArgumentNullException("exceptionWrapperHelper");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            _mapRulesConverter = mapRulesConverter;
            _exceptionWrapperHelper = exceptionWrapperHelper;
            _expressionBuilder = expressionBuilder;
        }

        public MapperExpression<TFrom, TTo> GetExpression<TFrom, TTo>(
            IFullMapRules mapRules,
            bool addCatchBlocks,
            string exceptionMessage)
        {
            var mappedPaths = new List<MappedPathDescription>();
            var convertedRules = _mapRulesConverter.Convert(mapRules);

            var toType = mapRules.TargetType;
            var fromType = typeof(TFrom);

            var newExpression = Expression.New(toType);
            var resultVariable = Expression.Variable(toType, "result");
            var fromParam = Expression.Parameter(fromType, "from");

            var mapStatements = new List<Expression>();
            Expression assignResult = Expression.Assign(resultVariable, newExpression);
            mapStatements.Add(addCatchBlocks ? _exceptionWrapperHelper.Wrap(assignResult, message: exceptionMessage) : assignResult);

            foreach (var rule in convertedRules)
            {
                if (rule == null)
                {
                    throw new InvalidRulesException("rule is null");
                }
                if (!rule.To.ValueType.GetTypeInfo().IsAssignableFrom(rule.Convert.ReturnType))
                {
                    throw new InvalidRulesException(string.Format("Invalid convert: lambda returns {0} and it is not assignable to {1}", rule.Convert.ReturnType, rule.To.ValueType));
                }
                var toPropExpr = Expression.MakeMemberAccess(resultVariable, rule.To.Member);
                var reusedParams = ReusedParameterFinderVisitor.FindReusedParameters(rule.Convert);
                var variables = new List<ParameterExpression>();
                var variableAssignments = new List<Expression>();
                var replacements = new Dictionary<ParameterExpression, Expression>();
                // convert arguments that come from TFrom
                int paramIndex;
                Expression precondition = null;
                for (paramIndex = 0; paramIndex < rule.From.Count; ++paramIndex)
                {
                    var from = rule.From[paramIndex];
                    if (from == null)
                    {
                        throw new InvalidRulesException("From contains null");
                    }
                    mappedPaths.Add(new MappedPathDescription
                    {
                        ToPath = rule.To,
                        Path = from.Path,
                        ProcessorDescription = from.ProcessorDescription,
                        MapperDescription = rule.MapperDescription,
                    });
                    var fromPropParam = rule.Convert.ParameterAt(paramIndex);
                    var fromProp = _expressionBuilder.BuildPathWithNullChecks(
                        fromParam, from.Path.Elements, false, from.PathType);
                    // the next line is needed because the path lambda parameter may be used in conditions
                    fromProp = ParameterReplacerVisitor.Replace(fromProp, from.Path.Start, fromParam);
                    if (reusedParams.Contains(fromPropParam) || from.SkipAssignmentOnNull && from.PathType.IsNullableType())
                    {
                        var newVariable = Expression.Variable(fromPropParam.Type);
                        variables.Add(newVariable);
                        variableAssignments.Add(Expression.Assign(newVariable, fromProp));
                        replacements[fromPropParam] = newVariable;
                        if (from.SkipAssignmentOnNull)
                        {
                            var currentPrecondition = Expression.NotEqual(
                                fromProp, Expression.Constant(null, from.PathType));
                            precondition = precondition != null
                                ? Expression.AndAlso(precondition, currentPrecondition)
                                : currentPrecondition;
                        }
                    }
                    else
                    {
                        replacements[fromPropParam] = fromProp;
                    }
                }
                if (rule.From.Count == 0)
                {
                    mappedPaths.Add(new MappedPathDescription
                    {
                        ToPath = rule.To,
                    });
                }
                // convert arguments that are processors
                for (int i = 0; i < rule.From.Count; ++i)
                {
                    var processorDesc = rule.From[i].ThrowIfNull("rule").ProcessorDescription;
                    if (processorDesc == null)
                    {
                        continue;
                    }
                    var procParam = rule.Convert.ParameterAt(paramIndex);
                    var proc = Expression.Constant(processorDesc.Processor);
                    replacements[procParam] = proc;
                    ++paramIndex;
                }
                // convert TTo argument (after map rule)
                if (rule.ToPassedToMap)
                {
                    var toParam = rule.Convert.ParameterAt(paramIndex);
                    replacements[toParam] = resultVariable;
                }
                var body = ParameterReplacerVisitor.Replace(rule.Convert.Body, replacements);
                var assigned = variables.Count > 0
                    ? Expression.Block(variables, variableAssignments.AppendOne(body))
                    : body;
                Expression assignment = Expression.Assign(toPropExpr, assigned);
                Expression assignmentWithCheck = precondition != null
                    ? Expression.IfThen(precondition, assignment)
                    : assignment;
                mapStatements.Add(addCatchBlocks ? _exceptionWrapperHelper.Wrap(assignmentWithCheck) : assignmentWithCheck);
            }
            mapStatements.Add(resultVariable);
            var block = Expression.Block(new[] { resultVariable }, mapStatements);
            return new MapperExpression<TFrom, TTo>
            {
                Expression = Expression.Lambda<Func<TFrom, TTo>>(block, new[] { fromParam }),
                MappedPaths = mappedPaths,
            };
        }
    }
}
