﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Строит выражения преобразования коллекций
    /// </summary>
    public interface IDirectCollectionConvertExpressionCreator
    {
        /// <summary>
        /// Построить выражение для преобразования коллекции
        /// </summary>
        /// <param name="source">Исходная коллекция</param>
        /// <param name="mapperExpression">Маппер</param>
        /// <param name="sourceType">Тип исходной коллекции</param>
        /// <param name="sourceElementType">Тип элемента исходной коллекции</param>
        /// <param name="targetType">Тип результата</param>
        /// <param name="targetElementType">Тип элемента результата</param>
        /// <returns>Преобразованная коллекция</returns>
        Expression Convert(
            [NotNull] Expression source,
            [NotNull] Expression mapperExpression,
            [NotNull] Type sourceType,
            [NotNull] Type sourceElementType,
            [NotNull] Type targetType,
            [NotNull] Type targetElementType);
    }
}
