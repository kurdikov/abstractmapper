﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules;

    class FullMapRulesCreator : IFullMapRulesCreator
    {
        [NotNull] private readonly IImplicitMapRulesProvider _implicitMapRulesProvider;
        [NotNull] private readonly IAutoConvertProvider _autoConvertProvider;

        public FullMapRulesCreator([NotNull] IImplicitMapRulesProvider implicitMapRulesProvider, [NotNull] IAutoConvertProvider autoConvertProvider)
        {
            if (implicitMapRulesProvider == null) throw new ArgumentNullException("implicitMapRulesProvider");
            if (autoConvertProvider == null) throw new ArgumentNullException("autoConvertProvider");
            _implicitMapRulesProvider = implicitMapRulesProvider;
            _autoConvertProvider = autoConvertProvider;
        }

        public IFullMapRules MakeFullRules<TFrom, TTo>(
            IMapRules<TTo, TFrom> explicitRules,
            IIgnoreRules<TTo> ignoreRules,
            AutoPropertiesBehavior autoPropertiesBehavior,
            InnerMapperStrategy innerMapperStrategy)
        {
            var rules = _implicitMapRulesProvider.AddImplicitRules(explicitRules, ignoreRules,
                autoPropertiesBehavior == AutoPropertiesBehavior.Strict);
            _autoConvertProvider.SetAutoConvert(rules, innerMapperStrategy);
            return rules;
        }
    }
}
