﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.Helpers;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models.AbstractMapper;

    internal class ConvertExpressionCreator : IConvertExpressionCreator
    {
        [NotNull]private readonly ICollectionConvertExpressionCreator _collectionConvertExpressionCreator;
        [NotNull]private readonly IMapperResolver _mapperResolver;
        [NotNull]private readonly IPrimitiveConvertExpressionCreator _primitiveExpressionCreator;
        [NotNull] private readonly IMapperMethodHelper _mapperMethodHelper;

        [NotNull]private readonly MethodInfo _resolverTryGet;
        [NotNull]private readonly MethodInfo _resolverGetDynamic;
        [NotNull]private readonly ConstructorInfo _exceptionConstructor;

        public ConvertExpressionCreator(
            [NotNull] IMemberInfoHelper memberInfoHelper,
            [NotNull]ICollectionConvertExpressionCreator collectionConvertExpressionCreator, 
            [NotNull]IMapperResolver mapperResolver,
            [NotNull]IPrimitiveConvertExpressionCreator primitiveExpressionCreator, 
            [NotNull]IMapperMethodHelper mapperMethodHelper)
        {
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (collectionConvertExpressionCreator == null) throw new ArgumentNullException("collectionConvertExpressionCreator");
            if (mapperResolver == null) throw new ArgumentNullException("mapperResolver");
            if (primitiveExpressionCreator == null) throw new ArgumentNullException("primitiveExpressionCreator");
            if (mapperMethodHelper == null) throw new ArgumentNullException("mapperMethodHelper");
            _collectionConvertExpressionCreator = collectionConvertExpressionCreator;
            _mapperResolver = mapperResolver;
            _primitiveExpressionCreator = primitiveExpressionCreator;
            _mapperMethodHelper = mapperMethodHelper;

            _resolverTryGet = memberInfoHelper.GetGenericDefinitionMethodInfo(() => _mapperResolver.TryGet<object, object>());
            _resolverGetDynamic = memberInfoHelper.GetGenericDefinitionMethodInfo(() => _mapperResolver.GetTypeDeterminingMapper<object>());
            _exceptionConstructor = memberInfoHelper.GetConstructorInfo(() => new AbstractMapperResolverException(null));
        }

        public Expression Convert(
            Expression source,
            Type targetType,
            bool useCollectionMapper,
            InnerMapperStrategy innerMapperStrategy,
            out MapperDescription description)
        {
            MapperExpression mapperExpression;
            var result = ConvertInternal(source, targetType, useCollectionMapper, innerMapperStrategy,
                out mapperExpression);
            if (mapperExpression != null && mapperExpression.Variable != null)
            {
                result = Expression.Block(
                    new[] {mapperExpression.Variable},
                    mapperExpression.Expression,
                    result);
                description = mapperExpression.Description;
            }
            else
            {
                description = mapperExpression != null ? mapperExpression.Description : null;
            }
            return result;
        }

        private Expression ConvertInternal(
            [NotNull]Expression source,
            [NotNull]Type targetType,
            bool useCollectionMapper,
            InnerMapperStrategy innerMapperStrategy,
            out MapperExpression mapperExpression)
        {
            var sourceType = source.Type;
            Expression result;
            if (sourceType != targetType
                && (sourceType.IsReferenceNotStringType() || targetType.IsReferenceNotStringType()))
            {
                var fromIsCollection = sourceType.IsGenericEnumerable();
                var toIsCollection = targetType.IsGenericEnumerable();
                if (fromIsCollection && toIsCollection && !useCollectionMapper)
                {
                    // to = new(from.Select(_mapper.Map))
                    var sourceElementType = sourceType.GetGenericEnumerableArgument();
                    var targetElementType = targetType.GetGenericEnumerableArgument();
                    var sourceElementParameter = Expression.Parameter(sourceElementType);
                    var elementConvert = ConvertInternal(sourceElementParameter, targetElementType, false, innerMapperStrategy,
                        out mapperExpression);
                    if (elementConvert == null)
                    {
                        throw new InvalidOperationException(string.Format("Unable to map from {0} to {1}", sourceElementType, targetElementType));
                    }
                    result = _collectionConvertExpressionCreator.Convert(
                        source,
                        elementConvert,
                        sourceElementParameter,
                        sourceType, sourceElementType, targetType, targetElementType);
                }
                else
                {
                    // to = Map(from)
                    mapperExpression = GetMapperExpression(
                        sourceType, targetType, fromIsCollection, toIsCollection, innerMapperStrategy);
                    var mapMethod = _mapperMethodHelper.GetMapMethod(mapperExpression.UsableInstance.Type);
                    result = Expression.Call(
                        mapperExpression.UsableInstance,
                        mapMethod,
                        new[] { source });
                }
            }
            else
            {
                if (!_primitiveExpressionCreator.TryGetExpression(source, targetType, out result))
                {
                    result = null;
                }
                mapperExpression = null;
            }
            return result;
        }

        private class MapperExpression
        {
            public ParameterExpression Variable { get; private set; }

            [NotNull]
            public Expression Expression { get; private set; }

            [NotNull]
            public Expression UsableInstance { get { return Variable ?? Expression; } }

            [NotNull]
            public MapperDescription Description { get; private set; }

            public MapperExpression(ParameterExpression variable, [NotNull] Expression expression, [NotNull]MapperDescription description)
            {
                if (expression == null) throw new ArgumentNullException("expression");
                if (description == null) throw new ArgumentNullException("description");
                Variable = variable;
                Expression = expression;
                Description = description;
            }
        }

        [NotNull]
        private MapperExpression GetMapperExpression(
            [NotNull]Type sourceType,
            [NotNull]Type targetType,
            bool sourceIsCollection,
            bool targetIsCollection,
            InnerMapperStrategy innerMapperStrategy)
        {
            MethodInfo resolverMethod = innerMapperStrategy.FlagEquals(InnerMapperStrategy.DynamicTypes)
                ? _resolverGetDynamic.MakeGenericMethod(targetType)
                : _resolverTryGet.MakeGenericMethod(sourceType, targetType);
            InnerMapperExpressionCreator creator = innerMapperStrategy.FlagEquals(InnerMapperStrategy.ResolveOnMapperBuilding)
                ? (InnerMapperExpressionCreator)GetResolvedInnerMapperExpression
                : GetInnerMapperExpression;
            var description = new MapperDescription(
                mapperFromType: sourceType,
                mapperFromSingleType: sourceIsCollection ? sourceType.GetGenericEnumerableArgument() : sourceType,
                mapperToType: targetType,
                mapperToSingleType: targetIsCollection ? targetType.GetGenericEnumerableArgument() : targetType);
            var mapperType = typeof(IMapper<,>).MakeGenericType(sourceType, targetType);
            var mapperExpression = creator(description, resolverMethod, mapperType).ThrowIfNull();
            return mapperExpression;
        }

        private MapperExpression GetInnerMapperExpression(
            [NotNull]MapperDescription description,
            [NotNull]MethodInfo resolverMethod,
            [NotNull]Type mapperType)
        {
            var expressions = new List<Expression>();
            var mapperVariable = Expression.Variable(mapperType);
            expressions.Add(
                Expression.Assign(
                    mapperVariable,
                    Expression.Call(
                        Expression.Constant(_mapperResolver),
                        resolverMethod)));
            expressions.Add(
                Expression.IfThen(
                    Expression.Equal(
                        mapperVariable,
                        Expression.Constant(null, mapperType)),
                    Expression.Throw(
                        Expression.New(
                            _exceptionConstructor,
                            Expression.Constant(mapperType, typeof(Type))))));
            expressions.Add(mapperVariable);
            return new MapperExpression(mapperVariable, Expression.Block(expressions), description);
        }

        private MapperExpression GetResolvedInnerMapperExpression([NotNull]MapperDescription description, [NotNull]MethodInfo resolverMethod, [NotNull]Type mapperType)
        {
            var resolverDelegate = DelegateCreator.Create<Func<object>>(_mapperResolver, resolverMethod);
            var mapper = resolverDelegate();
            if (mapper == null)
            {
                throw new AbstractMapperResolverException(mapperType);
            }
            description.MapperObject = mapper;
            return new MapperExpression(null, Expression.Constant(mapper, mapperType), description);
        }

        private delegate MapperExpression InnerMapperExpressionCreator([NotNull]MapperDescription description, [NotNull]MethodInfo resolverMethod, [NotNull]Type mapperType);
    }
}
