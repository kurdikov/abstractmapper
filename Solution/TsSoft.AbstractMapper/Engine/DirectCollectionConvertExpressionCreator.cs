namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;

    class DirectCollectionConvertExpressionCreator : IDirectCollectionConvertExpressionCreator
    {
        private class ConversionDescription
        {
            [NotNull]public Type TargetGenericDefinition { get; private set; }
            [NotNull]public MethodInfo Copy { get; private set; }
            [NotNull]public MethodInfo Convert { get; private set; }
            [CanBeNull]public MethodInfo CopyWithKnownCount { get; private set; }
            [CanBeNull]public MethodInfo ConvertWithKnownCount { get; private set; }
            public bool TryConstructor { get; private set; }

            public bool IsSuitableFor([NotNull]Type targetType, [NotNull]Type targetElementType)
            {
                if (TargetGenericDefinition.GetTypeInfo().IsInterface)
                {
                    return targetType == TargetGenericDefinition.MakeGenericType(targetElementType);
                }
                if (TargetGenericDefinition.IsArray)
                {
                    return targetType.IsArray;
                }
                return targetType.GetTypeInfo().IsAssignableFrom(TargetGenericDefinition.MakeGenericType(targetElementType));
            }

            public ConstructorInfo TryFindSuitableConstructor(
                [NotNull] Type targetType,
                [NotNull] Type targetElementType)
            {
                if (!TryConstructor)
                {
                    return null;
                }
                return targetType.GetTypeInfo().GetConstructor(new[] {TargetGenericDefinition.MakeGenericType(targetElementType)});
            }

            public Expression TryMakeExpression(
                [NotNull]Expression source,
                [NotNull]Type sourceElementType,
                [NotNull]Type targetType,
                [NotNull]Type targetElementType,
                [CanBeNull]Expression mapperExpression,
                [CanBeNull]Expression countExpression,
                [NotNull]Expression converter)
            {
                if (!IsSuitableFor(targetType, targetElementType))
                {
                    return null;
                }
                return MakeExpression(
                    source, sourceElementType, targetElementType, mapperExpression, countExpression, converter);
            }

            private Expression MakeExpression(
                [NotNull]Expression source,
                [NotNull]Type sourceElementType,
                [NotNull]Type targetElementType,
                [CanBeNull]Expression mapperExpression,
                [CanBeNull]Expression countExpression,
                [NotNull]Expression converter)
            {
                if (mapperExpression == null)
                {
                    return countExpression != null && CopyWithKnownCount != null
                        ? Expression.Call(converter, CopyWithKnownCount.MakeGenericMethod(targetElementType), source, countExpression)
                        : Expression.Call(converter, Copy.MakeGenericMethod(targetElementType), new[] { source });
                }
                return countExpression != null && ConvertWithKnownCount != null
                    ? Expression.Call(converter, ConvertWithKnownCount.MakeGenericMethod(sourceElementType, targetElementType), source, mapperExpression, countExpression)
                    : Expression.Call(converter, Convert.MakeGenericMethod(sourceElementType, targetElementType), source, mapperExpression);
            }

            public Expression TryMakeExpressionWithConstructor(
                [NotNull]Expression source,
                [NotNull]Type sourceElementType,
                [NotNull]Type targetType,
                [NotNull]Type targetElementType,
                [CanBeNull]Expression mapperExpression,
                [CanBeNull]Expression countExpression,
                [NotNull]Expression converter)
            {
                var constructor = TryFindSuitableConstructor(targetType, targetElementType);
                if (constructor == null)
                {
                    return null;
                }
                return Expression.New(constructor, MakeExpression(source, sourceElementType, targetElementType, mapperExpression, countExpression, converter));
            }

            public ConversionDescription(
                [NotNull] Type targetGenericDefinition,
                bool tryConstructor,
                [NotNull] MethodInfo copy,
                [NotNull] MethodInfo convert,
                MethodInfo copyWithKnownCount = null,
                MethodInfo convertWithKnownCount = null)
            {
                if (targetGenericDefinition == null) throw new ArgumentNullException("targetGenericDefinition");
                if (copy == null) throw new ArgumentNullException("copy");
                if (convert == null) throw new ArgumentNullException("convert");
                TargetGenericDefinition = targetGenericDefinition;
                TryConstructor = tryConstructor;
                Copy = copy;
                Convert = convert;
                CopyWithKnownCount = copyWithKnownCount;
                ConvertWithKnownCount = convertWithKnownCount;
            }
        }

        [NotNull] private readonly ConversionDescription[] _descriptions;
        [NotNull] private readonly ConstantExpression _converterExpression;

        public DirectCollectionConvertExpressionCreator(
            [NotNull] ICollectionConverter converter,
            [NotNull] IMemberInfoHelper memberInfo)
        {
            if (converter == null) throw new ArgumentNullException("converter");
            if (memberInfo == null) throw new ArgumentNullException("memberInfo");

            _converterExpression = Expression.Constant(converter);

            _descriptions = new[]
            {
                new ConversionDescription(
                    typeof(IEnumerable<>),
                    true,
                    memberInfo.GetGenericDefinitionMethodInfo(() => converter.MakeEnumerable<object>(null)),
                    memberInfo.GetGenericDefinitionMethodInfo(() => converter.MakeEnumerable<object, object>(null, null))),
                new ConversionDescription(
                    typeof(List<>),
                    true,
                    memberInfo.GetGenericDefinitionMethodInfo(() => converter.MakeList<object>(null)),
                    memberInfo.GetGenericDefinitionMethodInfo(() => converter.MakeList<object, object>(null, null)),
                    memberInfo.GetGenericDefinitionMethodInfo(() => converter.MakeList<object>(null, 0)),
                    memberInfo.GetGenericDefinitionMethodInfo(() => converter.MakeList<object, object>(null, null, 0))),
                new ConversionDescription(
                    typeof(object[]),
                    false,
                    memberInfo.GetGenericDefinitionMethodInfo(() => converter.MakeArray<object>(null)),
                    memberInfo.GetGenericDefinitionMethodInfo(() => converter.MakeArray<object, object>(null, null)),
                    memberInfo.GetGenericDefinitionMethodInfo(() => converter.MakeArray<object>(null, 0)),
                    memberInfo.GetGenericDefinitionMethodInfo(() => converter.MakeArray<object, object>(null, null, 0))),
            };
        }

        public Expression Convert(
            Expression source,
            Expression mapperExpression,
            Type sourceType,
            Type sourceElementType,
            Type targetType,
            Type targetElementType)
        {
            Expression sourceCount = null;
            var sourceTypeDefinition = sourceType.GetGenericTypeDefinition();

            if (sourceTypeDefinition == typeof(IReadOnlyCollection<>))
            {
                sourceCount = Expression.Property(
                    source,
                    "Count");
            }
            else if (sourceTypeDefinition == typeof(ICollection<>))
            {
                sourceCount = Expression.Property(
                    source,
                    "Count");
            }

            var withoutConstructor = _descriptions
                .Where(d => d != null)
                .Select(d => d.TryMakeExpression(
                    source, sourceElementType, targetType, targetElementType, mapperExpression, sourceCount, _converterExpression))
                .FirstOrDefault(e => e != null);
            if (withoutConstructor != null)
            {
                return withoutConstructor;
            }

            var withConstructor = _descriptions
                .Where(d => d != null)
                .Select(d => d.TryMakeExpressionWithConstructor(
                    source, sourceElementType, targetType, targetElementType, mapperExpression, sourceCount, _converterExpression))
                .FirstOrDefault(e => e != null);
            if (withConstructor != null)
            {
                return withConstructor;
            }

            throw new ArgumentException(string.Format(
                "An instance of type {0} may not be constructed from neither IEnumerable nor List", targetType));
        }
    }
}
