﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.AbstractMapper.Rules.Factories;
    using TsSoft.Expressions.Helpers.Closures;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Хелпер для абстрактного маппера
    /// </summary>
    public class AbstractMapperHelper : IAbstractMapperHelper
    {
        [NotNull] private readonly IMapExpressionCreator _expressionCreator;
        [NotNull] private readonly IIgnoreRulesFactory _ignoreRulesFactory;
        [NotNull] private readonly ILambdaCompiler _lambdaCompiler;
        [NotNull] private readonly IFullMapRulesCreator _fullMapRulesCreator;

        /// <summary>
        /// Хелпер для абстрактного маппера
        /// </summary>
        public AbstractMapperHelper(
            [NotNull] IMapExpressionCreator expressionCreator,
            [NotNull] IIgnoreRulesFactory ignoreRulesFactory,
            [NotNull] ILambdaCompiler lambdaCompiler,
            [NotNull] IFullMapRulesCreator fullMapRulesCreator)
        {
            if (expressionCreator == null) throw new ArgumentNullException("expressionCreator");
            if (ignoreRulesFactory == null) throw new ArgumentNullException("ignoreRulesFactory");
            if (lambdaCompiler == null) throw new ArgumentNullException("lambdaCompiler");
            if (fullMapRulesCreator == null) throw new ArgumentNullException("fullMapRulesCreator");
            _expressionCreator = expressionCreator;
            _ignoreRulesFactory = ignoreRulesFactory;
            _lambdaCompiler = lambdaCompiler;
            _fullMapRulesCreator = fullMapRulesCreator;
        }

        /// <summary>
        /// Создать полную коллекцию правил с заполненными источниками преобразования и выражениями преобразования
        /// </summary>
        /// <typeparam name="TTo">Тип, в который идёт преобразование</typeparam>
        /// <typeparam name="TFrom">Тип, из которого идёт преобразование</typeparam>
        /// <param name="explicitRules">Указанные в маппере правила</param>
        /// <param name="ignoreRules">Правила игнорирования</param>
        /// <param name="autoPropertiesBehavior">Поведение в случае, когда в TFrom не найдены подходящие свойства для построения автоправил заполнения TTo</param>
        /// <param name="innerMapperStrategy">Как получать внутренние мапперы</param>
        /// <returns>Упорядоченный список правил преобразования</returns>
        /// <exception cref="InvalidRulesException">Не удалось построить автоправила для некоторых свойств</exception>
        public IFullMapRules MakeFullRules<TFrom, TTo>(
            IMapRules<TTo, TFrom> explicitRules,
            IIgnoreRules<TTo> ignoreRules,
            AutoPropertiesBehavior autoPropertiesBehavior,
            InnerMapperStrategy innerMapperStrategy)
        {
            return _fullMapRulesCreator.MakeFullRules(explicitRules, ignoreRules, autoPropertiesBehavior, innerMapperStrategy);
        }

        /// <summary>
        /// Создать выражение для преобразования
        /// </summary>
        /// <typeparam name="TFrom">Тип, из которого осуществляется преобразование</typeparam>
        /// <typeparam name="TTo">Тип, в который осуществляется преобразование</typeparam>
        /// <param name="fullMapRules">Правила преобразования</param>
        /// <param name="addCatchBlocks">Оборачивать ли в try-catch выражения маппинга</param>
        /// <param name="exceptionMessage">Сообщение внутреннего исключения</param>
        /// <returns>Выражение преобразования</returns>
        public MapperExpression<TFrom, TTo> CreateMapperExpression<TFrom, TTo>(
            IFullMapRules fullMapRules,
            bool addCatchBlocks,
            string exceptionMessage)
        {
            return _expressionCreator.GetExpression<TFrom, TTo>(fullMapRules, addCatchBlocks, exceptionMessage);
        }

        /// <summary>
        /// Создать правила игнорирования
        /// </summary>
        public IIgnoreRules<TTo> CreateIgnoreRules<TTo>()
        {
            return _ignoreRulesFactory.CreateRules<TTo>();
        }

        /// <summary>
        /// Создать правила игнорирования
        /// </summary>
        public IIgnoreRules<TTo> CreateIgnoreRules<TTo, TFrom>()
        {
            return _ignoreRulesFactory.CreateRules<TTo, TFrom>();
        }

        /// <summary>
        /// Скомпилировать выражение
        /// </summary>
        /// <typeparam name="TDelegate">Тип делегата</typeparam>
        /// <param name="lambda">Выражение</param>
        /// <param name="compilationType">Тип компиляции</param>
        /// <returns>Делегат</returns>
        public TDelegate Compile<TDelegate>(Expression<TDelegate> lambda, LambdaCompilationType compilationType = LambdaCompilationType.PrecompileInner)
        {
            return _lambdaCompiler.Compile(lambda, compilationType);
        }
    }
}
