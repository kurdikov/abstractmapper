﻿namespace TsSoft.AbstractMapper.Engine
{
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;

    /// <summary>
    /// Преобразует коллекции в коллекции с помощью маппера
    /// </summary>
    public class CollectionConverter : ICollectionConverter
    {
        private int? TryGetCount<TSource>([NoEnumeration]IEnumerable<TSource> source)
        {
            var collectionSource = source as ICollection<TSource>;
            if (collectionSource != null)
            {
                return collectionSource.Count;
            }
            var readonlyCollectionSource = source as IReadOnlyCollection<TSource>;
            if (readonlyCollectionSource != null)
            {
                return readonlyCollectionSource.Count;
            }
            return null;
        }

        /// <summary>
        /// Превратить перечисление в перечисление
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <returns>Перечисление-результат</returns>
        public IEnumerable<T> MakeEnumerable<T>(IEnumerable<T> source)
        {
            return source != null ? source.Select(e => e) : null;
        }

        /// <summary>
        /// Превратить перечисление в коллекцию
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <returns>Перечисление-результат</returns>
        public List<T> MakeList<T>(IEnumerable<T> source)
        {
            if (source == null)
            {
                return null;
            }
            var count = TryGetCount(source);
            var result = count.HasValue ? new List<T>(count.Value) : new List<T>();
            result.AddRange(source);
            return result;
        }

        /// <summary>
        /// Превратить перечисление в коллекцию
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="count">Количество элементов в исходном перечислении</param>
        /// <returns>Перечисление-результат</returns>
        public List<T> MakeList<T>(IEnumerable<T> source, int count)
        {
            if (source == null)
            {
                return null;
            }
            var result = new List<T>(count);
            result.AddRange(source);
            return result;
        }

        /// <summary>
        /// Превратить перечисление в массив
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <returns>Перечисление-результат</returns>
        public T[] MakeArray<T>(IEnumerable<T> source)
        {
            if (source == null)
            {
                return null;
            }
            var count = TryGetCount(source);
            if (count.HasValue)
            {
                return MakeArray(source, count.Value);
            }
            return source.ToArray();
        }

        /// <summary>
        /// Превратить перечисление в массив
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="count">Количество элементов в исходном перечислении</param>
        /// <returns>Перечисление-результат</returns>
        public T[] MakeArray<T>(IEnumerable<T> source, int count)
        {
            if (source == null)
            {
                return null;
            }
            var result = new T[count];
            using (var enumerator = source.GetEnumerator())
            {
                for (int i = 0; i < count; ++i)
                {
                    enumerator.MoveNext();
                    result[i] = enumerator.Current;
                }
            }
            return result;
        }

        /// <summary>
        /// Превратить перечисление в множество
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <returns>Перечисление-результат</returns>
        public HashSet<T> MakeHashSet<T>(IEnumerable<T> source)
        {
            if (source == null)
            {
                return null;
            }
            var result = new HashSet<T>();
            foreach (var element in source)
            {
                if (element != null)
                {
                    result.Add(element);
                }
            }
            return result;
        }

        /// <summary>
        /// Превратить перечисление в множество
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="count">Количество элементов в исходном перечислении</param>
        /// <returns>Перечисление-результат</returns>
        public HashSet<T> MakeHashSet<T>(IEnumerable<T> source, int count)
        {
            var array = MakeArray(source, count);
            return array != null ? new HashSet<T>(array) : null;
        }

        /// <summary>
        /// Превратить перечисление в перечисление
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента перечисления-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <returns>Перечисление-результат</returns>
        public IEnumerable<TTarget> MakeEnumerable<TSource, TTarget>(
            IEnumerable<TSource> source,
            IMapper<TSource, TTarget> mapper)
        {
            if (source == null || mapper == null)
            {
                return null;
            }
            return source.Select(mapper.Map);
        }

        /// <summary>
        /// Превратить перечисление в коллекцию
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента коллекции-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <returns>Коллекция-результат</returns>
        public List<TTarget> MakeList<TSource, TTarget>(IEnumerable<TSource> source, IMapper<TSource, TTarget> mapper)
        {
            if (source == null || mapper == null)
            {
                return null;
            }
            var count = TryGetCount(source);
            var list = count.HasValue ? new List<TTarget>(count.Value) : new List<TTarget>();
            FillList(list, source, mapper);
            return list;
        }

        /// <summary>
        /// Превратить перечисление в коллекцию
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента коллекции-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <param name="count">Количество элементов в исходном перечислении</param>
        /// <returns>Коллекция-результат</returns>
        public List<TTarget> MakeList<TSource, TTarget>(IEnumerable<TSource> source, IMapper<TSource, TTarget> mapper, int count)
        {
            var list = new List<TTarget>(count);
            FillList(list, source, mapper);
            return list;
        }

        private void FillList<TSource, TTarget>(List<TTarget> list, IEnumerable<TSource> source, IMapper<TSource, TTarget> mapper)
        {
            if (source == null || mapper == null || list == null)
            {
                return;
            }
            foreach (var sourceValue in source)
            {
                list.Add(mapper.Map(sourceValue));
            }
        }

        /// <summary>
        /// Превратить перечисление в массив
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента массива-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <returns>Массив-результат</returns>
        public TTarget[] MakeArray<TSource, TTarget>(
            IEnumerable<TSource> source,
            IMapper<TSource, TTarget> mapper)
        {
            if (source == null || mapper == null)
            {
                return null;
            }
            var count = TryGetCount(source);
            if (count.HasValue)
            {
                return MakeArray(source, mapper, count.Value);
            }
            return source.Select(mapper.Map).ToArray();
        }

        /// <summary>
        /// Превратить перечисление в массив
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента массива-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <param name="count">Количество элементов в исходном перечислении</param>
        /// <returns>Массив-результат</returns>
        public TTarget[] MakeArray<TSource, TTarget>(
            IEnumerable<TSource> source,
            IMapper<TSource, TTarget> mapper,
            int count)
        {
            if (source == null || mapper == null)
            {
                return null;
            }
            var result = new TTarget[count];
            using (var enumerator = source.GetEnumerator())
            {
                enumerator.MoveNext();
                for (int i = 0; i < count; ++i)
                {
                    result[i] = mapper.Map(enumerator.Current);
                }
            }
            return result;
        }

        /// <summary>
        /// Превратить перечисление в множество
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента множества-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <returns>Множество-результат</returns>
        public HashSet<TTarget> MakeHashSet<TSource, TTarget>(
            IEnumerable<TSource> source,
            IMapper<TSource, TTarget> mapper)
        {
            if (source == null || mapper == null)
            {
                return null;
            }
            var result = new HashSet<TTarget>();
            foreach (var item in source)
            {
                var targetItem = mapper.Map(item);
                if (targetItem != null)
                {
                    result.Add(targetItem);
                }
            }
            return result;
        }

        /// <summary>
        /// Превратить перечисление в множество
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента множества-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <param name="count">Количество элементов в исходном перечислении</param>
        /// <returns>Множество-результат</returns>
        public HashSet<TTarget> MakeHashSet<TSource, TTarget>(IEnumerable<TSource> source, IMapper<TSource, TTarget> mapper, int count)
        {
            var array = MakeArray(source, mapper, count);
            return array != null ? new HashSet<TTarget>(array) : null;
        }
    }
}
