﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Строит выражение для преобразования выражения к другому типу
    /// </summary>
    public interface IConvertExpressionCreator
    {
        /// <summary>
        /// Построить выражение для приведения выражения к типу
        /// </summary>
        /// <param name="source">Исходное выражение</param>
        /// <param name="targetType">Тип, к которому выражение приводится</param>
        /// <param name="useCollectionMapper">Использовать маппер коллекции в коллекцию вместо поэлементного маппера</param>
        /// <param name="innerMapperStrategy">Как мапить объекты</param>
        /// <param name="mapperDescription">Описание внутреннего маппера, null в случае примитивного маппинга</param>
        /// <returns>Выражение для маппинга</returns>
        /// <remarks>Реализация предполагает, что содержимое source отлично от null</remarks>
        Expression Convert(
            [NotNull]Expression source, 
            [NotNull]Type targetType, 
            bool useCollectionMapper, 
            InnerMapperStrategy innerMapperStrategy,
            out MapperDescription mapperDescription);
    }
}
