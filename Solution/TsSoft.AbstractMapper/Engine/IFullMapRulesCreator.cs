﻿namespace TsSoft.AbstractMapper.Engine
{
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.Rules;

    /// <summary>
    /// Дополняет явно указанные правила преобразования автоправилами
    /// </summary>
    public interface IFullMapRulesCreator
    {
        /// <summary>
        /// Создать полную коллекцию правил с заполненными источниками преобразования и выражениями преобразования
        /// </summary>
        /// <typeparam name="TTo">Тип, в который идёт преобразование</typeparam>
        /// <typeparam name="TFrom">Тип, из которого идёт преобразование</typeparam>
        /// <param name="explicitRules">Указанные в маппере правила</param>
        /// <param name="ignoreRules">Правила игнорирования</param>
        /// <param name="autoPropertiesBehavior">Поведение в случае, когда в TFrom не найдены подходящие свойства для построения автоправил заполнения TTo</param>
        /// <param name="innerMapperStrategy">Как получать внутренние мапперы</param>
        /// <returns>Упорядоченный список правил преобразования</returns>
        /// <exception cref="InvalidRulesException">Не удалось построить автоправила для некоторых свойств</exception>
        [NotNull]
        IFullMapRules MakeFullRules<TFrom, TTo>(
            IMapRules<TTo, TFrom> explicitRules,
            IIgnoreRules<TTo> ignoreRules,
            AutoPropertiesBehavior autoPropertiesBehavior,
            InnerMapperStrategy innerMapperStrategy);
    }
}
