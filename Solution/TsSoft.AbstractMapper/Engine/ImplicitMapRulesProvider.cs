namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.RuleConverters;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.DynamicTypes;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    class ImplicitMapRulesProvider : IImplicitMapRulesProvider
    {
        [NotNull]private readonly IMemberInfoHelper _memberInfoHelper;
        [NotNull]private readonly ICorrespondingMemberHelper _correspondingMemberHelper;
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;
        [NotNull]private readonly IInterfaceImplementationProvider _interfaceImplementationProvider;

        public ImplicitMapRulesProvider(
            [NotNull] IMemberInfoHelper memberInfoHelper,
            [NotNull] ICorrespondingMemberHelper correspondingMemberHelper,
            [NotNull] IExpressionBuilder expressionBuilder,
            [NotNull] IInterfaceImplementationProvider interfaceImplementationProvider)
        {
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (correspondingMemberHelper == null) throw new ArgumentNullException("correspondingMemberHelper");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (interfaceImplementationProvider == null) throw new ArgumentNullException("interfaceImplementationProvider");
            _memberInfoHelper = memberInfoHelper;
            _correspondingMemberHelper = correspondingMemberHelper;
            _expressionBuilder = expressionBuilder;
            _interfaceImplementationProvider = interfaceImplementationProvider;
        }

        [NotNull]
        private ValueHoldingMember GetToMember([NotNull]Type toType, LambdaExpression toAccess, bool checkWritability = true)
        {
            if (toAccess == null)
            {
                throw new InvalidRulesException("rule.ToAccess is null");
            }
            var member = _memberInfoHelper.GetValueHoldingMember(toAccess);
            member = _correspondingMemberHelper.GetImplementationIfInterfaceProperty(toType, member);
            if (checkWritability && !member.IsWriteable)
            {
                throw new InvalidRulesException(string.Format("Member {0} defined on {1} is not writeable", member, toType));
            }
            return member;
        }

        public FullMapRules AddImplicitRules<TFrom, TTo>(
            IMapRules<TTo, TFrom> explicitRules,
            IIgnoreRules<TTo> ignoreRules,
            bool throwOnError)
        {
            var sourceType = typeof(TFrom);
            var targetType = typeof(TTo).GetTypeInfo().IsInterface
                ? _interfaceImplementationProvider.GetInterfaceImplementation(typeof(TTo))
                : typeof(TTo);
            var explicitRulesWithMembers = (explicitRules ?? new EmptyMapRules<TTo, TFrom>())
                .Where(er => er != null)
                .Select(er => new MemberedMapRule(er, GetToMember(targetType, er.ToAccess)))
                .ToList();
            var ignoredToMembers = ignoreRules.Select(ir => GetToMember(typeof(TTo), ir, false));
            var membersToMap = new HashSet<ValueHoldingMember>(ValueHoldingMember
                .GetValueHoldingMembers(targetType)
                .Where(vhm => vhm != null && vhm.IsWriteable));
            membersToMap.ExceptWith(ignoredToMembers);
            var mappedMembers = new HashSet<ValueHoldingMember>();

            var result = new FullMapRules(targetType);

            foreach (var implicitlyMappedMember in membersToMap.Except(
                explicitRulesWithMembers.Where(er => er != null).Select(er => er.ToMember)))
            {
                if (implicitlyMappedMember == null)
                {
                    continue;
                }
                var correspondingMember = GetFromMember(
                    typeof(TFrom),
                    implicitlyMappedMember,
                    throwOnError,
                    "Matching property for {0} not found in {1} or provided map rules");
                if (correspondingMember == null)
                {
                    continue;
                }
                result.Add(new MemberedMapRule(implicitlyMappedMember)
                {
                    ToAccess = _expressionBuilder.BuildMemberAccessExpression(targetType, implicitlyMappedMember),
                    FromAccess = new[]
                    {
                        new MapSource(_expressionBuilder.BuildMemberAccessExpression(sourceType, correspondingMember)) {SkipAssignmentOnNull = true},
                    },
                });
                mappedMembers.Add(implicitlyMappedMember);
            }
            // load explicit regular rules
            LoadRules(sourceType, result, explicitRulesWithMembers.Where(er => er != null && !er.ToPassedToMap), membersToMap, mappedMembers);
            // load explicit aftermap rules
            LoadRules(sourceType, result, explicitRulesWithMembers.Where(er => er != null && er.ToPassedToMap), membersToMap, mappedMembers);
            return result;
        }

        private ValueHoldingMember GetFromMember(
            [NotNull]Type fromType,
            [NotNull]ValueHoldingMember toMember,
            bool throwOnFailure,
            [NotNull]string exceptionMessage)
        {
            var correspondingMember = _correspondingMemberHelper.TryGetCorrespondingMember(fromType, toMember.Member);
            if (correspondingMember == null && throwOnFailure)
            {
                throw new InvalidRulesException(string.Format(
                    exceptionMessage,
                    toMember,
                    fromType));
            }
            return correspondingMember;
        }

        private void LoadRules(
            [NotNull]Type sourceType,
            [NotNull]FullMapRules target,
            [NotNull]IEnumerable<MemberedMapRule> source,
            [NotNull]ISet<ValueHoldingMember> membersToMap,
            [NotNull]ISet<ValueHoldingMember> mappedMembers)
        {
            foreach (var rule in source)
            {
                if (rule == null)
                {
                    continue;
                }
                if (!membersToMap.Contains(rule.ToMember) && rule.ToMember.IsPublic)
                {
                    throw new InvalidRulesException(string.Format("{0} is present in both MapRules and IgnoreRules", rule.ToMember));
                }
                if (!mappedMembers.Add(rule.ToMember))
                {
                    throw new InvalidRulesException(string.Format("Multiple map rules for {0}", rule.ToMember));
                }
                if (rule.FromAccess == null)
                {
                    var member = GetFromMember(
                        sourceType, rule.ToMember, true,
                        "Matching property for {0} not found in {1}, and the provided map rule omits path")
                        .ThrowIfNull("member");
                    rule.FromAccess = new[]
                    {
                        new MapSource(_expressionBuilder.BuildMemberAccessExpression(sourceType, member)),
                    };
                }
                target.Add(rule);
            }
        }
    }
}
