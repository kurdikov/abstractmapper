﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Строит выражения для преобразования примитивных свойств
    /// </summary>
    public class PrimitiveConvertExpressionCreator : IPrimitiveConvertExpressionCreator
    {
        [NotNull]private readonly IMemberInfoLibrary _memberInfoLibrary;
        [NotNull]private readonly IConversionChecker _conversionChecker;

        [NotNull]private readonly MethodInfo _enumParse;
        [NotNull]private readonly MethodInfo _enumTryParse;

        /// <summary>
        /// Строит выражения для преобразования примитивных свойств
        /// </summary>
        public PrimitiveConvertExpressionCreator(
            [NotNull]IMemberInfoHelper memberInfoHelper,
            [NotNull]IMemberInfoLibrary memberInfoLibrary,
            [NotNull]IConversionChecker conversionChecker)
        {
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (memberInfoLibrary == null) throw new ArgumentNullException("memberInfoLibrary");
            if (conversionChecker == null) throw new ArgumentNullException("conversionChecker");

            _memberInfoLibrary = memberInfoLibrary;
            _conversionChecker = conversionChecker;

            int e;
            // ReSharper disable ReturnValueOfPureMethodIsNotUsed
            _enumParse = memberInfoHelper.GetMethodInfo(() => Enum.Parse(typeof (Enum), "", true));
            _enumTryParse = memberInfoHelper.GetGenericDefinitionMethodInfo(() => Enum.TryParse("", true, out e));
            // ReSharper restore ReturnValueOfPureMethodIsNotUsed
        }

        [NotNull]
        private MethodInfo ToStringMethod([NotNull]Type type)
        {
            return type.GetTypeInfo().GetMethod("ToString", new Type[0])
                ?? _memberInfoLibrary.ObjectToString();
        }

        /// <summary>
        /// Строит выражение для преобразования примитивного типа в примитивный тип
        /// </summary>
        /// <param name="value">Преобразуемое значение</param>
        /// <param name="targetType">Тип, в который нужно преобразовать</param>
        /// <param name="convertExpression">Построенное выражение</param>
        /// <returns>Успешность построения</returns>
        public bool TryGetExpression(Expression value, Type targetType, out Expression convertExpression)
        {
            convertExpression = null;
            if (value.Type == targetType)
            {
                convertExpression = value;
            }
            else if (_conversionChecker.IsImplicitlyConvertible(value.Type, targetType))
            {
                convertExpression = Expression.Convert(value, targetType);
            }
            else if (targetType == typeof(string))
            {
                convertExpression = Expression.Call(value, ToStringMethod(value.Type));
            }
            else
            {
                var valueIsNullable = value.Type.IsNullableStruct();
                var underlyingValueType = valueIsNullable ? value.Type.GetTypeInfo().GetGenericArguments()[0] : value.Type;
                var targetIsNullable = targetType.IsNullableStruct();
                var underlyingTargetType = targetIsNullable ? targetType.GetTypeInfo().GetGenericArguments()[0] : targetType;

                if (underlyingValueType != null && underlyingValueType.GetTypeInfo().IsEnum)
                {
                    var enumUnderlyingType = underlyingValueType.GetTypeInfo().GetEnumUnderlyingType();
                    if ((!valueIsNullable || targetIsNullable)
                        && underlyingTargetType != null && _conversionChecker.IsPrimitiveImplicitlyConvertible(enumUnderlyingType, underlyingTargetType))
                    {
                        convertExpression = Expression.Convert(value, targetType);
                    }
                }
                else if (underlyingTargetType != null && underlyingTargetType.GetTypeInfo().IsEnum)
                {
                    var enumUnderlyingType = underlyingTargetType.GetTypeInfo().GetEnumUnderlyingType();
                    if ((!valueIsNullable || targetIsNullable)
                        && underlyingValueType != null && _conversionChecker.IsPrimitiveImplicitlyConvertible(underlyingValueType, enumUnderlyingType))
                    {
                        convertExpression = Expression.Convert(value, targetType);
                    }
                    else if (value.Type == typeof(string))
                    {
                        if (targetIsNullable)
                        {
                            var variable = Expression.Variable(underlyingTargetType);
                            convertExpression = Expression.Block(
                                variable,
                                Expression.IfThenElse(
                                    Expression.Call(
                                        _enumTryParse.MakeGenericMethod(underlyingTargetType),
                                        value,
                                        Expression.Constant(true, typeof(bool)),
                                        variable), 
                                    Expression.Convert(variable, targetType),
                                    Expression.Constant(null, targetType)));
                        }
                        else
                        {
                            convertExpression =
                                Expression.Convert(
                                    Expression.Call(
                                        _enumParse,
                                        Expression.Constant(targetType, typeof(Type)),
                                        value,
                                        Expression.Constant(true, typeof(bool))),
                                    targetType);
                        }
                    }
                }
            }
            return convertExpression != null;
        }
    }
}
