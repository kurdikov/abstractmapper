namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// ������ ��������� ��� �������������� ���������
    /// </summary>
    public class NewCollectionConvertExpressionCreator : ICollectionConvertExpressionCreator
    {
        [NotNull] private readonly IMemberInfoHelper _memberInfoHelper;
        [NotNull] private readonly IMemberInfoLibrary _library;
        [NotNull] private readonly IExpressionBuilder _expressionBuilder;

        [NotNull] private readonly PropertyInfo _nullableIntValue;
        [NotNull] private readonly PropertyInfo _nullableIntHasValue;

        [NotNull] private readonly MethodInfo _convert;

        /// <summary>
        /// ������ ��������� ��� �������������� ���������
        /// </summary>
        public NewCollectionConvertExpressionCreator([NotNull] IMemberInfoHelper memberInfoHelper, [NotNull] IMemberInfoLibrary library, [NotNull] IExpressionBuilder expressionBuilder)
        {
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (library == null) throw new ArgumentNullException("library");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            _memberInfoHelper = memberInfoHelper;
            _library = library;
            _expressionBuilder = expressionBuilder;

            _nullableIntValue = _memberInfoHelper.GetPropertyInfo((int? i) => i.Value);
            _nullableIntHasValue = _memberInfoHelper.GetPropertyInfo((int? i) => i.HasValue);
            _convert = _memberInfoHelper.GetGenericDefinitionMethodInfo(
                    () => Convert<object, object, object, object>(null, null, null));
        }

        /// <summary>
        /// ������� ��������� ��� �������������� IEnumerable � collectionType, ������� �������� ���������� elementType
        /// </summary>
        public Expression Convert(Expression enumerableExpression, Type collectionType, Type elementType)
        {
            var param = Expression.Parameter(elementType);
            var convertMethod = _convert.MakeGenericMethod(collectionType, elementType, collectionType, elementType);
            var convertDelegate = DelegateCreator.Create<Func<Expression, Expression, ParameterExpression, Expression>>(
                this, convertMethod);
            return convertDelegate(enumerableExpression, param, param);
        }

        /// <summary>
        /// ������� ��������� ��� �������������� ��������� � ���������
        /// </summary>
        /// <param name="source">�������� ���������</param>
        /// <param name="elementConvert">���������, ������������� ������� ���������</param>
        /// <param name="elementParameter">��������-������� � ��������� �������������� ��������</param>
        /// <param name="sourceType">��� �������� ���������</param>
        /// <param name="sourceElementType">��� �������� �������� ���������</param>
        /// <param name="targetType">��� ���������-����������</param>
        /// <param name="targetElementType">��� �������� ���������-����������</param>
        /// <returns>��������� ��� �������������� ���������</returns>
        public Expression Convert(
            Expression source,
            Expression elementConvert,
            ParameterExpression elementParameter,
            Type sourceType,
            Type sourceElementType,
            Type targetType,
            Type targetElementType)
        {
            var convertMethod = _convert.MakeGenericMethod(sourceType, sourceElementType, targetType, targetElementType);
            var convertDelegate = DelegateCreator.Create<Func<Expression, Expression, ParameterExpression, Expression>>(
                this, convertMethod);
            return convertDelegate(source, elementConvert, elementParameter);
        }

        private Expression GetSourceCount<TSource, TSourceElement>([NotNull]Expression sourceCollection)
        {
            Expression sourceCount = null;
            if (typeof(IReadOnlyCollection<TSourceElement>).GetTypeInfo().IsAssignableFrom(typeof(TSource)))
            {
                var property = _memberInfoHelper.GetPropertyInfo((IReadOnlyCollection<TSourceElement> c) => c.Count);
                sourceCount = Expression.Property(sourceCollection, property);
            }
            else if (typeof(ICollection<TSourceElement>).GetTypeInfo().IsAssignableFrom(typeof(TSource)))
            {
                var property = _memberInfoHelper.GetPropertyInfo((ICollection<TSourceElement> c) => c.Count);
                sourceCount = Expression.Property(sourceCollection, property);
            }
            return sourceCount;
        }

        private Expression Convert<TSource, TSourceElement, TTarget, TTargetElement>(
            [NotNull]Expression sourceCollection,
            [NotNull]Expression elementConvertExpression,
            [NotNull]ParameterExpression elementParameter)
        {
            Expression result;
            if (typeof(TTarget) == typeof(IEnumerable<TTargetElement>))
            {
                result = CreateEnumerable(
                    sourceCollection, elementConvertExpression, elementParameter,
                    typeof(TSourceElement), typeof(TTargetElement));
            }
            else if (typeof(TTarget).GetTypeInfo().IsInterface && typeof(TTarget).GetTypeInfo().IsAssignableFrom(typeof(List<TTargetElement>))
                     || typeof(TTarget) == typeof(List<TTargetElement>))
            {
                result = CreateList<TSourceElement, TTargetElement>(
                    sourceCollection, elementConvertExpression, elementParameter,
                    GetSourceCount<TSource, TSourceElement>(sourceCollection));
            }
            else if (typeof(TTarget).GetTypeInfo().IsInterface)
            {
                throw new ArgumentException(string.Format(
                    "The interface type {0} may not be constructed from neither IEnumerable nor List", typeof(TTarget)));
            }
            else if (typeof(TTarget).IsArray)
            {
                result = CreateArray<TSourceElement, TTargetElement>(
                    sourceCollection, elementConvertExpression, elementParameter,
                    GetSourceCount<TSource, TSourceElement>(sourceCollection));
            }
            else
            {
                var constructor = typeof(TTarget).GetTypeInfo().GetConstructor(
                    new[] { typeof(IEnumerable<TTargetElement>) });
                if (constructor != null)
                {
                    result = Expression.New(
                        constructor,
                        CreateEnumerable(
                            sourceCollection, elementConvertExpression, elementParameter,
                            typeof(TSourceElement), typeof(TTargetElement)));
                }
                else
                {
                    var listConstructor = typeof(TTarget).GetTypeInfo().GetConstructor(
                        new[] { typeof(List<TTargetElement>) });
                    if (listConstructor == null)
                    {
                        throw new ArgumentException(string.Format(
                            "The collection type {0} may not be constructed neither from IEnumerable nor from List", typeof(TTarget)));
                    }
                    result = Expression.New(
                        listConstructor,
                        CreateList<TSourceElement, TTargetElement>(
                            sourceCollection, elementConvertExpression, elementParameter,
                            GetSourceCount<TSource, TSourceElement>(sourceCollection)));
                }
            }

            return result;
        }

        private Expression CreateList<TSourceElement, TTargetElement>(
            [NotNull]Expression sourceCollection,
            [NotNull]Expression elementConvertExpression,
            [NotNull]ParameterExpression elementParameter,
            Expression sourceCount)
        {
            var listConstructorWithCapacity = _memberInfoHelper.GetConstructorInfo(() => new List<TTargetElement>(0));
            var list = Expression.Variable(typeof(List<TTargetElement>));
            Expression listAssign;
            if (sourceCount != null)
            {
                listAssign = Expression.Assign(list, Expression.New(listConstructorWithCapacity, sourceCount));
            }
            else
            {
                ParameterExpression runtimeCount;
                var getCountExpr = TryGetCountInRuntime<TSourceElement>(sourceCollection, out runtimeCount);
                listAssign = Expression.Block(new[] {runtimeCount},
                    getCountExpr,
                    Expression.Assign(
                        list,
                        Expression.Condition(
                            Expression.Property(runtimeCount, _nullableIntHasValue),
                            Expression.New(listConstructorWithCapacity, Expression.Property(runtimeCount, _nullableIntValue)),
                            Expression.New(list.Type))));
            }
            return Expression.Block(new[] { list },
                listAssign,
                FillList<TSourceElement, TTargetElement>(sourceCollection, elementConvertExpression, elementParameter, list),
                list);
        }

        private Expression FillList<TSourceElement, TTargetElement>(
            [NotNull]Expression sourceCollection,
            [NotNull]Expression elementConvertExpression,
            [NotNull]ParameterExpression elementParameter,
            [NotNull]Expression targetList)
        {
            var listAdd = _memberInfoHelper.GetMethodInfo((List<TTargetElement> l) => l.Add(default(TTargetElement)));
            return _expressionBuilder.Foreach<TSourceElement>(
                sourceCollection,
                elementParameter,
                Expression.Call(targetList, listAdd, new[] {elementConvertExpression}));
        }

        [NotNull]
        private Expression TryGetCountInRuntime<TSourceElement>(
            [NotNull] Expression sourceCollection,
            out ParameterExpression resultVar)
        {
            resultVar = Expression.Variable(typeof(int?));
            var readonlyVar = Expression.Variable(typeof(IReadOnlyCollection<TSourceElement>));
            var collVar = Expression.Variable(typeof(ICollection<TSourceElement>));
            var readonlyCount = Expression.Property(readonlyVar, _memberInfoHelper.GetPropertyInfo((IReadOnlyCollection<TSourceElement> c) => c.Count));
            var collCount = Expression.Property(collVar, _memberInfoHelper.GetPropertyInfo((ICollection<TSourceElement> c) => c.Count));
            return Expression.Block(
                new[] {readonlyVar, collVar},
                Expression.Assign(readonlyVar, Expression.TypeAs(sourceCollection, readonlyVar.Type)),
                Expression.IfThenElse(
                    Expression.NotEqual(
                        readonlyVar,
                        Expression.Constant(null, readonlyVar.Type)),
                    Expression.Assign(resultVar, Expression.Convert(readonlyCount, typeof(int?))),
                    Expression.Block(
                        Expression.Assign(collVar, Expression.TypeAs(sourceCollection, collVar.Type)),
                        Expression.IfThen(
                            Expression.NotEqual(
                                collVar,
                                Expression.Constant(null, collVar.Type)),
                            Expression.Assign(resultVar, Expression.Convert(collCount, typeof(int?)))))));
        }

        [NotNull]
        private Expression CreateArray<TSourceElement, TTargetElement>(
            [NotNull]Expression sourceCollection,
            [NotNull]Expression elementConvertExpression,
            [NotNull]ParameterExpression elementParameter,
            Expression sourceCount)
        {
            if (sourceCount != null)
            {
                return CreateArrayWithKnownSize<TSourceElement, TTargetElement>(
                    sourceCollection, elementConvertExpression, elementParameter, sourceCount);
            }
            ParameterExpression runtimeCount;
            var getCountExpr = TryGetCountInRuntime<TSourceElement>(sourceCollection, out runtimeCount);
            var enumerableToArray = Expression.Call(
                _library.EnumerableToArray(typeof(TTargetElement)),
                Expression.Call(
                    _library.EnumerableSelect(typeof(TSourceElement), typeof(TTargetElement)),
                    sourceCollection,
                    Expression.Lambda(
                        typeof(Func<,>).MakeGenericType(typeof(TSourceElement), typeof(TTargetElement)),
                        elementConvertExpression,
                        elementParameter)));
            return Expression.Block(
                new[] {runtimeCount,},
                getCountExpr,
                Expression.Condition(
                    Expression.Property(runtimeCount, _nullableIntHasValue),
                    CreateArrayWithKnownSize<TSourceElement, TTargetElement>(
                        sourceCollection,
                        elementConvertExpression,
                        elementParameter,
                        Expression.Property(runtimeCount, _nullableIntValue)),
                    enumerableToArray));
        }

        [NotNull]
        private Expression CreateArrayWithKnownSize<TSourceElement, TTargetElement>(
            [NotNull]Expression sourceCollection,
            [NotNull]Expression elementConvertExpression,
            [NotNull]ParameterExpression elementParameter,
            [NotNull]Expression sourceCount)
        {
            var array = Expression.Variable(typeof(TTargetElement[]));
            var forVar = Expression.Variable(typeof(int));
            var enumerator = Expression.Variable(typeof(IEnumerator<TSourceElement>));
            var getEnumerator = _memberInfoHelper.GetMethodInfo((IEnumerable<TSourceElement> e) => e.GetEnumerator());
            var enumeratorCurrent = _memberInfoHelper.GetPropertyInfo((IEnumerator<TSourceElement> e) => e.Current);

            return Expression.Block(
                new[] { array, enumerator, elementParameter },
                Expression.Assign(array, Expression.NewArrayBounds(typeof(TTargetElement), sourceCount)),
                Expression.TryFinally(
                    Expression.Block(
                        Expression.Assign(enumerator, Expression.Call(sourceCollection, getEnumerator)),
                        _expressionBuilder.ForRange(
                            forVar, 
                            Expression.Constant(0),
                            sourceCount,
                            Expression.Block(
                                Expression.Call(enumerator, _library.IenumeratorMoveNext()),
                                Expression.Assign(
                                    elementParameter,
                                    Expression.Property(enumerator, enumeratorCurrent)),
                                Expression.Assign(
                                    Expression.ArrayAccess(array, forVar),
                                    elementConvertExpression)))),
                    Expression.IfThen(
                        Expression.NotEqual(
                            enumerator,
                            Expression.Constant(null, typeof(object))),
                        Expression.Call(enumerator, _library.IdisposableDispose()))),
                array);
        }

        private Expression CreateEnumerable(
            [NotNull]Expression sourceCollection,
            [NotNull]Expression elementConvertExpression,
            [NotNull]ParameterExpression elementParameter,
            [NotNull]Type sourceElement,
            [NotNull]Type targetElement)
        {
            return Expression.Call(
                _library.EnumerableSelect(sourceElement, targetElement),
                sourceCollection,
                Expression.Lambda(
                    typeof(Func<,>).MakeGenericType(sourceElement, targetElement),
                    elementConvertExpression,
                    elementParameter));
        }
    }
}
