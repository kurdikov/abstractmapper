﻿namespace TsSoft.AbstractMapper.Engine
{
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Строит выражение для преобразования TFrom в TTo вместе с описаниями использованных путей
    /// </summary>
    public interface IMapExpressionCreator
    {
        /// <summary>
        /// Строит выражение для преобразования TFrom в TTo вместе с описаниями использованных путей
        /// </summary>
        /// <typeparam name="TFrom">Тип, из которого идёт преобразование</typeparam>
        /// <typeparam name="TTo">Тип, в который идёт преобразование</typeparam>
        /// <param name="mapRules">Правила преобразования</param>
        /// <param name="addCatchBlocks">Добавлять try-catch блоки, оборачивающие исключения, для каждого сгенерированного выражения преобразования</param>
        /// <param name="exceptionMessage">Сообщения исключений-обёрток</param>
        [NotNull]
        MapperExpression<TFrom, TTo> GetExpression<TFrom, TTo>(
            [NotNull]IFullMapRules mapRules,
            bool addCatchBlocks,
            string exceptionMessage);
    }
}
