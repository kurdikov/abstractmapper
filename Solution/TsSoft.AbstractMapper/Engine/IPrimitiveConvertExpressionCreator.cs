﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Строит выражения для преобразования примитивных свойств
    /// </summary>
    public interface IPrimitiveConvertExpressionCreator
    {
        /// <summary>
        /// Строит выражение для преобразования примитивного типа в примитивный тип
        /// </summary>
        /// <param name="value">Преобразуемое значение</param>
        /// <param name="targetType">Тип, в который нужно преобразовать</param>
        /// <param name="convertExpression">Построенное выражение</param>
        /// <returns>Успешность построения</returns>
        bool TryGetExpression([NotNull]Expression value, [NotNull]Type targetType, out Expression convertExpression);
    }
}
