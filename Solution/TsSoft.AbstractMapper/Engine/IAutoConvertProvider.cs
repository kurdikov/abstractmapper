﻿namespace TsSoft.AbstractMapper.Engine
{
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Rules;

    /// <summary>
    /// Дополняет правила выражениями преобразования
    /// </summary>
    public interface IAutoConvertProvider
    {
        /// <summary>
        /// Дополнить правила выражениями преобразования
        /// </summary>
        /// <param name="rules">Правила</param>
        /// <param name="innerMapperStrategy">Как получать внутренние мапперы</param>
        void SetAutoConvert([NotNull]IFullMapRules rules, InnerMapperStrategy innerMapperStrategy);
    }
}
