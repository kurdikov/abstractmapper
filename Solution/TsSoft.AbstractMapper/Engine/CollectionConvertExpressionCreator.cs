﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;

    /// <summary>
    /// Строит выражение для преобразования коллекций
    /// </summary>
    public class CollectionConvertExpressionCreator : ICollectionConvertExpressionCreator
    {
        [NotNull]private readonly IMemberInfoLibrary _library;

        /// <summary>
        /// Строит выражение для преобразования коллекций
        /// </summary>
        public CollectionConvertExpressionCreator([NotNull]IMemberInfoLibrary library)
        {
            if (library == null) throw new ArgumentNullException("library");
            _library = library;
        }

        /// <summary>
        /// Создать выражение для преобразования IEnumerable к collectionType, который является коллекцией elementType
        /// </summary>
        public Expression Convert(Expression ienumerableExpression, Type collectionType, Type elementType)
        {
            Expression result;
            var collectionTypeInfo = collectionType.GetTypeInfo();
            if (collectionType == typeof(IEnumerable<>).MakeGenericType(elementType))
            {
                result = ienumerableExpression;
            }
            else if (collectionTypeInfo.IsInterface && collectionTypeInfo.IsAssignableFrom(typeof(List<>).MakeGenericType(elementType))
                || collectionType == typeof(List<>).MakeGenericType(elementType))
            {
                var toListMethod = _library.EnumerableToList(elementType);
                result = Expression.Call(toListMethod, ienumerableExpression);
            }
            else if (collectionTypeInfo.IsInterface)
            {
                throw new ArgumentException(string.Format(
                    "The interface type {0} may not be constructed from neither IEnumerable nor List", collectionType));
            }
            else if (collectionType.IsArray)
            {
                var toArrayMethod = _library.EnumerableToArray(elementType);
                result = Expression.Call(toArrayMethod, ienumerableExpression);
            }
            else
            {
                var constructor = collectionTypeInfo.GetConstructor(
                    new[] { typeof(IEnumerable<>).MakeGenericType(elementType) });
                if (constructor != null)
                {
                    result = Expression.New(
                        constructor,
                        new[] { ienumerableExpression });
                }
                else
                {
                    var listConstructor = collectionTypeInfo.GetConstructor(
                        new[] { typeof(List<>).MakeGenericType(elementType) });
                    if (listConstructor == null)
                    {
                        throw new ArgumentException(string.Format(
                            "The collection type {0} may not be constructed neither from IEnumerable nor from List", collectionType));
                    }
                    var toListMethod = _library.EnumerableToList(elementType);
                    result = Expression.New(
                        listConstructor,
                        new Expression[]
                        {
                            Expression.Call(toListMethod, ienumerableExpression)
                        });
                }
            }
            return result;
        }

        /// <summary>
        /// Создать выражение для преобразования коллекции в коллекцию
        /// </summary>
        /// <param name="source">Исходная коллекция</param>
        /// <param name="elementConvert">Выражение, преобразующее элемент коллекции</param>
        /// <param name="elementParameter">Параметр-элемент в выражении преобразования элемента</param>
        /// <param name="sourceType">Тип исходной коллекции</param>
        /// <param name="sourceElementType">Тип элемента исходной коллекции</param>
        /// <param name="targetType">Тип коллекции-результата</param>
        /// <param name="targetElementType">Тип элемента коллекции-результата</param>
        /// <returns>Выражение для преобразования коллекции</returns>
        public Expression Convert(
            Expression source,
            Expression elementConvert,
            ParameterExpression elementParameter,
            Type sourceType,
            Type sourceElementType,
            Type targetType,
            Type targetElementType)
        {
            var enumerableSelect = _library.EnumerableSelect(sourceElementType, targetElementType);
            var enumerableExpression = Expression.Call(
                enumerableSelect,
                source,
                Expression.Lambda(
                    typeof(Func<,>).MakeGenericType(sourceElementType, targetElementType),
                    elementConvert,
                    elementParameter));
            return Convert(enumerableExpression, targetType, targetElementType);
        }
    }
}
