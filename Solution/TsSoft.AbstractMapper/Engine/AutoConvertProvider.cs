﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Дополняет правила выражениями преобразования
    /// </summary>
    public class AutoConvertProvider : IAutoConvertProvider
    {
        [NotNull] private readonly IConvertExpressionCreator _convertExpressionCreator;

        /// <summary>
        /// Дополняет правила выражениями преобразования
        /// </summary>
        public AutoConvertProvider([NotNull] IConvertExpressionCreator convertExpressionCreator)
        {
            if (convertExpressionCreator == null) throw new ArgumentNullException("convertExpressionCreator");
            _convertExpressionCreator = convertExpressionCreator;
        }

        /// <summary>
        /// Дополнить правила выражениями преобразования
        /// </summary>
        /// <param name="rules">Правила</param>
        /// <param name="innerMapperStrategy">Как получать внутренние мапперы</param>
        public void SetAutoConvert(IFullMapRules rules, InnerMapperStrategy innerMapperStrategy)
        {
            foreach (var rule in rules)
            {
                if (rule == null)
                {
                    throw new InvalidOperationException("rule is null");
                }
                if (rule.FromAccess == null)
                {
                    throw new InvalidOperationException("rule.FromAccess is null");
                }
                var member = rule.ToMember;
                MapperDescription mapperDescription;
                if (rule.Convert == null)
                {
                    if (rule.FromAccess.Count > 1)
                    {
                        throw new InvalidRulesException(string.Format("Unable to map multiple properties into {0} automatically; explicit map rule needed", rule.ToMember));
                    }
                    if (rule.FromAccess.Count == 0)
                    {
                        throw new InvalidRulesException(string.Format("Unable to map constant into {0} automatically; explicit map rule needed", rule.ToMember));
                    }
                    var fromProp = rule.FromAccess.First().ThrowIfNull("rule.FromAccess[0]");
                    var param = Expression.Parameter(fromProp.FromAccess.Body.Type);
                    var convert = _convertExpressionCreator.Convert(
                        param, member.ValueType, false, innerMapperStrategy, out mapperDescription);
                    if (convert == null)
                    {
                        throw new InvalidRulesException(string.Format("Cannot map from expression {0} with type {1} to {2}",
                            fromProp.FromAccess.Body,
                            fromProp.FromAccess.Body.Type,
                            rule.ToMember));
                    }
                    if (fromProp.FromAccess.Body.Type.IsNullableType())
                    {
                        convert = Expression.Condition(
                            Expression.NotEqual(param, Expression.Constant(null, param.Type)),
                            convert,
                            Expression.Default(convert.Type));
                    }
                    rule.Convert = Expression.Lambda(typeof(Func<,>).MakeGenericType(param.Type, member.ValueType), convert, param);
                    rule.MapperDescription = mapperDescription;
                }
                else if (!(member.ValueType.GetTypeInfo().IsAssignableFrom(rule.Convert.ReturnType)))
                {
                    var convertVar = Expression.Variable(rule.Convert.Body.Type);
                    var convert = _convertExpressionCreator.Convert(
                        convertVar, member.ValueType, false, InnerMapperStrategy.Default, out mapperDescription);
                    if (convert == null || mapperDescription != null)
                    {
                        throw new InvalidRulesException(string.Format("Cannot map from expression {0} with type {1} to {2} (note that there is a convert in the path rule)",
                            rule.Convert.Body,
                            rule.Convert.Body.Type,
                            member.ValueType));
                    }
                    var convertBlock = Expression.Block(
                        new[] { convertVar },
                        Expression.Assign(convertVar, rule.Convert.Body),
                        Expression.Condition(
                            Expression.NotEqual(convertVar, Expression.Constant(null, convertVar.Type)),
                            convert,
                            Expression.Default(convert.Type)));
                    rule.Convert = Expression.Lambda(convertBlock, rule.Convert.Parameters);
                }
            }
        }
    }
}
