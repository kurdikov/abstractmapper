﻿namespace TsSoft.AbstractMapper.Engine
{
    using System.Collections.Generic;

    /// <summary>
    /// Преобразует коллекции в коллекции с помощью маппера
    /// </summary>
    public interface ICollectionConverter
    {
        /// <summary>
        /// Превратить перечисление в перечисление
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <returns>Перечисление-результат</returns>
        IEnumerable<T> MakeEnumerable<T>(IEnumerable<T> source);
        /// <summary>
        /// Превратить перечисление в коллекцию
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <returns>Перечисление-результат</returns>
        List<T> MakeList<T>(IEnumerable<T> source);
        /// <summary>
        /// Превратить перечисление в коллекцию
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="count">Количество элементов в исходном перечислении</param>
        /// <returns>Перечисление-результат</returns>
        List<T> MakeList<T>(IEnumerable<T> source, int count);
        /// <summary>
        /// Превратить перечисление в массив
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <returns>Перечисление-результат</returns>
        T[] MakeArray<T>(IEnumerable<T> source);
        /// <summary>
        /// Превратить перечисление в массив
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="count">Количество элементов в исходном перечислении</param>
        /// <returns>Перечисление-результат</returns>
        T[] MakeArray<T>(IEnumerable<T> source, int count);
        /// <summary>
        /// Превратить перечисление в множество
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <returns>Перечисление-результат</returns>
        HashSet<T> MakeHashSet<T>(IEnumerable<T> source);
        /// <summary>
        /// Превратить перечисление в множество
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="count">Количество элементов в исходном перечислении</param>
        /// <returns>Перечисление-результат</returns>
        HashSet<T> MakeHashSet<T>(IEnumerable<T> source, int count);

        /// <summary>
        /// Превратить перечисление в перечисление
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента перечисления-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <returns>Перечисление-результат</returns>
        IEnumerable<TTarget> MakeEnumerable<TSource, TTarget>(
            IEnumerable<TSource> source,
            IMapper<TSource, TTarget> mapper);

        /// <summary>
        /// Превратить перечисление в коллекцию
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента коллекции-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <returns>Коллекция-результат</returns>
        List<TTarget> MakeList<TSource, TTarget>(IEnumerable<TSource> source, IMapper<TSource, TTarget> mapper);

        /// <summary>
        /// Превратить перечисление в коллекцию
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента коллекции-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <param name="count">Количество элементов в исходном перечислении</param>
        /// <returns>Коллекция-результат</returns>
        List<TTarget> MakeList<TSource, TTarget>(IEnumerable<TSource> source, IMapper<TSource, TTarget> mapper, int count);

        /// <summary>
        /// Превратить перечисление в массив
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента массива-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <returns>Массив-результат</returns>
        TTarget[] MakeArray<TSource, TTarget>(
            IEnumerable<TSource> source,
            IMapper<TSource, TTarget> mapper);

        /// <summary>
        /// Превратить перечисление в массив
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента массива-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <param name="count">Количество элементов в исходном перечислении</param>
        /// <returns>Массив-результат</returns>
        TTarget[] MakeArray<TSource, TTarget>(
            IEnumerable<TSource> source,
            IMapper<TSource, TTarget> mapper,
            int count);

        /// <summary>
        /// Превратить перечисление в множество
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента множества-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <returns>Множество-результат</returns>
        HashSet<TTarget> MakeHashSet<TSource, TTarget>(
            IEnumerable<TSource> source,
            IMapper<TSource, TTarget> mapper);

        /// <summary>
        /// Превратить перечисление в множество
        /// </summary>
        /// <typeparam name="TSource">Тип элемента исходного перечисления</typeparam>
        /// <typeparam name="TTarget">Тип элемента множества-результата</typeparam>
        /// <param name="source">Исходное перечисление</param>
        /// <param name="mapper">Маппер</param>
        /// <param name="count">Количество элементов в исходном перечислении</param>
        /// <returns>Множество-результат</returns>
        HashSet<TTarget> MakeHashSet<TSource, TTarget>(
            IEnumerable<TSource> source,
            IMapper<TSource, TTarget> mapper,
            int count);
    }
}
