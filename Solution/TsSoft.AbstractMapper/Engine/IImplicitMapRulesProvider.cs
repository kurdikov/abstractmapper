namespace TsSoft.AbstractMapper.Engine
{
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.Rules;

    /// <summary>
    /// ��������� ��������� ������ �������������
    /// </summary>
    public interface IImplicitMapRulesProvider
    {
        /// <summary>
        /// �������� ������������� ��������� ������, ��������� �� ����� ������ � ����������
        /// </summary>
        /// <typeparam name="TTo">���, � ������� ��� ��������������</typeparam>
        /// <typeparam name="TFrom">���, �� �������� ��� ��������������</typeparam>
        /// <param name="explicitRules">��������� � ������� �������</param>
        /// <param name="ignoreRules">������� �������������</param>
        /// <param name="throwOnError">����������� �� ���������� ��� ������</param>
        /// <returns>������������� ������ ������ ��������������</returns>
        /// <exception cref="InvalidRulesException">�� ������� ��������� ����������� ��� ��������� �������</exception>
        [NotNull]
        FullMapRules AddImplicitRules<TFrom, TTo>(
            IMapRules<TTo, TFrom> explicitRules,
            IIgnoreRules<TTo> ignoreRules,
            bool throwOnError);
    }
}
