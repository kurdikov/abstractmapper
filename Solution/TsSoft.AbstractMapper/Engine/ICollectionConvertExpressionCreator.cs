﻿namespace TsSoft.AbstractMapper.Engine
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Строит выражение для преобразования коллекций
    /// </summary>
    public interface ICollectionConvertExpressionCreator
    {
        /// <summary>
        /// Создать выражение для преобразования IEnumerable к collectionType, который является коллекцией elementType
        /// </summary>
        Expression Convert([NotNull]Expression enumerableExpression, [NotNull]Type collectionType, [NotNull]Type elementType);

        /// <summary>
        /// Создать выражение для преобразования коллекции в коллекцию
        /// </summary>
        /// <param name="source">Исходная коллекция</param>
        /// <param name="elementConvert">Выражение, преобразующее элемент коллекции</param>
        /// <param name="elementParameter">Параметр-элемент в выражении преобразования элемента</param>
        /// <param name="sourceType">Тип исходной коллекции</param>
        /// <param name="sourceElementType">Тип элемента исходной коллекции</param>
        /// <param name="targetType">Тип коллекции-результата</param>
        /// <param name="targetElementType">Тип элемента коллекции-результата</param>
        /// <returns>Выражение для преобразования коллекции</returns>
        Expression Convert(
            [NotNull] Expression source,
            [NotNull] Expression elementConvert,
            [NotNull] ParameterExpression elementParameter,
            [NotNull] Type sourceType,
            [NotNull] Type sourceElementType,
            [NotNull] Type targetType,
            [NotNull] Type targetElementType);
    }
}
