﻿namespace TsSoft.EntityRepository.Interfaces
{
    /// <summary>
    /// Интерфейс сложной сущности, у которой могут появляться зависимости до её сохранения
    /// </summary>
    public interface IComplexEntity
    {
        /// <summary>
        /// Сохранена ли сущность
        /// </summary>
        bool IsSaved { get; set; }
    }
}
