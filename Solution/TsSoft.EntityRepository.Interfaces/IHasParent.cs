﻿namespace TsSoft.EntityRepository.Interfaces
{
    /// <summary>
    /// Интерфейс сущности, имеющей родителя
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IHasParent<TEntity>
    {
        /// <summary>
        /// Родительская сущность
        /// </summary>
        TEntity Parent { get; set; }
    }
}