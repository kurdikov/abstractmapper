﻿// ReSharper disable CheckNamespace
namespace TsSoft.EntityRepository.Helpers
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    /// Проверяет на равенство сущности по ключу
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    /// <typeparam name="TKey">Тип ключа</typeparam>
    public class EntityComparer<T, TKey> : IEqualityComparer<T>
        where T: class, IEntityWithId<TKey>
        where TKey: IEquatable<TKey>
    {
        /// <summary>
        /// Проверяет на равенство сущности по ключу
        /// </summary>
        public static EntityComparer<T, TKey> Instance = new EntityComparer<T, TKey>();

        /// <summary>
        /// Равны ли сущности
        /// </summary>
        public bool Equals(T x, T y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            return x != null && y != null && EqualityComparer<TKey>.Default.Equals(x.Id, y.Id);
        }

        /// <summary>
        /// Получить хэш ключа
        /// </summary>
        public int GetHashCode(T obj)
        {
            return obj != null ? EqualityComparer<TKey>.Default.GetHashCode(obj.Id) : 0;
        }
    }
}
