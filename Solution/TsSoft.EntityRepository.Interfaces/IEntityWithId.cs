﻿namespace TsSoft.EntityRepository.Interfaces
{
    /// <summary>
    /// Интерфейс сущности с ключом
    /// </summary>
    /// <typeparam name="TId">Тип ключа</typeparam>
    public interface IEntityWithId<TId>
    {
        /// <summary>
        /// Ключ экземпляра
        /// </summary>
        TId Id { get; set; }
    }
}