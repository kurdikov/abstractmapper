﻿// ReSharper disable TypeParameterCanBeVariant
// R# wrongly thinks interfaces covariant on T can return Nullable<T>
namespace TsSoft.EntityRepository.Interfaces
{
    /// <summary>
    /// Интерфейс обладающий идентификатором родителя
    /// </summary>
    /// <typeparam name="TId">Тип идентификатора</typeparam>
    public interface IHasParentId<TId>
        where TId : struct
    {
        /// <summary>
        /// Ключ родительского элемента
        /// </summary>
        TId? ParentId { get; }
    }
}
// ReSharper restore TypeParameterCanBeVariant
