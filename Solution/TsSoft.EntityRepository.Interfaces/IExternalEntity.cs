﻿namespace TsSoft.EntityRepository.Interfaces
{
    using System;

    /// <summary>
    /// Интерфейс внешней сущности
    /// </summary>
    public interface IExternalEntity<out TId>
    {
        /// <summary>
        /// Идентификатор внешней сущности
        /// </summary>
        TId Id { get; }
    }

    /// <summary>
    /// Интерфейс внешней сущности
    /// </summary>
    public interface IExternalEntity : IExternalEntity<Guid>
    {
    }
}
