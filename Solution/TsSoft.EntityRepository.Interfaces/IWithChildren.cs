﻿namespace TsSoft.EntityRepository.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Интерфейс сущности, имеющей дочерние сущности
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IWithChildren<TEntity>
    {
        /// <summary>
        /// Дочерние сущности
        /// </summary>
        ICollection<TEntity> Children { get; }
    }
}