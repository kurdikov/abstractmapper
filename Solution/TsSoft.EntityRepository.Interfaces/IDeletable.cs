﻿namespace TsSoft.EntityRepository.Interfaces
{
    /// <summary>
    /// Интерфейс удаляемой сущности
    /// </summary>
    public interface IDeletable
    {
        /// <summary>
        /// Является ли сущность удалённой
        /// </summary>
        bool IsDeleted { get; set; }
    }
}
