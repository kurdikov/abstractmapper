﻿namespace TsSoft.EntityRepository.Interfaces
{
    /// <summary>
    /// Интерфейс элемента дерева
    /// </summary>
    /// <typeparam name="TEntity">Тип элемента дерева</typeparam>
    /// <typeparam name="TId">Тип ключа элемента дерева</typeparam>
    public interface ITreeElement<TEntity, TId> : IEntityWithId<TId>, IWithChildren<TEntity>, IHasParent<TEntity>, IHasParentId<TId> where TId : struct
    {

    }
}
