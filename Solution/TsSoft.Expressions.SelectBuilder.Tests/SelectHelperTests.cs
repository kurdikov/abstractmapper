﻿namespace TsSoft.Expressions.SelectBuilder.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Models;

    [TestClass]
    public class SelectHelperTests
    {
        [NotNull] private SelectHelper _helper;

        private ValueHoldingMember TestEntityProp(string name)
        {
            return new ValueHoldingMember(typeof(TestEntity).GetProperty(name));
        }

        private IReadOnlyList<ValueHoldingMember> TestEntityProps(params string[] names)
        {
            return names.Select(TestEntityProp).ToList();
        }

        [NotNull]
        private SelectHelper CreateCreator(Type[] entityTypes, Type[] externalTypes)
        {
            return AbstractMapperDependenciesFactory.GetDependency<SelectHelper>(
                MapperTypes.None, Entities.Are(entityTypes),
                External.Are(externalTypes));
        }

        private ParsedPath Path(params string[] props)
        {
            return new ParsedPath(elements: props.Select(p => new PathElement(TestEntityProp(p))).ToList(), start: null);
        }

        [TestInitialize]
        public void TestInit()
        {
            _helper = CreateCreator(new[] {typeof(TestEntity), typeof(From)}, new[] {typeof(ExternalEntity)});
        }

        [TestMethod]
        public void TestCreateFromIncludes()
        {
            var helper = CreateCreator(new[] {typeof(TestEntity)}, new[] {typeof(ExternalEntity)});
            var includes = new Includes<TestEntity>
            {
                e => e.External,
                e => e.Parent.External,
                e => e.Parent.Children.Select(c => c.External),
                e => e.Children.Select(c => c.Parent.External),
                e => e.Children.Select(c => c.Children),
            };
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Prop1 = e.Prop1,
                Prop2 = e.Prop2,
                Prop3 = e.Prop3,
                ExternalId = e.ExternalId,
                Parent = e.Parent != null
                    ? new DynamicTestEntity
                    {
                        Prop1 = e.Parent.Prop1,
                        Prop2 = e.Parent.Prop2,
                        Prop3 = e.Parent.Prop3,
                        ExternalId = e.Parent.ExternalId,
                        Children = e.Parent.Children.Select(
                            c => new DynamicTestEntity
                            {
                                Prop1 = c.Prop1,
                                Prop2 = c.Prop2,
                                Prop3 = c.Prop3,
                                ExternalId = c.ExternalId,
                            })
                    }
                    : null,
                Children = e.Children.Select(
                    c => new DynamicTestEntity
                    {
                        Prop1 = c.Prop1,
                        Prop2 = c.Prop2,
                        Prop3 = c.Prop3,
                        ExternalId = c.ExternalId,
                        Parent = c.Parent != null
                            ? new DynamicTestEntity
                            {
                                Prop1 = c.Parent.Prop1,
                                Prop2 = c.Parent.Prop2,
                                Prop3 = c.Parent.Prop3,
                                ExternalId = c.Parent.ExternalId
                            }
                            : null,
                        Children = c.Children.Select(
                            cc => new DynamicTestEntity
                            {
                                Prop1 = cc.Prop1,
                                Prop2 = cc.Prop2,
                                Prop3 = cc.Prop3,
                                ExternalId = cc.ExternalId,
                            })
                    })
            };
            var actual = helper.CreateFromIncludes(includes);
            SelectAssert.AreEqual(expectedSelect, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(4, actual.ExternalIncludes.Count);
            IncludeAssert.Contains(e => e.External, actual.ExternalIncludes);
            IncludeAssert.Contains(e => e.Parent.External, actual.ExternalIncludes);
            IncludeAssert.Contains(e => e.Children.Select(c => c.Parent.External), actual.ExternalIncludes);
            IncludeAssert.Contains(e => e.Parent.Children.Select(c => c.External), actual.ExternalIncludes);
        }

        [TestMethod]
        public void TestMergeSelectIntersectingByOneProperty()
        {
            var expr1 = new SelectExpression<TestEntity>
            {
                Select = e => new DynamicTestEntity {Prop1 = e.Prop1, Prop2 = e.Prop2},
                ExternalIncludes = new Includes<TestEntity>
                {
                    e => e.External,
                },
            };
            var expr2 = new SelectExpression<TestEntity>
            {
                Select = e => new DynamicTestEntity {Prop1 = e.Prop1, ExternalId = e.ExternalId},
            };
            Expression<Func<TestEntity, object>> expectedSelect =
                e => new DynamicTestEntity {Prop1 = e.Prop1, Prop2 = e.Prop2, ExternalId = e.ExternalId};
            var actual = _helper.Merge(expr1, expr2);
            SelectAssert.AreEqual(expectedSelect, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(1, actual.ExternalIncludes.Count);
            IncludeAssert.Contains(e => e.External, actual.ExternalIncludes);
        }

        [TestMethod]
        public void TestMergeSelectsWithDifferentPropertiesFromSameSubentityAndOnePathEndingOnSubentity()
        {
            var expr1 = new SelectExpression<TestEntity>
            {
                Select =
                    e =>
                        new TestEntity
                        {
                            Prop1 = e.Prop1,
                            Parent = new TestEntity {Prop1 = e.Parent.Prop1},
                            Children = e.Children
                        },
                ExternalIncludes = new Includes<TestEntity>
                {
                    e => e.External,
                },
            };
            var expr2 = new SelectExpression<TestEntity>
            {
                Select =
                    e =>
                        new DynamicTestEntity {Prop2 = e.Prop2, Parent = new DynamicTestEntity {Prop2 = e.Parent.Prop2}},
            };
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Prop1 = e.Prop1,
                Parent =
                    e.Parent != null ? new DynamicTestEntity {Prop1 = e.Parent.Prop1, Prop2 = e.Parent.Prop2} : null,
                Children =
                    e.Children.Select(
                        c =>
                            new DynamicTestEntity
                            {
                                Prop1 = c.Prop1,
                                Prop2 = c.Prop2,
                                Prop3 = c.Prop3,
                                ExternalId = c.ExternalId
                            }),
                Prop2 = e.Prop2,
            };
            var actual = _helper.Merge(expr1, expr2);
            SelectAssert.AreEqual(expectedSelect, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(1, actual.ExternalIncludes.Count);
            IncludeAssert.Contains(e => e.External, actual.ExternalIncludes);
        }

        [TestMethod]
        public void TestMergeSelectWithSubentityAndSelectWithoutIt()
        {
            var expr1 = new SelectExpression<TestEntity>
            {
                Select = e => new {e.Prop1, e.Prop2, Children = e.Children.Select(c => new {c.Prop1}).ToList()},
                ExternalIncludes = new Includes<TestEntity>
                {
                    e => e.External,
                },
            };
            var expr2 = new SelectExpression<TestEntity>
            {
                Select = e => new {e.Prop1, e.ExternalId},
            };
            Expression<Func<TestEntity, object>> expectedSelect =
                e =>
                    new DynamicTestEntity
                    {
                        Prop1 = e.Prop1,
                        Prop2 = e.Prop2,
                        Children = e.Children.Select(c => new DynamicTestEntity {Prop1 = c.Prop1}),
                        ExternalId = e.ExternalId
                    };
            var actual = _helper.Merge(expr1, expr2);
            SelectAssert.AreEqual(expectedSelect, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(1, actual.ExternalIncludes.Count);
            IncludeAssert.Contains(e => e.External, actual.ExternalIncludes);
        }

        [TestMethod]
        public void TestMergeSelectsWithConditions()
        {
            var expr1 = new SelectExpression<TestEntity>
            {
                Select = e => new {Children = e.Children.Where(c => c.Prop1 == 1).Select(c => new {c.Prop1})},
            };
            var expr2 = new SelectExpression<TestEntity>
            {
                Select = e => new {Children = e.Children.Where(c => c.Prop1 == 2).Select(c => new {c.Prop1})},
            };
            Expression<Func<TestEntity, object>> expectedSelect =
                e =>
                    new DynamicTestEntity
                    {
                        Children =
                            e.Children.Where(c => c.Prop1 == 1 || c.Prop1 == 2)
                                .Select(c => new DynamicTestEntity() {Prop1 = c.Prop1})
                    };
            var actual = _helper.Merge(expr1, expr2);
            SelectAssert.AreEqual(expectedSelect, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }

        [TestMethod]
        public void TestMergeSelectWithConditionAndSelectWithoutCondition()
        {
            var expr1 = new SelectExpression<TestEntity>
            {
                Select = e => new {Children = e.Children.Select(c => new {c.Prop1})},
            };
            var expr2 = new SelectExpression<TestEntity>
            {
                Select = e => new {Children = e.Children.Where(c => c.Prop1 == 2).Select(c => new {c.Prop1})},
            };
            Expression<Func<TestEntity, object>> expectedSelect =
                e =>
                    new DynamicTestEntity
                    {
                        Children = e.Children.Select(c => new DynamicTestEntity() {Prop1 = c.Prop1})
                    };
            var actual = _helper.Merge(expr1, expr2);
            SelectAssert.AreEqual(expectedSelect, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            actual = _helper.Merge(expr2, expr1);
            SelectAssert.AreEqual(expectedSelect, actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
        }

        [TestMethod]
        public void TestMergeWithNotMapped()
        {
            var expr1 = new SelectExpression<TestEntity>
            {
                Select = e => new {e.External },
            };
            var expr2 = new SelectExpression<TestEntity>
            {
                Select = e => new { e.Prop1 },
            };
            ExceptAssert.Throws<InvalidPathException>(() => _helper.Merge(expr1, expr2));
            ExceptAssert.Throws<InvalidPathException>(() => _helper.Merge(expr1, expr2));
        }

        private ISelectProvider<TestEntity> GetMockProvider(SelectExpression<TestEntity> providerSelect)
        {
            var mockProvider = new Mock<ISelectProvider<TestEntity>>(MockBehavior.Strict);
            mockProvider.Setup(mp => mp.Select).Returns(() => providerSelect);
            return mockProvider.Object;
        }
            
        [TestMethod]
        public void TestAddSelectIntersectingByOnePropertyDirectly()
        {
            var providerSelect = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop1 = te.Prop1,
                    ExternalId = te.ExternalId
                },
            };
            var actual = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity { Prop2 = te.Prop2, ExternalId = te.ExternalId }
            };
            _helper.AddSelect(actual, providerSelect, e => e);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop2 = te.Prop2,
                ExternalId = te.ExternalId,
                Prop1 = te.Prop1,
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectIntersectingByOnePropertyThroughProvider()
        {
            var providerSelect = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop1 = te.Prop1,
                    ExternalId = te.ExternalId
                },
            };
            var actual = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity { Prop2 = te.Prop2, ExternalId = te.ExternalId }
            };
            _helper.AddSelect(actual, GetMockProvider(providerSelect), e => e);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop2 = te.Prop2,
                ExternalId = te.ExternalId,
                Prop1 = te.Prop1,
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectNonIntersectingDirectly()
        {
            var providerSelect = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop1 = te.Prop1,
                    ExternalId = te.ExternalId
                },
            };
            var actual = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity { Prop2 = te.Prop2 }
            };
            _helper.AddSelect(actual, providerSelect, e => e.Parent);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop2 = te.Prop2,
                Parent = te.Parent != null ? new DynamicTestEntity { Prop1 = te.Parent.Prop1, ExternalId = te.Parent.ExternalId } : null,
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectNonIntersectingThroughProvider()
        {
            var providerSelect = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop1 = te.Prop1,
                    ExternalId = te.ExternalId
                },
            };
            var actual = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity { Prop2 = te.Prop2 }
            };
            _helper.AddSelect(actual, GetMockProvider(providerSelect), e => e.Parent);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop2 = te.Prop2,
                Parent = te.Parent != null ? new DynamicTestEntity { Prop1 = te.Parent.Prop1, ExternalId = te.Parent.ExternalId } : null,
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectFromCollectionDirectly()
        {
            var providerSelect = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop1 = te.Prop1,
                    ExternalId = te.ExternalId
                },
            };
            var actual = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity { Prop2 = te.Prop2 }
            };
            _helper.AddSelect(actual, providerSelect, e => e.Children);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop2 = te.Prop2,
                Children = te.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1, ExternalId = c.ExternalId }),
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectFromCollectionThroughProvider()
        {
            var providerSelect = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop1 = te.Prop1,
                    ExternalId = te.ExternalId
                },
            };
            var actual = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity { Prop2 = te.Prop2 }
            };
            _helper.AddSelect(actual, GetMockProvider(providerSelect), e => e.Children);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop2 = te.Prop2,
                Children = te.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1, ExternalId = c.ExternalId }),
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectWithObjectAndCollectionNonIntersectingDirectly()
        {
            var providerSelect = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop1 = te.Prop1,
                    Parent = new DynamicTestEntity { Prop1 = te.Parent.Prop1 },
                    Children = te.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 })
                },
            };
            var actual = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop2 = te.Prop2,
                    Parent = new DynamicTestEntity { Prop2 = te.Parent.Prop2 },
                    Children = te.Children.Select(c => new DynamicTestEntity { Prop2 = c.Prop2 })
                }
            };
            _helper.AddSelect(actual, providerSelect, e => e);
            Expression<Func<TestEntity, object>>  expectedSelect = te => new DynamicTestEntity
            {
                Prop2 = te.Prop2,
                Parent = te.Parent != null ? new DynamicTestEntity { Prop2 = te.Parent.Prop2, Prop1 = te.Parent.Prop1 } : null,
                Children = te.Children.Select(c => new DynamicTestEntity { Prop2 = c.Prop2, Prop1 = c.Prop1 }),
                Prop1 = te.Prop1,
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectWithObjectAndCollectionNonIntersectingThroughProvider()
        {
            var providerSelect = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop1 = te.Prop1,
                    Parent = new DynamicTestEntity { Prop1 = te.Parent.Prop1 },
                    Children = te.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 })
                },
            };
            var actual = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop2 = te.Prop2,
                    Parent = new DynamicTestEntity { Prop2 = te.Parent.Prop2 },
                    Children = te.Children.Select(c => new DynamicTestEntity { Prop2 = c.Prop2 })
                }
            };
            _helper.AddSelect(actual, GetMockProvider(providerSelect), e => e);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop2 = te.Prop2,
                Parent = te.Parent != null ? new DynamicTestEntity { Prop2 = te.Parent.Prop2, Prop1 = te.Parent.Prop1 } : null,
                Children = te.Children.Select(c => new DynamicTestEntity { Prop2 = c.Prop2, Prop1 = c.Prop1 }),
                Prop1 = te.Prop1,
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectWithObjectAndCollectionFromParentDirectly()
        {
            var providerSelect = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop1 = te.Prop1,
                    Parent = new DynamicTestEntity { Prop1 = te.Parent.Prop1 },
                    Children = te.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 })
                },
            };
            var actual = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop2 = te.Prop2,
                    Parent = new DynamicTestEntity { Prop2 = te.Parent.Prop2 },
                    Children = te.Children.Select(c => new DynamicTestEntity { Prop2 = c.Prop2 })
                }
            };
            _helper.AddSelect(actual, providerSelect, e => e.Parent);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop2 = te.Prop2,
                Parent = te.Parent != null ? new DynamicTestEntity
                {
                    Prop2 = te.Parent.Prop2,
                    Prop1 = te.Parent.Prop1,
                    Parent = te.Parent.Parent != null ? new DynamicTestEntity { Prop1 = te.Parent.Parent.Prop1 } : null,
                    Children = te.Parent.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 })
                } : null,
                Children = te.Children.Select(c => new DynamicTestEntity { Prop2 = c.Prop2 }),
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectWithObjectAndCollectionFromParentThroughProvider()
        {
            var providerSelect = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop1 = te.Prop1,
                    Parent = new DynamicTestEntity { Prop1 = te.Parent.Prop1 },
                    Children = te.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 })
                },
            };
            var actual = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop2 = te.Prop2,
                    Parent = new DynamicTestEntity { Prop2 = te.Parent.Prop2 },
                    Children = te.Children.Select(c => new DynamicTestEntity { Prop2 = c.Prop2 })
                }
            };
            _helper.AddSelect(actual, GetMockProvider(providerSelect), e => e.Parent);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop2 = te.Prop2,
                Parent = te.Parent != null ? new DynamicTestEntity
                {
                    Prop2 = te.Parent.Prop2,
                    Prop1 = te.Parent.Prop1,
                    Parent = te.Parent.Parent != null ? new DynamicTestEntity { Prop1 = te.Parent.Parent.Prop1 } : null,
                    Children = te.Parent.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 })
                } : null,
                Children = te.Children.Select(c => new DynamicTestEntity { Prop2 = c.Prop2 }),
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectWithObjectAndCollectionFromChildrenDirectly()
        {
            var providerSelect = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop1 = te.Prop1,
                    Parent = new DynamicTestEntity { Prop1 = te.Parent.Prop1 },
                    Children = te.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 })
                },
            };
            var actual = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop2 = te.Prop2,
                    Parent = new DynamicTestEntity { Prop2 = te.Parent.Prop2 },
                    Children = te.Children.Select(c => new DynamicTestEntity { Prop2 = c.Prop2 })
                }
            };
            _helper.AddSelect(actual, providerSelect, e => e.Children);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop2 = te.Prop2,
                Parent = te.Parent != null ? new DynamicTestEntity
                {
                    Prop2 = te.Parent.Prop2,
                } : null,
                Children = te.Children.Select(c => new DynamicTestEntity
                {
                    Prop2 = c.Prop2,
                    Prop1 = c.Prop1,
                    Parent = c.Parent != null ? new DynamicTestEntity { Prop1 = c.Parent.Prop1 } : null,
                    Children = c.Children.Select(cc => new DynamicTestEntity { Prop1 = cc.Prop1 })
                }),
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectWithObjectAndCollectionThroughProvider()
        {
            var providerSelect = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop1 = te.Prop1,
                    Parent = new DynamicTestEntity { Prop1 = te.Parent.Prop1 },
                    Children = te.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 })
                },
            };

            var actual = new SelectExpression<TestEntity>
            {
                Select = te => new DynamicTestEntity
                {
                    Prop2 = te.Prop2,
                    Parent = new DynamicTestEntity { Prop2 = te.Parent.Prop2 },
                    Children = te.Children.Select(c => new DynamicTestEntity { Prop2 = c.Prop2 })
                }
            };
            _helper.AddSelect(actual, GetMockProvider(providerSelect), e => e.Children);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop2 = te.Prop2,
                Parent = te.Parent != null ? new DynamicTestEntity
                {
                    Prop2 = te.Parent.Prop2,
                } : null,
                Children = te.Children.Select(c => new DynamicTestEntity
                    {
                        Prop2 = c.Prop2,
                        Prop1 = c.Prop1,
                        Parent = c.Parent != null ? new DynamicTestEntity { Prop1 = c.Parent.Prop1 } : null,
                        Children = c.Children.Select(cc => new DynamicTestEntity { Prop1 = cc.Prop1 })
                    }),
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectAnonymousPrimitivePropertiesToEmptySelect()
        {
            var actual = new SelectExpression<TestEntity>();
            _helper.AddSelect(actual, new SelectExpression<TestEntity>
            {
                Select = e => new { e.Prop1, e.Prop2 }
            }, e => e);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop1 = te.Prop1,
                Prop2 = te.Prop2,
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectAnonymousOneLevelObjectToEmptySelect()
        {
            var actual = new SelectExpression<TestEntity>();
            _helper.AddSelect(actual, new SelectExpression<TestEntity>
            {
                Select = e => new { e.Prop1, e.Prop2, Parent = new { e.Parent.Prop3 } }
            }, e => e);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop1 = te.Prop1,
                Prop2 = te.Prop2,
                Parent = te.Parent != null ? new DynamicTestEntity { Prop3 = te.Parent.Prop3 } : null,
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectAnonymousOneLevelCollectionToEmptySelect()
        {
            var actual = new SelectExpression<TestEntity>();
            _helper.AddSelect(actual, new SelectExpression<TestEntity>
            {
                Select = e => new { e.Prop1, e.Prop2, Children = e.Children.Select(ch => new { ch.Prop1 }) }
            }, e => e);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop1 = te.Prop1,
                Prop2 = te.Prop2,
                Children = te.Children.Select(ch => new DynamicTestEntity { Prop1 = ch.Prop1 }),
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectAnonymousTwoLevelsToEmptySelect()
        {
            var actual = new SelectExpression<TestEntity>();
            _helper.AddSelect(actual, new SelectExpression<TestEntity>
            {
                Select = e => new { e.Prop1, Children = e.Children.Select(ch => new { ch.Prop1, Parent = new { ch.Parent.Prop3 } }) }
            }, e => e);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop1 = te.Prop1,
                Children = te.Children.Select(ch => new DynamicTestEntity { Prop1 = ch.Prop1, Parent = ch.Parent != null ? new DynamicTestEntity { Prop3 = ch.Parent.Prop3 } : null }),
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectAnonymousFromSelf()
        {
            var actual = new SelectExpression<TestEntity>
            {
                Select = e => new
                {
                    e.Prop1,
                    Children = e.Children.Select(c => new { c.Prop2 }),
                    Parent = new { e.Parent.Prop2 },
                }
            };
            _helper.AddSelect(actual, new SelectExpression<TestEntity>
            {
                Select = e => new { e.Prop3 }
            }, e => e);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop1 = te.Prop1,
                Children = te.Children.Select(ch => new DynamicTestEntity
                {
                    Prop2 = ch.Prop2,
                }),
                Parent = te.Parent != null ? new DynamicTestEntity
                {
                    Prop2 = te.Parent.Prop2
                } : null,
                Prop3 = te.Prop3,
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectAnonymousFromProperty()
        {
            var actual = new SelectExpression<TestEntity>
            {
                Select = e => new
                {
                    e.Prop1,
                    Children = e.Children.Select(c => new { c.Prop2 }),
                    Parent = new { e.Parent.Prop2 },
                }
            };
            _helper.AddSelect(actual, new SelectExpression<TestEntity>
            {
                Select = e => new { e.Prop3 }
            }, e => e.Parent);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop1 = te.Prop1,
                Children = te.Children.Select(ch => new DynamicTestEntity
                {
                    Prop2 = ch.Prop2,
                }),
                Parent = te.Parent != null ? new DynamicTestEntity
                {
                    Prop2 = te.Parent.Prop2,
                    Prop3 = te.Parent.Prop3,
                } : null,

            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectAnonymousFromCollection()
        {
            var actual = new SelectExpression<TestEntity>
            {
                Select = e => new
                {
                    e.Prop1,
                    Children = e.Children.Select(c => new { c.Prop2 }),
                    Parent = new { e.Parent.Prop2 },
                }
            };
            _helper.AddSelect(actual, new SelectExpression<TestEntity>
            {
                Select = e => new { e.Prop3 }
            }, e => e.Children);
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop1 = te.Prop1,
                Children = te.Children.Select(ch => new DynamicTestEntity
                {
                    Prop2 = ch.Prop2,
                    Prop3 = ch.Prop3,
                }),
                Parent = te.Parent != null ? new DynamicTestEntity
                {
                    Prop2 = te.Parent.Prop2,
                } : null,

            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectAnonymousFromPropertyOfCollection()
        {
            var actual = new SelectExpression<TestEntity>
            {
                Select = e => new
                {
                    e.Prop1,
                    Children = e.Children.Select(c => new
                    {
                        c.Prop2,
                        Parent = new { c.Parent.Prop1 }
                    }),
                }
            };
            _helper.AddSelect(actual, new SelectExpression<TestEntity>
            {
                Select = e => new { e.Prop3 }
            }, e => e.Children.Select(c => c.Parent));
            Expression<Func<TestEntity, object>> expectedSelect = te => new DynamicTestEntity
            {
                Prop1 = te.Prop1,
                Children = te.Children.Select(ch => new DynamicTestEntity
                {
                    Prop2 = ch.Prop2,
                    Parent = ch.Parent != null ? new DynamicTestEntity
                    {
                        Prop3 = ch.Parent.Prop3,
                        Prop1 = ch.Parent.Prop1,
                    } : null
                }),
            };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectAnonymousFromInterfaceToSelectFromClassDirectly()
        {
            var actual = new SelectExpression<TestEntity> { Select = e => new { e.Prop2 } };
            _helper.AddSelect(actual, new SelectExpression<ITestEntity> { Select = e => new { e.Prop1 } }, e => e);
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity { Prop1 = e.Prop1, Prop2 = e.Prop2 };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectAnonymousFromInterfaceToSelectFromClassThroughProvider()
        {
            var mockIprovider = new Mock<ISelectProvider<ITestEntity>>(MockBehavior.Strict);
            mockIprovider.Setup(p => p.Select).Returns(new SelectExpression<ITestEntity> { Select = e => new { e.Prop1 } });

            var actual = new SelectExpression<TestEntity> { Select = e => new { e.Prop2 } };
            _helper.AddSelect(actual, mockIprovider.Object, e => e);
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity { Prop1 = e.Prop1, Prop2 = e.Prop2 };
            SelectAssert.AreEqual(expectedSelect, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectAnonymousFromInterfaceToSelectFromInterfaceThroughProvider()
        {
            var mockIprovider = new Mock<ISelectProvider<ITestEntity>>(MockBehavior.Strict);
            mockIprovider.Setup(p => p.Select).Returns(new SelectExpression<ITestEntity> { Select = e => new { e.Prop1 } });

            var iactual = new SelectExpression<ITestEntity> { Select = e => new { e.Prop2 } };
            _helper.AddSelect(iactual, mockIprovider.Object, e => e);
            Expression<Func<ITestEntity, object>> expectedSelect = e => new DynamicTestEntity { Prop1 = e.Prop1, Prop2 = e.Prop2 };
            SelectAssert.AreEqual(expectedSelect, iactual.Select);
        }

        [TestMethod]
        public void TestCreateFromInterfaceWithPrimitiveProperties()
        {
            var actual = _helper.CreateFromInterface<From, IFrom>();
            var expected = new SelectExpression<From> {Select = f => new DynamicFrom {Id = f.Id, Name = f.Name}};
            SelectAssert.AreEqual(expected.Select, actual.Select);
        }

        [TestMethod]
        public void TestCreateFromInterfaceWithObjectAndCollectionProperties()
        {
            var actual = _helper.CreateFromInterface<From, IComplexFrom>();
            var expected = new SelectExpression<From>
            {
                Select = f => new DynamicFrom
                {
                    Id = f.Id,
                    Parent = f.Parent != null ? new DynamicFrom {Id = f.Parent.Id, Name = f.Parent.Name} : null,
                    Children = f.Children.Select(c => new DynamicFrom {Id = c.Id, Name = c.Name})
                }
            };
            SelectAssert.AreEqual(expected.Select, actual.Select);
        }

        [TestMethod]
        public void TestCreateFromInterfaceIgnoringEntityProperty()
        {
            var actual = _helper.CreateFromInterface<From, IFromWithEntity>(true);
            var expected = new SelectExpression<From>
            {
                Select = f => new DynamicFrom
                {
                    Id = f.Id,
                }
            };
            SelectAssert.AreEqual(expected.Select, actual.Select);
        }

        [TestMethod]
        public void TestCreateFromExplicitlyImplementedInterface()
        {
            var actual = _helper.CreateFromInterface<From, IExplicit>(true);
            var expected = new SelectExpression<From>
            {
                Select = f => new DynamicFrom
                {
                    Id = f.Id,
                }
            };
            SelectAssert.AreEqual(expected.Select, actual.Select);
        }

        [TestMethod]
        public void TestAddSelectInterfaceWithEntityTypePropertyToEmptyInterface()
        {
            var select = new SelectExpression<ITestEntityWithEntityProp>();
            _helper.AddSelect(select, new SelectExpression<ITestEntityWithEntityProp>
            {
                Select = ite => new
                {
                    ite.Prop1,
                    Parent = new {ite.Parent.Prop1}
                }
            }, e => e);
            var expected = new SelectExpression<ITestEntityWithEntityProp>
            {
                Select = ite => new TestEntity
                {
                    Prop1 = ite.Prop1,
                    Parent = ite.Parent != null ? new TestEntity {Prop1 = ite.Parent.Prop1} : null,
                }
            };
            SelectAssert.AreEqual(expected.Select, select.Select);
        }
    }
}
