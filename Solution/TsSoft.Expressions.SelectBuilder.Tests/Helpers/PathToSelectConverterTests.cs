﻿namespace TsSoft.Expressions.SelectBuilder.Tests.Helpers
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Helpers;
    using TsSoft.Expressions.SelectBuilder.Models;

    [TestClass]
    public class PathToSelectConverterTests
    {
        private ParsedPath Parse<T>(Expression<Func<T, object>> path)
        {
            return PathParserVisitor.Parse(
                path, 
                StaticHelpers.Library,
                StaticHelpers.ExpressionBuilder);
        }

        [NotNull]private PathToSelectConverter _converter;

        [TestInitialize]
        public void Init()
        {
            _converter = AbstractMapperDependenciesFactory.GetDependency<PathToSelectConverter>(
                MapperTypes.None,
                Entities.Are(typeof(TestEntity)),
                External.Are());
        }

        [TestMethod]
        public void OnePathConvertTest()
        {
            var paths = new[]
            {
                Parse<TestEntity>(f => f.Children.Select(c => c.Prop1)),
            };

            var actual = _converter.Convert<TestEntity>(paths, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity {Children = e.Children.Select(c => new DynamicTestEntity {Prop1 = c.Prop1})};
            SelectAssert.AreEqual(
                expectedSelect,
                actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(0, actual.ExternalIncludes.Count);
        }

        [TestMethod]
        public void TwoDivergingPathsConvertTest()
        {
            var paths = new[]
            {
                Parse<TestEntity>(f => f.Children.Select(c => c.Prop1)),
                Parse<TestEntity>(f => f.Parent.Prop2)
            };

            var actual = _converter.Convert<TestEntity>(paths, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Children = e.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 }),
                Parent = e.Parent != null ? new DynamicTestEntity {Prop2 = e.Parent.Prop2} : null,
            };
            SelectAssert.AreEqual(
                expectedSelect,
                actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(0, actual.ExternalIncludes.Count);
        }

        [TestMethod]
        public void TwoSimilarPathsConvertTest()
        {
            var paths = new[]
            {
                Parse<TestEntity>(f => f.Children.Select(c => c.Prop1)),
                Parse<TestEntity>(f => f.Children.Select(c => c.Prop2)),
            };

            var actual = _converter.Convert<TestEntity>(paths, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Children = e.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1, Prop2 = c.Prop2 }),
            };
            SelectAssert.AreEqual(
                expectedSelect,
                actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(0, actual.ExternalIncludes.Count);
        }

        [TestMethod]
        public void OnePathWithConditionConvertTest()
        {
            var paths = new[]
            {
                Parse<TestEntity>(f => f.Children.Where(c => c.Prop2 == "2").Select(c => c.Prop1)),
            };

            var actual = _converter.Convert<TestEntity>(paths, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Children = e.Children.Where(c => c.Prop2 == "2").Select(c => new DynamicTestEntity { Prop1 = c.Prop1, Prop2 = c.Prop2 }),
            };
            SelectAssert.AreEqual(
                expectedSelect,
                actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(0, actual.ExternalIncludes.Count);
        }

        [TestMethod]
        public void TwoPathsWithConditionsConvertTest()
        {
            var paths = new[]
            {
                Parse<TestEntity>(f => f.Children.Where(c => c.Prop2 == "2").Select(c => c.Prop1)),
                Parse<TestEntity>(f => f.Children.Where(d => d.Prop3 == 1).Select(c => c.Prop1))
            };

            var actual = _converter.Convert<TestEntity>(paths, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Children = e.Children.Where(c => c.Prop2 == "2" || c.Prop3 == 1).Select(c => new DynamicTestEntity { Prop1 = c.Prop1, Prop2 = c.Prop2, Prop3 = c.Prop3 }),
            };
            SelectAssert.AreEqual(
                expectedSelect,
                actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(0, actual.ExternalIncludes.Count);
        }

        [TestMethod]
        public void OnePathWithConditionOnOuterParameterTest()
        {
            var paths = new[]
            {
                Parse<TestEntity>(f => f.Children.Where(c => c.Prop1 == f.Prop1).Select(c => c.Prop2)),
            };

            var actual = _converter.Convert<TestEntity>(paths, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Prop1 = e.Prop1,
                Children = e.Children.Where(c => c.Prop1 == e.Prop1).Select(c => new DynamicTestEntity { Prop1 = c.Prop1, Prop2 = c.Prop2 }),
            };
            SelectAssert.AreEqual(
                expectedSelect,
                actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(0, actual.ExternalIncludes.Count);
        }

        [TestMethod]
        public void TwoPathsWithConditionsOnOuterParametersTest()
        {
            var paths = new[]
            {
                Parse<TestEntity>(f => f.Children.Where(c => c.Prop1 == f.Prop1).Select(c => c.Prop2)),
                Parse<TestEntity>(f => f.Children.Where(c => c.Prop2 == f.Prop2).Select(c => c.Prop3)),
            };

            var actual = _converter.Convert<TestEntity>(paths, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Prop1 = e.Prop1,
                Prop2 = e.Prop2,
                Children = e.Children.Where(c => c.Prop1 == e.Prop1 || c.Prop2 == e.Prop2).Select(c => new DynamicTestEntity { Prop1 = c.Prop1, Prop2 = c.Prop2, Prop3 = c.Prop3 }),
            };
            SelectAssert.AreEqual(
                expectedSelect,
                actual.Select);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual.Select));
            Assert.AreEqual(0, actual.ExternalIncludes.Count);
        }
    }
}
