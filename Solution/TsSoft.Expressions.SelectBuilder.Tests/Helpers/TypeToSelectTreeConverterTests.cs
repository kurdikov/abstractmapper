﻿// ReSharper disable CheckNamespace
namespace TsSoft.Expressions.SelectBuilder.Helpers.Tests
// ReSharper restore CheckNamespace
{
    using System;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Models;

    [TestClass]
    public class TypeToSelectTreeConverterTests
    {
        [NotNull]
        private TypeToSelectTreeConverter _typeConverter;

        [TestInitialize]
        public void Init()
        {
            _typeConverter = AbstractMapperDependenciesFactory.GetDependency<TypeToSelectTreeConverter>(
                MockObjects.None,
                Entities.Are(typeof(From)),
                External.None);
        }

        public ValueHoldingMember GetFromProp(string name)
        {
            return ValueHoldingMember.GetValueHoldingMember(typeof(From), name);
        }

        public ValueHoldingMember GetFromProp(string name, BindingFlags flags)
        {
            return new ValueHoldingMember(typeof(From).GetProperty(name, flags));
        }

        public ValueHoldingMember GetIFromProp(string name)
        {
            return ValueHoldingMember.GetValueHoldingMember(typeof(IFrom), name);
        }

        [TestMethod]
        public void TestMakeTreeFromTwoPrimitiveProps()
        {
            var actual = _typeConverter.MakeTree<From, IFrom>(false);
            Assert.AreEqual(2, actual.RootNode.NextLevel.Count);
            Assert.IsNull(actual.RootNode.Condition);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetFromProp("Id")));
            Assert.IsNull(actual.RootNode.NextLevel[GetFromProp("Id")]);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetFromProp("Name")));
            Assert.IsNull(actual.RootNode.NextLevel[GetFromProp("Name")]);
        }

        [TestMethod]
        public void TestMakeTreeFromInheritedInterface()
        {
            var actual = _typeConverter.MakeTree<From, IInheritedFrom>(false);
            Assert.AreEqual(3, actual.RootNode.NextLevel.Count);
            Assert.IsNull(actual.RootNode.Condition);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetFromProp("Id")));
            Assert.IsNull(actual.RootNode.NextLevel[GetFromProp("Id")]);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetFromProp("Name")));
            Assert.IsNull(actual.RootNode.NextLevel[GetFromProp("Name")]);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetFromProp("Guid")));
            Assert.IsNull(actual.RootNode.NextLevel[GetFromProp("Guid")]);
        }

        [TestMethod]
        public void TestMakeTreeFromInterfaceWithObjectAndCollection()
        {
            var actual = _typeConverter.MakeTree<From, IComplexFrom>(false);
            Assert.AreEqual(3, actual.RootNode.NextLevel.Count);
            Assert.IsNull(actual.RootNode.Condition);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetFromProp("Id")));
            Assert.IsNull(actual.RootNode.NextLevel[GetFromProp("Id")]);

            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetFromProp(typeof(IComplexFrom).FullName + ".Parent", BindingFlags.NonPublic | BindingFlags.Instance)));
            var parent = actual.RootNode.NextLevel[GetFromProp(typeof(IComplexFrom).FullName + ".Parent", BindingFlags.NonPublic | BindingFlags.Instance)];
            Assert.IsNotNull(parent);
            Assert.IsNull(parent.Condition);
            Assert.AreEqual(2, parent.NextLevel.Count);
            Assert.IsTrue(parent.NextLevel.ContainsKey(GetIFromProp("Id")));
            Assert.IsNull(parent.NextLevel[GetIFromProp("Id")]);
            Assert.IsTrue(parent.NextLevel.ContainsKey(GetIFromProp("Name")));
            Assert.IsNull(parent.NextLevel[GetIFromProp("Name")]);

            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetFromProp("InterfaceChildren")));
            var children = actual.RootNode.NextLevel[GetFromProp("InterfaceChildren")];
            Assert.IsNotNull(children);
            Assert.IsNull(children.Condition);
            Assert.AreEqual(2, children.NextLevel.Count);
            Assert.IsTrue(children.NextLevel.ContainsKey(GetIFromProp("Id")));
            Assert.IsNull(children.NextLevel[GetIFromProp("Id")]);
            Assert.IsTrue(children.NextLevel.ContainsKey(GetIFromProp("Name")));
            Assert.IsNull(children.NextLevel[GetIFromProp("Name")]);
        }

        [TestMethod]
        public void TestMakeTreeFromInterfaceWithTwoCollections()
        {
            var actual = _typeConverter.MakeTree<From, ITwoFroms>(false);
            Assert.AreEqual(2, actual.RootNode.NextLevel.Count);
            Assert.IsNull(actual.RootNode.Condition);

            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetFromProp("InterfaceChildren")));
            var children = actual.RootNode.NextLevel[GetFromProp("InterfaceChildren")];
            Assert.IsNotNull(children);
            Assert.IsNull(children.Condition);
            Assert.AreEqual(2, children.NextLevel.Count);
            Assert.IsTrue(children.NextLevel.ContainsKey(GetIFromProp("Id")));
            Assert.IsNull(children.NextLevel[GetIFromProp("Id")]);
            Assert.IsTrue(children.NextLevel.ContainsKey(GetIFromProp("Name")));
            Assert.IsNull(children.NextLevel[GetIFromProp("Name")]);

            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetFromProp("AllChildren")));
            var allChildren = actual.RootNode.NextLevel[GetFromProp("AllChildren")];
            Assert.IsNotNull(allChildren);
            Assert.IsNull(allChildren.Condition);
            Assert.AreEqual(2, allChildren.NextLevel.Count);
            Assert.IsTrue(allChildren.NextLevel.ContainsKey(GetIFromProp("Id")));
            Assert.IsNull(allChildren.NextLevel[GetIFromProp("Id")]);
            Assert.IsTrue(allChildren.NextLevel.ContainsKey(GetIFromProp("Name")));
            Assert.IsNull(allChildren.NextLevel[GetIFromProp("Name")]);
        }

        [TestMethod]
        public void TestMakePathsWithEntityTypesInInterface()
        {
            var actual = _typeConverter.MakeTree<From, IFromWithEntity>(true);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsNull(actual.RootNode.Condition);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetFromProp("Id")));
            Assert.IsNull(actual.RootNode.NextLevel[GetFromProp("Id")]);

            ExceptAssert.Throws<InvalidOperationException>(() => _typeConverter.MakeTree<From, IFromWithEntity>(false));
        }

        [TestMethod]
        public void TestMakeTreeWithExplicitImplementation()
        {
            var actual = _typeConverter.MakeTree<From, IExplicit>(true);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsNull(actual.RootNode.Condition);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetFromProp(typeof(IExplicit).FullName + ".Name", BindingFlags.NonPublic | BindingFlags.Instance)));
            Assert.IsNull(actual.RootNode.NextLevel[GetFromProp(typeof(IExplicit).FullName + ".Name", BindingFlags.NonPublic | BindingFlags.Instance)]);
        }
    }
}
