﻿namespace TsSoft.Expressions.SelectBuilder.Tests.Helpers
{
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Helpers;

    [TestClass]
    public class SelectTreeSplitterTests
    {
        [NotNull] private SelectTreeSplitter _splitter;

        private ValueHoldingMember Prop([NotNull] string propName)
        {
            return new ValueHoldingMember(typeof(TestEntity).GetProperty(propName));
        }

        [TestInitialize]
        public void Init()
        {
            _splitter = new SelectTreeSplitter(new EntityTypesHelper(new CustomEnvironmentTestEntityTypesRetriever(Entities.Are(typeof(TestEntity)), External.Are(typeof(ExternalEntity)))));
        }

        [TestMethod]
        public void NoExternalPropertiesTest()
        {
            var tree = new SelectTree(
                Expression.Parameter(typeof(TestEntity)), new SelectTreeLevel
                {
                    {Prop("Prop1"), null},
                    {Prop("Prop2"), null},
                });
            var external = _splitter.CutExternalTrees(tree);
            Assert.IsNotNull(tree.RootNode.NextLevel);
            Assert.AreEqual(2, tree.RootNode.NextLevel.Count);
            Assert.IsTrue(tree.RootNode.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsTrue(tree.RootNode.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.AreEqual(0, external.Count);
        }

        [TestMethod]
        public void OneTopLevelExternalPropertyTest()
        {
            var extNode = new SelectTreeNode(typeof(External));
            var tree = new SelectTree(
                Expression.Parameter(typeof(TestEntity)), new SelectTreeLevel
                {
                    {Prop("Prop1"), null},
                    {Prop("External"), extNode},
                    {Prop("Prop2"), null},
                });
            var external = _splitter.CutExternalTrees(tree);
            Assert.IsNotNull(tree.RootNode.NextLevel);
            Assert.AreEqual(3, tree.RootNode.NextLevel.Count);
            Assert.IsTrue(tree.RootNode.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsTrue(tree.RootNode.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsTrue(tree.RootNode.NextLevel.ContainsKey(Prop("ExternalId")));
            Assert.AreEqual(1, external.Count);
            Assert.AreEqual(1, external.ElementAt(0).Key.Count);
            Assert.AreEqual(Prop("External"), external.ElementAt(0).Key.ElementAt(0));
            Assert.AreSame(extNode, external.ElementAt(0).Value);
        }

        [TestMethod]
        public void OneInnerExternalPropertyTest()
        {
            var extNode = new SelectTreeNode(typeof(External));
            var tree = new SelectTree(
                Expression.Parameter(typeof(TestEntity)), new SelectTreeLevel
                {
                    {Prop("Prop1"), null},
                    {Prop("Prop2"), null},
                    {
                        Prop("Parent"),
                        new SelectTreeNode(typeof(TestEntity))
                        {
                            NextLevel = new SelectTreeLevel {{Prop("Prop1"), null}, {Prop("External"), extNode}}
                        }
                    },
                });
            var external = _splitter.CutExternalTrees(tree);
            Assert.IsNotNull(tree.RootNode.NextLevel);
            Assert.AreEqual(3, tree.RootNode.NextLevel.Count);
            Assert.IsTrue(tree.RootNode.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsTrue(tree.RootNode.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsTrue(tree.RootNode.NextLevel.ContainsKey(Prop("Parent")));
            var parent = tree.RootNode.NextLevel[Prop("Parent")];
            Assert.AreEqual(2, parent.NextLevel.Count);
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("ExternalId")));
            Assert.AreEqual(1, external.Count);
            Assert.AreEqual(2, external.ElementAt(0).Key.Count);
            Assert.AreEqual(Prop("Parent"), external.ElementAt(0).Key.ElementAt(0));
            Assert.AreEqual(Prop("External"), external.ElementAt(0).Key.ElementAt(1));
            Assert.AreSame(extNode, external.ElementAt(0).Value);
        }

        [TestMethod]
        public void ManyExternalPropertiesTest()
        {
            var extNode1 = new SelectTreeNode(typeof(External));
            var extNode2 = new SelectTreeNode(typeof(External));
            var extNode3 = new SelectTreeNode(typeof(External));
            var tree = new SelectTree(Expression.Parameter(typeof(TestEntity)), new SelectTreeLevel
            {
                {Prop("Prop1"), null},
                {Prop("External"), extNode1},
                {Prop("Prop2"), null},
                {
                    Prop("Parent"), new SelectTreeNode(typeof(TestEntity)) {NextLevel = new SelectTreeLevel {
                        {Prop("Prop1"), null}, 
                        {Prop("External"), extNode2}, 
                        {Prop("Parent"), new SelectTreeNode(typeof(TestEntity)){NextLevel = new SelectTreeLevel {
                            {Prop("External"), extNode3},
                            {Prop("Prop1"), null},
                        }}}}}},
            });
            var external = _splitter.CutExternalTrees(tree);
            Assert.IsNotNull(tree.RootNode.NextLevel);
            Assert.AreEqual(4, tree.RootNode.NextLevel.Count);
            Assert.IsTrue(tree.RootNode.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsTrue(tree.RootNode.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsTrue(tree.RootNode.NextLevel.ContainsKey(Prop("ExternalId")));
            Assert.IsTrue(tree.RootNode.NextLevel.ContainsKey(Prop("Parent")));
            var parent = tree.RootNode.NextLevel[Prop("Parent")];
            Assert.AreEqual(3, parent.NextLevel.Count);
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("ExternalId")));
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("Parent")));
            var parentParent = parent.NextLevel[Prop("Parent")];
            Assert.AreEqual(2, parentParent.NextLevel.Count);
            Assert.IsTrue(parentParent.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsTrue(parentParent.NextLevel.ContainsKey(Prop("ExternalId")));
            Assert.AreEqual(3, external.Count);
            var extPathTree = external.FirstOrDefault(t => t.Key.Count == 1 && t.Key.ElementAt(0).Equals(Prop("External")));
            Assert.IsNotNull(extPathTree);
            Assert.AreSame(extNode1, extPathTree.Value);
            extPathTree = external.FirstOrDefault(t => t.Key.Count == 2 && t.Key.ElementAt(0).Equals(Prop("Parent")) && t.Key.ElementAt(1).Equals(Prop("External")));
            Assert.IsNotNull(extPathTree);
            Assert.AreSame(extNode2, extPathTree.Value);
            extPathTree = external.FirstOrDefault(t => t.Key.Count == 3 && t.Key.ElementAt(0).Equals(Prop("Parent")) && t.Key.ElementAt(1).Equals(Prop("Parent")) && t.Key.ElementAt(2).Equals(Prop("External")));
            Assert.IsNotNull(extPathTree);
            Assert.AreSame(extNode3, extPathTree.Value);
        }
    }
}
