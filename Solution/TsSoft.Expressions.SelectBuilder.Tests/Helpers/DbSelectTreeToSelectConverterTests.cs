﻿namespace TsSoft.Expressions.SelectBuilder.Tests.Helpers
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Helpers;
    using TsSoft.Expressions.SelectBuilder.Models;

    [TestClass]
    public class DbSelectTreeToSelectConverterTests
    {
        [NotNull]private DbSelectTreeToSelectConverter _converter;

        [TestInitialize]
        public void Init()
        {
            _converter = AbstractMapperDependenciesFactory.GetDependency<DbSelectTreeToSelectConverter>(
                MapperTypes.None,
                Entities.Are(typeof(TestEntity)),
                External.Are());
        }

        private ValueHoldingMember Prop([NotNull]string propName)
        {
            return new ValueHoldingMember(typeof(TestEntity).GetTypeInfo().GetProperty(propName));
        }

        [TestMethod]
        public void OnePropertyConvertTest()
        {
            var tree = new SelectTree(Expression.Parameter(typeof(TestEntity)))
            {
                RootNode =
                {
                    NextLevel = new SelectTreeLevel
                    {
                        {Prop("Prop1"), null},
                    }
                }
            };
            var actual = _converter.Convert<TestEntity>(tree, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity { Prop1 = e.Prop1 };
            SelectAssert.AreEqual(expectedSelect, actual);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual));
        }

        [TestMethod]
        public void TwoPropertiesConvertTest()
        {
            var tree = new SelectTree(Expression.Parameter(typeof(TestEntity)))
            {
                RootNode =
                {
                    NextLevel = new SelectTreeLevel
                    {
                        {Prop("Prop1"), null},
                        {Prop("Prop2"), null}
                    }
                }
            };
            var actual = _converter.Convert<TestEntity>(tree, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity { Prop1 = e.Prop1, Prop2 = e.Prop2 };
            SelectAssert.AreEqual(expectedSelect, actual);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual));
        }

        [TestMethod]
        public void OnePathConvertTest()
        {
            var tree = new SelectTree(Expression.Parameter(typeof(TestEntity)))
            {
                RootNode =
                {
                    NextLevel = new SelectTreeLevel
                    {
                        {
                            Prop("Children"), new SelectTreeNode(typeof(TestEntity))
                            {
                                NextLevel = new SelectTreeLevel
                                {
                                    {Prop("Prop1"), null}
                                }
                            }
                        }
                    }
                }
            };
            var actual = _converter.Convert<TestEntity>(tree, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity { Children = e.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 }) };
            SelectAssert.AreEqual(expectedSelect, actual);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual));
        }

        [TestMethod]
        public void TwoDivergingPathsConvertTest()
        {
            var tree = new SelectTree(Expression.Parameter(typeof(TestEntity)))
            {
                RootNode =
                {
                    NextLevel = new SelectTreeLevel
                    {
                        {Prop("Children"), new SelectTreeNode(typeof(TestEntity))
                            {
                                NextLevel = new SelectTreeLevel { {Prop("Prop1"), null} }
                            }
                        },
                        {Prop("Parent"), new SelectTreeNode(typeof(TestEntity))
                            {
                                NextLevel = new SelectTreeLevel {{Prop("Prop2"), null}}
                            }
                        }
                    }
                }
            };

            var actual = _converter.Convert<TestEntity>(tree, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Children = e.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1 }),
                Parent = e.Parent != null ? new DynamicTestEntity { Prop2 = e.Parent.Prop2 } : null,
            };
            SelectAssert.AreEqual(expectedSelect, actual);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual));
        }

        [TestMethod]
        public void TwoPathsThroughOnePropertyTest()
        {
            var tree = new SelectTree(Expression.Parameter(typeof(TestEntity)))
            {
                RootNode =
                {
                    NextLevel = new SelectTreeLevel
                    {
                        {
                            Prop("Children"), new SelectTreeNode(typeof(TestEntity))
                            {
                                NextLevel = new SelectTreeLevel
                                {
                                    {Prop("Prop1"), null},
                                    {Prop("Prop2"), null}
                                }
                            }
                        }
                    }
                }
            };

            var actual = _converter.Convert<TestEntity>(tree, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Children = e.Children.Select(c => new DynamicTestEntity { Prop1 = c.Prop1, Prop2 = c.Prop2 }),
            };
            SelectAssert.AreEqual(expectedSelect, actual);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual));
        }

        [TestMethod]
        public void OnePathWithConditionTest()
        {
            Expression<Func<TestEntity, bool>> condition = c => c.Prop2 == "2";
            var tree = new SelectTree(Expression.Parameter(typeof(TestEntity)))
            {
                RootNode =
                {
                    NextLevel = new SelectTreeLevel
                    {
                        {
                            Prop("Children"), new SelectTreeNode(typeof(TestEntity), condition)
                            {
                                NextLevel = new SelectTreeLevel
                                {
                                    {Prop("Prop1"), null},
                                }
                            }
                        }
                    }
                }
            };

            var actual = _converter.Convert<TestEntity>(tree, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Children = e.Children.Where(c => c.Prop2 == "2").Select(c => new DynamicTestEntity { Prop1 = c.Prop1 }),
            };
            SelectAssert.AreEqual(expectedSelect, actual);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual));
        }

        [TestMethod]
        public void TwoConditionsTest()
        {
            Expression<Func<TestEntity, bool>> condition = c => c.Prop2 == "2";
            Expression<Func<TestEntity, bool>> condition2 = d => d.Prop3 == 1;
            var tree = new SelectTree(Expression.Parameter(typeof(TestEntity)))
            {
                RootNode =
                {
                    NextLevel = new SelectTreeLevel
                    {
                        {
                            Prop("Children"), new SelectTreeNode(typeof(TestEntity), condition: condition)
                            {
                                NextLevel = new SelectTreeLevel
                                {
                                    {Prop("Prop1"), null},
                                    {
                                        Prop("Children"), new SelectTreeNode(typeof(TestEntity), condition: condition2)
                                        {
                                            NextLevel = new SelectTreeLevel
                                            {
                                                {Prop("Prop2"), null},
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            var actual = _converter.Convert<TestEntity>(tree, new SelectSettings());
            Expression<Func<TestEntity, object>> expectedSelect = e => new DynamicTestEntity
            {
                Children = e.Children.Where(c => c.Prop2 == "2").Select(c => new DynamicTestEntity { Prop1 = c.Prop1, Children = c.Children.Where(cc => cc.Prop3 == 1).Select(cc => new DynamicTestEntity {Prop2 = cc.Prop2})}),
            };
            SelectAssert.AreEqual(expectedSelect, actual);
            Assert.IsNull(NewExpressionTypeUniquenessChecker.Check(actual));
        }
    }
}
