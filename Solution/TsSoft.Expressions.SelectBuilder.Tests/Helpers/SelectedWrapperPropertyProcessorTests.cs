﻿namespace TsSoft.Expressions.SelectBuilder.Tests.Helpers
{
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses.Database;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Helpers;

    [TestClass]
    public class SelectedWrapperPropertyProcessorTests
    {
        [NotNull]private WrapperPropertyProcessor _processor;

        [TestInitialize]
        public void Init()
        {
            _processor =
                new WrapperPropertyProcessor(
                    new EntityTypesHelper(
                        new CustomEnvironmentTestEntityTypesRetriever(
                            Entities.Are(typeof(First)), External.Are(typeof(TestExternalEntity)))),
                    new SelectTreeMerger(),
                    new SelectTreeTypeChanger(StaticHelpers.CorrespondingMemberHelper),
                    StaticHelpers.CorrespondingMemberHelper);
        }

        private ValueHoldingMember Prop([NotNull] string propName)
        {
            return new ValueHoldingMember(typeof(First).GetTypeInfo().GetProperty(propName));
        }

        private ValueHoldingMember SProp([NotNull] string propName)
        {
            return new ValueHoldingMember(typeof(Second).GetTypeInfo().GetProperty(propName));
        }

        [TestMethod]
        public void TestNoWrapperProperties()
        {
            var actual = new SelectTree(
                Expression.Parameter(typeof(First)), new SelectTreeLevel
                {
                    {Prop("Name"), null},
                    {Prop("ZeroId"), null},
                });
            _processor.ProcessWrapperProperties(actual);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(2, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Name")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("ZeroId")));
        }

        [TestMethod]
        public void TestDependentProperty()
        {
            var actual = new SelectTree(
                Expression.Parameter(typeof(First)), new SelectTreeLevel
                {
                    {Prop("Name"), null},
                    {Prop("NullableBoolIsTrue"), null},
                    {Prop("ZeroId"), null},
                });
            _processor.ProcessWrapperProperties(actual);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(3, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Name")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("NullableBool")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("ZeroId")));
        }

        [TestMethod]
        public void TestNavigationWrapper()
        {
            var actual = new SelectTree(Expression.Parameter(typeof(First)), new SelectTreeLevel
            {
                {Prop("Name"), null},
                {Prop("SecondsWrapper"), new SelectTreeNode(typeof(Second)) {NextLevel = new SelectTreeLevel() {{SProp("Id"), null}}}},
                {Prop("ZeroId"), null},
            });
            _processor.ProcessWrapperProperties(actual);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(3, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Name")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Seconds")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("ZeroId")));
            var seconds = actual.RootNode.NextLevel[Prop("Seconds")];
            Assert.AreEqual(1, seconds.NextLevel.Count);
            Assert.IsTrue(seconds.NextLevel.ContainsKey(SProp("Id")));
        }
    }
}
