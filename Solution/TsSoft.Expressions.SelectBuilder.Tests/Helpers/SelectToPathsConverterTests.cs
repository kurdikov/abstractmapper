﻿namespace TsSoft.Expressions.SelectBuilder.Tests.Helpers
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.SelectBuilder.Helpers;

    [TestClass]
    public class SelectToPathsConverterTests
    {
        [TestMethod]
        public void OnePrimitivePropertySelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new {e.Prop1}).ToList();
            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(1, actual[0].Elements.Count);
            Assert.AreEqual("Prop1", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);
        }

        [TestMethod]
        public void OnePrimitivePropertyMemberBindSelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new DynamicTestEntity() {Prop1 = e.Prop1}).ToList();
            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(1, actual[0].Elements.Count);
            Assert.AreEqual("Prop1", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);
        }

        [TestMethod]
        public void TwoPrimitivePropertiesSelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new { e.Prop1, e.Prop2 }).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(1, actual[0].Elements.Count);
            Assert.AreEqual("Prop1", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);

            Assert.AreEqual(1, actual[1].Elements.Count);
            Assert.AreEqual("Prop2", actual[1].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[0].Conditions);
        }

        [TestMethod]
        public void TwoPrimitivePropertiesMemberBindSelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new DynamicTestEntity() { Prop1 = e.Prop1, Prop2 = e.Prop2 }).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(1, actual[0].Elements.Count);
            Assert.AreEqual("Prop1", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);

            Assert.AreEqual(1, actual[1].Elements.Count);
            Assert.AreEqual("Prop2", actual[1].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[0].Conditions);
        }

        [TestMethod]
        public void OneObjectPropertySelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new { Parent = new { e.Parent.Prop1, e.Parent.Prop2 }}).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(2, actual[0].Elements.Count);
            Assert.AreEqual("Parent", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);
            Assert.AreEqual("Prop1", actual[0].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[1].Conditions);

            Assert.AreEqual(2, actual[1].Elements.Count);
            Assert.AreEqual("Parent", actual[1].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[0].Conditions);
            Assert.AreEqual("Prop2", actual[1].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[1].Conditions);
        }

        [TestMethod]
        public void OneObjectPropertyMixedMemberBindInsideSelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new { Parent = new DynamicTestEntity { Prop1 = e.Parent.Prop1, Prop2 = e.Parent.Prop2 } }).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(2, actual[0].Elements.Count);
            Assert.AreEqual("Parent", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);
            Assert.AreEqual("Prop1", actual[0].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[1].Conditions);

            Assert.AreEqual(2, actual[1].Elements.Count);
            Assert.AreEqual("Parent", actual[1].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[0].Conditions);
            Assert.AreEqual("Prop2", actual[1].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[1].Conditions);
        }

        [TestMethod]
        public void OneObjectPropertyMixedMemberBindOutsideSelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new DynamicTestEntity { Parent = new { Prop1 = e.Parent.Prop1, Prop2 = e.Parent.Prop2 } }).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(2, actual[0].Elements.Count);
            Assert.AreEqual("Parent", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);
            Assert.AreEqual("Prop1", actual[0].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[1].Conditions);

            Assert.AreEqual(2, actual[1].Elements.Count);
            Assert.AreEqual("Parent", actual[1].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[0].Conditions);
            Assert.AreEqual("Prop2", actual[1].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[1].Conditions);
        }

        [TestMethod]
        public void OneObjectPropertyMemberBindSelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new DynamicTestEntity { Parent = new DynamicTestEntity { Prop1 = e.Parent.Prop1, Prop2 = e.Parent.Prop2 } }).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(2, actual[0].Elements.Count);
            Assert.AreEqual("Parent", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);
            Assert.AreEqual("Prop1", actual[0].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[1].Conditions);

            Assert.AreEqual(2, actual[1].Elements.Count);
            Assert.AreEqual("Parent", actual[1].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[0].Conditions);
            Assert.AreEqual("Prop2", actual[1].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[1].Conditions);
        }

        [TestMethod]
        public void OneCollectionPropertySelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new { Children = e.Children.Select(cc => new { Prop1 = cc.Prop1, Prop2 = cc.Prop2 }) }).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(2, actual[0].Elements.Count);
            Assert.AreEqual("Children", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);
            Assert.AreEqual("Prop1", actual[0].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[1].Conditions);

            Assert.AreEqual(2, actual[1].Elements.Count);
            Assert.AreEqual("Children", actual[1].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[0].Conditions);
            Assert.AreEqual("Prop2", actual[1].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[1].Conditions);
        }

        [TestMethod]
        public void OneCollectionPropertyMixedMemberBindInsideSelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new { Children = e.Children.Select(cc => new DynamicTestEntity { Prop1 = cc.Prop1, Prop2 = cc.Prop2 }) }).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(2, actual[0].Elements.Count);
            Assert.AreEqual("Children", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);
            Assert.AreEqual("Prop1", actual[0].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[1].Conditions);

            Assert.AreEqual(2, actual[1].Elements.Count);
            Assert.AreEqual("Children", actual[1].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[0].Conditions);
            Assert.AreEqual("Prop2", actual[1].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[1].Conditions);
        }

        [TestMethod]
        public void OneCollectionPropertyMixedMemberBindOutsideSelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new DynamicTestEntity { Children = e.Children.Select(cc => new { Prop1 = cc.Prop1, Prop2 = cc.Prop2 }) }).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(2, actual[0].Elements.Count);
            Assert.AreEqual("Children", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);
            Assert.AreEqual("Prop1", actual[0].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[1].Conditions);

            Assert.AreEqual(2, actual[1].Elements.Count);
            Assert.AreEqual("Children", actual[1].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[0].Conditions);
            Assert.AreEqual("Prop2", actual[1].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[1].Conditions);
        }

        [TestMethod]
        public void OneCollectionPropertyMemberBindSelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new DynamicTestEntity { Children = e.Children.Select(cc => new DynamicTestEntity { Prop1 = cc.Prop1, Prop2 = cc.Prop2 } )}).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(2, actual[0].Elements.Count);
            Assert.AreEqual("Children", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);
            Assert.AreEqual("Prop1", actual[0].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[1].Conditions);

            Assert.AreEqual(2, actual[1].Elements.Count);
            Assert.AreEqual("Children", actual[1].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[0].Conditions);
            Assert.AreEqual("Prop2", actual[1].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[1].Conditions);
        }

        [TestMethod]
        public void OneCollectionPropertyWithConditionTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            Expression<Func<TestEntity, bool>> condition = c => c.Prop1 == 1;
            var actual = converter.Convert<TestEntity>(e => new { Children = e.Children.Where(c => c.Prop1 == 1).Select(cc => new { Prop1 = cc.Prop1, Prop2 = cc.Prop2 }) }).ToList();
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(2, actual[0].Elements.Count);
            Assert.AreEqual("Children", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNotNull(actual[0].Elements[0].Conditions);
            ExprAssert.AreEqual(condition, actual[0].Elements[0].Conditions.First().DbCondition);
            Assert.AreEqual("Prop1", actual[0].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[1].Conditions);

            Assert.AreEqual(2, actual[1].Elements.Count);
            Assert.AreEqual("Children", actual[1].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[0].Step.DeclaringType);
            Assert.IsNotNull(actual[1].Elements[0].Conditions);
            ExprAssert.AreEqual(condition, actual[1].Elements[0].Conditions.First().DbCondition);
            Assert.AreEqual("Prop2", actual[1].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[1].Conditions);
        }

        [TestMethod]
        public void PropertyTreeSelectTest()
        {
            var converter = new SelectToPathsConverter(StaticHelpers.NewExpressionHelper);
            Expression<Func<TestEntity, bool>> firstCondition = c => c.Prop2 == "1";
            Expression<Func<TestEntity, bool>> secondCondition = c => c.Prop1 == 1;
            var actual = converter.Convert<TestEntity>(e => new
            {
                e.Prop1,
                Parent = new {e.Parent.Prop2, Children = e.Parent.Children.Where(c => c.Prop2 == "1").Select(c => new {c.Prop1})},
                Children = e.Children.Where(c => c.Prop1 == 1).Select(cc => new { Prop1 = cc.Prop1, Prop2 = cc.Prop2, Parent = new {cc.Parent.Prop3} })
            }).ToList();
            Assert.AreEqual(6, actual.Count);

            Assert.AreEqual(1, actual[0].Elements.Count);
            Assert.AreEqual("Prop1", actual[0].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[0].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[0].Elements[0].Conditions);

            Assert.AreEqual(2, actual[1].Elements.Count);
            Assert.AreEqual("Parent", actual[1].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[0].Conditions);
            Assert.AreEqual("Prop2", actual[1].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[1].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[1].Elements[1].Conditions);

            Assert.AreEqual(3, actual[2].Elements.Count);
            Assert.AreEqual("Parent", actual[2].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[2].Elements[0].Step.DeclaringType);
            Assert.IsNull(actual[2].Elements[0].Conditions);
            Assert.AreEqual("Children", actual[2].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[2].Elements[1].Step.DeclaringType);
            Assert.IsNotNull(actual[2].Elements[1].Conditions);
            ExprAssert.AreEqual(firstCondition, actual[2].Elements[1].Conditions.First().DbCondition);
            Assert.AreEqual("Prop1", actual[2].Elements[2].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[2].Elements[2].Step.DeclaringType);
            Assert.IsNull(actual[2].Elements[2].Conditions);

            Assert.AreEqual(2, actual[3].Elements.Count);
            Assert.AreEqual("Children", actual[3].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[3].Elements[0].Step.DeclaringType);
            Assert.IsNotNull(actual[3].Elements[0].Conditions);
            ExprAssert.AreEqual(secondCondition, actual[3].Elements[0].Conditions.First().DbCondition);
            Assert.AreEqual("Prop1", actual[3].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[3].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[3].Elements[1].Conditions);

            Assert.AreEqual(2, actual[4].Elements.Count);
            Assert.AreEqual("Children", actual[4].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[4].Elements[0].Step.DeclaringType);
            Assert.IsNotNull(actual[4].Elements[0].Conditions);
            ExprAssert.AreEqual(secondCondition, actual[4].Elements[0].Conditions.First().DbCondition);
            Assert.AreEqual("Prop2", actual[4].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[4].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[4].Elements[1].Conditions);

            Assert.AreEqual(3, actual[5].Elements.Count);
            Assert.AreEqual("Children", actual[5].Elements[0].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[5].Elements[0].Step.DeclaringType);
            Assert.IsNotNull(actual[5].Elements[0].Conditions);
            ExprAssert.AreEqual(secondCondition, actual[5].Elements[0].Conditions.First().DbCondition);
            Assert.AreEqual("Parent", actual[5].Elements[1].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[5].Elements[1].Step.DeclaringType);
            Assert.IsNull(actual[5].Elements[1].Conditions);
            Assert.AreEqual("Prop3", actual[5].Elements[2].Step.Name);
            Assert.AreSame(typeof(TestEntity), actual[5].Elements[2].Step.DeclaringType);
            Assert.IsNull(actual[5].Elements[2].Conditions);
        }
    }
}
