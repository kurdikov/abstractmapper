﻿namespace TsSoft.Expressions.SelectBuilder.Tests.Helpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.SelectBuilder.Helpers;

    [TestClass]
    public class IncludesToSelectTreeConverterTests
    {
        private class Test1
        {
            public int Id { get; set; }
            public int? Column2 { get; set; }
            public Test1 Parent { get; set; }
            public ICollection<Test1> Children { get; set; }
            public Test2 Navigation { get; set; }
            public ICollection<Test2> NavigationMulti { get; set; }
        }

        private class Test2
        {
            public string Id { get; set; }
            public Test1 Test1 { get; set; }
            public ICollection<Test1> TestOnes { get; set; }
        }


        private ValueHoldingMember GetTest1Prop(string id)
        {
            return new ValueHoldingMember(typeof(Test1).GetTypeInfo().GetProperty(id));
        }

        private ValueHoldingMember GetTest2Prop(string id)
        {
            return new ValueHoldingMember(typeof(Test2).GetTypeInfo().GetProperty(id));
        }

        [TestMethod]
        public void TestConvertEmptyIncludes()
        {
            var converter = new IncludeToSelectTreeConverter(
                StaticHelpers.FlatPathParser, new EntityTypesHelper(new CustomEnvironmentTestEntityTypesRetriever(Entities.Are(typeof(Test1), typeof(Test2)), External.None)));
            var actual = converter.Convert(new Includes<Test1>());
            Assert.AreEqual(typeof(Test1), actual.Start.Type);
            Assert.AreEqual(2, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Id")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Column2")));
        }

        [TestMethod]
        public void TestConvertOneShortInclude()
        {
            var converter = new IncludeToSelectTreeConverter(
                StaticHelpers.FlatPathParser, new EntityTypesHelper(new CustomEnvironmentTestEntityTypesRetriever(Entities.Are(typeof(Test1), typeof(Test2)), External.None)));
            var actual = converter.Convert(new Includes<Test1>{z => z.Navigation});
            Assert.AreEqual(typeof(Test1), actual.Start.Type);
            Assert.AreEqual(3, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Id")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Column2")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Navigation")));
            var nav = actual.RootNode.NextLevel[GetTest1Prop("Navigation")];
            Assert.AreEqual(1, nav.NextLevel.Count);
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest2Prop("Id")));
        }


        [TestMethod]
        public void TestConvertOneLongInclude()
        {
            var converter = new IncludeToSelectTreeConverter(
                StaticHelpers.FlatPathParser, new EntityTypesHelper(new CustomEnvironmentTestEntityTypesRetriever(Entities.Are(typeof(Test1), typeof(Test2)), External.None)));
            var actual = converter.Convert(new Includes<Test1> { z => z.Navigation.TestOnes.Select(o => o.Children) });
            Assert.AreEqual(typeof(Test1), actual.Start.Type);
            Assert.AreEqual(3, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Id")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Column2")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Navigation")));
            var nav = actual.RootNode.NextLevel[GetTest1Prop("Navigation")];
            Assert.AreEqual(2, nav.NextLevel.Count);
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest2Prop("Id")));
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest2Prop("TestOnes")));
            nav = nav.NextLevel[GetTest2Prop("TestOnes")];
            Assert.AreEqual(3, nav.NextLevel.Count);
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest1Prop("Id")));
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest1Prop("Column2")));
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest1Prop("Children")));
            nav = nav.NextLevel[GetTest1Prop("Children")];
            Assert.AreEqual(2, nav.NextLevel.Count);
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest1Prop("Id")));
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest1Prop("Column2")));
        }

        [TestMethod]
        public void TestConvertTwoDivergingIncludes()
        {
            var converter = new IncludeToSelectTreeConverter(
                StaticHelpers.FlatPathParser, new EntityTypesHelper(new CustomEnvironmentTestEntityTypesRetriever(Entities.Are(typeof(Test1), typeof(Test2)), External.None)));
            var actual = converter.Convert(new Includes<Test1>
            {
                z => z.Navigation.TestOnes,
                z => z.NavigationMulti.Select(t => t.Test1),
            });
            Assert.AreEqual(typeof(Test1), actual.Start.Type);
            Assert.AreEqual(4, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Id")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Column2")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Navigation")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("NavigationMulti")));
            var nav = actual.RootNode.NextLevel[GetTest1Prop("Navigation")];
            Assert.AreEqual(2, nav.NextLevel.Count);
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest2Prop("Id")));
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest2Prop("TestOnes")));
            nav = nav.NextLevel[GetTest2Prop("TestOnes")];
            Assert.AreEqual(2, nav.NextLevel.Count);
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest1Prop("Id")));
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest1Prop("Column2")));
            nav = actual.RootNode.NextLevel[GetTest1Prop("NavigationMulti")];
            Assert.AreEqual(2, nav.NextLevel.Count);
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest2Prop("Id")));
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest2Prop("Test1")));
            nav = nav.NextLevel[GetTest2Prop("Test1")];
            Assert.AreEqual(2, nav.NextLevel.Count);
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest1Prop("Id")));
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest1Prop("Column2")));
        }

        [TestMethod]
        public void TestConvertTwoOverlappingIncludes()
        {
            var converter = new IncludeToSelectTreeConverter(
                StaticHelpers.FlatPathParser, new EntityTypesHelper(new CustomEnvironmentTestEntityTypesRetriever(Entities.Are(typeof(Test1), typeof(Test2)), External.None)));
            var actual = converter.Convert(new Includes<Test1>
            {
                z => z.Navigation.TestOnes,
                z => z.Navigation.Test1,
            });
            Assert.AreEqual(typeof(Test1), actual.Start.Type);
            Assert.AreEqual(3, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Id")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Column2")));
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(GetTest1Prop("Navigation")));
            var nav = actual.RootNode.NextLevel[GetTest1Prop("Navigation")];
            Assert.AreEqual(3, nav.NextLevel.Count);
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest2Prop("Id")));
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest2Prop("TestOnes")));
            Assert.IsTrue(nav.NextLevel.ContainsKey(GetTest2Prop("Test1")));
            var nav2 = nav.NextLevel[GetTest2Prop("TestOnes")];
            Assert.AreEqual(2, nav2.NextLevel.Count);
            Assert.IsTrue(nav2.NextLevel.ContainsKey(GetTest1Prop("Id")));
            Assert.IsTrue(nav2.NextLevel.ContainsKey(GetTest1Prop("Column2")));
            nav2 = nav.NextLevel[GetTest2Prop("Test1")];
            Assert.AreEqual(2, nav2.NextLevel.Count);
            Assert.IsTrue(nav2.NextLevel.ContainsKey(GetTest1Prop("Id")));
            Assert.IsTrue(nav2.NextLevel.ContainsKey(GetTest1Prop("Column2")));
        }
    }
}
