﻿namespace TsSoft.Expressions.SelectBuilder.Tests.Helpers
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.SelectBuilder.Helpers;

    [TestClass]
    public class SelectToSelectTreeConverterTests
    {
        private ValueHoldingMember Prop([NotNull] string propName)
        {
            return new ValueHoldingMember(typeof(TestEntity).GetProperty(propName));
        }

        [TestMethod]
        public void OnePrimitivePropertySelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new { e.Prop1 });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(actual.RootNode.NextLevel[Prop("Prop1")]);
        }

        [TestMethod]
        public void OnePrimitivePropertyMemberBindSelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new DynamicTestEntity() { Prop1 = e.Prop1 });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(actual.RootNode.NextLevel[Prop("Prop1")]);
        }

        [TestMethod]
        public void TwoPrimitivePropertiesSelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new { e.Prop1, e.Prop2 });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(2, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(actual.RootNode.NextLevel[Prop("Prop1")]);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(actual.RootNode.NextLevel[Prop("Prop2")]);
        }

        [TestMethod]
        public void TwoPrimitivePropertiesMemberBindSelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new DynamicTestEntity { Prop1 = e.Prop1, Prop2 = e.Prop2 });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(2, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(actual.RootNode.NextLevel[Prop("Prop1")]);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(actual.RootNode.NextLevel[Prop("Prop2")]);
        }

        [TestMethod]
        public void OneObjectPropertySelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new { Parent = new { e.Parent.Prop1, e.Parent.Prop2 } });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Parent")));
            var parent = actual.RootNode.NextLevel[Prop("Parent")];
            Assert.IsNotNull(parent);
            Assert.IsNull(parent.Condition);
            Assert.IsNull(parent.ConditionParameters.FirstOrDefault());
            Assert.IsNull(parent.SelectParameters.FirstOrDefault());
            Assert.IsNotNull(parent.NextLevel);
            Assert.AreEqual(2, parent.NextLevel.Count);
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(parent.NextLevel[Prop("Prop1")]);
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(parent.NextLevel[Prop("Prop2")]);
        }

        [TestMethod]
        public void OneObjectPropertyMixedMemberBindInsideSelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new { Parent = new DynamicTestEntity() { Prop1 = e.Parent.Prop1, Prop2 = e.Parent.Prop2 } });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Parent")));
            var parent = actual.RootNode.NextLevel[Prop("Parent")];
            Assert.IsNotNull(parent);
            Assert.IsNull(parent.Condition);
            Assert.IsNull(parent.ConditionParameters.FirstOrDefault());
            Assert.IsNull(parent.SelectParameters.FirstOrDefault());
            Assert.IsNotNull(parent.NextLevel);
            Assert.AreEqual(2, parent.NextLevel.Count);
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(parent.NextLevel[Prop("Prop1")]);
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(parent.NextLevel[Prop("Prop2")]);
        }

        [TestMethod]
        public void OneObjectPropertyMixedMemberBindOutsideSelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new DynamicTestEntity() { Parent = new { Prop1 = e.Parent.Prop1, Prop2 = e.Parent.Prop2 } });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Parent")));
            var parent = actual.RootNode.NextLevel[Prop("Parent")];
            Assert.IsNotNull(parent);
            Assert.IsNull(parent.Condition);
            Assert.IsNull(parent.ConditionParameters.FirstOrDefault());
            Assert.IsNull(parent.SelectParameters.FirstOrDefault());
            Assert.IsNotNull(parent.NextLevel);
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(parent.NextLevel[Prop("Prop1")]);
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(parent.NextLevel[Prop("Prop2")]);
        }
        
        [TestMethod]
        public void OneObjectPropertyMemberBindSelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new DynamicTestEntity() { Parent = new DynamicTestEntity() { Prop1 = e.Parent.Prop1, Prop2 = e.Parent.Prop2 } });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Parent")));
            var parent = actual.RootNode.NextLevel[Prop("Parent")];
            Assert.IsNotNull(parent);
            Assert.IsNull(parent.Condition);
            Assert.IsNull(parent.ConditionParameters.FirstOrDefault());
            Assert.IsNull(parent.SelectParameters.FirstOrDefault());
            Assert.IsNotNull(parent.NextLevel);
            Assert.AreEqual(2, parent.NextLevel.Count);
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(parent.NextLevel[Prop("Prop1")]);
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(parent.NextLevel[Prop("Prop2")]);
        }

        [TestMethod]
        public void OneCollectionPropertySelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new { Children = e.Children.Select(cc => new { Prop1 = cc.Prop1, Prop2 = cc.Prop2 }) });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Children")));
            var children = actual.RootNode.NextLevel[Prop("Children")];
            Assert.IsNotNull(children);
            Assert.IsNull(children.Condition);
            Assert.IsNull(children.ConditionParameters.FirstOrDefault());
            Assert.IsNotNull(children.SelectParameters.FirstOrDefault());
            Assert.AreEqual("cc", children.SelectParameters.First().Name);
            Assert.IsNotNull(children.NextLevel);
            Assert.AreEqual(2, children.NextLevel.Count);
            Assert.IsTrue(children.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(children.NextLevel[Prop("Prop1")]);
            Assert.IsTrue(children.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(children.NextLevel[Prop("Prop2")]);
        }

        [TestMethod]
        public void OneCollectionPropertyMixedMemberBindInsideSelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new { Children = e.Children.Select(cc => new DynamicTestEntity(){ Prop1 = cc.Prop1, Prop2 = cc.Prop2 }) });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Children")));
            var children = actual.RootNode.NextLevel[Prop("Children")];
            Assert.IsNotNull(children);
            Assert.IsNull(children.Condition);
            Assert.IsNull(children.ConditionParameters.FirstOrDefault());
            Assert.IsNotNull(children.SelectParameters.FirstOrDefault());
            Assert.AreEqual("cc", children.SelectParameters.First().Name);
            Assert.IsNotNull(children.NextLevel);
            Assert.AreEqual(2, children.NextLevel.Count);
            Assert.IsTrue(children.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(children.NextLevel[Prop("Prop1")]);
            Assert.IsTrue(children.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(children.NextLevel[Prop("Prop2")]);
        }

        [TestMethod]
        public void OneCollectionPropertyMixedMemberBindOutsideSelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new DynamicTestEntity { Children = e.Children.Select(cc => new  { Prop1 = cc.Prop1, Prop2 = cc.Prop2 }) });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Children")));
            var children = actual.RootNode.NextLevel[Prop("Children")];
            Assert.IsNotNull(children);
            Assert.IsNull(children.Condition);
            Assert.IsNull(children.ConditionParameters.FirstOrDefault());
            Assert.IsNotNull(children.SelectParameters.FirstOrDefault());
            Assert.AreEqual("cc", children.SelectParameters.First().Name);
            Assert.IsNotNull(children.NextLevel);
            Assert.AreEqual(2, children.NextLevel.Count);
            Assert.IsTrue(children.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(children.NextLevel[Prop("Prop1")]);
            Assert.IsTrue(children.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(children.NextLevel[Prop("Prop2")]);
        }

        [TestMethod]
        public void OneCollectionPropertyMemberBindSelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            var actual = converter.Convert<TestEntity>(e => new DynamicTestEntity { Children = e.Children.Select(cc => new DynamicTestEntity  { Prop1 = cc.Prop1, Prop2 = cc.Prop2 }) });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Children")));
            var children = actual.RootNode.NextLevel[Prop("Children")];
            Assert.IsNotNull(children);
            Assert.IsNull(children.Condition);
            Assert.IsNull(children.ConditionParameters.FirstOrDefault());
            Assert.IsNotNull(children.SelectParameters.FirstOrDefault());
            Assert.AreEqual("cc", children.SelectParameters.First().Name);
            Assert.IsNotNull(children.NextLevel);
            Assert.AreEqual(2, children.NextLevel.Count);
            Assert.IsTrue(children.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(children.NextLevel[Prop("Prop1")]);
            Assert.IsTrue(children.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(children.NextLevel[Prop("Prop2")]);
        }

        [TestMethod]
        public void OneCollectionPropertyWithConditionTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            Expression<Func<TestEntity, bool>> condition = c => c.Prop1 == 1;
            var actual = converter.Convert<TestEntity>(e => new { Children = e.Children.Where(c => c.Prop1 == 1).Select(cc => new { Prop1 = cc.Prop1, Prop2 = cc.Prop2 }) });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(1, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Children")));
            var children = actual.RootNode.NextLevel[Prop("Children")];
            Assert.IsNotNull(children);
            Assert.IsNotNull(children.Condition);
            ExprAssert.IsLambdaBody(condition, children.Condition);
            Assert.AreEqual("c", children.ConditionParameters.First().Name);
            Assert.IsNotNull(children.SelectParameters.FirstOrDefault());
            Assert.AreEqual("cc", children.SelectParameters.First().Name);
            Assert.IsNotNull(children.NextLevel);
            Assert.AreEqual(2, children.NextLevel.Count);
            Assert.IsTrue(children.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(children.NextLevel[Prop("Prop1")]);
            Assert.IsTrue(children.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(children.NextLevel[Prop("Prop2")]);
        }


        [TestMethod]
        public void PropertyTreeSelectTest()
        {
            var converter = new SelectToSelectTreeConverter(StaticHelpers.NewExpressionHelper);
            Expression<Func<TestEntity, bool>> firstCondition = c => c.Prop2 == "1";
            Expression<Func<TestEntity, bool>> secondCondition = c => c.Prop1 == 1;
            var actual = converter.Convert<TestEntity>(e => new
            {
                e.Prop1,
                Parent = new { e.Parent.Prop2, Children = e.Parent.Children.Where(c1 => c1.Prop2 == "1").Select(c3 => new { c3.Prop1 }) },
                Children = e.Children.Where(c2 => c2.Prop1 == 1).Select(cc => new { Prop1 = cc.Prop1, Prop2 = cc.Prop2, Parent = new { cc.Parent.Prop3 } })
            });
            Assert.AreEqual("e", actual.RootNode.SelectParameters.First().Name);
            Assert.IsNotNull(actual.RootNode.NextLevel);
            Assert.AreEqual(3, actual.RootNode.NextLevel.Count);
            Assert.IsTrue(actual.RootNode.NextLevel.ContainsKey(Prop("Children")));

            var parent = actual.RootNode.NextLevel[Prop("Parent")];
            Assert.IsNotNull(parent);
            Assert.IsNotNull(parent.NextLevel);
            Assert.IsNull(parent.SelectParameters.FirstOrDefault());
            Assert.IsNull(parent.Condition);
            Assert.IsNull(parent.ConditionParameters.FirstOrDefault());
            Assert.IsNotNull(parent.NextLevel);
            Assert.AreEqual(2, parent.NextLevel.Count);
            Assert.IsTrue(parent.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(parent.NextLevel[Prop("Prop2")]);

            var parentChildren = parent.NextLevel[Prop("Children")];
            Assert.IsNotNull(parentChildren);
            Assert.IsNotNull(parentChildren.NextLevel);
            Assert.IsNotNull(parentChildren.SelectParameters.FirstOrDefault());
            Assert.AreEqual("c3", parentChildren.SelectParameters.First().Name);
            Assert.IsNotNull(parentChildren.Condition);
            ExprAssert.IsLambdaBody(firstCondition, parentChildren.Condition);
            Assert.IsNotNull(parentChildren.ConditionParameters.FirstOrDefault());
            Assert.AreEqual("c1", parentChildren.ConditionParameters.First().Name);
            Assert.AreEqual(1, parentChildren.NextLevel.Count);
            Assert.IsTrue(parentChildren.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(parentChildren.NextLevel[Prop("Prop1")]);

            var children = actual.RootNode.NextLevel[Prop("Children")];
            Assert.IsNotNull(children);
            Assert.IsNotNull(children.Condition);
            ExprAssert.IsLambdaBody(secondCondition, children.Condition);
            Assert.AreEqual("c2", children.ConditionParameters.First().Name);
            Assert.IsNotNull(children.SelectParameters.FirstOrDefault());
            Assert.AreEqual("cc", children.SelectParameters.First().Name);
            Assert.IsNotNull(children.NextLevel);
            Assert.AreEqual(3, children.NextLevel.Count);
            Assert.IsTrue(children.NextLevel.ContainsKey(Prop("Prop1")));
            Assert.IsNull(children.NextLevel[Prop("Prop1")]);
            Assert.IsTrue(children.NextLevel.ContainsKey(Prop("Prop2")));
            Assert.IsNull(children.NextLevel[Prop("Prop2")]);

            var childrenParent = children.NextLevel[Prop("Parent")];
            Assert.IsNotNull(childrenParent);
            Assert.IsNull(childrenParent.SelectParameters.FirstOrDefault());
            Assert.IsNull(childrenParent.Condition);
            Assert.IsNull(childrenParent.ConditionParameters.FirstOrDefault());
            Assert.IsNotNull(childrenParent.NextLevel);
            Assert.AreEqual(1, childrenParent.NextLevel.Count);
            Assert.IsTrue(childrenParent.NextLevel.ContainsKey(Prop("Prop3")));
            Assert.IsNull(childrenParent.NextLevel[Prop("Prop3")]);
        }
    }
}
