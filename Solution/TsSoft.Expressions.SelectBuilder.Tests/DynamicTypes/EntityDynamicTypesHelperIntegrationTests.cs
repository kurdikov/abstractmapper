﻿namespace TsSoft.Expressions.SelectBuilder.Tests.DynamicTypes
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using System.Threading.Tasks;
    using BaseTestClasses;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.DynamicTypes;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.SelectBuilder.DynamicTypes;

    [TestClass]
    public class EntityDynamicTypesHelperIntegrationTests
    {
        [TestMethod]
        public void GetDynamicTypeTest()
        {
            var retriever = new Mock<IEntityTypesRetriever>(MockBehavior.Strict);

            var realEntityType = typeof(TestEntity);

            retriever.Setup(r => r.GetEntityTypes()).Returns(new[] { realEntityType });
            retriever.Setup(r => r.IsEntityType(It.IsAny<Type>())).Returns((Type t) => t == realEntityType);
            var helper = new EntityDynamicTypesHelper(
                retriever.Object,
                SelectStaticHelpers.TypeBuilderCreator,
                SelectStaticHelpers.TypeClonemaker,
                new EntityTypesHelper(retriever.Object),
                SelectStaticHelpers.PropertyCreator,
                StaticHelpers.MemberNamingHelper);

            var type = helper.GetDynamicType(realEntityType, 0);
            foreach (var property in realEntityType.GetProperties())
            {
                var dynamicProp = type.GetProperty(property.Name);
                if (property.GetCustomAttribute<NotMappedAttribute>() != null)
                {
                    Assert.IsNotNull(dynamicProp);
                    Assert.AreEqual(property.PropertyType, dynamicProp.PropertyType);
                }
            }
        }

        [TestMethod]
        public void GetDynamicTypeByInterfaceTest()
        {
            var retriever = new Mock<IEntityTypesRetriever>(MockBehavior.Strict);

            var interfaceType = typeof(IFullTestEntity);

            retriever.Setup(r => r.GetEntityTypes()).Returns(new[] { interfaceType });
            retriever.Setup(r => r.IsEntityType(It.IsAny<Type>())).Returns((Type t) => t == interfaceType);
            var helper = new EntityDynamicTypesHelper(
                retriever.Object,
                SelectStaticHelpers.TypeBuilderCreator,
                SelectStaticHelpers.TypeClonemaker,
                new EntityTypesHelper(retriever.Object),
                SelectStaticHelpers.PropertyCreator,
                StaticHelpers.MemberNamingHelper);

            var type = helper.GetDynamicType(interfaceType, 0);
            var interfaces = type.GetInterfaces();
            Assert.AreEqual(1, interfaces.Length);
            var dynamicInt = interfaces[0];
            foreach (var property in interfaceType.GetProperties())
            {
                var dynamicProp = type.GetProperty(property.Name);
                var interProp = dynamicInt.GetProperty(property.Name);
                if (property.GetCustomAttribute<NotMappedAttribute>() != null)
                {
                    Assert.IsNull(dynamicProp, property.Name);
                    Assert.IsNull(interProp, property.Name);
                }
                else
                {
                    Assert.IsNotNull(dynamicProp);
                    Assert.IsNotNull(interProp);
                    if (!property.PropertyType.IsGenericEnumerable())
                    {
                        if (property.PropertyType != interfaceType)
                        {
                            Assert.AreEqual(property.PropertyType, dynamicProp.PropertyType, property.Name);
                            Assert.AreEqual(property.PropertyType, interProp.PropertyType, property.Name);
                        }
                        else
                        {
                            Assert.AreEqual(dynamicInt, dynamicProp.PropertyType, property.Name);
                            Assert.AreEqual(dynamicInt, interProp.PropertyType, property.Name);
                        }
                    }
                    else
                    {
                        var elem = property.PropertyType.GetGenericEnumerableArgument();
                        if (elem != interfaceType)
                        {
                            Assert.AreEqual(typeof(IEnumerable<>).MakeGenericType(elem), dynamicProp.PropertyType, property.Name);
                            Assert.AreEqual(typeof(IEnumerable<>).MakeGenericType(elem), interProp.PropertyType, property.Name);
                        }
                        else
                        {
                            Assert.AreEqual(typeof(IEnumerable<>).MakeGenericType(dynamicInt), dynamicProp.PropertyType, property.Name);
                            Assert.AreEqual(typeof(IEnumerable<>).MakeGenericType(dynamicInt), interProp.PropertyType, property.Name);
                        }
                    }
                }
            }
        }

        [TestMethod]
        public void ConcurrentGetDynamicTypeCallsDoNotCreateMultipleTypesTest()
        {
            var retriever = new Mock<IEntityTypesRetriever>(MockBehavior.Strict);
            var mockTypeCreator = new Mock<ITypeBuilderCreator>(MockBehavior.Strict);

            var tda = new TestDynamicAssembly();


            var realEntityType = typeof(TestEntity);
            mockTypeCreator.Setup(mt => mt.CreateInterface(It.IsAny<string>()))
                .Returns(tda.CreateInterface);
            mockTypeCreator.Setup(mt => mt.CreateWithDefaultConstructor(It.IsAny<string>(), realEntityType))
                .Returns((string n, Type t) => tda.Create(t));

            retriever.Setup(r => r.GetEntityTypes()).Returns(new[] { realEntityType });
            retriever.Setup(r => r.IsEntityType(It.IsAny<Type>())).Returns((Type t) => t == realEntityType);
            var helper = new EntityDynamicTypesHelper(
                retriever.Object,
                mockTypeCreator.Object,
                SelectStaticHelpers.TypeClonemaker,
                new EntityTypesHelper(retriever.Object),
                SelectStaticHelpers.PropertyCreator,
                StaticHelpers.MemberNamingHelper);

            var tasks = Enumerable.Range(1, 10).Select(i => Task.Run(() => helper.GetDynamicType(realEntityType, 0))).ToArray();
            Task.WaitAll(tasks);
            var result = tasks[0].Result;
            Assert.IsNotNull(result);

            var types = result.GetTypeInfo().Assembly.GetTypes();
            Assert.AreEqual(1, types.Count(t => t.GetTypeInfo().BaseType == realEntityType));
        }

        [TestMethod]
        public void GetDerivedTypeForTypeWithVirtualPropertyWithoutSetter()
        {
            var retriever = new Mock<IEntityTypesRetriever>(MockBehavior.Strict);

            var realEntityType = typeof(ClassWithPropertiesWithoutSetter);

            retriever.Setup(r => r.GetEntityTypes()).Returns(new[] { realEntityType });
            retriever.Setup(r => r.IsEntityType(It.IsAny<Type>())).Returns((Type t) => t == realEntityType);
            var helper = new EntityDynamicTypesHelper(
                retriever.Object,
                SelectStaticHelpers.TypeBuilderCreator,
                SelectStaticHelpers.TypeClonemaker,
                new EntityTypesHelper(retriever.Object),
                SelectStaticHelpers.PropertyCreator,
                StaticHelpers.MemberNamingHelper);

            var type = helper.GetDynamicType(realEntityType, 0);
            var props = type.GetProperties().OrderBy(p => p.Name).ToList();
            var shadowProp = StaticHelpers.MemberNamingHelper.WriteableShadow("Prop2");
            Assert.AreEqual(6, props.Count);
            Assert.AreEqual("Prop1", props[0].Name);
            Assert.AreEqual(typeof(byte), props[0].PropertyType);
            Assert.AreEqual("Prop2", props[1].Name);
            Assert.AreEqual(typeof(short), props[1].PropertyType);
            Assert.AreEqual(shadowProp, props[2].Name);
            Assert.AreEqual(typeof(short), props[2].PropertyType);
            Assert.AreEqual("Prop3", props[3].Name);
            Assert.AreEqual(typeof(int), props[3].PropertyType);
            Assert.AreEqual("Prop4", props[4].Name);
            Assert.AreEqual(typeof(long), props[4].PropertyType);
            Assert.AreEqual("Prop5", props[5].Name);
            Assert.AreEqual(typeof(decimal), props[5].PropertyType);

            var instance = Activator.CreateInstance(type);
            var typedInstance = (ClassWithPropertiesWithoutSetter)instance;
            props[2].SetValue(instance, (short)123);
            Assert.AreEqual((short)123, typedInstance.Prop2);
        }

        [TestMethod]
        public void GetDerivedTypeForTypeDerivedFromTypeWithVirtualPropertyWithoutSetter()
        {
            var retriever = new Mock<IEntityTypesRetriever>(MockBehavior.Strict);

            var realEntityType = typeof(DerivedClassWithPropertiesWithoutSetter);

            retriever.Setup(r => r.GetEntityTypes()).Returns(new[] { realEntityType });
            retriever.Setup(r => r.IsEntityType(It.IsAny<Type>())).Returns((Type t) => t == realEntityType);
            var helper = new EntityDynamicTypesHelper(
                retriever.Object,
                SelectStaticHelpers.TypeBuilderCreator,
                SelectStaticHelpers.TypeClonemaker,
                new EntityTypesHelper(retriever.Object),
                SelectStaticHelpers.PropertyCreator,
                StaticHelpers.MemberNamingHelper);

            var type = helper.GetDynamicType(realEntityType, 0);
            var props = type.GetProperties().OrderBy(p => p.Name).ToList();
            var shadowProp = StaticHelpers.MemberNamingHelper.WriteableShadow("Prop2");
            Assert.AreEqual(6, props.Count);
            Assert.AreEqual("Prop1", props[0].Name);
            Assert.AreEqual(typeof(byte), props[0].PropertyType);
            Assert.AreEqual("Prop2", props[1].Name);
            Assert.AreEqual(typeof(short), props[1].PropertyType);
            Assert.AreEqual(shadowProp, props[2].Name);
            Assert.AreEqual(typeof(short), props[2].PropertyType);
            Assert.AreEqual("Prop3", props[3].Name);
            Assert.AreEqual(typeof(int), props[3].PropertyType);
            Assert.AreEqual("Prop4", props[4].Name);
            Assert.AreEqual(typeof(long), props[4].PropertyType);
            Assert.AreEqual("Prop5", props[5].Name);
            Assert.AreEqual(typeof(decimal), props[5].PropertyType);

            var instance = Activator.CreateInstance(type);
            var typedInstance = (ClassWithPropertiesWithoutSetter)instance;
            props[2].SetValue(instance, (short)123);
            Assert.AreEqual((short)123, typedInstance.Prop2);
        }

        private class TestDynamicAssembly : AssemblyBuilderContainer
        {
            public TestDynamicAssembly() : base("TestDynamic")
            {
            }

            public TypeBuilder Create(Type parent)
            {
                return DefineTypeWithGuidSuffix("", TypeAttributes.Public |
                TypeAttributes.Class |
                TypeAttributes.AutoClass |
                TypeAttributes.AnsiClass |
                TypeAttributes.BeforeFieldInit |
                TypeAttributes.AutoLayout, parent);
            }

            public TypeBuilder CreateInterface()
            {
                return DefineTypeWithGuidSuffix(
                    "", TypeAttributes.Public |
                        TypeAttributes.Interface |
                        TypeAttributes.Abstract);
            }
        }
    }

    public class ClassWithPropertiesWithoutSetter
    {
        [Key]
        public byte Prop1 { get; set; }

        public virtual short Prop2 { get; private set; }

        public virtual int Prop3 { private get; set; }

        public long Prop4 { get; private set; }

        [NotMapped]
        public virtual decimal Prop5 { get; private set; }
    }

    public class DerivedClassWithPropertiesWithoutSetter : ClassWithPropertiesWithoutSetter
    {
    }
}
