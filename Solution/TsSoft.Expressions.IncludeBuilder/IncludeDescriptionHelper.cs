﻿namespace TsSoft.Expressions.IncludeBuilder
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Collections;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Хелпер для разбора инклюдов
    /// </summary>
    public class IncludeDescriptionHelper : IIncludeDescriptionHelper
    {
        [NotNull]private readonly IFlatPathParser _flatPathParser;
        [NotNull]private readonly ICollectionHelper _collectionHelper;
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;
        [NotNull]private readonly IEntityTypesHelper _entityTypesHelper;

        [NotNull]private readonly ConcurrentDictionary<IReadOnlyList<ValueHoldingMember>, Delegate> _applyConditionPathDelegateCache =
            new ConcurrentDictionary<IReadOnlyList<ValueHoldingMember>, Delegate>(FlatPathComparer.Instance);

        [NotNull]private readonly ConcurrentDictionary<IReadOnlyList<ValueHoldingMember>, Delegate> _applyPathDelegateCache =
            new ConcurrentDictionary<IReadOnlyList<ValueHoldingMember>, Delegate>(FlatPathComparer.Instance);

        /// <summary>
        /// Хелпер для разбора инклюдов
        /// </summary>
        public IncludeDescriptionHelper(
            [NotNull]IFlatPathParser flatPathParser,
            [NotNull]ICollectionHelper collectionHelper,
            [NotNull]IExpressionBuilder expressionBuilder,
            [NotNull]IEntityTypesHelper entityTypesHelper)
        {
            if (flatPathParser == null) throw new ArgumentNullException("flatPathParser");
            if (collectionHelper == null) throw new ArgumentNullException("collectionHelper");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            _flatPathParser = flatPathParser;
            _collectionHelper = collectionHelper;
            _expressionBuilder = expressionBuilder;
            _entityTypesHelper = entityTypesHelper;
        }

        [NotNull]
        private ValueHoldingMember GetIdSelector([NotNull]ValueHoldingMember externalEntity)
        {
            var result = _entityTypesHelper.TryGetExternalKey(externalEntity);
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("External key property not found on entity {0} for external property {1}", externalEntity.DeclaringType, externalEntity.Name));
            }
            return result;
        }

        /// <summary>
        /// Получить описание применения внешнего инклюда
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="include">Инклюд</param>
        /// <param name="entities">Извлечённые из БД сущности</param>
        public IncludeApplicationDescription IncludeToApplicationDescription<T>(
            IncludeDescription<T> include,
            IEnumerable<T> entities)
            where T: class
        {
            if (include.ExternalEntityContainersRetriever == null || include.ExternalProperty == null)
            {
                throw new ArgumentException(string.Format("Include {0} is not an external include", include.FullInclude));
            }
            return new IncludeApplicationDescription
            {
                DbEntities = (include.ExternalEntityContainersRetriever(entities) ?? Enumerable.Empty<object>()).Where(c => c != null).ToList(),
                ContainedEntity = include.ExternalProperty,
                IdSelector = GetIdSelector(include.ExternalProperty),
            };
        }

        /// <summary>
        /// Разбить инклюд на непосредственную и отложенную части (например, внешние или условные инклюды)
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="include">Инклюд</param>
        /// <param name="conditions">Условия</param>
        public IncludeDescription<T> IncludeToDescription<T>(
            Expression<Func<T, object>> include,
            ICollection<IIncludeCondition<T>> conditions = null)
        {
            conditions = conditions ?? new IIncludeCondition<T>[0];
            var path = _flatPathParser.Parse(include);
            var firstExternalProperty = path.FirstOrDefault(_entityTypesHelper.IsExternalProperty);

            var condition = conditions.FirstOrDefault(c => c != null && _collectionHelper.IsPrefix(path, c.PathProperties));
            var result = new IncludeDescription<T>
            {
                FullInclude = include,
            };

            if (firstExternalProperty == null && condition == null)
            {
                result.DbInclude = include;
                result.DbIncludeProperties = path;
            }
            else
            {
                if (firstExternalProperty != null)
                {
                    var dbPath = path.TakeWhile(p => !ReferenceEquals(p, firstExternalProperty)).ToList();
                    result.ExternalType = firstExternalProperty.ValueType;
                    if (condition == null)
                    {
                        result.DeclaringType = firstExternalProperty.DeclaringType;
                        result.DbInclude = _expressionBuilder.BuildIncludeByPath<T>(dbPath);
                        result.DbIncludeProperties = dbPath;
                        result.ExternalEntityContainersRetriever = _applyPathDelegateCache.GetOrAdd(
                            dbPath, MakeApplyFlatPathDelegate<T>) as Func<IEnumerable<T>, IEnumerable<object>>;
                        result.ExternalProperty = firstExternalProperty;
                    }
                }
                if (condition != null)
                {
                    var dbPath = path.Take(condition.PathProperties.Count - 1).ToList();
                    result.DbInclude = _expressionBuilder.BuildIncludeByPath<T>(dbPath);
                    result.DbIncludeProperties = dbPath;
                    result.ExternalType = null;
                    result.DeclaringType = LastProperty(condition.PathProperties).DeclaringType;
                    result.IncludeSuffix = _expressionBuilder.BuildIncludeByPath(
                        condition.IncludedEntityType,
                        path.Skip(condition.PathProperties.Count));
                    result.Condition = condition;
                }
            }


            return result;
        }
        
        private Func<IEnumerable<T>, IEnumerable<object>> MakeApplyFlatPathDelegate<T>(
            [NotNull]IEnumerable<ValueHoldingMember> path)
        {
            var param = Expression.Parameter(typeof(IEnumerable<T>));
            var expr = _expressionBuilder.BuildPathWithNullChecks(param, path, false);
            var lambda = Expression.Lambda<Func<IEnumerable<T>, IEnumerable<object>>>(expr, param);
            return lambda.Compile();
        }

        /// <summary>
        /// Получить описание применения условного инклюда
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="entities">Извлечённые из БД сущности</param>
        /// <param name="condition">Условный инклюд</param>
        public ConditionalIncludeApplicationDescription IncludeToConditionalDescription<T>(
            IEnumerable<T> entities, 
            IIncludeCondition<T> condition)
            where T : class
        {
            var lastProperty = LastProperty(condition.PathProperties);
            if (lastProperty.DeclaringType == null)
            {
                throw new InvalidOperationException(string.Format("{0} has no declaring type", lastProperty));
            }
            var applyFlatPathDelegate = _applyConditionPathDelegateCache.GetOrAdd(
                condition.PathProperties,
                p => MakeApplyFlatPathDelegate<T>(condition.PathProperties.Take(condition.PathProperties.Count - 1)))
                as Func<IEnumerable<T>, IEnumerable<object>>;
            if (applyFlatPathDelegate == null)
            {
                throw new InvalidOperationException("Delegate cache contains null");
            }
            return new ConditionalIncludeApplicationDescription(
                collection: lastProperty,
                containers: applyFlatPathDelegate(entities) ?? Enumerable.Empty<object>(),
                containerPk: _entityTypesHelper.GetPrimaryKey(lastProperty.DeclaringType),
                containedFk: condition.ForeignKey
            );
        }

        [NotNull]
        private ValueHoldingMember LastProperty([NotNull]IEnumerable<ValueHoldingMember> properties)
        {
            var lastProperty = properties.Last();
            if (lastProperty == null)
            {
                throw new NullReferenceException("properties.Last()");
            }
            return lastProperty;
        }

        private class FlatPathComparer : IEqualityComparer<IReadOnlyList<ValueHoldingMember>>
        {
            public static readonly FlatPathComparer Instance = new FlatPathComparer();

            public bool Equals(IReadOnlyList<ValueHoldingMember> x, IReadOnlyList<ValueHoldingMember> y)
            {
                return x != null && y != null && x.SequenceEqual(y, MemberInfoComparer.Instance)
                       || x == null && y == null;
            }

            public int GetHashCode(IReadOnlyList<ValueHoldingMember> obj)
            {
                if (obj == null)
                {
                    return 0;
                }
                unchecked
                {
                    int result = 0;
                    foreach (var step in obj)
                    {
                        result = result * 397 ^ MemberInfoComparer.Instance.GetHashCode(step);
                    }
                    return result;
                }
            }
        }
    }
}
