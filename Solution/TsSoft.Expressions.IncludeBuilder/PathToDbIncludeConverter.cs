﻿namespace TsSoft.Expressions.IncludeBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Collections;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    internal class PathToDbIncludeConverter : IPathToDbIncludeConverter
    {
        [NotNull]private readonly IFlatPathParser _pathParser;
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;
        [NotNull]private readonly IEntityTypesRetriever _entityTypesRetriever;
        [NotNull]private readonly ICollectionHelper _collectionHelper;

        public PathToDbIncludeConverter(
            [NotNull]IFlatPathParser pathParser,
            [NotNull]IExpressionBuilder expressionBuilder,
            [NotNull]IEntityTypesRetriever entityTypesRetriever,
            [NotNull]ICollectionHelper collectionHelper)
        {
            if (pathParser == null) throw new ArgumentNullException("pathParser");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (entityTypesRetriever == null) throw new ArgumentNullException("entityTypesRetriever");
            if (collectionHelper == null) throw new ArgumentNullException("collectionHelper");
            _pathParser = pathParser;
            _expressionBuilder = expressionBuilder;
            _entityTypesRetriever = entityTypesRetriever;
            _collectionHelper = collectionHelper;
        }

        [NotNull]
        private IEnumerable<ValueHoldingMember> OnlyDbInclude([NotNull]IEnumerable<ValueHoldingMember> path)
        {
            return path.TakeWhile(
                pi =>
                _entityTypesRetriever.IsEntityType(pi.ValueType) ||
                _entityTypesRetriever.IsEntityCollectionType(pi.ValueType));
        }

        public Expression<Func<T, object>> ToDbInclude<T>(Expression<Func<T, object>> from)
        {
            if (from == null)
            {
                return null;
            }
            var properties = _pathParser.Parse(from);
            if (!properties.Any())
            {
                return null;
            }
            var includeProperties = OnlyDbInclude(properties);
            var result = _expressionBuilder.BuildIncludeByPath<T>(includeProperties);
            return result;
        }

        public ICollection<Expression<Func<T, object>>> GetIncludes<T>(
            IEnumerable<Expression<Func<T, object>>> paths, 
            IEnumerable<Expression<Func<T, object>>> ignoreStarts)
        {
            var ignoreStartPaths = ignoreStarts.Select(_pathParser.Parse).ToList();
            var result = new HashSet<Expression<Func<T, object>>>(IncludeEqualityComparer<T>.Instance);
            foreach (var path in paths)
            {
                var properties = OnlyDbInclude(_pathParser.Parse(path)).ToList();
                if (properties.Any() && 
                    ignoreStartPaths.All(p => !_collectionHelper.IsPrefix(properties, p, MemberInfoComparer.Instance)))
                {
                    var newInclude = _expressionBuilder.BuildIncludeByPath<T>(properties);
                    if (newInclude != null)
                    {
                        result.Add(newInclude);
                    }
                }
            }
            return result;
        }
    }
}
