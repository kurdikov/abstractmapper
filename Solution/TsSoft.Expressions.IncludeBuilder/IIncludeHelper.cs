﻿namespace TsSoft.Expressions.IncludeBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.IncludeBuilder.Models;

    /// <summary>
    /// Хелпер для построения инклюдов
    /// </summary>
    public interface IIncludeHelper
    {
        /// <summary>
        /// Добавить инклюды инжектируемого обработчика вложенной сущности (например, маппера) в текущий обработчик
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности текущего маппера</typeparam>
        /// <typeparam name="TInjectedProcessorEntity">Тип сущности инжектируемого обработчика</typeparam>
        /// <param name="includes">Коллекция инклюдов текущего обработчика</param>
        /// <param name="processor">Инжектируемый обработчик сущности (например, маппер)</param>
        /// <param name="usedProperties">Обрабатываемые инжектируемым обработчиком свойства (по умолчанию - все свойства сущности подходящего типа)</param>
        /// <param name="usedCollections">Обрабатываемые инжектируемым обработчиком свойства-коллекции (по умолчанию - все свойства сущности подходящего типа)</param>
        /// <param name="usedCollectionCollections">Обрабатываемые инжектируемым обработчиком коллекции, вложенные в коллекции</param>
        void AddIncludes<TEntity, TInjectedProcessorEntity>(
            [NotNull]ICollection<Expression<Func<TEntity, object>>> includes,
            [NotNull]IIncludeProvider<TInjectedProcessorEntity> processor,
            IEnumerable<Expression<Func<TEntity, TInjectedProcessorEntity>>> usedProperties,
            IEnumerable<Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>>> usedCollections,
            IEnumerable<Expression<Func<TEntity, IEnumerable<IEnumerable<TInjectedProcessorEntity>>>>> usedCollectionCollections);

        /// <summary>
        /// Добавить инклюды инжектируемого обработчика вложенной сущности (например, маппера) в текущий обработчик
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности текущего маппера</typeparam>
        /// <typeparam name="TInjectedProcessorEntity">Тип сущности инжектируемого обработчика</typeparam>
        /// <param name="includes">Коллекция инклюдов текущего обработчика</param>
        /// <param name="processorIncludes">Инклюды инжектируемого обработчика сущности (например, маппера)</param>
        /// <param name="usedProperties">Обрабатываемые инжектируемым обработчиком свойства (по умолчанию - все свойства сущности подходящего типа)</param>
        /// <param name="usedCollections">Обрабатываемые инжектируемым обработчиком свойства-коллекции (по умолчанию - все свойства сущности подходящего типа)</param>
        /// <param name="usedCollectionCollections">Обрабатываемые инжектируемым обработчиком коллекции, вложенные в коллекции</param>
        void AddIncludes<TEntity, TInjectedProcessorEntity>(
            [NotNull]ICollection<Expression<Func<TEntity, object>>> includes,
            IEnumerable<Expression<Func<TInjectedProcessorEntity, object>>> processorIncludes,
            IEnumerable<Expression<Func<TEntity, TInjectedProcessorEntity>>> usedProperties,
            IEnumerable<Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>>> usedCollections,
            IEnumerable<Expression<Func<TEntity, IEnumerable<IEnumerable<TInjectedProcessorEntity>>>>> usedCollectionCollections);

        /// <summary>
        /// Добавить инклюды инжектируемого обработчика вложенной сущности (например, маппера) в текущий обработчик
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности текущего маппера</typeparam>
        /// <typeparam name="TInjectedProcessorEntity">Тип сущности инжектируемого обработчика</typeparam>
        /// <param name="includes">Коллекция инклюдов текущего обработчика</param>
        /// <param name="processor">Инжектируемый обработчик сущности (например, маппер)</param>
        /// <param name="usedProperty">Обрабатываемое инжектируемым обработчиком свойство</param>
        void AddIncludes<TEntity, TInjectedProcessorEntity>(
            [NotNull]ICollection<Expression<Func<TEntity, object>>> includes,
            [NotNull]IIncludeProvider<TInjectedProcessorEntity> processor,
            Expression<Func<TEntity, TInjectedProcessorEntity>> usedProperty);

        /// <summary>
        /// Добавить инклюды инжектируемого обработчика вложенной сущности (например, маппера) в текущий обработчик
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности текущего маппера</typeparam>
        /// <typeparam name="TInjectedProcessorEntity">Тип сущности инжектируемого обработчика</typeparam>
        /// <param name="includes">Коллекция инклюдов текущего обработчика</param>
        /// <param name="processor">Инжектируемый обработчик сущности (например, маппер)</param>
        /// <param name="usedCollection">Обрабатываемая инжектируемым обработчиком коллекция</param>
        void AddIncludes<TEntity, TInjectedProcessorEntity>(
            [NotNull]ICollection<Expression<Func<TEntity, object>>> includes,
            [NotNull]IIncludeProvider<TInjectedProcessorEntity> processor,
            Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>> usedCollection);

        /// <summary>
        /// Добавить инклюды инжектируемого обработчика сущности в текущий обработчик той же сущности
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности текущего обработчика</typeparam>
        /// <typeparam name="TEntityInterface">Интерфейс, принимаемый инжектируемым обработчиком</typeparam>
        /// <param name="includes">Коллекция инклюдов текущего обработчика</param>
        /// <param name="processor">Инжектируемый обработчик</param>
        void AddIncludesForSelfProcessor<TEntity, TEntityInterface>(
            [NotNull]ICollection<Expression<Func<TEntity, object>>> includes,
            [NotNull]IIncludeProvider<TEntityInterface> processor 
            ) where TEntity : TEntityInterface;

        /// <summary>
        /// Добавить инклюды инжектируемого обработчика вложенной сущности (например, маппера) в текущий обработчик
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности текущего маппера</typeparam>
        /// <typeparam name="TInjectedProcessorEntity">Тип сущности инжектируемого обработчика</typeparam>
        /// <param name="includes">Коллекция инклюдов текущего обработчика</param>
        /// <param name="processor">Инжектируемый обработчик сущности (например, маппер)</param>
        /// <param name="usedCollection">Обрабатываемая инжектируемым обработчиком коллекция</param>
        void AddIncludes<TEntity, TInjectedProcessorEntity>(
            [NotNull]ICollection<Expression<Func<TEntity, object>>> includes,
            [NotNull]IIncludeProvider<TInjectedProcessorEntity> processor,
            Expression<Func<TEntity, IEnumerable<IEnumerable<TInjectedProcessorEntity>>>> usedCollection);

        /// <summary>
        /// Добавить условия из инжектируемого обработчика сущности (условия на один инклюд соединяются с помощью OrElse)
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности текущего обработчика</typeparam>
        /// <typeparam name="TInjectedProcessorEntity">Тип сущности инжектируемого обработчика</typeparam>
        /// <param name="includes">Коллекция инклюдов текущего обработчика</param>
        /// <param name="processor">Инжектируемый обработчик</param>
        /// <param name="usedProperties">Обрабатываемые инжектируемым обработчиком свойства</param>
        /// <param name="usedCollections">Обрабатываемые инжектируемым обработчиком коллекции</param>
        void LoadConditionalIncludes<TEntity, TInjectedProcessorEntity>(
            [NotNull]Includes<TEntity> includes,
            [NotNull]IIncludeProvider<TInjectedProcessorEntity> processor,
            IEnumerable<Expression<Func<TEntity, TInjectedProcessorEntity>>> usedProperties,
            IEnumerable<Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>>> usedCollections);

        /// <summary>
        /// Добавить условия из инжектируемого обработчика сущности (условия на один инклюд соединяются с помощью OrElse)
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности текущего обработчика</typeparam>
        /// <typeparam name="TInjectedProcessorEntity">Тип сущности инжектируемого обработчика</typeparam>
        /// <param name="includes">Коллекция инклюдов текущего обработчика</param>
        /// <param name="processor">Инжектируемый обработчик</param>
        /// <param name="usedProperty">Обрабатываемое инжектируемым обработчиком свойство</param>
        void LoadConditionalIncludes<TEntity, TInjectedProcessorEntity>(
            [NotNull]Includes<TEntity> includes,
            [NotNull]IIncludeProvider<TInjectedProcessorEntity> processor,
            Expression<Func<TEntity, TInjectedProcessorEntity>> usedProperty);

        /// <summary>
        /// Добавить условия из инжектируемого обработчика сущности (условия на один инклюд соединяются с помощью OrElse)
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности текущего обработчика</typeparam>
        /// <typeparam name="TInjectedProcessorEntity">Тип сущности инжектируемого обработчика</typeparam>
        /// <param name="includes">Коллекция инклюдов текущего обработчика</param>
        /// <param name="processor">Инжектируемый обработчик</param>
        /// <param name="usedCollection">Обрабатываемая инжектируемым обработчиком коллекция</param>
        void LoadConditionalIncludes<TEntity, TInjectedProcessorEntity>(
            [NotNull]Includes<TEntity> includes,
            [NotNull]IIncludeProvider<TInjectedProcessorEntity> processor,
            Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>> usedCollection);

        /// <summary>
        /// Добавить условия на инклюды
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности текущего обработчика</typeparam>
        /// <typeparam name="TInjectedProcessorEntity">Тип сущности в добавляемых условиях</typeparam>
        /// <param name="conditions">Условия текущего обработчика</param>
        /// <param name="addedConditions">Добавляемые условия</param>
        /// <param name="usedProperties"></param>
        /// <param name="usedCollections"></param>
        void LoadConditionalIncludes<TEntity, TInjectedProcessorEntity>(
            [NotNull]ICollection<IIncludeCondition<TEntity>> conditions,
            [NotNull]IEnumerable<IIncludeCondition<TInjectedProcessorEntity>> addedConditions,
            IEnumerable<Expression<Func<TEntity, TInjectedProcessorEntity>>> usedProperties,
            IEnumerable<Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>>> usedCollections);
    }
}
