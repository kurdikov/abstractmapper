﻿namespace TsSoft.Expressions.IncludeBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Преобразует пути в инклюды
    /// </summary>
    public interface IPathToDbIncludeConverter
    {
        /// <summary>
        /// Преобразовать путь в чистый БД-инклюд
        /// </summary>
        /// <param name="path">Путь</param>
        [ContractAnnotation("notnull => notnull")]
        Expression<Func<T, object>> ToDbInclude<T>(Expression<Func<T, object>> path);

        /// <summary>
        /// Преобразовать пути в чистые БД-инклюд
        /// </summary>
        /// <param name="paths">Пути</param>
        /// <param name="ignoreStarts">Игнорировать пути, начинающиеся так</param>
        [NotNull]
        ICollection<Expression<Func<T, object>>> GetIncludes<T>(
            [NotNull]IEnumerable<Expression<Func<T, object>>> paths,
            [NotNull]IEnumerable<Expression<Func<T, object>>> ignoreStarts);
    }

}
