﻿namespace TsSoft.Bindings
{
    using TsSoft.BindingsDescription;
    using TsSoft.Expressions.IncludeBuilder;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.Expressions.IncludeBuilder
    /// </summary>
    public class ExpressionsIncludeBuilderBindings : BindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке TsSoft.Expressions.IncludeBuilder
        /// </summary>
        public ExpressionsIncludeBuilderBindings()
        {
            Bind<IConditionalIncludeCreator, ConditionalIncludeCreator>();
            Bind<IIncludeDescriptionHelper, IncludeDescriptionHelper>();
            Bind<IIncludeHelper, IncludeHelper>();
            Bind<IPathToDbIncludeConverter, PathToDbIncludeConverter>();
            Bind<ISelectToIncludeConverter, SelectToIncludeConverter>();
        }
    }
}
