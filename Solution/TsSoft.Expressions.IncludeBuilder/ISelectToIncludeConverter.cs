﻿namespace TsSoft.Expressions.IncludeBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Преобразует селект-выражения в инклюды
    /// </summary>
    public interface ISelectToIncludeConverter
    {
        /// <summary>
        /// Преобразовать в инклюды
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <param name="select">Селект-выражение</param>
        /// <returns>Инклюды</returns>
        Includes<TEntity> ConvertToIncludes<TEntity>([NotNull]IReadOnlySelectExpression<TEntity> select);

        /// <summary>
        /// Преобразовать в пути
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <param name="select">Селект-выражение</param>
        /// <returns>Пути</returns>
        IEnumerable<Expression<Func<TEntity, object>>> ConvertToPaths<TEntity>([NotNull]IReadOnlySelectExpression<TEntity> select);
    }
}
