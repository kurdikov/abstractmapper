﻿namespace TsSoft.Expressions.IncludeBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.IncludeBuilder.Models;

    /// <summary>
    /// Хелпер для разбора инклюдов
    /// </summary>
    public interface IIncludeDescriptionHelper
    {
        /// <summary>
        /// Получить описание применения внешнего инклюда
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="include">Инклюд</param>
        /// <param name="entities">Извлечённые из БД сущности</param>
        [NotNull]
        IncludeApplicationDescription IncludeToApplicationDescription<T>([NotNull]IncludeDescription<T> include, [NotNull]IEnumerable<T> entities) where T: class;

        /// <summary>
        /// Получить описание применения условного инклюда
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="entities">Извлечённые из БД сущности</param>
        /// <param name="condition">Условный инклюд</param>
        [NotNull]
        ConditionalIncludeApplicationDescription IncludeToConditionalDescription<T>(
            [NotNull]IEnumerable<T> entities, 
            [NotNull]IIncludeCondition<T> condition) where T : class;

        /// <summary>
        /// Разбить инклюд на непосредственную и отложенную части (например, внешние или условные инклюды)
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="include">Инклюд</param>
        /// <param name="conditions">Условия</param>
        [NotNull]
        IncludeDescription<T> IncludeToDescription<T>([NotNull]Expression<Func<T, object>> include, ICollection<IIncludeCondition<T>> conditions = null);
    }
}
