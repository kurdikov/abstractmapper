﻿namespace TsSoft.Expressions.IncludeBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;

    internal class IncludeHelper : IIncludeHelper
    {
        [NotNull]private readonly IExpressionCompositionHelper _compositionHelper;
        [NotNull]private readonly IFlatPathParser _flatPathParser;
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;
        [NotNull]private readonly IConditionalIncludeCreator _conditionalIncludeCreator;

        public IncludeHelper(
            [NotNull]IExpressionCompositionHelper compositionHelper,
            [NotNull]IFlatPathParser flatPathParser,
            [NotNull]IExpressionBuilder expressionBuilder,
            [NotNull]IConditionalIncludeCreator conditionalIncludeCreator)
        {
            if (compositionHelper == null) throw new ArgumentNullException("compositionHelper");
            if (flatPathParser == null) throw new ArgumentNullException("flatPathParser");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (conditionalIncludeCreator == null) throw new ArgumentNullException("conditionalIncludeCreator");
            _compositionHelper = compositionHelper;
            _flatPathParser = flatPathParser;
            _expressionBuilder = expressionBuilder;
            _conditionalIncludeCreator = conditionalIncludeCreator;
        }

        private void AddIncludes<TEntity>(
            [NotNull]ICollection<Expression<Func<TEntity, object>>> includes,
            [NotNull]IReadOnlyCollection<Expression> injectedProcessorIncludes,
            [NotNull]IEnumerable<Expression> processedPaths)
        {
            foreach (var path in processedPaths)
            {
                var properties = _flatPathParser.Parse(path);
                bool added = false;
                foreach (var include in injectedProcessorIncludes)
                {
                    includes.Add(_expressionBuilder.BuildIncludeByPath<TEntity>(properties.Concat(_flatPathParser.Parse((include)))));
                    added = true;
                }
                if (!added)
                {
                    includes.Add(_expressionBuilder.BuildIncludeByPath<TEntity>(properties));
                }
            }
        } 

        public void AddIncludes<TEntity, TInjectedProcessorEntity>(
            ICollection<Expression<Func<TEntity, object>>> includes, 
            IEnumerable<Expression<Func<TInjectedProcessorEntity, object>>> processorIncludes, 
            IEnumerable<Expression<Func<TEntity, TInjectedProcessorEntity>>> usedProperties, 
            IEnumerable<Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>>> usedCollections, 
            IEnumerable<Expression<Func<TEntity, IEnumerable<IEnumerable<TInjectedProcessorEntity>>>>> usedCollectionCollections)
        {
            IEnumerable<Expression> props = usedProperties ?? Enumerable.Empty<Expression>();
            IEnumerable<Expression> colls = usedCollections ?? Enumerable.Empty<Expression>();
            var colls2 = usedCollectionCollections ?? Enumerable.Empty<Expression>();
            AddIncludes(includes, processorIncludes.ToReadOnlyCollectionIfNeeded(), props.Concat(colls).Concat(colls2));
        }

        public void AddIncludes<TEntity, TInjectedProcessorEntity>(
            ICollection<Expression<Func<TEntity, object>>> includes, 
            IIncludeProvider<TInjectedProcessorEntity> processor, 
            IEnumerable<Expression<Func<TEntity, TInjectedProcessorEntity>>> usedProperties = null,
            IEnumerable<Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>>> usedCollections = null,
            IEnumerable<Expression<Func<TEntity, IEnumerable<IEnumerable<TInjectedProcessorEntity>>>>> usedCollectionCollections = null)
        {
            AddIncludes(includes, processor.Includes, usedProperties, usedCollections, usedCollectionCollections);
        }

        public void AddIncludesForSelfProcessor<TEntity, TEntityInterface>(
            ICollection<Expression<Func<TEntity, object>>> includes, 
            IIncludeProvider<TEntityInterface> processor) where TEntity : TEntityInterface
        {
            foreach (var include in processor.Includes)
            {
                includes.Add(_compositionHelper.ReplaceInterfaceWithImplementation<TEntity, TEntityInterface>(include));
            }
        }

        public void AddIncludes<TEntity, TInjectedProcessorEntity>(ICollection<Expression<Func<TEntity, object>>> includes, IIncludeProvider<TInjectedProcessorEntity> processor, Expression<Func<TEntity, TInjectedProcessorEntity>> usedProperty)
        {
            AddIncludes(includes, processor, usedProperty.ToEnumerable(), Enumerable.Empty<Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>>>());
        }

        public void AddIncludes<TEntity, TInjectedProcessorEntity>(ICollection<Expression<Func<TEntity, object>>> includes, IIncludeProvider<TInjectedProcessorEntity> processor, Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>> usedCollection)
        {
            AddIncludes(includes, processor, Enumerable.Empty<Expression<Func<TEntity, TInjectedProcessorEntity>>>(), usedCollection.ToEnumerable());
        }

        public void AddIncludes<TEntity, TInjectedProcessorEntity>(
            ICollection<Expression<Func<TEntity, object>>> includes,
            IIncludeProvider<TInjectedProcessorEntity> processor,
            Expression<Func<TEntity, IEnumerable<IEnumerable<TInjectedProcessorEntity>>>> usedCollection)
        {
            AddIncludes(includes, processor, Enumerable.Empty<Expression<Func<TEntity, TInjectedProcessorEntity>>>(), Enumerable.Empty<Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>>>(), usedCollection.ToEnumerable());
        }

        public void LoadConditionalIncludes<TEntity, TInjectedProcessorEntity>(
            ICollection<IIncludeCondition<TEntity>> conditions,
            IEnumerable<IIncludeCondition<TInjectedProcessorEntity>> addedConditions,
            IEnumerable<Expression<Func<TEntity, TInjectedProcessorEntity>>> usedProperties,
            IEnumerable<Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>>> usedCollections)
        {
            IEnumerable<Expression> expressions = (usedProperties ?? Enumerable.Empty<Expression>()).Concat(usedCollections ?? Enumerable.Empty<Expression>());
            var paths = expressions.Select(_flatPathParser.Parse).ToList();
            foreach (var condition in addedConditions)
            {
                if (condition == null)
                {
                    continue;
                }
                foreach (var path in paths)
                {
                    if (path == null)
                    {
                        continue;
                    }
                    var composition = _conditionalIncludeCreator.Compose<TEntity, TInjectedProcessorEntity>(path, condition);
                    var compositionPath = composition.PathProperties;
                    var samePath = conditions.FirstOrDefault(
                        c => c != null && c.PathProperties.SequenceEqual(compositionPath, MemberInfoComparer.Instance));
                    if (samePath == null)
                    {
                        conditions.Add(composition);
                    }
                    else
                    {
                        if (composition.ForeignKey.Count != samePath.ForeignKey.Count 
                            || composition.ForeignKey.Zip(samePath.ForeignKey, (info, propertyInfo) => !MemberInfoComparer.Instance.Equals(info, propertyInfo)).Any())
                        {
                            throw new ArgumentException(string.Format("В условиях {0} и {1} указаны различные внешние ключи сущности {2}",
                                composition,
                                samePath,
                                typeof(TInjectedProcessorEntity)));
                        }
                        samePath.ComposeUsingOr(composition.WhereExpression);
                    }
                }
            }
        }

        private void LoadConditionalIncludes<TEntity, TInjectedProcessorEntity>(
            [NotNull]ICollection<IIncludeCondition<TEntity>> conditions,
            [NotNull]IIncludeProvider<TInjectedProcessorEntity> processor,
            IEnumerable<Expression<Func<TEntity, TInjectedProcessorEntity>>> usedProperties,
            IEnumerable<Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>>> usedCollections)
        {
            LoadConditionalIncludes(conditions, processor.Includes.IncludeConditions, usedProperties, usedCollections);
        }

        public void LoadConditionalIncludes<TEntity, TInjectedProcessorEntity>(
            Includes<TEntity> includes, 
            IIncludeProvider<TInjectedProcessorEntity> processor,
            IEnumerable<Expression<Func<TEntity, TInjectedProcessorEntity>>> usedProperties,
            IEnumerable<Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>>> usedCollections)
        {
            LoadConditionalIncludes(includes.IncludeConditions, processor, usedProperties, usedCollections);
        }

        public void LoadConditionalIncludes<TEntity, TInjectedProcessorEntity>(
            Includes<TEntity> includes,
            IIncludeProvider<TInjectedProcessorEntity> processor,
            Expression<Func<TEntity, TInjectedProcessorEntity>> usedProperty)
        {
            LoadConditionalIncludes(includes, processor, usedProperty.ToEnumerable(), null);
        }

        public void LoadConditionalIncludes<TEntity, TInjectedProcessorEntity>(
            Includes<TEntity> includes,
            IIncludeProvider<TInjectedProcessorEntity> processor,
            Expression<Func<TEntity, IEnumerable<TInjectedProcessorEntity>>> usedCollection)
        {
            LoadConditionalIncludes(includes, processor, null, usedCollection.ToEnumerable());
        }
    }
}
