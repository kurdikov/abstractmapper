﻿namespace TsSoft.Expressions.IncludeBuilder
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Collections;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Tuples;
    using TsSoft.Expressions.OrderBy;

    internal class ConditionalIncludeCreator : IConditionalIncludeCreator
    {
        [NotNull]private readonly IFlatPathParser _flatPathParser;
        [NotNull]private readonly ICollectionHelper _collectionHelper;
        [NotNull]private readonly IMemberInfoHelper _memberInfoHelper;
        [NotNull]private readonly IEntityTypesHelper _entityTypesHelper;

        [NotNull]private readonly MethodInfo _createMethod;

        [NotNull]private readonly ConcurrentDictionary<EquatableTuple<Type, Type>, Delegate> _createDelegateCache =
            new ConcurrentDictionary<EquatableTuple<Type, Type>, Delegate>();

        public ConditionalIncludeCreator(
            [NotNull]IFlatPathParser flatPathParser, 
            [NotNull]ICollectionHelper collectionHelper,
            [NotNull]IMemberInfoHelper memberInfoHelper,
            [NotNull]IEntityTypesHelper entityTypesHelper)
        {
            if (flatPathParser == null) throw new ArgumentNullException("flatPathParser");
            if (collectionHelper == null) throw new ArgumentNullException("collectionHelper");
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            _flatPathParser = flatPathParser;
            _collectionHelper = collectionHelper;
            _memberInfoHelper = memberInfoHelper;
            _entityTypesHelper = entityTypesHelper;

            _createMethod = memberInfoHelper.GetGenericDefinitionMethodInfo(() => Create<object, object>(
                (IEnumerable<ValueHoldingMember>)null, null, null, null));
        }

        [NotNull]
        private IReadOnlyList<ValueHoldingMember> GetFkProperty<T>([NotNull]Expression<Func<T, object>> expr)
        {
            return new List<ValueHoldingMember>(1) { _memberInfoHelper.GetValueHoldingMember(expr) };
        }

        [NotNull]
        private IReadOnlyList<ValueHoldingMember> GetFkProperty([NotNull]ValueHoldingMember collection)
        {
            var result = _entityTypesHelper.GetInverseForeignKey(collection);
            return result;
        }

        public IIncludeCondition<TEntity> Create<TEntity, TIncludedEntity>(
            Expression<Func<TEntity, IEnumerable<TIncludedEntity>>> path,
            Expression<Func<TIncludedEntity, object>> foreignKeySelector,
            Expression<Func<TIncludedEntity, bool>> where,
            IEnumerable<IOrderByClause<TIncludedEntity>> orderBy = null)
        {
            return new IncludeCondition<TEntity, TIncludedEntity>
                {
                    Where = where,
                    OrderBy = orderBy,
                    ForeignKey = GetFkProperty(foreignKeySelector),
                    PathProperties = _flatPathParser.Parse(path),
                };
        }

        public IIncludeCondition<TEntity> Create<TEntity, TIncludedEntity>(
            Expression<Func<TEntity, IEnumerable<TIncludedEntity>>> path,
            Expression<Func<TIncludedEntity, bool>> where,
            IEnumerable<IOrderByClause<TIncludedEntity>> orderBy = null)
        {
            var pathProperties = _flatPathParser.Parse(path);
            var lastPathProperty = pathProperties.Last();
            if (lastPathProperty == null)
            {
                throw new InvalidOperationException("pathProperties.Last() == null");
            }
            return new IncludeCondition<TEntity, TIncludedEntity>
                {
                    Where = where,
                    OrderBy = orderBy,
                    ForeignKey = GetFkProperty(lastPathProperty),
                    PathProperties = pathProperties,
                };
        }

        [NotNull]
        private IIncludeCondition<TEntity> Create<TEntity, TIncludedEntity>(
            [NotNull]IEnumerable<ValueHoldingMember> path,
            [NotNull]IReadOnlyList<ValueHoldingMember> foreignKeySelector,
            [NotNull]object where,
            object orderBy)
        {
            var typedWhere = where as Expression<Func<TIncludedEntity, bool>>;
            if (typedWhere == null)
            {
                throw new ArgumentException(string.Format("Invalid where type : expected filter for {0}, got {1}", typeof(TIncludedEntity), where.GetType()));
            }
            return new IncludeCondition<TEntity, TIncludedEntity>
            {
                Where = typedWhere,
                OrderBy = orderBy as IEnumerable<IOrderByClause<TIncludedEntity>>,
                ForeignKey = foreignKeySelector,
                PathProperties = path.ToReadOnlyListIfNeeded(),
            };
        }

        [NotNull]
        private Func<IEnumerable<ValueHoldingMember>, IReadOnlyList<ValueHoldingMember>, object, object, IIncludeCondition<T>>
            GetCreateDelegate<T>([NotNull]Type includedEntityType)
        {
            var result = _createDelegateCache.GetOrAdd(
                EquatableTuple.Create(typeof(T), includedEntityType),
                tt => DelegateCreator.Create<Func<IEnumerable<ValueHoldingMember>, IReadOnlyList<ValueHoldingMember>, object, object, IIncludeCondition<T>>>(this, _createMethod.MakeGenericMethod(tt.Item1, tt.Item2)))
                as Func<IEnumerable<ValueHoldingMember>, IReadOnlyList<ValueHoldingMember>, object, object, IIncludeCondition<T>>;
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Include creator delegate cache contains null for {0}, {1}", typeof(T), includedEntityType));
            }
            return result;
        }

        public IEnumerable<IIncludeCondition<TNestedEntity>> GetNested<TEntity, TNestedEntity>(
            IIncludeCondition<TEntity> condition, 
            IEnumerable<IIncludeCondition<TEntity>> nestedConditions)
        {
            nestedConditions = nestedConditions.Where(nc => 
                nc != null
                && !ReferenceEquals(nc, condition)
                && _collectionHelper.IsPrefixAndNotEqual(nc.PathProperties, condition.PathProperties));
            return nestedConditions.Where(nc => nc != null).Select(nc =>
                GetCreateDelegate<TNestedEntity>(nc.IncludedEntityType)
                (
                    nc.PathProperties.Skip(condition.PathProperties.Count),
                    nc.ForeignKey,
                    nc.WhereExpression,
                    nc.OrderByObjects
                ));
        }

        public IIncludeCondition<TOuterEntity> Compose<TOuterEntity, TInnerEntity>(
            IEnumerable<ValueHoldingMember> path, 
            IIncludeCondition<TInnerEntity> innerCondition)
        {
            var @delegate = GetCreateDelegate<TOuterEntity>(innerCondition.IncludedEntityType);
            var result =  @delegate(
                path.Concat(innerCondition.PathProperties),
                innerCondition.ForeignKey,
                innerCondition.WhereExpression,
                innerCondition.OrderByObjects);
            if (result == null)
            {
                throw new InvalidOperationException("_createMethod returned null");
            }
            return result;
        }
    }
}
