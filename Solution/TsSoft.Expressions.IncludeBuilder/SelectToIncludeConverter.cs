﻿namespace TsSoft.Expressions.IncludeBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.SelectBuilder.Models;

    internal class SelectToIncludeConverter : ISelectToIncludeConverter
    {
        [NotNull]private readonly INewExpressionHelper _newExpressionHelper;
        [NotNull]private readonly IExpressionBuilder _expressionBuilder;
        [NotNull]private readonly IEntityTypesHelper _entityTypesHelper;

        public SelectToIncludeConverter(
            [NotNull]INewExpressionHelper newExpressionHelper,
            [NotNull]IExpressionBuilder expressionBuilder,
            [NotNull]IEntityTypesHelper entityTypesHelper)
        {
            if (newExpressionHelper == null) throw new ArgumentNullException("newExpressionHelper");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            _newExpressionHelper = newExpressionHelper;
            _expressionBuilder = expressionBuilder;
            _entityTypesHelper = entityTypesHelper;
        }

        public Includes<TEntity> ConvertToIncludes<TEntity>(IReadOnlySelectExpression<TEntity> select)
        {
            return new Includes<TEntity>(GetPaths(select, true));
        }

        public IEnumerable<Expression<Func<TEntity, object>>> ConvertToPaths<TEntity>(IReadOnlySelectExpression<TEntity> select)
        {
            return GetPaths(select, false);
        }

        [NotNull]
        private IEnumerable<Expression<Func<TEntity, object>>> GetPaths<TEntity>([NotNull]IReadOnlySelectExpression<TEntity> select, bool onlyIncludable)
        {
            var assignments = _newExpressionHelper.GetAssignments(select.Select);
            var paths = GetPaths(onlyIncludable, typeof(TEntity), assignments);
            var includes = paths.Select(p => _expressionBuilder.BuildIncludeByPath<TEntity>(p)).Where(i => i != null);
            return includes.Concat(select.ExternalIncludes ?? Enumerable.Empty<Expression<Func<TEntity, object>>>());
        }

        [NotNull]
        private IEnumerable<Stack<ValueHoldingMember>> GetPaths(
            bool onlyNavigation,
            [NotNull]Type entityType, 
            [NotNull]IEnumerable<KeyValuePair<MemberInfo, Expression>> assignments)
        {
            var result = new List<Stack<ValueHoldingMember>>();
            foreach (var assignment in assignments)
            {
                var parsed = _newExpressionHelper.ParseAssignment(assignment, entityType);
                if (parsed.UsedMember == null)
                {
                    throw new InvalidOperationException("UsedProperty is null");
                }
                IEnumerable<Stack<ValueHoldingMember>> innerPaths = Enumerable.Empty<Stack<ValueHoldingMember>>();
                var usedMember = parsed.UsedMember;
                if (_entityTypesHelper.IsNavigationalSingleProperty(usedMember))
                {
                    if (parsed.InnerAssignment == null)
                    {
                        throw new InvalidOperationException(string.Format("Invalid select: {0} is an object property, but its inner assignments are not found", parsed.UsedMember));
                    }
                    innerPaths = GetPaths(onlyNavigation, parsed.UsedMember.ValueType, parsed.InnerAssignment);
                }
                else if (_entityTypesHelper.IsNavigationalCollectionProperty(usedMember))
                {
                    var innerType = parsed.UsedMember.ValueType.GetGenericEnumerableArgument();
                    innerPaths = GetPaths(onlyNavigation, innerType, _newExpressionHelper.GetAssignments(parsed.InnerSelect));
                }
                else if (!onlyNavigation && _entityTypesHelper.IsColumnProperty(usedMember))
                {
                    innerPaths = new Stack<ValueHoldingMember>().ToEnumerable();
                }
                foreach (var path in innerPaths)
                {
                    if (path == null)
                    {
                        continue;
                    }
                    path.Push(parsed.UsedMember);
                    result.Add(path);
                }
            }
            if (!result.Any())
            {
                result.Add(new Stack<ValueHoldingMember>());
            }
            return result;
        }
    }
}
