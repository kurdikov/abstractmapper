﻿namespace TsSoft.Expressions.IncludeBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.OrderBy;

    /// <summary>
    /// Создаёт условные инклюды
    /// </summary>
    public interface IConditionalIncludeCreator
    {
        /// <summary>
        /// Создать условие на инклюд
        /// </summary>
        /// <typeparam name="TEntity">Основная запрашиваемая сущность</typeparam>
        /// <typeparam name="TIncludedEntity">Сущность, на которую накладывается условие</typeparam>
        /// <param name="path">Путь от запрашиваемой сущности до сущностей с условием</param>
        /// <param name="foreignKeySelector">Внешний ключ сущности с условием</param>
        /// <param name="where">Условие для выборки</param>
        /// <param name="orderBy">Сортировка</param>
        [NotNull]
        IIncludeCondition<TEntity> Create<TEntity, TIncludedEntity>(
            [NotNull]Expression<Func<TEntity, IEnumerable<TIncludedEntity>>> path,
            [NotNull]Expression<Func<TIncludedEntity, object>> foreignKeySelector,
            [NotNull]Expression<Func<TIncludedEntity, bool>> where,
            IEnumerable<IOrderByClause<TIncludedEntity>> orderBy = null);

        /// <summary>
        /// Создать условие на инклюд с автоматическим выбором внешнего ключа
        /// </summary>
        /// <typeparam name="TEntity">Основная запрашиваемая сущность</typeparam>
        /// <typeparam name="TIncludedEntity">Сущность, на которую накладывается условие</typeparam>
        /// <param name="path">Путь от запрашиваемой сущности до сущностей с условием</param>
        /// <param name="where">Условие для выборки</param>
        /// <param name="orderBy">Сортировка</param>
        [NotNull]
        IIncludeCondition<TEntity> Create<TEntity, TIncludedEntity>(
            [NotNull]Expression<Func<TEntity, IEnumerable<TIncludedEntity>>> path,
            [NotNull]Expression<Func<TIncludedEntity, bool>> where,
            IEnumerable<IOrderByClause<TIncludedEntity>> orderBy = null);

        /// <summary>
        /// Создать условия для вложенной сущности из вложенный условий для внешней сущности
        /// </summary>
        /// <typeparam name="TEntity">Исходная запрашиваемая сущность</typeparam>
        /// <typeparam name="TNestedEntity">Сущность, на которую наложено условие, содержащее вложенные условия</typeparam>
        /// <param name="condition">Условие, содержащее вложенные условия</param>
        /// <param name="nestedConditions">Вложенные условия</param>
        [NotNull]
        IEnumerable<IIncludeCondition<TNestedEntity>> GetNested<TEntity, TNestedEntity>(
            [NotNull]IIncludeCondition<TEntity> condition,
            [NotNull]IEnumerable<IIncludeCondition<TEntity>> nestedConditions);

        /// <summary>
        /// Собрать условие на инклюд для внешней сущности
        /// </summary>
        /// <typeparam name="TOuterEntity">Тип внешней сущности</typeparam>
        /// <typeparam name="TInnerEntity">Тип внутренней сущности</typeparam>
        /// <param name="path">Путь от внешней сущности к внутренней</param>
        /// <param name="innerCondition">Условие для внутренней сущности</param>
        [NotNull]
        IIncludeCondition<TOuterEntity> Compose<TOuterEntity, TInnerEntity>(
            [NotNull]IEnumerable<ValueHoldingMember> path,
            [NotNull]IIncludeCondition<TInnerEntity> innerCondition);
    }
}
