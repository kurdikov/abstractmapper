﻿namespace TsSoft.AbstractMapper.Include.Tests
{
    using System.Linq;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.AbstractMapper.Resolvers;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models.AbstractMapper;

    [TestClass]
    public class IncludeProviderMapperTests
    {
        [TestMethod]
        public void TestTwoLevelIncludes()
        {
            var mapper = AbstractMapperFactory.Create<TestEntityMapper>(
                MapperTypes.Are(typeof(TestEntityMapper), typeof(TestEntityMapperWithOneParent),
                                typeof(TestEntityMapperWithoutParent), typeof(TestBaseMapper)),
                Entities.Are(typeof(From)));
            var includes = mapper.Includes;
            IncludeAssert.Contains(f => f.Parent.Parent.Children, includes);
            IncludeAssert.Contains(f => f.Children, includes);
            IncludeAssert.Contains(f => f.Parent.Children, includes);
            IncludeAssert.Contains(f => f.Children.Select(c => c.Children), includes);
            IncludeAssert.Contains(f => f.Parent, includes);
            Assert.AreEqual(5, includes.Count);
        }

        [TestMethod]
        public void TestNestedMapperZeroNesting()
        {
            var mapper = AbstractMapperFactory.Create<NestedIncludeMapper>(
                MapperTypes.Are(typeof(NestedIncludeMapper)), Entities.Are(typeof(From)));
            var actual = mapper.Includes;
            Assert.AreEqual(0, actual.Count);
        }

        [TestMethod]
        public void TestNestedMapperOneNestingLevel()
        {
            var mapper = AbstractMapperFactory.Create<NestedIncludeMapper2>(
                MapperTypes.Are(typeof(NestedIncludeMapper2)), Entities.Are(typeof(From)));
            var actual = mapper.Includes;
            IncludeAssert.Contains(f => f.Parent, actual);
            IncludeAssert.Contains(f => f.Children, actual);
            Assert.AreEqual(2, actual.Count);
        }

        [TestMethod]
        public void TestNestedMapperTwoNestingLevels()
        {
            var mapper = AbstractMapperFactory.Create<NestedIncludeMapper3>(
                MapperTypes.Are(typeof(NestedIncludeMapper3)), Entities.Are(typeof(From)));
            var actual = mapper.Includes;
            Assert.AreEqual(4, actual.Count);
            IncludeAssert.Contains(f => f.Parent.Parent, actual);
            IncludeAssert.Contains(f => f.Parent.Children, actual);
            IncludeAssert.Contains(f => f.Children.Select(c => c.Parent), actual);
            IncludeAssert.Contains(f => f.Children.Select(c => c.Children), actual);
        }

        [TestMethod]
        public void TestBuildTimeResolutionMapper()
        {
            var entityTypesRetriever = new CustomEnvironmentTestEntityTypesRetriever(Entities.Are(typeof(From)), External.None);
            var mapperResolver = new Mock<IMapperResolver>(MockBehavior.Strict);
            var mockMapper = new Mock<IIncludeProviderMapper<From, SimpleNestedTo>>(MockBehavior.Strict);
            mockMapper.Setup(m => m.Includes).Returns(new Includes<From>());
            mapperResolver.Setup(r => r.TryGet<From, SimpleNestedTo>()).Returns(mockMapper.Object).Verifiable();
            var mapper = AbstractMapperFactory.Create<NoMapTimeInjectionMapper>(entityTypesRetriever, mapperResolver.Object);

            var res = mapper.GenerateIncludes(new GeneratorContext(mapper.GetType(), 0));    // хак для того, чтобы перегенерация состоялась
            Assert.IsNotNull(res);
            Assert.AreEqual(2, res.Count);

            res = mapper.GenerateIncludes(new GeneratorContext(mapper.GetType(), 0));
            Assert.IsNotNull(res);
            Assert.AreEqual(2, res.Count);

            mapperResolver.Verify(r => r.TryGet<From, SimpleNestedTo>(), Times.Exactly(2));
        }

        [TestMethod]
        public void TestMapTimeResolutionMapper()
        {
            var entityTypesRetriever = new CustomEnvironmentTestEntityTypesRetriever(Entities.Are(typeof(From)), External.None);
            var mapperResolver = new Mock<IMapperResolver>(MockBehavior.Strict);
            var mockMapper = new Mock<IIncludeProviderMapper<From, SimpleNestedTo>>(MockBehavior.Strict);
            mockMapper.Setup(m => m.Includes).Returns(new Includes<From>());
            mapperResolver.Setup(r => r.TryGet<From, SimpleNestedTo>()).Returns(mockMapper.Object).Verifiable();
            var mapper = AbstractMapperFactory.Create<MapTimeInjectionMapper>(entityTypesRetriever, mapperResolver.Object);

            var res = mapper.GenerateIncludes(new GeneratorContext(mapper.GetType(), 0));    // хак для того, чтобы перегенерация состоялась
            Assert.IsNotNull(res);
            Assert.AreEqual(2, res.Count);

            res = mapper.GenerateIncludes(new GeneratorContext(mapper.GetType(), 0));
            Assert.IsNotNull(res);
            Assert.AreEqual(2, res.Count);

            mapperResolver.Verify(r => r.TryGet<From, SimpleNestedTo>(), Times.Exactly(4));
        }

        public class TestBaseMapper : IncludeProviderMapper<From, ToBase>
        {
            public TestBaseMapper(IAbstractIncludeProviderMapperHelper helper)
                : base(helper)
            {
            }

            protected override IMapRules<ToBase, From> MapRules
            {
                get { return ToBaseRules<ToBase>.MapRules; }
            }

            protected override IIgnoreRules<ToBase> IgnoreRules
            {
                get { return new EmptyIgnoreRules<ToBase>(); }
            }
        }
        public class TestEntityMapperWithoutParent : IncludeProviderMapper<From, ToWithoutParent>
        {
            public TestEntityMapperWithoutParent(IAbstractIncludeProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<ToWithoutParent, From> MapRules
            {
                get
                {
                    return new MapRules<ToWithoutParent, From>
                    {
                        {to => to.ChildrenArray, from => from.Children},
                        {to => to.ChildrenCollection, from => from.Children},
                        {to => to.ChildrenList, from => from.Children},
                        {to => to.ChildrenInterface, from => from.Children},
                        {to => to.ChildrenSet, from => from.Children},
                        ToBaseRules<ToWithoutParent>.MapRules,
                    };
                }
            }

            protected override IIgnoreRules<ToWithoutParent> IgnoreRules
            {
                get { return new EmptyIgnoreRules<ToWithoutParent>(); }
            }
        }

        public class TestEntityMapperWithOneParent : IncludeProviderMapper<From, ToWithOneParent>
        {
            public TestEntityMapperWithOneParent(IAbstractIncludeProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<ToWithOneParent, From> MapRules
            {
                get { return ToBaseRules<ToWithOneParent>.MapRules; }
            }

            protected override IIgnoreRules<ToWithOneParent> IgnoreRules
            {
                get { return new EmptyIgnoreRules<ToWithOneParent>(); }
            }
        }

        public class TestEntityMapper : IncludeProviderMapper<From, ToWithTwoParents>
        {
            public TestEntityMapper(IAbstractIncludeProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override IMapRules<ToWithTwoParents, From> MapRules
            {
                get
                {
                    return new MapRules<ToWithTwoParents, From>
                        {
                            ToBaseRules<ToWithTwoParents>.MapRules,
                            ToProcessedPropsRules<ToWithTwoParents>.MapRules,
                        };
                }
            }

            protected override IIgnoreRules<ToWithTwoParents> IgnoreRules
            {
                get { return new EmptyIgnoreRules<ToWithTwoParents>(); }
            }
        }
        
        public class NestedIncludeMapper : IncludeProviderMapper<From, SimpleNestedTo>
        {
            public NestedIncludeMapper([NotNull] IAbstractIncludeProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            public override int MaxNestingLevel
            {
                get { return 0; }
            }
        }

        public class NestedIncludeMapper2 : NestedIncludeMapper
        {
            public NestedIncludeMapper2([NotNull] IAbstractIncludeProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            public override int MaxNestingLevel
            {
                get { return 1; }
            }
        }

        public class NestedIncludeMapper3 : NestedIncludeMapper
        {
            public NestedIncludeMapper3([NotNull] IAbstractIncludeProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            public override int MaxNestingLevel
            {
                get { return 2; }
            }
        }

        public class NoMapTimeInjectionMapper : IncludeProviderMapper<From, SimpleNestedTo>
        {
            public NoMapTimeInjectionMapper([NotNull] IAbstractIncludeProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override InnerMapperStrategy InnerMapperStrategy
            {
                get { return InnerMapperStrategy.StaticTypes | InnerMapperStrategy.ResolveOnMapperBuilding; }
            }
        }

        public class MapTimeInjectionMapper : IncludeProviderMapper<From, SimpleNestedTo>
        {
            public MapTimeInjectionMapper([NotNull] IAbstractIncludeProviderMapperHelper mapperHelper)
                : base(mapperHelper)
            {
            }

            protected override InnerMapperStrategy InnerMapperStrategy
            {
                get { return InnerMapperStrategy.StaticTypes | InnerMapperStrategy.ResolveOnMapping; }
            }
        }
    }
}
