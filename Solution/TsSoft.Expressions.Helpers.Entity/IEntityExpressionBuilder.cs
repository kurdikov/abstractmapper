﻿namespace TsSoft.Expressions.Helpers.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Построитель выражений для работы с сущностями
    /// </summary>
    public interface IEntityExpressionBuilder
    {
        /// <summary>
        /// Получить лямбда-выражение для выборки сущности по идентификатору
        /// </summary>
        [NotNull]
        LambdaExpression GetGetByIdLambda([NotNull]Type entityType, [NotNull]IReadOnlyList<ValueHoldingMember> key, [NotNull]IReadOnlyList<object> id);

        /// <summary>
        /// Получить лямбда-выражение для выборки сущности по идентификатору
        /// </summary>
        [NotNull]
        LambdaExpression GetGetByIdLambda([NotNull]Type entityType, [NotNull]ValueHoldingMember key, [NotNull]object id);

        /// <summary>
        /// Получить лямбда-выражение для выборки сущности по идентификатору
        /// </summary>
        [NotNull]
        LambdaExpression GetGetByIdLambda([NotNull]Type entityType, [NotNull]IReadOnlyList<object> id);

        /// <summary>
        /// Получить лямбда-выражение для выборки сущности по идентификатору
        /// </summary>
        [NotNull]
        Expression<Func<T, bool>> GetGetByIdLambda<T>([NotNull]IReadOnlyList<object> id);

        /// <summary>
        /// Получить инклюды для свойств
        /// </summary>
        [NotNull]
        LambdaExpression GetIncludeLambda([NotNull]Type entityType, [NotNull]ValueHoldingMember navigationalProperty);

        /// <summary>
        /// Получить список инклюдов для заданных навигационных свойств
        /// </summary>
        /// <returns>List[Expression[Func[entityType, object]]] с инклюдами</returns>
        [NotNull]
        object GetIncludeLambdas([NotNull]Type entityType, IEnumerable<ValueHoldingMember> navigationalProperties);

        /// <summary>
        /// Получить список инклюдов для всех навигационных свойств
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns>List[Expression[Func[entityType, object]]] с инклюдами</returns>
        [NotNull]
        object GetAllIncludes([NotNull]Type entityType);
    }
}
