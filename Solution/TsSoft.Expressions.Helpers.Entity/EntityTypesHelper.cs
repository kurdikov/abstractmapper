﻿namespace TsSoft.Expressions.Helpers.Entity
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Entity.Attributes;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Хелпер для работы с типами сущностей: определяет тип свойства в классе сущности, получает обёрнутые свойства из обёрток, получает имена таблиц и столбцов для raw sql
    /// </summary>
    public class EntityTypesHelper : IEntityTypesHelper
    {
        [NotNull] private readonly IEntityTypesRetriever _entityRetriever;

        [NotNull] private readonly IDictionary<Type, IReadOnlyList<ValueHoldingMember>> _primaryKeys =
            new Dictionary<Type, IReadOnlyList<ValueHoldingMember>>();

        [NotNull] private readonly ConcurrentDictionary<Type, IReadOnlyList<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>>>
            _foreignKeys = new ConcurrentDictionary<Type, IReadOnlyList<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>>>();

        [NotNull] private readonly ConcurrentDictionary<Type, string> _tableNameCache =
            new ConcurrentDictionary<Type, string>();

        /// <summary>
        /// Хелпер для работы с типами сущностей
        /// </summary>
        public EntityTypesHelper(
            [NotNull] IEntityTypesRetriever entityRetriever)
        {
            if (entityRetriever == null) throw new ArgumentNullException("entityRetriever");
            _entityRetriever = entityRetriever;

            foreach (var type in _entityRetriever.GetEntityTypes() ?? Enumerable.Empty<Type>())
            {
                if (type == null)
                {
                    continue;
                }
                var properties = ValueHoldingMember.GetValueHoldingMembers(type)
                    .ToList();
                var pk = properties.Where(pi => pi != null && pi.HasAttribute<KeyAttribute>())
                    .OrderBy(ColumnOrder)
                    .ToList();
                if (pk.Count == 0)
                {
                    var id = properties.SingleOrDefault(pi => "Id".Equals(pi.Name, StringComparison.OrdinalIgnoreCase))
                        ?? properties.SingleOrDefault(pi => pi != null && (type.Name + "Id").Equals(pi.Name, StringComparison.OrdinalIgnoreCase));
                    if (id == null)
                    {
                        throw new InvalidOperationException(
                            string.Format("Unable to determine primary key for entity type {0}", type));
                    }
                    pk = new List<ValueHoldingMember>(1) { id };
                }
                _primaryKeys.Add(type, pk);
            }
        }

        private int ColumnOrder([NotNull]ValueHoldingMember pi)
        {
            var attr = pi.GetCustomAttribute<ColumnAttribute>();
            return attr != null ? attr.Order : -1;
        }

        /// <summary>
        /// Создать объект сущности
        /// </summary>
        public virtual object Create(Type entityType)
        {
            return Activator.CreateInstance(entityType);
        }

        /// <summary>
        /// Является ли свойство навигационной коллекцией
        /// </summary>
        public virtual bool IsNavigationalCollectionProperty(ValueHoldingMember pi)
        {
            if (pi == null)
            {
                throw new ArgumentNullException("pi");
            }
            return pi.ValueType.GetTypeInfo().IsGenericType
                && !pi.HasAttribute<NotMappedAttribute>()
                && pi.ValueType.IsGenericEnumerable()
                && _entityRetriever.IsEntityType(pi.ValueType.GetGenericEnumerableArgument());
        }

        /// <summary>
        /// Является ли свойство навигационной ссылкой
        /// </summary>
        public virtual bool IsNavigationalSingleProperty(ValueHoldingMember pi)
        {
            if (pi == null)
            {
                throw new ArgumentNullException("pi");
            }
            return !pi.HasAttribute<NotMappedAttribute>()
                && _entityRetriever.IsEntityType(pi.ValueType);
        }

        /// <summary>
        /// Соответствует ли свойству столбец
        /// </summary>
        public virtual bool IsColumnProperty(ValueHoldingMember pi)
        {
            if (pi == null)
            {
                throw new ArgumentNullException("pi");
            }
            return !pi.HasAttribute<NotMappedAttribute>()
                && !IsNavigationalSingleProperty(pi)
                && !IsNavigationalCollectionProperty(pi)
                &&
                (
                    pi.IsWriteable
                    || pi.DeclaringType.GetTypeInfo().IsInterface
                        && !pi.ValueType.GetTypeInfo().IsInterface
                        && !_entityRetriever.IsEntityType(pi.ValueType)
                        && !_entityRetriever.IsEntityCollectionType(pi.ValueType)
                        && !_entityRetriever.IsExternalEntityType(pi.ValueType)
                );
        }

        /// <summary>
        /// Является ли свойство зависимым от свойства-столбца
        /// </summary>
        public virtual bool IsDependentProperty(ValueHoldingMember pi)
        {
            if (pi == null)
            {
                throw new ArgumentNullException("pi");
            }
            return pi.HasAttribute<DependentOnColumnAttribute>();
        }

        /// <summary>
        /// Является ли свойство обёрткой над навигационным свойством
        /// </summary>
        public virtual bool IsNavigationWrapperProperty(ValueHoldingMember property)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            return property.HasAttribute<NavigationWrapperAttribute>();
        }

        /// <summary>
        /// Получить свойство-столбец, от которого зависит данное
        /// </summary>
        public virtual ValueHoldingMember GetUnderlyingColumn(ValueHoldingMember dependentProperty)
        {
            var result = GetUnderlyingColumns(dependentProperty).Single();
            if (result == null)
            {
                throw new InvalidOperationException("GetUnderlyingColumns returned collection with null");
            }
            return result;
        }

        /// <summary>
        /// Получить свойства-столбцы, от которого зависит данное
        /// </summary>
        public virtual IEnumerable<ValueHoldingMember> GetUnderlyingColumns(ValueHoldingMember dependentProperty)
        {
            if (dependentProperty == null)
            {
                throw new ArgumentNullException("dependentProperty");
            }
            var declaringType = dependentProperty.DeclaringType;
            if (declaringType == null)
            {
                throw new InvalidOperationException(
                    string.Format(
                        "Unable to determine an underlying property for the property {0} without declaring type",
                        dependentProperty));
            }
            var attributes = dependentProperty.GetCustomAttributes<DependentOnColumnAttribute>();
            if (attributes == null)
            {
                throw new InvalidOperationException(
                    string.Format("GetCustomAttributes returned null for the property {0}", dependentProperty));
            }
            return
                attributes.Where(a => a != null)
                    .Select(a => GetColumnProperty(declaringType, dependentProperty, a.Column));
        }

        private ValueHoldingMember GetColumnProperty(
            [NotNull] Type declaringType, 
            ValueHoldingMember dependentProperty,
            [NotNull] string column)
        {
            var result = ValueHoldingMember.GetValueHoldingMember(declaringType, column);
            if (result == null)
            {
                throw new InvalidOperationException(
                    string.Format(
                        "Property {0} defined as underlying column for {1} not found in {2}",
                        column, dependentProperty, declaringType));
            }
            if (!IsColumnProperty(result))
            {
                throw new InvalidOperationException(
                    string.Format(
                        "Property {0} defined as underlying column for {1} but is not a column property of {2}",
                        column, dependentProperty, declaringType));
            }
            return result;
        }

        /// <summary>
        /// Получить навигационные свойства, обёрнутые данным
        /// </summary>
        public virtual IEnumerable<ValueHoldingMember> GetWrappedNavigation(ValueHoldingMember dependentProperty)
        {
            if (dependentProperty == null)
            {
                throw new ArgumentNullException("dependentProperty");
            }
            var declaringType = dependentProperty.DeclaringType;
            if (declaringType == null)
            {
                throw new InvalidOperationException(
                    string.Format(
                        "Unable to determine an underlying property for the property {0} without declaring type",
                        dependentProperty));
            }
            var attributes = dependentProperty.GetCustomAttributes<NavigationWrapperAttribute>();
            if (attributes == null)
            {
                throw new InvalidOperationException(
                    string.Format("GetCustomAttributes returned null for the property {0}", dependentProperty));
            }
            return
                attributes.Where(a => a != null)
                    .Select(a => GetNavigationProperty(declaringType, dependentProperty, a.Navigation));
        }

        private ValueHoldingMember GetNavigationProperty(
            [NotNull] Type declaringType,
            [NotNull] ValueHoldingMember dependentProperty, 
            [NotNull] string navigation)
        {
            var result = ValueHoldingMember.GetValueHoldingMember(declaringType, navigation);
            if (result == null)
            {
                throw new InvalidOperationException(
                    string.Format(
                        "Property {0} defined as underlying column for {1} not found in {2}",
                        navigation, dependentProperty, declaringType));
            }
            if (!dependentProperty.ValueType.GetTypeInfo().IsAssignableFrom(result.ValueType))
            {
                throw new InvalidOperationException(
                    string.Format(
                        "Property {0} is declared as wrapping the navigation property {1} in {2}, but has inconsistent type",
                        dependentProperty, result, declaringType));
            }
            return result;
        }

        /// <summary>
        /// Является ли свойство ссылкой на внешнюю сущность
        /// </summary>
        public virtual bool IsExternalProperty(ValueHoldingMember pi)
        {
            if (pi == null)
            {
                throw new ArgumentNullException("pi");
            }
            return _entityRetriever.IsExternalEntityType(pi.ValueType);
        }

        /// <summary>
        /// Является ли свойство NotMapped-свойством
        /// </summary>
        public virtual bool IsNonDbProperty(ValueHoldingMember pi)
        {
            if (pi == null)
            {
                throw new ArgumentNullException("pi");
            }
            return pi.HasAttribute<NotMappedAttribute>();
        }

        /// <summary>
        /// Получить свойство, содержащее первичный ключ
        /// </summary>
        public virtual ValueHoldingMember GetPrimaryKeySimple(Type entityType)
        {
            var pk = GetPrimaryKey(entityType);
            if (pk.Count != 1)
            {
                throw new InvalidOperationException(string.Format("Primary key for {0} is not simple", entityType));
            }
            var result = pk[0];
            if (result == null)
            {
                throw new NullReferenceException("pk[0]");
            }
            return result;
        }

        /// <summary>
        /// Получить свойства, содержащие первичный ключ
        /// </summary>
        public virtual IReadOnlyList<ValueHoldingMember> GetPrimaryKey(Type entityType)
        {
            IReadOnlyList<ValueHoldingMember> result;
            _primaryKeys.TryGetValue(entityType, out result);
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Type {0} is not an entity type", entityType));
            }
            return result;
        }

        /// <summary>
        /// Получить набор внешних ключей по атрибутам ForeignKeyAttribute
        /// </summary>
        /// <param name="entityType">Тип сущности</param>
        /// <returns>Набор пар (список образующих внешний ключ свойств, соответствующая ключу навигация)</returns>
        [NotNull]
        protected virtual IReadOnlyList<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>> RetrieveForeignKeys([NotNull]Type entityType)
        {
            var properties = ValueHoldingMember.GetValueHoldingMembers(entityType).ToList();
            var result = new HashSet<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>>();
            var reverseResult = new HashSet<KeyValuePair<ValueHoldingMember, ValueHoldingMember>>();
            foreach (var property in properties)
            {
                if (property == null)
                {
                    continue;
                }
                var attribute = property.GetCustomAttribute<ForeignKeyAttribute>();
                if (attribute == null)
                {
                    continue;
                }
                if (_entityRetriever.IsEntityType(property.ValueType))
                {
                    var names = attribute.Name.Split(',').Select(pn => pn.Trim());
                    result.Add(new KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>(
                        names.Select(n => properties.Single(pi => pi != null && pi.Name == n)).ToArray(),
                        property));
                }
                else
                {
                    reverseResult.Add(new KeyValuePair<ValueHoldingMember, ValueHoldingMember>(
                        property,
                        properties.Single(pi => pi != null && pi.Name == attribute.Name)));
                }
            }
            return reverseResult
                .GroupBy(kv => kv.Value)
                .Where(g => g != null)
                .Select(g => new KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>(
                    g.Select(kv => kv.Key).OrderBy(ColumnOrder).ToList(),
                    g.Key))
                .Concat(result)
                .ToList();
        }

        /// <summary>
        /// Получить пары свойств (внешний ключ, соответствующее навигационное свойство)
        /// </summary>
        public virtual IEnumerable<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>> GetForeignKeys(Type entityType)
        {
            var result = _foreignKeys.GetOrAdd(entityType, RetrieveForeignKeys);
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Foreign keys cache contains null for {0}", entityType));
            }
            return result;
        }

        /// <summary>
        /// Получить свойство, содержащее ключ для заданного sp-навигационного свойства
        /// </summary>
        public virtual ValueHoldingMember TryGetExternalKey(ValueHoldingMember externalNavigation)
        {
            if (externalNavigation.DeclaringType == null)
            {
                throw new InvalidOperationException(string.Format("Unable to find external key for the property {0} without declaring type", externalNavigation));
            }
            var result = ValueHoldingMember.GetValueHoldingMember(externalNavigation.DeclaringType, string.Format("{0}Id", externalNavigation.Name));
            return result;
        }

        /// <summary>
        /// Получить пары свойств (sp-ключ, соответствующее sp-свойство)
        /// </summary>
        public virtual IEnumerable<KeyValuePair<ValueHoldingMember, ValueHoldingMember>> GetExternalKeys(Type entityType)
        {
            var navigational = ValueHoldingMember.GetValueHoldingMembers(entityType)
                .Where(p => p != null && _entityRetriever.IsExternalEntityType(p.ValueType));
            var result = new List<KeyValuePair<ValueHoldingMember, ValueHoldingMember>>();
            foreach (var property in navigational)
            {
                if (property == null)
                {
                    continue;
                }
                var key = TryGetExternalKey(property);
                if (key != null)
                {
                    result.Add(new KeyValuePair<ValueHoldingMember, ValueHoldingMember>(key, property));
                }
            }
            return result;
        }

        /// <summary>
        /// Получить внешний ключ, соответствующий коллекции
        /// </summary>
        public virtual IReadOnlyList<ValueHoldingMember> GetInverseForeignKey(ValueHoldingMember collection)
        {
            var attr = collection.GetCustomAttribute<InversePropertyAttribute>();
            if (attr == null)
            {
                throw new ArgumentException(string.Format("InverseProperty attribute is absent on the property {0} of {1}", collection, collection.DeclaringType));
            }
            if (attr.Property == null)
            {
                throw new ArgumentException(string.Format("InverseProperty attribute on the property {0} of {1} contains null property", collection, collection.DeclaringType));
                
            }
            var singleType = collection.ValueType.IsGenericEnumerable()
                ? collection.ValueType.GetGenericEnumerableArgument()
                : collection.ValueType;
            var navigation = singleType.GetTypeInfo().GetProperty(attr.Property);
            if (navigation == null || navigation.PropertyType != collection.DeclaringType)
            {
                throw new InvalidOperationException(string.Format("InverseProperty attribute contains wrong value on the property {0} of {1}: property '{2}' not found in {3}", collection, collection.DeclaringType, attr.Property, singleType));
            }
            var fk = GetForeignKeys(singleType).SingleOrDefault(k => k.Value != null && k.Value.Equals(navigation));
            if (fk.Key == null)
            {
                throw new InvalidOperationException(string.Format("Unable to find foreign key corresponding to {0} in {1}", navigation, singleType));
            }
            return fk.Key;
        }

        /// <summary>
        /// Получить внешний ключ, соответствующий коллекции
        /// </summary>
        public virtual ValueHoldingMember GetInverseForeignKeySimple(ValueHoldingMember collection)
        {
            var result = GetInverseForeignKey(collection);
            if (result.Count != 1)
            {
                throw new InvalidOperationException(string.Format("Inverse foreign key for property {0} is not simple", collection));
            }
            if (result[0] == null)
            {
                throw new NullReferenceException("result[0]");
            }
            return result[0];
        }

        /// <summary>
        /// Получить имя таблицы
        /// </summary>
        /// <param name="entityType">Тип сущности</param>
        public virtual string GetTableName(Type entityType)
        {
            var result = _tableNameCache.GetOrAdd(entityType, RetrieveTableName);
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Table name cache contains null for {0}", entityType));
            }
            return result;
        }

        /// <summary>
        /// Получить имя столбца
        /// </summary>
        /// <param name="column">Свойство, которому соответствует столбец</param>
        /// <returns>Экранированное имя столбца для использования в SQL-запросе</returns>
        public virtual string GetColumnName(ValueHoldingMember column)
        {
            var columnAttr = column.GetCustomAttribute<ColumnAttribute>();
            var columnName = columnAttr == null ? column.Name : (columnAttr.Name ?? column.Name);
            if (columnName == null)
            {
                throw new InvalidOperationException(string.Format("{0} of {1} has null column name", column, column.DeclaringType));
            }
            return string.Format("[{0}]", columnName);
        }

        /// <summary>
        /// Получить имя таблицы из атрибута TableAttribute на типе
        /// </summary>
        protected virtual string RetrieveTableName(Type entityType)
        {
            var attr = entityType.GetTypeInfo().GetCustomAttribute<TableAttribute>();
            if (attr == null)
            {
                throw new InvalidOperationException(string.Format("TableAttribute is not defined on {0}", entityType));
            }
            return string.IsNullOrEmpty(attr.Schema)
                ? string.Format("[{0}]", attr.Name)
                : string.Format("[{0}].[{1}]", attr.Schema, attr.Name);
        }
    }
}
