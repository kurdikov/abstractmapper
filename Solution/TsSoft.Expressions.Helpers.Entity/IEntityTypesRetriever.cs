﻿namespace TsSoft.Expressions.Helpers.Entity
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Определяет, является ли тип типом сущности
    /// </summary>
    public interface IEntityTypesRetriever
    {
        /// <summary>
        /// Является ли тип сущностью БД
        /// </summary>
        bool IsEntityType(Type type);

        /// <summary>
        /// Является ли тип коллекцией сущностей БД
        /// </summary>
        bool IsEntityCollectionType(Type type);

        /// <summary>
        /// Получить все типы сущностей
        /// </summary>
        IEnumerable<Type> GetEntityTypes();

        /// <summary>
        /// Является ли тип внешней сущностью
        /// </summary>
        bool IsExternalEntityType(Type type);

        /// <summary>
        /// Получить все типы внешних сущностей
        /// </summary>
        IEnumerable<Type> GetExternalEntityTypes();
    }

    /// <summary>
    /// Расширения для IEntityTypesRetriever
    /// </summary>
    public static class EntityTypesRetrieverExtensions
    {
        /// <summary>
        /// Может ли тип участвовать в инклюде
        /// </summary>
        public static bool IsIncludableType([NotNull]this IEntityTypesRetriever retriever, Type type)
        {
            return retriever.IsDbIncludableType(type) ||
                   retriever.IsExternalEntityType(type);
        }

        /// <summary>
        /// Может ли тип участвовать в чистом БД-инклюде
        /// </summary>
        public static bool IsDbIncludableType([NotNull]this IEntityTypesRetriever retriever, Type type)
        {
            return retriever.IsEntityType(type) || retriever.IsEntityCollectionType(type);
        }
    }
}
