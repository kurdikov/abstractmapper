﻿namespace TsSoft.Expressions.Helpers.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Построитель выражений для работы с сущностями
    /// </summary>
    public class EntityExpressionBuilder : IEntityExpressionBuilder
    {
        [NotNull]private readonly IEntityTypesHelper _entityTypesHelper;
        [NotNull]private readonly ICollectionReflectionHelper _collectionReflectionHelper;

        /// <summary>
        /// Построитель выражений для работы с сущностями
        /// </summary>
        public EntityExpressionBuilder(
            [NotNull] IEntityTypesHelper entityTypesHelper, 
            [NotNull] ICollectionReflectionHelper collectionReflectionHelper)
        {
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            if (collectionReflectionHelper == null) throw new ArgumentNullException("collectionReflectionHelper");
            _entityTypesHelper = entityTypesHelper;
            _collectionReflectionHelper = collectionReflectionHelper;
        }

        /// <summary>
        /// Получить лямбда-выражение для выборки сущности по идентификатору
        /// </summary>
        public LambdaExpression GetGetByIdLambda(
            Type entityType,
            IReadOnlyList<ValueHoldingMember> key,
            IReadOnlyList<object> id)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType");
            }
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            if (id == null)
            {
                throw new ArgumentNullException("id");
            }
            if (key.Count != id.Count)
            {
                throw new ArgumentException("key.Count != id.Count");
            }
            if (key.Count < 1)
            {
                throw new ArgumentException("key.Count < 1");
            }
            var parameter = Expression.Parameter(entityType, "e");
            var funcType = typeof(Func<,>).MakeGenericType(entityType, typeof(bool));
            if (key[0] == null || id[0] == null)
            {
                throw new ArgumentException("0");
            }
            Expression expr = PropertyEquals(parameter, key[0], id[0]);
            for (int i = 1; i < key.Count; ++i)
            {
                if (key[i] == null || id[i] == null)
                {
                    throw new ArgumentException(i.ToString(CultureInfo.InvariantCulture));
                }
                expr = Expression.AndAlso(expr, PropertyEquals(parameter, key[i], id[i]));
            }
            var lambda = Expression.Lambda(funcType, expr, parameter);
            return lambda;
        }

        [NotNull]
        private Expression PropertyEquals([NotNull]ParameterExpression parameter, [NotNull]ValueHoldingMember property, [NotNull]object value)
        {
            if (property.ValueType != value.GetType())
            {
                throw new InvalidOperationException(string.Format(
                    "Type mismatch: primary key has type {0}, id has type {1}", property.ValueType, value.GetType()));
            }
            return Expression.Equal(
                Expression.MakeMemberAccess(parameter, property.Member),
                Expression.Constant(value, property.ValueType));
        }

        /// <summary>
        /// Получить лямбда-выражение для выборки сущности по идентификатору
        /// </summary>
        public LambdaExpression GetGetByIdLambda(Type entityType, ValueHoldingMember key, object id)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType");
            }
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            if (id == null)
            {
                throw new ArgumentNullException("id");
            }
            var parameter = Expression.Parameter(entityType, "e");
            var funcType = typeof(Func<,>).MakeGenericType(entityType, typeof(bool));
            var lambda = Expression.Lambda(
                funcType,
                PropertyEquals(parameter, key, id),
                parameter);
            return lambda;
        }

        /// <summary>
        /// Получить лямбда-выражение для выборки сущности по идентификатору
        /// </summary>
        public LambdaExpression GetGetByIdLambda(Type entityType, IReadOnlyList<object> id)
        {
            return GetGetByIdLambda(entityType, _entityTypesHelper.GetPrimaryKey(entityType), id);
        }

        /// <summary>
        /// Получить лямбда-выражение для выборки сущности по идентификатору
        /// </summary>
        public Expression<Func<T, bool>> GetGetByIdLambda<T>(IReadOnlyList<object> id)
        {
            return (Expression<Func<T, bool>>)GetGetByIdLambda(typeof(T), _entityTypesHelper.GetPrimaryKey(typeof(T)), id);
        }

        [NotNull]
        private LambdaExpression GetIncludeLambda([NotNull]ValueHoldingMember navigationalProperty, [NotNull]ParameterExpression parameter, [NotNull]Type funcType)
        {
            return Expression.Lambda(funcType, Expression.MakeMemberAccess(parameter, navigationalProperty.Member), parameter);
        }

        /// <summary>
        /// Получить инклюды для свойств
        /// </summary>
        public LambdaExpression GetIncludeLambda(Type entityType, ValueHoldingMember navigationalProperty)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType");
            }
            var parameter = Expression.Parameter(entityType, "e");
            var funcType = typeof(Func<,>).MakeGenericType(entityType, typeof(object));
            return GetIncludeLambda(navigationalProperty, parameter, funcType);
        }

        /// <summary>
        /// Получить список инклюдов для заданных навигационных свойств
        /// </summary>
        /// <returns>List[Expression[Func[entityType, object]]] с инклюдами</returns>
        public object GetIncludeLambdas(Type entityType, IEnumerable<ValueHoldingMember> navigationalProperties)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType");
            }
            var parameter = Expression.Parameter(entityType, "e");
            var funcType = typeof(Func<,>).MakeGenericType(entityType, typeof(object));
            var exprType = typeof(Expression<>).MakeGenericType(funcType);
            var includes = (navigationalProperties ?? Enumerable.Empty<ValueHoldingMember>())
                .Where(np => np != null)
                .Select(np => GetIncludeLambda(np, parameter, funcType));
            return _collectionReflectionHelper.MakeList(exprType, includes);
        }

        /// <summary>
        /// Получить список инклюдов для всех навигационных свойств
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns>List[Expression[Func[entityType, object]]] с инклюдами</returns>
        public object GetAllIncludes(Type entityType)
        {
            return GetIncludeLambdas(entityType, _entityTypesHelper.GetNavigationalSingleProperties(entityType).Concat(_entityTypesHelper.GetNavigationalCollectionProperties(entityType)));
        }
    }
}
