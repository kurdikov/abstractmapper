﻿namespace TsSoft.Expressions.Helpers.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Расширения для хелпера типов сущностей
    /// </summary>
    public static class EntityTypesHelperExtensions
    {
        /// <summary>
        /// Получить все свойства, которым соответствуют столбцы
        /// </summary>
        [NotNull]
        [ItemNotNull]
        public static IEnumerable<ValueHoldingMember> GetColumnProperties([NotNull]this IEntityTypesHelper helper, [NotNull]Type entityType)
        {
            return entityType.GetTypeInfo().GetProperties().Select(p => new ValueHoldingMember(p))
                .Concat(entityType.GetTypeInfo().GetFields().Select(f => new ValueHoldingMember(f)))
                .Where(helper.IsColumnProperty);
        }

        /// <summary>
        /// Получить свойство, содержащее внешний ключ, по навигационному свойству
        /// </summary>
        /// <returns>Свойство-внешний ключ или null, если исходная навигация не найдена среди внешних ключей</returns>
        public static IEnumerable<ValueHoldingMember> TryGetForeignKey([NotNull]this IEntityTypesHelper helper, [NotNull]ValueHoldingMember singleNavigation)
        {
            var fk = helper.GetForeignKeys(singleNavigation.DeclaringType)
                .FirstOrDefault(k => singleNavigation.Equals(k.Value));
            return fk.Key;
        }

        /// <summary>
        /// Является ли соответствующий навигации внешний ключ необязательным
        /// </summary>
        public static bool IsCorrespondingForeignKeyNotNullable([NotNull] this IEntityTypesHelper helper, [NotNull]ValueHoldingMember singleNavigation)
        {
            var fk = TryGetForeignKey(helper, singleNavigation);
            var result = fk != null && fk.All(k => k != null && !k.ValueType.IsNullableType());
            return result;
        }

        /// <summary>
        /// Получить все навигационные свойства, являющиеся коллекциями
        /// </summary>
        [NotNull]
        public static IEnumerable<ValueHoldingMember> GetNavigationalCollectionProperties([NotNull] this IEntityTypesHelper helper, [NotNull]Type entityType)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType");
            }
            var collections = ValueHoldingMember.GetValueHoldingMembers(entityType).Where(helper.IsNavigationalCollectionProperty);
            return collections;
        }

        /// <summary>
        /// Получить все навигационные свойства, не являющиеся коллекциями
        /// </summary>
        [NotNull]
        public static IEnumerable<ValueHoldingMember> GetNavigationalSingleProperties([NotNull] this IEntityTypesHelper helper, [NotNull]Type entityType)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType");
            }
            var properties = ValueHoldingMember.GetValueHoldingMembers(entityType).Where(helper.IsNavigationalSingleProperty);
            return properties;
        }
    }
}
