﻿namespace TsSoft.Expressions.Helpers.Entity
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Описание навигационных свойств, ведущих к дочерним сущностям
    /// </summary>
    public class ChildMembers
    {
        /// <summary>
        /// Навигационные свойства к одиночной сущности
        /// </summary>
        [NotNull]
        public IReadOnlyCollection<ValueHoldingMember> Single { get; private set; }

        /// <summary>
        /// Навигационные свойства к коллекции
        /// </summary>
        [NotNull]
        public IReadOnlyCollection<ValueHoldingMember> Collections { get; private set; }

        /// <summary>
        /// Описание навигационных свойств, ведущих к дочерним сущностям
        /// </summary>
        public ChildMembers([NotNull] IReadOnlyCollection<ValueHoldingMember> single, [NotNull] IReadOnlyCollection<ValueHoldingMember> collections)
        {
            if (single == null) throw new ArgumentNullException("single");
            if (collections == null) throw new ArgumentNullException("collections");
            Single = single;
            Collections = collections;
        }
    }
}
