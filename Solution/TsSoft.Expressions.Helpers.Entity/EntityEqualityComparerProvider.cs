﻿namespace TsSoft.Expressions.Helpers.Entity
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.EntityRepository.Interfaces;
    using TsSoft.Expressions.Helpers.Differences;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Extensions.NullityHelpers;

    /// <summary>
    /// Предоставляет сравнение на равенство по идентификаторам для классов, реализующих <see cref="IEntityWithId{TId}"/>
    /// </summary>
    public class EntityEqualityComparerProvider : IEqualityComparerProvider
    {
        /// <summary>
        /// Предоставляет сравнение на равенство по идентификаторам для классов, реализующих <see cref="IEntityWithId{TId}"/>
        /// </summary>
        public static readonly EntityEqualityComparerProvider Instance = new EntityEqualityComparerProvider();

        [NotNull]
        private readonly ConcurrentDictionary<Type, object> _cache =
            new ConcurrentDictionary<Type, object>();

        private IEqualityComparer<T> CreateEqualityComparer<T>()
        {
            var entityInterface = typeof(T)
                .GetTypeInfo()
                .GetInterfaces()
                .AppendOne(typeof(T))
                .FirstOrDefault(i => i != null && i.GetTypeInfo().IsGenericType && i.GetGenericTypeDefinition() == typeof(IEntityWithId<>));
            if (entityInterface == null)
            {
                return null;
            }
            var comparerType = typeof(EntityComparer<,>).MakeGenericType(typeof(T), entityInterface.GetTypeInfo().GetGenericArguments()[0]);
            var instanceField = comparerType.GetTypeInfo().GetField("Instance", BindingFlags.Static | BindingFlags.Public).ThrowIfNull(
                "Instance field not found in {0}", comparerType);
            return instanceField.GetValue(null) as IEqualityComparer<T>;
        }

        /// <summary>
        /// Получить сравнитель на равенство для типа
        /// </summary>
        /// <typeparam name="T">Тип объектов</typeparam>
        /// <returns>Сравнитель на равенство</returns>
        public IEqualityComparer<T> GetEqualityComparer<T>()
        {
            return _cache.GetOrAdd(typeof(T), type => CreateEqualityComparer<T>()) as IEqualityComparer<T>;
        }
    }
}
