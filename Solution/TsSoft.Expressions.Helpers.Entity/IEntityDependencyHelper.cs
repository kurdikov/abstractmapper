﻿namespace TsSoft.Expressions.Helpers.Entity
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Хелпер для определения зависимостей между сущностями
    /// </summary>
    public interface IEntityDependencyHelper
    {
        /// <summary>
        /// Получить все типы сущностей, на которые существуют незануляемые ссылки из данной
        /// </summary>
        [NotNull]
        [ItemNotNull]
        IEnumerable<Type> GetHardDependencies([NotNull]Type entityType);

        /// <summary>
        /// Получить все типы сущностей, на которые существуют незануляемые ссылки из данной, по списку её внешних ключей
        /// </summary>
        [NotNull]
        [ItemNotNull]
        IEnumerable<Type> GetHardDependencies([NotNull]IEnumerable<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>> entityForeignKeys);

        /// <summary>
        /// Является ли свойство навигационным к коллекции зависимых сущностей
        /// </summary>
        bool IsNavigationalToChildrenProperty([NotNull]ValueHoldingMember pi, [NotNull]IEnumerable<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>> foreignKeys);

        /// <summary>
        /// Является ли свойство навигационным к зависимой сущности
        /// </summary>
        bool IsNavigationalToSingleChildProperty([NotNull]ValueHoldingMember pi, [NotNull]IEnumerable<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>> foreignKeys);

        /// <summary>
        /// Является ли свойство навигационным к сущности, от которой зависит данная
        /// </summary>
        bool IsNavigationalToParentProperty([NotNull]ValueHoldingMember pi, [NotNull]IEnumerable<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>> foreignKeys);

        /// <summary>
        /// Получить свойства, содержащие зависимые сущности
        /// </summary>
        /// <param name="entityType">Тип сущности</param>
        [NotNull]
        ChildMembers GetChildMembers([NotNull]Type entityType);
    }
}
