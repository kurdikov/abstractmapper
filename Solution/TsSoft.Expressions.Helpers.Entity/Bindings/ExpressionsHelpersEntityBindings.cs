﻿namespace TsSoft.Bindings
{
    using TsSoft.BindingsDescription;
    using TsSoft.Expressions.Helpers.Entity;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.Expressions.Helpers.Entity
    /// </summary>
    public class ExpressionsHelpersEntityBindings : BindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке TsSoft.Expressions.Helpers.Entity
        /// </summary>
        public ExpressionsHelpersEntityBindings()
        {
            Bind<IEntityTypesHelper, EntityTypesHelper>();
            Bind<IEntityExpressionBuilder, EntityExpressionBuilder>();
            Bind<IEntityDependencyHelper, EntityDependencyHelper>();
        }
    }
}
