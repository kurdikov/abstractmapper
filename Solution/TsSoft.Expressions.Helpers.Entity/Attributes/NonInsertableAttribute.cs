﻿namespace TsSoft.Expressions.Helpers.Entity.Attributes
{
    using System;

    /// <summary>
    /// Атрибут для разметки сущностей, в соответствующий которым объект БД нельзя сделать insert
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class NonInsertableAttribute : Attribute
    {
    }
}
