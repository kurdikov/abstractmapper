﻿namespace TsSoft.Expressions.Helpers.Entity.Attributes
{
    using System;

    /// <summary>
    /// Атрибут для разметки сущностей, с соответствующим которым объектом БД нельзя сделать delete
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class NonDeletableAttribute : Attribute
    {
    }
}
