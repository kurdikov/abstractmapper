﻿namespace TsSoft.Expressions.Helpers.Entity.Attributes
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Атрибут для разметки NotMapped-свойств, зависящих от Mapped-свойства, соответствующего столбцу
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class DependentOnColumnAttribute : Attribute
    {
        /// <summary>
        /// Имя столбца, значение которого необходимо для вычисления значения размечаемого свойства
        /// </summary>
        [NotNull]
        public readonly string Column;

        /// <summary>
        /// Атрибут для разметки NotMapped-свойств, зависящих от Mapped-свойства, соответствующего столбцу
        /// </summary>
        public DependentOnColumnAttribute([NotNull]string column)
        {
            if (column == null)
            {
                throw new ArgumentNullException("column");
            }
            Column = column;
        }
    }
}
