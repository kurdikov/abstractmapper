﻿namespace TsSoft.Expressions.Helpers.Entity.Attributes
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Атрибут для разметки NotMapped-свойств, пробрасывающих навигационное свойство
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class NavigationWrapperAttribute : Attribute
    {
        /// <summary>
        /// Имя пробрасываемого навигационного свойства
        /// </summary>
        [NotNull]
        public readonly string Navigation;

        /// <summary>
        /// Атрибут для разметки NotMapped-свойств, пробрасывающих навигационное свойство
        /// </summary>
        public NavigationWrapperAttribute([NotNull]string navigation)
        {
            if (navigation == null)
            {
                throw new ArgumentNullException("navigation");
            }
            Navigation = navigation;
        }    
    }
}
