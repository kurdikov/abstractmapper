﻿namespace TsSoft.Expressions.Helpers.Entity.Attributes
{
    using System;

    /// <summary>
    /// Атрибут для разметки навигационных коллекций, которые являются ссылкой на родителей
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class NavigationToParentAttribute : Attribute
    {
    }
}
