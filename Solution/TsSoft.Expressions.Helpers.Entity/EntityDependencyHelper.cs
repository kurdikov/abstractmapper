namespace TsSoft.Expressions.Helpers.Entity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Entity.Attributes;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// ������ ��� ����������� ������������ ����� ����������
    /// </summary>
    public class EntityDependencyHelper : IEntityDependencyHelper
    {
        [NotNull]private readonly IEntityTypesHelper _entityTypesHelper;

        /// <summary>
        /// ������ ��� ����������� ������������ ����� ����������
        /// </summary>
        public EntityDependencyHelper([NotNull]IEntityTypesHelper entityTypesHelper)
        {
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            _entityTypesHelper = entityTypesHelper;
        }

        /// <summary>
        /// �������� ��� ���� ���������, �� ������� ���������� ������������ ������ �� ������
        /// </summary>
        public virtual IEnumerable<Type> GetHardDependencies(Type entityType)
        {
            return GetHardDependencies(_entityTypesHelper.GetForeignKeys(entityType));
        }

        /// <summary>
        /// �������� ��� ���� ���������, �� ������� ���������� ������������ ������ �� ������, �� ������ � ������� ������
        /// </summary>
        public virtual IEnumerable<Type> GetHardDependencies(IEnumerable<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>> entityForeignKeys)
        {
            var dependencies = (entityForeignKeys ?? Enumerable.Empty<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>>())
                .Where(fk => fk.Key != null && fk.Value != null
                             && !fk.Key.Any(p => p != null && p.ValueType.IsNullableType())
                             && !fk.Value.ValueType.GetTypeInfo().IsDefined(typeof(NonInsertableAttribute)))
                .Select(fk => fk.Value.ValueType)
                .Distinct();
            return dependencies;
        }

        /// <summary>
        /// �������� �� �������� ������������� � ��������� ��������� ���������
        /// </summary>
        public virtual bool IsNavigationalToChildrenProperty(
            ValueHoldingMember pi,
            IEnumerable<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>> foreignKeys)
        {
            if (pi == null)
            {
                throw new ArgumentNullException("pi");
            }
            if (foreignKeys == null)
            {
                throw new ArgumentNullException("foreignKeys");
            }
            return _entityTypesHelper.IsNavigationalCollectionProperty(pi)
                && foreignKeys.All(fk => !pi.Equals(fk.Value))
                && !pi.HasAttribute<NavigationToParentAttribute>();
        }

        /// <summary>
        /// �������� �� �������� ������������� � ��������� ��������
        /// </summary>
        public virtual bool IsNavigationalToSingleChildProperty(
            ValueHoldingMember pi,
            IEnumerable<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>> foreignKeys)
        {
            if (pi == null)
            {
                throw new ArgumentNullException("pi");
            }
            if (foreignKeys == null)
            {
                throw new ArgumentNullException("foreignKeys");
            }
            return _entityTypesHelper.IsNavigationalSingleProperty(pi) && foreignKeys.All(fk => !pi.Equals(fk.Value));
        }

        /// <summary>
        /// �������� �� �������� ������������� � ��������, �� ������� ������� ������
        /// </summary>
        public virtual bool IsNavigationalToParentProperty(
            ValueHoldingMember pi,
            IEnumerable<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>> foreignKeys)
        {
            if (pi == null)
            {
                throw new ArgumentNullException("pi");
            }
            if (foreignKeys == null)
            {
                throw new ArgumentNullException("foreignKeys");
            }
            return _entityTypesHelper.IsNavigationalSingleProperty(pi) && foreignKeys.Any(fk => pi.Equals(fk.Value))
                || _entityTypesHelper.IsNavigationalCollectionProperty(pi) && pi.HasAttribute<NavigationToParentAttribute>();
        }

        /// <summary>
        /// �������� ��������, ���������� ��������� ��������
        /// </summary>
        /// <param name="entityType">��� ��������</param>
        public virtual ChildMembers GetChildMembers(Type entityType)
        {
            var members = ValueHoldingMember.GetValueHoldingMembers(entityType).ToList();
            var fks = _entityTypesHelper.GetForeignKeys(entityType);
            var singleChildMembers = members.Where(
                p => p != null && IsNavigationalToSingleChildProperty(p, fks)
                     && !p.ValueType.GetTypeInfo().IsDefined(typeof(NonDeletableAttribute)))
                .ToList();
            var collectionChildMembers = members.Where(
                p => p != null && IsNavigationalToChildrenProperty(p, fks)
                     && !p.ValueType.GetGenericEnumerableArgument().GetTypeInfo().IsDefined(typeof(NonDeletableAttribute)))
                .ToList();
            return new ChildMembers(singleChildMembers, collectionChildMembers);
        }
    }
}
