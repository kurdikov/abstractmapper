﻿namespace TsSoft.Expressions.Helpers.Entity
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Хелпер для работы с типами сущностей: определяет тип свойства в классе сущности, получает обёрнутые свойства из обёрток, получает имена таблиц и столбцов для raw sql
    /// </summary>
    public interface IEntityTypesHelper
    {
        /// <summary>
        /// Создать объект сущности
        /// </summary>
        [NotNull]
        object Create([NotNull]Type entityType);

        /// <summary>
        /// Является ли свойство навигационной коллекцией
        /// </summary>
        bool IsNavigationalCollectionProperty([NotNull]ValueHoldingMember property);

        /// <summary>
        /// Является ли свойство навигационной ссылкой
        /// </summary>
        bool IsNavigationalSingleProperty([NotNull]ValueHoldingMember property);

        /// <summary>
        /// Соответствует ли свойству столбец
        /// </summary>
        bool IsColumnProperty([NotNull]ValueHoldingMember property);

        /// <summary>
        /// Является ли свойство зависимым от свойства-столбца
        /// </summary>
        bool IsDependentProperty([NotNull]ValueHoldingMember property);

        /// <summary>
        /// Является ли свойство обёрткой над навигационным свойством
        /// </summary>
        bool IsNavigationWrapperProperty([NotNull]ValueHoldingMember property);

        /// <summary>
        /// Является ли свойство ссылкой на внешнюю сущность
        /// </summary>
        bool IsExternalProperty([NotNull]ValueHoldingMember property);

        /// <summary>
        /// Является ли свойство NotMapped-свойством
        /// </summary>
        bool IsNonDbProperty([NotNull]ValueHoldingMember property);

        /// <summary>
        /// Получить свойство-столбец, от которого зависит данное
        /// </summary>
        [NotNull]
        ValueHoldingMember GetUnderlyingColumn([NotNull]ValueHoldingMember dependentProperty);

        /// <summary>
        /// Получить свойства-столбцы, от которого зависит данное
        /// </summary>
        [NotNull]
        [ItemNotNull]
        IEnumerable<ValueHoldingMember> GetUnderlyingColumns([NotNull]ValueHoldingMember dependentProperty);

        /// <summary>
        /// Получить навигационные свойства, обёрнутые данным
        /// </summary>
        [NotNull]
        [ItemNotNull]
        IEnumerable<ValueHoldingMember> GetWrappedNavigation([NotNull]ValueHoldingMember dependentProperty);

        /// <summary>
        /// Получить внешний ключ, соответствующий коллекции
        /// </summary>
        [NotNull]
        IReadOnlyList<ValueHoldingMember> GetInverseForeignKey([NotNull]ValueHoldingMember collection);

        /// <summary>
        /// Получить простой внешний ключ (из одного столбца), соответствующий коллекции
        /// </summary>
        [NotNull]
        ValueHoldingMember GetInverseForeignKeySimple([NotNull]ValueHoldingMember collection);

        /// <summary>
        /// Получить свойство, содержащее первичный ключ
        /// </summary>
        /// <exception cref="InvalidOperationException">Первичный ключ состоит из нескольких столбцов</exception>
        [NotNull]
        ValueHoldingMember GetPrimaryKeySimple([NotNull]Type entityType);

        /// <summary>
        /// Получить свойства, содержащие первичный ключ
        /// </summary>
        [NotNull]
        IReadOnlyList<ValueHoldingMember> GetPrimaryKey([NotNull] Type entityType);

            /// <summary>
        /// Получить пары свойств (внешний ключ, соответствующее навигационное свойство)
        /// </summary>
        [NotNull]
        [ItemNotNull]
        IEnumerable<KeyValuePair<IReadOnlyList<ValueHoldingMember>, ValueHoldingMember>> GetForeignKeys([NotNull]Type entityType);

        /// <summary>
        /// Получить свойство, содержащее ключ для заданного внешнего навигационного свойства
        /// </summary>
        [CanBeNull]
        ValueHoldingMember TryGetExternalKey([NotNull]ValueHoldingMember externalNavigation);

        /// <summary>
        /// Получить пары свойств (ключ, соответствующее внешнее навигационное свойство)
        /// </summary>
        [NotNull]
        [ItemNotNull]
        IEnumerable<KeyValuePair<ValueHoldingMember, ValueHoldingMember>> GetExternalKeys([NotNull]Type entityType); 

        /// <summary>
        /// Получить имя таблицы
        /// </summary>
        /// <param name="entityType">Тип сущности</param>
        /// <returns>Экранированное имя таблицы для использования в SQL-запросе</returns>
        [NotNull]
        string GetTableName([NotNull]Type entityType);

        /// <summary>
        /// Получить имя столбца
        /// </summary>
        /// <param name="column">Столбец</param>
        /// <returns>Экранированное имя столбца для использования в SQL-запросе</returns>
        [NotNull]
        string GetColumnName([NotNull]ValueHoldingMember column);
    }
}
