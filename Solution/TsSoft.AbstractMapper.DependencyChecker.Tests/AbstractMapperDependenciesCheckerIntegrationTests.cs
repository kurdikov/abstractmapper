﻿namespace TsSoft.AbstractMapper.DependencyChecker.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Ninject;
    using TsSoft.AbstractMapper.NinjectResolver;
    using TsSoft.AbstractMapper.Rules;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.NinjectBindings;

    [TestClass]
    public class AbstractMapperDependenciesCheckerIntegrationTests
    {
        private StandardKernel _kernel;

        [TestInitialize]
        public void Init()
        {
            _kernel = new StandardKernel();
            NinjectBinder.BindAllInSingletonScope(_kernel, new[] {typeof(IMapper<,>), typeof(NinjectMapperBinder)});
        }

        [TestMethod]
        public void AllSecondLevelDependenciesSatisfied()
        {
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<TwoLevelDependencyMapper>(_kernel);
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<Mapper<DcFromChild, DcToChild>>(_kernel);
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<Mapper<DcFromGrandchild, DcToGrandchild>>(_kernel);
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<Mapper<DcFromTwoLevel, DcToTwoLevel>>(_kernel);

            _kernel.Get<IMapper<DcFrom, DcTo>>();

            Assert.IsTrue(AbstractMapperDependenciesChecker.HasInContext(_kernel, typeof(IMapper<DcFrom, DcTo>)));

            var result = AbstractMapperDependenciesChecker.CheckAlreadyRequestedMappers(
                typeof(TwoLevelDependencyMapper).ToEnumerable(),
                _kernel).ToList();
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void OnlySelfDependency()
        {
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<TwoLevelDependencyMapper>(_kernel);
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<Mapper<DcFromChild, DcToChild>>(_kernel);
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<Mapper<DcFromGrandchild, DcToGrandchild>>(_kernel);
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<Mapper<DcFromTwoLevel, DcToTwoLevel>>(_kernel);

            _kernel.Get<IMapper<DcFrom, DcTo>>();

            Assert.IsTrue(AbstractMapperDependenciesChecker.HasInContext(_kernel, typeof(IMapper<DcFrom, DcTo>)));

            var result = AbstractMapperDependenciesChecker.CheckAlreadyRequestedMappers(
                typeof(TwoLevelDependencyMapper).ToEnumerable(),
                _kernel).ToList();
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void OneSecondLevelUnsatisfiedDependency()
        {
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<TwoLevelDependencyMapper>(_kernel);
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<Mapper<DcFromChild, DcToChild>>(_kernel);
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<Mapper<DcFromGrandchild, DcToGrandchild>>(_kernel);

            _kernel.Get<IMapper<DcFrom, DcTo>>();

            Assert.IsTrue(AbstractMapperDependenciesChecker.HasInContext(_kernel, typeof(IMapper<DcFrom, DcTo>)));

            var result = AbstractMapperDependenciesChecker.CheckAlreadyRequestedMappers(
                typeof(TwoLevelDependencyMapper).ToEnumerable(),
                _kernel).ToList();
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod]
        public void TwoSecondLevelUnsatisfiedDependencies()
        {
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<TwoLevelDependencyMapper>(_kernel);
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<Mapper<DcFromChild, DcToChild>>(_kernel);

            _kernel.Get<IMapper<DcFrom, DcTo>>();

            Assert.IsTrue(AbstractMapperDependenciesChecker.HasInContext(_kernel, typeof(IMapper<DcFrom, DcTo>)));

            var result = AbstractMapperDependenciesChecker.CheckAlreadyRequestedMappers(
                typeof(TwoLevelDependencyMapper).ToEnumerable(),
                _kernel).ToList();
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod]
        public void OneFirstLevelUnsatisfiedDependency()
        {
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<TwoLevelDependencyMapper>(_kernel);

            _kernel.Get<IMapper<DcFrom, DcTo>>();

            Assert.IsTrue(AbstractMapperDependenciesChecker.HasInContext(_kernel, typeof(IMapper<DcFrom, DcTo>)));

            var result = AbstractMapperDependenciesChecker.CheckAlreadyRequestedMappers(
                typeof(TwoLevelDependencyMapper).ToEnumerable(),
                _kernel).ToList();
            Assert.AreEqual(1, result.Count);
        }

        private class TwoLevelDependencyMapper : Mapper<DcFrom, DcTo>
        {
            public TwoLevelDependencyMapper(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IIgnoreRules<DcTo> IgnoreRules
            {
                get
                {
                    return new IgnoreRules<DcTo>
                    {
                        t => t.Self,
                    };
                }
            }
        }

        private class SelfDependentMapper : Mapper<DcFrom, DcTo>
        {
            public SelfDependentMapper(IAbstractMapperHelper mapperHelper) : base(mapperHelper)
            {
            }

            protected override IIgnoreRules<DcTo> IgnoreRules
            {
                get
                {
                    return new IgnoreRules<DcTo>
                    {
                        t => t.Child,
                    };
                }
            }
        }

        private class DcFrom
        {
            public DcFrom Self { get; set; }

            public DcFromChild Child { get; set; }
        }

        private class DcFromChild
        {
            public DcFromGrandchild Grandchild { get; set; }

            public ICollection<DcFromTwoLevel> TwoLevels { get; set; }
        }

        private class DcFromGrandchild
        {
        }

        private class DcFromTwoLevel
        {
        }

        private class DcTo
        {
            public DcTo Self { get; set; }

            public DcToChild Child { get; set; }
        }

        private class DcToChild
        {
            public DcToGrandchild Grandchild { get; set; }

            public ICollection<DcToTwoLevel> TwoLevels { get; set; }
        }

        private class DcToGrandchild
        {
        }

        private class DcToTwoLevel
        {
        }
    }
}
