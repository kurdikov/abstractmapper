﻿namespace TsSoft.AbstractMapper.NinjectResolver.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Reflection;
    using System.Reflection.Emit;
    using Moq;
    using Ninject;
    using Ninject.Modules;
    using TsSoft.AbstractMapper.Bindings;
    using TsSoft.AbstractMapper.Exceptions;
    using TsSoft.AbstractMapper.NinjectResolver.Attributes;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.SelectBuilder.Models;

    [TestClass]
    public class MapperBinderTest
    {
        public class Module : NinjectModule
        {
            public override void Load()
            {
            }
        }

        [NotNull]
        private Type CopyClass<T>([NotNull]ModuleBuilder builder)
        {
            var result = builder.DefineType(typeof(T).Name, TypeAttributes.Public |
                                           TypeAttributes.Class |
                                           TypeAttributes.AutoClass |
                                           TypeAttributes.AnsiClass |
                                           TypeAttributes.BeforeFieldInit |
                                           TypeAttributes.AutoLayout, typeof(T));
            return result.CreateTypeInfo().AsType();
        }

        [TestMethod]
        public void TestBindAllMappers()
        {
            var kernel = new StandardKernel();
            var mockRetriever = new Mock<IAssemblyTypesRetriever>(MockBehavior.Strict);
            mockRetriever.Setup(r => r.GetTypes(It.Is<Assembly>(ass => ass.Equals(typeof(Module).Assembly))))
                .Returns(new[]
                    {
                        typeof(ABMapper),
                        typeof(BAMapper), 
                        typeof(CDEntityMapper),
                        typeof(DCEntityMapper), 
                        typeof(EFEntitySelectMapper),
                        typeof(FEEntitySelectMapper),
                        typeof(GenericMapper<>),
                        typeof(GenericMapperWithClass<>),
                        typeof(GenericMapperWithAttribute<>),
                        typeof(GenericMapperInherited),
                        typeof(NamedBinding),
                        typeof(NamedBinding2),
                        typeof(IG),
                        typeof(G1),
                        typeof(G2),
                        typeof(G3),
                        typeof(A),
                        typeof(B),
                        typeof(C),
                        typeof(D),
                        typeof(E),
                        typeof(F),
                        typeof(EGEntityCollectionMapper),
                        typeof(FGEntitySelectCollectionMapper),
                        typeof(GenericMapperWithNew<>),
                        typeof(GEMapper),
                        typeof(GEMapper2),
                    });
            NinjectMapperBinder.BindAllMappersFromAssembliesWithModulesInSingletonScope(kernel, new INinjectModule[] { new Module() },
                new[] {typeof(IMapper<,>), typeof(IIncludeProviderMapper<,>), typeof(IIncludeProviderCollectionMapper<,>),
                typeof(ISelectProviderMapper<,>), typeof(ISelectProviderCollectionMapper<,>)}, mockRetriever.Object);

            kernel.Get<IMapper<A, B>>();
            kernel.Get<IMapper<B, A>>();

            var mcd = kernel.Get<IMapper<C, D>>();
            var mdc = kernel.Get<IMapper<D, C>>();
            var mcdi = kernel.Get<IIncludeProviderMapper<C, D>>();
            var mdci = kernel.Get<IIncludeProviderMapper<D, C>>();
            Assert.AreSame(mcd, mcdi);
            Assert.AreSame(mdc, mdci);

            var mef = kernel.Get<IMapper<E, F>>();
            var mfe = kernel.Get<IMapper<F, E>>();
            var mefs = kernel.Get<ISelectProviderMapper<E, F>>();
            var mfes = kernel.Get<ISelectProviderMapper<F, E>>();
            Assert.AreSame(mef, mefs);
            Assert.AreSame(mfe, mfes);
            
            kernel.Get<IMapper<A, IG>>();
            kernel.Get<IMapper<A, G1>>();
            kernel.Get<IMapper<A, G2>>();
            kernel.Get<IMapper<A, G3>>();
            ExceptAssert.Throws<ActivationException>(() => kernel.Get<IMapper<G1, IG>>());
            kernel.Get<IMapper<G1, G1>>();
            kernel.Get<IMapper<G1, G2>>();
            kernel.Get<IMapper<G1, G3>>();
            kernel.Get<IMapper<B, G1>>();
            kernel.Get<IMapper<B, G2>>();
            ExceptAssert.Throws<ActivationException>(() => kernel.Get<IMapper<B, G3>>());
            kernel.Get<IMapper<C, G1>>();
            kernel.Get<IMapper<C, G2>>();
            ExceptAssert.Throws<ActivationException>(() => kernel.Get<IMapper<C, G3>>());
            var mdg = kernel.Get<IMapper<D, G1>>("Name1");
            var mdgs = kernel.Get<ISelectProviderMapper<D, G1>>("Name1");
            kernel.Get<IMapper<D, G1>>("Name2");
            Assert.AreSame(mdg, mdgs);

            var meg = kernel.Get<IMapper<IEnumerable<E>, G1>>();
            var megi = kernel.Get<IIncludeProviderCollectionMapper<E, G1>>();
            var mfg = kernel.Get<IMapper<IEnumerable<F>, G1>>();
            var mfgs = kernel.Get<ISelectProviderCollectionMapper<F, G1>>();
            Assert.AreSame(meg, megi);
            Assert.AreSame(mfg, mfgs);

            Assert.AreEqual(2, kernel.GetAll<IMapper<G1, E>>().Count());
        }

        [TestMethod]
        public void TestBindAllMappersThrowsOnTwoMappersWithSameInterface()
        {
            var kernel = new StandardKernel();
            var mockRetriever = new Mock<IAssemblyTypesRetriever>(MockBehavior.Strict);
            mockRetriever.Setup(r => r.GetTypes(It.Is<Assembly>(ass => ass.Equals(typeof(Module).Assembly))))
                .Returns(new[] { typeof(ABMapper), typeof(ABMapper2) });
            ExceptAssert.Throws<MapperBinderException>(
                () => NinjectMapperBinder.BindAllMappersFromAssembliesWithModulesInSingletonScope(
                    kernel,
                    new INinjectModule[] {new Module()},
                    new[]
                    {
                        typeof(IMapper<,>),
                        typeof(IIncludeProviderMapper<,>),
                        typeof(IIncludeProviderCollectionMapper<,>),
                        typeof(ISelectProviderMapper<,>),
                        typeof(ISelectProviderCollectionMapper<,>)
                    }, 
                    mockRetriever.Object));
        }

        [TestMethod]
        public void TestBindAllInterfacesInSingletonScope()
        {
            var kernel = new StandardKernel();
            NinjectMapperBinder.BindAllInterfacesInSingletonScope<ABMapper>(kernel);
            kernel.Get<IMapper<A, B>>();

            NinjectMapperBinder.BindAllInterfacesInSingletonScope<BAMapper>(kernel, "name");
            kernel.Get<IMapper<B, A>>("name");

            NinjectMapperBinder.BindAllInterfacesInSingletonScope<EFEntitySelectMapper>(kernel);
            var mef = kernel.Get<IMapper<E, F>>();
            var mefs = kernel.Get<ISelectProviderMapper<E, F>>();
            Assert.AreSame(mef, mefs);

            ExceptAssert.Throws<ActivationException>(() => kernel.Get<INotMapper<F, E>>());
        }

        public class A
        {
        }

        public class B
        {
        }

        public class C
        {
        }

        public class D
        {
        }

        public class E
        {
        }

        public class F
        {
        }

        public interface INotMapper<out T1, T2> : ISelectProviderMapper<T2, T1>
        {
        }

        public class ABMapper : IMapper<A, B>
        {
            public B Map(A @from)
            {
                throw new NotImplementedException();
            }
        }
        public class ABMapper2 : IMapper<A, B>
        {
            public B Map(A @from)
            {
                throw new NotImplementedException();
            }
        }

        public class BAMapper : IMapper<B, A>
        {
            public A Map(B @from)
            {
                throw new NotImplementedException();
            }
        }

        public class CDEntityMapper : IIncludeProviderMapper<C, D>
        {
            public D Map(C @from)
            {
                throw new NotImplementedException();
            }

            public IReadOnlyIncludes<C> Includes { get; private set; }
        }

        public class DCEntityMapper : IIncludeProviderMapper<D, C>
        {
            public C Map(D @from)
            {
                throw new NotImplementedException();
            }

            public IReadOnlyIncludes<D> Includes { get; private set; }
        }

        public class EFEntitySelectMapper : INotMapper<F, E>
        {
            public F Map(E @from)
            {
                throw new NotImplementedException();
            }

            public IReadOnlySelectExpression<E> Select { get; private set; }
        }

        public class FEEntitySelectMapper : ISelectProviderMapper<F, E>
        {
            public E Map(F @from)
            {
                throw new NotImplementedException();
            }

            public IReadOnlySelectExpression<F> Select { get; private set; }
        }

        public interface IG
        {
        }
        public class G1 : IG
        {
        }
        public class G2: G1, IG
        {
        }
        public class G3: G1
        {
        }

        public class GenericMapper<T> : IMapper<A, T> where T: IG
        {
            public T Map(A from)
            {
                throw new NotImplementedException();
            }
        }

        public class GenericMapperWithClass<T> : IMapper<B, T> where T: G1
        {
            public T Map(B from)
            {
                throw new NotImplementedException();
            }
        }

        public class GenericMapperWithNew<T> : IMapper<G1, T> where T : IG, new()
        {
            public T Map(G1 @from)
            {
                throw new NotImplementedException();
            }
        }

        public class GenericMapperInherited : GenericMapperWithClass<G2>
        {
        }

        [ExtendedModelsMapped(typeof(G2))]
        public class GenericMapperWithAttribute<T> : IMapper<C, T> where T: G1
        {
            public T Map(C from)
            {
                throw new NotImplementedException();
            }
        }

        [NinjectNamedBinding("Name1")]
        public class NamedBinding : ISelectProviderMapper<D, G1>
        {
            public G1 Map(D from)
            {
                throw new NotImplementedException();
            }

            public IReadOnlySelectExpression<D> Select { get; private set; }
        }

        [NinjectNamedBinding("Name2")]
        public class NamedBinding2 : IMapper<D, G1>
        {
            public G1 Map(D from)
            {
                throw new NotImplementedException();
            }
        }

        public class EGEntityCollectionMapper : IIncludeProviderCollectionMapper<E, G1>
        {
            public G1 Map(System.Collections.Generic.IEnumerable<E> from)
            {
                throw new NotImplementedException();
            }

            public IReadOnlyIncludes<E> Includes
            {
                get { throw new NotImplementedException(); }
            }
        }

        public class FGEntitySelectCollectionMapper : ISelectProviderCollectionMapper<F, G1>
        {

            public G1 Map(System.Collections.Generic.IEnumerable<F> from)
            {
                throw new NotImplementedException();
            }

            public IReadOnlySelectExpression<F> Select
            {
                get { throw new NotImplementedException(); }
            }
        }

        [AllowMultipleBinding]
        public class GEMapper : ISelectProviderMapper<G1, E>
        {
            public E Map(G1 @from)
            {
                throw new NotImplementedException();
            }

            public IReadOnlySelectExpression<G1> Select { get; private set; }
        }
        [AllowMultipleBinding]
        public class GEMapper2 : IIncludeProviderMapper<G1, E>
        {
            public E Map(G1 @from)
            {
                throw new NotImplementedException();
            }

            public IReadOnlyIncludes<G1> Includes { get; private set; }
        }
    }
}
