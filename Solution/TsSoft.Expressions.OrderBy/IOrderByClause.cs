﻿namespace TsSoft.Expressions.OrderBy
{
    using System.Linq;
    using JetBrains.Annotations;

    /// <summary>
    /// Условие сортировки
    /// </summary>
    /// <typeparam name="TEntity">Сортируемый тип</typeparam>
    public interface IOrderByClause<TEntity>
    {
        /// <summary>
        /// Применить условие сортировки как первое
        /// </summary>
        /// <param name="source">Запрос</param>
        /// <returns>Запрос с сортировкой</returns>
        [NotNull]
        IOrderedQueryable<TEntity> ApplyOrderBy([NotNull]IQueryable<TEntity> source);

        /// <summary>
        /// Применить условие сортировки как не первое
        /// </summary>
        /// <param name="source">Запрос</param>
        /// <returns>Запрос с сортировкой</returns>
        [NotNull]
        IOrderedQueryable<TEntity> ApplyThenBy([NotNull]IOrderedQueryable<TEntity> source);
    }
}
