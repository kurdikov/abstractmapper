﻿namespace TsSoft.Expressions.OrderBy
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Условие сортировки
    /// </summary>
    /// <typeparam name="TEntity">Сортируемый тип</typeparam>
    public abstract class OrderByClause<TEntity> : IOrderByClause<TEntity>
    {
        /// <summary>
        /// Условие сортировки
        /// </summary>
        /// <typeparam name="TKeyType">Тип поля, по которому происходит сортировка</typeparam>
        [NotNull]
        public static OrderByClause<TEntity, TKeyType> Create<TKeyType>(
            [NotNull]Expression<Func<TEntity, TKeyType>> keySelector, bool descending = false)
        {
            return new OrderByClause<TEntity, TKeyType>(keySelector, descending);
        }

        /// <summary>
        /// Создать условие сортировки
        /// </summary>
        /// <typeparam name="TKeyType">Сортируемый тип</typeparam>
        /// <param name="keySelector">Путь к полю, по которому происходит сортировка</param>
        /// <param name="descending">Направление сортировки</param>
        /// <returns>Условие сортировки</returns>
        [NotNull]
        public static IOrderByClause<TEntity> Create<TKeyType>(
            [NotNull]LambdaExpression keySelector, 
            bool descending = false)
        {
            var clauseType = typeof (OrderByClause<,>).MakeGenericType(typeof (TKeyType), keySelector.ReturnType);
            return Activator.CreateInstance(clauseType, new object[] {keySelector, descending}) as IOrderByClause<TEntity>;
        }

        /// <summary>
        /// Применить условие сортировки как первое
        /// </summary>
        /// <param name="source">Запрос</param>
        /// <returns>Запрос с сортировкой</returns>
        public abstract IOrderedQueryable<TEntity> ApplyOrderBy(IQueryable<TEntity> source);

        /// <summary>
        /// Применить условие сортировки как не первое
        /// </summary>
        /// <param name="source">Запрос</param>
        /// <returns>Запрос с сортировкой</returns>
        public abstract IOrderedQueryable<TEntity> ApplyThenBy(IOrderedQueryable<TEntity> source);
    }

    /// <summary>
    /// Условие сортировки
    /// </summary>
    /// <typeparam name="TEntity">Тип сортируемой сущности</typeparam>
    /// <typeparam name="TKeyType">Тип поля, по которому идёт сортировка</typeparam>
    public class OrderByClause<TEntity, TKeyType> : OrderByClause<TEntity>
    {
        /// <summary>
        /// Путь к полю, по которому идёт сортировка
        /// </summary>
        [NotNull]
        public Expression<Func<TEntity, TKeyType>> KeySelector { get; set; }

        /// <summary>
        /// Направление сортировки
        /// </summary>
        public bool Descending { get; set; }

        /// <summary>
        /// Условие сортировки
        /// </summary>
        /// <param name="keySelector">Путь к полю, по которому идёт сортировка</param>
        /// <param name="descending">Направление сортировки</param>
        public OrderByClause([NotNull]Expression<Func<TEntity, TKeyType>> keySelector, bool descending = false)
        {
            if (keySelector == null) throw new ArgumentNullException("keySelector");
            KeySelector = keySelector;
            Descending = descending;
        }

        /// <summary>
        /// Применить условие сортировки как первое
        /// </summary>
        /// <param name="source">Запрос</param>
        /// <returns>Запрос с сортировкой</returns>
        public override IOrderedQueryable<TEntity> ApplyOrderBy(IQueryable<TEntity> source)
        {
            return Descending
                       ? source.OrderByDescending(KeySelector)
                       : source.OrderBy(KeySelector);
        }

        /// <summary>
        /// Применить условие сортировки как не первое
        /// </summary>
        /// <param name="source">Запрос</param>
        /// <returns>Запрос с сортировкой</returns>
        public override IOrderedQueryable<TEntity> ApplyThenBy(IOrderedQueryable<TEntity> source)
        {
            return Descending
                       ? source.ThenByDescending(KeySelector)
                       : source.ThenBy(KeySelector);
        }
    }
}
