﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Описание условного инклюда
    /// </summary>
    /// <typeparam name="TDb">Тип условно включённой сущности</typeparam>
    /// <typeparam name="TFk">Тип внешнего ключа условно включённой сущности</typeparam>
    public interface IConditionalIncludeFunctionalDescription<TDb, out TFk>
    {
        /// <summary>
        /// Получить ключ контейнера
        /// </summary>
        TFk GetPk(object container);

        /// <summary>
        /// Получить внешний ключ включённой сущности
        /// </summary>
        TFk GetFk(TDb includedEntity);

        /// <summary>
        /// Получить коллекцию включённых в контейнер сущностей
        /// </summary>
        ICollection<TDb> GetCollection(object container);
    }
}
