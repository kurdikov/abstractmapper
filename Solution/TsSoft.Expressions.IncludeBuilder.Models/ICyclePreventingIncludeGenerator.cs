﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using TsSoft.Expressions.Models.AbstractMapper;

    /// <summary>
    /// Интерфейс генератора селектов, способного включить себя в качестве зависимого генератора
    /// </summary>
    /// <typeparam name="TFrom">Тип сущности</typeparam>
    public interface ICyclePreventingIncludeGenerator<TFrom> : ICyclePreventingGenerator, IIncludeProvider<TFrom>
    {
        /// <summary>
        /// Сгенерировать инклюды в заданном контексте
        /// </summary>
        Includes<TFrom> GenerateIncludes(GeneratorContext context);
    }
}
