﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.OrderBy;

    /// <summary>
    /// Условие на инклюды
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IIncludeCondition<TEntity>
    {
        /// <summary>
        /// Линейный путь к условно включённым сущностям
        /// </summary>
        [NotNull]
        IReadOnlyList<ValueHoldingMember> PathProperties { get; }

        /// <summary>
        /// Внешний ключ включённой сущности
        /// </summary>
        [NotNull]
        IReadOnlyList<ValueHoldingMember> ForeignKey { get; }
            
        /// <summary>
        /// Условие
        /// </summary>
        [NotNull]
        LambdaExpression WhereExpression { get; }

        /// <summary>
        /// Условия сортировки для включённых сущностей
        /// </summary>
        IEnumerable<object> OrderByObjects { get; }

        /// <summary>
        /// Тип условно включённой сущности
        /// </summary>
        [NotNull]
        Type IncludedEntityType { get; }

        /// <summary>
        /// Объединить условия с помощью оператора ИЛИ
        /// </summary>
        /// <param name="expression">Дополнительное условие</param>
        void ComposeUsingOr(LambdaExpression expression);
    }

    internal class IncludeCondition<TEntity, TIncludedEntity> : IIncludeCondition<TEntity>
    {
        [NotNull]public Expression<Func<TIncludedEntity, bool>> Where { get; set; }

        public IEnumerable<IOrderByClause<TIncludedEntity>> OrderBy { get; set; }

        public IReadOnlyList<ValueHoldingMember> PathProperties { get; set; }

        public IReadOnlyList<ValueHoldingMember> ForeignKey { get; set; }

        public LambdaExpression WhereExpression { get { return Where; } }

        public IEnumerable<object> OrderByObjects { get { return OrderBy; } }

        public Type IncludedEntityType { get { return typeof (TIncludedEntity); } }

        public void ComposeUsingOr(LambdaExpression expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            var typedExpression = expression as Expression<Func<TIncludedEntity, bool>>;
            if (typedExpression == null)
            {
                throw new ArgumentException(string.Format(
                    "Нельзя объединить условие типа {0} с данным условием типа {1}",
                    expression.GetType(),
                    Where.GetType()));
            }
            Where = Where.Or(typedExpression);
        }
    }
}
