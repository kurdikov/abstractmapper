﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Описание применения условного инклюда
    /// </summary>
    public class ConditionalIncludeApplicationDescription
    {
        /// <summary>
        /// Контейнеры
        /// </summary>
        [NotNull]
        public IEnumerable<object> Containers { get; private set; }

        /// <summary>
        /// Ключ контейнера
        /// </summary>
        [NotNull]
        public IReadOnlyList<ValueHoldingMember> ContainerPk { get; private set; }

        /// <summary>
        /// Внешний ключ содержимого
        /// </summary>
        [NotNull]
        public IReadOnlyList<ValueHoldingMember> ContainedFk { get; private set; }

        /// <summary>
        /// Свойство-коллекция контейнера
        /// </summary>
        [NotNull]
        public ValueHoldingMember Collection { get; private set; }

        /// <summary>
        /// Тип содержимого
        /// </summary>
        [NotNull]
        public Type ContainedType { get; private set; }

        /// <summary>
        /// Описание применения условного инклюда
        /// </summary>
        public ConditionalIncludeApplicationDescription(
            [NotNull] IEnumerable<object> containers,
            [NotNull] IReadOnlyList<ValueHoldingMember> containerPk,
            [NotNull] IReadOnlyList<ValueHoldingMember> containedFk,
            [NotNull] ValueHoldingMember collection)
        {
            if (containers == null) throw new ArgumentNullException("containers");
            if (containerPk == null) throw new ArgumentNullException("containerPk");
            if (containedFk == null) throw new ArgumentNullException("containedFk");
            if (collection == null) throw new ArgumentNullException("collection");
            if (containerPk.Count != containedFk.Count || containerPk.Count < 1 || containedFk[0] == null)
            {
                throw new ArgumentException("Invalid number of properties in keys");
            }
            Containers = containers;
            ContainerPk = containerPk;
            ContainedFk = containedFk;
            Collection = collection;
            ContainedType = containedFk[0].DeclaringType;
        }
    }
}
