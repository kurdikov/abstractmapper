﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using JetBrains.Annotations;

    /// <summary>
    /// Обработчик сущности, отдающий необходимые ему инклюды
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IIncludeProvider<TEntity>
    {
        /// <summary>
        /// Необходимые обработчику инклюды
        /// </summary>
        [NotNull]
        IReadOnlyIncludes<TEntity> Includes { get; }
    }
}
