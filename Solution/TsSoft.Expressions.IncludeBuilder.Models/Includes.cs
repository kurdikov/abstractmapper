﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Инклюды
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public class Includes<TEntity> : List<Expression<Func<TEntity, object>>>, IReadOnlyIncludes<TEntity>
    {
        /// <summary>
        /// Условия на инклюды
        /// </summary>
        [NotNull]
        public List<IIncludeCondition<TEntity>> IncludeConditions { get; set; }

        /// <summary>
        /// Инклюды
        /// </summary>
        public Includes()
        {
            IncludeConditions = new List<IIncludeCondition<TEntity>>();
        }

        /// <summary>
        /// Копия инклюдов
        /// </summary>
        /// <param name="collection">Исходные инклюды</param>
        public Includes([NotNull] IEnumerable<Expression<Func<TEntity, object>>> collection)
            : base(collection)
        {
            IncludeConditions = new List<IIncludeCondition<TEntity>>();
        }

        /// <summary>
        /// Копия инклюдов
        /// </summary>
        /// <param name="includes">Исходные инклюды</param>
        public Includes([NotNull] Includes<TEntity> includes)
            : base(ThrowIfNull(includes))
        {
            IncludeConditions = new List<IIncludeCondition<TEntity>>(includes.IncludeConditions);
        }

        private static Includes<TEntity> ThrowIfNull([NotNull]Includes<TEntity> includes)
        {
            if (includes == null) throw new ArgumentNullException("includes");
            return includes;
        }

        /// <summary>
        /// Условия на инклюды
        /// </summary>
        IEnumerable<IIncludeCondition<TEntity>> IReadOnlyIncludes<TEntity>.IncludeConditions
        {
            get { return IncludeConditions; }
        }
    }
}
