﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Описание внешнего инклюда
    /// </summary>
    /// <typeparam name="TExt">Тип внешней сущности</typeparam>
    /// <typeparam name="TExtId">Тип идентификатора внешней сущности</typeparam>
    public interface IExternalIncludeFunctionalDescription<in TExt, TExtId>
    {
        /// <summary>
        /// Получить идентификатор внешней сущности из содержащего её контейнера вместе с флагом, определяющим, заполнен ли идентификатор
        /// </summary>
        /// <param name="entity">Контейнер со ссылкой на внешнюю сущность</param>
        KeyValuePair<bool, TExtId> GetId(object entity);

        /// <summary>
        /// Заполнить ссылку на внешнюю сущность
        /// </summary>
        /// <param name="entity">Контейнер со ссылкой на внешнюю сущность</param>
        /// <param name="externalEntity">Внешняя сущность</param>
        void Set(object entity, TExt externalEntity);
    }
}
