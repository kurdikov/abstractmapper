﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Описание условного инклюда
    /// </summary>
    /// <typeparam name="TDb">Тип условно включённой сущности</typeparam>
    /// <typeparam name="TFk">Тип внешнего ключа условно включённой сущности</typeparam>
    /// <typeparam name="TContainer">Тип контейнера, содержащего условно включённые сущности</typeparam>
    public class ConditionalIncludeFunctionalDescription<TContainer, TDb, TFk> :
        IConditionalIncludeFunctionalDescription<TDb, TFk>
    {
        [NotNull]private readonly Func<TContainer, ICollection<TDb>> _collectionGetter;
        [NotNull]private readonly Func<TContainer, TFk> _pkGetter;
        [NotNull]private readonly Func<TDb, TFk> _fkGetter;

        /// <summary>
        /// Описание условного инклюда
        /// </summary>
        public ConditionalIncludeFunctionalDescription(
            [NotNull]Expression<Func<TContainer, ICollection<TDb>>> collectionGetter, 
            [NotNull]Expression<Func<TContainer, TFk>> pkGetter, 
            [NotNull]Expression<Func<TDb, TFk>> fkGetter)
        {
            if (collectionGetter == null) throw new ArgumentNullException("collectionGetter");
            if (pkGetter == null) throw new ArgumentNullException("pkGetter");
            if (fkGetter == null) throw new ArgumentNullException("fkGetter");
            _collectionGetter = collectionGetter.Compile();
            _pkGetter = pkGetter.Compile();
            _fkGetter = fkGetter.Compile();
        }

        /// <summary>
        /// Получить ключ контейнера
        /// </summary>
        public TFk GetPk(object container)
        {
            return _pkGetter((TContainer) container);
        }

        /// <summary>
        /// Получить внешний ключ включённой сущности
        /// </summary>
        public TFk GetFk(TDb includedEntity)
        {
            return _fkGetter(includedEntity);
        }

        /// <summary>
        /// Получить коллекцию включённых в контейнер сущностей
        /// </summary>
        public ICollection<TDb> GetCollection(object container)
        {
            return _collectionGetter((TContainer) container);
        }
    }
}