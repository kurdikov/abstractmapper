﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Результат обработки внешнего инклюда: контейнеры для сущностей, как доставать их Id и куда писать результат
    /// </summary>
    public class IncludeApplicationDescription
    {
        /// <summary>
        /// Полученные из БД сущности
        /// </summary>
        [NotNull]
        public ICollection<object> DbEntities { get; set; }

        /// <summary>
        /// Как доставать идентификатор сущностей
        /// </summary>
        [NotNull]
        public ValueHoldingMember IdSelector { get; set; }

        /// <summary>
        /// Куда записывать сущности
        /// </summary>
        [NotNull]
        public ValueHoldingMember ContainedEntity { get; set; }

        /// <summary>
        /// Тип полученных из БД сущностей
        /// </summary>
        [NotNull]
        public Type DbEntityType { get; set; }
    }
}
