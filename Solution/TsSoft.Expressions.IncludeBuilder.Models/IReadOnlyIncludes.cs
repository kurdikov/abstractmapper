﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Инклюды
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IReadOnlyIncludes<TEntity> : IReadOnlyCollection<Expression<Func<TEntity, object>>>
    {
        /// <summary>
        /// Условия на инклюды
        /// </summary>
        [NotNull]
        IEnumerable<IIncludeCondition<TEntity>> IncludeConditions { get; } 
    }
}
