﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Коллекция описаний внешних инклюдов
    /// </summary>
    /// <typeparam name="TExt">Тип внешней сущности</typeparam>
    /// <typeparam name="TId">Тип идентификатора внешней сущности</typeparam>
    public class ExternalIncludeDescriptionCollection<TExt, TId>
    {
        /// <summary>
        /// Коллекция описаний внешних инклюдов
        /// </summary>
        public ExternalIncludeDescriptionCollection([NotNull] IReadOnlyCollection<KeyValuePair<IncludeApplicationDescription, IExternalIncludeFunctionalDescription<TExt, TId>>> descriptions)
        {
            if (descriptions == null) throw new ArgumentNullException("descriptions");
            Descriptions = descriptions;
        }

        /// <summary>
        /// Описания
        /// </summary>
        [NotNull]
        public IReadOnlyCollection<KeyValuePair<IncludeApplicationDescription, IExternalIncludeFunctionalDescription<TExt, TId>>> Descriptions
        { get; private set; }
    }
}
