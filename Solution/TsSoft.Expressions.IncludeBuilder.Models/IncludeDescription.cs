﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Описание инклюда
    /// </summary>
    public class IncludeDescription<T>
    {
        /// <summary>
        /// БД-часть инклюда
        /// </summary>
        public Expression<Func<T, object>> DbInclude { get; set; }

        /// <summary>
        /// Свойства из БД-части инклюда
        /// </summary>
        public IReadOnlyList<ValueHoldingMember> DbIncludeProperties { get; set; }

        /// <summary>
        /// Полный инклюд
        /// </summary>
        public Expression<Func<T, object>> FullInclude { get; set; }

        /// <summary>
        /// Условный инклюд, обрезавший данный
        /// </summary>
        public IIncludeCondition<T> Condition { get; set; }

        /// <summary>
        /// Инклюд для вложенной сущности
        /// </summary>
        public LambdaExpression IncludeSuffix { get; set; }

        /// <summary>
        /// Тип БД-сущности, на которой обрезан полный инклюд / содержащей свойство с внешней сущностью
        /// </summary>
        public Type DeclaringType { get; set; }

        /// <summary>
        /// Тип получаемой внешней сущности, null в случае БД-сущности
        /// </summary>
        public Type ExternalType { get; set; }

        /// <summary>
        /// Свойство, содержащее внешнюю сущность
        /// </summary>
        public ValueHoldingMember ExternalProperty { get; set; }

        /// <summary>
        /// Как получить по сущностям контейнеры внешних сущностей
        /// </summary>
        public Func<IEnumerable<T>, IEnumerable<object>> ExternalEntityContainersRetriever { get; set; }
    }
}
