﻿namespace TsSoft.Expressions.IncludeBuilder.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;

    /// <summary>
    /// Описание внешнего инклюда
    /// </summary>
    /// <typeparam name="TExt">Тип внешней сущности</typeparam>
    /// <typeparam name="TExtId">Тип идентификатора внешней сущности</typeparam>
    /// <typeparam name="TDb">Тип контейнера, содержащего внешнюю сущность</typeparam>
    public class ExternalIncludeFunctionalDescription<TDb, TExt, TExtId> : IExternalIncludeFunctionalDescription<TExt, TExtId>
    {
        [NotNull]
        private Func<TDb, KeyValuePair<bool, TExtId>> IdSelector { get; set; }

        [NotNull]
        private Action<TDb, TExt> Setter { get; set; }

        /// <summary>
        /// Создать описание внешнего инклюда
        /// </summary>
        /// <param name="idSelector">Сущность БД -> пара (имеется ненулевая ссылка на внешнюю сущность, идентификатор сущности)</param>
        /// <param name="setter">Установщик ссылки</param>
        public ExternalIncludeFunctionalDescription(
            [NotNull]Expression<Func<TDb, KeyValuePair<bool, TExtId>>> idSelector,
            [NotNull]Expression<Action<TDb, TExt>> setter)
        {
            if (idSelector == null)
            {
                throw new ArgumentNullException("idSelector");
            }
            if (setter == null)
            {
                throw new ArgumentNullException("setter");
            }
            IdSelector = idSelector.Compile();
            Setter = setter.Compile();
        }

        /// <summary>
        /// Получить идентификатор внешней сущности из содержащего её контейнера вместе с флагом, определяющим, заполнен ли идентификатор
        /// </summary>
        /// <param name="entity">Контейнер со ссылкой на внешнюю сущность</param>
        public KeyValuePair<bool, TExtId> GetId(object entity)
        {
            var dbEntity = (TDb)entity;
            return IdSelector(dbEntity);
        }

        /// <summary>
        /// Заполнить ссылку на внешнюю сущность
        /// </summary>
        /// <param name="entity">Контейнер со ссылкой на внешнюю сущность</param>
        /// <param name="externalEntity">Внешняя сущность</param>
        public void Set(object entity, TExt externalEntity)
        {
            var dbEntity = (TDb)entity;
            Setter(dbEntity, externalEntity);
        }
    }

}
