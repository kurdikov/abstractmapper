﻿namespace TsSoft.Bindings
{
    using TsSoft.BindingsDescription;
    using TsSoft.ContextWrapper;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.ContextWrapper
    /// </summary>
    public class ContextWrapperBindings : BindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке TsSoft.ContextWrapper
        /// </summary>
        public ContextWrapperBindings()
        {
            Bindings.Add(typeof(IItemWrapper<>), typeof(ItemWrapper<>));
            Bind<IContextAwareTaskFactory, ContextAwareTaskFactory>();
        }
    }
}
