﻿namespace TsSoft.ContextWrapper
{
    /// <summary>
    /// Ковариантный интерфейс для Lazy
    /// </summary>
    public interface ILazy<out T>
    {
        /// <summary>
        /// Вычисленное значение
        /// </summary>
        T Value { get; }

        /// <summary>
        /// Значение вычислено
        /// </summary>
        bool IsValueCreated { get; }
    }
}
