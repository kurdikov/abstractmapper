﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace TsSoft.ContextWrapper
{
    /// <summary>
    /// Множество строк с возможностью многопоточного доступа
    /// </summary>
    public sealed class StringSet : IEnumerable<string>
    {
        private readonly ConcurrentDictionary<string, object> _set = new ConcurrentDictionary<string, object>();

        /// <summary>
        /// Получить перечислитель
        /// </summary>
        public IEnumerator<string> GetEnumerator()
        {
            return _set.Keys.GetEnumerator();
        }

        /// <summary>
        /// Получить перечислитель
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Добавить строку
        /// </summary>
        public void Add(string key)
        {
            _set.TryAdd(key, null);
        }
    }
}
