﻿#if (NET45 || NET46)
namespace TsSoft.ContextWrapper
{
    using System;
    using System.Runtime.Remoting.Messaging;
    using JetBrains.Annotations;

    /// <summary>
    /// Хранилище объектов с использованием CallContext
    /// </summary>
    public class CallContextStore : ItemStoreBase, IItemStore
    {
        /// <summary>
        /// Множество ключей в контексте
        /// </summary>
        [NotNull]
        protected readonly static StringSet Keys = new StringSet();

        private const string StoreName = "CallContextStore";

        /// <summary>
        /// Получить объект по ключу
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="key">Ключ</param>
        public virtual T Get<T>(string key)
            where T: class
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            return GetFromContext(key, GetFromCallContext<ILazy<T>>, StoreName);
        }

        /// <summary>
        /// Подготовить хранилище для нового контекста
        /// </summary>
        public virtual void NewContext()
        {
            NewContext(Keys, GetFromCallContext<object>, PutIntoCallContext, StoreName);
        }

        /// <summary>
        /// Удалить все объекты из хранилища
        /// </summary>
        public virtual void DisposeAll()
        {
            Dispose();
        }

        /// <summary>
        /// Инициализировать хранилище для объекта
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        /// <param name="contextElement">Элемент контекста с производящей объект функцией</param>
        protected override void Initialize(string key, IDisposableLazy<object> contextElement)
        {
            PutIntoContext(key, contextElement, PutIntoCallContext, StoreName, Keys);
        }

        /// <summary>
        /// Вызвать Dispose для объекта из хранилища
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public virtual void DisposeIfCreated(string key)
        {
            DisposeByKey(key, GetFromCallContext<IDisposable>, StoreName);
        }

        private static T GetFromCallContext<T>([NotNull] string key)
            where T: class
        {
            return CallContext.LogicalGetData(key) as T;
        }

        private static void PutIntoCallContext<T>([NotNull]string key, T item)
            where T: class
        {
            CallContext.LogicalSetData(key, item);
        }

        private static void DeleteFromContext([NotNull] string key)
        {
            CallContext.LogicalSetData(key, null);
        }

        /// <summary>
        /// Вызвать Dispose для всех объектов из хранилища
        /// </summary>
        public static void Dispose()
        {
            Dispose(Keys, GetFromCallContext<IDisposable>, StoreName);
        }

        /// <summary>
        /// Очистить хранилище
        /// </summary>
        public virtual void Clear()
        {
            ClearContext(Keys, DeleteFromContext);
        }

        /// <summary>
        /// Инициализировано ли хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public virtual bool IsInitialized<T>(string key)
        {
            return IsInitialized(key, GetFromCallContext<ILazy<T>>);
        }

        /// <summary>
        /// Создано ли значение в хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public virtual bool IsValueCreated<T>(string key)
        {
            return IsValueCreated(key, GetFromCallContext<ILazy<T>>);
        }
    }
}
#endif
