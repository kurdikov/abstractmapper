﻿#if (NETSTANDARD15 || NET46)
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using JetBrains.Annotations;

namespace TsSoft.ContextWrapper
{
    /// <summary>
    /// Хранилище в контексте потока
    /// </summary>
    public class AsyncLocalStore : ItemStoreBase, IItemStore
    {
        [NotNull]private static ConcurrentDictionary<string, AsyncLocal<object>> _items = new ConcurrentDictionary<string, AsyncLocal<object>>();

        private const string StoreName = "AsyncLocalStore";

        /// <summary>
        /// Получить объект по ключу
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="key">Ключ</param>
        public T Get<T>([NotNull]string key)
            where T : class
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            return GetFromContext(key, GetFromAsyncLocalContext<ILazy<T>>, StoreName);
        }

        /// <summary>
        /// Подготовить хранилище для нового контекста
        /// </summary>
        public void NewContext()
        {
            var newValue = new Dictionary<string, object>();
            NewContext(_items.Keys, key => _items[key].Value, (key, obj) => newValue[key] = obj, StoreName);
        }

        /// <summary>
        /// Вызвать Dispose для всех объектов из хранилища
        /// </summary>
        public void DisposeAll()
        {
            Dispose();
        }

        private static T GetFromAsyncLocalContext<T>([NotNull] string key)
            where T : class
        {
            if (_items == null)
            {
                throw new InvalidOperationException("Store not initialized");
            }
            AsyncLocal<object> result;
            _items.TryGetValue(key, out result);
            return result?.Value as T;
        }

        private static void PutIntoAsyncLocalContext<T>([NotNull] string key, T item)
        {
            _items.AddOrUpdate(
                key,
                k => new AsyncLocal<object> { Value = item },
                (k, v) => { v.Value = item; return v; });
        }

        /// <summary>
        /// Вызвать Dispose для всех объектов из хранилища
        /// </summary>
        public static void Dispose()
        {
            if (_items == null)
            {
                return;
            }
            Dispose(_items.Keys.ToList(), GetFromAsyncLocalContext<IDisposable>, StoreName);
        }

        /// <summary>
        /// Вызвать Dispose для объекта из хранилища
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public void DisposeIfCreated(string key)
        {
            if (_items == null)
            {
                return;
            }
            DisposeByKey(key, GetFromAsyncLocalContext<IDisposable>, StoreName);
        }

        /// <summary>
        /// Инициализировать хранилище для объекта
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        /// <param name="contextElement">Элемент контекста с производящей объект функцией</param>
        protected override void Initialize(string key, IDisposableLazy<object> contextElement)
        {
            PutIntoContext(key, contextElement, PutIntoAsyncLocalContext, StoreName, null);
        }

        /// <summary>
        /// Очистить хранилище
        /// </summary>
        public void Clear()
        {
            foreach (var item in _items)
            {
                item.Value.Value = null;
            }
        }

        /// <summary>
        /// Инициализировано ли хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public bool IsInitialized<T>(string key)
        {
            return _items != null && IsInitialized(key, GetFromAsyncLocalContext<ILazy<T>>);
        }

        /// <summary>
        /// Создано ли значение в хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public bool IsValueCreated<T>(string key)
        {
            return _items != null && IsValueCreated(key, GetFromAsyncLocalContext<ILazy<T>>);
        }
    }
}
#endif
