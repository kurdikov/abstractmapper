﻿namespace TsSoft.ContextWrapper
{
    using System;

    /// <summary>
    /// Ковариантный интерфейс для лениво создающегося, детерминированно уничтожаемого объекта
    /// </summary>
    /// <typeparam name="T">Тип объекта</typeparam>
    public interface IDisposableLazy<out T> : ILazy<T>, IDisposable
#if (NET45 || NET46)
        , ICloneable
#endif
    {
        /// <summary>
        /// Получить копию объекта
        /// </summary>
#if (NET45 || NET46)
        new
#endif
        IDisposableLazy<T> Clone();
    }
}
