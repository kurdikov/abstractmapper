﻿namespace TsSoft.ContextWrapper
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using JetBrains.Annotations;

    /// <summary>
    /// Базовый класс хранилища
    /// </summary>
    public abstract class ItemStoreBase
    {
        /// <summary>
        /// Логирующая функция
        /// </summary>
        public static Action<string> Log { get; set; }
        
        /// <summary>
        /// Получить объект из контекста
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="key">Ключ объекта</param>
        /// <param name="getFromContext">Функция получения из контекста</param>
        /// <param name="storeName">Имя хранилища</param>
        [NotNull]
        protected static T GetFromContext<T>(
            [NotNull] string key,
            [NotNull] Func<string, ILazy<T>> getFromContext,
            [NotNull] string storeName)
            where T : class
        {
            if (Log != null)
            {
                Log(string.Format("{0} requested from {1} by thread {2}", key, storeName, Thread.CurrentThread.ManagedThreadId));
            }
            var lazy = getFromContext(key);
            if (lazy == null)
            {
                throw new InvalidOperationException(string.Format(
                    "Store for {0} with key {1} not initialized", typeof(T), key));
            }
            var result = lazy.Value;
            if (result == null)
            {
                throw new InvalidOperationException(string.Format(
                    "Store called factory method for {0} with key {1}, but it returned null", typeof(T), key));
            }
            return result;
        }

        /// <summary>
        /// Ининциализировано ли хранилище
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="key">Ключ объекта</param>
        /// <param name="getFromContext">Функция получения из контекста</param>
        protected static bool IsInitialized<T>([NotNull]string key, [NotNull] Func<string, ILazy<T>> getFromContext)
        {
            var lazy = getFromContext(key);
            return lazy != null;
        }

        /// <summary>
        /// Создано ли значение в хранилище
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="key">Ключ объекта</param>
        /// <param name="getFromContext">Функция получения из контекста</param>
        protected static bool IsValueCreated<T>([NotNull] string key, [NotNull] Func<string, ILazy<T>> getFromContext)
        {
            var lazy = getFromContext(key);
            return lazy != null && lazy.IsValueCreated;
        }

        /// <summary>
        /// Поместить объект в контекст
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="key">Ключ объекта</param>
        /// <param name="contextElement">Элемент с производящей объект функцией</param>
        /// <param name="setter">Функция записи объекта в контекст</param>
        /// <param name="storeName">Имя хранилища</param>
        /// <param name="keySet">Множество ключей</param>
        protected static void PutIntoContext<T>(
            [NotNull] string key,
            [NotNull] ILazy<T> contextElement,
            [NotNull] Action<string, ILazy<T>> setter,
            [NotNull] string storeName,
            StringSet keySet)
            where T : class
        {
            if (Log != null)
            {
                Log(string.Format("{0} stored in {1} by thread {2}", key, storeName, Thread.CurrentThread.ManagedThreadId));
            }
            if (keySet != null)
            {
                keySet.Add(key);
            }
            setter(key, contextElement);
        }

        /// <summary>
        /// Вызвать Dispose для объекта из хранилища
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        /// <param name="getter">Функция получения объекта из контекста</param>
        /// <param name="storeName">Имя хранилища</param>
        protected static void DisposeByKey(string key, [NotNull]Func<string, IDisposable> getter, [NotNull]string storeName)
        {
            if (key == null)
            {
                return;
            }
            var item = getter(key);
            if (item != null)
            {
                item.Dispose();
                if (Log != null)
                {
                    Log(string.Format("{0} disposed from {1} by thread {2}", key, storeName, Thread.CurrentThread.ManagedThreadId));
                }
            }
            else
            {
                if (Log != null)
                {
                    Log(string.Format("{0} is found not disposable in {1} by thread {2}", key, storeName, Thread.CurrentThread.ManagedThreadId));
                }
            }
        }

        /// <summary>
        /// Удалить все объекты из хранилища
        /// </summary>
        /// <param name="keySet">Множество ключей</param>
        /// <param name="getter">Функция получения объекта из контекста</param>
        /// <param name="storeName">Имя хранилища</param>
        protected static void Dispose(
            [NotNull]IEnumerable<string> keySet,
            [NotNull]Func<string, IDisposable> getter,
            [NotNull]string storeName)
        {
            if (Log != null)
            {
                Log(string.Format("Items from {0} disposed by thread {1}", storeName, Thread.CurrentThread.ManagedThreadId));
            }
            foreach (var key in keySet)
            {
                DisposeByKey(key, getter, storeName);
            }
        }

        /// <summary>
        /// Создать новый контекст
        /// </summary>
        /// <param name="keySet">Множество ключей</param>
        /// <param name="getter">Функция получения объекта из контекста</param>
        /// <param name="setter">Функция записи объекта в контекст</param>
        /// <param name="storeName">Имя хранилища</param>
        protected void NewContext(
            [NotNull]IEnumerable<string> keySet,
            [NotNull]Func<string, object> getter,
            [NotNull]Action<string, object> setter,
            [NotNull]string storeName)
        {
            if (Log != null)
            {
                Log(string.Format("{0} prepared for new context by thread {1}", storeName, Thread.CurrentThread.ManagedThreadId));
            }
            foreach (var key in keySet)
            {
                if (key == null)
                {
                    continue;
                }
                var current = getter(key);
                if (current == null)
                {
                    continue;
                }
                var cloneable = current as IDisposableLazy<object>;
                if (cloneable == null)
                {
                    throw new InvalidOperationException(string.Format(
                        "Non-cloneable item of type {2} stored in {0} with the key {1}", storeName, key, current.GetType()));
                }
                var clone = cloneable.Clone() as IDisposableLazy<object>;
                if (clone == null)
                {
                    throw new InvalidOperationException(string.Format(
                        "Non-cloneable item of type {2} stored in {0} with the key {1}", storeName, key, current.GetType()));
                }
                Initialize(key, clone);
            }
        }

        /// <summary>
        /// Очистить контекст
        /// </summary>
        /// <param name="keySet">Множество ключей</param>
        /// <param name="setter">Функция записи объекта в контекст</param>
        protected static void ClearContext(
            [NotNull] IEnumerable<string> keySet,
            [NotNull] Action<string> setter)
        {
            foreach (var key in keySet)
            {
                if (key == null)
                {
                    continue;
                }
                setter(key);
            }
        }


        /// <summary>
        /// Инициализировать хранилище для объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="key">Ключ объекта</param>
        /// <param name="itemFactory">Функция, производящая объект по ключу</param>
        public virtual void Initialize<T>(string key, Func<string, T> itemFactory) where T : class
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            if (itemFactory == null)
            {
                throw new ArgumentNullException("itemFactory");
            }
            var elem = new DisposableLazy<T>(() => itemFactory(key));
            Initialize(key, elem);
        }

        /// <summary>
        /// Инициализировать хранилище для объекта
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        /// <param name="contextElement">Элемент контекста с производящей объект функцией</param>
        protected abstract void Initialize([NotNull]string key, [NotNull]IDisposableLazy<object> contextElement);
    }
}
