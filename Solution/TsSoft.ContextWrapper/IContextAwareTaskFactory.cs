﻿namespace TsSoft.ContextWrapper
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// Фабрика задач, переносящая контекст
    /// </summary>
    public interface IContextAwareTaskFactory
    {
        /// <summary>
        /// Запустить задачу в перенесённом контексте
        /// </summary>
        [NotNull]
        Task StartNewWithContextSwitch(Action action);
        /// <summary>
        /// Запустить задачу в перенесённом контексте
        /// </summary>
        [NotNull]
        Task StartNewWithContextSwitch(Action action, CancellationToken cancellationToken);
        /// <summary>
        /// Запустить задачу в перенесённом контексте
        /// </summary>
        [NotNull]
        Task StartNewWithContextSwitch(Action action, TaskCreationOptions creationOptions);
        /// <summary>
        /// Запустить задачу в перенесённом контексте
        /// </summary>
        [NotNull]
        Task StartNewWithContextSwitch(Action action, CancellationToken cancellationToken, TaskCreationOptions creationOptions, TaskScheduler scheduler);

        /// <summary>
        /// Запустить задачу в перенесённом контексте
        /// </summary>
        [NotNull]
        Task<T> StartNewWithContextSwitch<T>(Func<T> func);
        /// <summary>
        /// Запустить задачу в перенесённом контексте
        /// </summary>
        [NotNull]
        Task<T> StartNewWithContextSwitch<T>(Func<T> func, CancellationToken cancellationToken);
        /// <summary>
        /// Запустить задачу в перенесённом контексте
        /// </summary>
        [NotNull]
        Task<T> StartNewWithContextSwitch<T>(Func<T> func, TaskCreationOptions creationOptions);
        /// <summary>
        /// Запустить задачу в перенесённом контексте
        /// </summary>
        [NotNull]
        Task<T> StartNewWithContextSwitch<T>(Func<T> func, CancellationToken cancellationToken, TaskCreationOptions creationOptions, TaskScheduler scheduler);

        /// <summary>
        /// Запустить асинхронную задачу-promise в перенесённом контексте
        /// </summary>
        [NotNull]
        Task RunWithContextSwitch(Func<Task> func);

        /// <summary>
        /// Запустить асинхронную задачу-promise в перенесённом контексте
        /// </summary>
        [NotNull]
        Task<T> RunWithContextSwitch<T>(Func<Task<T>> func);
    }
}
