﻿namespace TsSoft.ContextWrapper
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using JetBrains.Annotations;

    /// <summary>
    /// Хранилище в контексте потока
    /// </summary>
    public class ThreadStaticStore: ItemStoreBase, IItemStore
    {
        [ThreadStatic] 
        private static IDictionary<string, object> _items;

        private const string StoreName = "ThreadStaticStore";

        private static void EnsureStoreExists()
        {
            _items = _items ?? new Dictionary<string, object>();
        }

        /// <summary>
        /// Получить объект по ключу
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="key">Ключ</param>
        public T Get<T>([NotNull]string key)
            where T : class
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            return GetFromContext(key, GetFromThreadStaticContext<ILazy<T>>, StoreName);
        }

        /// <summary>
        /// Подготовить хранилище для нового контекста
        /// </summary>
        public void NewContext()
        {
            throw new NotSupportedException("ThreadStaticStore does not support context switching");
        }

        /// <summary>
        /// Вызвать Dispose для всех объектов из хранилища
        /// </summary>
        public void DisposeAll()
        {
            Dispose();
        }

        private static T GetFromThreadStaticContext<T>([NotNull] string key)
            where T: class
        {
            if (_items == null)
            {
                throw new InvalidOperationException("Store not initialized");
            }
            object result;
            _items.TryGetValue(key, out result);
            return result as T;
        }

        private static void PutIntoThreadStaticContext<T>([NotNull] string key, T item)
        {
            EnsureStoreExists();
            Debug.Assert(_items != null);
            _items.Add(key, item);
        }

        /// <summary>
        /// Вызвать Dispose для всех объектов из хранилища
        /// </summary>
        public static void Dispose()
        {
            if (_items == null)
            {
                return;
            }
            Dispose(_items.Keys.ToList(), GetFromThreadStaticContext<IDisposable>, StoreName);
        }

        /// <summary>
        /// Вызвать Dispose для объекта из хранилища
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public void DisposeIfCreated(string key)
        {
            if (_items == null)
            {
                return;
            }
            DisposeByKey(key, GetFromThreadStaticContext<IDisposable>, StoreName);
        }
        
        /// <summary>
        /// Инициализировать хранилище для объекта
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        /// <param name="contextElement">Элемент контекста с производящей объект функцией</param>
        protected override void Initialize(string key, IDisposableLazy<object> contextElement)
        {
            PutIntoContext(key, contextElement, PutIntoThreadStaticContext, StoreName, null);
        }

        /// <summary>
        /// Очистить хранилище
        /// </summary>
        public void Clear()
        {
            if (_items == null)
            {
                return;
            }
            _items.Clear();
        }

        /// <summary>
        /// Инициализировано ли хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public bool IsInitialized<T>(string key)
        {
            return _items != null && IsInitialized(key, GetFromThreadStaticContext<ILazy<T>>);
        }

        /// <summary>
        /// Создано ли значение в хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        public bool IsValueCreated<T>(string key)
        {
            return _items != null && IsValueCreated(key, GetFromThreadStaticContext<ILazy<T>>);
        }
    }
}
