﻿namespace TsSoft.ContextWrapper
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Обёртка для хранилища
    /// </summary>
    /// <typeparam name="T">Тип хранимого объекта</typeparam>
    public class ItemWrapper<T> : IItemWrapper<T>
        where T: class
    {
        [NotNull]
        private readonly IItemFactory<T> _factory;
        [NotNull]
        private readonly IItemStore _store;

        [NotNull]
        private string _key;

        /// <summary>
        /// Ключ объекта
        /// </summary>
        [NotNull]
        public string Key
        {
            get { return _key; }
            set 
            { 
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                _key = value;
            }
        }

        /// <summary>
        /// Обёртка для хранилища
        /// </summary>
        public ItemWrapper([NotNull]IItemFactory<T> factory, [NotNull]IItemStore store)
        {
            if (factory == null) throw new ArgumentNullException("factory");
            if (store == null) throw new ArgumentNullException("store");
            _factory = factory;
            _store = store;

            _key = string.Format("TsSoft.Wrapper_{0}", typeof(T));
        }

        /// <summary>
        /// Хранимый объект
        /// </summary>
        public T Current
        {
            get
            {
                var result = _store.Get<T>(Key);
                return result;
            }
        }

        /// <summary>
        /// Инициализировать объект в хранилище
        /// </summary>
        public void Initialize()
        {
            _store.Initialize(Key, _factory.Create);
        }

        /// <summary>
        /// Вызвать Dispose объекта, если он создан и IDisposable
        /// </summary>
        public void Dispose()
        {
            _store.DisposeIfCreated(Key);
        }

        /// <summary>
        /// Инициализировано ли хранилище
        /// </summary>
        public bool IsInitialized
        {
            get { return _store.IsInitialized<T>(Key); }
        }

        /// <summary>
        /// Создано ли значение в хранилище
        /// </summary>
        public bool IsValueCreated { get { return _store.IsValueCreated<T>(Key); } }
    }
}
