﻿namespace TsSoft.ContextWrapper
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Реализация ковариантного lazy-интерфейса для размещения в хранилище
    /// </summary>
    public class DisposableLazy<T> : IDisposableLazy<T>
    {
        [NotNull] private readonly Lazy<T> _lazy;

        [NotNull] private readonly Func<T> _factory;

        /// <summary>
        /// Реализация ковариантного lazy-интерфейса для размещения в хранилище
        /// </summary>
        public DisposableLazy([NotNull]Func<T> factory)
        {
            if (factory == null) throw new ArgumentNullException("factory");
            _factory = factory;
            _lazy = new Lazy<T>(factory);
        }

        /// <summary>
        /// Вызвать Dispose для содержимого, если оно IDisposable и уже создано
        /// </summary>
        public void Dispose()
        {
            if (!IsValueCreated)
            {
                return;
            }
            var disposable = Value as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }
        }

        /// <summary>
        /// Содержимое
        /// </summary>
        public T Value
        {
            get { return _lazy.Value; }
        }

        /// <summary>
        /// Создано ли содержимое
        /// </summary>
        public bool IsValueCreated
        {
            get { return _lazy.IsValueCreated; }
        }

        /// <summary>
        /// Скопировать фабрику содержимого
        /// </summary>
        [NotNull]
        public DisposableLazy<T> Clone()
        {
            return new DisposableLazy<T>(_factory);
        }

        IDisposableLazy<T> IDisposableLazy<T>.Clone()
        {
            return Clone();
        }

#if (NET45 || NET46)
        /// <summary>
        /// Скопировать фабрику содержимого
        /// </summary>
        object ICloneable.Clone()
        {
            return Clone();
        }
#endif
    }
}
