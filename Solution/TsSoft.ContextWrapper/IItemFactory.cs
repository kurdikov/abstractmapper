﻿namespace TsSoft.ContextWrapper
{
    /// <summary>
    /// Фабрика объектов
    /// </summary>
    /// <typeparam name="T">Тип объекта</typeparam>
    public interface IItemFactory<out T>
        where T : class
    {
        /// <summary>
        /// Создать объект
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <returns>Объект</returns>
        T Create(string key);
    }
}
