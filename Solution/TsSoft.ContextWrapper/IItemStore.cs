﻿namespace TsSoft.ContextWrapper
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Контекстно-зависимое хранилище объектов
    /// </summary>
    public interface IItemStore
    {
        /// <summary>
        /// Инициализировать хранилище для объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="key">Ключ объекта</param>
        /// <param name="itemFactory">Функция, производящая объект по ключу</param>
        void Initialize<T>(string key, Func<string, T> itemFactory) where T: class;

        /// <summary>
        /// Получить объект по ключу
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="key">Ключ</param>
        [NotNull]
        T Get<T>(string key) where T: class;

        /// <summary>
        /// Подготовить хранилище для нового контекста
        /// </summary>
        void NewContext();

        /// <summary>
        /// Вызвать Dispose для объекта из хранилища
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        void DisposeIfCreated(string key);

        /// <summary>
        /// Вызвать Dispose для всех объектов из хранилища
        /// </summary>
        void DisposeAll();

        /// <summary>
        /// Удалить все элементы из хранилища
        /// </summary>
        void Clear();

        /// <summary>
        /// Инициализировано ли хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        bool IsInitialized<T>([NotNull]string key);

        /// <summary>
        /// Создано ли значение в хранилище
        /// </summary>
        /// <param name="key">Ключ объекта</param>
        bool IsValueCreated<T>([NotNull] string key);
    }
}
