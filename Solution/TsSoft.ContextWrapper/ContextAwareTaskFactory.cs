﻿namespace TsSoft.ContextWrapper
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    class ContextAwareTaskFactory : IContextAwareTaskFactory
    {
        [NotNull] private readonly IItemStore _itemStore;

        public ContextAwareTaskFactory([NotNull] IItemStore itemStore)
        {
            if (itemStore == null) throw new ArgumentNullException("itemStore");
            _itemStore = itemStore;
        }

        private void SwitchContext()
        {
            _itemStore.NewContext();
        }

        private void DisposeContext()
        {
            _itemStore.DisposeAll();
            _itemStore.Clear();
        }

        [NotNull]
        private Action SwitchContextAndRun([NotNull]Action action)
        {
            return () =>
            {
                try
                {
                    SwitchContext();
                    action();
                }
                finally
                {
                    DisposeContext();
                }
            };
        }

        [NotNull]
        private Func<T> SwitchContextAndRun<T>([NotNull]Func<T> func)
        {
            return () =>
            {
                T result;
                try
                {
                    SwitchContext();
                    result = func();
                }
                finally
                {
                    DisposeContext();
                }
                return result;
            };
        }

        [NotNull]
        private async Task SwitchContextRunAndWait([NotNull]Func<Task> action)
        {
            try
            {
                SwitchContext();
                var task = action();
                if (task != null)
                {
                    await task.ConfigureAwait(false);
                }
            }
            finally
            {
                DisposeContext();
            }
        }

        [NotNull]
        private async Task<T> SwitchContextRunAndWait<T>([NotNull]Func<Task<T>> func)
        {
            T result;
            try
            {
                SwitchContext();
                var task = func();
                result = task != null ? await task.ConfigureAwait(false) : default(T);
            }
            finally
            {
                DisposeContext();
            }
            return result;
        }

        public Task StartNewWithContextSwitch(Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            return Task.Run(SwitchContextAndRun(action));
        }

        public Task<T> StartNewWithContextSwitch<T>(Func<T> func)
        {
            if (func == null)
            {
                throw new ArgumentNullException("func");
            }
            return Task.Run(SwitchContextAndRun(func));
        }

        public Task StartNewWithContextSwitch(Action action, CancellationToken cancellationToken)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            return Task.Run(SwitchContextAndRun(action), cancellationToken);
        }

        public Task<T> StartNewWithContextSwitch<T>(Func<T> func, CancellationToken cancellationToken)
        {
            if (func == null)
            {
                throw new ArgumentNullException("func");
            }
            return Task.Run(SwitchContextAndRun(func), cancellationToken);
        }

        public Task StartNewWithContextSwitch(Action action, TaskCreationOptions creationOptions)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            return Task.Factory.StartNew(SwitchContextAndRun(action), creationOptions);
        }

        public Task<T> StartNewWithContextSwitch<T>(Func<T> func, TaskCreationOptions creationOptions)
        {
            if (func == null)
            {
                throw new ArgumentNullException("func");
            }
            return Task.Factory.StartNew(SwitchContextAndRun(func), creationOptions);
        }

        public Task StartNewWithContextSwitch(Action action, CancellationToken cancellationToken, TaskCreationOptions creationOptions, TaskScheduler scheduler)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            if (scheduler == null)
            {
                throw new ArgumentNullException("scheduler");
            }
            return Task.Factory.StartNew(SwitchContextAndRun(action), cancellationToken, creationOptions, scheduler);
        }

        public Task<T> StartNewWithContextSwitch<T>(Func<T> func, CancellationToken cancellationToken, TaskCreationOptions creationOptions, TaskScheduler scheduler)
        {
            if (func == null)
            {
                throw new ArgumentNullException("func");
            }
            if (scheduler == null)
            {
                throw new ArgumentNullException("scheduler");
            }
            return Task.Factory.StartNew(SwitchContextAndRun(func), cancellationToken, creationOptions, scheduler);
        }

        public Task RunWithContextSwitch(Func<Task> func)
        {
            if (func == null)
            {
                throw new ArgumentNullException("func");
            }
            return SwitchContextRunAndWait(func);
        }

        public Task<T> RunWithContextSwitch<T>(Func<Task<T>> func)
        {
            if (func == null)
            {
                throw new ArgumentNullException("func");
            }
            return SwitchContextRunAndWait(func);
        }
    }
}
