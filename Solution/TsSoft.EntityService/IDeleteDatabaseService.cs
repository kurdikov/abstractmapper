﻿namespace TsSoft.EntityService
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    ///     Осуществляет удаление сущностей из бд
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    /// <typeparam name="TViewModel">Тип модели</typeparam>
    public interface IDeleteDatabaseService<TEntity, TViewModel>
    {
        /// <summary>
        ///     Удаляет сущность по предикату
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать изменения в бд</param>
        /// <returns>Модель удалённой сущности</returns>
        [CanBeNull]
        TViewModel DeleteSingle(Expression<Func<TEntity, bool>> where, bool commit = true);

        /// <summary>
        ///     Удаляет сущность по предикату
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать изменения в бд</param>
        /// <returns>Модель удалённой сущности</returns>
        [NotNull]
        [ItemCanBeNull]
        Task<TViewModel> DeleteSingleAsync(Expression<Func<TEntity, bool>> where, bool commit = true);

        /// <summary>
        ///     Удаляет сущности по предикату
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать изменения в бд</param>
        /// <returns>Модели удалённой сущности</returns>
        [NotNull]
        IEnumerable<TViewModel> Delete(Expression<Func<TEntity, bool>> where, bool commit = true);

        /// <summary>
        ///     Удаляет сущности по предикату
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit">Записать изменения в бд</param>
        /// <returns>Модели удалённой сущности</returns>
        [NotNull]
        [ItemNotNull]
        Task<IEnumerable<TViewModel>> DeleteAsync(Expression<Func<TEntity, bool>> where, bool commit = true);
    }
}
