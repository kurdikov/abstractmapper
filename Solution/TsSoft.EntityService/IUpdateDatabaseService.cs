namespace TsSoft.EntityService
{
    using System;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// ��������� ���������� ��������� �� �� �������
    /// </summary>
    /// <typeparam name="TEntity">��� ��������</typeparam>
    /// <typeparam name="TViewModel">��� ������</typeparam>
    public interface IUpdateDatabaseService<TEntity, TViewModel>
    {
        /// <summary>
        /// �������� ���� �������� � ��
        /// </summary>
        /// <param name="where">�������� ������� ��������, ������� ���� ��������</param>
        /// <param name="model">������, ������� ����� ���������</param>
        /// <param name="commit">�������� ��������� � ��</param>
        /// <returns>������ �� ���������� ��������</returns>
        [CanBeNull]
        TViewModel UpdateSingle(Expression<Func<TEntity, bool>> where, TViewModel model, bool commit = true);

        /// <summary>
        /// �������� ���� �������� � ��
        /// </summary>
        /// <param name="where">�������� ������� ��������, ������� ���� ��������</param>
        /// <param name="model">������, ������� ����� ���������</param>
        /// <param name="commit">�������� ��������� � ��</param>
        /// <returns>������ �� ���������� ��������</returns>
        [NotNull]
        [ItemCanBeNull]
        Task<TViewModel> UpdateSingleAsync(Expression<Func<TEntity, bool>> where, TViewModel model, bool commit = true);
    }
}
