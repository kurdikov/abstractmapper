namespace TsSoft.EntityService
{
    using System;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.EntityRepository;
    using TsSoft.EntityService.UpdateAction;
    using TsSoft.Expressions.IncludeBuilder;
    using TsSoft.Expressions.IncludeBuilder.Models;

    /// <summary>
    ///     ��������� ���������� ��������� �� �� �������
    /// </summary>
    /// <typeparam name="TEntity">��� ��������</typeparam>
    /// <typeparam name="TViewModel">��� ������</typeparam>
    public class UpdateDatabaseService<TEntity, TViewModel> : InternalChangeEntityDatabaseService<TEntity, TViewModel>,
        IUpdateDatabaseService<TEntity, TViewModel>
    {
        [NotNull] private readonly IUpdateRepository<TEntity> _repository;
        [NotNull] private readonly IReadOnlyIncludes<TEntity> _updateIncludes;

        /// <summary>
        ///     ��������� ���������� ��������� �� �� �������
        /// </summary>
        public UpdateDatabaseService(
            [NotNull] ISelectProviderMapper<TEntity, TViewModel> entitySelectMapper,
            [NotNull] IUpdatePathProviderMapper<TViewModel, TEntity> updateMapper,
            [NotNull] IUpdateEntityActionFactory updateEntityActionFactory,
            [NotNull] IUpdateRepository<TEntity> repository,
            [NotNull] IPathToDbIncludeConverter pathToDbIncludeMapper)
            : base(entitySelectMapper, updateMapper, updateEntityActionFactory)
        {
            if (repository == null) throw new ArgumentNullException("repository");
            if (pathToDbIncludeMapper == null) throw new ArgumentNullException("pathToDbIncludeMapper");
            _repository = repository;
            var includeProvider = updateMapper as IIncludeProvider<TEntity>;
            _updateIncludes = includeProvider != null
                ? includeProvider.Includes
                : new Includes<TEntity>(pathToDbIncludeMapper.GetIncludes(updateMapper.Paths, updateMapper.DeleteAndCreateOnPaths));
        }

        /// <summary>
        ///     �������� ���� �������� � ��
        /// </summary>
        /// <param name="where">�������� ������� ��������, ������� ���� ��������</param>
        /// <param name="model">������, ������� ����� ���������</param>
        /// <param name="commit"></param>
        /// <returns>������ �� ���������� ��������</returns>
        public TViewModel UpdateSingle(Expression<Func<TEntity, bool>> @where, TViewModel model, bool commit = true)
        {
            var updated = _repository.UpdateSingleFrom(Updater, @where, _updateIncludes, UpdateMapper.Map(model), commit);
            return EntitySelectMapper.Map(updated);
        }

        /// <summary>
        ///     �������� ���� �������� � ��
        /// </summary>
        /// <param name="where">�������� ������� ��������, ������� ���� ��������</param>
        /// <param name="model">������, ������� ����� ���������</param>
        /// <param name="commit"></param>
        /// <returns>������ �� ���������� ��������</returns>
        public async Task<TViewModel> UpdateSingleAsync(Expression<Func<TEntity, bool>> @where, TViewModel model, bool commit = true)
        {
            var updated = await _repository.UpdateSingleFromAsync(AsyncUpdater, @where, _updateIncludes, UpdateMapper.Map(model), commit)
                .ConfigureAwait(false);
            return EntitySelectMapper.Map(updated);
        }
    }
}
