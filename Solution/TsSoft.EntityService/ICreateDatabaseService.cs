﻿namespace TsSoft.EntityService
{
    using System;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    ///     Осуществляет добавление сущностей в бд по модели
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    /// <typeparam name="TViewModel">Тип модели</typeparam>
    public interface ICreateDatabaseService<out TEntity, TViewModel>
    {
        /// <summary>
        ///     Создает сущность по модели
        /// </summary>
        /// <param name="model">Модель</param>
        /// <param name="createAction">Действие, которое надо произвести с моделью перед ее вставкой в бд</param>
        /// <param name="commit">Записать изменения в бд</param>
        /// <returns>Модель по созданной сущности</returns>
        [NotNull]
        TViewModel Create(TViewModel model, Action<TEntity> createAction = null, bool commit = true);

        /// <summary>
        ///     Создает сущность по модели
        /// </summary>
        /// <param name="model">Модель</param>
        /// <param name="createAction">Действие, которое надо произвести с моделью перед ее вставкой в бд</param>
        /// <param name="commit">Записать изменения в бд</param>
        /// <returns>Модель по созданной сущности</returns>
        [NotNull]
        [ItemNotNull]
        Task<TViewModel> CreateAsync(TViewModel model, Action<TEntity> createAction = null, bool commit = true);
    }
}
