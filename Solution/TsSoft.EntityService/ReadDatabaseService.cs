namespace TsSoft.EntityService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.EntityRepository;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.Helpers.Collections;
    using TsSoft.Expressions.OrderBy;

    /// <summary>
    ///     ��������� ��������� ��������� � ������ �� � ������
    /// </summary>
    /// <typeparam name="TEntity">��� ��������</typeparam>
    /// <typeparam name="TViewModel">��� ������</typeparam>
    public class ReadDatabaseService<TEntity, TViewModel> : InternalDatabaseService<TEntity, TViewModel>,
        IReadDatabaseService<TEntity, TViewModel>
    {
        [NotNull] private readonly ICollectionHelper _collectionHelper;
        [NotNull] private readonly IReadRepository<TEntity> _repository;

        /// <summary>
        ///     ��������� ��������� ��������� � ������ �� � ������
        /// </summary>
        public ReadDatabaseService(
            [NotNull] ISelectProviderMapper<TEntity, TViewModel> entitySelectMapper,
            [NotNull] IReadRepository<TEntity> repository,
            [NotNull] ICollectionHelper collectionHelper)
            : base(entitySelectMapper)
        {
            if (repository == null) throw new ArgumentNullException("repository");
            if (collectionHelper == null) throw new ArgumentNullException("collectionHelper");
            _repository = repository;
            _collectionHelper = collectionHelper;
        }

        /// <summary>
        ///     �������� ��������� ��������� �� ��
        ///     � ������� ������������ ���������, ������������ � ������
        /// </summary>
        /// <param name="where">�������� ��������� ���������</param>
        /// <param name="orderBy">���������� ���������</param>
        /// <returns>������������ �������</returns>
        public IEnumerable<TViewModel> Get(Expression<Func<TEntity, bool>> @where,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            var collection = _repository.Get(@where, EntitySelectMapper.Select, orderBy);
            return _collectionHelper.NotNull(collection).Select(EntitySelectMapper.Map);
        }

        /// <summary>
        ///     �������� �������������� ��������� ��� view-�������
        /// </summary>
        /// <param name="where">�������� � ��������</param>
        /// <param name="pageNumber">����� ��������</param>
        /// <param name="pageSize">������ ��������</param>
        /// <param name="orderBy">����������</param>
        /// <returns>�������������� ���������</returns>
        public IPage<TViewModel> GetPaged(Expression<Func<TEntity, bool>> @where, int pageNumber,
            int pageSize,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            var pagedResult = _repository.GetPaged(
                where,
                pageNumber,
                pageSize,
                EntitySelectMapper.Select,
                orderBy);
            return new Page<TViewModel>(
                pagedResult.TotalRecords,
                pagedResult.Result.Select(EntitySelectMapper.Map));
        }

        /// <summary>
        ///     �������� ���� �������� �� �� � ������� � ������
        /// </summary>
        /// <param name="where">�������� ��������� ��������</param>
        /// <returns>������</returns>
        public TViewModel GetSingle(Expression<Func<TEntity, bool>> @where)
        {
            return EntitySelectMapper.Map(
                _repository.GetSingle(@where, EntitySelectMapper.Select));
        }

        /// <summary>
        ///     �������� ��������� ��������� �� ��
        ///     � ������� ������������ ���������, ������������ � ������
        /// </summary>
        /// <param name="where">�������� ��������� ���������</param>
        /// <param name="orderBy">���������� ���������</param>
        /// <returns>������, ����������� ���������� ������� ����� ������������ �������</returns>
        public async Task<IEnumerable<TViewModel>> GetAsync(Expression<Func<TEntity, bool>> @where,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            var collection =
                await _repository.GetAsync(@where, EntitySelectMapper.Select, orderBy).ConfigureAwait(false);
            return _collectionHelper.NotNull(collection).Select(EntitySelectMapper.Map);
        }

        /// <summary>
        ///     �������� ���� �������� �� �� � ������� � ������
        /// </summary>
        /// <param name="where">�������� ��������� ��������</param>
        /// <returns>������, ����������� ���������� ������� ����� ������</returns>
        public async Task<TViewModel> GetSingleAsync(Expression<Func<TEntity, bool>> @where)
        {
            return EntitySelectMapper.Map(
                await _repository.GetSingleAsync(@where, EntitySelectMapper.Select).ConfigureAwait(false));
        }

        /// <summary>
        ///     �������� �������������� ��������� ��� view-�������
        /// </summary>
        /// <param name="where">�������� � ��������</param>
        /// <param name="pageNumber">����� ��������</param>
        /// <param name="pageSize">������ ��������</param>
        /// <param name="orderBy">����������</param>
        /// <returns>������, ����������� ���������� ������� ����� �������� ����������</returns>
        public async Task<IPage<TViewModel>> GetPagedAsync(Expression<Func<TEntity, bool>> @where,
            int pageNumber, int pageSize,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            var pagedResult = await _repository.GetPagedAsync(
                where,
                pageNumber,
                pageSize,
                EntitySelectMapper.Select,
                orderBy).ConfigureAwait(false);
            return new Page<TViewModel>(
                pagedResult.TotalRecords,
                pagedResult.Result.Select(EntitySelectMapper.Map));
        }
    }
}
