﻿namespace TsSoft.EntityService.UpdateAction
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Helpers;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Управляет обновлением сущностей
    /// </summary>
    public class EntityUpdateManager : IEntityUpdateManager
    {
        [NotNull]private readonly ICollectionRemover _collectionRemover;
        [NotNull]private readonly IEntityTypesRetriever _entityTypesRetriever;
        [NotNull]private readonly IEntityTypesHelper _entityTypesHelper;
        [NotNull]private readonly IKeyExpressionBuilder _keyExpressionHelper;

        [NotNull]private readonly MethodInfo _removeCollection;
        [NotNull]private readonly MethodInfo _removeWholeCollection;
        [NotNull]private readonly MethodInfo _removeCollectionAsync;
        [NotNull]private readonly MethodInfo _removeWholeCollectionAsync;

        /// <summary>
        /// Управляет обновлением сущностей
        /// </summary>
        public EntityUpdateManager(
            [NotNull] IMemberInfoHelper memberInfoHelper,
            [NotNull] IEntityTypesRetriever entityTypesRetriever,
            [NotNull] IEntityTypesHelper entityTypesHelper,
            [NotNull] ICollectionRemover collectionRemover,
            [NotNull] IKeyExpressionBuilder keyExpressionHelper)
        {
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (entityTypesRetriever == null) throw new ArgumentNullException("entityTypesRetriever");
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            if (collectionRemover == null) throw new ArgumentNullException("collectionRemover");
            if (keyExpressionHelper == null) throw new ArgumentNullException("keyExpressionHelper");
            _entityTypesRetriever = entityTypesRetriever;
            _entityTypesHelper = entityTypesHelper;
            _collectionRemover = collectionRemover;
            _keyExpressionHelper = keyExpressionHelper;

            _removeCollection = memberInfoHelper.GetGenericDefinitionMethodInfo(
                (ICollectionRemover r) => r.RemoveEntities<object, object>(null, null, null));
            _removeWholeCollection = memberInfoHelper.GetGenericDefinitionMethodInfo(
                (ICollectionRemover r) => r.RemoveEntities<object>(null));
            _removeCollectionAsync = memberInfoHelper.GetGenericDefinitionMethodInfo(
                (ICollectionRemover r) => r.RemoveEntitiesAsync<object, object>(null, null, null));
            _removeWholeCollectionAsync = memberInfoHelper.GetGenericDefinitionMethodInfo(
                (ICollectionRemover r) => r.RemoveEntitiesAsync<object>(null));
        }

        /// <summary>
        /// Является ли примитивным значением (т.е. столбцом)
        /// </summary>
        public bool IsPrimitive([NotNull] Type type)
        {
            return type.GetTypeInfo().IsValueType || type == typeof(string) || type.IsNullableStruct();
        }

        /// <summary>
        /// Является ли навигационной коллекцией
        /// </summary>
        public bool IsCollection([NotNull] Type type)
        {
            return _entityTypesRetriever.IsEntityCollectionType(type);
        }

        /// <summary>
        /// Является ли навигационным свойством
        /// </summary>
        public bool IsObject([NotNull] Type type)
        {
            return _entityTypesRetriever.IsEntityType(type);
        }

        [NotNull]
        private Expression MakeRemoveCollectionExpression(
            [NotNull]ValueHoldingMember collectionProperty, 
            [NotNull]Type objectType,
            [NotNull]Expression target, 
            bool async)
        {
            var foreignKey = _entityTypesHelper.GetInverseForeignKey(collectionProperty);
            var containerPk = _entityTypesHelper.GetPrimaryKey(target.Type);
            if (foreignKey.Count != containerPk.Count)
            {
                throw new InvalidOperationException(string.Format(
                    "Key column number mismatch for primary key of {0} and inverse foreign key for {1} from {2}",
                    target.Type,
                    collectionProperty,
                    collectionProperty.DeclaringType));
            }
            var fkValueVariables = new ParameterExpression[containerPk.Count];
            var expressions = new List<Expression>(containerPk.Count + 1);
            Expression expr = null;
            var deleteParam = Expression.Parameter(objectType);
            for (int i = 0; i < containerPk.Count; ++i)
            {
                if (containerPk[i] == null || foreignKey[i] == null)
                {
                    throw new NullReferenceException(string.Format("Null key column number {0} for primary key of {1} and inverse foreign key for {2} from {3}",
                        i, target.Type, collectionProperty, collectionProperty.DeclaringType));
                }
                var fkValueVar = Expression.Variable(foreignKey[i].ValueType);
                fkValueVariables[i] = fkValueVar;
                var fkValueAssign = Expression.Assign(fkValueVar, Expression.MakeMemberAccess(target, containerPk[i].Member));
                expressions.Add(fkValueAssign);
                var equals = Expression.Equal(Expression.MakeMemberAccess(deleteParam, foreignKey[i].Member), fkValueVar);
                expr = expr != null ? Expression.AndAlso(expr, equals) : equals;
            }
            if (expr == null)
            {
                throw new InvalidOperationException(string.Format(
                    "No key columns for primary key of {0} and inverse foreign key for {1} from {2}",
                    target.Type,
                    collectionProperty,
                    collectionProperty.DeclaringType));
            }
            var deleteLambda = Expression.Lambda(typeof(Func<,>).MakeGenericType(objectType, typeof(bool)),
                expr,
                deleteParam);
            var method = async ? _removeWholeCollectionAsync : _removeWholeCollection;
            var deleteMethod = method.MakeGenericMethod(objectType);
            var deleteExpr = Expression.Call(
                Expression.Constant(_collectionRemover, typeof(ICollectionRemover)),
                deleteMethod,
                new Expression[] { deleteLambda });
            expressions.Add(deleteExpr);
            return Expression.Block(fkValueVariables, expressions);
        }

        /// <summary>
        /// Собрать выражение для удаления коллекции
        /// </summary>
        public Expression MakeRemoveCollectionExpression(ValueHoldingMember collectionProperty, Type objectType, Expression target)
        {
            return MakeRemoveCollectionExpression(collectionProperty, objectType, target, false);
        }

        /// <summary>
        /// Собрать выражение для удаления коллекции
        /// </summary>
        public Expression MakeAsyncRemoveCollectionExpression(ValueHoldingMember collectionProperty, Type objectType, Expression target)
        {
            return MakeRemoveCollectionExpression(collectionProperty, objectType, target, true);
        }

        [NotNull]
        private Expression MakeRemoveCollectionPartExpression([NotNull]Expression source, [NotNull]Expression target, bool async)
        {
            var elementType = source.Type.GetGenericEnumerableArgument();
            var keySelectorParameter = Expression.Parameter(elementType, "p");
            var extractedKey = MakeExtractKeyExpression(keySelectorParameter);
            var keyType = extractedKey.Type;
            var keySelector = Expression.Lambda(
                typeof(Func<,>).MakeGenericType(elementType, keyType),
                extractedKey,
                keySelectorParameter).Compile();

            var method = async ? _removeCollectionAsync : _removeCollection;
            var removeCollection = method.MakeGenericMethod(elementType, keyType);
            return Expression.Call(
                Expression.Constant(_collectionRemover, typeof(ICollectionRemover)),
                removeCollection,
                target,
                source,
                Expression.Constant(keySelector));
        }

        /// <summary>
        /// Собрать выражение для удаления части коллекции
        /// </summary>
        public Expression MakeRemoveCollectionPartExpression(Expression source, Expression target)
        {
            return MakeRemoveCollectionPartExpression(source, target, false);
        }

        /// <summary>
        /// Собрать выражение для удаления части коллекции
        /// </summary>
        public Expression MakeAsyncRemoveCollectionPartExpression(Expression source, Expression target)
        {
            return MakeRemoveCollectionPartExpression(source, target, true);
        }

        /// <summary>
        /// Собрать выражение для выделения ключа сущности, по которому происходит сопоставление в коллекциях
        /// </summary>
        public Expression MakeExtractKeyExpression(Expression obj)
        {
            var key = _entityTypesHelper.GetPrimaryKey(obj.Type);
            return _keyExpressionHelper.CreateExtractKeyExpression(obj, key);
        }
    }
}
