﻿namespace TsSoft.EntityService.UpdateAction
{
    using TsSoft.Expressions.Models.Reflection;

    /// <summary>
    /// Настройки обновления свойств сущности БД
    /// </summary>
    public interface IEntityUpdateManager : IObjectUpdateManager
    {
    }
}
