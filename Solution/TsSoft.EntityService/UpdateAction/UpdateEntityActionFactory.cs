﻿namespace TsSoft.EntityService.UpdateAction
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Exceptions;

    internal class UpdateEntityActionFactory : IUpdateEntityActionFactory
    {
        [NotNull]private readonly IUpdateObjectActionFactory _updateObjectActionFactory;
        [NotNull] private readonly IEntityUpdateManager _entityUpdateManager;

        public UpdateEntityActionFactory([NotNull] IUpdateObjectActionFactory updateObjectActionFactory, [NotNull] IEntityUpdateManager entityUpdateManager)
        {
            if (updateObjectActionFactory == null) throw new ArgumentNullException("updateObjectActionFactory");
            if (entityUpdateManager == null) throw new ArgumentNullException("entityUpdateManager");
            _updateObjectActionFactory = updateObjectActionFactory;
            _entityUpdateManager = entityUpdateManager;
        }

        public Action<T, T> MakeUpdateAction<T>(
            IEnumerable<Expression<Func<T, object>>> paths,
            IEnumerable<Expression<Func<T, object>>> dropCreateOnPaths = null)
        {
            try
            {
                return _updateObjectActionFactory.MakeUpdateAction(paths, _entityUpdateManager, dropCreateOnPaths);
            }
            catch (UnableToBuildActionException e)
            {
                if (!e.IsKnown)
                {
                    throw new InvalidOperationException(
                        string.Format(
                            "Unable to build an update delegate for {0}: unknown type {1}. Check that your IEntityUpdateManager implementation correctly handles {1} (by default it delegates to IEntityTypesRetriever and its IsEntityType and IsEntityCollectionType methods).",
                            typeof(T),
                            e.InvalidType),
                        e);
                }
                throw;
            }
        }


        public Func<T, T, IEnumerable<Func<Task>>> MakeAsyncUpdateAction<T>(
            IEnumerable<Expression<Func<T, object>>> paths, 
            IEnumerable<Expression<Func<T, object>>> dropCreateOnPaths = null)
        {
            try
            {
                return _updateObjectActionFactory.MakeAsyncUpdateAction(paths, _entityUpdateManager, dropCreateOnPaths);
            }
            catch (UnableToBuildActionException e)
            {
                if (!e.IsKnown)
                {
                    throw new InvalidOperationException(
                        string.Format(
                            "Unable to build an update delegate for {0}: unknown type {1}. Check that your IEntityUpdateManager implementation correctly handles {1} (by default it delegates to IEntityTypesRetriever and its IsEntityType and IsEntityCollectionType methods).",
                            typeof(T),
                            e.InvalidType),
                        e);
                }
                throw;
            }
        }
    }
}
