﻿namespace TsSoft.EntityService.UpdateAction
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// Фабрика обновляторов
    /// </summary>
    public interface IUpdateEntityActionFactory
    {
        /// <summary>
        /// Построить функцию-обновлятор
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="paths">Обновляемые пути</param>
        /// <param name="dropCreateOnPaths">На каких путях не производить слияния, а удалять и пересоздавать сущности</param>
        /// <returns></returns>
        [NotNull]
        Action<T, T> MakeUpdateAction<T>(
            [NotNull]IEnumerable<Expression<Func<T, object>>> paths,
            IEnumerable<Expression<Func<T, object>>> dropCreateOnPaths = null);

        /// <summary>
        /// Построить асинхронную обновляющую функцию
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="paths">Обновляемые пути</param>
        /// <param name="dropCreateOnPaths">На каких путях не производить слияния, а удалять и пересоздавать сущности</param>
        /// <returns>Функция, обновляющая объект и возвращающая набор производителей дополнительных задач</returns>
        [NotNull]
        Func<T, T, IEnumerable<Func<Task>>> MakeAsyncUpdateAction<T>(
            [NotNull]IEnumerable<Expression<Func<T, object>>> paths,
            IEnumerable<Expression<Func<T, object>>> dropCreateOnPaths = null);
    }
}
