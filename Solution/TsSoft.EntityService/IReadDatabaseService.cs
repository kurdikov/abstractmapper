﻿namespace TsSoft.EntityService
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.OrderBy;

    /// <summary>
    ///     Реализует получение сущностей и мапинг их в модели
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    /// <typeparam name="TViewModel">Тип модели</typeparam>
    public interface IReadDatabaseService<TEntity, TViewModel>
    {
        /// <summary>
        ///     Получить коллекцию сущностей из бд
        ///     и вернуть перечисление сущностей, размапленных в модели
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="orderBy">Сортировка сущностей</param>
        /// <returns>Перечисление моделей</returns>
        [NotNull]
        IEnumerable<TViewModel> Get(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null);

        /// <summary>
        ///     Получить пагинированный результат для view-моделей
        /// </summary>
        /// <param name="where">Предикат к сущности</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Размер страницы</param>
        /// <param name="orderBy">Сортировка</param>
        /// <returns>Пагинированный результат</returns>
        [NotNull]
        IPage<TViewModel> GetPaged(
            Expression<Func<TEntity, bool>> where,
            int pageNumber,
            int pageSize,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null);

        /// <summary>
        ///     Получить одну сущность из бд и смапить в модель
        /// </summary>
        /// <param name="where">Предикат получения сущности</param>
        /// <returns>Модель</returns>
        [CanBeNull]
        TViewModel GetSingle(Expression<Func<TEntity, bool>> where);

        /// <summary>
        ///     Получить коллекцию сущностей из бд
        ///     и вернуть перечисление сущностей, размапленных в модели
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="orderBy">Сортировка сущностей</param>
        /// <returns>Задача, результатом выполнения которой будет перечисление моделей</returns>
        [NotNull]
        [ItemNotNull]
        Task<IEnumerable<TViewModel>> GetAsync(
            Expression<Func<TEntity, bool>> where,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null);

        /// <summary>
        ///     Получить одну сущность из бд и смапить в модель
        /// </summary>
        /// <param name="where">Предикат получения сущности</param>
        /// <returns>Задача, результатом выполнения которой будет модель</returns>
        [NotNull]
        [ItemCanBeNull]
        Task<TViewModel> GetSingleAsync(Expression<Func<TEntity, bool>> where);

        /// <summary>
        ///     Получить пагинированный результат для view-моделей
        /// </summary>
        /// <param name="where">Предикат к сущности</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Размер страницы</param>
        /// <param name="orderBy">Сортировка</param>
        /// <returns>Задача, результатом выполнения которой будет страница результата</returns>
        [NotNull]
        [ItemNotNull]
        Task<IPage<TViewModel>> GetPagedAsync(
            Expression<Func<TEntity, bool>> where,
            int pageNumber,
            int pageSize,
            IEnumerable<IOrderByClause<TEntity>> orderBy = null);
    }
}
