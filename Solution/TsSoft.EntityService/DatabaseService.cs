﻿namespace TsSoft.EntityService
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Models;
    using TsSoft.Expressions.OrderBy;

    /// <summary>
    ///     Сервис для получения и обновления сущностей БД с автоматическим маппингом
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    /// <typeparam name="TViewModel">Тип модели</typeparam>
    public class DatabaseService<TEntity, TViewModel> :
        IDatabaseService<TEntity, TViewModel>
        where TEntity : class, new()
    {
        [NotNull] private readonly IReadDatabaseService<TEntity, TViewModel> _readDatabaseService;
        [NotNull] private readonly ICreateDatabaseService<TEntity, TViewModel> _createDatabaseService;
        [NotNull] private readonly IUpdateDatabaseService<TEntity, TViewModel> _updateDatabaseService;
        [NotNull] private readonly IDeleteDatabaseService<TEntity, TViewModel> _deleteDatabaseService;

        /// <summary>
        ///     Сервис для получения и обновления сущностей БД с автоматическим маппингом
        /// </summary>
        public DatabaseService(
            [NotNull] IReadDatabaseService<TEntity, TViewModel> readDatabaseService,
            [NotNull] ICreateDatabaseService<TEntity, TViewModel> createDatabaseService,
            [NotNull] IUpdateDatabaseService<TEntity, TViewModel> updateDatabaseService,
            [NotNull] IDeleteDatabaseService<TEntity, TViewModel> deleteDatabaseService)
        {
            if (readDatabaseService == null) throw new ArgumentNullException("readDatabaseService");
            if (createDatabaseService == null) throw new ArgumentNullException("createDatabaseService");
            if (updateDatabaseService == null) throw new ArgumentNullException("updateDatabaseService");
            if (deleteDatabaseService == null) throw new ArgumentNullException("deleteDatabaseService");
            _readDatabaseService = readDatabaseService;
            _createDatabaseService = createDatabaseService;
            _updateDatabaseService = updateDatabaseService;
            _deleteDatabaseService = deleteDatabaseService;
        }

        /// <summary>
        ///     Получить коллекцию сущностей из бд
        ///     и вернуть перечисление сущностей, размапленных в модели
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="orderBy">Сортировка сущностей</param>
        /// <returns>Перечисление моделей</returns>
        public IEnumerable<TViewModel> Get(Expression<Func<TEntity, bool>> @where, IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return _readDatabaseService.Get(@where, orderBy);
        }

        /// <summary>
        ///     Получить пагинированный результат для view-моделей
        /// </summary>
        /// <param name="where">Предикат к сущности</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Размер страницы</param>
        /// <param name="orderBy">Сортировка</param>
        /// <returns>Пагинированный результат</returns>
        public IPage<TViewModel> GetPaged(Expression<Func<TEntity, bool>> @where, int pageNumber, int pageSize, IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return _readDatabaseService.GetPaged(@where, pageNumber, pageSize, orderBy);
        }

        /// <summary>
        ///     Получить одну сущность из бд и смапить в модель
        /// </summary>
        /// <param name="where">Предикат получения сущности</param>
        /// <returns>Модель</returns>
        public TViewModel GetSingle(Expression<Func<TEntity, bool>> @where)
        {
            return _readDatabaseService.GetSingle(@where);
        }

        /// <summary>
        ///     Получить коллекцию сущностей из бд
        ///     и вернуть перечисление сущностей, размапленных в модели
        /// </summary>
        /// <param name="where">Предикат получения сущностей</param>
        /// <param name="orderBy">Сортировка сущностей</param>
        /// <returns>Задача, результатом выполнения которой будет перечисление моделей</returns>
        public Task<IEnumerable<TViewModel>> GetAsync(Expression<Func<TEntity, bool>> @where, IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return _readDatabaseService.GetAsync(@where, orderBy);
        }

        /// <summary>
        ///     Получить одну сущность из бд и смапить в модель
        /// </summary>
        /// <param name="where">Предикат получения сущности</param>
        /// <returns>Задача, результатом выполнения которой будет модель</returns>
        public Task<TViewModel> GetSingleAsync(Expression<Func<TEntity, bool>> @where)
        {
            return _readDatabaseService.GetSingleAsync(@where);
        }

        /// <summary>
        ///     Получить пагинированный результат для view-моделей
        /// </summary>
        /// <param name="where">Предикат к сущности</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <param name="pageSize">Размер страницы</param>
        /// <param name="orderBy">Сортировка</param>
        /// <returns>Задача, результатом выполнения которой будет страница результата</returns>
        public Task<IPage<TViewModel>> GetPagedAsync(Expression<Func<TEntity, bool>> @where, int pageNumber, int pageSize, IEnumerable<IOrderByClause<TEntity>> orderBy = null)
        {
            return _readDatabaseService.GetPagedAsync(@where, pageNumber, pageSize, orderBy);
        }

        /// <summary>
        /// Обновить одну сущность в бд
        /// </summary>
        /// <param name="where">Предикат выборки сущности, которую надо обновить</param>
        /// <param name="model">Модель, которой будем обновлять</param>
        /// <param name="commit"></param>
        /// <returns>Модель по обновлённой сущности</returns>
        public TViewModel UpdateSingle(Expression<Func<TEntity, bool>> @where, TViewModel model, bool commit)
        {
            return _updateDatabaseService.UpdateSingle(@where, model, commit);
        }

        /// <summary>
        /// Обновить одну сущность в бд
        /// </summary>
        /// <param name="where">Предикат выборки сущности, которую надо обновить</param>
        /// <param name="model">Модель, которой будем обновлять</param>
        /// <param name="commit"></param>
        /// <returns>Модель по обновлённой сущности</returns>
        public Task<TViewModel> UpdateSingleAsync(Expression<Func<TEntity, bool>> @where, TViewModel model, bool commit)
        {
            return _updateDatabaseService.UpdateSingleAsync(@where, model, commit);
        }

        /// <summary>
        ///     Создает сущность по модели
        /// </summary>
        /// <param name="model">Модель</param>
        /// <param name="createAction">Действие, которое надо произвести с моделью перед ее вставкой в бд</param>
        /// <param name="commit"></param>
        /// <returns>Модель по созданной сущности</returns>
        public TViewModel Create(TViewModel model, Action<TEntity> createAction = null, bool commit = true)
        {
            return _createDatabaseService.Create(model, createAction, commit);
        }

        /// <summary>
        ///     Создает сущность по модели
        /// </summary>
        /// <param name="model">Модель</param>
        /// <param name="createAction">Действие, которое надо произвести с моделью перед ее вставкой в бд</param>
        /// <param name="commit"></param>
        /// <returns>Модель по созданной сущности</returns>
        public Task<TViewModel> CreateAsync(TViewModel model, Action<TEntity> createAction = null, bool commit = true)
        {
            return _createDatabaseService.CreateAsync(model, createAction, commit);
        }

        /// <summary>
        ///     Удаляет сущность по предикату
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit"></param>
        /// <returns>Модель по удалённой сущности</returns>
        public TViewModel DeleteSingle(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return _deleteDatabaseService.DeleteSingle(@where, commit);
        }

        /// <summary>
        ///     Удаляет сущность по предикату
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit"></param>
        /// <returns>Модель по удалённой сущности</returns>
        public Task<TViewModel> DeleteSingleAsync(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return _deleteDatabaseService.DeleteSingleAsync(@where, commit);
        }

        /// <summary>
        ///     Удаляет сущности по предикату
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit"></param>
        /// <returns>Модели удалённой сущности</returns>
        public IEnumerable<TViewModel> Delete(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return _deleteDatabaseService.Delete(@where, commit);
        }

        /// <summary>
        ///     Удаляет сущности по предикату
        /// </summary>
        /// <param name="where">Предикат</param>
        /// <param name="commit"></param>
        /// <returns>Модели удалённой сущности</returns>
        public Task<IEnumerable<TViewModel>> DeleteAsync(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            return _deleteDatabaseService.DeleteAsync(@where, commit);
        }
    }
}