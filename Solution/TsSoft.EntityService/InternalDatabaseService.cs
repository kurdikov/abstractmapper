namespace TsSoft.EntityService
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;

    /// <summary>
    /// ������� ����� DatabaseServices
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TViewModel"></typeparam>
    public abstract class InternalDatabaseService<TEntity, TViewModel>
    {
        /// <summary>
        /// ������, �������� ����������� ��� ���������� ������ ������
        /// </summary>
        [NotNull]
        protected readonly ISelectProviderMapper<TEntity, TViewModel> EntitySelectMapper;
       
        /// <summary>
        /// ������� ����� DatabaseServices
        /// </summary>
        protected InternalDatabaseService([NotNull] ISelectProviderMapper<TEntity, TViewModel> entitySelectMapper)
        {
            if (entitySelectMapper == null) throw new ArgumentNullException("entitySelectMapper");
            EntitySelectMapper = entitySelectMapper;
        }
    }
}