﻿namespace TsSoft.EntityService.Mapper
{
    using TsSoft.AbstractMapper;
    using TsSoft.Expressions.IncludeBuilder.Models;

    /// <summary>
    /// Интерфейс маппера, обновляющего сущности
    /// </summary>
    /// <typeparam name="TFrom">Тип, из которого происходит преобразование</typeparam>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    public interface IUpdateEntityMapper<in TFrom, TTo> : IUpdateFuncProviderMapper<TFrom, TTo>, IIncludeProvider<TTo>
    {
    }
}
