﻿namespace TsSoft.EntityService.Mapper
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models.Reflection;

    /// <summary>
    /// Абстрактный маппер, отдающий делегат для обновления сущности в базе данных
    /// </summary>
    /// <typeparam name="TFrom">Тип, из которого происходит преобразование</typeparam>
    /// <typeparam name="TTo">Тип, в который происходит преобразование</typeparam>
    public class UpdateEntityMapper<TFrom, TTo> : UpdateFuncProviderMapper<TFrom, TTo>, IUpdateEntityMapper<TFrom, TTo>
        where TFrom : class 
        where TTo : class, new()
    {
        [NotNull]
        private readonly IObjectUpdateManager _updateManager;

        [NotNull] private readonly Lazy<Includes<TTo>> _includes;

        /// <summary>
        /// Управляющий обновлением объект
        /// </summary>
        protected override IObjectUpdateManager UpdateManager
        {
            get { return _updateManager; }
        }

        /// <summary>
        /// Абстрактный маппер, отдающий делегат для обновления
        /// </summary>
        public UpdateEntityMapper([NotNull] IAbstractUpdateEntityMapperHelper mapperHelper)
            : base(mapperHelper)
        {
            _updateManager = mapperHelper.GetEntityUpdateManager();
            _includes = new Lazy<Includes<TTo>>(() => mapperHelper.GetIncludes(Paths, DeleteAndCreateOnPaths));
        }

        /// <summary>
        /// Вся ли необходимая информация получена из построенного выражения
        /// </summary>
        protected override bool IsExpressionProcessed
        {
            get { return base.IsExpressionProcessed && _includes.IsValueCreated; }
        }

        /// <summary>
        /// Необходимые для обновления инклюды
        /// </summary>
        public virtual IReadOnlyIncludes<TTo> Includes
        {
            get { return GetLazyValueAndClearCacheIfProcessed(_includes); }
        }
    }
}
