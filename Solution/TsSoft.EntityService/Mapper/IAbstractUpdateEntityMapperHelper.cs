﻿namespace TsSoft.EntityService.Mapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.EntityService.UpdateAction;
    using TsSoft.Expressions.IncludeBuilder.Models;

    /// <summary>
    /// Хелпер для абстрактного маппера, отдающего делегат обновления
    /// </summary>
    public interface IAbstractUpdateEntityMapperHelper : IAbstractUpdateFuncProviderMapperHelper
    {
        /// <summary>
        /// Получить управляющий обновлением сущности объект
        /// </summary>
        [NotNull]
        IEntityUpdateManager GetEntityUpdateManager();

        /// <summary>
        /// Получить инклюды по обновляемым путям
        /// </summary>
        [NotNull]
        Includes<T> GetIncludes<T>(
            [NotNull]IEnumerable<Expression<Func<T, object>>> paths,
            [NotNull]IEnumerable<Expression<Func<T, object>>> deleteAndCreateOnPaths);
    }
}
