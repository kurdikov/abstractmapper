﻿namespace TsSoft.EntityService.Mapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.AbstractMapper.Engine;
    using TsSoft.AbstractMapper.Rules.Factories;
    using TsSoft.EntityService.UpdateAction;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Closures;
    using TsSoft.Expressions.IncludeBuilder;
    using TsSoft.Expressions.IncludeBuilder.Models;

    class AbstractUpdateEntityMapperHelper : AbstractUpdateFuncProviderMapperHelper, IAbstractUpdateEntityMapperHelper
    {
        [NotNull] private readonly IEntityUpdateManager _entityUpdateManager;
        [NotNull] private readonly IPathToDbIncludeConverter _pathToDbIncludeConverter;

        public AbstractUpdateEntityMapperHelper(
            [NotNull] IMapExpressionCreator expressionCreator,
            [NotNull] IIgnoreRulesFactory ignoreRulesFactory,
            [NotNull] ILambdaCompiler lambdaCompiler,
            [NotNull] IFullMapRulesCreator fullMapRulesCreator,
            [NotNull] IUpdatePathsCreator updateCreator,
            [NotNull] IUpdateObjectActionFactory updateObjectActionFactory,
            [NotNull] IEntityUpdateManager entityUpdateManager,
            [NotNull] IPathToDbIncludeConverter pathToDbIncludeConverter)
            : base(expressionCreator, ignoreRulesFactory, lambdaCompiler, fullMapRulesCreator, updateCreator, updateObjectActionFactory)
        {
            if (entityUpdateManager == null) throw new ArgumentNullException("entityUpdateManager");
            if (pathToDbIncludeConverter == null) throw new ArgumentNullException("pathToDbIncludeConverter");
            _entityUpdateManager = entityUpdateManager;
            _pathToDbIncludeConverter = pathToDbIncludeConverter;
        }

        public IEntityUpdateManager GetEntityUpdateManager()
        {
            return _entityUpdateManager;
        }

        public Includes<T> GetIncludes<T>(
            IEnumerable<Expression<Func<T, object>>> paths,
            IEnumerable<Expression<Func<T, object>>> deleteAndCreateOnPaths)
        {
            return new Includes<T>(_pathToDbIncludeConverter.GetIncludes(paths, deleteAndCreateOnPaths));
        }
    }
}
