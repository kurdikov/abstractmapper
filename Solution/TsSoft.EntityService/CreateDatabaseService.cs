namespace TsSoft.EntityService
{
    using System;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.EntityRepository;
    using TsSoft.EntityService.UpdateAction;

    /// <summary>
    ///     ������������ ���������� ��������� � �� �� ������
    /// </summary>
    /// <typeparam name="TEntity">��� ��������</typeparam>
    /// <typeparam name="TViewModel">��� ������</typeparam>
    public class CreateDatabaseService<TEntity, TViewModel> : InternalChangeEntityDatabaseService<TEntity, TViewModel>,
        ICreateDatabaseService<TEntity, TViewModel>
        where TEntity : class, new()
    {
        [NotNull] private readonly ICreateRepository<TEntity> _createRepository;

        /// <summary>
        ///     ������������ ���������� ��������� � �� �� ������
        /// </summary>
        public CreateDatabaseService(
            [NotNull] ISelectProviderMapper<TEntity, TViewModel> entitySelectMapper,
            [NotNull] IUpdatePathProviderMapper<TViewModel, TEntity> updateMapper,
            [NotNull] IUpdateEntityActionFactory updateEntityActionFactory, 
            [NotNull] ICreateRepository<TEntity> createRepository)
            : base(entitySelectMapper, updateMapper, updateEntityActionFactory)
        {
            if (createRepository == null) throw new ArgumentNullException("createRepository");
            _createRepository = createRepository;
        }

        [NotNull]
        private TEntity CreateEntity(TViewModel model, Action<TEntity> createAction)
        {
            var entity = new TEntity();
            if (createAction != null)
            {
                createAction(entity);
            }
            Updater(UpdateMapper.Map(model), entity);
            return entity;
        }

        /// <summary>
        ///     ������� �������� �� ������
        /// </summary>
        /// <param name="model">������</param>
        /// <param name="createAction">��������, ������� ���� ���������� � ������� ����� �� �������� � ��</param>
        /// <param name="commit"></param>
        /// <returns>������ �� ��������� ��������</returns>
        public TViewModel Create(TViewModel model, Action<TEntity> createAction = null, bool commit = true)
        {
            var entity = CreateEntity(model, createAction);
            entity = _createRepository.Create(entity, commit);
            return EntitySelectMapper.Map(entity);
        }

        /// <summary>
        ///     ������� �������� �� ������
        /// </summary>
        /// <param name="model">������</param>
        /// <param name="createAction">��������, ������� ���� ���������� � ������� ����� �� �������� � ��</param>
        /// <param name="commit"></param>
        /// <returns>������ �� ��������� ��������</returns>
        public async Task<TViewModel> CreateAsync(TViewModel model, Action<TEntity> createAction = null, bool commit = true)
        {
            var entity = CreateEntity(model, createAction);
            entity = await _createRepository.CreateAsync(entity, commit).ConfigureAwait(false);
            return EntitySelectMapper.Map(entity);
        }
    }
}
