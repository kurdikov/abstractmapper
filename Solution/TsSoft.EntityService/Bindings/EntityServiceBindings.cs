﻿namespace TsSoft.Bindings
{
    using TsSoft.BindingsDescription;
    using TsSoft.EntityService;
    using TsSoft.EntityService.Mapper;
    using TsSoft.EntityService.UpdateAction;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.EntityService
    /// </summary>
    public class EntityServiceBindings : BindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке TsSoft.EntityService
        /// </summary>
        public EntityServiceBindings()
        {
            Bindings.Add(typeof(ICreateDatabaseService<,>), typeof(CreateDatabaseService<,>));
            Bindings.Add(typeof(IReadDatabaseService<,>), typeof(ReadDatabaseService<,>));
            Bindings.Add(typeof(IUpdateDatabaseService<,>), typeof(UpdateDatabaseService<,>));
            Bindings.Add(typeof(IDeleteDatabaseService<,>), typeof(DeleteDatabaseService<,>));
            Bindings.Add(typeof(IDatabaseService<,>), typeof (DatabaseService<,>));
            Bind<IUpdateEntityActionFactory, UpdateEntityActionFactory>();
            Bind<IEntityUpdateManager, EntityUpdateManager>();
            Bind<IAbstractUpdateEntityMapperHelper, AbstractUpdateEntityMapperHelper>();
        }
    }
}