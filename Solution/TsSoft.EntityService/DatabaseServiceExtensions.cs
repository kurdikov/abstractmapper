﻿namespace TsSoft.EntityService
{
    using System;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.EntityRepository.Interfaces;

    /// <summary>
    /// Расширения сервиса
    /// </summary>
    public static class DatabaseServiceExtensions
    {
        /// <summary>
        ///     Получить одну сущность из бд и смапить в модель
        /// </summary>
        /// <param name="service">Сервис</param>
        /// <param name="key">Ключ</param>
        /// <returns>Модель</returns>
        public static TViewModel GetSingle<TEntity, TViewModel, TId>(
            [NotNull]this IReadDatabaseService<TEntity, TViewModel> service,
            TId key)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return service.GetSingle(e => e.Id.Equals(key));
        }

        /// <summary>
        /// Обновить одну сущность в бд
        /// </summary>
        /// <param name="service">Сервис</param>
        /// <param name="key">Ключ</param>
        /// <param name="model">Модель, которой будем обновлять</param>
        /// <param name="commit"></param>
        /// <returns>Модель по обновлённой сущности</returns>
        public static TViewModel UpdateSingle<TEntity, TViewModel, TId>(
            [NotNull]this IUpdateDatabaseService<TEntity, TViewModel> service,
            TId key,
            TViewModel model,
            bool commit = true)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return service.UpdateSingle(e => e.Id.Equals(key), model, commit);
        }

        /// <summary>
        ///     Удаляет сущность по ключу
        /// </summary>
        /// <param name="service">Сервис</param>
        /// <param name="id">Ключ</param>
        /// <param name="commit">Записать изменения в бд</param>
        /// <returns>Модель по удалённой сущности</returns>
        public static TViewModel DeleteSingle<TEntity, TViewModel, TId>(
            [NotNull]this IDeleteDatabaseService<TEntity, TViewModel> service,
            TId id, bool commit = true)
            where TEntity: class, IEntityWithId<TId>
            where TId: IEquatable<TId>
        {
            return service.DeleteSingle(e => e.Id.Equals(id), commit);
        }

        /// <summary>
        ///     Получить одну сущность из бд и смапить в модель
        /// </summary>
        /// <param name="service">Сервис</param>
        /// <param name="key">Ключ</param>
        /// <returns>Задача, результат которой - модель</returns>
        public static Task<TViewModel> GetSingleAsync<TEntity, TViewModel, TId>(
            [NotNull]this IReadDatabaseService<TEntity, TViewModel> service,
            TId key)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return service.GetSingleAsync(e => e.Id.Equals(key));
        }

        /// <summary>
        /// Обновить одну сущность в бд
        /// </summary>
        /// <param name="service">Сервис</param>
        /// <param name="key">Ключ</param>
        /// <param name="model">Модель, которой будем обновлять</param>
        /// <param name="commit">Записать изменения в бд</param>
        /// <returns>Задача, результат которой - модель обновлённой сущности</returns>
        public static Task<TViewModel> UpdateSingleAsync<TEntity, TViewModel, TId>(
            [NotNull]this IUpdateDatabaseService<TEntity, TViewModel> service,
            TId key,
            TViewModel model, 
            bool commit = true)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return service.UpdateSingleAsync(e => e.Id.Equals(key), model, commit);
        }

        /// <summary>
        ///     Удаляет сущность по ключу
        /// </summary>
        /// <param name="service">Сервис</param>
        /// <param name="id">Ключ</param>
        /// <param name="commit">Записать изменения в бд</param>
        /// <returns>Задача, результат которой - модель удалённой сущности</returns>
        public static Task<TViewModel> DeleteSingleAsync<TEntity, TViewModel, TId>(
            [NotNull]this IDeleteDatabaseService<TEntity, TViewModel> service,
            TId id, bool commit = true)
            where TEntity : class, IEntityWithId<TId>
            where TId : IEquatable<TId>
        {
            return service.DeleteSingleAsync(e => e.Id.Equals(id), commit);
        }
    }
}