namespace TsSoft.EntityService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.EntityService.UpdateAction;

    /// <summary>
    ///     ������� ����� ��� ��������, ���������������
    ///     ��������� ��������
    /// </summary>
    /// <typeparam name="TEntity">��� ��������</typeparam>
    /// <typeparam name="TViewModel">��� ������</typeparam>
    public abstract class InternalChangeEntityDatabaseService<TEntity, TViewModel> :
        InternalDatabaseService<TEntity, TViewModel>
    {
        /// <summary>
        ///     ����������� ���������� �������� �� ��������
        /// </summary>
        [NotNull] protected readonly Func<TEntity, TEntity, IEnumerable<Func<Task>>> AsyncUpdater;

        /// <summary>
        ///     ������ ������ � ��������, �������� ���� ��������
        /// </summary>
        [NotNull] protected readonly IUpdatePathProviderMapper<TViewModel, TEntity> UpdateMapper;

        /// <summary>
        ///     ���������� �������� �� ��������
        /// </summary>
        [NotNull] protected readonly Action<TEntity, TEntity> Updater;

        /// <summary>
        ///     ������� ����� ��� ��������, ���������������
        ///     ��������� ��������
        /// </summary>
        protected InternalChangeEntityDatabaseService(
            [NotNull] ISelectProviderMapper<TEntity, TViewModel> entitySelectMapper,
            [NotNull] IUpdatePathProviderMapper<TViewModel, TEntity> updateMapper,
            [NotNull] IUpdateEntityActionFactory updateEntityActionFactory) : base(entitySelectMapper)
        {
            if (updateMapper == null) throw new ArgumentNullException("updateMapper");
            if (updateEntityActionFactory == null) throw new ArgumentNullException("updateEntityActionFactory");
            UpdateMapper = updateMapper;

            var funcProvider = UpdateMapper as IUpdateFuncProvider<TEntity>;
            if (funcProvider != null && funcProvider.Updater != null)
            {
                Updater = funcProvider.Updater;
            }
            else
            {
                Updater = updateEntityActionFactory.MakeUpdateAction(
                    UpdateMapper.Paths,
                    UpdateMapper.DeleteAndCreateOnPaths);
            }

            var asyncFuncProvider = UpdateMapper as IAsyncUpdateFuncProvider<TEntity>;
            if (asyncFuncProvider != null && asyncFuncProvider.AsyncUpdater != null)
            {
                AsyncUpdater = asyncFuncProvider.AsyncUpdater;
            }
            else
            {
                AsyncUpdater = updateEntityActionFactory.MakeAsyncUpdateAction(
                    UpdateMapper.Paths,
                    UpdateMapper.DeleteAndCreateOnPaths);
            }
        }
    }
}
