namespace TsSoft.EntityService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using TsSoft.AbstractMapper;
    using TsSoft.EntityRepository;

    /// <summary>
    ///     ������������ �������� ��������� �� ��
    /// </summary>
    /// <typeparam name="TEntity">��� ��������</typeparam>
    /// <typeparam name="TViewModel">��� ������</typeparam>
    public class DeleteDatabaseService<TEntity, TViewModel> : InternalDatabaseService<TEntity, TViewModel>,
        IDeleteDatabaseService<TEntity, TViewModel>
    {
        [NotNull] private readonly IDeleteRepository<TEntity> _repository;

        /// <summary>
        ///     ������������ �������� ��������� �� ��
        /// </summary>
        public DeleteDatabaseService(
            [NotNull] ISelectProviderMapper<TEntity, TViewModel> entitySelectMapper,
            [NotNull] IDeleteRepository<TEntity> repository)
            : base(entitySelectMapper)
        {
            if (repository == null) throw new ArgumentNullException("repository");
            _repository = repository;
        }

        /// <summary>
        ///     ������� �������� �� ���������
        /// </summary>
        /// <param name="where">��������</param>
        /// <param name="commit"></param>
        /// <returns>������ �� �������� ��������</returns>
        public TViewModel DeleteSingle(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            var deletedEntity = _repository.DeleteSingle(where, commit);
            return EntitySelectMapper.Map(deletedEntity);
        }

        /// <summary>
        ///     ������� �������� �� ���������
        /// </summary>
        /// <param name="where">��������</param>
        /// <param name="commit"></param>
        /// <returns>������ �� �������� ��������</returns>
        public async Task<TViewModel> DeleteSingleAsync(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            var deletedEntity = await _repository.DeleteSingleAsync(where, commit).ConfigureAwait(false);
            return EntitySelectMapper.Map(deletedEntity);
        }

        /// <summary>
        ///     ������� �������� �� ���������
        /// </summary>
        /// <param name="where">��������</param>
        /// <param name="commit"></param>
        /// <returns>������ �������� ��������</returns>
        public IEnumerable<TViewModel> Delete(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            var deletedEntities = _repository.Delete(where, commit);
            return deletedEntities.Select(EntitySelectMapper.Map);
        }

        /// <summary>
        ///     ������� �������� �� ���������
        /// </summary>
        /// <param name="where">��������</param>
        /// <param name="commit"></param>
        /// <returns>������ �������� ��������</returns>
        public async Task<IEnumerable<TViewModel>> DeleteAsync(Expression<Func<TEntity, bool>> @where, bool commit = true)
        {
            var deletedEntities = await _repository.DeleteAsync(where, commit).ConfigureAwait(false);
            return deletedEntities.Select(EntitySelectMapper.Map);
        }
    }
}