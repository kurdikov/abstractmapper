﻿namespace TsSoft.EntityService
{
    /// <summary>
    ///     Агрегирует
    ///     <see cref="ICreateDatabaseService{TEntity,TViewModel}" />,
    ///     <see cref="IReadDatabaseService{TEntity,TViewModel}" />,
    ///     <see cref="IUpdateDatabaseService{TEntity,TViewModel}" />,
    ///     <see cref="IDeleteDatabaseService{TEntity,TViewModel}" />
    ///     реализует CRUD для модели
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    /// <typeparam name="TViewModel">Тип модели</typeparam>
    public interface IDatabaseService<TEntity, TViewModel> :
        IReadDatabaseService<TEntity, TViewModel>,
        IUpdateDatabaseService<TEntity, TViewModel>,
        ICreateDatabaseService<TEntity, TViewModel>,
        IDeleteDatabaseService<TEntity, TViewModel>
    {
    }
}
