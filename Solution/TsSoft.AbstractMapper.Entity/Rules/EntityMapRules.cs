﻿namespace TsSoft.AbstractMapper.Rules
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Коллекция правил преобразования сущности БД
    /// </summary>
    /// <typeparam name="TTo">Тип, в который преобразуется сущность БД</typeparam>
    /// <typeparam name="TFrom">Тип сущности БД</typeparam>
    public class EntityMapRules<TTo, TFrom> : MapRules<TTo, TFrom>
    {
        // map single property to one property using IIncludeProvider
        /// <summary>
        /// </summary>
        /// <typeparam name="TToProperty">Тип свойства, в которое идёт преобразование</typeparam>
        /// <typeparam name="TFromProperty">Тип свойства, из которого идёт преобразование</typeparam>
        /// <typeparam name="TProcessor">Тип обработчика</typeparam>
        /// <param name="toAccess">Куда</param>
        /// <param name="processor">Обработчик</param>
        /// <param name="convert">Как</param>
        /// <param name="fromAccess">Что</param>
        /// <param name="dummy">Маркер правила с обработчиком</param>
        public void Add<TFromProperty, TToProperty, TProcessor>(
            Expression<Func<TTo, TToProperty>> toAccess,
            Expression<Func<TFrom, TFromProperty>> fromAccess,
            IncludeProviderRule dummy,
            [NotNull]TProcessor processor,
            Expression<Func<TFromProperty, TProcessor, TToProperty>> convert)
            where TProcessor : IIncludeProvider<TFromProperty>
        {
            Rules.Add(new MapRule
            {
                ToAccess = toAccess,
                FromAccess = new[] { new MapSource(fromAccess, new ProcessorDescription(processor: processor, processedType: typeof(TFromProperty))), },
                Convert = convert,
            });
        }

        // map collection property to one property using IIncludeProvider
        /// <summary>
        /// </summary>
        /// <typeparam name="TToProperty">Тип свойства, в которое идёт преобразование</typeparam>
        /// <typeparam name="TFromProperty">Тип элемента свойства-коллекции, из которого идёт преобразование</typeparam>
        /// <typeparam name="TProcessor">Тип обработчика</typeparam>
        /// <param name="toAccess">Куда</param>
        /// <param name="processor">Обработчик</param>
        /// <param name="convert">Как</param>
        /// <param name="fromAccess">Что</param>
        /// <param name="dummy">Маркер правила с обработчиком</param>
        public void Add<TFromProperty, TToProperty, TProcessor>(
            Expression<Func<TTo, TToProperty>> toAccess,
            Expression<Func<TFrom, IEnumerable<TFromProperty>>> fromAccess,
            IncludeCollectionProviderRule dummy,
            [NotNull]TProcessor processor,
            Expression<Func<IEnumerable<TFromProperty>, TProcessor, TToProperty>> convert)
            where TProcessor : IIncludeProvider<TFromProperty>
        {
            Rules.Add(new MapRule
            {
                ToAccess = toAccess,
                FromAccess = new[] { new MapSource(fromAccess, new ProcessorDescription(processor: processor, processedType: typeof(TFromProperty))), },
                Convert = convert,
            });
        }

        // map single property to one property using ISelectProvider
        /// <summary>
        /// </summary>
        /// <typeparam name="TToProperty">Тип свойства, в которое идёт преобразование</typeparam>
        /// <typeparam name="TFromProperty">Тип свойства, из которого идёт преобразование</typeparam>
        /// <typeparam name="TProcessor">Тип обработчика</typeparam>
        /// <param name="toAccess">Куда</param>
        /// <param name="processor">Обработчик</param>
        /// <param name="convert">Как</param>
        /// <param name="fromAccess">Что</param>
        /// <param name="dummy">Маркер правила с обработчиком</param>
        public void Add<TFromProperty, TToProperty, TProcessor>(
            Expression<Func<TTo, TToProperty>> toAccess,
            Expression<Func<TFrom, TFromProperty>> fromAccess,
            SelectProviderRule dummy,
            [NotNull]TProcessor processor,
            Expression<Func<TFromProperty, TProcessor, TToProperty>> convert)
            where TProcessor : ISelectProvider<TFromProperty>
        {
            Rules.Add(new MapRule
            {
                ToAccess = toAccess,
                FromAccess = new[] { new MapSource(fromAccess, new ProcessorDescription(processor: processor, processedType: typeof(TFromProperty))), },
                Convert = convert,
            });
        }

        // map collection property to one property using ISelectProvider
        /// <summary>
        /// </summary>
        /// <typeparam name="TToProperty">Тип свойства, в которое идёт преобразование</typeparam>
        /// <typeparam name="TFromProperty">Тип элемента свойства-коллекции, из которого идёт преобразование</typeparam>
        /// <typeparam name="TProcessor">Тип обработчика</typeparam>
        /// <param name="toAccess">Куда</param>
        /// <param name="processor">Обработчик</param>
        /// <param name="convert">Как</param>
        /// <param name="fromAccess">Что</param>
        /// <param name="dummy">Маркер правила с обработчиком</param>
        public void Add<TFromProperty, TToProperty, TProcessor>(
            Expression<Func<TTo, TToProperty>> toAccess,
            Expression<Func<TFrom, IEnumerable<TFromProperty>>> fromAccess,
            SelectCollectionProviderRule dummy,
            [NotNull]TProcessor processor,
            Expression<Func<IEnumerable<TFromProperty>, TProcessor, TToProperty>> convert)
            where TProcessor : ISelectProvider<TFromProperty>
        {
            Rules.Add(new MapRule
            {
                ToAccess = toAccess,
                FromAccess = new[] { new MapSource(fromAccess, new ProcessorDescription(processor: processor, processedType: typeof(TFromProperty))), },
                Convert = convert,
            });
        }
    }
}
