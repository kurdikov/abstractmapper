﻿namespace TsSoft.AbstractMapper.Rules
{
    /// <summary>
    /// Маркер правила маппинга, использующего ISelectProvider
    /// </summary>
    public class SelectProviderRule
    {
        /// <summary>
        /// Маркер правила маппинга, использующего ISelectProvider
        /// </summary>
        public static SelectProviderRule Rule = null;
    }
    /// <summary>
    /// Маркер правила маппинга, использующего ISelectProvider
    /// </summary>
    public class SelectCollectionProviderRule
    {
        /// <summary>
        /// Маркер правила маппинга, использующего ISelectProvider
        /// </summary>
        public static SelectCollectionProviderRule Rule = null;
    }
    /// <summary>
    /// Маркер правила маппинга, использующего IIncludeProvider
    /// </summary>
    public class IncludeProviderRule
    {
        /// <summary>
        /// Маркер правила маппинга, использующего IIncludeProvider
        /// </summary>
        public static IncludeProviderRule Rule = null;
    }
    /// <summary>
    /// Маркер правила маппинга, использующего IIncludeProvider
    /// </summary>
    public class IncludeCollectionProviderRule
    {
        /// <summary>
        /// Маркер правила маппинга, использующего IIncludeProvider
        /// </summary>
        public static IncludeCollectionProviderRule Rule = null;
    }
}
