﻿namespace TsSoft.Expressions.PseudoExpressionParser.Tests.ExpressionCreator
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading;
    using BaseTestClasses;
    using JetBrains.Annotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TsSoft.AbstractMapper.Factory;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.PseudoExpressionParser.ExpressionCreator;
    using TsSoft.Expressions.PseudoExpressionParser.Resolvers;

    [TestClass]
    public class ExpressionCreatorIntegrationTests
    {
        [NotNull]
        private IExpressionCreator _expressionCreator;
#if NET45
        private CultureInfo _oldCulture;
#endif

        [TestInitialize]
        public void Init()
        {
            _expressionCreator = new ExpressionCreator(
                new ExpressionContextFactory(
                    new MethodResolver(new DelegateTypeHelper(), new LambdaBodyMaker(), new ConversionChecker()), 
                    //new SimpleMethodResolver(new ParameterTypeMatcher(new LambdaBodyMaker(), new DelegateTypeHelper())),
                    new MemberInfoLibrary(new MemberInfoHelper())));
            SetInvariantCulture();
        }

        [TestCleanup]
        public void Cleanup()
        {
            RestoreCulture();
        }

        private void SetInvariantCulture()
        {
#if NET45
            _oldCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
#endif
        }

        private void RestoreCulture()
        {
#if NET45
            Thread.CurrentThread.CurrentCulture = _oldCulture;
#endif
        }

        [TestMethod]
        public void TestStringConcat()
        {
            var result = _expressionCreator.CreateFilter<string>("x", "(x + \" \" + x).Contains(\"abc\")");
            ExprAssert.AreEqual(x => (x + " " + x).Contains("abc"), result);
        }

        [TestMethod]
        public void TestConcatProperties()
        {
            var result = _expressionCreator.CreateFilter<TestClass>("x", "(x.String + \" \" + x.String).Contains(\"abc\")");
            ExprAssert.AreEqual(x => (x.String + " " + x.String).Contains("abc"), result);

            result = _expressionCreator.CreateFilter<TestClass>("x", "(x.Inner.String + x.Inner.String).Contains(\"abc\")");
            ExprAssert.AreEqual(x => (x.Inner.String + x.Inner.String).Contains("abc"), result);

            result = _expressionCreator.CreateFilter<TestClass>("x", "(x.Inner.String + \" \" + x.Inner.String).Contains(\"abc\")");
            ExprAssert.AreEqual(x => (x.Inner.String + " " + x.Inner.String).Contains("abc"), result);
        }

        [TestMethod]
        public void TestFilterWithStaticClass()
        {
            var result = _expressionCreator.CreateFilter<TestClass>("x", "x.Decimal > 200m", typeof(TestEnum));
            ExprAssert.AreEqual(x => x.Decimal > 200m, result);
        }

        [TestMethod]
        public void TestEnumBitwiseOperations()
        {
            var param = Expression.Parameter(typeof(TestEnum));

            var result = _expressionCreator.CreateExpression<TestEnum, bool>("x", "(x & 1) == 1");
            Expression<Func<TestEnum, bool>> expected = Expression.Lambda<Func<TestEnum, bool>>(
                Expression.Equal(
                    Expression.And(
                        Expression.Convert(param, typeof(int)),
                        Expression.Constant(1)),
                    Expression.Constant(1)),
                param);
            ExprAssert.AreEqual(expected, result);

            result = _expressionCreator.CreateExpression<TestEnum, bool>("x", "(x | 1) == 1");
            expected = Expression.Lambda<Func<TestEnum, bool>>(
                Expression.Equal(
                    Expression.Or(
                        Expression.Convert(param, typeof(int)),
                        Expression.Constant(1)),
                    Expression.Constant(1)),
                param);
            ExprAssert.AreEqual(expected, result);

            result = _expressionCreator.CreateExpression<TestEnum, bool>("x", "(x ^ 1) == 1");
            expected = Expression.Lambda<Func<TestEnum, bool>>(
                Expression.Equal(
                    Expression.ExclusiveOr(
                        Expression.Convert(param, typeof(int)),
                        Expression.Constant(1)),
                    Expression.Constant(1)),
                param);
            ExprAssert.AreEqual(expected, result);

            result = _expressionCreator.CreateExpression<TestEnum, bool>("x", "(x & TestEnum.Value) == 1", typeof(TestEnum));
            expected = Expression.Lambda<Func<TestEnum, bool>>(
                Expression.Equal(
                    Expression.And(
                        Expression.Convert(param, typeof(int)),
                        Expression.Convert(Expression.Constant(TestEnum.Value), typeof(int))),
                    Expression.Constant(1)),
                param);
            ExprAssert.AreEqual(expected, result);

            result = _expressionCreator.CreateExpression<TestEnum, bool>("x", "(x | TestEnum.Value) == 1", typeof(TestEnum));
            expected = Expression.Lambda<Func<TestEnum, bool>>(
                Expression.Equal(
                    Expression.Or(
                        Expression.Convert(param, typeof(int)),
                        Expression.Convert(Expression.Constant(TestEnum.Value), typeof(int))),
                    Expression.Constant(1)),
                param);
            ExprAssert.AreEqual(expected, result);

            result = _expressionCreator.CreateExpression<TestEnum, bool>("x", "(x ^ TestEnum.Value) == 1", typeof(TestEnum));
            expected = Expression.Lambda<Func<TestEnum, bool>>(
                Expression.Equal(
                    Expression.ExclusiveOr(
                        Expression.Convert(param, typeof(int)),
                        Expression.Convert(Expression.Constant(TestEnum.Value), typeof(int))),
                    Expression.Constant(1)),
                param);
            ExprAssert.AreEqual(expected, result);

            result = _expressionCreator.CreateExpression<TestEnum, bool>("x", "(TestEnum.Value & x) == 1", typeof(TestEnum));
            expected = Expression.Lambda<Func<TestEnum, bool>>(
                Expression.Equal(
                    Expression.And(
                        Expression.Convert(Expression.Constant(TestEnum.Value), typeof(int)),
                        Expression.Convert(param, typeof(int))),
                    Expression.Constant(1)),
                param);
            ExprAssert.AreEqual(expected, result);

            result = _expressionCreator.CreateExpression<TestEnum, bool>("x", "(TestEnum.Value | x) == 1", typeof(TestEnum));
            expected = Expression.Lambda<Func<TestEnum, bool>>(
                Expression.Equal(
                    Expression.Or(
                        Expression.Convert(Expression.Constant(TestEnum.Value), typeof(int)),
                        Expression.Convert(param, typeof(int))),
                    Expression.Constant(1)),
                param);
            ExprAssert.AreEqual(expected, result);

            result = _expressionCreator.CreateExpression<TestEnum, bool>("x", "(TestEnum.Value ^ x) == 1", typeof(TestEnum));
            expected = Expression.Lambda<Func<TestEnum, bool>>(
                Expression.Equal(
                    Expression.ExclusiveOr(
                        Expression.Convert(Expression.Constant(TestEnum.Value), typeof(int)),
                        Expression.Convert(param, typeof(int))),
                    Expression.Constant(1)),
                param);
            ExprAssert.AreEqual(expected, result);

            result = _expressionCreator.CreateExpression<TestEnum, bool>("x", "(TestEnum.Value & x) == TestEnum.Value", typeof(TestEnum));
            expected = Expression.Lambda<Func<TestEnum, bool>>(
                Expression.Equal(
                    Expression.Convert(
                        Expression.And(
                            Expression.Convert(Expression.Constant(TestEnum.Value), typeof(int)),
                            Expression.Convert(param, typeof(int))),
                        typeof(TestEnum)),
                    Expression.Constant(TestEnum.Value)),
                param);
            ExprAssert.AreEqual(expected, result);

            result = _expressionCreator.CreateExpression<TestEnum, bool>("x", "(TestEnum.Value | x) == TestEnum.Value", typeof(TestEnum));
            expected = Expression.Lambda<Func<TestEnum, bool>>(
                Expression.Equal(
                    Expression.Convert(
                        Expression.Or(
                            Expression.Convert(Expression.Constant(TestEnum.Value), typeof(int)),
                            Expression.Convert(param, typeof(int))),
                        typeof(TestEnum)),
                    Expression.Constant(TestEnum.Value)),
                param);
            ExprAssert.AreEqual(expected, result);

            result = _expressionCreator.CreateExpression<TestEnum, bool>("x", "(TestEnum.Value ^ x) == TestEnum.Value", typeof(TestEnum));
            expected = Expression.Lambda<Func<TestEnum, bool>>(
                Expression.Equal(
                    Expression.Convert(
                        Expression.ExclusiveOr(
                            Expression.Convert(Expression.Constant(TestEnum.Value), typeof(int)),
                            Expression.Convert(param, typeof(int))),
                        typeof(TestEnum)),
                    Expression.Constant(TestEnum.Value)),
                param);
            ExprAssert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestBoolConst()
        {
            var result = _expressionCreator.CreateExpression<bool>("false");
            ExprAssert.AreEqual(() => false, result);

            result = _expressionCreator.CreateExpression<bool>("true");
            ExprAssert.AreEqual(() => true, result);
        }

        [TestMethod]
        public void TestDecimalConst()
        {
            var result = _expressionCreator.CreateExpression<decimal>("1m");
            ExprAssert.AreEqual(() => 1m, result);

            result = _expressionCreator.CreateExpression<decimal>("1.2m");
            ExprAssert.AreEqual(() => 1.2m, result);

            result = _expressionCreator.CreateExpression<decimal>("0m");
            ExprAssert.AreEqual(() => 0m, result);

            result = _expressionCreator.CreateExpression<decimal>("0.0m");
            ExprAssert.AreEqual(() => 0m, result);

            result = _expressionCreator.CreateExpression<decimal>("-1m");
            ExprAssert.AreEqual(Expression.Lambda<Func<decimal>>(Expression.Negate(Expression.Constant(1m))), result);

            result = _expressionCreator.CreateExpression<decimal>("-1.2m");
            ExprAssert.AreEqual(Expression.Lambda<Func<decimal>>(Expression.Negate(Expression.Constant(1.2m))), result);
        }

        [TestMethod]
        public void TestDoubleConst()
        {
            var result = _expressionCreator.CreateExpression<double>("1d");
            ExprAssert.AreEqual(() => 1d, result);

            result = _expressionCreator.CreateExpression<double>("1.2d");
            ExprAssert.AreEqual(() => 1.2d, result);

            result = _expressionCreator.CreateExpression<double>("1.2");
            ExprAssert.AreEqual(() => 1.2d, result);

            result = _expressionCreator.CreateExpression<double>("0d");
            ExprAssert.AreEqual(() => 0d, result);

            result = _expressionCreator.CreateExpression<double>("0.0d");
            ExprAssert.AreEqual(() => 0d, result);

            result = _expressionCreator.CreateExpression<double>("0.0");
            ExprAssert.AreEqual(() => 0d, result);

            result = _expressionCreator.CreateExpression<double>("-1d");
            ExprAssert.AreEqual(Expression.Lambda<Func<double>>(Expression.Negate(Expression.Constant(1d))), result);

            result = _expressionCreator.CreateExpression<double>("-1.2d");
            ExprAssert.AreEqual(Expression.Lambda<Func<double>>(Expression.Negate(Expression.Constant(1.2d))), result);

            result = _expressionCreator.CreateExpression<double>("-1.2");
            ExprAssert.AreEqual(Expression.Lambda<Func<double>>(Expression.Negate(Expression.Constant(1.2d))), result);
        }

        [TestMethod]
        public void TestFloatConst()
        {
            var result = _expressionCreator.CreateExpression<float>("1f");
            ExprAssert.AreEqual(() => 1f, result);

            result = _expressionCreator.CreateExpression<float>("1.2f");
            ExprAssert.AreEqual(() => 1.2f, result);

            result = _expressionCreator.CreateExpression<float>("0f");
            ExprAssert.AreEqual(() => 0f, result);

            result = _expressionCreator.CreateExpression<float>("0.0f");
            ExprAssert.AreEqual(() => 0f, result);

            result = _expressionCreator.CreateExpression<float>("-1f");
            ExprAssert.AreEqual(Expression.Lambda<Func<float>>(Expression.Negate(Expression.Constant(1f))), result);

            result = _expressionCreator.CreateExpression<float>("-1.2f");
            ExprAssert.AreEqual(Expression.Lambda<Func<float>>(Expression.Negate(Expression.Constant(1.2f))), result);
        }

        [TestMethod]
        public void TestByteConst()
        {
            var result = _expressionCreator.CreateExpression<byte>("1y");
            ExprAssert.AreEqual(() => (byte)1, result);

            result = _expressionCreator.CreateExpression<byte>("96y");
            ExprAssert.AreEqual(() => (byte)96, result);

            result = _expressionCreator.CreateExpression<byte>("0x60y");
            ExprAssert.AreEqual(() => (byte)96, result);

            result = _expressionCreator.CreateExpression<byte>("0y");
            ExprAssert.AreEqual(() => (byte)0, result);
        }

        [TestMethod]
        public void TestShortConst()
        {
            var result = _expressionCreator.CreateExpression<short>("1s");
            ExprAssert.AreEqual(() => (short)1, result);

            result = _expressionCreator.CreateExpression<short>("96s");
            ExprAssert.AreEqual(() => (short)96, result);

            result = _expressionCreator.CreateExpression<short>("0x60s");
            ExprAssert.AreEqual(() => (short)96, result);

            result = _expressionCreator.CreateExpression<short>("0s");
            ExprAssert.AreEqual(() => (short)0, result);

            result = _expressionCreator.CreateExpression<short>("-1s");
            ExprAssert.AreEqual(Expression.Lambda<Func<short>>(Expression.Negate(Expression.Constant((short)1, typeof(short)))), result);

            result = _expressionCreator.CreateExpression<short>("-96s");
            ExprAssert.AreEqual(Expression.Lambda<Func<short>>(Expression.Negate(Expression.Constant((short)96, typeof(short)))), result);
        }

        [TestMethod]
        public void TestIntConst()
        {
            var result = _expressionCreator.CreateExpression<int>("1");
            ExprAssert.AreEqual(() => 1, result);

            result = _expressionCreator.CreateExpression<int>("96");
            ExprAssert.AreEqual(() => 96, result);

            result = _expressionCreator.CreateExpression<int>("0x60");
            ExprAssert.AreEqual(() => 96, result);

            result = _expressionCreator.CreateExpression<int>("0");
            ExprAssert.AreEqual(() => 0, result);

            result = _expressionCreator.CreateExpression<int>("-1");
            ExprAssert.AreEqual(Expression.Lambda<Func<int>>(Expression.Negate(Expression.Constant(1))), result);

            result = _expressionCreator.CreateExpression<int>("-96");
            ExprAssert.AreEqual(Expression.Lambda<Func<int>>(Expression.Negate(Expression.Constant(96))), result);

            // TODO exceptional cases
        }

        [TestMethod]
        public void TestLongConst()
        {
            var result = _expressionCreator.CreateExpression<long>("1l");
            ExprAssert.AreEqual(() => 1L, result);

            result = _expressionCreator.CreateExpression<long>("96l");
            ExprAssert.AreEqual(() => 96L, result);

            result = _expressionCreator.CreateExpression<long>("0x60l");
            ExprAssert.AreEqual(() => 96L, result);

            result = _expressionCreator.CreateExpression<long>("0l");
            ExprAssert.AreEqual(() => 0L, result);

            result = _expressionCreator.CreateExpression<long>("-1l");
            ExprAssert.AreEqual(Expression.Lambda<Func<long>>(Expression.Negate(Expression.Constant(1L))), result);

            result = _expressionCreator.CreateExpression<long>("-96l");
            ExprAssert.AreEqual(Expression.Lambda<Func<long>>(Expression.Negate(Expression.Constant(96L))), result);
        }

        [TestMethod]
        public void TestStringConst()
        {
            var result = _expressionCreator.CreateExpression<string>(@"""1""");
            ExprAssert.AreEqual(() => "1", result);

            result = _expressionCreator.CreateExpression<string>(@"""123""");
            ExprAssert.AreEqual(() => "123", result);

            result = _expressionCreator.CreateExpression<string>(@"""123""");
            ExprAssert.AreEqual(() => "123", result);

            result = _expressionCreator.CreateExpression<string>(@"""12\""3""");
            ExprAssert.AreEqual(() => "12\"3", result);

            result = _expressionCreator.CreateExpression<string>(@"""123""");
            ExprAssert.AreEqual(() => "123", result);

            result = _expressionCreator.CreateExpression<string>(@"""123\u12345""");
            var str = result.Compile()();
            Assert.IsNotNull(str);
            Assert.AreEqual(5, str.Length);
            Assert.AreEqual('\u1234', str[3]);

            result = _expressionCreator.CreateExpression<string>(@"""b\'\""\\\0\a\b\f\n\r\te""");
            ExprAssert.AreEqual(() => "b'\"\\\0\a\b\f\n\r\te", result);
        }

        [TestMethod]
        public void TestCharConst()
        {
            var result = _expressionCreator.CreateExpression<char>("'a'");
            ExprAssert.AreEqual(() => 'a', result);

            result = _expressionCreator.CreateExpression<char>("'\u1234'");
            ExprAssert.AreEqual(() => '\u1234', result);
        }

        [TestMethod]
        public void TestNull()
        {
            var result = _expressionCreator.CreateExpression<object>("null");
            ExprAssert.AreEqual(() => null, result);
        }

        [TestMethod]
        public void TestDateTimeConst()
        {
            var result = _expressionCreator.CreateExpression<DateTime>("date(2015-01-14)");
            ExprAssert.AreEqual(Expression.Lambda<Func<DateTime>>(Expression.Constant(new DateTime(2015, 1, 14))), result);
            
            result = _expressionCreator.CreateExpression<DateTime>("date(2015-01-14T18:19:20Z)");
            ExprAssert.AreEqual(Expression.Lambda<Func<DateTime>>(Expression.Constant(new DateTime(2015, 1, 14, 18, 19, 20, DateTimeKind.Utc).ToLocalTime())), result);

            result = _expressionCreator.CreateExpression<DateTime>("date(2015-01-14T18:19:20)");
            ExprAssert.AreEqual(Expression.Lambda<Func<DateTime>>(Expression.Constant(new DateTime(2015, 1, 14, 18, 19, 20, DateTimeKind.Local))), result);

            result = _expressionCreator.CreateExpression<DateTime>("date()");
            ExprAssert.AreEqual(() => DateTime.Now, result);
        }

        [TestMethod]
        public void TestTimeSpanConst()
        {
            var result = _expressionCreator.CreateExpression<TimeSpan>("time(18:19:20)");
            ExprAssert.AreEqual(Expression.Lambda<Func<TimeSpan>>(Expression.Constant(new TimeSpan(18, 19, 20))), result);

            result = _expressionCreator.CreateExpression<TimeSpan>("time()");
            var body = result.Body as ConstantExpression;
            Assert.IsNotNull(body);
            var bodyValue = (TimeSpan)body.Value;
            Assert.AreEqual(TimeSpan.Zero, bodyValue);
        }

        [TestMethod]
        public void TestUuidConst()
        {
            var result = _expressionCreator.CreateExpression<Guid>("uuid(11111111-1111-1111-1111-111111111111)");
            ExprAssert.AreEqual(Expression.Lambda<Func<Guid>>(Expression.Constant(Guid.Parse("11111111-1111-1111-1111-111111111111"))), result);

            result = _expressionCreator.CreateExpression<Guid>("uuid(11111111111111111111111111111111)");
            ExprAssert.AreEqual(Expression.Lambda<Func<Guid>>(Expression.Constant(Guid.Parse("11111111-1111-1111-1111-111111111111"))), result);
        }

        [TestMethod]
        public void TestArrayConstant()
        {
            var result = _expressionCreator.CreateExpression<int[]>("[1,2,3]");
            ExprAssert.AreEqual(() => new[] {1,2,3}, result);

            var resultLong = _expressionCreator.CreateExpression<long[]>("[1l,2l,3l]");
            ExprAssert.AreEqual(() => new[] { 1L, 2, 3 }, resultLong);

            var resultDecimal = _expressionCreator.CreateExpression<decimal[]>("[1m,2.1m,3.2m,4m]");
            ExprAssert.AreEqual(() => new[] { 1, 2.1m, 3.2m, 4 }, resultDecimal);

            var resultFloat = _expressionCreator.CreateExpression<float[]>("[1f,2.1f,3.2f,4f]");
            ExprAssert.AreEqual(() => new[] { 1f, 2.1f, 3.2f, 4f }, resultFloat);

            var resultDouble = _expressionCreator.CreateExpression<double[]>("[1d, 2.1d, 3.2, 4d]");
            ExprAssert.AreEqual(() => new[] { 1d, 2.1d, 3.2d, 4d }, resultDouble);

            var resultString = _expressionCreator.CreateExpression<string[]>("[\"1\", \"2\", \"abc\"]");
            ExprAssert.AreEqual(() => new[] {"1", "2", "abc"}, resultString);
        }

        [TestMethod]
        public void TestBooleanOperatorsAndComparisons()
        {
            var result = _expressionCreator.CreateExpression<int, bool>("x", "x>2");
            ExprAssert.AreEqual(x => x>2, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x>=2 or x<=3");
            ExprAssert.AreEqual(x => x >= 2 || x<=3, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x!=2 || x==3");
            ExprAssert.AreEqual(x => x != 2 || x == 3, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x>=2 and x<3");
            ExprAssert.AreEqual(x => x >= 2 && x < 3, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x!=2 && x==3");
            ExprAssert.AreEqual(x => x != 2 && x == 3, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x!=2 ^ x==3");
            ExprAssert.AreEqual(x => x != 2 ^ x == 3, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x!=2 && x==3 || x==4");
            ExprAssert.AreEqual(x => x != 2 && x == 3 || x == 4, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "(x!=2 && x==3) || x==4");
            ExprAssert.AreEqual(x => (x != 2 && x == 3) || x == 4, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "(x!=2 or x==3) and x==4");
            ExprAssert.AreEqual(x => (x != 2 || x == 3) && x == 4, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "(x!=2 && x==3) || x==4");
            ExprAssert.AreEqual(x => (x != 2 && x == 3) || x == 4, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x!=2 && (x==3 || x ==4)");
            ExprAssert.AreEqual(x => x != 2 && (x == 3 || x == 4), result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x!=2 && x==3 ^ x == 4");
            ExprAssert.AreEqual(x => x != 2 && x == 3 ^ x==4, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "(x!=2 && x==3) ^ x == 4");
            ExprAssert.AreEqual(x => (x != 2 && x == 3) ^ x == 4, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x!=2 && (x==3 ^ x == 4)");
            ExprAssert.AreEqual(x => x != 2 && (x == 3 ^ x == 4), result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x!=2 && (x==3 ^ x == 4) && x > 5");
            ExprAssert.AreEqual(x => x != 2 && (x == 3 ^ x == 4) && x > 5, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x!=2 && (x==3 ^ x == 4) || x > 5");
            ExprAssert.AreEqual(x => x != 2 && (x == 3 ^ x == 4) || x > 5, result);
            
            result = _expressionCreator.CreateExpression<int, bool>("x", "x!=2 && !(x==3 ^ x == 4) || x > 5");
            ExprAssert.AreEqual(x => x != 2 && !(x == 3 ^ x == 4) || x > 5, result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x!=2 and not (x==3 xor x == 4) or x > 5");
            ExprAssert.AreEqual(x => x != 2 && !(x == 3 ^ x == 4) || x > 5, result);
        }

        [TestMethod]
        public void TestBitwiseOperations()
        {
            var result = _expressionCreator.CreateExpression<int, int>("x", "x | 0x01");
            ExprAssert.AreEqual(x => x | 1, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x & 0x01");
            ExprAssert.AreEqual(x => x & 1, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x ^ 0x01");
            ExprAssert.AreEqual(x => x ^ 1, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x & 0x01 | x ^ 0x03");
            ExprAssert.AreEqual(x => x & 1 | x ^ 3, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x & 0x01 ^ x | 0x03");
            ExprAssert.AreEqual(x => x & 1 ^ x | 3, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x | 0x01 ^ x & 0x03");
            ExprAssert.AreEqual(x => x | 1 ^ x & 3, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x | 0x01 & x ^ 0x03");
            ExprAssert.AreEqual(x => x | 1 & x ^ 3, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x ^ 0x01 & x | 0x03");
            ExprAssert.AreEqual(x => x ^ 1 & x | 3, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x ^ 0x01 | x & 0x03");
            ExprAssert.AreEqual(x => x ^ 1 | x & 3, result);
        }

        [TestMethod]
        public void TestShiftOperations()
        {
            var result = _expressionCreator.CreateExpression<int, int>("x", "x >> 2");
            ExprAssert.AreEqual(x => x >> 2, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x << 2");
            ExprAssert.AreEqual(x => x << 2, result);
            
            result = _expressionCreator.CreateExpression<int, int>("x", "x >> 2 << 1");
            ExprAssert.AreEqual(x => x >> 2 << 1, result);
        }

        [TestMethod]
        public void TestArithmeticOperations()
        {
            var result = _expressionCreator.CreateExpression<int, int>("x", "x + 2");
            ExprAssert.AreEqual(x => x + 2, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x - 2");
            ExprAssert.AreEqual(x => x - 2, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x * 2");
            ExprAssert.AreEqual(x => x * 2, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x / 2");
            ExprAssert.AreEqual(x => x / 2, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x % 2");
            ExprAssert.AreEqual(x => x % 2, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x + 2 + 3");
            ExprAssert.AreEqual(x => x + 2 + 3, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x - 2 - 3");
            ExprAssert.AreEqual(x => x - 2 - 3, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x - 2 + 3");
            ExprAssert.AreEqual(x => x - 2 + 3, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x + (2 - x)");
            ExprAssert.AreEqual(x => x + (2 - x), result);

            result = _expressionCreator.CreateExpression<int, int>("x", "x + 2*x");
            ExprAssert.AreEqual(x => x + 2*x, result);

            result = _expressionCreator.CreateExpression<int, int>("x", "(x + 2)*x");
            ExprAssert.AreEqual(x => (x + 2) * x, result);
        }

        [TestMethod]
        public void TestMemberAccess()
        {
            var result1 = _expressionCreator.CreateExpression<TestEntity, int>("x", "x.Prop1");
            ExprAssert.AreEqual(x => x.Prop1, result1);

            var result2 = _expressionCreator.CreateExpression<TestEntity, string>("x", "x.Prop2");
            ExprAssert.AreEqual(x => x.Prop2, result2);

            var result3 = _expressionCreator.CreateExpression<TestEntity, int>("x", "x.Parent.Prop1");
            ExprAssert.AreEqual(x => x.Parent.Prop1, result3);

            var result4 = _expressionCreator.CreateExpression<TestEntity, IEnumerable<int>>("x", "x.Children.Prop1");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Prop1), result4);

            var result5 = _expressionCreator.CreateExpression<TestEntity, IEnumerable<int>>("x", "x.Children.Children.Prop1");
            // ReSharper disable RedundantDelegateCreation
            // R# is going crazy again - no way this is redundant; c# compiler inserts IEnumerable instead of ICollection
            ExprAssert.AreEqual(x => x.Children.SelectMany(new Func<TestEntity, ICollection<TestEntity>>(c => c.Children)).Select(c => c.Prop1), result5);
            // ReSharper restore RedundantDelegateCreation
        }

        [TestMethod]
        public void TestMethodCall()
        {
            var result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Zero()");
            ExprAssert.AreEqual(x => x.Zero(), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.One(0)");
            ExprAssert.AreEqual(x => x.One(0), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.OneOverload(0)");
            ExprAssert.AreEqual(x => x.OneOverload(0), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.OneOverload(\"0\")");
            ExprAssert.AreEqual(x => x.OneOverload("0"), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Two(0, 0)");
            ExprAssert.AreEqual(x => x.Two(0, 0), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.TwoOverload(0, 0)");
            ExprAssert.AreEqual(x => x.TwoOverload(0, 0), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.TwoOverload(0, \"0\")");
            ExprAssert.AreEqual(x => x.TwoOverload(0, "0"), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.TwoOverload(0, 0.ToString())");
// ReSharper disable once SpecifyACultureInStringConversionExplicitly
            ExprAssert.AreEqual(x => x.TwoOverload(0, 0.ToString()), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.TwoOverload(\"0\", 0)");
            ExprAssert.AreEqual(x => x.TwoOverload("0", 0), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.TwoOverload(0, 0.ToString(\"00\"))");
            ExprAssert.AreEqual(x => x.TwoOverload(0, 0.ToString("00")), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.TwoOverload(0, 0.ToString(\"00\")) == true");
// ReSharper disable once RedundantBoolCompare
            ExprAssert.AreEqual(x => x.TwoOverload(0, 0.ToString("00")) == true, result);

            var resultStr = _expressionCreator.CreateExpression<TestClass, string>(
                "x", "x.TwoOverload(0, 0.ToString(\"00\")).ToString()");
            ExprAssert.AreEqual(x => x.TwoOverload(0, 0.ToString("00")).ToString(), resultStr);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Generic(0)");
            ExprAssert.AreEqual(x => x.Generic(0), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Generic(\"0\")");
            ExprAssert.AreEqual(x => x.Generic("0"), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Generic2(0, 0)");
            ExprAssert.AreEqual(x => x.Generic2(0, 0), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Generic2(0, \"0\")");
            ExprAssert.AreEqual(x => x.Generic2(0, "0"), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Generic3(0, 0)");
            ExprAssert.AreEqual(x => x.Generic3(0, 0), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Generic3(0, \"0\")");
            ExprAssert.AreEqual(x => x.Generic3(0, "0"), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Generic3(\"0\", 0)");
            ExprAssert.AreEqual(x => x.Generic3("0", 0), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Generic3(\"0\", \"0\")");
            ExprAssert.AreEqual(x => x.Generic3("0", "0"), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.GenericOverload(0, 0)");
            ExprAssert.AreEqual(x => x.GenericOverload(0, 0), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.GenericOverload(0, \"0\")");
            ExprAssert.AreEqual(x => x.GenericOverload(0, "0"), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.GenericOverload(\"0\", 0)");
            ExprAssert.AreEqual(x => x.GenericOverload("0", 0), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.GenericOverload(\"0\", \"0\")");
            ExprAssert.AreEqual(x => x.GenericOverload("0", "0"), result);
        }

        [TestMethod]
        public void TestArrayAccess()
        {
            var result = _expressionCreator.CreateExpression<int[], int>("x", "x[1]");
            ExprAssert.AreEqual(x => x[1], result);

            var result2 = _expressionCreator.CreateExpression<int[][], int>("x", "x[1][2]");
            ExprAssert.AreEqual(x => x[1][2], result2);
        }

        [TestMethod]
        public void TestSugar()
        {
            var result = _expressionCreator.CreateExpression<int, bool>("x", "x in [1,2,3]");
            ExprAssert.AreEqual(x => new[]{1,2,3}.Contains(x), result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "x not in [1,2,3]");
            ExprAssert.AreEqual(x => !new[] { 1, 2, 3 }.Contains(x), result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "[1,2,3] is empty");
            ExprAssert.AreEqual(x => !new[] { 1, 2, 3 }.Any(), result);

            result = _expressionCreator.CreateExpression<int, bool>("x", "[1,2,3] is not empty");
            ExprAssert.AreEqual(x => new[] { 1, 2, 3 }.Any(), result);

            var resultstr = _expressionCreator.CreateExpression<string, bool>("x", "\"123\" contains x");
            ExprAssert.AreEqual(x => "123".Contains(x), resultstr);

            resultstr = _expressionCreator.CreateExpression<string, bool>("x", "\"123\" starts with x");
            ExprAssert.AreEqual(x => "123".StartsWith(x), resultstr);

            resultstr = _expressionCreator.CreateExpression<string, bool>("x", "\"123\" ends with x");
            ExprAssert.AreEqual(x => "123".EndsWith(x), resultstr);

            var resultBool = _expressionCreator.CreateExpression<bool>("[1,2,3].SelectMany(x => [1]).ToArray() in [[1]]");
            ExprAssert.AreEqual(() => new[]{new[]{1}}.Contains(new[]{1,2,3}.SelectMany(x => new[]{1}).ToArray()), resultBool);
        }

        [TestMethod]
        public void TestLinq()
        {
            var result = _expressionCreator.CreateExpression<TestEntity, bool>("x", "x.Children.All(y => y.Prop1 == 0)");
            ExprAssert.AreEqual(x => x.Children.All(y => y.Prop1 == 0), result);

            var resultInt = _expressionCreator.CreateExpression<int[], int>("x", "x.Aggregate((p, q) => p + q)");
            ExprAssert.AreEqual(resultInt, ints => ints.Aggregate((p,q) => p + q));

            var resultStr = _expressionCreator.CreateExpression<int[], string>("x", "x.Aggregate(\"\", (p, q) => p + q.ToString(\"00\"))", true);
            ExprAssert.AreEqual(resultStr, ints => ints.Aggregate("", (s, i) => s + i.ToString("00")));

            var resultBool = _expressionCreator.CreateExpression<IEnumerable<int>, bool>(
                "x", "x.Aggregate(\"\", (p, q) => p + q.ToString(\"00\"), s => s == \"123\")");
            ExprAssert.AreEqual(resultBool, ints => ints.Aggregate("", (s, i) => s + i.ToString("00"), s => s == "123"));

            result = _expressionCreator.CreateExpression<TestEntity, bool>("x", "x.Children.Any()");
            ExprAssert.AreEqual(x => x.Children.Any(), result);

            result = _expressionCreator.CreateExpression<TestEntity, bool>("x", "x.Children.Any(x => x.Children.Any())");
            ExprAssert.AreEqual(x => x.Children.Any(y => y.Children.Any()), result);

            var resultDouble = _expressionCreator.CreateExpression<TestEntity, double>("x", "x.Children.Prop1.Average()");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Prop1).Average(), resultDouble);

            resultDouble = _expressionCreator.CreateExpression<TestEntity, double>("x", "x.Children.Average(c => c.Prop1)");
            ExprAssert.AreEqual(x => x.Children.Average(c => c.Prop1), resultDouble);

            var resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.Concat(x.Children.Children)");
// ReSharper disable once RedundantDelegateCreation
            // see above
            ExprAssert.AreEqual(resultColl, x => x.Children.Concat(x.Children.SelectMany(new Func<TestEntity, ICollection<TestEntity>>(y => y.Children))));

            var resultEntBool = _expressionCreator.CreateExpression<TestEntity, TestEntity, bool>(
                "x", "y", "x.Children.Contains(y)");
            ExprAssert.AreEqual((x, y) => x.Children.Contains(y), resultEntBool);

            resultInt = _expressionCreator.CreateExpression<int[], int>("x", "x.Count()");
            ExprAssert.AreEqual(resultInt, ints => ints.Count());

            resultInt = _expressionCreator.CreateExpression<int[], int>("x", "x.Count(y => y % 2 == 1)");
            ExprAssert.AreEqual(resultInt, ints => ints.Count(y => y % 2 == 1));

            var resultLong = _expressionCreator.CreateExpression<int[], long>("x", "x.LongCount()");
            ExprAssert.AreEqual(resultLong, ints => ints.LongCount());

            resultLong = _expressionCreator.CreateExpression<int[], long>("x", "x.LongCount(y => y % 2 == 1)");
            ExprAssert.AreEqual(resultLong, ints => ints.LongCount(y => y % 2 == 1));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.DefaultIfEmpty()");
            ExprAssert.AreEqual(resultColl, e => e.Children.DefaultIfEmpty());

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.DefaultIfEmpty(x)");
            ExprAssert.AreEqual(resultColl, e => e.Children.DefaultIfEmpty(e));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.Distinct()");
            ExprAssert.AreEqual(resultColl, e => e.Children.Distinct());

            resultInt = _expressionCreator.CreateExpression<int[], int>("x", "x.ElementAt(1)");
            ExprAssert.AreEqual(resultInt, x => x.ElementAt(1));

            resultInt = _expressionCreator.CreateExpression<int[], int>("x", "x.ElementAtOrDefault(1)");
            ExprAssert.AreEqual(resultInt, x => x.ElementAtOrDefault(1));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>("x", "x.Children.Except(x.Parent.Children)");
            ExprAssert.AreEqual(resultColl, x => x.Children.Except(x.Parent.Children));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>("x", "x.Children.Union(x.Parent.Children)");
            ExprAssert.AreEqual(resultColl, x => x.Children.Union(x.Parent.Children));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>("x", "x.Children.Intersect(x.Parent.Children)");
            ExprAssert.AreEqual(resultColl, x => x.Children.Intersect(x.Parent.Children));

            var resultEnt = _expressionCreator.CreateExpression<TestEntity, TestEntity>("x", "x.Children.First()");
            ExprAssert.AreEqual(resultEnt, x => x.Children.First());

            resultEnt = _expressionCreator.CreateExpression<TestEntity, TestEntity>("x", "x.Children.First(y => y.Prop2 == \"0\")");
            ExprAssert.AreEqual(resultEnt, x => x.Children.First(y => y.Prop2 == "0"));

            resultEnt = _expressionCreator.CreateExpression<TestEntity, TestEntity>("x", "x.Children.FirstOrDefault()");
            ExprAssert.AreEqual(resultEnt, x => x.Children.FirstOrDefault());

            resultEnt = _expressionCreator.CreateExpression<TestEntity, TestEntity>("x", "x.Children.FirstOrDefault(y => y.Prop2 == \"0\")");
            ExprAssert.AreEqual(resultEnt, x => x.Children.FirstOrDefault(y => y.Prop2 == "0"));

            resultEnt = _expressionCreator.CreateExpression<TestEntity, TestEntity>("x", "x.Children.Last()");
            ExprAssert.AreEqual(resultEnt, x => x.Children.Last());

            resultEnt = _expressionCreator.CreateExpression<TestEntity, TestEntity>("x", "x.Children.Last(y => y.Prop2 == \"0\")");
            ExprAssert.AreEqual(resultEnt, x => x.Children.Last(y => y.Prop2 == "0"));

            resultEnt = _expressionCreator.CreateExpression<TestEntity, TestEntity>("x", "x.Children.LastOrDefault()");
            ExprAssert.AreEqual(resultEnt, x => x.Children.LastOrDefault());

            resultEnt = _expressionCreator.CreateExpression<TestEntity, TestEntity>("x", "x.Children.LastOrDefault(y => y.Prop2 == \"0\")");
            ExprAssert.AreEqual(resultEnt, x => x.Children.LastOrDefault(y => y.Prop2 == "0"));

            var resultEntInt = _expressionCreator.CreateExpression<TestEntity, int>("x", "x.Children.Prop1.Max()");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Prop1).Max(), resultEntInt);

            resultEntInt = _expressionCreator.CreateExpression<TestEntity, int>("x", "x.Children.Prop1.Max(y => y % 2)");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Prop1).Max(y => y % 2), resultEntInt);

            resultEntInt = _expressionCreator.CreateExpression<TestEntity, int>("x", "x.Children.Prop1.Min()");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Prop1).Min(), resultEntInt);

            resultEntInt = _expressionCreator.CreateExpression<TestEntity, int>("x", "x.Children.Prop1.Min(y => y % 2)");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Prop1).Min(y => y % 2), resultEntInt);

            resultEntInt = _expressionCreator.CreateExpression<TestEntity, int>("x", "x.Children.Prop1.Sum()");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Prop1).Sum(), resultEntInt);

            var resultOrdered = _expressionCreator.CreateExpression<TestEntity, IOrderedEnumerable<TestEntity>>(
                "x", "x.Children.OrderBy(x => x.Prop1)");
            ExprAssert.AreEqual(x => x.Children.OrderBy(y => y.Prop1), resultOrdered);

            resultOrdered = _expressionCreator.CreateExpression<TestEntity, IOrderedEnumerable<TestEntity>>(
                "x", "x.Children.OrderBy(x => x.Prop1).ThenBy(x => x.Prop3)");
            ExprAssert.AreEqual(x => x.Children.OrderBy(y => y.Prop1).ThenBy(z => z.Prop3), resultOrdered);

            resultOrdered = _expressionCreator.CreateExpression<TestEntity, IOrderedEnumerable<TestEntity>>(
                "x", "x.Children.OrderByDescending(x => x.Prop1).ThenByDescending(x => x.Prop3)");
            ExprAssert.AreEqual(x => x.Children.OrderByDescending(y => y.Prop1).ThenByDescending(z => z.Prop3), resultOrdered);

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.Reverse()");
            ExprAssert.AreEqual(x => x.Children.Reverse(), resultColl);

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.Select(c => c.Parent)");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Parent), resultColl);

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.Select((c, i) => c.Parent)");
            ExprAssert.AreEqual(x => x.Children.Select((c, i) => c.Parent), resultColl);

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.SelectMany(c => c.Children)");
            ExprAssert.AreEqual(x => x.Children.SelectMany(c => c.Children), resultColl);

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.SelectMany((c, i) => c.Children)");
            ExprAssert.AreEqual(x => x.Children.SelectMany((c, i) => c.Children), resultColl);

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.SelectMany(c => c.Children, (p, cc) => p.Parent)");
            ExprAssert.AreEqual(x => x.Children.SelectMany(c => c.Children, (p, cc) => p.Parent), resultColl);

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.SelectMany((c, i) => c.Children, (p, cc) => p.Parent)");
            ExprAssert.AreEqual(x => x.Children.SelectMany((c, i) => c.Children, (p, cc) => p.Parent), resultColl);

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.SelectMany(c => c.Children, (p, cc) => cc.Parent)");
            ExprAssert.AreEqual(x => x.Children.SelectMany(c => c.Children, (p, cc) => cc.Parent), resultColl);

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.SelectMany((c, i) => c.Children, (p, cc) => cc.Parent)");
            ExprAssert.AreEqual(x => x.Children.SelectMany((c, i) => c.Children, (p, cc) => cc.Parent), resultColl);

            resultBool = _expressionCreator.CreateExpression<IEnumerable<int>, bool>("x", "x.SequenceEqual([1,2,3])");
            ExprAssert.AreEqual(resultBool, x => x.SequenceEqual(new[] {1,2,3}));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>("x", "x.Children.Skip(1)");
            ExprAssert.AreEqual(resultColl, x => x.Children.Skip(1));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>("x", "x.Children.SkipWhile(y => y.Prop1 == 1)");
            ExprAssert.AreEqual(resultColl, x => x.Children.SkipWhile(y => y.Prop1 == 1));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>("x", "x.Children.SkipWhile((y, i) => y.Prop1 + i == 1)");
            ExprAssert.AreEqual(resultColl, x => x.Children.SkipWhile((y, i) => y.Prop1 + i == 1));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>("x", "x.Children.Take(1)");
            ExprAssert.AreEqual(resultColl, x => x.Children.Take(1));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>("x", "x.Children.TakeWhile(y => y.Prop1 == 1)");
            ExprAssert.AreEqual(resultColl, x => x.Children.TakeWhile(y => y.Prop1 == 1));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>("x", "x.Children.TakeWhile((y, i) => y.Prop1 + i == 1)");
            ExprAssert.AreEqual(resultColl, x => x.Children.TakeWhile((y, i) => y.Prop1 + i == 1));

            var resultArray = _expressionCreator.CreateExpression<TestEntity, TestEntity[]>(
                "x", "x.Children.Children.ToArray()");
// ReSharper disable once RedundantDelegateCreation
            ExprAssert.AreEqual(resultArray, x => x.Children.SelectMany(new Func<TestEntity, ICollection<TestEntity>>(c => c.Children)).ToArray());

            var resultList = _expressionCreator.CreateExpression<TestEntity, List<TestEntity>>(
                "x", "x.Children.Parent.ToList()");
            ExprAssert.AreEqual(resultList, x => x.Children.Select(c => c.Parent).ToList());

            var resultDictionary = _expressionCreator.CreateExpression<TestEntity, Dictionary<int, TestEntity>>(
                "x", "x.Children.Parent.ToDictionary(p => p.Prop1)");
            ExprAssert.AreEqual(resultDictionary, x => x.Children.Select(c => c.Parent).ToDictionary(p => p.Prop1));
            
            resultDictionary = _expressionCreator.CreateExpression<TestEntity, Dictionary<int, TestEntity>>(
               "x", "x.Children.Parent.ToDictionary(p => p.Prop1, p => p.Parent)");
            ExprAssert.AreEqual(resultDictionary, x => x.Children.Select(c => c.Parent).ToDictionary(p => p.Prop1, p => p.Parent));

            var resultLookup = _expressionCreator.CreateExpression<TestEntity, ILookup<int, TestEntity>>(
                "x", "x.Children.Parent.ToLookup(p => p.Prop1)");
            ExprAssert.AreEqual(resultLookup, x => x.Children.Select(c => c.Parent).ToLookup(p => p.Prop1));

            resultLookup = _expressionCreator.CreateExpression<TestEntity, ILookup<int, TestEntity>>(
               "x", "x.Children.Parent.ToLookup(p => p.Prop1, p => p.Parent)");
            ExprAssert.AreEqual(resultLookup, x => x.Children.Select(c => c.Parent).ToLookup(p => p.Prop1, p => p.Parent));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.Where(c => c.Prop1 != 0)");
            ExprAssert.AreEqual(resultColl, x => x.Children.Where(c => c.Prop1 != 0));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.Where((c, i) => c.Prop1 - i != 0)");
            ExprAssert.AreEqual(resultColl, x => x.Children.Where((c, i) => c.Prop1 - i != 0));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.Zip(x.Children.Children, (c, cc) => c.Parent)");
// ReSharper disable once RedundantDelegateCreation
            ExprAssert.AreEqual(resultColl, x => x.Children.Zip(x.Children.SelectMany(new Func<TestEntity, ICollection<TestEntity>>(c => c.Children)), (c, cc) => c.Parent));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.Zip(x.Children.Children, (c, cc) => cc.Parent)");
// ReSharper disable once RedundantDelegateCreation
            ExprAssert.AreEqual(resultColl, x => x.Children.Zip(x.Children.SelectMany(new Func<TestEntity, ICollection<TestEntity>>(c => c.Children)), (c, cc) => cc.Parent));

            var resultGrouping = _expressionCreator.CreateExpression<TestEntity, IEnumerable<IGrouping<int, TestEntity>>>("x", "x.Children.GroupBy(c => c.Prop1)");
            ExprAssert.AreEqual(resultGrouping, x => x.Children.GroupBy(c => c.Prop1));

            resultGrouping = _expressionCreator.CreateExpression<TestEntity, IEnumerable<IGrouping<int, TestEntity>>>("x", "x.Children.GroupBy(c => c.Prop1, c => c)");
            ExprAssert.AreEqual(resultGrouping, x => x.Children.GroupBy(c => c.Prop1, c => c));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.GroupBy(c => c.Prop1, (p1, coll) => coll.First())");
            ExprAssert.AreEqual(resultColl, x => x.Children.GroupBy(c => c.Prop1, (p1, coll) => coll.First()));

            var resultCollColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<IEnumerable<int>>>(
                "x", "x.Children.GroupBy(c => c.Prop1, (p1, coll) => coll.Prop1)");
            ExprAssert.AreEqual(resultCollColl, x => x.Children.GroupBy(c => c.Prop1, (p1, coll) => coll.Select(c => c.Prop1)));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.GroupBy(c => c.Prop1, c => c.Parent, (p1, coll) => coll.First())");
            ExprAssert.AreEqual(resultColl, x => x.Children.GroupBy(c => c.Prop1, c => c.Parent, (p1, coll) => coll.First()));

            resultCollColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<IEnumerable<int>>>(
                "x", "x.Children.GroupBy(c => c.Prop1, c => c.Parent, (p1, coll) => coll.Prop1)");
            ExprAssert.AreEqual(resultCollColl, x => x.Children.GroupBy(c => c.Prop1, c => c.Parent, (p1, coll) => coll.Select(c => c.Prop1)));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.GroupJoin([x.Parent], y => y.Prop3, z => z.Prop3, (e1, ecoll) => e1)");
            ExprAssert.AreEqual(resultColl, x => x.Children.GroupJoin(
                new[] {x.Parent}, y => y.Prop3, z => z.Prop3, (entity, entities) => entity));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.GroupJoin([x.Parent], y => y.Prop3, z => z.Prop3, (e1, ecoll) => ecoll).SelectMany(c => c)");
            ExprAssert.AreEqual(resultColl, x => x.Children.GroupJoin(
                new[] { x.Parent }, y => y.Prop3, z => z.Prop3, (entity, entities) => entities).SelectMany(c => c));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.Join([x.Parent], y => y.Prop3, z => z.Prop3, (e1, e2) => e1)");
            ExprAssert.AreEqual(resultColl, x => x.Children.Join(
                new[] { x.Parent }, y => y.Prop3, z => z.Prop3, (e1, e2) => e1));

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<TestEntity>>(
                "x", "x.Children.Join([x.Parent], y => y.Prop3, z => z.Prop3, (e1, e2) => e2)");
            ExprAssert.AreEqual(resultColl, x => x.Children.Join(
                new[] { x.Parent }, y => y.Prop3, z => z.Prop3, (e1, e2) => e2));
        }

        [TestMethod]
        public void TestStringStaticMethods()
        {
            var result = _expressionCreator.CreateStringifier<int>("x", @"String.Format(""{0} - {1}"", [""123"", ""456""], x)");
            ExprAssert.AreEqual(result, x => string.Format("{0} - {1}", new [] {"123", "456"}, x));

            var resultStr = _expressionCreator.CreateExpression<string, string>("x", "\"1\" + x");
            ExprAssert.AreEqual(x => "1" + x, resultStr);
        }

        [TestMethod]
        public void TestEnumValue()
        {
            var result = _expressionCreator.CreateExpression<TestEnum>("TestEnum.Value", typeof(TestEnum));
            ExprAssert.AreEqual(() => TestEnum.Value, result);
        }

        [TestMethod]
        public void TestStaticMembers()
        {
            var result = _expressionCreator.CreateExpression<bool>("TestClass.Field", typeof(TestClass));
            ExprAssert.AreEqual(() => TestClass.Field, result);
            result = _expressionCreator.CreateExpression<bool>("TestClass.Property", typeof(TestClass));
            ExprAssert.AreEqual(() => TestClass.Property, result);
        }

        [TestMethod]
        public void TestEnumComparison()
        {
            var result = _expressionCreator.CreateExpression<bool>("TestEnum.Value == TestEnum.Value2", typeof(TestEnum));
            ExprAssert.AreEqual(
                Expression.Lambda<Func<bool>>(Expression.Equal(Expression.Constant(TestEnum.Value), Expression.Constant(TestEnum.Value2))),
                result);

            result = _expressionCreator.CreateExpression<bool>("1 == TestEnum.Value2", typeof(TestEnum));
            ExprAssert.AreEqual(
                Expression.Lambda<Func<bool>>(Expression.Equal(Expression.Convert(Expression.Constant(1), typeof(TestEnum)), Expression.Constant(TestEnum.Value2))),
                result);
        
            result = _expressionCreator.CreateExpression<bool>("TestEnum.Value == 2", typeof(TestEnum));
            ExprAssert.AreEqual(
                Expression.Lambda<Func<bool>>(Expression.Equal(Expression.Constant(TestEnum.Value), Expression.Convert(Expression.Constant(2), typeof(TestEnum)))),
                result);;
        }

        [TestMethod]
        public void TestNullableValueTypeComparison()
        {
            var param = Expression.Parameter(typeof(TestClass));
            var method = StaticHelpers.MemberInfo.GetMethodInfo((TestClass c) => c.Nullable());

            var result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Nullable() == null");
            ExprAssert.AreEqual(
                Expression.Lambda<Func<TestClass, bool>>(Expression.Equal(Expression.Call(param, method), Expression.Constant(null)), param),
                result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "null == x.Nullable()");
            ExprAssert.AreEqual(
                Expression.Lambda<Func<TestClass, bool>>(Expression.Equal(Expression.Constant(null), Expression.Call(param, method)), param),
                result);
        }

        [TestMethod]
        public void TestCoalesce()
        {
            var result = _expressionCreator.CreateExpression<TestClass, string>("x", "x.String ?? \"1\"");
            ExprAssert.AreEqual(x => x.String ?? "1", result);

            var bResult = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Nullable() ?? true");
            ExprAssert.AreEqual(x => x.Nullable() ?? true, bResult);
        }

        [TestMethod]
        public void TestGloballyClosuredConsts()
        {
            var param = Expression.Parameter(typeof(TestClass));

            var result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Zero() == true", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.Zero() == true, result);
            ExprAssert.AreEqualWithConstants(x => x.Zero() == true, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Decimal > 1.0m", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.Decimal > 1.0m, result);
            ExprAssert.AreEqualWithConstants(x => x.Decimal > 1.0m, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Double > 1.0", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.Double > 1.0, result);
            ExprAssert.AreEqualWithConstants(x => x.Double > 1.0, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Float > 1.0f", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.Float > 1.0f, result);
            ExprAssert.AreEqualWithConstants(x => x.Float > 1.0f, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Long == 0x01l", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.Long == 1L, result);
            ExprAssert.AreEqualWithConstants(x => x.Long == 1L, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Byte > 0x01y", new ExpressionContextSettings { ClosureOverConstants = true });
            var expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.GreaterThan(Expression.Property(param, "Byte"), Expression.Constant((byte)1, typeof(byte))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Short > 0x01s", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.GreaterThan(Expression.Property(param, "Short"), Expression.Constant((short)1, typeof(short))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.String.Length == 0x01", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.String.Length == 1, result);
            ExprAssert.AreEqualWithConstants(x => x.String.Length == 1, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Long == 1l", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.Long == 1L, result);
            ExprAssert.AreEqualWithConstants(x => x.Long == 1L, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Byte > 1y", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.GreaterThan(Expression.Property(param, "Byte"), Expression.Constant((byte)1, typeof(byte))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Short > 1s", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.GreaterThan(Expression.Property(param, "Short"), Expression.Constant((short)1, typeof(short))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.String.Length == 1", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.String.Length == 1, result);
            ExprAssert.AreEqualWithConstants(x => x.String.Length == 1, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Date == date(2015-01-14)", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.Equal(Expression.Property(param, "Date"), Expression.Constant(new DateTime(2015, 1, 14))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Time == time(18:19:20)", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.Equal(Expression.Property(param, "Time"), Expression.Constant(new TimeSpan(18, 19, 20))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Uuid == uuid(11111111-1111-1111-1111-111111111111)", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.Equal(Expression.Property(param, "Uuid"), Expression.Constant(new Guid("11111111-1111-1111-1111-111111111111"))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.String == \"1\"", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.String == "1", result);
            ExprAssert.AreEqualWithConstants(x => x.String == "1", result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Char == '1'", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.Equal(Expression.Property(param, "Char"), Expression.Constant('1', typeof(char))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.String == \"1\" && x.Zero() && x.One(0)", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.String == "1" && x.Zero() && x.One(0), result);
            ExprAssert.AreEqualWithConstants(x => x.String == "1" && x.Zero() && x.One(0), result);
        }

        [TestMethod]
        public void TestLocallyClosuredConsts()
        {
            var param = Expression.Parameter(typeof(TestClass));

            var result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Zero() == `true", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.Zero() == true, result);
            ExprAssert.AreEqualWithConstants(x => x.Zero() == true, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Decimal > `1.0m", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.Decimal > 1.0m, result);
            ExprAssert.AreEqualWithConstants(x => x.Decimal > 1.0m, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Double > `1.0", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.Double > 1.0, result);
            ExprAssert.AreEqualWithConstants(x => x.Double > 1.0, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Float > `1.0f", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.Float > 1.0f, result);
            ExprAssert.AreEqualWithConstants(x => x.Float > 1.0f, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Long == `0x01l", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.Long == 1L, result);
            ExprAssert.AreEqualWithConstants(x => x.Long == 1L, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Byte > `0x01y", new ExpressionContextSettings { ClosureOverConstants = true });
            var expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.GreaterThan(Expression.Property(param, "Byte"), Expression.Constant((byte)1, typeof(byte))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Short > `0x01s", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.GreaterThan(Expression.Property(param, "Short"), Expression.Constant((short)1, typeof(short))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.String.Length == `0x01", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.String.Length == 1, result);
            ExprAssert.AreEqualWithConstants(x => x.String.Length == 1, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Long == `1l", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.Long == 1L, result);
            ExprAssert.AreEqualWithConstants(x => x.Long == 1L, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Byte > `1y", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.GreaterThan(Expression.Property(param, "Byte"), Expression.Constant((byte)1, typeof(byte))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Short > `1s", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.GreaterThan(Expression.Property(param, "Short"), Expression.Constant((short)1, typeof(short))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.String.Length == `1", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.String.Length == 1, result);
            ExprAssert.AreEqualWithConstants(x => x.String.Length == 1, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Date == `date(2015-01-14)", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.Equal(Expression.Property(param, "Date"), Expression.Constant(new DateTime(2015, 1, 14))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Time == `time(18:19:20)", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.Equal(Expression.Property(param, "Time"), Expression.Constant(new TimeSpan(18, 19, 20))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Uuid == `uuid(11111111-1111-1111-1111-111111111111)", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.Equal(Expression.Property(param, "Uuid"), Expression.Constant(new Guid("11111111-1111-1111-1111-111111111111"))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.String == `\"1\"", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.String == "1", result);
            ExprAssert.AreEqualWithConstants(x => x.String == "1", result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Char == `'1'", new ExpressionContextSettings { ClosureOverConstants = true });
            expected = Expression.Lambda<Func<TestClass, bool>>(
                    Expression.Equal(Expression.Property(param, "Char"), Expression.Constant('1', typeof(char))), param);
            ExprAssert.AreNotEqual(expected, result);
            ExprAssert.AreEqualWithConstants(expected, result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.String == `\"1\" && x.Zero() && x.One(`0)", new ExpressionContextSettings { ClosureOverConstants = true });
            ExprAssert.AreNotEqual(x => x.String == "1" && x.Zero() && x.One(0), result);
            ExprAssert.AreEqualWithConstants(x => x.String == "1" && x.Zero() && x.One(0), result);
        }

        [TestMethod]
        public void TestGenericOverloadResolution()
        {
            var result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.CuriousOverload(1, 1)");
            ExprAssert.AreEqual(x => x.CuriousOverload(1, 1), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.CuriousOverload(1, \"1\")");
            ExprAssert.AreEqual(x => x.CuriousOverload<int, string>(1, "1"), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.CuriousOverload(\"1\", 1)");
            ExprAssert.AreEqual(x => x.CuriousOverload<string>("1", 1), result);

            result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.CuriousOverload(\"1\", \"1\")");
            ExprAssert.AreEqual(x => x.CuriousOverload<string, string>("1", "1"), result);
        }

        [TestMethod]
        public void TestConstraintViolation()
        {
            ExceptAssert.Throws<UnresolvedMethodException>(
                () => _expressionCreator.CreateExpression<TestClass, bool>(
                    "x", "x.MethodWithGenericConstraint(0)"));

            var result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.MethodWithGenericConstraint(\"0\")");
            ExprAssert.AreEqual(x => x.MethodWithGenericConstraint("0"), result);
        }

        [TestMethod]
        public void TestQuotedLambdaArgument()
        {
            var result = _expressionCreator.CreateExpression<int[], bool>("x", "x.AsQueryable().Any(y => y == 0)", new ExpressionContextSettings {ExtensionTypes = new[] {typeof(Queryable)}});
            ExprAssert.AreEqual(x => x.AsQueryable().Any(y => y == 0), result);
        }

        [TestMethod]
        public void TestMethodReturningFunc()
        {
            var result = _expressionCreator.CreateExpression<int[], TestClass, bool>("x", "y", "x.Any(y.MethodReturningFunc())");
            ExprAssert.AreEqual((x, y) => x.Any(y.MethodReturningFunc()), result);

            result = _expressionCreator.CreateExpression<int[], TestClass, bool>("x", "y", "x.Select(y.MethodReturningFunc()).FirstOrDefault()");
            ExprAssert.AreEqual((x, y) => x.Select(y.MethodReturningFunc()).FirstOrDefault(), result);
        }

        [TestMethod]
        public void TestMethodWithGenericClassParameter()
        {
            var result = _expressionCreator.CreateExpression<GenericClass<int>, TestClass, bool>("x", "y", "y.MethodWithGenericClassParameter(x)");
            ExprAssert.AreEqual((x, y) => y.MethodWithGenericClassParameter(x), result);
        }

        [TestMethod]
        public void TestContravariantGeneric()
        {
            var result = _expressionCreator.CreateExpression<IContravariant<int>, TestClass, bool>("x", "y", "y.MethodWithContravariantParameter(x)");
            ExprAssert.AreEqual((x, y) => y.MethodWithContravariantParameter(x), result);

            var resultStr = _expressionCreator.CreateExpression<IContravariant<string>, TestClass, bool>("x", "y", "y.MethodWithContravariantParameter(x)");
            ExprAssert.AreEqual((x, y) => y.MethodWithContravariantParameter(x), resultStr);
        }

        [TestMethod]
        public void TestUnresolvableGeneric()
        {
            ExceptAssert.Throws<UnresolvedMethodException>(() => _expressionCreator.CreateExpression<TestClass, object>("x", "x.UnresolvableGeneric()"));
            ExceptAssert.Throws<UnresolvedMethodException>(() => _expressionCreator.CreateExpression<TestClass, object>("x", "x.UnresolvableInterdependentGeneric(y => y, z => z)"));
        }

        [TestMethod]
        public void TestInvalidLambdaArgument()
        {
            ExceptAssert.Throws<UnresolvedMethodException>(() => _expressionCreator.CreateExpression<int[], int>("x", "x.FirstOrDefault(y => y.NoSuchMethod())"));
        }

        [TestMethod]
        public void TestNullableTypeInference()
        {
            var result = _expressionCreator.CreateExpression<TestClass, int?, bool>("x", "y", "x.MethodWithNullableParameter(y)");
            ExprAssert.AreEqual((x, y) => x.MethodWithNullableParameter(y), result);
        }

        [TestMethod]
        public void TestNullableContravariantTypeInference()
        {
            var result = _expressionCreator.CreateExpression<TestClass, IContravariant<int?>, bool>("x", "y", "x.MethodWithNullableParameterOfContravariant(y)");
            ExprAssert.AreEqual((x, y) => x.MethodWithNullableParameterOfContravariant(y), result);
        }

        [TestMethod]
        public void TestArrayTypeInference()
        {
            var result = _expressionCreator.CreateExpression<TestClass, int[], bool>("x", "y", "x.MethodWithArrayParameter(y)");
            ExprAssert.AreEqual((x, y) => x.MethodWithArrayParameter(y), result);
        }

        [TestMethod]
        public void TestArrayContravariantTypeInference()
        {
            var result = _expressionCreator.CreateExpression<TestClass, IContravariant<int[]>, bool>("x", "y", "x.MethodWithArrayOfContravariant(y)");
            ExprAssert.AreEqual((x, y) => x.MethodWithArrayOfContravariant(y), result);
        }

        [TestMethod]
        public void TestMultidimensionalArrayTypeInference()
        {
            var result = _expressionCreator.CreateExpression<TestClass, int[,], bool>("x", "y", "x.MethodWithMultidimensionalArrayParameter(y)");
            ExprAssert.AreEqual((x, y) => x.MethodWithMultidimensionalArrayParameter(y), result);

            ExceptAssert.Throws<UnresolvedMethodException>(() =>
                _expressionCreator.CreateExpression<TestClass, int[], bool>("x", "y", "x.MethodWithMultidimensionalArrayParameter(y)"));
        }

        [TestMethod]
        public void TestMultidimensionalArrayContravariantTypeInference()
        {
            var result = _expressionCreator.CreateExpression<TestClass, IContravariant<int[,]>, bool>("x", "y", "x.MethodWithMultidimensionalArrayOfContravariant(y)");
            ExprAssert.AreEqual((x, y) => x.MethodWithMultidimensionalArrayOfContravariant(y), result);

            ExceptAssert.Throws<UnresolvedMethodException>(() =>
                _expressionCreator.CreateExpression<TestClass, int[], bool>("x", "y", "x.MethodWithMultidimensionalArrayOfContravariant(y)"));
        }

        [TestMethod]
        public void TestLinqNumericSelectors()
        {
            var resultLong = _expressionCreator.CreateExpression<IEnumerable<TestClass>, long>("x", "x.Max(y => y.Long)");
            ExprAssert.AreEqual(x => x.Max(y => y.Long), resultLong);
        }

        [TestMethod]
        public void TestLinqNonNumericSelectors()
        {
            var resultEnt = _expressionCreator.CreateExpression<TestEntity, TestEntity>("x", "x.Children.Max(y => y)");
            ExprAssert.AreEqual(x => x.Children.Max(y => y), resultEnt);
            resultEnt = _expressionCreator.CreateExpression<TestEntity, TestEntity>("x", "x.Children.Min(y => y)");
            ExprAssert.AreEqual(x => x.Children.Min(y => y), resultEnt);
        }

        [TestMethod]
        public void TestLambdasWithBuiltinParamTypes()
        {
            var resultDouble = _expressionCreator.CreateExpression<TestEntity, double>("x", "x.Children.Select(c => c.Prop1).Average((int i) => i)");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Prop1).Average(i => i), resultDouble);

            var resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<int>>("x", "x.Children.Select(c => c.Prop1).Select((int i, int index) => i + index)");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Prop1).Select((i, index) => i + index), resultColl);

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<int>>("x", "x.Children.Select(c => c.Prop1).Select((int i, index) => i + index)");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Prop1).Select((i, index) => i + index), resultColl);

            resultColl = _expressionCreator.CreateExpression<TestEntity, IEnumerable<int>>("x", "x.Children.Select(c => c.Prop1).Select((i, int index) => i + index)");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Prop1).Select((i, index) => i + index), resultColl);
        }

        [TestMethod]
        public void TestLambdaWithCustomParamTypes()
        {
            var result = _expressionCreator.CreateExpression<TestEntity, IEnumerable<int>>("x", "x.Children.Select((BaseTestClasses.TestEntity/BaseTestClasses c) => c.Prop1)");
            ExprAssert.AreEqual(x => x.Children.Select(c => c.Prop1), result);
        }

        [TestMethod]
        public void TestMethodCallsWithBuiltinTypeParams()
        {
            var result = _expressionCreator.CreateExpression<TestClass, bool>("x", "x.Generic<int>(0)");
            ExprAssert.AreEqual(x => x.Generic(0), result);

            var result2 = _expressionCreator.CreateExpression<TestClass, int, bool>("x", "y", "x.Generic<double>(y)");
            ExprAssert.AreEqual((x, y) => x.Generic((double)y), result2);
        }

        [TestMethod]
        public void TestMethodCallsWithCustomTypeParams()
        {
            var result = _expressionCreator.CreateExpression<TestClass, From, bool>("x", "y", "x.Generic<BaseTestClasses.IFrom/BaseTestClasses>(y)");
            ExprAssert.AreEqual((x, y) => x.Generic<IFrom>(y), result);

            var ifromResult = _expressionCreator.CreateExpression<TestClass, From, IFrom>("x", "y", "x.UnresolvableGeneric<BaseTestClasses.IFrom/BaseTestClasses>()");
            ExprAssert.AreEqual((x, y) => x.UnresolvableGeneric<IFrom>(), ifromResult);

            result = _expressionCreator.CreateExpression<TestClass, From, bool>("x", "y", "x.UnresolvableInterdependentGeneric<BaseTestClasses.From/BaseTestClasses, BaseTestClasses.From/BaseTestClasses>(f => f, f => f)");
            ExprAssert.AreEqual((x, y) => x.UnresolvableInterdependentGeneric<From, From>(f => f, f => f), result);
        }

        [TestMethod]
        public void TestTypeConstants()
        {
            var result = _expressionCreator.CreateExpression<Type>("type(int)");
            ExprAssert.AreEqual(() => typeof(int), result);

            result = _expressionCreator.CreateExpression<Type>("type(uint)");
            ExprAssert.AreEqual(() => typeof(uint), result);

            result = _expressionCreator.CreateExpression<Type>("type(BaseTestClasses.IFrom/BaseTestClasses)");
            ExprAssert.AreEqual(() => typeof(IFrom), result);

            result = _expressionCreator.CreateExpression<Type>("type(TsSoft.Expressions.PseudoExpressionParser.Tests.ExpressionCreator.ExpressionCreatorIntegrationTests+TestClass/TsSoft.Expressions.PseudoExpressionParser.Tests)");
            ExprAssert.AreEqual(() => typeof(TestClass), result);
        }

        [TestMethod]
        public void TestCasts()
        {
            var result = _expressionCreator.CreateExpression<int, long>("x", "x cast long");
            ExprAssert.AreEqual(x => (long)x, result);

            var resultBool = _expressionCreator.CreateExpression<int, bool>("x", "x is long");
#pragma warning disable 184
            ExprAssert.AreEqual(x => x is long, resultBool);
#pragma warning restore 184

            var resultNullable = _expressionCreator.CreateExpression<From, IFrom>("x", "x as BaseTestClasses.IFrom/BaseTestClasses");
            ExprAssert.AreEqual(x => x as IFrom, resultNullable);
        }

        [TestMethod]
        public void TestCallMethodsOnDynamicTypes()
        {
            var settings = new ExpressionContextSettings {AllowStaticCallsOnTypeConstants = true};

            var result = _expressionCreator.CreateExpression<string>("static(string).Empty", settings);
            ExprAssert.AreEqual(() => string.Empty, result);

            result = _expressionCreator.CreateExpression<string>("static(string).Concat(\"1\", \"2\")", settings);
            ExprAssert.AreEqual(() => string.Concat("1", "2"), result);

            var resultBool = _expressionCreator.CreateExpression<bool>("static(TsSoft.Expressions.PseudoExpressionParser.Tests.ExpressionCreator.ExpressionCreatorIntegrationTests+TestClass/TsSoft.Expressions.PseudoExpressionParser.Tests).Field", settings);
            ExprAssert.AreEqual(() => TestClass.Field, resultBool);

            resultBool = _expressionCreator.CreateExpression<bool>("static(TsSoft.Expressions.PseudoExpressionParser.Tests.ExpressionCreator.ExpressionCreatorIntegrationTests+TestClass/TsSoft.Expressions.PseudoExpressionParser.Tests).Property", settings);
            ExprAssert.AreEqual(() => TestClass.Property, resultBool);

            resultBool = _expressionCreator.CreateExpression<bool>("static(TsSoft.Expressions.PseudoExpressionParser.Tests.ExpressionCreator.ExpressionCreatorIntegrationTests+TestClass/TsSoft.Expressions.PseudoExpressionParser.Tests).Method()", settings);
            ExprAssert.AreEqual(() => TestClass.Method(), resultBool);

            ExceptAssert.Throws<InvalidSyntaxException>(() => _expressionCreator.CreateExpression<string>("static(string).Empty"));
        }

        class TestClass
        {
            public bool Zero()
            {
                return false;
            }
            public bool One(int one)
            {
                return false;
            }
            public bool OneOverload(int one)
            {
                return false;
            }
            public bool OneOverload(string one)
            {
                return false;
            }
            public bool Two(int one, int two)
            {
                return false;
            }
            public bool TwoOverload(int one, string two)
            {
                return false;
            }
            public bool TwoOverload(string one, int two)
            {
                return false;
            }
            public bool TwoOverload(int one, int two)
            {
                return false;
            }
            public bool Generic<T>(T one)
            {
                return false;
            }
            public bool Generic2<T>(int one, T two)
            {
                return false;
            }
            public bool Generic3<T1, T2>(T1 one, T2 two)
            {
                return false;
            }
            public bool GenericOverload<T>(T one, int two)
            {
                return false;
            }
            public bool GenericOverload<T>(T one, string two)
            {
                return false;
            }

            public bool? Nullable()
            {
                return null;
            }

            public string String { get; set; }
            public long Long { get; set; }
            public float Float { get; set; }
            public double Double { get; set; }
            public byte Byte { get; set; }
            public short Short { get; set; }
            public decimal Decimal { get; set; }
            public DateTime Date { get; set; }
            public TimeSpan Time { get; set; }
            public Guid Uuid { get; set; }
            public char Char { get; set; }

            public TestClass Inner { get; set; }

            public static bool Field = true;
            public static bool Property { get { return true; } }
            public static bool Method() {return true;}

            public bool CuriousOverload(int i1, int i2){return false;}
            public bool CuriousOverload<T>(T i1, int i2) { return false; }
            public bool CuriousOverload<T1, T2>(T1 i1, T2 i2) { return false; }

            public bool MethodWithContravariantParameter<T>(IContravariant<T> obj) {return false;}
            public bool MethodWithGenericClassParameter<T>(GenericClass<T> obj) {return false;}
            public bool MethodWithGenericConstraint<T>(T obj) where T: class {return false;}

            public bool MethodWithNullableParameter<T>(T? obj) where T: struct {return false;}
            public bool MethodWithArrayParameter<T>(T[] obj) { return false; }
            public bool MethodWithMultidimensionalArrayParameter<T>(T[,] obj) { return false; }
            public bool MethodWithNullableParameterOfContravariant<T>(IContravariant<T?> obj) where T : struct { return false; }
            public bool MethodWithArrayOfContravariant<T>(IContravariant<T[]> obj) {return false;}
            public bool MethodWithMultidimensionalArrayOfContravariant<T>(IContravariant<T[,]> obj) { return false; }

            public Func<int, bool> MethodReturningFunc() {return i => i == 0;}

            [UsedImplicitly]
            public T UnresolvableGeneric<T>() {return default(T);}

            [UsedImplicitly]
            public bool UnresolvableInterdependentGeneric<T1, T2>(Func<T1, T2> f1, Func<T2, T1> f2) { return false; }
        }

        private class Base
        {
        }

        class Derived : Base
        {
        }

        interface IContravariant<in T>
        {
        }

        class GenericClass<T>
        {
        }

        enum TestEnum
        {
            Value = 1,
            Value2 = 2,
        }
    }

}
