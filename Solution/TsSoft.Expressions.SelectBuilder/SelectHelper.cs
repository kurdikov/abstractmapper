﻿namespace TsSoft.Expressions.SelectBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Helpers;
    using TsSoft.Expressions.SelectBuilder.Models;

    internal class SelectHelper : ISelectHelper
    {
        [NotNull]private readonly IPathParser _pathParser;
        [NotNull] private readonly IIncludeToSelectTreeConverter _includeToSelectTreeConverter;
        [NotNull] private readonly ISelectTreeToSelectConverter _treeToSelectConverter;
        [NotNull] private readonly ISelectTreeMerger _merger;
        [NotNull] private readonly ISelectToSelectTreeConverter _selectToSelectTreeConverter;
        [NotNull] private readonly IParsedPathsToSelectTreeConverter _parsedPathToSelectTreeConverter;
        [NotNull] private readonly ITypeToSelectTreeConverter _typeToSelectTreeConverter;

        public SelectHelper(
            [NotNull] IPathParser pathParser,
            [NotNull] IIncludeToSelectTreeConverter includeToSelectTreeConverter,
            [NotNull] ISelectTreeToSelectConverter treeToSelectConverter,
            [NotNull] ISelectTreeMerger merger,
            [NotNull] ISelectToSelectTreeConverter selectToSelectTreeConverter,
            [NotNull] IParsedPathsToSelectTreeConverter parsedPathToSelectTreeConverter,
            [NotNull] ITypeToSelectTreeConverter typeToSelectTreeConverter)
        {
            if (pathParser == null) throw new ArgumentNullException("pathParser");
            if (includeToSelectTreeConverter == null) throw new ArgumentNullException("includeToSelectTreeConverter");
            if (treeToSelectConverter == null) throw new ArgumentNullException("treeToSelectConverter");
            if (merger == null) throw new ArgumentNullException("merger");
            if (selectToSelectTreeConverter == null) throw new ArgumentNullException("selectToSelectTreeConverter");
            if (parsedPathToSelectTreeConverter == null) throw new ArgumentNullException("parsedPathToSelectTreeConverter");
            if (typeToSelectTreeConverter == null) throw new ArgumentNullException("typeToSelectTreeConverter");
            _pathParser = pathParser;
            _includeToSelectTreeConverter = includeToSelectTreeConverter;
            _treeToSelectConverter = treeToSelectConverter;
            _merger = merger;
            _selectToSelectTreeConverter = selectToSelectTreeConverter;
            _parsedPathToSelectTreeConverter = parsedPathToSelectTreeConverter;
            _typeToSelectTreeConverter = typeToSelectTreeConverter;
        }

        public SelectExpression<TEntity> CreateFromIncludes<TEntity>(IReadOnlyIncludes<TEntity> includes)
        {
            return CreateFromIncludes(includes, new SelectSettings());
        }

        public SelectExpression<TEntity> CreateFromIncludes<TEntity>(IReadOnlyIncludes<TEntity> includes, SelectSettings settings)
        {
            var tree = _includeToSelectTreeConverter.Convert(includes);
            var select = _treeToSelectConverter.Convert<TEntity>(tree, settings);
            return select;
        }

        public SelectExpression<TEntity> Merge<TEntity>(
            IReadOnlySelectExpression<TEntity> first,
            IReadOnlySelectExpression<TEntity> second)
        {
            return Merge(first, second, new SelectSettings());
        }

        public SelectExpression<TEntity> Merge<TEntity>(IReadOnlySelectExpression<TEntity> first, IReadOnlySelectExpression<TEntity> second, SelectSettings settings)
        {
            var resultTree = _selectToSelectTreeConverter.Convert(first);
            _merger.Merge(resultTree, _selectToSelectTreeConverter.Convert(second));
            var select = _treeToSelectConverter.ConvertDbTree<TEntity>(resultTree, settings);
            return select;
        }

        private void InternalAddSelect<TEntity>(
            [NotNull]SelectExpression<TEntity> select,
            [NotNull]LambdaExpression path,
            [NotNull]SelectTree tree,
            [NotNull]SelectSettings settings)
        {
            var parsedPath = _pathParser.Parse(path);
            var addedTree = _parsedPathToSelectTreeConverter.Convert(
                typeof(TEntity), new KeyValuePair<ParsedPath, SelectTree>(parsedPath, tree).ToEnumerable());
            var selectTree = _selectToSelectTreeConverter.Convert(select);
            _merger.Merge(selectTree, addedTree);
            var result = _treeToSelectConverter.ConvertDbTree<TEntity>(selectTree, settings);
            select.Select = result.Select;
            select.ExternalIncludes = result.ExternalIncludes;
        }

        private void InternalAddSelect<TEntity, TDependentEntity>(
            [NotNull]SelectExpression<TEntity> select,
            [NotNull]IReadOnlySelectExpression<TDependentEntity> innerSelect,
            [NotNull]LambdaExpression path,
            [NotNull]SelectSettings settings)
        {
            var tree = _selectToSelectTreeConverter.Convert(innerSelect);
            InternalAddSelect(select, path, tree, settings);
        }

        private void InternalAddSelect<TEntity, TDependentEntity>(
            [NotNull]SelectExpression<TEntity> select,
            [NotNull]ISelectProvider<TDependentEntity> processor,
            [NotNull]LambdaExpression path,
            [NotNull]SelectSettings settings)
        {
            InternalAddSelect(select, processor.Select, path, settings);
        }

        public void AddSelect<TEntity, TDependentEntity>(
            SelectExpression<TEntity> select, 
            ISelectProvider<TDependentEntity> processor, 
            Expression<Func<TEntity, TDependentEntity>> path,
            SelectSettings settings)
        {
            InternalAddSelect(select, processor, path, settings);
        }

        public void AddSelect<TEntity, TDependentEntity>(
            SelectExpression<TEntity> select, 
            ISelectProvider<TDependentEntity> processor, 
            Expression<Func<TEntity, IEnumerable<TDependentEntity>>> path,
            SelectSettings settings)
        {
            InternalAddSelect(select, processor, path, settings);
        }

        public void AddSelect<TEntity, TDependentEntity>(
            SelectExpression<TEntity> select, 
            IReadOnlySelectExpression<TDependentEntity> innerSelect, 
            Expression<Func<TEntity, IEnumerable<TDependentEntity>>> path,
            SelectSettings settings)
        {
            InternalAddSelect(select, innerSelect, path, settings);
        }

        public void AddSelect<TEntity, TDependentEntity>(
            SelectExpression<TEntity> select, 
            IReadOnlySelectExpression<TDependentEntity> innerSelect, 
            Expression<Func<TEntity, TDependentEntity>> path,
            SelectSettings settings)
        {
            InternalAddSelect(select, innerSelect, path, settings);
        }

        public void AddSelect<TEntity, TDependentEntity>(
            SelectExpression<TEntity> select,
            ISelectProvider<TDependentEntity> processor,
            Expression<Func<TEntity, TDependentEntity>> path)
        {
            InternalAddSelect(select, processor, path, new SelectSettings());
        }

        public void AddSelect<TEntity, TDependentEntity>(
            SelectExpression<TEntity> select,
            ISelectProvider<TDependentEntity> processor,
            Expression<Func<TEntity, IEnumerable<TDependentEntity>>> path)
        {
            InternalAddSelect(select, processor, path, new SelectSettings());
        }

        public void AddSelect<TEntity, TDependentEntity>(
            SelectExpression<TEntity> select,
            IReadOnlySelectExpression<TDependentEntity> innerSelect,
            Expression<Func<TEntity, IEnumerable<TDependentEntity>>> path)
        {
            InternalAddSelect(select, innerSelect, path, new SelectSettings());
        }

        public void AddSelect<TEntity, TDependentEntity>(
            SelectExpression<TEntity> select,
            IReadOnlySelectExpression<TDependentEntity> innerSelect,
            Expression<Func<TEntity, TDependentEntity>> path)
        {
            InternalAddSelect(select, innerSelect, path, new SelectSettings());
        }
        
        public SelectExpression<TEntity> CreateFromInterface<TEntity, TInterface>(SelectSettings settings, bool ignoreEntityTypes = false) where TEntity : TInterface
        {
            var tree = _typeToSelectTreeConverter.MakeTree<TEntity, TInterface>(ignoreEntityTypes);
            if (tree.RootNode.NextLevel == null || !tree.RootNode.NextLevel.Any())
            {
                throw new InvalidOperationException(string.Format("No properties in interface {0}", typeof(TInterface)));
            }
            var result = _treeToSelectConverter.TransformAndConvert<TEntity>(tree, settings);
            return result;
        }

        public SelectExpression<TEntity> CreateFromInterface<TEntity, TInterface>(bool ignoreEntityTypes = false) where TEntity : TInterface
        {
            return CreateFromInterface<TEntity, TInterface>(new SelectSettings(), ignoreEntityTypes);
        }

        public SelectExpression<TEntity> CreateByPrimaryKey<TEntity>()
        {
            var tree = _typeToSelectTreeConverter.MakePkTree<TEntity>();
            var result = _treeToSelectConverter.TransformAndConvert<TEntity>(tree, new SelectSettings());
            return result;
        }
    }
}
