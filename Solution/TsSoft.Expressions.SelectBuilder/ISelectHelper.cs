﻿namespace TsSoft.Expressions.SelectBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Хелпер для построения select-выражений
    /// </summary>
    public interface ISelectHelper
    {
        /// <summary>
        /// Слить два select-выражения
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности БД</typeparam>
        /// <param name="first">Первое выражение</param>
        /// <param name="second">Второе выражение</param>
        [NotNull]
        SelectExpression<TEntity> Merge<TEntity>([NotNull]IReadOnlySelectExpression<TEntity> first, [NotNull]IReadOnlySelectExpression<TEntity> second);

        /// <summary>
        /// Слить два select-выражения
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности БД</typeparam>
        /// <param name="first">Первое выражение</param>
        /// <param name="second">Второе выражение</param>
        /// <param name="settings">Настройки генерации селекта</param>
        [NotNull]
        SelectExpression<TEntity> Merge<TEntity>([NotNull]IReadOnlySelectExpression<TEntity> first, [NotNull]IReadOnlySelectExpression<TEntity> second, [NotNull]SelectSettings settings);

        /// <summary>
        /// Создать select-выражение по include-выражениям
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности БД</typeparam>
        /// <param name="includes">Include-выражения</param>
        [NotNull]
        SelectExpression<TEntity> CreateFromIncludes<TEntity>(IReadOnlyIncludes<TEntity> includes);

        /// <summary>
        /// Создать select-выражение по include-выражениям
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности БД</typeparam>
        /// <param name="includes">Include-выражения</param>
        /// <param name="settings">Настройки генерации селекта</param>
        [NotNull]
        SelectExpression<TEntity> CreateFromIncludes<TEntity>(IReadOnlyIncludes<TEntity> includes, [NotNull]SelectSettings settings);

        /// <summary>
        /// Создать select-выражение по интерфейсу сущности, выбирающее все свойства из интерфейса
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности БД</typeparam>
        /// <typeparam name="TInterface">Тип реализуемого сущностью интерфейса</typeparam>
        [NotNull]
        SelectExpression<TEntity> CreateFromInterface<TEntity, TInterface>(bool ignoreEntityTypes = false) where TEntity : TInterface;

        /// <summary>
        /// Создать select-выражение по интерфейсу сущности, выбирающее все свойства из интерфейса
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности БД</typeparam>
        /// <typeparam name="TInterface">Тип реализуемого сущностью интерфейса</typeparam>
        [NotNull]
        SelectExpression<TEntity> CreateFromInterface<TEntity, TInterface>([NotNull]SelectSettings settings, bool ignoreEntityTypes = false) where TEntity : TInterface;

        /// <summary>
        /// Создать select-выражение для выбора первичного ключа сущности
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        [NotNull]
        SelectExpression<TEntity> CreateByPrimaryKey<TEntity>();

        /// <summary>
        /// Добавить в select-выражение свойства, используемые внутренним обработчиком
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности select-выражения</typeparam>
        /// <typeparam name="TDependentEntity">Тип сущности внутреннего обработчика</typeparam>
        /// <param name="select">Текущее select-выражение</param>
        /// <param name="processor">Внутренний обработчик</param>
        /// <param name="path">Передаваемый внутреннему обработчику путь</param>
        void AddSelect<TEntity, TDependentEntity>(
            [NotNull]SelectExpression<TEntity> select,
            [NotNull]ISelectProvider<TDependentEntity> processor,
            [NotNull]Expression<Func<TEntity, TDependentEntity>> path);

        /// <summary>
        /// Добавить в select-выражение свойства, используемые внутренним обработчиком
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности select-выражения</typeparam>
        /// <typeparam name="TDependentEntity">Тип сущности внутреннего обработчика</typeparam>
        /// <param name="select">Текущее select-выражение</param>
        /// <param name="processor">Внутренний обработчик</param>
        /// <param name="path">Передаваемый внутреннему обработчику путь</param>
        void AddSelect<TEntity, TDependentEntity>(
            [NotNull]SelectExpression<TEntity> select,
            [NotNull]ISelectProvider<TDependentEntity> processor,
            [NotNull]Expression<Func<TEntity, IEnumerable<TDependentEntity>>> path);

        /// <summary>
        /// Добавить в select-выражение свойства, используемые внутренним обработчиком
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности select-выражения</typeparam>
        /// <typeparam name="TDependentEntity">Тип сущности внутреннего обработчика</typeparam>
        /// <param name="select">Текущее select-выражение</param>
        /// <param name="innerSelect">Select-выражение внутреннего обработчика</param>
        /// <param name="path">Передаваемый внутреннему обработчику путь</param>
        void AddSelect<TEntity, TDependentEntity>(
            [NotNull]SelectExpression<TEntity> select,
            [NotNull]IReadOnlySelectExpression<TDependentEntity> innerSelect,
            [NotNull]Expression<Func<TEntity, TDependentEntity>> path);

        /// <summary>
        /// Добавить в select-выражение свойства, используемые внутренним обработчиком
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности select-выражения</typeparam>
        /// <typeparam name="TDependentEntity">Тип сущности внутреннего обработчика</typeparam>
        /// <param name="select">Текущее select-выражение</param>
        /// <param name="innerSelect">Select-выражение внутреннего обработчика</param>
        /// <param name="path">Передаваемый внутреннему обработчику путь</param>
        void AddSelect<TEntity, TDependentEntity>(
            [NotNull]SelectExpression<TEntity> select,
            [NotNull]IReadOnlySelectExpression<TDependentEntity> innerSelect,
            [NotNull]Expression<Func<TEntity, IEnumerable<TDependentEntity>>> path);

        /// <summary>
        /// Добавить в select-выражение свойства, используемые внутренним обработчиком
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности select-выражения</typeparam>
        /// <typeparam name="TDependentEntity">Тип сущности внутреннего обработчика</typeparam>
        /// <param name="select">Текущее select-выражение</param>
        /// <param name="processor">Внутренний обработчик</param>
        /// <param name="path">Передаваемый внутреннему обработчику путь</param>
        /// <param name="settings">Настройки генерации селекта</param>
        void AddSelect<TEntity, TDependentEntity>(
            [NotNull]SelectExpression<TEntity> select,
            [NotNull]ISelectProvider<TDependentEntity> processor,
            [NotNull]Expression<Func<TEntity, TDependentEntity>> path,
            [NotNull]SelectSettings settings);

        /// <summary>
        /// Добавить в select-выражение свойства, используемые внутренним обработчиком
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности select-выражения</typeparam>
        /// <typeparam name="TDependentEntity">Тип сущности внутреннего обработчика</typeparam>
        /// <param name="select">Текущее select-выражение</param>
        /// <param name="processor">Внутренний обработчик</param>
        /// <param name="path">Передаваемый внутреннему обработчику путь</param>
        /// <param name="settings">Настройки генерации селекта</param>
        void AddSelect<TEntity, TDependentEntity>(
            [NotNull]SelectExpression<TEntity> select,
            [NotNull]ISelectProvider<TDependentEntity> processor,
            [NotNull]Expression<Func<TEntity, IEnumerable<TDependentEntity>>> path,
            [NotNull]SelectSettings settings);

        /// <summary>
        /// Добавить в select-выражение свойства, используемые внутренним обработчиком
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности select-выражения</typeparam>
        /// <typeparam name="TDependentEntity">Тип сущности внутреннего обработчика</typeparam>
        /// <param name="select">Текущее select-выражение</param>
        /// <param name="innerSelect">Select-выражение внутреннего обработчика</param>
        /// <param name="path">Передаваемый внутреннему обработчику путь</param>
        /// <param name="settings">Настройки генерации селекта</param>
        void AddSelect<TEntity, TDependentEntity>(
            [NotNull]SelectExpression<TEntity> select,
            [NotNull]IReadOnlySelectExpression<TDependentEntity> innerSelect,
            [NotNull]Expression<Func<TEntity, TDependentEntity>> path,
            [NotNull]SelectSettings settings);

        /// <summary>
        /// Добавить в select-выражение свойства, используемые внутренним обработчиком
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности select-выражения</typeparam>
        /// <typeparam name="TDependentEntity">Тип сущности внутреннего обработчика</typeparam>
        /// <param name="select">Текущее select-выражение</param>
        /// <param name="innerSelect">Select-выражение внутреннего обработчика</param>
        /// <param name="path">Передаваемый внутреннему обработчику путь</param>
        /// <param name="settings">Настройки генерации селекта</param>
        void AddSelect<TEntity, TDependentEntity>(
            [NotNull]SelectExpression<TEntity> select,
            [NotNull]IReadOnlySelectExpression<TDependentEntity> innerSelect,
            [NotNull]Expression<Func<TEntity, IEnumerable<TDependentEntity>>> path,
            [NotNull]SelectSettings settings);
    }
}
