﻿namespace TsSoft.Expressions.SelectBuilder.DynamicTypes
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.DynamicTypes;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    internal class TypeClonemaker : ITypeClonemaker
    {
        [NotNull] private readonly IPropertyCreator _propertyCreator;

        public TypeClonemaker([NotNull] IPropertyCreator propertyCreator)
        {
            if (propertyCreator == null) throw new ArgumentNullException("propertyCreator");
            _propertyCreator = propertyCreator;
        }

        [NotNull]
        private Type GetReplacementPrimitiveType([NotNull]Type sourceType, [NotNull]Func<Type, Type> replaceWith)
        {
            Type result = replaceWith(sourceType) ?? sourceType;
            return result;
        }

        [NotNull]
        private Type GetReplacementType([NotNull]Type sourceType, [NotNull]Func<Type, Type> replaceWith, [NotNull]Type genericCollectionType)
        {
            return sourceType.IsGenericEnumerable()
                ? genericCollectionType.MakeGenericType(GetReplacementPrimitiveType(sourceType.GetGenericEnumerableArgument(), replaceWith))
                : GetReplacementPrimitiveType(sourceType, replaceWith);
        }

        private void CloneProperties(
            [NotNull]Type source,
            [NotNull]Func<Type, Type> replaceWith,
            [NotNull]Type genericCollectionType,
            [NotNull]Func<ValueHoldingMember, bool> ignore,
            [NotNull]Action<string, Type> cloneFunc)
        {
            if (genericCollectionType == null || !genericCollectionType.GetTypeInfo().IsGenericTypeDefinition)
            {
                throw new ArgumentException(
                    "The collection type is not generic",
                    "genericCollectionType");
            }
            foreach (var property in ValueHoldingMember.GetValueHoldingMembers(source).Where(pi => !ignore(pi)))
            {
                var fieldType = GetReplacementType(property.ValueType, replaceWith, genericCollectionType);
                cloneFunc(property.Name, fieldType);
            }
        }

        public void CloneAsFields(
            Type source, 
            TypeBuilder builder, 
            Func<Type, Type> replaceWith,
            Type genericCollectionType,
            Func<ValueHoldingMember, bool> ignore)
        {
            CloneProperties(source, replaceWith, genericCollectionType, ignore,
                (name, type) => builder.DefineField(name, type, FieldAttributes.Public));
        }

        public void CloneAsProperties(
            Type source,
            TypeBuilder builder,
            Func<Type, Type> replaceWith,
            Type genericCollectionType,
            Func<ValueHoldingMember, bool> ignore)
        {
            CloneProperties(source, replaceWith, genericCollectionType, ignore,
                (name, type) => _propertyCreator.CreateAutoProperty(builder, type, name));
        }

        public void CloneAsInterfaceProperties(
            Type source, 
            TypeBuilder builder, 
            Func<Type, Type> replaceWith, 
            Type genericCollectionType,
            Func<ValueHoldingMember, bool> ignore)
        {
            CloneProperties(source, replaceWith, genericCollectionType, ignore,
                (name, type) => _propertyCreator.DefineProperty(builder, type, name));
        }
    }
}
