﻿namespace TsSoft.Expressions.SelectBuilder.DynamicTypes
{
    using System;
    using System.Reflection;
    using System.Reflection.Emit;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.DynamicTypes;

    internal class TypeBuilderCreator : AssemblyBuilderContainer, ITypeBuilderCreator
    {
        private const string DynamicAssemblyName = "TsSoft.Expressions.SelectBuilder.Dynamic";

        [NotNull]private static readonly object LockObj = new object();

        public TypeBuilderCreator()
            : base(DynamicAssemblyName)
        {
        }

        [NotNull]
        private TypeBuilder CheckTypeNameAndCreate([NotNull]string typeName, TypeAttributes attributes, Type baseType = null)
        {
            lock (LockObj)
            {
                TypeBuilder result = ModuleBuilder.DefineType(typeName, attributes, baseType);
                return result;
            }
        }

        public TypeBuilder CreateWithDefaultConstructor(string typeName)
        {
            return CreateWithDefaultConstructor(typeName, null);
        }

        public TypeBuilder CreateWithDefaultConstructor(string typeName, Type baseType)
        {
            var result = CheckTypeNameAndCreate(typeName, PublicClass, baseType);
            result.DefineDefaultConstructor(
                MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName);
            return result;
        }

        public TypeBuilder CreateInterface(string typeName)
        {
            var result = CheckTypeNameAndCreate(typeName, PublicInterface);
            return result;
        }
    }
}
