﻿namespace TsSoft.Expressions.SelectBuilder.DynamicTypes
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Пул динамических типов сущностей
    /// </summary>
    public interface IDynamicTypePool
    {
        /// <summary>
        /// Сбросить пул
        /// </summary>
        void Reset();

        /// <summary>
        /// Получить следующий экземпляр динамического типа
        /// </summary>
        /// <param name="type">Статический тип</param>
        [NotNull]
        Type GetNext([NotNull]Type type);
    }
}
