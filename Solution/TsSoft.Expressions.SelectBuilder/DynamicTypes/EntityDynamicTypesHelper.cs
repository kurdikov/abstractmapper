﻿namespace TsSoft.Expressions.SelectBuilder.DynamicTypes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.DynamicTypes;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    internal class EntityDynamicTypesHelper : IEntityDynamicTypesHelper
    {
        [NotNull]private readonly IEntityTypesRetriever _retriever;
        [NotNull]private readonly ITypeBuilderCreator _typeBuilderCreator;
        [NotNull]private readonly ITypeClonemaker _typeClonemaker;
        [NotNull]private readonly IEntityTypesHelper _entityTypesHelper;
        [NotNull]private readonly IPropertyCreator _propertyCreator;
        [NotNull]private readonly IMemberNamingHelper _memberNamingHelper;

        [NotNull]
        private readonly IDictionary<Type, Type> _entityToInterface = new Dictionary<Type, Type>();
        [NotNull]
        private readonly LazyConcurrentDictionary<KeyValuePair<Type, int>, Type> _entityToDynamic =
            new LazyConcurrentDictionary<KeyValuePair<Type, int>, Type>();

        public EntityDynamicTypesHelper(
            [NotNull]IEntityTypesRetriever retriever,
            [NotNull]ITypeBuilderCreator typeBuilderCreator,
            [NotNull]ITypeClonemaker typeClonemaker,
            [NotNull]IEntityTypesHelper entityTypesHelper,
            [NotNull]IPropertyCreator propertyCreator,
            [NotNull]IMemberNamingHelper memberNamingHelper)
        {
            if (retriever == null) throw new ArgumentNullException("retriever");
            if (typeBuilderCreator == null) throw new ArgumentNullException("typeBuilderCreator");
            if (typeClonemaker == null) throw new ArgumentNullException("typeClonemaker");
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            if (propertyCreator == null) throw new ArgumentNullException("propertyCreator");
            if (memberNamingHelper == null) throw new ArgumentNullException("memberNamingHelper");
            _retriever = retriever;
            _typeBuilderCreator = typeBuilderCreator;
            _typeClonemaker = typeClonemaker;
            _entityTypesHelper = entityTypesHelper;
            _propertyCreator = propertyCreator;
            _memberNamingHelper = memberNamingHelper;

            var builderLookup = new Dictionary<Type, TypeBuilder>();
            var entityTypes = (retriever.GetEntityTypes() ?? Enumerable.Empty<Type>()).ToList();
            foreach (var type in entityTypes)
            {
                if (type == null)
                {
                    continue;
                }
                builderLookup.Add(type, typeBuilderCreator.CreateInterface(string.Format("IDynamic{0}_{1:N}", type.Name, Guid.NewGuid())));
            }
            foreach (var type in entityTypes)
            {
                if (type == null)
                {
                    continue;
                }
                var builder = builderLookup[type];
                if (builder == null)
                {
                    throw new InvalidOperationException(string.Format("Null builder for dynamic {0}", type));
                }
                typeClonemaker.CloneAsInterfaceProperties(
                    type,
                    builder,
                    t => t != null && _retriever.IsEntityType(t) ? builderLookup[t].AsType() : t, 
                    typeof(IEnumerable<>),
                    _entityTypesHelper.IsNonDbProperty);
                _entityToInterface.Add(type, builder.CreateTypeInfo().AsType());
            }
        }

        private Type GetBaseTypeForProperty([NotNull] Type type)
        {
            if (_retriever.IsEntityType(type))
            {
                return IsDerivable(type) ? type : _entityToInterface[type];
            }
            if (type.GetTypeInfo().IsInterface)
            {
                return typeof(object);
            }
            return type;
        }

        private Type CreateNewDynamicType([NotNull]Type type, int id)
        {
            var builder = _typeBuilderCreator.CreateWithDefaultConstructor(string.Format("<Dynamic>{0}{1}_{2:N}", type.Name, id, Guid.NewGuid()));
            Type dynamicInterface;
            if (_entityToInterface.TryGetValue(type, out dynamicInterface) && dynamicInterface != null)
            {
                builder.AddInterfaceImplementation(dynamicInterface);
            }
            _typeClonemaker.CloneAsProperties(type, builder,
                GetBaseTypeForProperty,
                typeof(IEnumerable<>),
                _entityTypesHelper.IsNonDbProperty);
            return builder.CreateTypeInfo().AsType();
        }

        private Type CreateNewDerivedType([NotNull]Type type, int id)
        {
            var builder = _typeBuilderCreator.CreateWithDefaultConstructor(string.Format("<Derived>{0}{1}_{2:N}", type, id, Guid.NewGuid()), type);
            foreach (var prop in type.GetTypeInfo().GetProperties())
            {
                var vhm = new ValueHoldingMember(prop);
                if (_entityTypesHelper.IsNonDbProperty(vhm)
                    || _entityTypesHelper.IsNavigationWrapperProperty(vhm)
                    || _entityTypesHelper.IsDependentProperty(vhm))
                {
                    continue;
                }
                var getter = prop.GetGetMethod(true);
                var setter = prop.GetSetMethod(true);
                if (getter != null && getter.IsVirtual && !getter.IsFinal && !getter.IsPrivate
                    && (setter == null || setter.IsPrivate))
                {
                    _propertyCreator.MakeWriteableShadow(builder, prop.PropertyType, prop.Name,
                        _memberNamingHelper.WriteableShadow(prop.Name));
                }
            }
            return builder.CreateTypeInfo().AsType();
        }

        private bool IsDerivable([NotNull] Type type)
        {
            var typeInfo = type.GetTypeInfo();
            return typeInfo.IsClass && typeInfo.IsPublic && !typeInfo.IsAbstract;
        }

        private Type CreateNewType(KeyValuePair<Type, int> id)
        {
            if (id.Key == null)
            {
                throw new ArgumentException("Null type passed to CreateNewType");
            }
            return IsDerivable(id.Key)
                ? CreateNewDerivedType(id.Key, id.Value)
                : CreateNewDynamicType(id.Key, id.Value);
        }

        public Type GetDynamicType(Type entityType, int index)
        {
            var result = _entityToDynamic.LazyGetOrAdd(
                new KeyValuePair<Type, int>(entityType, index),
                CreateNewType);
            if (result == null)
            {
                throw new InvalidOperationException(string.Format("Dynamic type cache contains null for {0}.{1}", entityType, index));
            }
            return result;
        }

        public IDynamicTypePool GetPool()
        {
            return new DynamicTypePool(this);
        }
    }
}
