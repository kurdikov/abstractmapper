﻿namespace TsSoft.Expressions.SelectBuilder.DynamicTypes
{
    using System;
    using JetBrains.Annotations;

    /// <summary>
    /// Хелпер для динамических типов сущностей
    /// </summary>
    public interface IEntityDynamicTypesHelper
    {
        /// <summary>
        /// Получить динамический тип
        /// </summary>
        /// <param name="entityType">Статический тип сущности</param>
        /// <param name="index">Номер динамического типа</param>
        [NotNull]
        Type GetDynamicType(Type entityType, int index = 0);

        /// <summary>
        /// Получить пул динамических типов сущностей
        /// </summary>
        [NotNull]
        IDynamicTypePool GetPool();
    }
}
