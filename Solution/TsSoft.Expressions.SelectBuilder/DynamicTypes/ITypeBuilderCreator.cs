﻿namespace TsSoft.Expressions.SelectBuilder.DynamicTypes
{
    using System;
    using System.Reflection.Emit;
    using JetBrains.Annotations;

    /// <summary>
    /// Создатель динамических типов
    /// </summary>
    public interface ITypeBuilderCreator
    {
        /// <summary>
        /// Создаёт построитель типа, в который включён пустой конструктор без параметров
        /// </summary>
        /// <param name="typeName">Имя типа</param>
        /// <returns>Построитель типа</returns>
        /// <exception cref="T:System.ArgumentException">Динамический тип с заданным именем уже существует</exception>
        [NotNull]
        TypeBuilder CreateWithDefaultConstructor([NotNull]string typeName);

        /// <summary>
        /// Создаёт построитель типа, в который включён пустой конструктор без параметров
        /// </summary>
        /// <param name="typeName">Имя типа</param>
        /// <param name="baseType">Базовый тип</param>
        /// <returns>Построитель типа</returns>
        /// <exception cref="T:System.ArgumentException">Динамический тип с заданным именем уже существует</exception>
        [NotNull]
        TypeBuilder CreateWithDefaultConstructor([NotNull]string typeName, Type baseType);

        /// <summary>
        /// Создаёт построитель интерфейса
        /// </summary>
        /// <param name="typeName">Имя типа</param>
        /// <returns>Построитель типа</returns>
        /// <exception cref="T:System.ArgumentException">Динамический тип с заданным именем уже существует</exception>
        [NotNull]
        TypeBuilder CreateInterface([NotNull] string typeName);
    }
}
