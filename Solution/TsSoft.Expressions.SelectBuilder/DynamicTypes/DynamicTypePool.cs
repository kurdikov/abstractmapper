﻿namespace TsSoft.Expressions.SelectBuilder.DynamicTypes
{
    using System;
    using System.Collections.Concurrent;
    using JetBrains.Annotations;

    internal class DynamicTypePool : IDynamicTypePool
    {
        [NotNull]private readonly IEntityDynamicTypesHelper _helper;

        [NotNull]private readonly ConcurrentDictionary<Type, int> _currentIndices = new ConcurrentDictionary<Type, int>();

        public DynamicTypePool([NotNull]IEntityDynamicTypesHelper helper)
        {
            if (helper == null) throw new ArgumentNullException("helper");
            _helper = helper;

            Reset();
        }

        public void Reset()
        {
            _currentIndices.Clear();
        }

        public Type GetNext(Type type)
        {
            return _helper.GetDynamicType(type, _currentIndices.AddOrUpdate(type, 0, (t, i) => i + 1));
        }
    }
}
