﻿namespace TsSoft.Expressions.SelectBuilder.DynamicTypes
{
    using System;
    using System.Reflection.Emit;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Копирует свойства в динамический тип
    /// </summary>
    public interface ITypeClonemaker
    {
        /// <summary>
        /// Скопировать свойства из типа в тип как поля, заменяя типы свойств и подменяя типы коллекций
        /// </summary>
        /// <param name="source">Исходный тип</param>
        /// <param name="builder">Построитель нового типа</param>
        /// <param name="replaceWith">Сопоставление типов, которые нужно заменить</param>
        /// <param name="genericCollectionType">Тип коллекции для нового типа</param>
        /// <param name="ignore">Игнорируемые свойства</param>
        void CloneAsFields(
            [NotNull]Type source,
            [NotNull]TypeBuilder builder, 
            [NotNull]Func<Type, Type> replaceWith, 
            [NotNull]Type genericCollectionType,
            [NotNull]Func<ValueHoldingMember, bool> ignore);

        /// <summary>
        /// Скопировать свойства из типа в тип, заменяя типы свойств и подменяя типы коллекций
        /// </summary>
        /// <param name="source">Исходный тип</param>
        /// <param name="builder">Построитель нового типа</param>
        /// <param name="replaceWith">Сопоставление типов, которые нужно заменить</param>
        /// <param name="genericCollectionType">Тип коллекции для нового типа</param>
        /// <param name="ignore">Игнорируемые свойства</param>
        void CloneAsProperties(
            [NotNull]Type source,
            [NotNull]TypeBuilder builder,
            [NotNull]Func<Type, Type> replaceWith,
            [NotNull]Type genericCollectionType,
            [NotNull]Func<ValueHoldingMember, bool> ignore);

        /// <summary>
        /// Скопировать свойства из типа в тип-интерфейс, заменяя типы свойств и подменяя типы коллекций
        /// </summary>
        /// <param name="source">Исходный тип</param>
        /// <param name="builder">Построитель нового интерфейса</param>
        /// <param name="replaceWith">Сопоставление типов, которые нужно заменить</param>
        /// <param name="genericCollectionType">Тип коллекции для нового типа</param>
        /// <param name="ignore">Игнорируемые свойства</param>
        void CloneAsInterfaceProperties([NotNull]Type source,
            [NotNull]TypeBuilder builder,
            [NotNull]Func<Type, Type> replaceWith,
            [NotNull]Type genericCollectionType,
            [NotNull]Func<ValueHoldingMember, bool> ignore);

    }
}
