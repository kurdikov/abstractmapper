﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Копирует деревья выбранных свойств, заменяя в нём типы
    /// </summary>
    public interface ISelectTreeTypeChanger
    {
        /// <summary>
        /// Построить копию дерева выбранных свойств, заменяя типы узлов и свойства
        /// </summary>
        /// <param name="newType">Тип сущности в новом дереве</param>
        /// <param name="tree">Исходное дерево</param>
        [NotNull]
        SelectTree ChangeType([NotNull]Type newType, [NotNull]SelectTree tree);

        /// <summary>
        /// Построить копию узла дерева выбранных свойств, заменяя типы узлов и свойства
        /// </summary>
        /// <param name="newType">Тип сущности в новом дереве</param>
        /// <param name="node">Исходный узел</param>
        [ContractAnnotation("node:notnull => notnull")]
        SelectTreeNode ChangeType([NotNull]Type newType, [CanBeNull]SelectTreeNode node);
    }
}
