namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// ���������� ��������� �������� � ������ ��������� �������
    /// </summary>
    public interface ITypeToSelectTreeConverter
    {
        /// <summary>
        /// �������� �� ���������� �������� ������ ��������� �������
        /// </summary>
        /// <typeparam name="TEntity">��� ��������</typeparam>
        /// <typeparam name="TInterface">��� ����������</typeparam>
        /// <param name="ignoreEntityTypes">������������ ���� ��������� � ���������� ������ ������������ ����������</param>
        [NotNull]
        SelectTree MakeTree<TEntity, TInterface>(bool ignoreEntityTypes) where TEntity : TInterface;

        /// <summary>
        /// �������� ������ ��������� ������� ��� ���������� ����� ��������
        /// </summary>
        /// <typeparam name="TEntity">��� ��������</typeparam>
        [NotNull]
        SelectTree MakePkTree<TEntity>();
    }
}
