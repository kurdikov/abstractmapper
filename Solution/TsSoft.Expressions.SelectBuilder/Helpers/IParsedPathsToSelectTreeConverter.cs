﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Преобразует использованный путь в дерево выбранных свойств
    /// </summary>
    public interface IParsedPathsToSelectTreeConverter
    {
        /// <summary>
        /// Собрать дерево выбранных свойств по использованным путям
        /// </summary>
        /// <param name="entityType">Тип выбираемой сущности</param>
        /// <param name="paths">Перечисление пар (использованный путь, дерево свойств для обработчика конца пути)</param>
        [NotNull]
        SelectTree Convert([NotNull]Type entityType, [NotNull]IEnumerable<KeyValuePair<ParsedPath, SelectTree>> paths);
    }
}
