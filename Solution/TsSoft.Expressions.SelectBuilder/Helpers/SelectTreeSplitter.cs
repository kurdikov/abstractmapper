﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    class SelectTreeSplitter : ISelectTreeSplitter
    {
        [NotNull] private readonly IEntityTypesHelper _entityTypesHelper;

        public SelectTreeSplitter([NotNull] IEntityTypesHelper entityTypesHelper)
        {
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            _entityTypesHelper = entityTypesHelper;
        }

        public IReadOnlyCollection<KeyValuePair<IReadOnlyCollection<ValueHoldingMember>, SelectTreeNode>> CutExternalTrees(SelectTree tree)
        {
            var externalTrees = new List<KeyValuePair<IReadOnlyCollection<ValueHoldingMember>, SelectTreeNode>>();
            if (tree.RootNode.NextLevel != null)
            {
                var currentPath = new List<ValueHoldingMember>();
                Process(tree.RootNode.NextLevel, externalTrees, currentPath);
            }
            return externalTrees;
        }

        private void Process(
            [NotNull]IDictionary<ValueHoldingMember, SelectTreeNode> subtrees,
            [NotNull]ICollection<KeyValuePair<IReadOnlyCollection<ValueHoldingMember>, SelectTreeNode>> externalTrees,
            [NotNull]List<ValueHoldingMember> currentPath)
        {
            foreach (var subtree in subtrees.ToList())  // avoiding InvalidOperationException: collection was modified
            {
                if (subtree.Key == null)
                {
                    continue;
                }
                currentPath.Add(subtree.Key);
                if (_entityTypesHelper.IsExternalProperty(subtree.Key))
                {
                    var externalKey = _entityTypesHelper.TryGetExternalKey(subtree.Key);
                    if (externalKey == null)
                    {
                        throw new InvalidOperationException(string.Format("Unable to find external key for property {0} of {1}",
                            subtree.Key, subtree.Key.DeclaringType));
                    }
                    subtrees.Remove(subtree.Key);
                    subtrees[externalKey] = null;
                    externalTrees.Add(new KeyValuePair<IReadOnlyCollection<ValueHoldingMember>, SelectTreeNode>(
                        currentPath.ToList(),
                        subtree.Value));
                }
                else if (subtree.Value != null && subtree.Value.NextLevel != null)
                {
                    Process(subtree.Value.NextLevel, externalTrees, currentPath);
                }
                currentPath.RemoveAt(currentPath.Count - 1);
            }
        }
    }
}
