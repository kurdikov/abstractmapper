﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Осуществляет слияние деревьев выбранных свойств
    /// </summary>
    public interface ISelectTreeMerger
    {
        /// <summary>
        /// Добавить условие и подузлы узла-источника в узел-цель
        /// </summary>
        /// <param name="target">Цель</param>
        /// <param name="source">Источник</param>
        /// <param name="ignoreSourceCondition">Игнорировать условие в узле-источнике</param>
        void Merge([NotNull]SelectTreeNode target, [NotNull]SelectTreeNode source, bool ignoreSourceCondition = false);

        /// <summary>
        /// Добавить условие и подузлы узла-источника в узел-цель
        /// </summary>
        /// <param name="target">Цель</param>
        /// <param name="source">Источник</param>
        void Merge([NotNull] SelectTree target, [NotNull] SelectTree source);
    }
}
