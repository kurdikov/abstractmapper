﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Преобразует селект-выражение в набор использованных в нём путей
    /// </summary>
    public interface ISelectToPathsConverter
    {
        /// <summary>
        /// Преобразовать селект-выражение в набор использованных в нём путей
        /// </summary>
        /// <typeparam name="T">Тип выбираемой сущности</typeparam>
        /// <param name="select">Селект-выражение</param>
        [NotNull]
        IEnumerable<ParsedPath> Convert<T>([NotNull]Expression<Func<T, object>> select);
    }
}
