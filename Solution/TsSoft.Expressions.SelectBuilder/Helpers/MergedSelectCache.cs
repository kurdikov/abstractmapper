﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.SelectBuilder;
    using TsSoft.Expressions.SelectBuilder.Models;

    class MergedSelectCache<T> : IMergedSelectCache<T>
    {
        [NotNull]
        private readonly ConcurrentDictionary<KeyValuePair<IReadOnlySelectExpression<T>, IReadOnlySelectExpression<T>>, IReadOnlySelectExpression<T>>
            _cache = new ConcurrentDictionary<KeyValuePair<IReadOnlySelectExpression<T>, IReadOnlySelectExpression<T>>, IReadOnlySelectExpression<T>>();

        [NotNull]
        private readonly ISelectHelper _selectHelper;

        public MergedSelectCache([NotNull]ISelectHelper selectHelper)
        {
            if (selectHelper == null) throw new ArgumentNullException("selectHelper");
            _selectHelper = selectHelper;
        }

        public IReadOnlySelectExpression<T> GetMerged(
            IReadOnlySelectExpression<T> firstSelect, 
            IReadOnlySelectExpression<T> secondSelect)
        {
            return _cache.GetOrAdd(new KeyValuePair<IReadOnlySelectExpression<T>, IReadOnlySelectExpression<T>>(firstSelect, secondSelect), _selectHelper.Merge(firstSelect, secondSelect));
        }
    }
}
