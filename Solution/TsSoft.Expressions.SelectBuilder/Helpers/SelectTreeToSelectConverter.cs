﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Models;

    class SelectTreeToSelectConverter : ISelectTreeToSelectConverter
    {
        [NotNull] private readonly IDbSelectTreeToSelectConverter _dbSelectTreeToSelectConverter;
        [NotNull] private readonly IExpressionBuilder _expressionBuilder;
        [NotNull] private readonly ISelectTreeSplitter _selectTreeSplitter;
        [NotNull] private readonly IWrapperPropertyProcessor _wrapperPropertyProcessor;

        public SelectTreeToSelectConverter(
            [NotNull] IDbSelectTreeToSelectConverter dbSelectTreeToSelectConverter,
            [NotNull] IExpressionBuilder expressionBuilder,
            [NotNull] ISelectTreeSplitter selectTreeSplitter,
            [NotNull] IWrapperPropertyProcessor wrapperPropertyProcessor)
        {
            if (dbSelectTreeToSelectConverter == null) throw new ArgumentNullException("dbSelectTreeToSelectConverter");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (selectTreeSplitter == null) throw new ArgumentNullException("selectTreeSplitter");
            if (wrapperPropertyProcessor == null) throw new ArgumentNullException("wrapperPropertyProcessor");
            _dbSelectTreeToSelectConverter = dbSelectTreeToSelectConverter;
            _expressionBuilder = expressionBuilder;
            _selectTreeSplitter = selectTreeSplitter;
            _wrapperPropertyProcessor = wrapperPropertyProcessor;
        }

        public SelectExpression<T> Convert<T>(
            SplitSelectTree splitTree,
            SelectSettings settings)
        {
            var selectExpr = _dbSelectTreeToSelectConverter.Convert<T>(splitTree.InternalTree, settings);
            var select = new SelectExpression<T> { Select = selectExpr };
            if (splitTree.ExternalTrees != null)
            {
                foreach (var externalTree in splitTree.ExternalTrees)
                {
                    select.ExternalIncludes.Add(_expressionBuilder.BuildIncludeByPath<T>(externalTree.Key));
                }
            }
            if (splitTree.InternalTree.ExternalIncludes != null)
            {
                foreach (var treeInclude in splitTree.InternalTree.ExternalIncludes)
                {
                    select.ExternalIncludes.Add((Expression<Func<T, object>>)treeInclude);
                }
            }
            select.ExternalIncludes = select.ExternalIncludes.Distinct(IncludeEqualityComparer<T>.Instance).ToList();
            return select;
        }

        public SelectExpression<T> Convert<T>(
            SelectTree tree,
            SelectSettings settings)
        {
            var externalTrees = _selectTreeSplitter.CutExternalTrees(tree);
            var select = Convert<T>(new SplitSelectTree(tree, externalTrees), settings);
            return select;
        }

        public SelectExpression<T> ConvertDbTree<T>(
            SelectTree tree,
            SelectSettings settings)
        {
            var select = Convert<T>(new SplitSelectTree(tree, null), settings);
            return select;
        }

        public SelectExpression<T> TransformAndConvert<T>(SelectTree tree, SelectSettings settings)
        {
            _wrapperPropertyProcessor.ProcessWrapperProperties(tree);
            var select = Convert<T>(tree, settings);
            return select;
        }
    }
}
