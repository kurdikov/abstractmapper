﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.SelectBuilder.Models;

    class SelectSettingsHelper : ISelectSettingsHelper
    {
        [NotNull] private readonly IEntityTypesHelper _entityTypesHelper;

        public SelectSettingsHelper([NotNull] IEntityTypesHelper entityTypesHelper)
        {
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            _entityTypesHelper = entityTypesHelper;
        }

        public bool ShouldCheckNull(SelectSettings settings, ValueHoldingMember property)
        {
            return !settings.SkipNullChecks
                   || (settings.AlwaysCheckNull != null && settings.AlwaysCheckNull.Contains(property))
                   || !_entityTypesHelper.IsCorrespondingForeignKeyNotNullable(property);
        }
    }
}
