﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    class WrapperPropertyProcessor : IWrapperPropertyProcessor
    {
        [NotNull] private readonly IEntityTypesHelper _entityTypesHelper;
        [NotNull] private readonly ISelectTreeMerger _merger;
        [NotNull] private readonly ISelectTreeTypeChanger _typeChanger;
        [NotNull] private readonly ICorrespondingMemberHelper _correspondingMemberHelper;

        public WrapperPropertyProcessor(
            [NotNull] IEntityTypesHelper entityTypesHelper,
            [NotNull] ISelectTreeMerger merger,
            [NotNull] ISelectTreeTypeChanger typeChanger,
            [NotNull] ICorrespondingMemberHelper correspondingMemberHelper)
        {
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            if (merger == null) throw new ArgumentNullException("merger");
            if (typeChanger == null) throw new ArgumentNullException("typeChanger");
            if (correspondingMemberHelper == null) throw new ArgumentNullException("correspondingMemberHelper");
            _entityTypesHelper = entityTypesHelper;
            _merger = merger;
            _typeChanger = typeChanger;
            _correspondingMemberHelper = correspondingMemberHelper;
        }

        public void ProcessWrapperProperties(SelectTree tree)
        {
            if (tree.RootNode.NextLevel != null)
            {
                ProcessLevel(tree.RootNode.NextLevel);
            }
        }

        /// <summary>
        /// Превращает дерево выбранных из обёртки свойств в дерево свойств, выбранных из mapped-навигации
        /// </summary>
        /// <param name="entityType">Тип mapped-навигации</param>
        /// <param name="node">Узел свойства-обёртки</param>
        private SelectTreeNode ChangeNodeType([NotNull]Type entityType, SelectTreeNode node)
        {
            var result = _typeChanger.ChangeType(entityType, node);
            if (result != null && result.NextLevel != null)
            {
                ProcessLevel(result.NextLevel);
            }
            return result;
        }

        /// <summary>
        /// Обработать поддерево
        /// </summary>
        /// <param name="level">Поддерево выбранных свойств</param>
        private void ProcessLevel(
            [NotNull]IDictionary<ValueHoldingMember, SelectTreeNode> level)
        {
            foreach (var subtree in level.ToList()) // avoiding InvalidOperationException: collection was modified
            {
                if (subtree.Key == null)
                {
                    continue;
                }
                var property = subtree.Key;
                if (_entityTypesHelper.IsDependentProperty(property))
                {
                    foreach (var underlyingProp in _entityTypesHelper.GetUnderlyingColumns(property))
                    {
                        if (underlyingProp == null)
                        {
                            continue;
                        }
                        level[underlyingProp] = null;
                    }
                    level.Remove(property);
                }
                else if (_entityTypesHelper.IsNavigationWrapperProperty(property))
                {
                    var wrapSubtree = subtree.Value;
                    foreach (var wrappedProp in _entityTypesHelper.GetWrappedNavigation(property))
                    {
                        if (wrappedProp == null)
                        {
                            continue;
                        }
                        MergeOrCreate(level, wrappedProp, wrapSubtree);
                    }
                    level.Remove(property);
                }
                else if (property.IsNonPublicPropertyWithGetter)   // explicit interface implementation
                {
                    var actualMember = _correspondingMemberHelper.TryGetCorrespondingMemberForExplicitImplementation(property);
                    if (actualMember != null && !actualMember.Equals(property))
                    {
                        if (subtree.Value == null)
                        {
                            level[actualMember] = null;
                        }
                        else
                        {
                            MergeOrCreate(level, actualMember, subtree.Value);
                        }
                        level.Remove(property);
                    }
                }
                else if (subtree.Value != null && subtree.Value.NextLevel != null)
                {
                    ProcessLevel(subtree.Value.NextLevel);
                }
            }
        }

        private void MergeOrCreate(
            [NotNull]IDictionary<ValueHoldingMember, SelectTreeNode> level,
            [NotNull]ValueHoldingMember actualProperty, 
            SelectTreeNode value)
        {
            SelectTreeNode existingNode;
            var wrappedType = actualProperty.ValueType.GetGenericEnumerableArgumentOrSelf();
            level.TryGetValue(actualProperty, out existingNode);
            var unwrappedSubtree = ChangeNodeType(wrappedType, value);
            if (existingNode != null && unwrappedSubtree != null)
            {
                _merger.Merge(existingNode, unwrappedSubtree);
            }
            else
            {
                level[actualProperty] = unwrappedSubtree;
            }
        }
    }
}
