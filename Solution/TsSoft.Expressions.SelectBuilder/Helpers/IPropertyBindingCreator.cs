﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;

    /// <summary>
    /// Строит присваивания для свойства класса-сущности
    /// </summary>
    public interface IPropertyBindingCreator
    {
        /// <summary>
        /// Построить new-выражение
        /// </summary>
        /// <param name="dynamicType">Динамический тип объекта, создаваемого внутри select</param>
        /// <param name="bindings">Присваивания</param>
        [NotNull]
        MemberInitExpression NewDynamic([NotNull]Type dynamicType, [NotNull]IEnumerable<MemberBinding> bindings);
            
        /// <summary>
        /// Построить присваивание для свойства, соответствующего столбцу БД
        /// </summary>
        /// <param name="entityProperty">Свойство, соответствующее столбцу БД</param>
        /// <param name="dynamicType">Динамический тип для сущности</param>
        /// <param name="entity">Выражение с сущностью</param>
        [NotNull]
        MemberAssignment BindColumn([NotNull]ValueHoldingMember entityProperty, [NotNull]Type dynamicType, [NotNull]Expression entity);

        /// <summary>
        /// Построить присваивание для навигационного свойства с одной сущностью
        /// </summary>
        /// <param name="entityProperty">Навигационное свойство</param>
        /// <param name="dynamicType">Динамический тип сущности</param>
        /// <param name="subentityDynamicType">Динамический тип свойства</param>
        /// <param name="entity">Выражение с сущностью</param>
        /// <param name="bindings">Присваивания для объекта в свойстве</param>
        /// <param name="checkNull">Вставить проверку на null для навигационного свойства</param>
        [NotNull]
        MemberAssignment BindNavigationSingle(
            [NotNull]ValueHoldingMember entityProperty,
            [NotNull]Type dynamicType,
            [NotNull]Type subentityDynamicType,
            [NotNull]Expression entity,
            [NotNull]IEnumerable<MemberAssignment> bindings,
            bool checkNull);

        /// <summary>
        /// Построить присваивание для навигационного свойства с коллекцией сущностей
        /// </summary>
        /// <param name="entityProperty">Навигационное свойство</param>
        /// <param name="dynamicType">Динамический тип сущности</param>
        /// <param name="subentityType">Тип элемента коллекции</param>
        /// <param name="subentityDynamicType">Динамический тип элемента коллекции</param>
        /// <param name="entity">Выражение с сущностью</param>
        /// <param name="subentityParameter">Параметр внутреннего select</param>
        /// <param name="bindings">Присваивания для объекта в свойстве</param>
        /// <param name="whereExpression">Условие на элемент коллекции</param>
        [NotNull]
        MemberAssignment BindNavigationCollection(
            [NotNull]ValueHoldingMember entityProperty,
            [NotNull]Type dynamicType,
            [NotNull]Type subentityType,
            [NotNull]Type subentityDynamicType,
            [NotNull]Expression entity,
            [NotNull]ParameterExpression subentityParameter,
            [NotNull]IEnumerable<MemberAssignment> bindings,
            LambdaExpression whereExpression);
    }
}
