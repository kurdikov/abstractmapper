﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    class IncludeToSelectTreeConverter : IIncludeToSelectTreeConverter
    {
        [NotNull] private readonly IFlatPathParser _flatPathParser;
        [NotNull] private readonly IEntityTypesHelper _entityTypesHelper;

        public IncludeToSelectTreeConverter([NotNull] IFlatPathParser flatPathParser, [NotNull] IEntityTypesHelper entityTypesHelper)
        {
            if (flatPathParser == null) throw new ArgumentNullException("flatPathParser");
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            _flatPathParser = flatPathParser;
            _entityTypesHelper = entityTypesHelper;
        }

        public SelectTree Convert<T>(IReadOnlyIncludes<T> includes)
        {
            return Convert(includes as IReadOnlyCollection<Expression<Func<T, object>>>);
        }

        private void AddColumns([NotNull]IDictionary<ValueHoldingMember, SelectTreeNode> treeLevel, [NotNull]Type entityType)
        {
            foreach (var property in ValueHoldingMember.GetValueHoldingMembers(entityType))
            {
                if (property != null && _entityTypesHelper.IsColumnProperty(property))
                {
                    treeLevel[property] = null;
                }
            }
        }

        public SelectTree Convert<T>(IEnumerable<Expression<Func<T, object>>> includes)
        {
            var firstLevel = new SelectTreeLevel();
            AddColumns(firstLevel, typeof(T));
            if (includes != null)
            {
                foreach (var include in includes)
                {
                    var path = _flatPathParser.Parse(include);
                    var currentLevel = firstLevel;
                    foreach (var property in path)
                    {
                        if (property == null)
                        {
                            throw new InvalidPathException("Null property in path");
                        }
                        SelectTreeNode propertyNode;
                        if (!currentLevel.TryGetValue(property, out propertyNode) || propertyNode == null)
                        {
                            propertyNode = currentLevel[property] = new SelectTreeNode(property.ValueType.GetGenericEnumerableArgumentOrSelf());
                        }
                        currentLevel = (propertyNode.NextLevel = propertyNode.NextLevel ?? new SelectTreeLevel());
                        AddColumns(currentLevel, property.ValueType.GetGenericEnumerableArgumentOrSelf());
                    }
                }
            }
            return new SelectTree(Expression.Parameter(typeof(T), "e"), firstLevel);
        }
    }
}
