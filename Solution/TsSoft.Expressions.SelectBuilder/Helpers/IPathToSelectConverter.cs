﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Собирает селект-выражение по набору путей
    /// </summary>
    public interface IPathToSelectConverter
    {
        /// <summary>
        /// Собрать селект-выражение по набору путей (предполагается, что путь включает в себя только mapped-свойства)
        /// </summary>
        /// <typeparam name="T">Тип выбираемой сущности</typeparam>
        /// <param name="selectedPaths">Пути</param>
        /// <param name="settings">Настройки построения селекта</param>
        SelectExpression<T> Convert<T>([NotNull]IEnumerable<ParsedPath> selectedPaths, [CanBeNull]SelectSettings settings);
    }
}
