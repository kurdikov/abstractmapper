﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Собирает селект-выражение с внешними инклюдами по дереву выбранных свойств
    /// </summary>
    /// <remarks>В основном занимается внешними инклюдами, сборка непосредственно селекта делегируется в <see cref="IDbSelectTreeToSelectConverter" /></remarks>
    public interface ISelectTreeToSelectConverter
    {
        /// <summary>
        /// Получить селект-выражение по дереву выбранных свойств
        /// </summary>
        /// <typeparam name="T">Тип сущности БД</typeparam>
        /// <param name="splitTree">Разбитое на внутреннюю и внешние части дерево выбранных свойств</param>
        /// <param name="settings">Настройки построения селекта</param>
        [NotNull]
        SelectExpression<T> Convert<T>(
            [NotNull]SplitSelectTree splitTree,
            [NotNull]SelectSettings settings);

        /// <summary>
        /// Получить селект-выражение по дереву выбранных свойств, среди которых могут быть внешние
        /// </summary>
        /// <typeparam name="T">Тип сущности БД</typeparam>
        /// <param name="tree">Дерево выбранных свойств</param>
        /// <param name="settings">Настройки построения селекта</param>
        [NotNull]
        SelectExpression<T> Convert<T>(
            [NotNull]SelectTree tree,
            [NotNull]SelectSettings settings);

        /// <summary>
        /// Получить селект-выражение по дереву выбранных (только) из БД свойств
        /// </summary>
        /// <typeparam name="T">Тип сущности БД</typeparam>
        /// <param name="tree">Дерево выбранных свойств</param>
        /// <param name="settings">Настройки построения селекта</param>
        [NotNull]
        SelectExpression<T> ConvertDbTree<T>(
            [NotNull]SelectTree tree,
            [NotNull]SelectSettings settings);

        /// <summary>
        /// Собрать селект-выражение по дереву выбранных свойств, среди которых могут быть обёртки и внешние свойства
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="tree">Дерево выбранных свойств</param>
        /// <param name="settings">Настройки построения селекта</param>
        [NotNull]
        SelectExpression<T> TransformAndConvert<T>([NotNull]SelectTree tree, [NotNull]SelectSettings settings);
    }
}
