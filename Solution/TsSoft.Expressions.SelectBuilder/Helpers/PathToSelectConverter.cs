﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Models;

    class PathToSelectConverter : IPathToSelectConverter
    {
        [NotNull] private readonly IParsedPathsToSelectTreeConverter _treeBuilder;
        [NotNull] private readonly ISelectTreeToSelectConverter _selectBuilder;

        public PathToSelectConverter([NotNull] IParsedPathsToSelectTreeConverter treeBuilder, [NotNull] ISelectTreeToSelectConverter selectBuilder)
        {
            if (treeBuilder == null) throw new ArgumentNullException("treeBuilder");
            if (selectBuilder == null) throw new ArgumentNullException("selectBuilder");
            _treeBuilder = treeBuilder;
            _selectBuilder = selectBuilder;
        }

        public SelectExpression<T> Convert<T>(IEnumerable<ParsedPath> selectedPaths, SelectSettings settings)
        {
            var tree = _treeBuilder.Convert(
                typeof(T),
                selectedPaths.Select(p => new KeyValuePair<ParsedPath, SelectTree>(p, null)));
            var select = _selectBuilder.TransformAndConvert<T>(tree, settings ?? new SelectSettings());
            return select;
        }
    }
}
