﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Models;

    class SelectToSelectTreeConverter : ISelectToSelectTreeConverter
    {
        [NotNull]private readonly INewExpressionHelper _newExpressionHelper;

        public SelectToSelectTreeConverter([NotNull] INewExpressionHelper newExpressionHelper)
        {
            if (newExpressionHelper == null) throw new ArgumentNullException("newExpressionHelper");
            _newExpressionHelper = newExpressionHelper;
        }

        public SelectTree Convert<T>(IReadOnlySelectExpression<T> selectExpression)
        {
            if (selectExpression.Select == null)
            {
                return new SelectTree(Expression.Parameter(typeof(T)));
            }
            var result = Convert(selectExpression.Select);
            result.ExternalIncludes = selectExpression.ExternalIncludes;
            return result;
        }

        public SelectTree Convert<T>(Expression<Func<T, object>> selectExpression)
        {
            var selectParameter = selectExpression.ParameterAt(0);
            var tree = new SelectTree(selectParameter);
            var topLevelAssignments = _newExpressionHelper.GetAssignments(selectExpression);
            tree.RootNode.NextLevel = ConvertAssignments(typeof(T), topLevelAssignments);
            return tree;
        }

        public SelectTreeLevel ConvertAssignments(
            [NotNull]Type entityType,
            [NotNull]IEnumerable<KeyValuePair<MemberInfo, Expression>> assignments)
        {
            var result = new SelectTreeLevel();
            foreach (var assignment in assignments)
            {
                var parsed = _newExpressionHelper.ParseAssignment(assignment, entityType);
                if (parsed.UsedMember == null)
                {
                    throw new InvalidOperationException(string.Format("Unable to find property used in assignment {0} = {1}", assignment.Key, assignment.Value));
                }
                result[parsed.UsedMember] = GetNode(parsed.UsedMember.ValueType, parsed);
            }
            return result;
        }

        private SelectTreeNode GetNode([NotNull]Type propertyType, [NotNull]SelectMemberAssignment assignment)
        {
            SelectTreeNode result = null;
            if (assignment.InnerSelect != null)
            {
                var innerAssignments = _newExpressionHelper.GetAssignments(assignment.InnerSelect);
                result = new SelectTreeNode(propertyType.GetGenericEnumerableArgumentOrSelf())
                {
                    NextLevel = ConvertAssignments(propertyType.GetGenericEnumerableArgument(), innerAssignments)
                };
                result.AddSelectParameter(assignment.InnerSelect.ParameterAt(0));
                result.AddCondition(assignment.Condition);
            }
            else if (assignment.InnerAssignment != null)
            {
                result = new SelectTreeNode(propertyType.GetGenericEnumerableArgumentOrSelf())
                {
                    NextLevel = ConvertAssignments(propertyType, assignment.InnerAssignment)
                };
            }
            return result;
        }
    }
}
