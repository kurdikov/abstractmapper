﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Обрабатывает настройки построения select-выражения
    /// </summary>
    public interface ISelectSettingsHelper
    {
        /// <summary>
        /// Нужно ли вставлять проверку одиночной навигации на null
        /// </summary>
        /// <param name="settings">Настройки построения</param>
        /// <param name="property">Свойство-одиночная навигация</param>
        bool ShouldCheckNull([NotNull]SelectSettings settings, [NotNull]ValueHoldingMember property);
    }
}
