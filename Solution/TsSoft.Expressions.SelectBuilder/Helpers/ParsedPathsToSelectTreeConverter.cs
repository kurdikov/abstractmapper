namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Models;

    class ParsedPathsToSelectTreeConverter : IParsedPathsToSelectTreeConverter
    {
        [NotNull] private readonly IEntityTypesHelper _entityTypesHelper;
        [NotNull] private readonly ISelectTreeMerger _merger;
        [NotNull] private readonly IPathHelper _pathHelper;
        [NotNull] private readonly ICorrespondingMemberHelper _correspondingMemberHelper;
        [NotNull] private readonly ISelectTreeTypeChanger _typeChanger;
        [NotNull] private readonly IExpressionBuilder _expressionBuilder;
        [NotNull] private readonly IFlatPathParser _flatPathParser;

        public ParsedPathsToSelectTreeConverter(
            [NotNull] IEntityTypesHelper entityTypesHelper,
            [NotNull] ISelectTreeMerger merger,
            [NotNull] IPathHelper pathHelper,
            [NotNull] ICorrespondingMemberHelper correspondingMemberHelper,
            [NotNull] ISelectTreeTypeChanger typeChanger,
            [NotNull] IExpressionBuilder expressionBuilder,
            [NotNull] IFlatPathParser flatPathParser)
        {
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            if (merger == null) throw new ArgumentNullException("merger");
            if (pathHelper == null) throw new ArgumentNullException("pathHelper");
            if (correspondingMemberHelper == null) throw new ArgumentNullException("correspondingMemberHelper");
            if (typeChanger == null) throw new ArgumentNullException("typeChanger");
            if (expressionBuilder == null) throw new ArgumentNullException("expressionBuilder");
            if (flatPathParser == null) throw new ArgumentNullException("flatPathParser");
            _entityTypesHelper = entityTypesHelper;
            _merger = merger;
            _pathHelper = pathHelper;
            _correspondingMemberHelper = correspondingMemberHelper;
            _typeChanger = typeChanger;
            _expressionBuilder = expressionBuilder;
            _flatPathParser = flatPathParser;
        }

        public SelectTree Convert(Type entityType, IEnumerable<KeyValuePair<ParsedPath, SelectTree>> paths)
        {
            var rootNode = new SelectTreeNode(entityType) {NextLevel = new SelectTreeLevel()};
            var externalIncludes = new List<LambdaExpression>();
            foreach (var pathPair in paths)
            {
                var path = pathPair.Key;
                if (path == null)
                {
                    continue;
                }
                rootNode.AddSelectParameter(path.Start);
                AddPath(rootNode, path, pathPair.Value, PathDeadendBehavior.TakeAllPrimitiveProperties, false);
                if (pathPair.Value != null && pathPair.Value.ExternalIncludes != null)
                {
                    externalIncludes.AddRange(pathPair.Value.ExternalIncludes.Select(i =>
                        _expressionBuilder.BuildIncludeByPath(entityType, path.Elements.Select(e => e != null ? e.Step : null).Concat(_flatPathParser.Parse(i)))));
                }
            }
            return new SelectTree(rootNode.SelectParameters.FirstOrDefault() ?? Expression.Parameter(entityType), rootNode)
            {
                ExternalIncludes = externalIncludes
            };
        }

        /// <summary>
        /// ���������� �� ���� �� ������������� ��������
        /// </summary>
        /// <param name="path">����</param>
        /// <param name="processorTree">����������� ����</param>
        private bool IsDeadendPath([NotNull]ParsedPath path, SelectTree processorTree)
        {
            PathElement lastElement;
            return
                processorTree == null
                &&
                (
                    path.Elements.Count == 0
                    || (lastElement = path.Elements[path.Elements.Count - 1]) != null 
                        && !_entityTypesHelper.IsColumnProperty(lastElement.Step)
                        && !_entityTypesHelper.IsDependentProperty(lastElement.Step)
                );
        }

        private void AddPath(
            [NotNull]SelectTreeNode node,
            [NotNull]ParsedPath path,
            SelectTree processorTree,
            PathDeadendBehavior deadendBehavior,
            bool isAdditionalPath)
        {
            if (processorTree == SelectTree.ExhaustedNesting)
            {
                return;
            }
            if (deadendBehavior == PathDeadendBehavior.IgnorePath && IsDeadendPath(path, processorTree))
            {
                return;
            }
            ValueHoldingMember lastStep;
            var lastElementNode = ProcessElements(node, path, out lastStep);
            if (processorTree != null)
            {
                MergeProcessorTree(processorTree, lastElementNode, lastStep);
            }
            else if (lastElementNode != null && lastElementNode.NextLevel != null)
            {
                ProcessDeadend(lastElementNode.NextLevel, path, deadendBehavior);
            }
            if (!isAdditionalPath && path.AdditionalPaths != null)
            {
                if (node.NextLevel == null)
                {
                    throw new InvalidPathException("Additional paths on primitive property");
                }
                AddPaths(node, path.AdditionalPaths, PathDeadendBehavior.TakeOnlyId, true);
            }
        }

        private SelectTreeNode ProcessElements([NotNull]SelectTreeNode rootNode, [NotNull]ParsedPath path, out ValueHoldingMember lastStep)
        {
            var currentNode = rootNode;
            lastStep = null;
            foreach (var element in path.Elements)
            {
                if (element == null)
                {
                    throw new InvalidPathException("Path contains null element");
                }
                if (currentNode == null || currentNode.NextLevel == null)
                {
                    throw new InvalidPathException(
                        string.Format("Path contains subproperties of column property {0}. Check that {1} is an entity type for your IEntityTypesRetriever implementation",
                            lastStep,
                            lastStep != null ? lastStep.ValueType : null));
                }
                SelectTreeNode nextNode;
                var step = _correspondingMemberHelper.GetImplementationIfInterfaceProperty(currentNode.Type, element.Step);
                if (!currentNode.NextLevel.TryGetValue(step, out nextNode))
                {
                    nextNode = currentNode.NextLevel[step] = CreateNode(element);
                }
                else
                {
                    MergeIntoNode(nextNode, element);
                }
                if (nextNode != null && element.Parameter != null && path.Branches != null)
                {
                    AddPaths(nextNode, path.Branches[element.Parameter], PathDeadendBehavior.TakeOnlyId, true);
                }
                currentNode = nextNode;
                lastStep = step;
            }
            return currentNode;
        }

        private void MergeProcessorTree([NotNull]SelectTree processorTree, SelectTreeNode processedNode, ValueHoldingMember step)
        {
            if (processedNode == null)
            {
                throw new InvalidPathException(
                    string.Format("Processor on primitive property {0}. Check that {1} is an entity type for your IEntityTypesRetriever implementation",
                        step,
                        step != null ? step.ValueType : null));
            }
            var processorTreeRootNode = processorTree.RootNode.Type == processedNode.Type
                ? processorTree.RootNode
                : _typeChanger.ChangeType(processedNode.Type, processorTree.RootNode);
            _merger.Merge(processedNode, processorTreeRootNode, true);
        }

        private void ProcessDeadend([NotNull]SelectTreeLevel deadendNodeNextLevel, [NotNull]ParsedPath path, PathDeadendBehavior deadendBehavior)
        {
            var pathEndType = _pathHelper.GetPathSingleType(path);
            IEnumerable<ValueHoldingMember> columns;
            switch (deadendBehavior)
            {
                case PathDeadendBehavior.TakeOnlyId:
                    columns = _entityTypesHelper.GetPrimaryKey(pathEndType);
                    break;
                case PathDeadendBehavior.TakeAllPrimitiveProperties:
                    columns = ValueHoldingMember.GetValueHoldingMembers(pathEndType).Where(_entityTypesHelper.IsColumnProperty);
                    break;
                case PathDeadendBehavior.ThrowException:
                    throw new InvalidPathException(string.Format("No processor on deadend path {0} ending on entity {1}", path, pathEndType));
                default:
                    columns = Enumerable.Empty<ValueHoldingMember>();
                    break;
            }
            foreach (var column in columns)
            {
                if (column == null)
                {
                    throw new NullReferenceException("column");
                }
                deadendNodeNextLevel[column] = null;
            }
        }

        private void AddPaths(
            [NotNull]SelectTreeNode node,
            [NotNull]IEnumerable<ParsedPath> paths,
            PathDeadendBehavior deadendBehavior,
            bool isAdditionalPath)
        {
            foreach (var additionalPath in paths)
            {
                if (additionalPath == null)
                {
                    continue;
                }
                AddPath(node, additionalPath, null, deadendBehavior, isAdditionalPath);
            }
        }

        private SelectTreeNode CreateNode([NotNull]PathElement element)
        {
            if (_entityTypesHelper.IsColumnProperty(element.Step) || _entityTypesHelper.IsDependentProperty(element.Step))
            {
                return null;
            }
            var result = new SelectTreeNode(element.Step.ValueType.GetGenericEnumerableArgumentOrSelf()) {NextLevel = new SelectTreeLevel()};
            MergeIntoNode(result, element);
            return result;
        }

        private void MergeIntoNode(SelectTreeNode currentNode, [NotNull]PathElement element)
        {
            if (currentNode == null)
            {
                return;
            }
            if (element.Parameter != null)
            {
                currentNode.AddSelectParameter(element.Parameter);
            }
            if (element.Conditions != null)
            {
                currentNode.AddConditions(element.Conditions.Where(c => c != null).Select(c => c.DbCondition));
                foreach (var condition in element.Conditions)
                {
                    if (condition == null)
                    {
                        continue;
                    }
                    AddPaths(currentNode, condition.UsedPaths, PathDeadendBehavior.TakeOnlyId, false);
                }
            }
            else
            {
                currentNode.AddNullCondition();
            }
        }
    }
}
