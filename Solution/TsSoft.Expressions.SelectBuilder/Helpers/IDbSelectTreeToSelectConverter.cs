﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Преобразователь дерева свойств в select-выражение
    /// </summary>
    public interface IDbSelectTreeToSelectConverter
    {
        /// <summary>
        /// Создать select-выражение по дереву свойств
        /// </summary>
        /// <typeparam name="T">Тип выбираемой сущности</typeparam>
        /// <param name="tree">Дерево свойств</param>
        /// <param name="settings">Настройки построения выражения</param>
        /// <returns>Построенное select-выражение</returns>
        [NotNull]
        Expression<Func<T, object>> Convert<T>([NotNull]SelectTree tree, [NotNull]SelectSettings settings);
    }
}
