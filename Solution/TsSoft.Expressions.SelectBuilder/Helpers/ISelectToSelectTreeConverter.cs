﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Преобразователь select-выражения в дерево свойств
    /// </summary>
    public interface ISelectToSelectTreeConverter
    {
        /// <summary>
        /// Построить дерево выбранных свойств по select-выражению
        /// </summary>
        /// <typeparam name="T">Тип выбираемой сущности</typeparam>
        /// <param name="selectExpression">Select-выражение</param>
        /// <returns>Дерево выбранных свойств</returns>
        [NotNull]
        SelectTree Convert<T>([NotNull]IReadOnlySelectExpression<T> selectExpression);

        /// <summary>
        /// Построить дерево выбранных свойств по select-выражению
        /// </summary>
        /// <typeparam name="T">Тип выбираемой сущности</typeparam>
        /// <param name="selectExpression">Select-выражение</param>
        /// <returns>Дерево выбранных свойств</returns>
        [NotNull]
        SelectTree Convert<T>([NotNull]Expression<Func<T, object>> selectExpression);
    }
}
