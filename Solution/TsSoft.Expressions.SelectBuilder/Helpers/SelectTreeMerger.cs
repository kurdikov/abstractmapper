namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// ������������ ������� �������� ��������� �������
    /// </summary>
    class SelectTreeMerger : ISelectTreeMerger
    {
        public void Merge(SelectTree target, SelectTree source)
        {
            Merge(target.RootNode, source.RootNode, false);
            if (source.ExternalIncludes != null)
            {
                IEnumerable<LambdaExpression> includes = source.ExternalIncludes;
                if (target.ExternalIncludes != null)
                {
                    includes = includes.Concat(target.ExternalIncludes);
                }
                target.ExternalIncludes = new List<LambdaExpression>(includes);
            }
        }

        public void Merge(SelectTreeNode target, SelectTreeNode source, bool ignoreSourceCondition)
        {
            target.AddSelectParameters(source.SelectParameters);
            if (!ignoreSourceCondition)
            {
                target.AddCondition(source.Condition, source.ConditionParameters);
            }
            MergeNextLevel(target, source);
        }

        private void MergeNextLevel([NotNull]SelectTreeNode target, [NotNull]SelectTreeNode source)
        {
            if (source.NextLevel != null)
            {
                target.NextLevel = target.NextLevel ?? new SelectTreeLevel();
                foreach (var sourceSubnode in source.NextLevel)
                {
                    if (sourceSubnode.Key == null)
                    {
                        continue;
                    }
                    SelectTreeNode targetSubnode;
                    if (!target.NextLevel.TryGetValue(sourceSubnode.Key, out targetSubnode))
                    {
                        target.NextLevel[sourceSubnode.Key] = sourceSubnode.Value;
                    }
                    else if (sourceSubnode.Value != null)
                    {
                        if (targetSubnode == null)
                        {
                            targetSubnode = target.NextLevel[sourceSubnode.Key] = new SelectTreeNode(sourceSubnode.Value.Type);
                        }
                        Merge(targetSubnode, sourceSubnode.Value, false);
                    }
                }
            }
        }
    }
}
