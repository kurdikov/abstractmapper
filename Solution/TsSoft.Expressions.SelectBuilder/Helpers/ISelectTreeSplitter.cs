﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System.Collections.Generic;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Разбивает дерево выбранных свойств на БД-часть и внешнюю часть
    /// </summary>
    public interface ISelectTreeSplitter
    {
        /// <summary>
        /// Удалить из дерева выбранных свойств внешние части
        /// </summary>
        /// <param name="tree">Дерево выбираемых свойств</param>
        /// <returns>Внешние части с путями к ним</returns>
        [NotNull]
        IReadOnlyCollection<KeyValuePair<IReadOnlyCollection<ValueHoldingMember>, SelectTreeNode>> CutExternalTrees([NotNull]SelectTree tree);
    }
}
