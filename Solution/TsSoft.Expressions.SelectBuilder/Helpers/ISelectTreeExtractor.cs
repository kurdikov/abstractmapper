﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Получает дерево выбранных свойств из обработчика
    /// </summary>
    public interface ISelectTreeExtractor
    {
        /// <summary>
        /// Получить дерево выбранных свойств для обработчика
        /// </summary>
        /// <param name="processor">Описание обработчика</param>
        /// <param name="context">Контекст генерации</param>
        SelectTree GetSelectTree(ProcessorDescription processor, GeneratorContext context = null);

        /// <summary>
        /// Получить дерево выбранных свойств для обработчика
        /// </summary>
        /// <param name="entityType">Тип обрабатываемой сущности БД</param>
        /// <param name="processor">Экземпляр обработчика</param>
        /// <param name="context">Контекст генерации</param>
        SelectTree GetSelectTree(Type entityType, object processor, GeneratorContext context = null);
    }
}
