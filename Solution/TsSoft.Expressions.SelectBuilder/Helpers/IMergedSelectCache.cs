﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using JetBrains.Annotations;
    using TsSoft.Expressions.SelectBuilder.Models;

    /// <summary>
    /// Кэш селект-выражений
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    public interface IMergedSelectCache<T>
    {
        /// <summary>
        /// Получить из кэша или создать там селект, являющийся слиянием двух селектов
        /// </summary>
        /// <param name="firstSelect">Основной селект</param>
        /// <param name="secondSelect">Добавляемый селект</param>
        /// <returns>Селект, являющийся слиянием основного и добавляемого</returns>
        [NotNull]
        IReadOnlySelectExpression<T> GetMerged(
            [NotNull]IReadOnlySelectExpression<T> firstSelect,
            [NotNull]IReadOnlySelectExpression<T> secondSelect);
    }
}
