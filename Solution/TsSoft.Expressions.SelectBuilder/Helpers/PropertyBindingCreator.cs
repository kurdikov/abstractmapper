﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;

    internal class PropertyBindingCreator : IPropertyBindingCreator
    {
        [NotNull]private readonly IMemberInfoLibrary _library;
        [NotNull]private readonly ICorrespondingMemberHelper _correspondingMemberHelper;
        [NotNull]private readonly IMemberNamingHelper _memberNamingHelper;

        public PropertyBindingCreator(
            [NotNull]IMemberInfoLibrary library,
            [NotNull]ICorrespondingMemberHelper correspondingMemberHelper,
            [NotNull]IMemberNamingHelper memberNamingHelper)
        {
            if (library == null) throw new ArgumentNullException("library");
            if (correspondingMemberHelper == null) throw new ArgumentNullException("correspondingMemberHelper");
            if (memberNamingHelper == null) throw new ArgumentNullException("memberNamingHelper");
            _library = library;
            _correspondingMemberHelper = correspondingMemberHelper;
            _memberNamingHelper = memberNamingHelper;
        }

        public MemberInitExpression NewDynamic(Type dynamicType, IEnumerable<MemberBinding> bindings)
        {
            return Expression.MemberInit(
                Expression.New(dynamicType),
                bindings);
        }

        [NotNull]
        private MemberAssignment MakeBind([NotNull]ValueHoldingMember entityProperty, [NotNull]Type dynamicType, [NotNull]Expression binding)
        {
            var dynamicProperty = _correspondingMemberHelper.GetCorrespondingMember(dynamicType, entityProperty.Member);
            if (!dynamicProperty.ValueType.GetTypeInfo().IsAssignableFrom(binding.Type))
            {
                throw new InvalidOperationException(string.Format("Unable to bind member {0} declared on {1} to expression {2} of type {3}",
                    dynamicProperty, dynamicType, binding, binding.Type));
            }
            if (!dynamicProperty.IsWriteable)
            {
                var shadow = dynamicType.GetTypeInfo().GetProperty(_memberNamingHelper.WriteableShadow(dynamicProperty.Name));
                if (shadow != null)
                {
                    dynamicProperty = new ValueHoldingMember(shadow);
                }
            }
            return Expression.Bind(dynamicProperty.Member, binding);
        }

        public MemberAssignment BindColumn(ValueHoldingMember entityProperty, Type dynamicType, Expression entity)
        {
            return MakeBind(entityProperty, dynamicType, Expression.MakeMemberAccess(entity, entityProperty.Member));
        }

        public MemberAssignment BindNavigationSingle(
            ValueHoldingMember entityProperty,
            Type dynamicType,
            Type subentityDynamicType,
            Expression entity,
            IEnumerable<MemberAssignment> bindings,
            bool checkNull)
        {
            Expression newDynamic = NewDynamic(
                        subentityDynamicType,
                        bindings);
            var assignment = checkNull
                ? Expression.Condition(
                    Expression.NotEqual(Expression.MakeMemberAccess(entity, entityProperty.Member), Expression.Constant(null)),
                    newDynamic,
                    Expression.Constant(null, subentityDynamicType))
                : newDynamic;
            return MakeBind(entityProperty, dynamicType, assignment);
        }

        [NotNull]
        private Expression MakeWhereIfNeeded(
            [NotNull]ValueHoldingMember entityProperty,
            Type subentityType,
            [NotNull]Expression entity,
            LambdaExpression whereExpression)
        {
            Expression member = Expression.MakeMemberAccess(entity, entityProperty.Member);
            return whereExpression == null
                       ? member
                       : Expression.Call(
                           _library.EnumerableWhere(subentityType),
                           member,
                           whereExpression);
        }

        public MemberAssignment BindNavigationCollection(
            ValueHoldingMember entityProperty,
            Type dynamicType,
            Type subentityType,
            Type subentityDynamicType,
            Expression entity,
            ParameterExpression subentityParameter,
            IEnumerable<MemberAssignment> bindings,
            LambdaExpression whereExpression)
        {
            var enumerable = Expression.Call(
                _library.EnumerableSelect(subentityType, subentityDynamicType),
                MakeWhereIfNeeded(entityProperty, subentityType, entity, whereExpression),
                Expression.Lambda(
                    typeof(Func<,>).MakeGenericType(subentityType, subentityDynamicType),
                    NewDynamic(subentityDynamicType, bindings),
                    subentityParameter));
            if (!entityProperty.ValueType.GetTypeInfo().IsAssignableFrom(enumerable.Type))
            {
                var listElementType = subentityType.GetTypeInfo().IsAssignableFrom(subentityDynamicType)
                    ? subentityType
                    : subentityDynamicType;
                enumerable = Expression.Call(
                    _library.EnumerableToList(listElementType),
                    enumerable);
            }
            return MakeBind(entityProperty, dynamicType, enumerable);
        }
    }
}
