﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using JetBrains.Annotations;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Заменяет в дереве выбранных свойств свойства-обёртки на свойства-столбцы
    /// </summary>
    public interface IWrapperPropertyProcessor
    {
        /// <summary>
        /// Заменить в дереве свойства-обёртки на свойства-столбцы
        /// </summary>
        /// <param name="tree">Дерево выбранных свойств</param>
        /// <returns>Дерево выбранных свойств, в котором нет свойств-обёрток</returns>
        void ProcessWrapperProperties([NotNull]SelectTree tree);
    }
}
