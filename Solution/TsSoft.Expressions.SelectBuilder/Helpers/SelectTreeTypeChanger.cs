namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    class SelectTreeTypeChanger : ISelectTreeTypeChanger
    {
        [NotNull] private readonly ICorrespondingMemberHelper _correspondingMemberHelper;

        public SelectTreeTypeChanger([NotNull] ICorrespondingMemberHelper correspondingMemberHelper)
        {
            if (correspondingMemberHelper == null) throw new ArgumentNullException("correspondingMemberHelper");
            _correspondingMemberHelper = correspondingMemberHelper;
        }

        public SelectTree ChangeType(Type newType, SelectTree tree)
        {
            return new SelectTree(
                newType == tree.Start.Type ? tree.Start : Expression.Parameter(newType),
                ChangeType(newType, tree.RootNode));
        }

        public SelectTreeNode ChangeType(Type newType, SelectTreeNode node)
        {
            if (node == null)
            {
                return null;
            }
            var result = new SelectTreeNode(newType);
            result.AddSelectParameters(node.SelectParameters);
            result.AddCondition(node.Condition, node.ConditionParameters);
            if (node.NextLevel != null)
            {
                result.NextLevel = new SelectTreeLevel(node.NextLevel.Where(kv => kv.Key != null).Select(kv => ChangeSubnode(newType, kv.Key, kv.Value)));
            }
            return result;
        }

        /// <summary>
        /// ���������� ���� ������ �������, ��������� �� ������, � ���� ������ �������, ��������� �� ��������� ��������
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="wrapperProperty"></param>
        /// <param name="subnode"></param>
        /// <returns></returns>
        private KeyValuePair<ValueHoldingMember, SelectTreeNode> ChangeSubnode(
            [NotNull] Type entityType,
            [NotNull] ValueHoldingMember wrapperProperty,
            [CanBeNull] SelectTreeNode subnode)
        {
            var key = _correspondingMemberHelper.GetImplementationIfInterfaceProperty(entityType, wrapperProperty);
            return new KeyValuePair<ValueHoldingMember, SelectTreeNode>(
                key,
                ChangeType(key.ValueType.GetGenericEnumerableArgumentOrSelf(), subnode));
        }
    }
}
