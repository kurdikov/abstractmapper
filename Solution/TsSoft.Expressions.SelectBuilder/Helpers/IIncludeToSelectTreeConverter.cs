﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models.Path;

    /// <summary>
    /// Преобразует include-выражения в дерево выбранных свойств
    /// </summary>
    public interface IIncludeToSelectTreeConverter
    {
        /// <summary>
        /// Построить дерево выбранных свойств по include-выражениям
        /// </summary>
        /// <typeparam name="T">Тип выбираемой сущности</typeparam>
        /// <param name="includes">Include-выражения</param>
        [NotNull]
        SelectTree Convert<T>([CanBeNull]IReadOnlyIncludes<T> includes);

        /// <summary>
        /// Построить дерево выбранных свойств по include-выражениям
        /// </summary>
        /// <typeparam name="T">Тип выбираемой сущности</typeparam>
        /// <param name="includes">Include-выражения</param>
        [NotNull]
        SelectTree Convert<T>([CanBeNull]IEnumerable<Expression<Func<T, object>>> includes);
    }
}
