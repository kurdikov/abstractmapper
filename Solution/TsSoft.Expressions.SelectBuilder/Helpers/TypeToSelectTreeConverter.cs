﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    class TypeToSelectTreeConverter : ITypeToSelectTreeConverter
    {
        [NotNull] private readonly IEntityTypesHelper _entityTypesHelper;
        [NotNull] private readonly ISelectTreeTypeChanger _typeChanger;

        public TypeToSelectTreeConverter([NotNull] IEntityTypesHelper entityTypesHelper, [NotNull] ISelectTreeTypeChanger typeChanger)
        {
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            if (typeChanger == null) throw new ArgumentNullException("typeChanger");
            _entityTypesHelper = entityTypesHelper;
            _typeChanger = typeChanger;
        }

        public SelectTree MakeTree<TEntity, TInterface>(bool ignoreEntityTypes) where TEntity : TInterface
        {
            var interfaceResult = new SelectTree(Expression.Parameter(typeof(TInterface)))
            {
                RootNode = {NextLevel = GetNextLevel(typeof(TInterface), ignoreEntityTypes)}
            };
            var result = _typeChanger.ChangeType(typeof(TEntity), interfaceResult);
            return result;
        }

        private SelectTreeLevel GetNextLevel([NotNull]Type interfaceType, bool ignoreEntityTypes)
        {
            if (interfaceType.GetTypeInfo().IsClass)
            {
                if (ignoreEntityTypes)
                {
                    return null;
                }
                throw new InvalidOperationException(string.Format("Entity type and interface type are the same: {0}", interfaceType));
            }
            var properties = GetProperties(interfaceType);
            var result = new SelectTreeLevel();
            foreach (var property in properties)
            {
                if (property == null)
                {
                    continue;
                }
                if (_entityTypesHelper.IsColumnProperty(property))
                {
                    result[property] = null;
                }
                else
                {
                    var singleType = property.ValueType.GetGenericEnumerableArgumentOrSelf();
                    var nextLevel = GetNextLevel(singleType, ignoreEntityTypes);
                    if (nextLevel != null)
                    {
                        result[property] = new SelectTreeNode(singleType)
                        {
                            NextLevel = nextLevel,
                        };
                    }
                }
            }
            return result;
        }

        [NotNull]
        private IEnumerable<ValueHoldingMember> GetProperties([NotNull]Type type)
        {
            return ValueHoldingMember.GetValueHoldingMembers(type);
        }

        public SelectTree MakePkTree<TEntity>()
        {
            var result = new SelectTree(Expression.Parameter(typeof(TEntity)));
            var pk = _entityTypesHelper.GetPrimaryKey(typeof(TEntity));
            result.RootNode.NextLevel = new SelectTreeLevel(
                pk.Select(pkProp => new KeyValuePair<ValueHoldingMember, SelectTreeNode>(pkProp, null)));
            return result;
        }
    }
}
