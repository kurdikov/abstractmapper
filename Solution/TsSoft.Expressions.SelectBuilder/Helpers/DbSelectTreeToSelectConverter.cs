namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Entity;
    using TsSoft.Expressions.Helpers.Exceptions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Helpers.Visitors;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.DynamicTypes;
    using TsSoft.Expressions.SelectBuilder.Models;

    class DbSelectTreeToSelectConverter : IDbSelectTreeToSelectConverter
    {
        [NotNull]private readonly IEntityDynamicTypesHelper _dynamicTypesHelper;
        [NotNull]private readonly IEntityTypesHelper _entityTypesHelper;
        [NotNull]private readonly IPropertyBindingCreator _bindingCreator;
        [NotNull] private readonly ISelectSettingsHelper _selectSettingsHelper;

        public DbSelectTreeToSelectConverter(
            [NotNull] IEntityDynamicTypesHelper dynamicTypesHelper,
            [NotNull] IEntityTypesHelper entityTypesHelper,
            [NotNull] IPropertyBindingCreator bindingCreator,
            [NotNull] ISelectSettingsHelper selectSettingsHelper)
        {
            if (dynamicTypesHelper == null) throw new ArgumentNullException("dynamicTypesHelper");
            if (entityTypesHelper == null) throw new ArgumentNullException("entityTypesHelper");
            if (bindingCreator == null) throw new ArgumentNullException("bindingCreator");
            if (selectSettingsHelper == null) throw new ArgumentNullException("selectSettingsHelper");
            _dynamicTypesHelper = dynamicTypesHelper;
            _entityTypesHelper = entityTypesHelper;
            _bindingCreator = bindingCreator;
            _selectSettingsHelper = selectSettingsHelper;
        }

        public Expression<Func<T, object>> Convert<T>(SelectTree tree, SelectSettings settings)
        {
            if (tree.Start.Type != typeof(T))
            {
                throw new ArgumentException(string.Format("Type mismatch: requested select expression for {0}, but the tree has type {1}",
                    typeof(T),
                    tree.Start.Type));
            }
            var pool = _dynamicTypesHelper.GetPool();
            var outerDynamicType = pool.GetNext(typeof(T));
            var result =  Expression.Lambda<Func<T, object>>(
                _bindingCreator.NewDynamic(
                    outerDynamicType,
                    GetBindings(tree.Start, outerDynamicType, pool, tree.RootNode, settings, new Dictionary<ParameterExpression, Expression>())),
                tree.Start);
            return result;
        }

        [NotNull]
        private IEnumerable<MemberAssignment> GetAllColumnBindings(
            [NotNull]Type entityType,
            [NotNull]Type dynamicType,
            [NotNull]Expression prefix)
        {
            return _entityTypesHelper.GetColumnProperties(entityType).Where(cp => cp != null).Select(
                cp => _bindingCreator.BindColumn(cp, dynamicType, prefix));
        }
            
        [NotNull]
        private IEnumerable<MemberAssignment> GetBindings(
            [NotNull]Expression prefix,
            [NotNull]Type dynamicType,
            [NotNull]IDynamicTypePool pool,
            [NotNull]SelectTreeNode node,
            [NotNull]SelectSettings settings,
            [NotNull]IReadOnlyDictionary<ParameterExpression, Expression> parameterReplacements)
        {
            if (node.NextLevel == null)
            {
                yield break;
            }
            var newParameterReplacements = new Dictionary<ParameterExpression, Expression>();
            foreach (var replacement in parameterReplacements)
            {
                newParameterReplacements[replacement.Key] = replacement.Value;
            }
            AddParameterReplacements(newParameterReplacements, node.SelectParameters, prefix);
            foreach (var treeLevelElement in node.NextLevel)
            {
                var property = treeLevelElement.Key;
                var nextLevel = treeLevelElement.Value;
                if (property == null)
                {
                    continue;
                }
                if (_entityTypesHelper.IsColumnProperty(property))
                {
                    yield return _bindingCreator.BindColumn(property, dynamicType, prefix);
                }
                else if (_entityTypesHelper.IsNavigationalSingleProperty(property))
                {
                    var subentityDynamicType = pool.GetNext(property.ValueType);
                    var nextPrefix = Expression.MakeMemberAccess(prefix, property.Member);
                    var subentityBindings = nextLevel != null 
                        ? GetBindings(
                            nextPrefix,
                            subentityDynamicType,
                            pool,
                            nextLevel,
                            settings,
                            newParameterReplacements)
                        : GetAllColumnBindings(property.ValueType, subentityDynamicType, nextPrefix);
                    yield return _bindingCreator.BindNavigationSingle(
                        property, dynamicType, subentityDynamicType, prefix, subentityBindings,
                        _selectSettingsHelper.ShouldCheckNull(settings, property));
                }
                else if (_entityTypesHelper.IsNavigationalCollectionProperty(property))
                {
                    var singleType = property.ValueType.GetGenericEnumerableArgument();
                    var innerDynamicType = pool.GetNext(singleType);
                    var innerParameter = Expression.Parameter(singleType);
                    var innerBindings = nextLevel != null
                        ? GetBindings(innerParameter, innerDynamicType, pool, nextLevel, settings, newParameterReplacements)
                        : GetAllColumnBindings(singleType, innerDynamicType, innerParameter);
                    yield return _bindingCreator.BindNavigationCollection(
                        property,
                        dynamicType,
                        singleType,
                        innerDynamicType,
                        prefix,
                        innerParameter,
                        innerBindings,
                        nextLevel != null ? GetCondition(singleType, nextLevel.Condition, nextLevel.ConditionParameters, newParameterReplacements) : null);
                }
                else
                {
                    throw new InvalidPathException(string.Format("Path contains not supported property {0} of {1}", property, property.DeclaringType));
                }
            }
        }

        private void AddParameterReplacements(
            [NotNull]IDictionary<ParameterExpression, Expression> parameterReplacements,
            [NotNull]IEnumerable<ParameterExpression> selectParameters,
            [NotNull]Expression prefix)
        {
            foreach (var selectParam in selectParameters)
            {
                if (selectParam == null)
                {
                    continue;
                }
                parameterReplacements[selectParam] = prefix;
            }
        }


        private LambdaExpression GetCondition(
            [NotNull]Type parameterType,
            Expression condition,
            [NotNull]IEnumerable<ParameterExpression> conditionParameters,
            [NotNull]IDictionary<ParameterExpression, Expression> parameterReplacements)
        {
            if (condition == null)
            {
                return null;
            }
            var param = Expression.Parameter(parameterType);
            AddParameterReplacements(parameterReplacements, conditionParameters, param);
            var fixedBody = ParameterReplacerVisitor.Replace(condition, parameterReplacements);
            return Expression.Lambda(typeof(Func<,>).MakeGenericType(parameterType, typeof(bool)),
                fixedBody,
                param);
        }
    }
}
