﻿namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.NestedGenerators;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.IncludeBuilder.Models;
    using TsSoft.Expressions.Models.AbstractMapper;
    using TsSoft.Expressions.Models.Path;
    using TsSoft.Expressions.SelectBuilder.Models;

    class SelectTreeExtractor : ISelectTreeExtractor
    {
        [NotNull] private readonly ISelectToSelectTreeConverter _selectConverter;
        [NotNull] private readonly IIncludeToSelectTreeConverter _includeConverter;
        [NotNull] private readonly INestedGeneratorHelper _nestedGeneratorHelper;

        [NotNull] private readonly MethodInfo _tryGetProcessorSelect;

        public SelectTreeExtractor(
            [NotNull] ISelectToSelectTreeConverter selectConverter,
            [NotNull] IIncludeToSelectTreeConverter includeConverter,
            [NotNull] IMemberInfoHelper memberInfoHelper,
            [NotNull] INestedGeneratorHelper nestedGeneratorHelper)
        {
            if (selectConverter == null) throw new ArgumentNullException("selectConverter");
            if (includeConverter == null) throw new ArgumentNullException("includeConverter");
            if (memberInfoHelper == null) throw new ArgumentNullException("memberInfoHelper");
            if (nestedGeneratorHelper == null) throw new ArgumentNullException("nestedGeneratorHelper");
            _selectConverter = selectConverter;
            _includeConverter = includeConverter;
            _nestedGeneratorHelper = nestedGeneratorHelper;

            _tryGetProcessorSelect = memberInfoHelper.GetGenericDefinitionMethodInfo(() => TryGetProcessorSelect<object>(null, null));
        }

        private SelectTree GetFromSelectProvider<TInner>(ISelectProvider<TInner> processor)
        {
            if (processor != null)
            {
                return SelectToAssignments(processor.Select);
            }
            return null;
        }

        private SelectTree SelectToAssignments<TInner>(IReadOnlySelectExpression<TInner> select)
        {
            if (select != null && select.Select != null)
            {
                var result = _selectConverter.Convert(select.Select);
                result.ExternalIncludes = select.ExternalIncludes;
                return result;
            }
            return null;
        }

        private SelectTree IncludesToAssignments<TInner>(IReadOnlyIncludes<TInner> includes)
        {
            if (includes == null)
            {
                return null;
            }
            return _includeConverter.Convert(includes);
        }

        private SelectTree GetFromIncludeProvider<TInner>(IIncludeProvider<TInner> processor)
        {
            if (processor != null)
            {
                return IncludesToAssignments(processor.Includes);
            }
            return null;
        }

        private SelectTree TryGetProcessorSelect<TInner>(object processor, GeneratorContext context)
        {
            return GetFromSelectGenerator(processor as ICyclePreventingSelectGenerator<TInner>, context)
                   ?? GetFromSelectProvider(processor as ISelectProvider<TInner>)
                   ?? GetFromIncludeGenerator(processor as ICyclePreventingIncludeGenerator<TInner>, context)
                   ?? GetFromIncludeProvider(processor as IIncludeProvider<TInner>);
        }

        private SelectTree GetFromIncludeGenerator<TInner>(ICyclePreventingIncludeGenerator<TInner> cyclePreventingIncludeGenerator, GeneratorContext context)
        {
            return _nestedGeneratorHelper.GenerateInContext(
                cyclePreventingIncludeGenerator,
                g => g.Includes,
                (g, c) => g.GenerateIncludes(c),
                IncludesToAssignments,
                context,
                SelectTree.ExhaustedNesting);
        }

        private SelectTree GetFromSelectGenerator<TInner>(ICyclePreventingSelectGenerator<TInner> cyclePreventingSelectGenerator, GeneratorContext context)
        {
            return _nestedGeneratorHelper.GenerateInContext(
                cyclePreventingSelectGenerator,
                g => g.Select,
                (g, c) => g.GenerateSelect(c),
                SelectToAssignments,
                context,
                SelectTree.ExhaustedNesting);
        }

        public SelectTree GetSelectTree(ProcessorDescription processor, GeneratorContext context)
        {
            if (processor == null)
            {
                return null;
            }
            var method = _tryGetProcessorSelect.MakeGenericMethod(processor.ProcessedType);
            var @delegate = DelegateCreator.Create<Func<object, GeneratorContext, SelectTree>>(this, method);
            return @delegate(processor.Processor, context);
        }

        public SelectTree GetSelectTree(Type entityType, object processor, GeneratorContext context)
        {
            if (processor == null)
            {
                return null;
            }
            var method = _tryGetProcessorSelect.MakeGenericMethod(entityType);
            var @delegate = DelegateCreator.Create<Func<object, GeneratorContext, SelectTree>>(this, method);
            return @delegate(processor, context);
        }
    }
}
