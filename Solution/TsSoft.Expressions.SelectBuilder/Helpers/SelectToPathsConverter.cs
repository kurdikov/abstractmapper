namespace TsSoft.Expressions.SelectBuilder.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using JetBrains.Annotations;
    using TsSoft.Expressions.Helpers;
    using TsSoft.Expressions.Helpers.Extensions;
    using TsSoft.Expressions.Helpers.Reflection;
    using TsSoft.Expressions.Models;
    using TsSoft.Expressions.Models.Path;

    class SelectToPathsConverter : ISelectToPathsConverter
    {
        [NotNull] private readonly INewExpressionHelper _newExpressionHelper;

        public SelectToPathsConverter([NotNull] INewExpressionHelper newExpressionHelper)
        {
            if (newExpressionHelper == null) throw new ArgumentNullException("newExpressionHelper");
            _newExpressionHelper = newExpressionHelper;
        }

        public IEnumerable<ParsedPath> Convert<T>(Expression<Func<T, object>> @select)
        {
            var parameter = select.Parameters[0];
            if (parameter == null)
            {
                throw new InvalidOperationException(string.Format("Parameter of select expression is null: {0}", select));
            }
            var builder = new ParsedPathBuilder(parameter);
            var topLevelAssignments = _newExpressionHelper.GetAssignments(select);
            return MakePathsByAssignments(builder, topLevelAssignments, typeof(T)).Where(b => b != null).Select(b => b.ToPath());
        }

        [NotNull]
        private IEnumerable<ParsedPathBuilder> MakePathsByAssignments(
            [NotNull]ParsedPathBuilder prefix, 
            [NotNull]IEnumerable<KeyValuePair<MemberInfo, Expression>> assignments, 
            [NotNull]Type entityType)
        {
            return assignments.Select(a => MakePathsByAssignment(prefix, a, entityType)).SelectMany(p => p);
        }

        [NotNull]
        private IEnumerable<ParsedPathBuilder> MakePathsByAssignment(
            [NotNull] ParsedPathBuilder prefix,
            KeyValuePair<MemberInfo, Expression> assignment,
            [NotNull] Type entityType)
        {
            var parsedAssignment = _newExpressionHelper.ParseAssignment(assignment, entityType);
            if (parsedAssignment.UsedMember == null)
            {
                throw new InvalidOperationException(string.Format("Unable to find property used in assignment {0} = {1}", assignment.Key, assignment.Value));
            }
            var condition = parsedAssignment.Condition != null
                ? new PathCondition(
                    parsedAssignment.Condition.Parameters[0],
                    parsedAssignment.Condition,
                    parsedAssignment.Condition,
                    new Multidictionary<ParameterExpression, ParsedPath>(), 
                    null)
                : null;
            var newPrefix = prefix.AddElement(new PathElement(
                parsedAssignment.UsedMember, 
                condition));
            IEnumerable<ParsedPathBuilder> result;
            if (parsedAssignment.InnerSelect != null)
            {
                var innerAssignments = _newExpressionHelper.GetAssignments(parsedAssignment.InnerSelect);
                result = MakePathsByAssignments(newPrefix, innerAssignments, parsedAssignment.UsedMember.ValueType.GetGenericEnumerableArgument());
            }
            else if (parsedAssignment.InnerAssignment != null)
            {
                result = MakePathsByAssignments(newPrefix, parsedAssignment.InnerAssignment, parsedAssignment.UsedMember.ValueType);
            }
            else
            {
                result = newPrefix.ToEnumerable();
            }
            return result;
        }
    }
}
