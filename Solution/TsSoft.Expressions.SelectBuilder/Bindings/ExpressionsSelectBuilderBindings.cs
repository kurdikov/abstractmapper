﻿namespace TsSoft.Bindings
{
    using TsSoft.BindingsDescription;
    using TsSoft.Expressions.SelectBuilder;
    using TsSoft.Expressions.SelectBuilder.DynamicTypes;
    using TsSoft.Expressions.SelectBuilder.Helpers;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке TsSoft.Expressions.SelectBuilder
    /// </summary>
    public class ExpressionsSelectBuilderBindings : BindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке TsSoft.Expressions.SelectBuilder
        /// </summary>
        public ExpressionsSelectBuilderBindings()
        {
            Bind<IPropertyBindingCreator, PropertyBindingCreator>();
            Bind<ITypeBuilderCreator, TypeBuilderCreator>();
            Bind<ITypeClonemaker, TypeClonemaker>();
            Bind<IEntityDynamicTypesHelper, EntityDynamicTypesHelper>();
            Bind<ISelectHelper, SelectHelper>();
            Bindings.Add(typeof(IMergedSelectCache<>), typeof(MergedSelectCache<>));
            Bind<IPathToSelectConverter, PathToSelectConverter>();
            Bind<ISelectToPathsConverter, SelectToPathsConverter>();
            Bind<ISelectSettingsHelper, SelectSettingsHelper>();
            Bind<IDbSelectTreeToSelectConverter, DbSelectTreeToSelectConverter>();
            Bind<ISelectToSelectTreeConverter, SelectToSelectTreeConverter>();
            Bind<ISelectTreeSplitter, SelectTreeSplitter>();
            Bind<IWrapperPropertyProcessor, WrapperPropertyProcessor>();
            Bind<IIncludeToSelectTreeConverter, IncludeToSelectTreeConverter>();
            Bind<ISelectTreeExtractor, SelectTreeExtractor>();
            Bind<IParsedPathsToSelectTreeConverter, ParsedPathsToSelectTreeConverter>();
            Bind<ISelectTreeMerger, SelectTreeMerger>();
            Bind<ISelectTreeTypeChanger, SelectTreeTypeChanger>();
            Bind<ISelectTreeToSelectConverter, SelectTreeToSelectConverter>();
            Bind<ITypeToSelectTreeConverter, TypeToSelectTreeConverter>();
        }
    }
}
