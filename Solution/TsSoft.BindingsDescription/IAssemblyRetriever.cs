namespace TsSoft.BindingsDescription
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// �������� ��� ������-�����������, ������� ��������
    /// </summary>
    public interface IAssemblyRetriever
    {
        /// <summary>
        /// �������� ������-�����������, ������� ��������
        /// </summary>
        /// <param name="tssoftTypes">��������������� ������������ ������������ ����</param>
        /// <param name="shouldLoad">��������</param>
        /// <returns>��������� ������-������������</returns>
        ICollection<Assembly> GetAssembliesWithReferences(
            IEnumerable<Type> tssoftTypes,
            Func<AssemblyName, bool> shouldLoad = null);
    }
}