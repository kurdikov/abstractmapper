﻿namespace TsSoft.BindingsDescription
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Получает сборки-зависимости, включая непрямые
    /// </summary>
    public class AssemblyRetriever : IAssemblyRetriever
    {
        private static bool LoadOnlyTsSoftAssemblies(AssemblyName name)
        {
            return name != null && name.FullName.StartsWith("TsSoft.");
        }

        /// <summary>
        /// Получить сборки-зависимости, включая непрямые
        /// </summary>
        /// <param name="tssoftTypes">Непосредственно используемые библиотечные типы</param>
        /// <param name="shouldLoad">Предикат</param>
        /// <returns>Коллекция сборок-зависимостей</returns>
        [NotNull]
        public ICollection<Assembly> GetAssembliesWithReferences(
            IEnumerable<Type> tssoftTypes,
            Func<AssemblyName, bool> shouldLoad = null)
        {
            shouldLoad = shouldLoad ?? LoadOnlyTsSoftAssemblies;
            var assemblies = new HashSet<Assembly>(
                (tssoftTypes ?? Enumerable.Empty<Type>())
                    .Where(t => t != null)
                    .Select(t => t.GetTypeInfo().Assembly)
                    .Where(a => shouldLoad(a.GetName())));
            IEnumerable<Assembly> references;
            do
            {
                references = assemblies.SelectMany(a => a != null ? a.GetReferencedAssemblies() : Enumerable.Empty<AssemblyName>())
                                           .Where(shouldLoad)
                                           .Select(Assembly.Load)
                                           .Except(assemblies)
                                           .ToList();
                assemblies.UnionWith(references);
            } while (references.Any());
            return assemblies;
        }
    }
}
