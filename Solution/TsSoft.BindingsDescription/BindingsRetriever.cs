﻿namespace TsSoft.BindingsDescription
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using JetBrains.Annotations;

    /// <summary>
    /// Получает описание интерфейсов и их реализаций из сборок
    /// </summary>
    public class BindingsRetriever : IBindingsRetriever
    {

        [NotNull]
        private string GetMessage(Assembly assembly, [NotNull]ReflectionTypeLoadException reflectionTypeLoadException)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("Unable to load assembly {0}. Failed to load types: ", assembly);
            sb.Append(string.Join(", ", reflectionTypeLoadException.Types ?? Enumerable.Empty<Type>()));
            sb.Append(". Exceptions: ");
            sb.Append(string.Join(
                Environment.NewLine,
                reflectionTypeLoadException.LoaderExceptions ?? Enumerable.Empty<Exception>()));
            return sb.ToString();
        }

        [NotNull]
        private IEnumerable<Type> GetTypes([NotNull]Assembly assembly)
        {
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                throw new Exception(GetMessage(assembly, ex), ex);
            }
        }

        /// <summary>
        /// Получить описание интерфейсов и их реализаций из сборок
        /// </summary>
        /// <param name="assemblies">Сборки</param>
        /// <returns>Множество интерфейсов и их реализаций</returns>
        public HashSet<KeyValuePair<Type, Type>> GetBindings(IEnumerable<Assembly> assemblies)
        {
            var bindings = new HashSet<KeyValuePair<Type, Type>>();
            foreach (var assembly in assemblies ?? Enumerable.Empty<Assembly>())
            {
                if (assembly == null)
                {
                    throw new ArgumentException("assemblies contains null");
                }
                var bindingDescriptionTypes = GetTypes(assembly)
                    .Where(t =>
                    {
                        try
                        {
                            var info = t != null ? t.GetTypeInfo() : null;
                            return
                                info != null
                                && info.IsClass
                                && !info.IsAbstract
                                && typeof(IBindingsDescription).GetTypeInfo().IsAssignableFrom(t)
                                && info.GetConstructors().Any(c => c != null && !c.GetParameters().Any());
                        }
                        catch (Exception e)
                        {
                            throw new InvalidOperationException(string.Format("Can not load type {0} from assembly {1}", t, t != null ? t.GetTypeInfo().Assembly : null), e);
                        }
                    });
                foreach (var bindingDescriptionType in bindingDescriptionTypes)
                {
                    if (bindingDescriptionType == null)
                    {
                        throw new NullReferenceException("bindingDescriptionType");
                    }
                    var description = Activator.CreateInstance(bindingDescriptionType) as IBindingsDescription;
                    if (description == null)
                    {
                        throw new NullReferenceException("description");
                    }
                    // ReSharper disable once ConstantNullCoalescingCondition
                    var currentBindings = description.GetBindings() ?? Enumerable.Empty<KeyValuePair<Type, Type>>();
                    bindings.UnionWith(currentBindings);
                }
            }
            return bindings;
        }
    }
}
