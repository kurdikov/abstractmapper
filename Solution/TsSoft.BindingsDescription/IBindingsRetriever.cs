﻿namespace TsSoft.BindingsDescription
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Получает описание интерфейсов и их реализаций из сборок
    /// </summary>
    public interface IBindingsRetriever
    {
        /// <summary>
        /// Получить описание интерфейсов и их реализаций из сборок
        /// </summary>
        /// <param name="assemblies">Анализируемые сборки</param>
        /// <returns>Множество интерфейсов и их реализаций</returns>
        [NotNull]
        HashSet<KeyValuePair<Type, Type>> GetBindings(IEnumerable<Assembly> assemblies);
    }
}
