﻿namespace TsSoft.BindingsDescription
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Описание интерфейсов и их реализаций в сборке
    /// </summary>
    public abstract class BindingsDescription : IBindingsDescription
    {
        /// <summary>
        /// Описание интерфейсов и их реализаций в сборке
        /// </summary>
        [NotNull]
        protected readonly Dictionary<Type, Type> Bindings = new Dictionary<Type, Type>();

        /// <summary>
        /// Добавить интерфейс и его стандартную реализацию
        /// </summary>
        /// <typeparam name="TInterface">Тип-интерфейс</typeparam>
        /// <typeparam name="TImplementation">Тип-реализация по умолчанию</typeparam>
        protected void Bind<TInterface, TImplementation>()
            where TImplementation : class, TInterface
        {
            var typeInfo = typeof(TImplementation).GetTypeInfo();
            if (!typeInfo.IsClass || typeInfo.IsAbstract)
            {
                throw new InvalidOperationException("Implementation should be a non-abstract class");
            }
            Bindings.Add(typeof(TInterface), typeof(TImplementation));
        }

        /// <summary>
        /// Получить интерфейсы и их реализации
        /// </summary>
        /// <returns>Список интерфейсов и их реализаций</returns>
        public IReadOnlyDictionary<Type, Type> GetBindings()
        {
            return Bindings;
        }
    }
}
