﻿namespace TsSoft.BindingsDescription
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    /// Описание интерфейсов и их стандартных реализаций в сборке
    /// </summary>
    public interface IBindingsDescription
    {
        /// <summary>
        /// Получить описание интерфейсов и их стандартных реализаций
        /// </summary>
        /// <returns>Список пар интерфейс-реализация</returns>
        [NotNull]
        IReadOnlyDictionary<Type, Type> GetBindings();
    }
}
