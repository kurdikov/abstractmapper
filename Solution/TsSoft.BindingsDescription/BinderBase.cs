﻿namespace TsSoft.BindingsDescription
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    /// Базовый класс для связывания интерфейсов с реализациями
    /// </summary>
    public abstract class BinderBase
    {
        [NotNull]private readonly IBindingsRetriever _bindingsRetriever;
        [NotNull]private readonly IAssemblyRetriever _assemblyRetriever;

        [NotNull] private readonly HashSet<KeyValuePair<Type, Type>> _bindings;

        /// <summary>
        /// Базовый класс для связывания интерфейсов с реализациями
        /// </summary>
        protected BinderBase(IBindingsRetriever bindingsRetriever, IAssemblyRetriever assemblyRetriever)
        {
            _bindingsRetriever = bindingsRetriever ?? new BindingsRetriever();
            _assemblyRetriever = assemblyRetriever ?? new AssemblyRetriever();

            _bindings = new HashSet<KeyValuePair<Type, Type>>();
        }

        /// <summary>
        /// Добавить связи интерфейсов со стандартными реализациями
        /// </summary>
        /// <typeparam name="T">Тип-описание связей интерфейсов со стандартными реализациями</typeparam>
        public BinderBase Add<T>()
            where T : IBindingsDescription
        {
            _bindings.UnionWith(_bindingsRetriever.GetBindings(new[] {typeof(T).GetTypeInfo().Assembly}));
            return this;
        }

        /// <summary>
        /// Добавить связи интерфейсов со стандартными реализациями, описанные в классе связей, а также в сборках, от которых зависит его сборка
        /// </summary>
        /// <typeparam name="T">Тип-описание связей интерфейсов со стандартными реализациями</typeparam>
        public BinderBase AddWithDependencies<T>()
        {
            _bindings.UnionWith(_bindingsRetriever.GetBindings(_assemblyRetriever.GetAssembliesWithReferences(new[] { typeof(T) })));
            return this;
        }

        /// <summary>
        /// Добавить связи интерфейсов со стандартными реализациями из сборок с указанными типами и их зависимостей
        /// </summary>
        /// <param name="types">Типы, связи из сборок с которыми нужно загрузить</param>
        /// <param name="shouldLoad">Какие зависимые сборки грузить</param>
        public BinderBase AddByUsedTypes(IEnumerable<Type> types, Func<AssemblyName, bool> shouldLoad = null)
        {
            _bindings.UnionWith(_bindingsRetriever.GetBindings(_assemblyRetriever.GetAssembliesWithReferences(types, shouldLoad)));
            return this;
        }

        /// <summary>
        /// Добавить связи интерфейсов со стандартными реализациями из сборок с указанными типами и их зависимостей
        /// </summary>
        /// <param name="assemblies">Сборки, связи из которых нужно загрузить</param>
        public BinderBase AddFromAssemblies(IEnumerable<Assembly> assemblies)
        {
            _bindings.UnionWith(_bindingsRetriever.GetBindings(assemblies));
            return this;
        }

        /// <summary>
        /// Исключить связь интерфейса
        /// </summary>
        /// <typeparam name="T">Исключаемый интерфейс</typeparam>
        /// <returns></returns>
        public BinderBase Except<T>()
        {
            _bindings.RemoveWhere(kv => kv.Key == typeof(T));
            return this;
        }

        /// <summary>
        /// Осуществить связывание
        /// </summary>
        /// <param name="bindSingle">Функция, осуществляющая связывание одного типа</param>
        protected void Bind([NotNull]Action<KeyValuePair<Type, Type>> bindSingle)
        {
            foreach (var binding in _bindings)
            {
                bindSingle(binding);
            }
        }
    }
}
