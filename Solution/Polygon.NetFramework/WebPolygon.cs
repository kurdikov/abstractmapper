﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using Microsoft.Owin.Hosting;
using Owin;

namespace Polygon
{
    class WebPolygon
    {
        public void Run()
        {
            using (WebApp.Start<Startup>("http://localhost:7254"))
            {
                Console.ReadLine();
            }
        }

        public class Startup
        {
            public void Configuration(IAppBuilder app)
            {
                var config = new HttpConfiguration();
                config.Services.Replace(typeof(IHttpControllerTypeResolver), new HttpControllerTypeResolver());
                config.MapHttpAttributeRoutes();
                config.EnsureInitialized();
                app.UseWebApi(config);
            }
        }

        public class HttpControllerTypeResolver : IHttpControllerTypeResolver
        {
            public ICollection<Type> GetControllerTypes(IAssembliesResolver assembliesResolver)
            {
                return new[]
                {
                    typeof(TestController),
                };
            }
        }

        public class TestController : ApiController
        {
            [Route("")]
            [HttpGet]
            public Task<int> Get()
            {
                return Task.FromResult(0);
            }
        }
    }
}
