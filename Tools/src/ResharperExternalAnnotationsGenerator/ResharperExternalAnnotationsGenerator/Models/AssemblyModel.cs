﻿namespace ResharperExternalAnnotationsGenerator.Models
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Serialization;

    [XmlRoot("assembly")]
    public class AssemblyModel
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("member")]
        public List<MemberModel> Members { get; set; }
    }

    public class MemberModel
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("attribute")]
        public List<AttributeModel> Attributes { get; set; } 

        [XmlElement("parameter")]
        public List<ParameterModel> Parameters { get; set; }

        public bool IsNotEmpty()
        {
            return Attributes != null && Attributes.Count > 0
                   || Parameters != null && Parameters.Any(p => p != null && p.IsNotEmpty());
        }
    }

    public class AttributeModel
    {
        [XmlAttribute("ctor")]
        public string Constructor { get; set; }

        [XmlElement("argument")]
        public List<string> Arguments { get; set; }
    }

    public class ParameterModel
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("attribute")]
        public List<AttributeModel> Attributes { get; set; }

        public bool IsNotEmpty()
        {
            return Attributes != null && Attributes.Count > 0;
        }
    }
}
