﻿namespace ResharperExternalAnnotationsGenerator
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Xml.Serialization;
    using ResharperExternalAnnotationsGenerator.Models;

    class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length < 1 || args[0] == null)
            {
                Console.WriteLine("Usage: {0} [assembly.dll]", Process.GetCurrentProcess().ProcessName);
                Console.ReadLine();
                return;
            }

            var assembly = Assembly.LoadFrom(Path.GetFullPath(args[0]));
            if (assembly == null)
            {
                throw new InvalidDataException(string.Format("Assembly loaded from {0} is null", args[0]));
            }
            var analyzed = new Analyzer().AnalyzeAssembly(assembly);
            var serializer = new XmlSerializer(typeof(AssemblyModel));
            if (Console.Out == null)
            {
                throw new InvalidOperationException("Console.Out is null");
            }
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            serializer.Serialize(Console.Out, analyzed, ns);
        }
    }
}
