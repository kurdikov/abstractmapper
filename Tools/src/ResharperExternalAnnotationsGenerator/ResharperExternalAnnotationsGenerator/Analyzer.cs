﻿namespace ResharperExternalAnnotationsGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Xml;
    using JetBrains.Annotations;
    using ResharperExternalAnnotationsGenerator.Models;

    internal class Analyzer
    {
        [NotNull]
        private readonly Type[] _noTypes = new Type[0];

        [NotNull]
        public AssemblyModel AnalyzeAssembly([NotNull] Assembly assembly)
        {
            return new AssemblyModel
            {
                Name = assembly.GetName().Name,
                Members = assembly.GetTypes().Where(t => t != null && t.IsPublic).SelectMany(AnalyzeType).ToList(),
            };
        }

        public IEnumerable<MemberModel> AnalyzeType([NotNull] Type type)
        {
            var genericArguments = _noTypes;
            if (type.IsGenericType)
            {
                genericArguments = type.GetGenericArguments();
            }
            return type.GetMethods().Select(m => AnalyzeMethod(m, genericArguments))
                .Concat(type.GetProperties().Select(p => AnalyzeProperty(p, genericArguments)))
                .Concat(type.GetConstructors().Select(c => AnalyzeConstructor(c, genericArguments)))
                .Concat(type.GetFields().Select(f => AnalyzeField(f, genericArguments)))
                .Concat(type.GetNestedTypes().Where(t => t != null & t.IsPublic).SelectMany(AnalyzeType))
                .Where(member => member.IsNotEmpty());
        }

        public MemberModel AnalyzeMethod([NotNull] MethodInfo method, [NotNull]Type[] typeArguments)
        {
            var parameters = method.GetParameters();
            Type[] methodArguments = _noTypes;
            if (method.IsGenericMethodDefinition)
            {
                methodArguments = method.GetGenericArguments();
            }
            var result = new MemberModel
            {
                Name = GetName('M', method, parameters, typeArguments, methodArguments),
                Attributes = GetAttributes(method).ToList(),
                Parameters = parameters.Select(AnalyzeParameter).ToList(),
            };
            return result;
        }

        public MemberModel AnalyzeProperty([NotNull] PropertyInfo property, [NotNull]Type[] typeArguments)
        {
            var parameters = property.GetIndexParameters();
            var result = new MemberModel
            {
                Name = GetName('P', property, parameters, typeArguments, _noTypes),
                Attributes = GetAttributes(property).ToList(),
                Parameters = parameters.Select(AnalyzeParameter).ToList(),
            };
            return result;
        }

        public MemberModel AnalyzeConstructor([NotNull] ConstructorInfo constructor, [NotNull]Type[] typeArguments)
        {
            var parameters = constructor.GetParameters();
            var result = new MemberModel
            {
                Name = GetName('M', constructor, parameters, typeArguments, _noTypes),
                Attributes = GetAttributes(constructor).ToList(),
                Parameters = parameters.Select(AnalyzeParameter).ToList(),
            };
            return result;
        }

        public MemberModel AnalyzeField([NotNull] FieldInfo field, [NotNull]Type[] typeArguments)
        {
            return new MemberModel
            {
                Name = GetName('F', field, new ParameterInfo[0], typeArguments, _noTypes),
                Attributes = GetAttributes(field).ToList(),
                Parameters = new List<ParameterModel>(),
            };
        }

        private ParameterModel AnalyzeParameter(ParameterInfo arg)
        {
            if (arg == null)
            {
                throw new ArgumentNullException("arg");
            }
            return new ParameterModel
            {
                Name = arg.Name,
                Attributes = GetAttributes(arg).ToList(),
            };
        }

        private string GetName(
            char prefix, 
            [NotNull]MemberInfo member, 
            [NotNull]ParameterInfo[] parameters,
            [NotNull]Type[] typeArguments,
            [NotNull]Type[] memberArguments)
        {
            var sb = new StringBuilder();
            sb.Append(prefix);
            sb.Append(':');
            sb.Append(member.DeclaringType != null ? member.DeclaringType.FullName : string.Empty);
            sb.Append('.');
            sb.Append(member.Name.Replace('.', '#'));
            if (memberArguments.Length > 0)
            {
                sb.Append("``");
                sb.Append(memberArguments.Length);
            }
            if (parameters.Length > 0)
            {
                sb.Append('(');
                for (int i = 0; i < parameters.Length; ++i)
                {
                    if (parameters[i] == null)
                    {
                        throw new InvalidOperationException(string.Format("Parameter #{0} of {1} declared on {2} is null", i, member, member.DeclaringType));
                    }
                    if (i > 0)
                    {
                        sb.Append(',');
                    }
                    sb.Append(GetName(parameters[i].ParameterType, typeArguments, memberArguments));
                }
                sb.Append(')');
            }
            return sb.ToString();
        }

        private string GetName([NotNull]Type type, [NotNull]Type[] typeArguments, [NotNull]Type[] memberArguments)
        {
            if (type.IsGenericParameter)
            {
                for (int i = 0; i < typeArguments.Length; ++i)
                {
                    if (typeArguments[i] == type)
                    {
                        return "`" + i;
                    }
                }
                for (int i = 0; i < memberArguments.Length; ++i)
                {
                    if (memberArguments[i] == type)
                    {
                        return "``" + i;
                    }
                }
            }
            if (type.IsArray)
            {
                var elementType = type.GetElementType();
                if (elementType == null)
                {
                    throw new InvalidOperationException(string.Format("Element type of {0} is null", type));
                }
                return string.Format("{0}[{1}]", 
                    GetName(elementType, typeArguments, memberArguments),
                    new string(',', type.GetArrayRank() - 1));
            }
            if (type.IsByRef)
            {
                var elementType = type.GetElementType();
                if (elementType == null)
                {
                    throw new InvalidOperationException(string.Format("Element type of {0} is null", type));
                }
                return GetName(elementType, typeArguments, memberArguments) + '@';
            }
            if (type.IsGenericType)
            {
                return string.Format("{0}.{1}{{{2}}}",
                    type.Namespace,
                    type.Name.Substring(0, type.Name.IndexOf('`')),
                    string.Join(",", type.GetGenericArguments().Select(a => GetName(a, typeArguments, memberArguments))));
            }
            return type.FullName;
        }

        [NotNull]
        private IEnumerable<AttributeModel> GetAttributes([NotNull] MemberInfo member)
        {
            return LoadAttributes(member).Select(MakeAttributeModel);
        }

        [NotNull]
        private IEnumerable<AttributeModel> GetAttributes([NotNull] ParameterInfo member)
        {
            return LoadAttributes(member).Select(MakeAttributeModel);
        }

        private AttributeModel MakeAttributeModel(CustomAttributeData arg)
        {
            if (arg == null || arg.Constructor == null)
            {
                throw new ArgumentNullException("arg");
            }
            return new AttributeModel
            {
                Constructor = GetName('M', arg.Constructor, arg.Constructor.GetParameters(), _noTypes, _noTypes),
                Arguments = arg.ConstructorArguments.Select(a => XmlConvert.EncodeName(a.ToString())).ToList(),
            };
        }

        [NotNull]
        private IEnumerable<CustomAttributeData> LoadAttributes([NotNull]MemberInfo member)
        {
            var attrs = member.GetCustomAttributesData();
            return attrs != null ? attrs.Where(IsResharperAttribute) : Enumerable.Empty<CustomAttributeData>();
        }

        [NotNull]
        private IEnumerable<CustomAttributeData> LoadAttributes([NotNull]ParameterInfo param)
        {
            var attrs = param.GetCustomAttributesData();
            return attrs != null ? attrs.Where(IsResharperAttribute) : Enumerable.Empty<CustomAttributeData>();
        }

        private bool IsResharperAttribute(CustomAttributeData attr)
        {
            return attr != null && attr.AttributeType != null && IsResharperAttributeNamespace(attr.AttributeType.Namespace);
        }

        private bool IsResharperAttributeNamespace(string ns)
        {
            return ns != null && ns.StartsWith("JetBrains.Annotations");
        }
    }
}
